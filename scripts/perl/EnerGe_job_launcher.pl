#!/usr/bin/perl
use strict;
use warnings;
local $| = 1; # flush print

################################################################################
# Basic emulation of multi thread for EnerGe simulation at ccin2p3
#
# The input must be a .mac file, with the desired number of events,
# so as if you were launching your simulation on a single CPU.
#
# Some parameters needed by the script are directly written in the section TUNABLE PARAMETERS
# Please adapt them to your will before launching the script (or it will quickly die)
#
# This script first checks for necessary folder: where results (ROOT) files,
# shell scripts and intermediary .mac files will be created
# This folder is TUNABLE PARAMETER
# Then it looks for arguments, number of arguments can be 1 to 5:
#	the last argument must be the .mac file
#	available options are -L/-NL (enable/disable launching of the jobs)
#	-f force overwritting on existing files or folders
#	and -c (followed by value_of_coeff, see comments of $coeff)
# $coeff (-c) and $flag_L (-L/-NL) are TUNABLE PARAMETERS which change the default behaviour
#
# Then the .mac file is read, and the number of events is extracted
# which allows to calculate the number of needed job ; the formula used is:
# nb job(s) = int((nb evts - 1)/$max_UI_power*$coeff)+1;
#
# Then new folders are created, in the folder from TUNABLE PARAMETERS
# called as the .mac file without ".mac". If the folder already exist, a
# increment is added.
#
# Finally shell (from a model) and .mac (from the read one) files are created.
# Jobs are launched if $flag_L is set to >0 by option or value.
#
# Jonathan Gaffiot, 30/03/10, last mod 24/01/16
# Matthieu Vivier, 15/06/2017, adapated for Choozerent simulation at CCIN2P3
# Chloe Goupy, 06/06/2023, copied from Choozerent and adapted for EnerGe and slurm manager at CCIN2P3
################################################################################

######################## TUNABLE PARAMETERS ########################
# Options and parameters of the script
my $flag_L = 1; # (option -L/NL) 1 to launch jobs at the end of the script, 0 do not
my $flag_f = 0; # (option -f) 1 to force writting on existing folders (and files), 0 do not
my $max_UI_power = 151200; # (option -max) max UI power which will be asked. This default value corresponds to 42 hours of CPU time out of 48 possible in the queue named "long".
my $coeff = 1.5; # (option -c) $max_UI_power divide by nb of evts of your simulation done with $max_UI_power
# do not forget a margin for safety...
my $dataFile = '';
my $name_dataFile = '';
# Folder
my $rep_base = '/sps/nucleus/cgoupy/EnerGe';

################################################################################

# Global variables
my $name='';
my $path='';
my $TEXT_mac_ref='';
my $NbEvts=0;
my $NbJob=0;
my $HEPevt_file='';
my $HEPevt_shift='';
my $input_file='';
#my $exe="new";
my $opt_in="-m";

################################################################################

print "Checking environment variables... ";
my @env_list=('ENERGE_PATH','ROOTSYS','G4WORKDIR','G4VERSIONCODE');# 'THrONG_DIR', 'NeutronHPCrossSections','LIBPATH','G4SYSTEM
foreach my $el (@env_list) {
    if (not $ENV{$el}) {
        die "\n$el env var missing\nSource group_profile with environment variable EnerGe defined ?\n";
    }
}

#if ($ENV{'G4VERSIONCODE'} == 40904) {
#  print "OLD simu found... ";
#  $exe = "old";
#  $opt_in = "-i";
#}
if ($ENV{'G4VERSIONCODE'} == 41007) {
    print "Geant 4.10.7 simulation package found... ";
} else {
    die "\n => Can not find correct Geant 4 simulation (4.10.7): ",$ENV{'G4VERSIONCODE'},"\n";
}

if (not -x "/pbs/throng/nucleus/cgoupy/EnerGe/bin/EnerGe_sim") {
    die "\n => Can not find EnerGe_sim executable in /pbs/throng/nucleus/cgoupy/EnerGe/bin/\n Check symlink and/or (re)compile simulation ?\n";
}

################################################################################

my $TEXT_job_ref = "#!/bin/bash
#SBATCH --account=nucleus
#SBATCH --time UIPOWER_MIN:UIPOWER_SEC
#SBATCH --mem=8G
#SBATCH -L sps
#SBATCH -n 1
#SBATCH -J JOB_NAME
#SBATCH --output=STDOUT
\necho\necho \'========================================\'
echo \'Launching EnerGe Simulation:\'
echo \/pbs/throng/nucleus/cgoupy/EnerGe/bin/EnerGe_sim -b -s SEED $opt_in FILE_MAC -o FILE_ROOT -f FILE_DATA
echo \'========================================\'
echo\n\n\/pbs/throng/nucleus/cgoupy/EnerGe/bin/EnerGe_sim -b -s SEED $opt_in FILE_MAC -o FILE_ROOT -f FILE_DATA
\necho\necho \'========================================\'
echo \'Results are in REP_OUT\'
echo \'========================================\'
echo\n
sacct --name=JOB_NAME --format='Submit, Start, Elapsed, ReqCPUS, TimeLimit, CPUTime, JobID'
";

################################################################################

print "OK\tfolders... ";
if (not -d $rep_base) {
    die "\n=> Change the variable \$rep_base, written in the text of this script. Here \$rep_base=$rep_base, a missing folder on this computer\n";
}
if (not $rep_base =~ /\/$/) { $rep_base .= '/'; }

################################################################################

print "OK\targuments... ";
if (@ARGV == 0 or @ARGV>6) {
    die "\n=> Incorrect number of arguments: possibilities are 1 to 6 and here is @{[$#ARGV+1]}
    The last argument must be the .mac file
    Available options are:
    -L/-NL (enable/disable launching of the jobs)
    -c <value_of_coeff> (put \$coeff to the given value)
    -max <value_of_max_UI_power> (put \$max_UI_power to the given value)
    -f (force writting on existing folders and files)\n";
}
if ($ARGV[-1] =~ /(\w+)\.mac$/) {
    $input_file = $ARGV[-1];
    $name = $1;
    $path = $rep_base.'REP_'.$name;
} else { die "\n=> The last argument must be a .mac file\n"; }

if ($#ARGV>0) {
ARG:for (my $i=0; $i<$#ARGV; $i++) {
    if ($ARGV[$i] eq '-L')  { $flag_L=1; }
    elsif ($ARGV[$i] eq '-NL') { $flag_L=0; }
    elsif ($ARGV[$i] eq '-f')  { $flag_f=1; }
    elsif ($ARGV[$i] eq '-c') {
        if  ($ARGV[++$i] =~ /^\d+(\.\d*)?$/) { $coeff=$ARGV[$i]; }
        else { die "\n=> Options -c must be followed by the new value of the coeff, and here is $ARGV[$i]\n"; }
    }
    elsif ($ARGV[$i] eq '-max') {
        if  ($ARGV[++$i] =~ /^\d+(\.\d*)?$/) { $max_UI_power=$ARGV[$i]; }
        else { die "\n=> Options -max must be followed by the new value of the max_UI_power, and here is $ARGV[$i]\n"; }
    }
    elsif($ARGV[$i] eq '-data'){
        if  ($ARGV[++$i] =~ /(\w+)\.dat$/) { $dataFile=$ARGV[$i]; $name_dataFile = $1;}
        else { die "\n=> Options -data must be followed by the a .dat file, and here is $ARGV[$i]\n"; }
    }else { die "\n=> Incorrect option $ARGV[$i]: can only be -L or -NL or -c or -f or -max\n"; }
}
}
print "OK";

################################################################################

print "\t.mac file...";
open (my $H_INPUT,"<",$input_file)  or die "\nHANDLE H_INPUT: Can't open file $input_file: $!\n";
INPUT:while(my $line=<$H_INPUT>) {
    if ($line =~ /^\s*#/ or $line eq "") { next INPUT; }

        if ($line =~ /^\s*\/run\/beamOn\s+(\d+)/) {
            $NbEvts=$1;
            $TEXT_mac_ref .= "/run/beamOn NB_EVTS\n";
        }
    elsif ($line =~ /^\s*\/generator\/vtx\/set\s+HEPevt/) {
        if ($line =~ /^\s*\/generator\/vtx\/set\s+HEPevt\s+"([^"]+)"/) {
            $HEPevt_file = $1;
        $TEXT_mac_ref .= "/generator/vtx/set HEPevt SOURCE_FILE\n";
    } else { die "\n=> Incorrect format to set HEPevt, need 1 argument between quotes: $line\n"; }
}
elsif ($line =~ /^\s*\/generator\/vtx\/set\s+18\s+"([^"]+)"/) {
$HEPevt_file = $1;
$TEXT_mac_ref .= "/generator/vtx/set 18 SOURCE_FILE\n";
} else { $TEXT_mac_ref .= $line; }
}
close $H_INPUT;

if (not $NbEvts) { die "\n=> String /run/beamOn\\s+\\d+ not find in this file => no evts\n"; }
if ($NbEvts<1000) { die "\n=> For $NbEvts events, you don't need this script !\n"; }
if ($HEPevt_file) {
    print "\tsource found...";

    if ($HEPevt_file =~ /\s/) {
        my @tab = split(/\s+/, $HEPevt_file);
        if (@tab != 2) { die "\n=> Incorrect format to set HEPevt, need a file path and optionnally a shift: $HEPevt_file\n"; }
        $HEPevt_file = $tab[0];
        $HEPevt_shift = $tab[1];
        my $number = "[+-]?[0-9]+\.?[0-9]*([eE][+-]?[0-9]+)?";
        if (not $HEPevt_shift =~ /^\($number,$number,$number\)$/) {
            die "\n=> Incorrect format to set HEPevt, shift must be written (x,y,z): $HEPevt_shift\n";
        }
    }

    if (not -e $HEPevt_file) { die "\n=> HEPevt file missing: $HEPevt_file\nCheck the path ?\n"; }

    my $NbHEPevt= 0;
    open(my $H_HEPevt,"<",$HEPevt_file) or die "\nHANDLE H_HEPevt: Can't open file $HEPevt_file: $!\n";
    while (my $line=<$H_HEPevt>) { chomp $line; $NbHEPevt++ if $line =~ /^\s*\d+\s*$/; }
    close $H_HEPevt;
    if ($NbEvts > $NbHEPevt) { die "\n=> Less events in HEPevt file ($NbHEPevt) than needed ($NbEvts)\nRegenerate the HEPevt file ?\n"; }
}
print "OK\n";

################################################################################

print "====================================================================\n";
print "$NbEvts EVENTS ASKED\n";
print "THE POWER/EVENTS COEFF IS $coeff (UI power / nb events)\n";
print "AND THE MAX UI POWER IS $max_UI_power (in #PBS -l T=...)\n";

$NbJob = sprintf("%d",($NbEvts-1)/$max_UI_power*$coeff)+1;
if ($NbJob > 1) {
    $NbEvts = sprintf("%d",$NbEvts/$NbJob)+1;
    if($HEPevt_file) { $NbEvts--; } # to be sure we have enough events in the HEPevt file
}
if ($NbJob > 999 ) { # This is a try
    die "=> The script would create and perhaps launch $NbJob files, which seems too many (>99).
    Anyway, small modifications would be necessary since numerotation is truncated to 2 figures\n";
}

if ($NbJob == 1) {print "SO 1 JOB WILL BE CREATED\n"; }
else { print "SO $NbJob JOBS WILL BE CREATED with $NbEvts events\n"; }
if ($flag_L>0) {print "BEWARE: JOBS WILL BE LAUNCHED\n"; }
else { print "Jobs won't be launch, but files will be created\n"; }
print "====================================================================\n\n";

################################################################################

if (not $flag_f) {
    print "Creating new folder:\n";
    if ( -d $path ) {
        my $i=1;
        my $new_name = $name."_$i";
    NAME:while(-d $rep_base.'REP_'.$new_name) {
        $i++;
        $new_name = $name."_$i";
    }
        $name = $new_name;
        $path = $rep_base.'REP_'.$new_name;
    }
}
else { print "Creating or overwritting folders:\n"; }
mkdir $path or die "=> Can't create or overwrite $path: $!\n";
print "$path\n\n";

################################################################################

# Copy the original .mac file
` cp $input_file $path/$name.mac `;
#Copy the original .dat file
if($dataFile eq ''){
    $dataFile = '/pbs/throng/nucleus/cgoupy/EnerGe/simulation/data/EnerGe_Options.dat';
    $name_dataFile = 'EnerGe_Options';
}
    ` cp $dataFile $path/$name_dataFile.dat `;


# Create the launching files
if ($NbJob==1) { &MakeShFile($name); } # Will use original mac file, just create the shell file to launch the job
else {
    # In case splitting the original HEPevt file is needed.
    my $H_IN_HEP;
    my $NbHEPevt= 0;
    my $line="";
    if($HEPevt_file) {
        open ($H_IN_HEP,"<",$HEPevt_file) or die "HANDLE H_IN_HEP: Can't open file $HEPevt_file: $!\n";
    }
    
    # Loop over jobs
    CREATE:for (my $i=0; $i<$NbJob; $i++) {
        # The job name
        my $job_name = sprintf("%s%03d","$name.",$i);
            # which would be my $job_name = sprintf("%s%02d","$name.",$i); for 2 digits
        # Creating the mac file
        my $job_mac_file = "$path/$job_name.mac";
        my $TEXT_mac = $TEXT_mac_ref;
        $TEXT_mac =~ s/NB_EVTS/$NbEvts/;

        if($HEPevt_file) {
            if (not $HEPevt_file =~ /([^\/]+)$/ ) { die "=> Incorrect HEPevt file name: $HEPevt_file\n"; }
            my $job_HEPevt_file = $1;
            $job_HEPevt_file =~ s/\.\w+$//;
            $job_HEPevt_file = sprintf("%s%03d%s","$path/$job_HEPevt_file.",$i,".txt");
                    #         $job_HEPevt_file = sprintf("%s%02d%s","$path/$job_HEPevt_file.",$i,".txt");
            if ($HEPevt_shift) { $TEXT_mac =~ s/SOURCE_FILE/"$job_HEPevt_file $HEPevt_shift"/; }
            else { $TEXT_mac =~ s/SOURCE_FILE/"$job_HEPevt_file"/; }

            my $H_OUT_JOB_HEP;
            my $NbJobEvt=0;
            open ($H_OUT_JOB_HEP,">",$job_HEPevt_file) or die "\nHANDLE H_OUT_JOB_HEP: Can't open file $job_HEPevt_file: $!\n";
            print "Creating HEPevt file: $job_HEPevt_file from evt n°$NbHEPevt";
            print $H_OUT_JOB_HEP $line;
        JOB_HEP:while ($line=<$H_IN_HEP>) {
            if ($line =~ /^\s*\d+\s*$/) { $NbHEPevt++; $NbJobEvt++; }
            if ($NbJobEvt == $NbEvts+1) { $NbHEPevt--; last JOB_HEP; }
            print $H_OUT_JOB_HEP $line;
        }
            print " to evt n°$NbHEPevt\n";
            close $H_OUT_JOB_HEP;
        }

        open (my $H_OUT_MAC,">",$job_mac_file)  or die "HANDLE H_OUT_MAC: Can't open file $job_mac_file: $!\n";
        print $H_OUT_MAC $TEXT_mac;
        close $H_OUT_MAC;

        print "Creating mac file: $job_mac_file\n";
    
        # Creating the shell file to launch the job
        &MakeShFile($job_name);
        }
    
    if ($HEPevt_file) { close $H_IN_HEP; }
}

################################################################################
if ($flag_L==0) {
    print "\n******************** NORMAL END OF THE SCRIPT ********************\n\n";
    exit 0;
}

print "Launching job".($NbJob>1?"s":"").":\n";
print "====================================================================\n";
if ($NbJob>1) {
LAUNCH:for (my $i=0; $i<$NbJob; $i++) {
    &Launch( sprintf("%s%03d%s","$path/JOB_$name.",$i,".sh")) ;
        # &Launch( sprintf("%s%02d%s","$path/JOB_$name.",$i,".sh")) ;
}
} else { &Launch("$path/JOB_$name.sh"); }
print "====================================================================\n";
print "******************** NORMAL END OF THE SCRIPT ********************\n\n";
exit 0;

################################################################################
################################################################################

sub MakeShFile
{
    my ($local_name) = @_;

    my $sh_file = "$path/JOB_$local_name.sh";
    my $mac_file = "$path/$local_name.mac";
    my $data_file = "$path/$name_dataFile.dat";
    my $root_file = "$path/$local_name.root";
    my $out_file = "$path/$local_name.out";
    my $seed = int(rand(2147483648));

    my $TEXT_job = $TEXT_job_ref;
    my $UI_power = $NbEvts*$coeff;

     #Conversion of time from seconds to minutes:seconds
    my $UI_power_min=int($UI_power/60);
    my $UI_power_sec=$UI_power%60;

    $TEXT_job =~ s/UIPOWER_MIN/$UI_power_min/;
    $TEXT_job =~ s/UIPOWER_SEC/$UI_power_sec/;
    $TEXT_job =~ s/JOB_NAME/JOB_$local_name/g;
    $TEXT_job =~ s/STDOUT/$out_file/;
    $TEXT_job =~ s/FILE_MAC/$mac_file/g;
    $TEXT_job =~ s/FILE_DATA/$data_file/g;
    $TEXT_job =~ s/FILE_ROOT/$root_file/g;
    $TEXT_job =~ s/SEED/$seed/g;
    $TEXT_job =~ s/REP_OUT/$path/;

    open (my $H_OUT_SH,">",$sh_file)  or die "HANDLE H_OUT_SH: Can't open file $sh_file: $!\n";
    print $H_OUT_SH $TEXT_job;
    close $H_OUT_SH;
    print "Creating $sh_file\n\n";
    return;
}

################################################################################

sub Launch
{
    my ($local_sh_file) = @_;
    my $local_name = $local_sh_file;
    $local_name =~ s/.sh//;

    if (not -e $local_sh_file) { die "=> Impossible to launch job: $local_sh_file missing\n"; }
    print "Launching $local_sh_file\n";
    `sbatch $local_sh_file `;
  return;
}
