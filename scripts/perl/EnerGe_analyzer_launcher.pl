#!/usr/bin/perl
use strict;
use warnings;
use Cwd;

################################################################################
# Analyzer launcher for EnerGe at ccin2p3
#
# The input must be the standard analyzer imputs,
# so as if you were launching your analysis on a local computer.
# BUT:
# -b (batch) option will be set in any case
# -p (path) option will be redirected to $rep (see below)
# stdout and stderr will be redirected to $test (see below)
# So you can basically skip -b -p options.
#
# Jonathan Gaffiot, 21 april 2012
# Matthieu Vivier, 30/11/2017, adapated for Choozerent at CCIN2P3
# Chloe Goupy, 06/06/2023, copied from Choozerent and adapted for EnerGe and slurm manager at CCIN2P3
################################################################################

# Reference analysis time per file (cpu time in seconds)
my $max_time = 151200; # This default value corresponds to 42 hours of CPU time out of 48 possible in the queue named "long".
my $ref_time = 50;

# Folder and executable
my $rep = '/sps/nucleus/cgoupy/EnerGe/REP_Cryostat_Full4piCOVPrototype_2cmthick_AtmMuons_1/Analysis/Thr_data/';
my $test = '/sps/nucleus/cgoupy/EnerGe/REP_Cryostat_Full4piCOVPrototype_2cmthick_AtmMuons_1/Analysis/Thr_data/';
my $exe = '/pbs/throng/nucleus/cgoupy/EnerGe/bin/analyser';

-d $rep or die "***Error1.0: Analysis folder missing: $rep\nCheck the path ?\n";
-d $test or die "***Error1.2: Test folder missing: $test\nCheck the path ?\n";
(-f $exe and -x $exe) or die "***Error1.4: analyzer missing: $exe\nCheck the path ?\n";

# Base script for job launching
my $TEXT_job_ref = "#!/bin/bash
#SBATCH --account=nucleus
#SBATCH --time UIPOWER_MIN:UIPOWER_SEC
#SBATCH --mem=8G
#SBATCH -n 1
#SBATCH -L sps
#SBATCH -J JOB_NAME
#SBATCH --output=STDOUT
\necho\necho \'========================================\'
\necho NB files: LIST\necho EXE_LINE
echo \'========================================\'\necho\n
echo\n\n\ EXE_LINE
echo \'========================================\'
echo\n
sacct --name=JOB_NAME --format='Submit, Start, Elapsed, ReqCPUS, TimeLimit, CPUTime, JobID'";

# Global variables
my @run_list=();
my @run_list_list=();
my @job_list=();
my $exe_line = "$exe -b -p $rep";
my $TEXT_job='';
my ($opt, $name, $file, $list, $root_path, $first_run, $last_run, $time);
my $nb_job=1;
my $nb_run=0;
my $nb_run_per_job=0;
my $simu=0;

# Special analyzer_launcher arguments
my $special_arguments =
"###  SPECIAL: analyzer_launcher arguments: [-t run_time] [-T max_time] ###
'-t' must be followed by the time to analyze 1 run
'-T' must be followed by the maximal time you want to allow for a job,
in order to split your analysis in several jobs
Default: run_time = $ref_time and max_time = $max_time
\t________________________________________\n\n";

################################################################################
# Check of arguments
ARG:for (my $i=0; $i<$#ARGV+1; $i++) { # need to remove time option before test with analyzer
    $ARGV[$i] eq '-t' and $time = $ARGV[$i+1], splice(@ARGV, $i, 2), last ARG;
}
ARG:for (my $i=0; $i<$#ARGV+1; $i++) { # need to remove time option before test with analyzer
    $ARGV[$i] eq '-T' and $max_time = $ARGV[$i+1], splice(@ARGV, $i, 2), last ARG;
}
if ($#ARGV<0) {
    print ` $exe `;
    print $special_arguments;
    die "No arguments. Exiting...\n\n";
} # no arguments
elsif (system(join(' ',$exe, '-d', @ARGV, "1>/dev/null 2>/dev/null"))>>8 != 0) { # calling the analyzer with 'die' option and no output to test arguments
    system(join(' ',$exe, @ARGV));
    print $special_arguments;
    die "Incorrect arguments. Exiting...\n\n"; # display error output and exit this script
}

# Analysis of arguments list
ARG:for (my $i=0; $i<$#ARGV+1; $i++) {
    $ARGV[$i] eq '-b' and next ARG;
    if ( $ARGV[$i] eq '-d') { system(join(' ',$exe, @ARGV)); exit(1); }
    $ARGV[$i] eq '-p' and $i++, next ARG;
    if ($ARGV[$i] eq '-o' ) {
        $opt = $ARGV[$i+1];
        if ($opt eq '') { $opt = 'E'; $simu=1;} #doing edep algo by default
        if ($opt =~ /I/ or $opt =~ /simu/) { $simu=1; }
        $exe_line .= " -o $opt";
        $i++, next ARG;
    }
    $ARGV[$i] eq '-f' and $first_run = $ARGV[$i+1], $i++, next ARG; # treated when reading the run_lists
    $ARGV[$i] eq '-l' and $last_run = $ARGV[$i+1], $i++, next ARG; # treated when reading the run_lists
    $ARGV[$i] eq '-r' and $root_path = $ARGV[$i+1], $i++, next ARG;
    if ($ARGV[$i] =~ /\.root$/) { push(@run_list, $ARGV[$i]); }
    else { push(@run_list_list, $ARGV[$i]); }
}

################################################################################
# Setting path for Root files
if ($root_path !~ /^\/sps/) { die "***Error2.1: path for root files must be an absolute path (/sps/nucleus/...)\n"; }
#else {$root_path = getcwd(); } #get current working directory
$root_path .= "/" if $root_path !~ /\/$/;

# Check files in run_list
foreach my $file (@run_list) {
    if ($file !~ /^\/sps/) {
        #    $file =~ /[^\/]+$/;
        #    $file = $root_path.$&;
        $file = $root_path.$file;
    }
    my $i=0;
    while ($i < @run_list) {
        if ( not -f $run_list[$i]) {
            warn "*Warning: skipping file $file (not found in $root_path)\n";
            splice @run_list, $i, 1;
        } else { $i++; }
    }
}

# Reading run_lists
$first_run and $first_run =~ s/^.+-(\d+)\.root$/$1/;
$last_run and $last_run =~ s/^.+-(\d+)\.root$/$1/;
foreach $list (@run_list_list) {
    open (F1,"<$root_path$list") or die "HANDLE F1: Can not open file $list: $!\n";
LINE:while (my $line=<F1>) {
    $line =~ s/\n//;
    $line =~ s/^(.+\.root).+$/$1/;
    (my $num = $line) =~ s/^.+-(\d+)\.root.+$/$1/;
    if ($first_run and $num < $first_run) { next LINE; }
    if ($last_run and $num > $last_run) { last LINE; }
    if ($line =~ /\.root$/ and -f "$root_path$line") {
        push(@run_list, "$root_path$line");
    } else { warn "*Warning: skipping file $line in run_list $list (not found in $root_path or not ending with .root)\n"; }
}
    close F1;
}
@run_list = sort(@run_list);
$nb_run = @run_list;
$nb_run or die "***Error3: no run to process\n";

# Number of job
$time = $ref_time if not $time;
$time *= $nb_run;
if ($time>$max_time) {
    print "*Warning Analyzer_launcher::time: required time ($time) exceed maximum computing time at CCIN2P3 ($max_time)\n";
    print "Trying to split file list in several jobs of max time\n";
    
    $nb_job = int($time / $max_time) + 1;
    $time = $max_time;
    $nb_run_per_job = int($nb_run / $nb_job) + 1;
    
    $nb_job > 1000 and die " You were about to launch more than 1000 jobs, it seems really too much\nExiting...\n";
    print " You are about to launch $nb_job jobs\n Continue ? (y/n)\t";
    my $sdtin = <STDIN>;
    while ( $sdtin !~ /^y/i) {
        if ($sdtin =~ /^n/i) { exit 0; }
        print " Continue ? (y/n)\t";
        $sdtin = <STDIN>;
    }
}

################################################################################
# Creating the jobs
if ($nb_job==1) {
    # Dump of run_list in the exe line
    foreach my $file (@run_list) { $exe_line .= " $file"; }

    # Creation of script arguments
    if ($nb_run==1) {
        ($list = $ARGV[$#ARGV]) =~ s/^.+?(\d+)\.root$/$1/;
        $name = "$list"."_$opt";
    } else {
        ($first_run = $run_list[0]) =~ s/^.+?(\d+)\.root$/$1/;
        ($last_run = $run_list[$#run_list]) =~ s/^.+?(\d+)\.root$/$1/;
        $list = "from $first_run to $last_run";
        $name = "$first_run-$last_run"."_$opt";
    }

    my $out_file="$test/JOB_$name.out";
    #Conversion of time from seconds to minutes:seconds
    my $time_min=int($time/60);
    my $time_sec=$time%60;

    # Creation of the actual script text
    $TEXT_job = $TEXT_job_ref;
    $TEXT_job =~ s/UIPOWER_MIN/$time_min/;
    $TEXT_job =~ s/UIPOWER_SEC/$time_sec/;
    $TEXT_job =~ s/JOB_NAME/JOB_$name/;
    $TEXT_job =~ s/STDOUT/$out_file/;
    $TEXT_job =~ s/NB/$nb_run/;
    $TEXT_job =~ s/LIST/$list/;
    $TEXT_job =~ s/EXE_LINE/$exe_line/g;
    
    # Creation of the job file
    $file = "JOB_analyzer_$name.sh";
    open (F21,">$rep$file")  or die "HANDLE F21: Can not open file $rep$file: $!\n";
    print F21 $TEXT_job;
    close F21;
    push(@job_list, "$rep$file");
}
else {
    for (my $job=0; $job<$nb_job; $job++) {
        # Search for first and last job
        $first_run = $run_list[$job*$nb_run_per_job];
        if ($job<$nb_job-1) { $last_run = $run_list[($job+1)*$nb_run_per_job-1]; }
        else { $last_run = $run_list[$#run_list]; }
        
        # Dump of run_list in the exe line
        my $job_exe_line = $exe_line;
        for (my $i=$job*$nb_run_per_job; $i<@run_list && $i<($job+1)*$nb_run_per_job; $i++) {
            $job_exe_line .= " $run_list[$i]";
        }
        
        # Creation of script arguments
        $first_run =~ s/^.+?(\d+)\.root$/$1/;
        $last_run =~ s/^.+?(\d+)\.root$/$1/;
        $list = "from $first_run to $last_run";
        $name = "$first_run-$last_run"."_$opt";
	
    	my $out_file="$test/JOB_$name.out";
    	#Conversion of time from seconds to minutes:seconds
        my $time_min=int($time/60);
        my $time_sec=$time%60;                

        # Creation of the actual script text
        $TEXT_job = $TEXT_job_ref;
        $TEXT_job =~ s/UIPOWER_MIN/$time_min/;
        $TEXT_job =~ s/UIPOWER_SEC/$time_sec/;
        $TEXT_job =~ s/JOB_NAME/JOB_$name\.$job/;
        $TEXT_job =~ s/STDOUT/$out_file/;
        $TEXT_job =~ s/NB/$nb_run_per_job/;
        $TEXT_job =~ s/LIST/$list/;
        $TEXT_job =~ s/EXE_LINE/$job_exe_line/g;
        
        # Creation of the job file
        $file = "JOB_analyzer_$job.sh";
        open (F22,">$rep$file")  or die "HANDLE F22: Can not open file $rep$file: $!\n";
        print F22 $TEXT_job;
        close F22;
        push(@job_list, "$rep$file");
    }
}

################################################################################
# Launching the jobs
print "Launching job(s):\n";
print "====================================================================\n";
foreach $file (@job_list) {
    print "Launching $file\n";
    `sbatch $file `;
    #` mv $file $trash `;
}
print "====================================================================\n";

################################################################################
print "\n******************** NORMAL END OF THE SCRIPT ********************\n\n";
exit 0;
