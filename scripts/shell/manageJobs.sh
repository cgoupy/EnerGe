#!/bin/bash

# script to run APP_NAME jobs

# activate debuging
#set -vx

# 
APP_NAME="EnerGe"

# choose one of the applications (uncomment the corresponding APP_NAME line), 
# then edit ARG_SINGULARITY_COMMAND (somewhere below)
#    
# for simulation (executable: EnerGe_sim) see
#     https://gitlab.maisondelasimulation.fr/CENNS/EnerGe/blob/master/simulation/EnerGe_sim.cc#L62
APP_TO_RUN="simulation"

# for analyser (executable: analyser) see
#     https://gitlab.maisondelasimulation.fr/CENNS/EnerGe/blob/master/analyser/analyser.cc
#APP_TO_RUN="analyser"

usage() {
    printf "\n Script to run ${APP_NAME} jobs\n"
    printf "\n Usage:"
    printf "\n    download/copy manageJobs.sh script in a directory"
    printf "\n    verify in manageJobs.sh the environment (ENV), the COMPUTING_SYSTEM and the run type (RUN_TYPE) configuration"
    printf "\n    the run type configuration contains the SLURM options"
    printf "\n    adjust in each code block the parameters, if needed"
    printf "\n    define in the ARGUMENTS block other arguments, if needed"
    printf "\n    for simulation, execute this script (arguments are case sensitive)"
    printf "\n        ./manageJobs.sh ENV COMPUTING_SYSTEM RUN_TYPE [\"--test-only\"] MACRO_FILE OPTION_FILE [OTHER_ARGUMENTS]"
    printf "\n    for analyser, execute this script (arguments are case sensitive)"
    printf "\n        ./manageJobs.sh ENV COMPUTING_SYSTEM RUN_TYPE [\"--test-only\"] analyser"
    printf "\n ENV environment options"
    printf "\n    OS_INSTALL "
    printf "\n    SINGULARITY "
    printf "\n COMPUTING_SYSTEM computing system"
    printf "\n    CLIP "
    printf "\n    DRACO "
    printf "\n    VMG - V.M. Ghete mobile workstation"
    printf "\n RUN_TYPE options"
    printf "\n    SRUN run SLURM interactive jobs"
    printf "\n    SBATCH run SLURM batch jobs"
    printf "\n    INT run non-SLURM interactive jobs"
    printf "\n MACRO_FILE"
    printf "\n    macro file for EnerGe_sim (corresponds to \"-m MAC_FILE\" )"
    printf "\n OPTION_FILE"
    printf "\n    option file for EnerGe_sim (corresponds to \"-f OPTION_FILE\" )"
    printf "\n OTHER_ARGUMENTS: optional arguments for the singularity command"
    printf "\n    must fit with the arguments required by the run command"
    printf "\n"
    }


if [[ ${APP_TO_RUN} == "simulation" && $# -lt 5 ]]; then
    printf "\n $0 requires at least five arguments. See usage below.\n"
    usage
    exit 1
elif [[ ${APP_TO_RUN} == "analyser" && $# -lt 4 ]]; then
    printf "\n $0 requires at least three arguments and correct ARG_SINGULARITY_COMMAND. See usage below.\n"
    usage
    exit 1
else
    if [[ $1 == "help" || $1 == "-help" || $1 == "--help" ]]; then
        usage
        exit 1
    fi
fi

# https://gist.github.com/earthgecko/3089509
# bash generate random 8 character alphanumeric string (upper and lowercase) and 
NEW_UUID=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 8 | head -n 1)
JOB_ID=$(date -d "today" +"%Y-%m-%d_%H-%M")_${NEW_UUID}

# job directory - all results are saved in JOB_DIR directory 
JOB_DIR=${JOB_ID}
mkdir -p ${JOB_DIR}

# write to console and log file (bash script!)
LOG_FILE="${JOB_DIR}/manageJobs_${JOB_ID}".log

exec 3<&1
coproc mytee { tee ${LOG_FILE} >&3; }
exec >&${mytee[1]} 2>&1

printf "\n\n Log file: "
printf " $LOG_FILE \n" 


if [[ $# -ge 3 ]]; then
    
    ENV=$1
    if [[ $ENV == "OS_INSTALL" ]]; then
        # FIXME do nothing else for now
        :
    elif [[ $ENV == "SINGULARITY" ]]; then
        SINGULARITY_VERSION_REQUIRED="3.0.0"
        SINGULARITY_HUB="SingularityHub/EnerGe/v2.3/sif-v0.0.1/x86_64-centos7-gcc73-opt"
    else
        printf "\n No such environment $1 defined. Exiting!\n"
        usage
        exit 1
    fi
    printf "\n Using environment ${ENV}"
    
    
    COMPUTING_SYSTEM=$2
    if [[ ${COMPUTING_SYSTEM} == "CLIP" ]]; then
        BIND_STORAGE="/scratch-cbe,/scratch,/mnt/hephy/cresst,/mnt/hephy/nucleus"
        SINGULARITY_HUB_PATH="/mnt/hephy/nucleus"
        
        SIF_CONTAINER=${SINGULARITY_HUB_PATH}/${SINGULARITY_HUB}/"EnerGe.sif"
    elif [[ ${COMPUTING_SYSTEM} == "DRACO" ]]; then
        BIND_STORAGE="" #FIXME
        SINGULARITY_HUB_PATH="" #FIXME
        
        SIF_CONTAINER=${SINGULARITY_HUB_PATH}/${SINGULARITY_HUB}/"EnerGe.sif"
    elif [[ ${COMPUTING_SYSTEM} == "VMG" ]]; then
        BIND_STORAGE="/data/ghete"
        SINGULARITY_HUB_PATH="/data/ghete/NUCLEUS"
    
        SIF_CONTAINER=${SINGULARITY_HUB_PATH}/${SINGULARITY_HUB}/"EnerGe.sif"
        CURRENT_SIF_CONTAINER=${SIF_CONTAINER}

        # scratch storage
        CS_TMPDIR="/data/ghete/NUCLEUS/jobs"
    else
        printf "\n No such computer system $2 defined. Exiting!\n"
        usage
        exit 1
    fi
    printf "\n Using computer system ${COMPUTING_SYSTEM}"
    
    RUN_TYPE=$3
    if [[ ${RUN_TYPE} == "SRUN" || ${RUN_TYPE} == "SBATCH" ]]; then
        
        # add here all SLURM options common for SRUN and SBATCH
        # start always with a space/blank the option added, SLURM_OPTIONS_SRUN_SBATCH+="_blank_OPTION", the space serves as separator
        
        SLURM_OPTIONS_SRUN_SBATCH=" "

        if [[ $# -ge 4 ]]; then
            if [ $4 == "--test-only" ]; then
                # test-only: see srun/sbatch documentation
                # https://slurm.schedmd.com/sbatch.html, https://slurm.schedmd.com/srun.html
                # no job is actually submitted
                SLURM_OPTIONS_SRUN_SBATCH+=" --test-only"
            fi
        fi
        
        # assign a short name to your job - adjust as desired (default: ${JOB_NAME})
        JOB_NAME="EnerGe"
        SLURM_OPTIONS_SRUN_SBATCH+=" --job-name=${JOB_NAME}"

        # working directory of the batch script (full path or relative path 
        # to the directory where the command is executed)
        SLURM_OPTIONS_SRUN_SBATCH+=" --chdir=${JOB_DIR}"

        # STDOUT and ERR output file(s)
        # by default both standard output and standard error are directed to the same file.
        SLURM_OPTIONS_SRUN_SBATCH+=" --output=${JOB_NAME}.%N.%j.out"
        SLURM_OPTIONS_SRUN_SBATCH+=" --error=${JOB_NAME}.%N.%j.err"

        # partition (job queue) and qos (quality of service)
        # check configuration of the system with command `scontrol show partition`
        SLURM_OPTIONS_SRUN_SBATCH+=" --partition=c"
        SLURM_OPTIONS_SRUN_SBATCH+=" --qos=c_short"

        # e-mail 
        SLURM_OPTIONS_SRUN_SBATCH+=" --mail-type=ALL"
        SLURM_OPTIONS_SRUN_SBATCH+=" --mail-user=vasile-mihai.ghete@oeaw.ac.at"

        # computing requirements: number of nodes, number of tasks across all node, 
        # cores per task (>1 if multithread tasks), 
        SLURM_OPTIONS_SRUN_SBATCH+=" --nodes=1"
        SLURM_OPTIONS_SRUN_SBATCH+=" --ntasks=1"
        SLURM_OPTIONS_SRUN_SBATCH+=" --cpus-per-task=1"
        SLURM_OPTIONS_SRUN_SBATCH+=" --mem-per-cpu=1G"

        # add here all other SLURM options available for SBATCH only (e.g. --array)
        # idem, start always with a space/blank the option added
        SLURM_OPTIONS_SBATCH=" "
        if [[ ${RUN_TYPE} == "SBATCH" ]]; then
            printf ""                         # use printf to avoid a bash error if no option added
            #SLURM_OPTIONS_SBATCH+=" --array=0-3"
        fi

        # concatenate here the common options for SRUN & SBATCH and the options for SBATCH only
        SLURM_OPTIONS=${SLURM_OPTIONS_SRUN_SBATCH}
        SLURM_OPTIONS+=" "        
        SLURM_OPTIONS+=${SLURM_OPTIONS_SBATCH}
        
        # for sbatch, use "--wrap" to pass parameters instead of a separate script
        # idem, start always with a space/blank the option added
        if [[ ${RUN_TYPE} == "SBATCH" ]]; then
            SBATCH_WRAP_OPTIONS=" "
            if [[ ${COMPUTING_SYSTEM} == "CLIP" ]]; then
                SBATCH_WRAP_OPTIONS+=" module purge &&"
            elif [[ ${COMPUTING_SYSTEM} == "DRACO" ]]; then
                SBATCH_WRAP_OPTIONS+=" module purge &&"
                SBATCH_WRAP_OPTIONS+=" module load singularity &&"
            else
                # do nothing
                :
            fi
        fi
                 
        printf "\n Run type ${RUN_TYPE}"

    elif [[ ${RUN_TYPE} == "INT" ]]; then
        printf "\n Run type ${RUN_TYPE}"
        # FIXME add other things needed for interactive jobs (aka jobs outside SLURM)
    else
        printf "\n No such run type ${RUN_TYPE} defined. Exiting!\n"
        usage
        exit 1
    fi

    # add here arguments for the run command (ARGUMENTS block)
    if [[ $# -ge 4 && ${APP_TO_RUN} == "simulation" ]]; then
        if [ $4 != "--test-only" ]; then
            MAC_FILE=$4
            OPTION_FILE=$5
        else
            if [[ $# -ge 5 ]]; then
                MAC_FILE=$5
                OPTION_FILE=$6
            else
                printf "\n $0 requires at least five arguments if \"--test-only\" is used. See usage below.\n"
                usage
                exit 1
            fi
        fi
        
        # convert the MAC_FILE and the OPTION_FILE to absolute path, if needed
        # 
        CURRENT_DIR=$(pwd)
        if [[ "${MAC_FILE}" == /* ]]; then
            # for an absolute path, keep it as such
            MAC_FILE_ABS=${MAC_FILE}
        else
            # if relative path, add the absolute path of the current directory
            MAC_FILE_ABS=${CURRENT_DIR}/${MAC_FILE}
        fi

        if [[ "${OPTION_FILE}" == /* ]]; then
            # for an absolute path, keep it as such
            OPTION_FILE_ABS=${OPTION_FILE}
        else
            # if relative path, add the absolute path of the current directory
            OPTION_FILE_ABS=${CURRENT_DIR}/${OPTION_FILE}
        fi
        
    fi
    
    if [[ "$ENV" == "OS_INSTALL" ]]; then
        # 
        if [[ ${RUN_TYPE} == "SRUN" ]]; then
            printf "\n SLURM interactive jobs using srun: "
            printf "\n Not yet implemented \n" # FIXME
            exit 1        
        elif [[ ${RUN_TYPE} == "SBATCH" ]]; then
            printf "\n SLURM batch jobs: \n"
            printf "\n Not yet implemented \n" # FIXME
            exit 1
        elif [[ ${RUN_TYPE} == "INT" ]]; then
            printf "\n Interactive jobs: "
            printf "\n Not yet implemented \n" # FIXME
            exit 1
        else
            # do nothing, should not arrive here as RUN_TYPE is tested before
            :
        fi
            
    elif [[ "$ENV" == "SINGULARITY" ]]; then

        printf "\n\n $0 script requires singularity 3 version"

        if [[ ${COMPUTING_SYSTEM} == "DRACO" ]]; then 
            # load the latest singularity version available in the system
            module load singularity

            # alternatively, load a specific version of singularity 
            # (comment line with '--latest', uncomment line with version)
            # module load singularity/3.2.1
        fi
        
        # check singularity version
        SINGULARITY_VERSION_CURRENT="$(singularity version)"
        if [ "$(printf '%s\n' "$SINGULARITY_VERSION_REQUIRED" "$SINGULARITY_VERSION_CURRENT" | sort -V | head -n1)" = "$SINGULARITY_VERSION_REQUIRED" ]; then 
            printf "\n Current singularity version:  ${SINGULARITY_VERSION_CURRENT}"
            printf "\n .... ---> OK\n"
        else
            printf "\n Current singularity version $SINGULARITY_VERSION_CURRENT is smaller than required version $SINGULARITY_VERSION_REQUIRED"
            printf "\n Upgrade the installed singularity! \n Exiting!\n"
            exit 1
        fi

        # the arguments for the singularity command -adjust them as desired
        #
        if [[ ${APP_TO_RUN} == "simulation" ]]; then
            # for simulation (executable: EnerGe_sim) see
            #     https://gitlab.maisondelasimulation.fr/CENNS/EnerGe/blob/master/simulation/EnerGe_sim.cc#L62
            ARG_SINGULARITY_COMMAND=" -b -m ${MAC_FILE_ABS} -f ${OPTION_FILE_ABS} -o $( basename "${MAC_FILE}" )_${JOB_ID}.root"
            
            # FIXME temporary fix for v2.2.0
            export ENERGE_DATA=/usr/local/EnerGe/v2.2.0_small-inner-shielding-layer/data
            export SINGULARITYENV_ENERGE_DATA=$ENERGY_DATA
            
            SINGULARITY_COMMAND="singularity run --cleanenv --app ${APP_TO_RUN} --bind ${BIND_STORAGE} ${SIF_CONTAINER}" 
            SINGULARITY_COMMAND+=${ARG_SINGULARITY_COMMAND}
        elif [[ ${APP_TO_RUN} == "analyser" ]]; then
            # for analyser (executable: analyser) see
            #     https://gitlab.maisondelasimulation.fr/CENNS/EnerGe/blob/master/analyser/analyser.cc
            ARG_SINGULARITY_COMMAND=" -b"
            # add/adjust here the rest of the parameters
            ARG_SINGULARITY_COMMAND+=" -o C"
            ARG_SINGULARITY_COMMAND+=" -r /mnt/hephy/cresst/vasile-mihai.ghete/NUCLEUS/EnerGe/test_2020_03_21/2020-03-21_18-53_hSvC4nGA"
            ARG_SINGULARITY_COMMAND+=" Gammas_SphereGun.mac_2020-03-21_18-53_hSvC4nGA.root"
            
            export ANALYSIS_DATA_PATH=$(pwd)
            export SINGULARITYENV_ANALYSIS_DATA_PATH=$ANALYSIS_DATA_PATH
            
            export ANALYSIS_SETTINGS_FILE="my_analyser_settings.dat"
            export SINGULARITYENV_ANALYSIS_SETTINGS_FILE=$ANALYSIS_SETTINGS_FILE
            
            SINGULARITY_COMMAND="singularity run --cleanenv --app ${APP_TO_RUN} --bind ${BIND_STORAGE} ${SIF_CONTAINER}" 
            SINGULARITY_COMMAND+=${ARG_SINGULARITY_COMMAND}
        else
            printf "\n APP_TO_RUN \n ${APP_TO_RUN}"
            printf "\n not implemented. Exiting! \n"
            exit 1
        fi
            

        # 
        if [[ ${RUN_TYPE} == "SRUN" ]]; then

            printf "\n SLURM interactive jobs using srun and singularity: run command \n"
            echo "srun ${SLURM_OPTIONS} ${SINGULARITY_COMMAND}"
            printf "\n"
            srun ${SLURM_OPTIONS} ${SINGULARITY_COMMAND}

        elif [[ ${RUN_TYPE} == "SBATCH" ]]; then
            
            SBATCH_WRAP=${SBATCH_WRAP_OPTIONS}" srun "${SINGULARITY_COMMAND}
            
            printf "\n SLURM batch jobs: run command \n"
            echo " sbatch ${SLURM_OPTIONS} --wrap=\"${SBATCH_WRAP}\""
            printf "\n\n"
            sbatch ${SLURM_OPTIONS} --wrap="${SBATCH_WRAP}"

        elif [[ ${RUN_TYPE} == "INT" ]]; then
            printf "\n Interactive jobs from singularity container:\n"
            echo " /bin/bash --noprofile --norc -c \"${SINGULARITY_COMMAND}\""
        
            cd ${JOB_DIR}
            /bin/bash --noprofile --norc -c "${SINGULARITY_COMMAND}"        
        else
            # do nothing, should not arrive here as RUN_TYPE is tested before
            :
        fi
            
    else
        printf "\n No such environment $1 defined. Exiting!\n"
        exit 1
    fi

fi

printf "\n Exit script $0 \n"

# deactivate debuging
#set +vx

exit 0
