#!/bin/bash

# script to build, install and run ${APP_NAME}
APP_NAME="EnerGe"

# activate debuging
#set -vx

usage() {
    printf "\n Script to build and install ${APP_NAME}\n"
    printf " Usage:\n"
    printf "  download manageRelease.sh script in a directory\n"
    printf "  edit the USER CONFIGURATION blocks in manageRelease.sh script\n"
    printf "  execute the script\n"
    printf "    ./manageRelease.sh ENV ACTION APP_NAME\n"
    printf "\n ENV environment options\n"
    printf "   CVMFS CERN-VM FS\n"
    printf "   USER_SETUP \n"
    printf "   SINGULARITY \n"
    printf "\n ACTION options\n"
    printf "  C clone and check-out branch.\n"
    printf "      ACTION_ARG: branch to be checked out.\n"
    printf "  U update branch.\n"
    printf "      ACTION_ARG: ignored\n"
    printf "  B build release/container.\n"
    printf "      For ENV CVMFS or USER_SETUP: build release\n"
    printf "          ACTION_ARG: ignored\n"
    printf "      For ENV=SINGULARITY, build container\n"
    printf "          ACTION_ARG: layer\n"
    printf "  I install release.\n"
    printf "      For ENV CVMFS or USER_SETUP: install release\n"
    printf "          ACTION_ARG: ignored\n"
    printf "      For ENV=SINGULARITY, copy container to SingularityHub\n"
    printf "          ACTION_ARG: layer\n"
    }

if [[ $# -lt 3 ]]; then
    printf "\n $0 requires at least three arguments. See usage below.\n"
    usage
    exit 1
else
    if [[ $1 == "help" || $1 == "-help" || $1 == "--help" ]]; then
        usage
        exit 1
    fi
fi


###############################################################################
###############################################################################

# START: USER CONFIGURATION PART 1 - SW-ENVIRONMENT-INDEPENDENT PART <---------

# top directory WORKING_DIR
export WORKING_DIR='/afs/cern.ch/user/g/ghete/work/NUCLEUS'

# set the directory where the source code will be checked out
PACKAGE_DIR=${WORKING_DIR}/simulation/manageRelease/${APP_NAME}

# set the file containing the deploy token - warning: never put it on git repository, protect it properly on disk (user read only)
# set also the username for the token
DEPLOY_TOKEN_FILE=${WORKING_DIR}/simulation/manageRelease/NUCLEUS.EnerGe.token/singularity_token.txt
DEPLOY_TOKEN_USER="gitlab_deploy_ro"

# remember the current directory
CURRENT_DIR=`pwd`

# ${APP_NAME} version is common for all operations: "version" is a branch or a tag from GitLab repository
# GCC, ROOT and GEANT4 versions can depend on the environment, so they are set when defining the environment

PACKAGE_VERSION="v2.3"

# END: USER CONFIGURATION PART 1 - SW-ENVIRONMENT-INDEPENDENT PART <-----------

###############################################################################
###############################################################################


# write to console and log file (bash script!)
LOG_FILE="manageRelease_"$1"_"$2"_"$3"_"${PACKAGE_VERSION}"_"$(date -d "today" +"%Y-%m-%d_%H-%M").log

exec 3<&1
coproc mytee { tee ${LOG_FILE} >&3; }
exec >&${mytee[1]} 2>&1

printf "Log file: "
printf "$LOG_FILE \n" 



if [[ $1 == "CVMFS" || $1 == "USER_SETUP" || $1 == "SINGULARITY" ]]; then
    ENV=$1
    printf "\n Using environment $ENV \n"
else
    printf "\n No such environment $1 defined. Exiting!\n"
    usage
    exit 1
fi
  
ACTION=$2

if [[ "$ENV" == "CVMFS" || "$ENV" == "USER_SETUP" ]]; then
    if [[ "$ACTION" == "C" || "$ACTION" == "U" ]]; then
        BRANCH=$3
        printf "\n Checkout/update $BRANCH branch. \n"
    elif [[ "$ACTION" == "B" ]]; then
        printf "\n Building release \n"
    elif [[ "$ACTION" == "I" ]]; then
        printf "\n Installing release \n"
    else
        printf "\n No action $ACTION possible for $ENV environment. Exiting!\n"
        usage
        exit 1
    fi
        
elif [[ "$ENV" == "SINGULARITY" ]]; then
    
    printf " $0 script requires singularity 3 version"
    
    # check singularity version
    SINGULARITY_VERSION_REQUIRED="3.0.0"
    SINGULARITY_VERSION_CURRENT="$(singularity version)"
    
    if [[ $? -ne 0 ]]; then
        printf "\n 'singularity version' command returned error."
        printf "\n Check the installed singularity! \n Exiting!\n"
        exit 1
    fi        
    
    if [ "$(printf '%s\n' "$SINGULARITY_VERSION_REQUIRED" "$SINGULARITY_VERSION_CURRENT" | sort -V | head -n1)" = "$SINGULARITY_VERSION_REQUIRED" ]; then 
        printf "\n Current singularity version:  ${SINGULARITY_VERSION_CURRENT}"
        printf "\n .... ---> OK\n"
    else
        printf "\n Current singularity version $SINGULARITY_VERSION_CURRENT is smaller than required version $SINGULARITY_VERSION_REQUIRED"
        printf "\n Upgrade the installed singularity! \n Exiting!\n"
        exit 1
    fi
   
    
    if [[ "$ACTION" == "C" || "$ACTION" == "U" ]]; then
        BRANCH=$3
        printf "\n Checkout/update $BRANCH branch for $ENV environment. \n"
    elif [[ "$ACTION" == "B" || "$ACTION" == "I" ]]; then
        printf "\n IMPORTANT NOTE: needs sudo rights on the computer to build the singularity container."
        printf "\n --------------> be VERY CAREFUL what you write, delete, etc in the definition file"
        printf "\n --------------> to not compromise the installed system.\n"
        
        LAYER=$3
        
    else
        printf "\n No action $ACTION possible for $ENV environment. Exiting!\n"
        usage
        exit 1
    fi
    
else
    printf "\n No such environment $1 defined. Exiting!\n"
    exit 1
fi

# get the OS and set the computing architecture

if [ "$(cat /etc/redhat-release | grep '^Scientific Linux .*6.*')" ]; then
    OS_TAG="slc6"
elif [ "$(cat /etc/centos-release | grep 'CentOS Linux release 7')" ]; then
    OS_TAG="centos7"
else
    printf "Unknown OS" 2>&1 
fi

###############################################################################
###############################################################################

# START: USER CONFIGURATION PART 2 - ENVIRONMENT-DEPENDENT PART <--------------

# software environment

if [ "${ENV}" == "CVMFS" ]; then
    # software dependencies come from cvmfs - use preferably views
    # check in advance the release needed; the OS from architecture must match the computer OS
    
    LCG_RELEASE="LCG_86"
    
    # gcc compiler information
    GCC_VERSION="62"
    GCC_DEBUG="dbg"
    #GCC_DEBUG="opt"
        
    COMPUTING_ARCH="x86_64-"${OS_TAG}"-gcc"${GCC_VERSION}"-"${GCC_DEBUG}

    if [ -d /cvmfs/sft.cern.ch/lcg ]; then
        source /cvmfs/sft.cern.ch/lcg/views/setupViews.sh ${LCG_RELEASE} ${COMPUTING_ARCH}
        
        # uncomment next line to change the CMake version - must have the same architecture as the view
        #source /cvmfs/sft.cern.ch/lcg/releases/LCG_94/CMake/3.11.1/${COMPUTING_ARCH}/CMake-env.sh

        # uncomment next line to change the ROOT version - must have the same architecture as the view
        #source /cvmfs/sft.cern.ch/lcg/releases/LCG_89/ROOT/6.10.02/${COMPUTING_ARCH}/ROOT-env.sh
        
        # Qt support - included from QT release from the LGC view
        
    else
        printf "CVMFS requested, but not available. Exiting.\n"
        cd ${CURRENT_DIR}
        exit 1
        
    fi

    # G4VERSIONCODE set with XXYY corresponding to Geant4 version 
    export G4VERSIONCODE="41001"


elif [ "${ENV}" == "USER_SETUP" ]; then   
    # set up your software environment for non-cvmfs environment and non-singularity environment
 
    # example for HEPHY heplx setup
    source /afs/hephy.at/project/cresst/software/setEnv.sh

    # gcc compiler information - must match setEnv.sh settings
    GCC_VERSION="49"
    GCC_DEBUG="dbg"
    #GCC_DEBUG="opt"
        
    COMPUTING_ARCH="x86_64-"${OS_TAG}"-gcc"${GCC_VERSION}"-"${GCC_DEBUG}

    # ROOT 
    #source /afs/hephy.at/project/cresst/software/root-6.08.04/bin/thisroot.sh
    
    # Geant4 support
    export G4INSTALL=/afs/hephy.at/project/cresst/software/geant4-10.1.3
    source $G4INSTALL/bin/geant4.sh
    export G4VERSIONCODE="41001"

    # Qt support
    #export QT_PATH=/mypath/Qt/path-to-bin/
    #export PATH=${QT_PATH}:$PATH
    
elif [ "${ENV}" == "SINGULARITY" ]; then   
    printf "\n Software environment for SINGULARITY hard-coded in the singularity definition scripts\n" 

    # the following information must match the singularity definition settings
    
    GCC_VERSION="73"
    #GCC_DEBUG="dbg"
    GCC_DEBUG="opt"
    
    COMPUTING_ARCH="x86_64-"${OS_TAG}"-gcc"${GCC_VERSION}"-"${GCC_DEBUG}

    ROOT_VERSION="v6-08-06"
    GEANT4_VERSION="v10.1.3"
    G4VERSIONCODE="41001"
    
    CONTAINER_VERSION="v0.0.1"
    
    # read the actual value of the deploy token
    DEPLOY_TOKEN=${DEPLOY_TOKEN_USER}":"$(cat ${DEPLOY_TOKEN_FILE})

    # define a temporary path for the sif files - must be writable by root user  
    export SIF_PATH_TMP=/tmp/sif/${COMPUTING_ARCH}
    export PACKAGE_SIF_PATH_TMP=${SIF_PATH_TMP}/${PACKAGE_VERSION}
    mkdir -p ${PACKAGE_SIF_PATH_TMP}
    
    # define the final path for the sif files - the sif files will be copied here during the install step
    # enough space must be available
    export SIF_PATH_BASE=${WORKING_DIR}/SingularityHub/${APP_NAME}
    export SIF_PATH=${SIF_PATH_BASE}/OS_ROOT_GEANT/sif-${CONTAINER_VERSION}/${COMPUTING_ARCH}
    export PACKAGE_SIF_PATH=${SIF_PATH_BASE}/${PACKAGE_VERSION}/sif-${CONTAINER_VERSION}/${COMPUTING_ARCH}
    mkdir -p ${SIF_PATH}
    mkdir -p ${PACKAGE_SIF_PATH}
    
else
    printf "\n No such environment $ENV is defined - can not define the required settings.\n"
    usage
    cd ${CURRENT_DIR}
    exit 1
fi

# additional cmake options for building ${APP_NAME}
#    -DCMAKE_INSTALL_PREFIX is already included, see build fragment below
#    other possible options
#    -DWITH_GEANT4_UIVIS=ON (default: OFF)
#    -DWITH_GEANT4_QT=ON    (default: OFF)
#    -DDOC_DOXYGEN=ON       (default OFF)
#    -DBUILD_SIMULATION=OFF   (default ON)
#    -DBUILD_ANALYSER=OFF   (default ON)
# un-comment the desired options below
ADD_CMAKE_OPTIONS=" "
ADD_CMAKE_OPTIONS+=" -DWITH_GEANT4_UIVIS=ON"
ADD_CMAKE_OPTIONS+=" -DWITH_GEANT4_QT=ON"
ADD_CMAKE_OPTIONS+=" -DDOC_DOXYGEN=ON"
#ADD_CMAKE_OPTIONS+=" -DBUILD_SIMULATION=OFF"
#ADD_CMAKE_OPTIONS+=" -DBUILD_ANALYSER=OFF"

# END: USER CONFIGURATION PART 2 - ENVIRONMENT-DEPENDENT PART <----------------

###############################################################################
###############################################################################


# code below does not need any change by the user

# top directory for ${APP_NAME} source code, build and install directories

# source code will be written to ${PACKAGE_DIR}/${APP_NAME}
# build directory for non-singularity build is ${PACKAGE_DIR}/${APP_NAME}_build
# install directory for non-singularity build is ${PACKAGE_DIR}/${APP_NAME}_install

if [ -d ${PACKAGE_DIR} ]; then
    printf "\n Directory ${PACKAGE_DIR} already exists.\n"
    
    if [ ${ACTION} == "C" ]; then
        cd ${PACKAGE_DIR}
        if (shopt -s nullglob dotglob; f=(*); ((${#f[@]}))); then
            printf "\nFor cloning the repository (option C), the top directory must be empty.\n"
            printf "Remove the content of the directory \n  ${PACKAGE_DIR}\nthen re-run this script.\n\n"

            cd ${CURRENT_DIR}
            exit 1
        fi
    fi
else
    mkdir -p ${PACKAGE_DIR}
fi

# source code directory
export PACKAGE_SRC_PATH=${PACKAGE_DIR}/${APP_NAME}
mkdir -p ${PACKAGE_SRC_PATH}

# The functions below operate on PATH-like variables whose fields are separated
# with ':'.
# Note: The *name* of the PATH-style variable must be passed in as the 1st
#       argument and that variable's value is modified *directly*.

# SYNOPSIS: path_prepend varName path
# Note: Forces path into the first position, if already present.
#       Duplicates are removed too, unless they're directly adjacent.
# EXAMPLE: path_prepend PATH /usr/local/bin
#
# https://stackoverflow.com/questions/24515385/is-there-a-general-way-to-add-prepend-remove-paths-from-general-environment-vari


path_prepend() {
  local aux=":${!1}:"
  aux=${aux//:$2:/:}; aux=${aux#:}; aux=${aux%:}
  printf -v "$1" '%s' "${2}${aux:+:}${aux}"  
}

# SYNOPSIS: path_append varName path
# Note: Forces path into the last position, if already present.
#       Duplicates are removed too, unless they're directly adjacent.
# EXAMPLE: path_append PATH /usr/local/bin
path_append() {
  local aux=":${!1}:"
  aux=${aux//:$2:/:}; aux=${aux#:}; aux=${aux%:}
  printf -v "$1" '%s' "${aux}${aux:+:}${2}"
}

# SYNOPSIS: path_remove varName path
# Note: Duplicates are removed too, unless they're directly adjacent.
# EXAMPLE: path_remove PATH /usr/local/bin
path_remove() {
  local aux=":${!1}:"
  aux=${aux//:$2:/:}; aux=${aux#:}; aux=${aux%:}
  printf -v "$1" '%s' "$aux"
}


if [[ "$ENV" != "SINGULARITY" ]]; then
    
    # build directory and install directory
    export PACKAGE_BUILD_PATH=${PACKAGE_DIR}/${APP_NAME}_build/${COMPUTING_ARCH}
    mkdir -p ${PACKAGE_BUILD_PATH}

    export PACKAGE_INSTALL_PATH=${PACKAGE_DIR}/${APP_NAME}_install/${COMPUTING_ARCH}/${PACKAGE_VERSION}
    mkdir -p ${PACKAGE_INSTALL_PATH}

    # append ${APP_NAME} library to ENERGE_DYLD_LIBRARY_PATH (mac OS only)
    path_append ENERGE_DYLD_LIBRARY_PATH ${PACKAGE_INSTALL_PATH}/lib
    
    # append ${APP_NAME} library to LD_LIBRARY_PATH
    path_append ENERGE_LD_LIBRARY_PATH ${PACKAGE_INSTALL_PATH}/lib
    
    # add ${APP_NAME} binary to PATH
    path_prepend PATH ${PACKAGE_INSTALL_PATH}/bin
    path_prepend PATH ${ROOTSYS}/bin
    
    export ROOT_INCLUDE_DIR=${PACKAGE_INSTALL_PATH}/include
    
    # add ENERGE specific variables (variable names are hard-coded in the source code)
    
    export ENERGE_PATH=${PACKAGE_PATH}
    export ENERGE_INSTALL_PATH=${PACKAGE_INSTALL_PATH}
    export ENERGE_DATA=${ENERGE_PATH}/simulation/data
    # G4VERSIONCODE already defined when setting G4INSTALL (XXYY corresponds to Geant4 version) 
    # export G4VERSIONCODE=4XXYY 
    
    # remove CMS paths (specific for V.M. Ghete using a CMS account)
    path_remove PATH /afs/cern.ch/cms/caf/scripts
    path_remove PATH /cvmfs/cms.cern.ch/common
    path_remove PATH /cvmfs/cms.cern.ch/bin
    
    # print the software environment
    printf "\nSoftware environment:\n\n"
    
    printf "   gcc version, from gcc -v \n\n"
    gcc -v
    printf "\n\n"
    
    printf "  ROOT version: ${ROOTSYS} \n\n"
    
    printf "  GEANT4 configuration, from \"geant4-config --cflags\" \n"
    geant4-config --cflags
    printf "\n\n"
    
    printf "  QT version, from \"qmake --version\" \n"
    qmake --version
    printf "\n\n"
    
    printf "  cmake version, from \"cmake --version\" \n"
    cmake --version
    printf "\n\n"
    
fi


if [ ${ACTION} == "C" ]; then
    
    printf "\n Clone release, check out branch ${BRANCH} in empty directory \n ${PACKAGE_SRC_PATH}\n\n"
    
    git clone git@gitlab.maisondelasimulation.fr:CENNS/${APP_NAME}.git -b ${BRANCH} ${PACKAGE_SRC_PATH}
    
    printf "\n Edit all the necessary files in \n ${PACKAGE_SRC_PATH}\n"
    printf "\n When ready, run this script with B option.\n"
    
    cd ${CURRENT_DIR}

elif [ ${ACTION} == "U" ]; then
    cd ${PACKAGE_SRC_PATH}
    
    # quick and dirty check if the git repository was cloned
    if [ -d .git ]; then
        BRANCH=`git branch | grep \* | cut -d ' ' -f2`
        printf "\n Update branch \"${BRANCH}\" \n"
    else
        printf "No .git repository in directory \n ${PACKAGE_SRC_PATH}\n"
        printf "Run first this script with the \"C\" option\n"

        cd ${CURRENT_DIR}
        exit 1
    fi
    
    git pull
    
    printf "\n Edit all the necessary files in \n ${PACKAGE_SRC_PATH}\n"
    printf "\n When ready, run this script with B option.\n"
    
    cd ${CURRENT_DIR}

elif [ ${ACTION} == "B" ]; then
        
    if [ ${ENV} == "SINGULARITY" ]; then
        
        if [ "$OS_TAG" == "centos7" ]; then
            cd ${PACKAGE_SRC_PATH}/scripts/singularity/centos7/
            echo
            echo "Singularity scripts directory: "
            echo `pwd`
        else
            printf "\n Building singularity container for ${OS_TAG} operating system not implemented.\n"
            cd ${CURRENT_DIR}
            exit 1
        fi

        DEF_FILE="singularity_${LAYER}.def"
        
        # set the path to sif file to the actual path  
        sed -i "s,REPLACE_SIF_PATH,${SIF_PATH}," ${DEF_FILE}
        
        # set the ${APP_NAME} - global replace!
        sed -i "s,REPLACE_APP_NAME,${APP_NAME},g" ${DEF_FILE}

        # set the ${CONTAINER_VERSION}
        sed -i "s,REPLACE_CONTAINER_VERSION,${CONTAINER_VERSION}," ${DEF_FILE}
        
        # set the ROOT, GEANT4 and ${APP_NAME} versions, and the additional cmake options
        # if a match is not found in the file, no substitution is made
        sed -i "s,REPLACE_ROOT_VERSION,${ROOT_VERSION}," ${DEF_FILE}
        sed -i "s,REPLACE_GEANT4_VERSION,${GEANT4_VERSION}," ${DEF_FILE}
        sed -i "s,REPLACE_G4VERSIONCODE,${G4VERSIONCODE}," ${DEF_FILE}
        sed -i "s,REPLACE_PACKAGE_VERSION,${PACKAGE_VERSION}," ${DEF_FILE}
        sed -i "s,REPLACE_ADD_CMAKE_OPTIONS,${ADD_CMAKE_OPTIONS}," ${DEF_FILE}
        
        # set the deploy token key
        sed -i "s,REPLACE_DEPLOY_TOKEN,${DEPLOY_TOKEN}," ${DEF_FILE}

        # save a copy of def file for inspection, after replacing the variables 
        cp -p ${DEF_FILE} ${DEF_FILE}".modified"
        
        if [ ${LAYER} == "${APP_NAME}" ]; then
            printf "\n The singularity sif files will be created in directory \n  ${PACKAGE_SIF_PATH_TMP} \n"
            sudo singularity build --force ${PACKAGE_SIF_PATH_TMP}/${LAYER}.sif ${DEF_FILE}
            
        else
            printf "\n The singularity sif files will be created in directory \n  ${SIF_PATH_TMP} \n"
            sudo singularity build --force ${SIF_PATH_TMP}/${LAYER}.sif ${DEF_FILE}
        fi
            
        # set back REPLACE_SIF_PATH
        sed -i "s,${SIF_PATH},REPLACE_SIF_PATH," ${DEF_FILE}

        # set back ${APP_NAME} - global replace, setting it back is quite dangerous if name is common
        # in that case, use /pattern/! s/pattern/, skipping the accidental replacements
        sed -i "s,${APP_NAME},REPLACE_APP_NAME,g" ${DEF_FILE}

        # set back REPLACE_CONTAINER_VERSION
        sed -i "s,${CONTAINER_VERSION},REPLACE_CONTAINER_VERSION," ${DEF_FILE}

        # set back REPLACE_ROOT_VERSION, REPLACE_GEANT4_VERSION, REPLACE_PACKAGE_VERSION, REPLACE_ADD_CMAKE_OPTIONS
        sed -i "s,${ROOT_VERSION},REPLACE_ROOT_VERSION," ${DEF_FILE}
        sed -i "s,${GEANT4_VERSION},REPLACE_GEANT4_VERSION," ${DEF_FILE}
        sed -i "s,${G4VERSIONCODE},REPLACE_G4VERSIONCODE," ${DEF_FILE}
        sed -i "s,${PACKAGE_VERSION},REPLACE_PACKAGE_VERSION," ${DEF_FILE}
        if [[ ${ADD_CMAKE_OPTIONS} != " " ]]; then
            sed -i "s,${ADD_CMAKE_OPTIONS},REPLACE_ADD_CMAKE_OPTIONS," ${DEF_FILE}
        fi

        # set back REPLACE_DEPLOY_TOKEN
        sed -i "s,${DEPLOY_TOKEN},REPLACE_DEPLOY_TOKEN," ${DEF_FILE}
        
        # compare the original def file with the "restored" (replace forth and back file)
        cp -p ${DEF_FILE} ${DEF_FILE}".restored"

        cd ${CURRENT_DIR}

    else
        
        printf "\nStart building ${APP_NAME} for $ENV environment...\n\n"   
        printenv
        printf "\n"   
        
        cd ${PACKAGE_BUILD_PATH}
        
        if (shopt -s nullglob dotglob; f=(*); ((${#f[@]}))); then
            # if the directory is not empty, a build exists - make clean before rebuilding
            printf "\nRunning cmake clean in \n ${PACKAGE_BUILD_PATH} \n"
            make clean
        fi
        
        printf "\nRunning cmake...\n" 
        printf "  cmake ${PACKAGE_SRC_PATH} --debug-output -DCMAKE_INSTALL_PREFIX=${PACKAGE_INSTALL_PATH} ${ADD_CMAKE_OPTIONS} \n\n" 
        cmake ${PACKAGE_SRC_PATH} --debug-output -DCMAKE_INSTALL_PREFIX=${PACKAGE_INSTALL_PATH} ${ADD_CMAKE_OPTIONS} 
    
        printf "\nRunning make -j8...\n\n" 
        make -j8 
        
        #printf "\nRunning make ...\n\n" 
        #make
        
        cd ${CURRENT_DIR}
    fi
    
elif [ ${ACTION} == "I" ]; then
    if [ ${ENV} == "SINGULARITY" ]; then
        if [ ${LAYER} == "${APP_NAME}" ]; then
            printf "\n The singularity sif file ${LAYER}.sif will be copied from directory \n  ${PACKAGE_SIF_PATH_TMP}"
            printf "\n to directory \n  ${PACKAGE_SIF_PATH} \n"
            cp --preserve=timestamps ${PACKAGE_SIF_PATH_TMP}/${LAYER}.sif ${PACKAGE_SIF_PATH}/
        else
            printf "\n The singularity sif file ${LAYER}.sif will be copied from directory \n  ${SIF_PATH_TMP}"
            printf "\n to directory \n  ${SIF_PATH} \n"
            cp --preserve=timestamps ${SIF_PATH_TMP}/${LAYER}.sif ${SIF_PATH}/
        fi
    else
        printf "\nInstall ${APP_NAME} for $ENV environment in directory \n  ${PACKAGE_INSTALL_PATH}\n" 
        cd ${PACKAGE_BUILD_PATH}
        make install

        cd ${CURRENT_DIR}
    fi

else 
    printf "\nOption ${ACTION} not implemented.\n"
    
    usage
    cd ${CURRENT_DIR}
    exit 1
fi

cd ${CURRENT_DIR}

# deactivate debuging
#set +vx

exit 0
    
