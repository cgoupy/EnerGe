# Simulation code for Germanium detector (NUCLEUS COV, HPGe Detector)
This code is a simplified version of the Choozerent code. It will be used to simulate different germanium detectors. (COV prtotoype, NUCLEUS COV, HPGe detector...)

## BUILD ENVIRONMENT
The EnerGe suite relies on the multi-platform CMake software to generate native build files.

## OS
Should build on all Unix and the most recent Mac OS X systems.

## DEPENDENCIES
### For all platforms
* C/C++ compiler and linker with C++11 support, such as the GNU compilation tools or LLVM tools provided with Xcode on MacOS X.
* CMake version 2.8 at least (tested with 3.7.2, 3.9, 3.10.2, 3.16.3, 3.20.5).
* ROOT: version 6.0 at least (tested with 6.08/04, 6.10/08, 6.10/05, 6.12/04, 6.18/00, 6.20/08), preferably installed using CMake (see ROOT documentation).
* GEANT4: recommended version 4.10.7. Build has been tested against all versions from 4.10.4 up to 4.10.7. Must have been built with Qt or OPEN GL or RayTracer options enabled, etc. for visualisation (see Geant 4 documentation).
* Visualisation libraries for GEANT4: 
     * X11 (Linux) or XQuartz (mac OS X) for OPEN GL and RayTracer
     * Qt 4/5 librairies for visualisation: can be found at https://download.qt.io/archive/qt/

### Linux (e.g. Ubuntu packages) 
* cmake
* build-essential: C++ compiler/linker tools
* libgl1-mesa-dev: OpenGL libraries
* libxmu-dev: X11 support
* libqt4-dev: Qt4 libraries, with which Geant4 must be built (if you need visualisation).

### Supported and tested Mac OS X platforms (Intel and M1)
Sierra 10.12.4 with Xcode 8 and embedded LLVM tools
* cmake 3.7.2
* Qt 4.8.6: need to build it from source and apply patch described at: https://codereview.qt-project.org/#/c/157137/

High Sierra 10.13.3 with Xcode 9 and embedded LLVM tools:
* cmake 3.10.2
* Qt.5.4: installed through .dmg installer.

Mojave 10.14.6 with Xcode 11 and embedded LLVM tools:
* cmake 3.16.3
* Qt.5.6: installed through .dmg installer.

Catalina 10.15.7 with Xcode 12 and embedded LLVM tools:
* cmake 3.20.5
* Qt.5.9: installed through .dmg installer.

BigSur 11.6.7 with Xcode 13.2.1 and embedded LLVM tools:
* cmake 3.22.1
* Qt.5.9: installed through homedrew on M1 chip.

## ENVIRONMENT
* $G4VERSIONCODE must be defined (format 41001 for GEANT4.10.1, patched or not)
* ROOT should be installed with CMake, and bin/thisroot.(c)sh must be sourced (ROOTSYS will be set).
* GEANT4.10.1 (necessarily installed with CMake): /path/to/geant4.10.1-install/bin/geant4.(c)sh must be sourced.
* $ENERGE_PATH must point to /mypath/EnerGe/.
* $ENERGE_INSTALL_PATH must point to /mypath/EnerGe/<install-directory>.
* $ENERGE_DATA must point to /mypath/EnerGe/simulation/data.

Below is an example of a proper (bash) shell environment setting to build, install and run EnerGe
```
#======== Qt support =====#
export PATH=mypath/Qt/path-to-bin/:$PATH

#======== ROOT support =====#
source mypath/root-builddir/bin/thisroot.sh

#======== G4 support =====#
export G4INSTALL=/mypath/geant4.10.01.p03-install
source $G4INSTALL/bin/geant4.sh
export G4VERSIONCODE=41001

#======== EnerGe support =====#
export ENERGE_PATH=/mypath/EnerGe
export ENERGE_INSTALL_PATH=$ENERGE_PATH/EnerGe-install
export ENERGE_DATA=$ENERGE_PATH/simulation/data
export ENERGE_DYLD_LIBRARY_PATH=$ENERGE_DYLD_LIBRARY_PATH:$ENERGE_INSTALL_PATH/lib/
export ENERGE_LD_LIBRARY_PATH=$ENERGE_LD_LIBRARY_PATH:$ENERGE_INSTALL_PATH/lib/
export PATH=$ENERGE_INSTALL_PATH/bin/:$PATH
```
### ADDITIONAL ENVIRONMENT FOR ANALYSER

```
# set the path of analyser settings dat file (as  example: the current directory)
export ANALYSIS_DATA_PATH=$(pwd)
  
# set the name of analyser settings dat file            
export ANALYSIS_SETTINGS_FILE="my_analyser_settings.dat"

# export the variables when using singularity
export SINGULARITYENV_ANALYSIS_DATA_PATH=$ANALYSIS_DATA_PATH
export SINGULARITYENV_ANALYSIS_SETTINGS_FILE=$ANALYSIS_SETTINGS_FILE

```

If the variables are not set, and ENERGE_PATH analyser_settings.dat are used.


## INSTALLATION
1. Clone EnerGe (using SSH, you need an access to the git repository of Maison de la Simulation):
```
    git clone git@gitlab.cern.ch:7999/cgoupy/EnerGe.git
```
2. Make a build directory wherever you would like, and move to it:
```
    mkdir build
    cd build
```
3. Configure EnerGe, i.e. run cmake onto the EnerGe source to generate the build files into the build directory:
```
    cmake /mypath/EnerGe
```
4. Build (with N cores):
```
    make -jN
```
5. (optional) Install, i.e. copy to the installation path the executables, shared libraries, and configuration files needed at run-time (make sure you defined the path to the install directory using the option -DCMAKE_PREFIX_INSTALL):
```
    make install
```

### Advanced build configuration:
You may find it useful to run `ccmake` to have a detailed overview of all the options.
```
    ccmake /mypath/EnerGe
```
By default, the build is optimized for speed. You may wish to build in debug mode with
```
    cmake /mypath/EnerGe -DCMAKE_BUILD_TYPE=Debug
```
or change your install path with
```
    cmake /mypath/EnerGe -DCMAKE_INSTALL_PREFIX=/mypath/EnerGe-install
```
or  remove the analyser from the build process with
```
    cmake -DBUILD_ANALYSER=OFF
```
or generate IDE-dedendent project files, e.g. with Xcode (refer to the CMake documentation) with
```
    cmake /mypath/EnerGe -G Xcode
```

## Cleaning and uninstall
To clean the build folder:
```
    make clean
```
To uninstall the suite, simply remove the install folder:
```
    rm -rf /mypath/EnerGe-install
```

### manageRelease.sh script
A bash script to build, install and run EnerGe can be found in the scripts/shell directory. The script set up the environment and the variables needed.

