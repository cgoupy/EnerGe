/*
 ========================================================
 Adapted from Nucifer analysis and simulation software
 March 2017: Matthieu Vivier
 ========================================================
 */
#ifndef DATA_DEFINITION_H
#define DATA_DEFINITION_H

/* Maximilien Fechner, oct 2009 */
/* Jonathan Gaffiot, 2010-2012 */
/* Matthieu Vivier, 2017-...    */

// this file contains the definition of ROOT objects


#include <Index.h>
#include <DataDefinitionException.hh>
using namespace EnerGe;

//#include <RtypesCore.h>
#include <TObject.h>
#include <TNamed.h>
#include <TTimeStamp.h>
#include <TClonesArray.h>
#include <TVector3.h>

#include <iosfwd>
#include <algorithm>
#include <vector>
#include <map>

class TString;

////////////////////////////////////////////////////////////////
//////////////////////////// MCInfo ////////////////////////////
////////////////////////////////////////////////////////////////

class Particle : public TObject {
public:
    constexpr static UInt_t NbVertex=3;
    enum Vertex {
        Injection,
        FirstInteraction,
        LastInteraction,
    };
    
private:
    Int_t    PDG;
    Int_t    FinalProcess;
    Volumes  VolInj;
    Float_t  Einj;
    
    //    Float_t  Einj;
    //    Float_t  EdepVol[NbVolumes];
    //    Float_t  EdepQuenched;
    TVector3 Positions[NbVertex]; // injection, first vertex and last vertex
    TVector3 Direction[NbVertex]; // injection
    
    //  std::map<Int_t,Float_t> EincCrystalMap; // map to record Kinetic energy of every neutron reaching cystal
    //  std::map<TString,std::map<Int_t,Float_t>> EincVolumeMap; // map to record Kinetic energy of every neutron reaching a volume
    //  std::map<TString,std::map<Int_t, std::vector<Float_t>> > EdepFirstElasticMap;
    
public:
    
    Particle():TObject() { Zero(); }
    Particle ( const TObject& object ) = delete;
    Particle ( const Particle& part ) = delete;
    
    inline Int_t   GetPDG()          const { return PDG; }
    inline Int_t   GetFinalProcess() const { return FinalProcess; }
    inline Volumes GetVolInj()       const { return VolInj; }
    
    //  inline std::map<Int_t,Float_t> GetEincCrystalMap()    const { return EincCrystalMap; }
    //  inline std::map<TString,std::map<Int_t,Float_t>> GetEincVolumeMap()    const { return EincVolumeMap; }
    //  inline std::map<TString, std::map<Int_t, std::vector<Float_t>> > GetEdepFistElasticMap() const { return EdepFirstElasticMap; }
    
    inline Float_t GetEinj()         const { return Einj; }
    //    inline Float_t GetEdep()         const { return EdepVol[VOL_CRYSTAL]; }
    //    inline Float_t GetEdepQuenched() const { return EdepQuenched; }
    //    inline Float_t GetEdepVol(Volumes vol) const { if (vol>=(Int_t)NbVolumes) { return -666; } return EdepVol[vol]; }
    //    inline Float_t GetEdepVol(Int_t i)     const { if (i>=(Int_t)NbVolumes)   { return -666; } return EdepVol[i]; }
    
    inline const TVector3& GetPosition(UInt_t i) const {
        if (i<NbVertex) { return Positions[i]; }
        throw(datadef_except("Particle():GetPosition: Unknown vertex"));
    }
    inline const TVector3& GetMomentumDirection(UInt_t i) const {
        if (i<NbVertex) { return Direction[i]; }
        throw(datadef_except("Particle():GetMomentumDirection: Unknown vertex"));
    }
    
    inline Float_t  GetCoordinate(UInt_t i, UInt_t j) const {
        if (i<NbVertex) {  return Positions[i][j]; }
        throw(datadef_except("Particle():GetPosition: Unknown vertex"));
    }
    
    inline void SetPDG(Int_t pdg)                  { PDG = pdg; }
    inline void SetFinalProcess(Int_t proc_code)   { FinalProcess = proc_code; }
    inline void SetVolInj(Volumes vol)             { VolInj = vol; }
    
    inline void SetEinj(Float_t e)                 { Einj = e; }
    
    //void SetEincCrystal(Int_t i,Float_t e);
    //void SetEincVolume(TString VolName, Int_t i, Float_t e);
    
    //    inline void SetEdepQuenched(Float_t e)         { EdepQuenched = e; }
    //    inline void AddEdepVol(Volumes vol, Float_t e) { EdepVol[vol] += e; }
    
    inline void SetPosition(UInt_t i, Float_t x, Float_t y, Float_t z) { if (i<NbVertex) { Positions[i].SetXYZ(x,y,z); } }
    inline void SetPosition(UInt_t i, const TVector3& v) { if (i<NbVertex) { Positions[i].SetXYZ(v.x(),v.y(),v.z()); } }
    inline void SetMomentumDirection(UInt_t i, Float_t x, Float_t y, Float_t z) { if (i<NbVertex) { Direction[i].SetXYZ(x,y,z); } }
    inline void SetMomentumDirection(UInt_t i, const TVector3& v) { if (i<NbVertex) { Direction[i].SetXYZ(v.x(),v.y(),v.z()); } }
    
    inline virtual void Clear(const Option_t* = "") { Zero(); }
    void Zero() ;
    
    ClassDef(Particle,2)
};

// Monte Carlo event: an event is a collection of primary and secondary particles
class MCEvent : public TObject {
    
private:
    
    Float_t  EdepVol[NbVolumes];
    Float_t  EdepQuenched;
    
    Particle *PrimaryParticle;
    TClonesArray *SecondaryParticles;
    
public:
    
    //constructors
    MCEvent():TObject(){Init();}
    MCEvent ( const TObject& object ) = delete;
    MCEvent ( const MCEvent& evt ) = delete;
    
    //destructor
    virtual ~MCEvent() { delete PrimaryParticle; delete SecondaryParticles; }
    
    //Getters
    //inline Float_t GetEinj()         const { return Einj; }
    inline Float_t GetEdepQuenched() const { return EdepQuenched; }
    inline Float_t GetEdepVol(Volumes vol) const {return EdepVol[vol]; }
    inline Float_t GetEdepVol(Int_t i)     const { return EdepVol[i]; }
    inline Particle* GetPrimaryParticle() const {return PrimaryParticle; }
    inline TClonesArray* GetSecondaryParticleTCA() const {return SecondaryParticles; }
    inline Particle* GetSecondaryParticle(Int_t i)  const {
        return ( i<SecondaryParticles->GetEntriesFast() ? static_cast<Particle*>(SecondaryParticles->At(i)) : nullptr );
    }
    
    //Setters
    //inline void SetEinj(Float_t e)                 { Einj = e; }
    inline void SetEdepQuenched(Float_t e)         { EdepQuenched = e; }
    inline void AddEdepVol(Volumes vol, Float_t e) { EdepVol[vol] += e; }
    inline Particle* ConstructOneParticleAt(Int_t i) { return static_cast<Particle*>(SecondaryParticles->ConstructedAt(i)); }
    
    void Zero();
    inline void Init() { Zero(); PrimaryParticle = new Particle(); SecondaryParticles = new TClonesArray("Particle",3);}
    inline virtual void Clear(const Option_t* = "") { SecondaryParticles->Clear("C"); Zero(); }
    
    ClassDef(MCEvent,2)
};


////////////////////////////////////////////////////////////////
class MCInfo : public TNamed {
private:
    Int_t PrimaryType;
    Double_t Time;
    TClonesArray *MCEvents;
    
public:
    MCInfo(): TNamed() { Init(); } //an example of constructor chaining... (new to C++ 11)
    MCInfo(const char* name, const char* title): TNamed(name, title) { Init(); }
    MCInfo(const TString& name, const TString& title): TNamed(name, title) { Init(); }
    MCInfo ( const TNamed& ) = delete;
    MCInfo ( const MCInfo& ) = delete;
    
    virtual ~MCInfo() { delete MCEvents; }
    
    inline Int_t     GetPrimaryType()      const { return PrimaryType; }
    inline Double_t  GetTime()             const { return Time; }
    inline Int_t     GetNbEvents()       const { return MCEvents->GetEntriesFast(); }
    inline TClonesArray* GetMCEventsTCA() const { return MCEvents; }
    inline MCEvent* GetMCEvent(Int_t i)  const {
        return ( i<MCEvents->GetEntriesFast() ? static_cast<MCEvent*>(MCEvents->At(i)) : nullptr );
    }
    
    //Are these functions really useful? Because they sum over all MCEvent-wise energy depositions
    Float_t GetEinj() const;
    Float_t GetEdepQuenched() const;
    Float_t GetEdepVol(Volumes vol) const;
    Float_t GetEdepVol(Int_t i) const;
    
    //  std::map<Int_t,Float_t> GetEincCrystalMap() const; // return the EincCrystalMap for the first particle injected
    //  std::map<TString,std::map<Int_t,Float_t>> GetEincVolumeMap() const; // return the EincVolumeMap for the first particle injected
    //  std::map<TString, std::map<Int_t, std::vector<Float_t>> > GetEdepFirstElasticMap() const;
    
    inline Volumes GetVolInj() const { return static_cast<MCEvent*>(MCEvents->At(0))->GetPrimaryParticle()->GetVolInj(); }
    
    inline void SetPrimaryType(Int_t type) { PrimaryType = type; }
    inline void SetTime(Double_t t)        { Time = t; }
    inline MCEvent* ConstructMCEventAt(Int_t i) { return static_cast<MCEvent*>(MCEvents->ConstructedAt(i)); }
    
    inline void Init() { Zero(); MCEvents = new TClonesArray("MCEvent",3); }
    inline void Zero() { PrimaryType = -1; Time = 0.; }
    inline virtual void Clear(const Option_t* = "") { MCEvents->Clear("C"); Zero(); }
    
    ClassDef(MCInfo,2)
};

////////////////////////////////////////////////////////////////
////////////////////////// TruthTQInfo /////////////////////////
////////////////////////////////////////////////////////////////
// Simulated photon hits
//struct PE {
//public:
//  Int_t Count;  // weight of photons, often 1
//  Double_t Time; // time of hit
//  PE(): Count(0), Time(0.f) {}
//  PE(Int_t c, Float_t t): Count(c), Time(t) {}
//};

////////////////////////////////////////////////////////////////
// MC info about individual photon hits
//class TruthTQInfo : public TNamed {
//private:
//  UInt_t n_pes;
//  PE* PEs; //[n_pes]
//  UInt_t n_pulse;
//  Float_t* Pulse; //[n_pulse]
//
//public:
//  TruthTQInfo():TNamed() { Zero(); }
//  TruthTQInfo(const char* name, const char* title):TNamed(name, title) { Zero(); }
//  TruthTQInfo(const TString& name, const TString& title):TNamed(name, title) { Zero(); }
//  TruthTQInfo(const TNamed& ) = delete;
//  TruthTQInfo(const TruthTQInfo& ) = delete;
//
//  virtual ~TruthTQInfo() {
//    if (n_pes)   { delete[] PEs; }
//    if (n_pulse) { delete[] Pulse; }
//  };
//
//  Int_t  GetNbPEs() const ;
//  inline UInt_t   GetPEsSize()          const { return n_pes; }
//  inline Int_t    GetPECount(UInt_t i)  const { if (i<n_pes) { return PEs[i].Count; } else { return kMinInt; } }
//  inline Double_t GetPETime(UInt_t i)   const { if (i<n_pes) { return PEs[i].Time; }  else { return -1; } }
//  inline UInt_t   GetPulseSize()        const { return n_pulse; }
//  inline Float_t* GetPulse()            const { return Pulse; }
//  inline Float_t  GetPulseBin(UInt_t i) const { if (i<n_pulse) { return Pulse[i]; } else { return -1; } }
//  Float_t GetPulseCharge(UInt_t start=1u, UInt_t stop=0u) const;
//
//  void SetPEs(const std::vector<PE> &v, Bool_t reset=true);
//  void SetPulse(const std::vector<Float_t> &v, Bool_t reset=true);
//
//  void Zero() ;
//  virtual void Clear(const Option_t* = "") {
//    if (n_pes)   { delete[] PEs; }
//    if (n_pulse) { delete[] Pulse; }
//    Zero();
//  }
//
//  ClassDef(TruthTQInfo,10)
//};
#endif
