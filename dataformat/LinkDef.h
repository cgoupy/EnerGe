#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;

#pragma link C++ class  Particle+;
#pragma link C++ class  MCEvent+;
#pragma link C++ class  MCInfo+;
//#pragma link C++ struct PE+;
//#pragma link C++ class  TruthTQInfo+;

#endif
