#ifndef DATA_DEFINITION_EXCEPTION_HH
#define DATA_DEFINITION_EXCEPTION_HH 1

#include <exception>
#include <string>
#include <fstream>
#include <sstream>
#include <type_traits>

/* This header should use String.hh (for scat) and Exception.hh (for except),
 * but cross-including with CMake is not possible.
 * The tree should be refactored but...
 * JG 10 March 2016
 */

namespace detail_datadefinition_exception {
  template<typename T>
  inline void PushToStream(std::ostringstream& oss, T&& val)
  {
    oss << std::forward<T>(val);
  }

  template<typename T, typename... Args>
  inline void PushToStream(std::ostringstream& oss, T&& val, Args&&... args)
  {
    oss << std::forward<T>(val);
    detail_datadefinition_exception::PushToStream(oss,std::forward<Args>(args)...);
  }
  template <typename... Args>
  inline std::string scat(Args&&... args)
  {
    std::ostringstream oss;
    detail_datadefinition_exception::PushToStream(oss,std::forward<Args>(args)...);
    //return std::move(oss.str());
      return oss.str();
  }
} // detail_datadefinition_exception::

class datadef_except : public std::exception {
public:
  virtual ~datadef_except() noexcept {}

  explicit datadef_except(const std::string& arg): std::exception(), message(arg) {}
  explicit datadef_except(const std::string& arg, std::ofstream& write): std::exception(), message(arg) {
    write << message << std::endl;
  }

  inline virtual const std::string& swhat() const noexcept { return message; }
  inline virtual const char* what() const noexcept { return message.c_str(); }

  template <typename T, typename ... Args>
  datadef_except(T position, Args ... args):std::exception() {
    message = detail_datadefinition_exception::scat("***Error ",position,": ",args ...);
  }
//   template <typename T, typename U, typename V, typename ... Args>
//   datadef_except(T classname, U method, V num, Args ... args): std::exception() {
//     static_assert(std::is_arithmetic<V>::value," 3rd argument of datadef_except must be numeric!");
//     message = detail_datadefinition_exception::scat("***Error ",classname,"::",method,num,": ",args ...);
//   }
  template <typename T, typename U, typename V, typename ... Args, typename = typename std::enable_if<std::is_arithmetic<V>::value>::type>
  datadef_except(T classname, U method, V num, Args ... args): std::exception() {
    message = detail_datadefinition_exception::scat("***Error ",classname,"::",method,num,": ",args ...);
  }

private:
  std::string message;
};

#endif // DATA_DEFINITION_EXCEPTION_HH
