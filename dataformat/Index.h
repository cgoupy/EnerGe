/*
 ========================================================
 Adapted from Nucifer analysis and simulation software
 Removed some unnecessary constant and enumerations
 March 2017: Matthieu Vivier
 ========================================================
 */
#ifndef Index_h
#define Index_h

#include <RtypesCore.h>

namespace EnerGe {

//adapted from ROOT
#define MY_BIT(n) ((UInt_t)1<<(n))

// no need for now, but keep it out commented, just in case...
/*constexpr UInt_t NbEventStates = 17u;
 enum EventStates : Int_t {
 eNONE=0,
 eDATA_FAIL=-1, // real data, failed (bad scaler, problem in decoding...)
 eDATA_OK=1,    // real data, OK
 eSIMU_FAIL=-2, // simulation Geant4, failed (threshold not crossed...)
 eSIMU_OK=2,    // simulation Geant4, OK
 eRAND=11,      // all others for fake data, built from scratch to test our analysis
 eLED=12,
 eMUON=13,
 eTANK=14,
 ePR=28,
 eDEL=29,
 eNU_PR=38,
 eNU_DEL=39,
 eBKG=100,
 eACC=101,
 eCORR_PR=108,
 eCORR_DEL=109
 };*/

//Energy deposition volumes
// You need to ensure a correspondence with all the volumes declared in DCEventAction.cc
constexpr UInt_t NbVolumes = 10u; //total number of volumes
constexpr UInt_t NbOrsayOVCrystals=3u; //max number of crystals in the OV prototype at Orsay

enum Volumes : Int_t {
    //common to different geometries
    VOL_WORLD=0,
    VOL_INACTIVE=1,
    
    // general
    VOL_END=2,
    
    // Specific to attenuation lab & shielding lab
    VOL_SLAB=3,
    VOL_LAB=4,
    
    //COV Prototype
    VOL_TOP_GE =5,
    VOL_MID_LWO=6,
    VOL_BOT_GE=7,
    VOL_CRYO=8,

    VOL_HPGE=9
};

// generator codes
constexpr Int_t NbGeneCode = 9;
enum class OldGene : Int_t {
    fill_HEPevt = 2, // NuMC antineutrino: position is uniform in liquid, vertex comes from NuMC through a HEPevent file
    simple_gun = 3,
    GPS = 9,
    fill_GPS = 24,
    HEPevent = 38,
    PaintFill_GPS = 42,
    PaintFill_HEPevt = 50,
    defered_track_process = 51,
    unknown = 99
};

enum class NewGene : Int_t {
    GPS,
    HEPevent,
    fill_HEPevt, // NuMC antineutrino: position is uniform in liquid, vertex comes from NuMC through a HEPevent file
    simple_gun,
    fill_GPS,
    defered_track_process,
    PaintFill_GPS,
    PaintFill_HEPevt,
    Plane_AtmNeutron,
    Sphere_AtmNeutron,
    Sphere_AmbientGamma,
    unknown = 99
};

}

#endif
