/*
 ========================================================
 Adapted from Nucifer analysis and simulation software
 March 2017: Matthieu Vivier
 ========================================================
 */
#include <DataDefinition.h>
#include <Index.h>
using namespace EnerGe;

#include <TObject.h>
#include <TNamed.h>
#include <TTimeStamp.h>
#include <TClonesArray.h>
#include <TString.h>
#include <TPRegexp.h>
#include <TMath.h>
using namespace TMath;

#include <stdexcept>
#include <iostream>
using namespace std;

ClassImp(Particle)
ClassImp(MCEvent)
ClassImp(MCInfo)


/////////////////////////
//// PARTICLE RELATED ////
/////////////////////////

void Particle::Zero()
{
    PDG = 0;
    VolInj = VOL_END;
    Einj = 0.f;
    //    EincCrystalMap.clear();
    //    EincVolumeMap.clear();
    //    EdepFirstElasticMap.clear();
    for (UInt_t i=0; i<NbVertex; i++) { Positions[i].SetXYZ(0.f,0.f,0.f); }
    for (UInt_t i=0; i<NbVertex; i++) { Direction[i].SetXYZ(0.f,0.f,0.f); }
}

//void Particle::SetEincCrystal(Int_t i, Float_t e)
//{
//    if (EincCrystalMap.count(i) == 0){
//        EincCrystalMap.emplace(i,e);
//    }
//}
//
//void Particle::SetEincVolume(TString VolName, Int_t i, Float_t e)
//{
//    if (EincVolumeMap.count(VolName) == 0){
//        std::map<Int_t, Float_t> myMap;
//        EincVolumeMap.emplace(VolName, myMap);
//        EincVolumeMap.at(VolName).emplace(i,e);
//    }
//    else if (EincVolumeMap.at(VolName).count(i)==0)
//    {
//        EincVolumeMap.at(VolName).emplace(i,e);
//    }
//}

/////////////////////////
//// MC EVENT RELATED ////
/////////////////////////

void MCEvent::Zero(){
    
    EdepQuenched = 0.f;
    
    for (UInt_t i=0; i<NbVolumes; i++) { EdepVol[i] = 0.f; }
}


/////////////////////////
//// MC INFO RELATED ////
/////////////////////////
Float_t MCInfo::GetEinj() const
{
    Float_t E=0.f;
    for (Int_t i=0; i<MCEvents->GetEntriesFast(); i++) {
        E += static_cast<MCEvent*>(MCEvents->At(i))->GetPrimaryParticle()->GetEinj();
    }
    return E;
}

// return the EincCrystalMap of the first particle injected only
//std::map<Int_t,Float_t> MCInfo::GetEincCrystalMap() const
//{
//    std::map<Int_t,Float_t> Emap;
//    Emap = static_cast<Particle*>(Particles->At(0))->GetEincCrystalMap();
//    return Emap;
//}

// return the EincVolumeMap of the first particle injected only
//std::map<TString,std::map<Int_t,Float_t>> MCInfo::GetEincVolumeMap() const
//{
//    std::map<TString,std::map<Int_t,Float_t>> EVolmap;
//    EVolmap = static_cast<Particle*>(Particles->At(0))->GetEincVolumeMap();
//    return EVolmap;
//}

// return the EdepFirstElasticMap of the first particle injected only
//std::map<TString, std::map<Int_t, std::vector<Float_t>> > MCInfo::GetEdepFirstElasticMap() const
//{
//    std::map<TString, std::map<Int_t, std::vector<Float_t>> > Emap;
//    Emap = static_cast<Particle*>(Particles->At(0))->GetEdepFistElasticMap();
//    return Emap;
//}

//Float_t MCInfo::GetEdep() const // /!\ Edep = EdepVol[VOL_SCINT]
//{
//    Float_t E=0.f;
//    for (Int_t i=0; i<MCEvents->GetEntriesFast(); i++) {
//        E += static_cast<MCEvent*>(MCEvents->At(i))->GetEdep();
//    }
//    return E;
//}

Float_t MCInfo::GetEdepQuenched() const
{
    Float_t E=0.f;
    for (Int_t i=0; i<MCEvents->GetEntriesFast(); i++) {
        E += static_cast<MCEvent*>(MCEvents->At(i))->GetEdepQuenched();
    }
    return E;
}

Float_t MCInfo::GetEdepVol(Volumes vol) const
{
    Float_t E=0.f;
    for (Int_t i=0; i<MCEvents->GetEntriesFast(); i++) {
        E += static_cast<MCEvent*>(MCEvents->At(i))->GetEdepVol(vol);
    }
    return E;
}

Float_t MCInfo::GetEdepVol(Int_t j) const
{
    Float_t E=0.f;
    if (j>=static_cast<Int_t>(NbVolumes)) {return -666.f;}
    for (Int_t i=0; i<MCEvents->GetEntriesFast(); i++) {
        E += static_cast<MCEvent*>(MCEvents->At(i))->GetEdepVol(j);
    }
    return E;
}
