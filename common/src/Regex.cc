#include <Regex.hh>

#ifdef REGEX_TEST

#include <iostream>
#include <algorithm>
#include <iterator>

using namespace std;

int main()
{
  string str("nopwat\n");
  Regex reg("no");
  Regex reg2("pwat");
  cout << "match: " << ( reg2.Match(str) ? "OK" : "NO" ) << endl ;
  reg.Sub(str,"");
  cout << str;
  string str2("1\t2   3");
  Regex reg3("\\s+");
  auto v = reg3.Split(str2);
  copy(v.begin(), v.end(), ostream_iterator<string>(cout,",")); cout << endl;
}
#endif
