/** Inspired by DCparam, the API for the Double Chooz simple "database"
    of numeric and string values, written by Glenn Horton-Smith.
*/

#include <Parameters.hh>

#include <Regex.hh>
#include <String.hh>

#ifdef PARAMBASE__USE_ROOT
# include <TDirectory.h>
# include <TObjString.h>
# include <TFile.h>
#endif

#include <cstdlib>
#include <iostream>
#include <fstream>
#include <string>
//#include <regex>
#include <memory>
using namespace std;

////////////////////////////////////////////////////////////////
////////////////////////// ParamBase ///////////////////////////
////////////////////////////////////////////////////////////////

ParamBase * ParamBase::theParamBase= nullptr;

////////////////////////////////////////////////////////////////
// Get the absolute path from environment
std::string ParamBase::GetPath(const char* env_path) const
{
  if (!env_path) throw parambase_except(" ***Error ParamBase::GetPath(): env_path is empty");

  const char* env = std::getenv(env_path);
  if(!env) throw parambase_except(" ***Error ParamBase::GetPath(): env_path is not defined: ", env_path);

  return scat(env,"/");
}

//Check http://www.cplusplus.com/reference/regex/ECMAScript/ for the regular expression pattern syntax
void ParamBase::ParseDataLine(const std::string& line, const char* regex_pattern, std::string prefix, ParamBase::EOverwrite overwrite)
{
  Regex regex(regex_pattern);
  int NbMatch = regex.Match(line);
    
  if(NbMatch == 1) std::cerr << " *Warning ParamBase::ParseDataLine: missing key value for '"<<line<<"'\n";
  else if(NbMatch > 1){
    
    std::string key = prefix.empty() ? regex[0] : prefix + "." + regex[0];
    if (StringMap.count(key) || NumMap.count(key)){
      
      std::cerr << " *Warning ParamBase::ParseDataLine: " << (overwrite == kKeep ? "PRESERVING" : "OVERRIDING" ) << " previous value for " << key << std::endl;
      if (overwrite == kKeep) return;
      
    } 
    UpdateKeyValues(key, regex.GetSplitResults());
    
  }
  
}


////////////////////////////////////////////////////////////////
/** ReadFile: read a file.  See class documentation for file format.
    If a parameter is defined more than once, e.g., due to calling ReadFile
    multiple times with different input files, the overwrite parameter decides
    whether the first or last definition is used.
    @param  input     input stream
    @param  overwrite  indicates how to handle parameter redefinitions
 */
void ParamBase::ReadFile(std::istream& input, std::string prefix, ParamBase::EOverwrite overwrite)
{
  if (IsReadOnly) throw parambase_except(" ***Error ParamBase::ReadFile1: Read-only database" );

  std::string line;
  while (std::getline(input,line)){
    
    // skip blank lines and lines beginning with '#'
    if (line.empty() || line.front() == '#' || line.find_first_not_of(' ')== std::string::npos) continue;
    ParseDataLine(line, "^\\s*(\\w+)\\s+([+-]?\\w+(?:[\\.+-]?\\w+([+-]?\\w+)?)?)(?:\\s+(\\w+))?", prefix, overwrite);
  }
  
}

////////////////////////////////////////////////////////////////
/// ReadFile: read from a file of the given name.
void ParamBase::ReadFile(std::string file_path, const char* env_path, const char* prefix, ParamBase::EOverwrite overwrite)
{
  if (env_path) file_path = GetPath(env_path) + file_path;
  if (IsReadOnly) throw parambase_except(" ***Error ParamBase::ReadFile: Read-only database, can not update: ", file_path);

#ifdef DEBUG
  std::cout << " Attempting to read database from file "<<file_path<< std::endl;
#endif

  std::ifstream input(file_path);
  if (!input) throw parambase_except(" ***Error ParamBase::ReadFile: Could not open: ", file_path);
  ReadFile(input, prefix, overwrite);
  
}

////////////////////////////////////////////////////////////////

void ParamBase::WriteText(ostream &os) const
{
  // write out parameters in the same order in which they were read in
  os.precision(15);
  for (size_t i=0; i!=KeyList.size(); i++) {
    const string& key( KeyList[i] );
    os << key;
    if ( HasNumValue(key) ) { os << ' '  << NumMap.at(key); }
    if ( HasStrValue(key) ) { os << " : " << StringMap.at(key); }
    os << endl;
  }
  os.flush();

  // write out tables (in the map order, i.e. not ordered)
  for (auto itMap=TableMap.begin(), endMap=TableMap.end(); itMap!=endMap; ++itMap) {
    os<<"\n\n"<<(*itMap).first<<"\n";

    ParamTable* tab = (*itMap).second.get();
    vector<string> ColNames = tab->GetColumnNames();

    for (auto it=ColNames.begin(), end=ColNames.end(); it!=end; ++it) {
      os << "\t" << *it;
    }
    os << "\n";

    for (int iRow=0; iRow<tab->GetNbRows(); iRow++) {
      for (auto it=ColNames.begin(), end=ColNames.end(); it!=end; ++it) {
	os << "\t" << (*tab)[*it].at(iRow);
      }
     os << "\n";
   }
  }
  os.flush();

  cout << " --- Database successfully written in text file: "<<KeyList.size()
    <<" parameters and "<<TableMap.size()<<" tables ---\n";
}

////////////////////////////////////////////////////////////////
/// WriteFile: write to a file of the given name.
void ParamBase::WriteText(const char* filename, const char* env_path) const
{
  string FileWithPath = filename;
  if (env_path) { FileWithPath = GetPath(env_path) + FileWithPath; }

  cout << " Attempting to write database on text file: "<<FileWithPath<<endl;
  ofstream ofs;
  ofs.open(filename, std::ios::out);
  if (!ofs.is_open() || !ofs.good()) {
    throw parambase_except(" ***Error ParamBase::WriteText: Could not open ", filename);
  }

  WriteText(ofs);
  ofs.close();
}

////////////////////////////////////////////////////////////////

const string& ParamBase::GetStrValue(const string& key) const
{
  if ( StringMap.count(key)==0 ) {
    throw parambase_except(" ***Error ParamBase::GetStrValue: Attempt to retrieve undefined key ", key);
  }
  return StringMap.at(key);
}

////////////////////////////////////////////////////////////////

double& ParamBase::operator[](const string& key)
{
  if (IsReadOnly) {
    throw parambase_except(" ***Error ParamBase::operator[]: Attempt to modify read-only base: ", key);
  }
  if ( NumMap.count(key)!=0 ) {
//     throw parambase_except(" ***Error ParamBase::operator[]2: Attempt to create already existing key: ", key);
    cerr << " *Warning ParamBase::operator[]: Overriding existing key: "<< key<<"\n";
  }
  if ( StringMap.count(key)==0 ) { KeyList.push_back(key); } // the key may have been already created with just a string value
  return NumMap[key];
}

////////////////////////////////////////////////////////////////

string& ParamBase::operator()(const string& key)
{
  if (IsReadOnly) {
    throw parambase_except(" ***Error ParamBase::operator()1: Attempt to modify read-only base: ", key);
  }
  if ( StringMap.count(key)!=0 ) {
//     throw parambase_except(" ***Error ParamBase::operator()2: Attempt to create already existing key: ", key);
    cerr << " *Warning ParamBase::operator(): Overriding existing key: "<< key<<"\n";
  }
  if ( NumMap.count(key)==0 ) { KeyList.push_back(key); } // the key may have been already created with just a numeric value
  return StringMap[key];
}

////////////////////////////////////////////////////////////////

const ParamTable* ParamBase::GetTablePtr(const char *tablename) const
{
  if ( TableMap.count(string(tablename))==0 ) {
    throw parambase_except(" ***Error ParamBase::GetTable:Attempt to access undefined table ", tablename);
  }
  return TableMap.at(string(tablename)).get();
}

////////////////////////////////////////////////////////////////

void ParamBase::DefineTable(const char *filename, const char *tablename, const char* env_path)
{
  if (IsReadOnly) {
    throw parambase_except( " ***Error DataBase::DefineTable2: Read-only database: ", tablename);
  }

  if ( TableMap.count(tablename)!=0 ) {
    throw parambase_except(" ***Error ParamBase::DefineTable3: Attempt to redefine already defined table: ", tablename);
  }

  string FileWithPath = filename;
  if (env_path) { FileWithPath = GetPath(env_path) + FileWithPath; }

  TableMap[tablename] = std::unique_ptr<ParamTable>( new ParamTable( FileWithPath ) );
  StringMap[tablename] = string("Table read from ")+FileWithPath;
  NumMap[tablename] = TableMap[tablename]->GetNbRows();
  KeyList.push_back(tablename);
}

////////////////////////////////////////////////////////////////

std::vector< double > ParamTable::GetColumnAsNum(const std::string& colname) const
{
  if ( ColMap.count(colname)==0 ) {
    throw parambase_except(" ***Error ParamTable::GetColumnAsNum: Unknown : ",colname);
  }

  std::vector<double> v;
  for (auto &el: ColMap.at(colname)) { v.push_back(stod(el)); }
  return v;
}

////////////////////////////////////////////////////////////////
///////////////////////// ParamTable ///////////////////////////
////////////////////////////////////////////////////////////////

/** Create a ParamTable object and initialize it from a file.
    It may seem strange that no "ReadFile()" function is provided, and
    no default ParamTable() constructor is provided, but this is intentional:
    no uninitialized tables are allowed, and once initialized, tables are
    intended to be immutable. */
ParamTable::ParamTable(const char *filename)
{
#ifdef DEBUG
  std::cout << " Attempting to read table from file "<<filename<<std::endl;
#endif

  std::ifstream input(filename);
  if (!input) throw parambase_except(" ***Error ParamTable::ParamTable: Could not open ", filename);

  FillColumnNames(input, "^=\\s*(?:(\\w+)\\s+)*(\\w+)");
  for (const auto& name : ColNames) ColMap[name].reserve(10);

  std::string line;
  while(std::getline(input,line)) {

    // skip blank lines and lines beginning with '#'
    if (line.empty() || line.front() == '#' || line.find_first_not_of(' ')==string::npos) continue;
    ParseDataLine(line, "^\\s*(?:(\\w+)\\s+)*(\\w+)");
    
  }

#ifdef DEBUG
  std::cout << " --- Table successfully defined with "<<NbRow<<" rows and "<<ColNames.size()<<" columns found ---\n";
#endif
}

void ParamTable::FillColumnNames(std::ifstream& input, const char* regex_pattern){
  
  std::string line;
  while (std::getline(input,line) && line.front() != '=');
  
  std::smatch matches;
  std::regex regex(regex_pattern);
  if (!std::regex_search(line, matches, regex)) throw parambase_except(" ***Error ParamTable::GetHeaders: Could not parse headers");
  
    ColNames.reserve(matches.size()-1);
#if __cplusplus >= 201402L
    std::copy_if(matches.begin()+1, matches.end(), std::back_inserter(ColNames), [](const auto& match)
                 {return match.matched;});
#else
    std::copy_if(matches.begin()+1, matches.end(), std::back_inserter(ColNames), [](decltype(*begin(matches))& match) {return match.matched;});
#endif

  
}

void ParamTable::ParseDataLine(const std::string& line, const char* regex_pattern){
  
  std::smatch matches;
  std::regex regex(regex_pattern);
  if (!std::regex_search(line, matches, regex)) 
    throw parambase_except(" ***Error ParamTable:ParseDataLine Could not parse line ", line);
  if (matches.size() != ColNames.size() + 1)
    throw parambase_except(" ***Error ParamTable::ParamTable: expected ",ColNames.size()," columns, only got ",matches.size()-1," values on row ",NbRow);
  
  for (size_t i=0; i<ColNames.size(); ++i) ColMap[ColNames[i]].push_back(matches[i+1]);
  ++NbRow;
  
}

////////////////////////////////////////////////////////////////
/* Text can not be written in ROOT TFile as easily as in (o)fstream:
 * we need to write a TObject or one of its daughters.
 * The simplest way is to concatenate the text in a single TString (which is NOT a daughter of TObject),
 * then push the TString in a TObjString (a TObject encapsulating a TString), then write the TObjString.
 */

#ifdef PARAMBASE__USE_ROOT
void ParamBase::WriteRoot(TFile* file) const
{
  TDirectory* dir = TDirectory::CurrentDirectory(); // save the current directory
  file->cd(); // change (Root) directory to the output file

  TObjString ObjStr;
  string SaveStr;

  // write out parameters in the same order in which they were read in
  for (size_t i=0; i!=KeyList.size(); i++) {
    const string& key( KeyList[i] );
    SaveStr += key;
    if ( HasNumValue(key) ) { SaveStr += scat(" ",NumMap.at(key)); }
    if ( HasStrValue(key) ) { SaveStr += scat(" : ",StringMap.at(key)); }
    SaveStr += "\n";
  }
  ObjStr.SetString(SaveStr.c_str());
  ObjStr.Write("Parameters");

  // write out tables (in the map order, i.e. not ordered)
  for (auto itMap=TableMap.begin(), endMap=TableMap.end(); itMap!=endMap; ++itMap) {
    SaveStr.clear();
    SaveStr = scat("\n\n",(*itMap).first,"\n");

    ParamTable* tab = (*itMap).second.get();
    const vector<string>* ColNames = &tab->GetColumnNames();
    for (auto itV=ColNames->begin(), end=ColNames->end(); itV!=end; ++itV) {
      SaveStr += scat("\t",*itV);
    } SaveStr += "\n";

    for (int iRow=0; iRow<tab->GetNbRows(); iRow++) {
      for (auto itV=ColNames->begin(), end=ColNames->end(); itV!=end; ++itV) {
	SaveStr += scat("\t",(*tab)[*itV].at(iRow));
      } SaveStr += "\n";
   }

    ObjStr.SetString(SaveStr.c_str());
    ObjStr.Write(scat("Table ",(*itMap).first).c_str());
  }

  dir->cd(); // back to previous directory
  cout << " --- Database successfully written in ROOT file "<<file->GetName()<<": "
    <<KeyList.size()<<" parameters and "<<TableMap.size()<<" tables ---\n";
}

////////////////////////////////////////////////////////////////

void ParamBase::WriteRoot(const char *filename) const
{
  cout << " Attempting to write database on ROOT file "<<filename<<endl;
  TFile file(filename, "RECREATE");
  if (!file.IsOpen()) {
    throw parambase_except(" ***Error ParamBase::WriteRoot1: Could not open ", filename);
  }
  WriteRoot(&file);
  file.Close();
}
#endif
