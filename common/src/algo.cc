#include <algo.hh>
#include <Parameters.hh>
#include <Exception.hh>
#include <DataDefinition.h>

#include <TRandom.h>
#include <TRandom3.h>
#include <TDirectory.h>
#include <TTree.h>
#include <TChain.h>
#include <TPRegexp.h>
#include <TString.h>

#include <iostream>

#include <cstdlib>
#include <stdexcept>
using namespace std;

ClassImp(Algo)

Double_t Algo::minE=0.;
Double_t Algo::minE1=0.;
Double_t Algo::minE2D=0.;
Double_t Algo::maxE2D=0.;
Double_t Algo::maxE=0.;
Double_t Algo::maxE1=0.;
Double_t Algo::minEinj=0.;
Double_t Algo::maxEinj=0.;
Double_t Algo::minEinc=0.;
Double_t Algo::maxEinc=0.;

Double_t Algo::Originx=0.;
Double_t Algo::Originy=0.;
Double_t Algo::Originz=0.;
Double_t Algo::minVz=0.;
Double_t Algo::maxVz=0.;
Double_t Algo::minVx=0.;
Double_t Algo::maxVx=0.;
Double_t Algo::minVy=0.;
Double_t Algo::maxVy=0.;
Double_t Algo::minVsecx=0.;
Double_t Algo::maxVsecx=0.;
Double_t Algo::minVsecy=0.;
Double_t Algo::maxVsecy=0.;
Double_t Algo::minVr=0.;
Double_t Algo::maxVr=0.;

Int_t Algo::NbBinE=0;
Int_t Algo::NbBinE1=0;
Int_t Algo::NbBinE2D=0;
Int_t Algo::NbBinEinj=0;
Int_t Algo::NbBinEinc=0;
Int_t Algo::NbBinV=0;
Int_t Algo::NbBinR=0;
Int_t Algo::unit_edep=0;
Int_t Algo::unit_prim=0;
Int_t Algo::unit_second=0;

TString Algo::VolumeNameList[] = {
    "Vol_World",
    "Vol_Inactive",
    "Vol_End",
    "Vol_Slab",
    "Vol_Lab",
    "Vol_Top_Ge",
    "Vol_Mid_LWO",
    "Vol_Bot_Ge",
    "Vol_Cryostat",
    "Vol_HPGe"
};

UInt_t Algo::OrsayOVCrysIndex[] = {0};
//Bool_t Algo::VetoFlag[] = {0};
Double_t Algo::OrsayOVCrysTh[] = {0};
Bool_t Algo::ApplyResol=0;

Double_t Algo::A_resol[] = {0};
Double_t Algo::B_resol[] = {0};
Double_t Algo::C_resol[] = {0};

TString Algo::Coinc_names[Algo::NCoincs]={"coinc_Bot_only", "coinc_Mid_only", "coinc_Mid_&_Bot", "coinc_Top_only", "coinc_Top_&_Bot", "coinc_Top_&_Mid", "coinc_Top_&_Mid_&_Bot"};

std::map<UInt_t,Algo::GeoVolume> Algo::VolMap;

std::pair<TString,Double_t> Algo::EdepRange[] = {
    std::make_pair("eV",100),
    std::make_pair("keV",1),
    std::make_pair("keV",10),
    std::make_pair("keV",100),
    std::make_pair("MeV",1),
    std::make_pair("MeV",10),
    std::make_pair("MeV",100),
    std::make_pair("GeV",1),
    std::make_pair("GeV",10),
    std::make_pair("GeV",100)
};

TString PrintTime(Double_t t)
{
    TString s;
    if (t > 60) {
        t /= 60;
        if (t > 60) {
            t /= 60;
            if (t > 24) {
                t /= 24;
                s.Form("%f day",t);
            }
            else { s.Form("%f h",t); }
        }
        else { s.Form("%f min",t); }
    }
    else { s.Form("%f s",t); }
    return s;
}

////////////////////////////////////////////////////////////////
/////////// MOTHER of ALGOs //////////////////
////////////////////////////////////////////////////////////////
// abstract class for all algorithms

void Algo::InitVolMap(){
    
    if (!VolMap.empty() || VolMap.size() > 0) {return;}
    
    for (UInt_t i=0; i<EnerGe::NbVolumes; i++){
        
        TString DirectoryName="";
        
        VolMap[i] = GeoVolume{false,VolumeNameList[i],DirectoryName+VolumeNameList[i]};
    }
}

void Algo::SetOrsayOVCrystalIndex(){
    
    OrsayOVCrysIndex[0] = EnerGe::Volumes::VOL_TOP_GE;
    OrsayOVCrysIndex[1] = EnerGe::Volumes::VOL_MID_LWO;
    OrsayOVCrysIndex[2] = EnerGe::Volumes::VOL_BOT_GE;

}

void Algo::ReadParameterDB()
{
    InitVolMap();
    
    if (unit_edep != 0.) { return; } // variables already initialised
    
    const ParamBase &db(ParamBase::GetDataBase());
    try {
        
        // unit
        unit_edep = static_cast<Int_t>(db["unit_edep"]); //energy unit for Edep analysis
        unit_prim = static_cast<Int_t>(db["unit_prim"]); //energy unit for Prim analysis
        unit_second = static_cast<Int_t>(db["unit_second"]); //energy unit for Sec analysis
        
        //List of volumes (for Edep analysis)
        //Need to ensure correspondence with volumes declared in Index.h
        VolMap[0].IsAnalyzed = static_cast<UInt_t>(db["VolWorld"]);
        VolMap[1].IsAnalyzed = static_cast<UInt_t>(db["VolInactive"]);
        VolMap[2].IsAnalyzed = static_cast<UInt_t>(db["VolEnd"]);
        
        VolMap[3].IsAnalyzed = static_cast<UInt_t>(db["VolSlab"]);
        VolMap[4].IsAnalyzed = static_cast<UInt_t>(db["VolLab"]);
        
        VolMap[5].IsAnalyzed = static_cast<UInt_t>(db["Vol_Top_Ge"]);
        VolMap[6].IsAnalyzed = static_cast<UInt_t>(db["Vol_Mid_LWO"]);
        VolMap[7].IsAnalyzed = static_cast<UInt_t>(db["Vol_Bot_Ge"]);
        
        VolMap[8].IsAnalyzed = static_cast<UInt_t>(db["Vol_Cryostat"]);
        
        VolMap[9].IsAnalyzed = static_cast<UInt_t>(db["Vol_HPGe"]);
        
        // histo bounds (used by various algorithms)
        minE = db["minE"];
        maxE = db["maxE"];
        minE1 = db["minE1"];
        maxE1 = db["maxE1"];
        minE2D = db["minE2D"];
        maxE2D = db["maxE2D"];
        minEinj = db["minEinj"];
        maxEinj = db["maxEinj"];
        minEinc = db["minEinc"];
        maxEinc = db["maxEinc"];
        
        //Histo bounds used by Vertex analysis
        Originx = db["Origin_x"];
        Originy = db["Origin_y"];
        Originz = db["Origin_z"];
        minVz = db["minVz"];
        maxVz = db["maxVz"];
        minVx = db["minVx"];
        maxVx = db["maxVx"];
        minVy = db["minVy"];
        maxVy = db["maxVy"];
        minVsecx = db["minVsecx"];
        maxVsecx = db["maxVsecx"];
        minVsecy = db["minVsecy"];
        maxVsecy = db["maxVsecy"];
        minVr = db["minVr"];
        maxVr = db["maxVr"];
        
        // histo number of bins
        NbBinE = static_cast<Int_t>(db["NbBinE"]);
        NbBinE1 = static_cast<Int_t>(db["NbBinE1"]);
        NbBinE2D = static_cast<Int_t>(db["NbBinE"]);
        NbBinEinj = static_cast<Int_t>(db["NbBinEinj"]);
        NbBinEinc = static_cast<Int_t>(db["NbBinEinc"]);
        NbBinR = static_cast<Int_t>(db["NbBinR"]);
        NbBinV = static_cast<Int_t>(db["NbBinV"]);
        
        //Parameters used by Orsay OV Algo
        
        OrsayOVCrysTh[0] = static_cast<Double_t>(db["TopCOV_th"])*0.001; //from keV to MeV
        OrsayOVCrysTh[1] = static_cast<Double_t>(db["MidLWO_th"])*0.001; //from keV to MeV
        OrsayOVCrysTh[2] = static_cast<Double_t>(db["BotCOV_th"])*0.001; //from keV to MeV
        
        ApplyResol = static_cast<Bool_t>(db["Apply_resolution"]);
        A_resol[0] = static_cast<Double_t>(db["TopCOV_A"])*0.001; //from keV to MeV;
        A_resol[1] = static_cast<Double_t>(db["MidLWO_A"])*0.001; //from keV to MeV;
        A_resol[2] = static_cast<Double_t>(db["BotCOV_A"])*0.001; //from keV to MeV;
        
        B_resol[0] = static_cast<Double_t>(db["TopCOV_B"])*0.001*0.001; //from keV2 to MeV2;
        B_resol[1] = static_cast<Double_t>(db["MidLWO_B"])*0.001*0.001; //from keV2 to MeV2;
        B_resol[2] = static_cast<Double_t>(db["BotCOV_B"])*0.001*0.001; //from keV2 to MeV2;
        
        C_resol[0] = static_cast<Double_t>(db["TopCOV_C"]);
        C_resol[1] = static_cast<Double_t>(db["MidLWO_C"]);
        C_resol[2] = static_cast<Double_t>(db["BotCOV_C"]);
    }
    catch(runtime_error &err) {
        cerr << " ***Error Algo::Initialise: Missing parameter in database\n"<<err.what()<<"\n";
        throw err;
    }
}

void Algo::SetConvertCoeff(Double_t &convert_coeff, TString &unit_name, Int_t unit_index){
    
    switch (unit_index){
        case 1 :
            unit_name = "meV";
            convert_coeff = 1.e9;
            break;
        case 2 :
            unit_name = "eV";
            convert_coeff = 1000000.;
            break;
        case 3 :
            unit_name = "keV";
            convert_coeff = 1000.;
            break;
        case 4 :
            unit_name = "MeV";
            convert_coeff = 1.;
            break;
        case 5 :
            unit_name = "GeV";
            convert_coeff = 0.001;
            break;
        default :
            unit_name = "MeV";
            convert_coeff = 1.;
            break;
    }
}

//Method which sets a weight for the energy related histograms.
void Algo::SetEdepWeight(Int_t GeneCode, Double_t E, Double_t &Edep_w){
    
    switch (GeneCode){
            
        case (Int_t) NewGene::Plane_AtmNeutron:
            Edep_w = 1/E;
            break;
            
        case (Int_t) NewGene::Sphere_AtmNeutron:
            Edep_w = 1/E;
            break;
            
        default:
            Edep_w = 1;
            break;
    }
}

TDirectory* Algo::CreateDirectory(TDirectory* RootDir)
{
    if (!HasDirectory) { AlgoDir = RootDir->mkdir((fName+"Dir").Data(),fTitle.Data()); HasDirectory = true; }
    else { std::cerr << "*Warning Algo::CreateDirectory: Algo directory already exists\n"; }
    return AlgoDir;
}

Bool_t Algo::Cd() { return AlgoDir->cd(); }

void Algo::Init()
{
    AlgoDir=gDirectory;
//    if (TPMERegexp("print_matrix").Match(option))         { PrintMatrix = true; }      else { PrintMatrix = false; }
//    if (TPMERegexp("no_write_histo").Match(option))       { WriteHisto = false; }      else { WriteHisto = true; }
//    if (TPMERegexp("no_write_on_ofstream").Match(option)) { WriteOnOfstream = false; } else { WriteOnOfstream = true; }
//    if (TPMERegexp("no_write_in_process").Match(option))  { WriteInProccess = false; } else { WriteInProccess = true; }
//    if (TPMERegexp("no_print_in_process").Match(option))  { PrintInProcess = false; }  else { PrintInProcess = true; }
}
