#include <String.hh>

hash_t HashRunTime(const char* str)
{
  hash_t ret{basis};
  while(*str){
    ret ^= *str;
    ret *= prime;
    ++str;
  }
  return ret;
}
