#include <algo_list.hh>

#include <algo.hh>

#include <iostream>
#include <map>

using namespace std;

std::map<TString,Algo*> AlgoList::List = {};

Algo* AlgoList::GetAlgo(const TString& algo_name)
{
  if ( List.count(algo_name) ) { return List.at(algo_name); }
  return nullptr;
}

void AlgoList::AddAlgo(const TString& name, Algo* algo)
{
  if (algo == nullptr) {
    cerr << " *Warning AlgoList::AddAlgo1: Skipping null ptr algo: "<<name<<endl;
  }
  if (List.count(name) == 0) { List[name] = algo; }
  else {
    cerr << " *Warning AlgoList::AddAlgo2: Algo is already registred: "<<name<<endl;
    delete algo;
  }
}

void AlgoList::Clear()
{
  for (auto itA = List.begin(); itA != List.end(); ++itA) {
    itA->second->Cd();
    delete itA->second;
  }
  List.clear();
}
