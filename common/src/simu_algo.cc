#include <simu_algo.hh>

#include <Index.h>
#include <DataDefinition.h>
#include <Parameters.hh>
#include <String.hh>
#include <Regex.hh>
#include <make_unique.hh>
//using namespace EnerGe;

#include <TMath.h>
#include <TClass.h>
#include <TText.h>
#include <TPaveText.h>
#include <TClonesArray.h>
#include <TTree.h>
#include <TString.h>
#include <TPRegexp.h>
#include <TCanvas.h>
#include <TFile.h>
#include <TPad.h>
#include <TF1.h>
#include <TRandom.h>

#include <sstream>
#include <fstream>
#include <stdexcept>
#include <vector>
#include <memory>

////////////////////////////////////////////////////////////////////////////////
//////////////// Edep analysis of simulation ///////////////////
////////////////////////////////////////////////////////////////////////////////

void EdepAlgo::Initialise(TTree* tree)
{
    ReadParameterDB();
    
    //Initialising unit parameters for energy deposition
    SetConvertCoeff(edep_convert_coeff,edep_unit_name,Algo::unit_edep);
    
    //Initialising unit parameters for energy injection
    SetConvertCoeff(einj_convert_coeff,einj_unit_name,Algo::unit_prim);
    
    
    NbEvtTotal = tree->GetEntries();
    NbEvtTot = tree->GetEntries(); //NbEvtTot is a double : needed to scale the histograms
    vNeutronFinalProcess.assign(NbNeutronProcess,0);
    
    hNameE = scat("_",minE1,"-",maxE1,edep_unit_name);
    
    for (UInt_t i=0; i<EnerGe::NbVolumes; i++){    //Create the differents Edep Histograms for the volumes
        if(VolMap[i].IsAnalyzed){
            TString VolumeName = VolMap[i].Name;
            AlgoDir->mkdir(VolMap[i].DirName+"/Edep");
            AlgoDir->mkdir(VolMap[i].DirName+"/Einj");
            
            //Energy deposition sorted according to the nature of primary particle - can be useful for example for generator which shoots different particle types (like radioactive decays...)
            mhVolumes["Edep_electron_"+VolumeName] = make_unique<TH1D>("nEdep_electron_"+VolumeName,"Edep_electron / "+VolumeName,NbBinE,minE,maxE);
            mhVolumes["Edep_electron_"+VolumeName]->GetXaxis()->SetTitle("Edep ["+edep_unit_name+"]");
            
            mhVolumes["Edep_gamma_"+VolumeName]    = make_unique<TH1D>("nEdep_gamma_"+VolumeName,"Edep_gamma / "+VolumeName,NbBinE,minE,maxE);
            mhVolumes["Edep_gamma_"+VolumeName]->GetXaxis()->SetTitle("Edep ["+edep_unit_name+"]");
            
            mhVolumes["Edep_neutron_"+VolumeName] = make_unique<TH1D>("nEdep_neutron_"+VolumeName,"Edep_neutron / "+VolumeName,NbBinE,minE,maxE);
            mhVolumes["Edep_neutron_"+VolumeName]->GetXaxis()->SetTitle("Edep ["+edep_unit_name+"]");
            
            mhVolumes["Edep_positron_"+VolumeName] = make_unique<TH1D>("nEdep_positron_"+VolumeName,"Edep_positron / "+VolumeName,NbBinE,minE,maxE);
            mhVolumes["Edep_positron_"+VolumeName]->GetXaxis()->SetTitle("Edep ["+edep_unit_name+"]");
            
            
            //"Custom" energy deposition histogram (lin scale)
            mhVolumes["Edep_"+VolumeName+hNameE] = make_unique<TH1D>("Edep_"+VolumeName+hNameE,"Edep / "+VolumeName+hNameE,NbBinE1,minE1,maxE1);
            mhVolumes["Edep_"+VolumeName+hNameE]->GetXaxis()->SetTitle("Edep ["+edep_unit_name+"]");
            
            //histogram to see what was the initial energy of the particle that made an energy deposit in "VolumeName" (lin scale)
            mhVolumes["Einj_origin_Edep_"+VolumeName+hNameE] = make_unique<TH1D>("nEinj_origin_Edep_"+VolumeName+hNameE,"Einj / evt"+hNameE,NbBinEinj,minEinj,maxEinj);
            mhVolumes["Einj_origin_Edep_"+VolumeName+hNameE]->GetXaxis()->SetTitle("Einj ["+einj_unit_name+"]");
            
            //same than above with log scale
            mhVolumes["Einj_log_origin_Edep_"+VolumeName+hNameE]  = make_unique<TH1D>("nEinj_log_origin_Edep_"+VolumeName+hNameE,"Log(Einj) / evt"+hNameE,NbBinEinj,TMath::Log10(1e-9*einj_convert_coeff),TMath::Log10(maxEinj));
            mhVolumes["Einj_log_origin_Edep_"+VolumeName+hNameE]->GetXaxis()->SetTitle("Log(Einj/1"+einj_unit_name+")");
            
            //log scale histogram for energy deposition
            mhVolumes["Edep_log_"+VolumeName] = make_unique<TH1D>("nEdep_log_"+VolumeName,"Log(Edep) / evt",NbBinE,TMath::Log10(1e-9*edep_convert_coeff),TMath::Log10(maxE));
            mhVolumes["Edep_log_"+VolumeName]->GetXaxis()->SetTitle("Log(Edep/1"+edep_unit_name+")");
            
            //"Permanent" energy deposition histograms (different energy ranges in lin scale)
            for (UInt_t p=0; p<EdepAlgo::NEdepRange; p++){
                
                TString EdepName = Form("0-%g%s",EdepRange[p].second,EdepRange[p].first.Data());
                TString hName = Form("Edep_%s_%s",VolumeName.Data(),EdepName.Data());
                mhVolumes[hName] = make_unique<TH1D>("n"+hName,"Edep / "+VolumeName,NbBinE,0,EdepRange[p].second); //units here in MeV !!
                mhVolumes[hName]->GetXaxis()->SetTitle(Form("E_{dep} [%s]",EdepRange[p].first.Data()));
                
                mhVolumes["Einj_log_origin_"+hName] = make_unique<TH1D>("nEinj_log_origin_"+hName,Form("Log(Einj) / %s %s",VolumeName.Data(),EdepName.Data()),NbBinEinj,TMath::Log10(1e-9*einj_convert_coeff),TMath::Log10(maxEinj));
                mhVolumes["Einj_log_origin_"+hName]->GetXaxis()->SetTitle("Log(Einj/1"+einj_unit_name+")");
            }
        }
    }
    
    //mh["TimeMC"]->GetXaxis()->SetTitle("Time [µs]");
    //mh["Edep_defered"]->GetXaxis()->SetTitle("Edep ["+edep_unit_name+"]");
    //mh["Edep_prompt"]->GetXaxis()->SetTitle("Edep ["+edep_unit_name+"]");
    
    //mh["Einj"]->GetXaxis()->SetTitle("Einj ["+edep_unit_name+"]");
    //mh["Einj_log"]->GetXaxis()->SetTitle("Log(Einj/1"+edep_unit_name+")");
    //mh["EincCrystal"]->GetXaxis()->SetTitle("Eincident Neutron Crystal [MeV]");
    //mh["Einj_defered"]->GetXaxis()->SetTitle("Einj ["+edep_unit_name+"]");
    //mh["Einj_prompt"]->GetXaxis()->SetTitle("Einj ["+edep_unit_name+"]");
    //mh["Einj_positron"]->GetXaxis()->SetTitle("Einj ["+edep_unit_name+"]");
    //mh["Einj_electron"]->GetXaxis()->SetTitle("Einj ["+edep_unit_name+"]");
    //mh["Einj_gamma"]->GetXaxis()->SetTitle("Einj ["+edep_unit_name+"]");
    //mh["Einj_neutron"]->GetXaxis()->SetTitle("Einj ["+edep_unit_name+"]");
    //mh["path_neutron"]->GetXaxis()->SetTitle("Mean free path [mm]");
    //mh["path_gamma"]->GetXaxis()->SetTitle("Mean free path [mm]");
    //mh["path_positron"]->GetXaxis()->SetTitle("Mean free path [mm]");
    //mh["path_electron"]->GetXaxis()->SetTitle("Mean free path [mm]");
    
    for (auto it=mh.begin(); it!=mh.end(); ++it) { it->second->GetYaxis()->SetTitle("Hits []"); }
    for (auto it=mhVolumes.begin(); it!=mhVolumes.end(); ++it) { it->second->GetYaxis()->SetTitle("Hits []");}
}

void EdepAlgo::Process(ULong64_t , Int_t T)
{
    // multiple tree=run=file management
    if (CurrentTree != T ) {
        CurrentTree = T;
        UT=0.;
    } // reset storage at tree change
    
    defered=false;
    theNeutronFinalProcess=None;
    
    Einj = einj_convert_coeff*((*MC)->GetEinj()); // always kinetic energy, originally in MeV
    deltaUT = (*MC)->GetTime()-UT; // should be µs
    UT = (*MC)->GetTime(); // should be µs
    
    if (deltaUT < 0.) {
        ++NbUnordered;
        bout << " *Warning EdepAlgo::Process: Unordered events, deltaUT<0.: "<<deltaUT<<std::endl;
        bout << "   Current event: primary type= "<<(*MC)->GetPrimaryType()<<" with particle number= "<<(*MC)->GetNbEvents()<<":";
        for (Int_t j=0; j<(*MC)->GetNbEvents(); j++) { bout << "\t" << (*MC)->GetMCEvent(j)->GetPrimaryParticle()->GetPDG(); }
        bout << std::endl;
    }
    
    if ( (*MC)->GetPrimaryType() == (Int_t)NewGene::defered_track_process
        or (*MC)->GetPrimaryType() == (Int_t)OldGene::defered_track_process) {
        NbEvtDefered ++;
        defered=true;
    } else { NbEvtGenerator++; }
    
    //Define weight for filling MC event histograms generated with atm neutron gen (because phi(E) x E is sampled instead of phi)
    Double_t edep_w = 1;
//    if ((*MC)->GetPrimaryType() == (Int_t) NewGene::Plane_AtmNeutron || (*MC)->GetPrimaryType() == (Int_t) NewGene::Sphere_AtmNeutron) {edep_w = 1/((*MC)->GetEinj());}
//    else {edep_w = 1;}
    
    // histo of whole event properties
    //mh["TimeMC"]->Fill(deltaUT);
    
    for (UInt_t i=0; i<EnerGe::NbVolumes; i++){
        if(VolMap[i].IsAnalyzed){
            TString VolumeName = VolMap[i].Name;
            Edep = edep_convert_coeff*((*MC)->GetEdepVol(i));
            
            mhVolumes["Edep_log_"+VolumeName]->Fill(TMath::Log10(Edep),edep_w);
            
            
            if(Edep > minE1 && Edep < maxE1){
                mhVolumes["Edep_"+VolumeName+hNameE]->Fill(Edep,edep_w);
            }
            
            Edep*= 1./(edep_convert_coeff); // Repassage en MeV
            
            if(Edep*1e6 > 0. && Edep*1e6 < 100.){
                mhVolumes["Edep_"+VolumeName+"_0-100eV"]->Fill(Edep*1e6,edep_w);
                mhVolumes["Einj_log_origin_Edep_"+VolumeName+"_0-100eV"]->Fill(TMath::Log10(Einj),edep_w);
            }
            
            if(Edep*1000 > 0. && Edep*1000 < 1.){
                mhVolumes["Edep_"+VolumeName+"_0-1keV"]->Fill(Edep*1000,edep_w);
                mhVolumes["Einj_log_origin_Edep_"+VolumeName+"_0-1keV"]->Fill(TMath::Log10(Einj),edep_w);
            }
            
            if(Edep*1000 > 0. && Edep*1000 < 10.){
                mhVolumes["Edep_"+VolumeName+"_0-10keV"]->Fill(Edep*1000,edep_w);
                mhVolumes["Einj_log_origin_Edep_"+VolumeName+"_0-10keV"]->Fill(TMath::Log10(Einj),edep_w);
            }
            
            if(Edep*1000 > 0. && Edep*1000 < 100.){
                mhVolumes["Edep_"+VolumeName+"_0-100keV"]->Fill(Edep*1000,edep_w);
                mhVolumes["Einj_log_origin_Edep_"+VolumeName+"_0-100keV"]->Fill(TMath::Log10(Einj),edep_w);
            }
            
            if(Edep > 0. && Edep < 1.){
                mhVolumes["Edep_"+VolumeName+"_0-1MeV"]->Fill(Edep,edep_w);
                mhVolumes["Einj_log_origin_Edep_"+VolumeName+"_0-1MeV"]->Fill(TMath::Log10(Einj),edep_w);
            }
            
            if(Edep > 0. && Edep < 10.){
                mhVolumes["Edep_"+VolumeName+"_0-10MeV"]->Fill(Edep,edep_w);
                mhVolumes["Einj_log_origin_Edep_"+VolumeName+"_0-10MeV"]->Fill(TMath::Log10(Einj),edep_w);
            }
            
            if(Edep > 0. && Edep < 100.){
                mhVolumes["Edep_"+VolumeName+"_0-100MeV"]->Fill(Edep,edep_w);
                mhVolumes["Einj_log_origin_Edep_"+VolumeName+"_0-100MeV"]->Fill(TMath::Log10(Einj),edep_w);
            }
            
            if(Edep/1000. > 0. && Edep/1000. < 1.){
                mhVolumes["Edep_"+VolumeName+"_0-1GeV"]->Fill(Edep/1000.,edep_w);
                mhVolumes["Einj_log_origin_Edep_"+VolumeName+"_0-1GeV"]->Fill(TMath::Log10(Einj),edep_w);
            }
            
            if(Edep/1000. > 0. && Edep/1000. < 10.){
                mhVolumes["Edep_"+VolumeName+"_0-10GeV"]->Fill(Edep/1000.,edep_w);
                mhVolumes["Einj_log_origin_Edep_"+VolumeName+"_0-10GeV"]->Fill(TMath::Log10(Einj),edep_w);
            }
            
            if(Edep/1000. > 0. && Edep/1000. < 100.){
                mhVolumes["Edep_"+VolumeName+"_0-100GeV"]->Fill(Edep/1000.,edep_w);
                mhVolumes["Einj_log_origin_Edep_"+VolumeName+"_0-100GeV"]->Fill(TMath::Log10(Einj),edep_w);
            }
            
            Edep*= edep_convert_coeff; // de MeV, on repasse à l'unité souhaitée
            if(i == 0 && Edep > minE1 && Edep < maxE1){
                mhVolumes["Einj_origin_Edep_"+VolumeName+hNameE]->Fill(Einj,edep_w);
                mhVolumes["Einj_log_origin_Edep_"+VolumeName+hNameE]->Fill(TMath::Log10(Einj),edep_w);
            }
        }
    }

    // histos of particle properties
    for (Int_t j=0; j<(*MC)->GetNbEvents(); j++) { //loop on the number of particles in ONE beamon (edit: this most often useless, since usually in one beanon, we only have one particle which is generated. Useful however where several particle are generated in one beamon, as for example for radioactive decays...)
        MCEvent* theMCEvent = (*MC)->GetMCEvent(j);
        Particle* thePart = theMCEvent->GetPrimaryParticle();
        
        Einj = thePart->GetEinj(); // already in MeV
        PDG = thePart->GetPDG();
        FinalProcess = thePart->GetFinalProcess();
        path = (thePart->GetPosition(Particle::LastInteraction)-thePart->GetPosition(Particle::Injection)).Mag();
        
        //    mh["Einj"]->Fill(edep_convert_coeff*Einj,edep_w);
        //    mh["Einj_log"]->Fill(TMath::Log10(edep_convert_coeff*Einj),edep_w);
        mNbPrimaryPart[PDG]++;
        
        switch(PDG) {
            case -11: // positron
                for (UInt_t i=0; i<EnerGe::NbVolumes; i++){
                    if(VolMap[i].IsAnalyzed){
                        TString VolumeName = VolMap[i].Name;
                        Edep = edep_convert_coeff*theMCEvent->GetEdepVol(i);
                        if(Edep > minE && Edep < maxE){
                            //mhVolumes["Edep_positron_"+VolumeName]->Fill(Edep);
                        }
                    }
                }
                //mh["Einj_positron"]->Fill(Einj);
                //mh["path_positron"]->Fill(path);
                break;
            case 22: // gamma
                for (UInt_t i=0; i<EnerGe::NbVolumes; i++){
                    if(VolMap[i].IsAnalyzed){
                        TString VolumeName = VolMap[i].Name;
                        Edep = edep_convert_coeff*theMCEvent->GetEdepVol(i);
                        if(Edep > minE && Edep < maxE){
                            mhVolumes["Edep_gamma_"+VolumeName]->Fill(Edep,edep_w);
                        }
                    }
                }
                //mh["Einj_gamma"]->Fill(Einj);
                //mh["path_gamma"]->Fill(path);
                break;
            case 11: // electron
                for (UInt_t i=0; i<EnerGe::NbVolumes; i++){
                    if(VolMap[i].IsAnalyzed){
                        TString VolumeName = VolMap[i].Name;
                        Edep = edep_convert_coeff*theMCEvent->GetEdepVol(i);
                        if(Edep > minE && Edep < maxE){
                            mhVolumes["Edep_electron_"+VolumeName]->Fill(Edep,edep_w);
                        }
                    }
                }
                //mh["Einj_electron"]->Fill(Einj);
                //mh["path_electron"]->Fill(path);
                break;
                
                //            case 13: // mu -
                //                for (UInt_t i=0; i<EnerGe::NbVolumes; i++){
                //                    if(VolMap[i].IsAnalyzed){
                //                        TString VolumeName = VolMap[i].Name;
                //                        Edep = edep_convert_coeff*thePart->GetEdepVol(i);
                //                        if(Edep > minE && Edep < maxE){
                //                            mhVolumes["Edep_electron_"+VolumeName]->Fill(Edep);
                //                        }
                //                    }
                //                }
                //                //mh["Einj_electron"]->Fill(Einj);
                //                //mh["path_electron"]->Fill(path);
                //                break;
                
            case 2112: // neutron
                for (UInt_t i=0; i<EnerGe::NbVolumes; i++){
                    if(VolMap[i].IsAnalyzed){
                        TString VolumeName = VolMap[i].Name;
                        Edep = edep_convert_coeff*theMCEvent->GetEdepVol(i);
                        if(Edep > minE && Edep < maxE){
                            mhVolumes["Edep_neutron_"+VolumeName]->Fill(Edep,edep_w);
                        }
                    }
                }
                //   mh["Einj_neutron"]->Fill(Einj);
                //mh["path_neutron"]->Fill(path);
                if (mCapturingNuclei.count(FinalProcess) == 0) {
                    mCapturingNuclei[FinalProcess] = 1;
                } else { mCapturingNuclei[FinalProcess]++; }
                
                if ( FinalProcess > 2e9 ) { theNeutronFinalProcess = Capture; }
                else if ( FinalProcess > 1e9 ) { theNeutronFinalProcess = NucleusEmission; }
                else {
                    switch (FinalProcess) {
                        case 4111: theNeutronFinalProcess = Elastic; break;
                        case 4121: theNeutronFinalProcess = Inelastic; break;
                        case 4131: theNeutronFinalProcess = Capture; break;
                        case 4141: theNeutronFinalProcess = Fission; break;
                        case 4151: theNeutronFinalProcess = HadronAtRest; break;
                        case 4152: theNeutronFinalProcess = LeptonAtRest; break;
                        case 4161: theNeutronFinalProcess = ChargeExchange; break;
                        case 4210: theNeutronFinalProcess = RadioactiveDecay; break;
                        case 1091: theNeutronFinalProcess = Transportation1; break;
                        case  999: theNeutronFinalProcess = Transportation2; break;
                        default: theNeutronFinalProcess = Unknown;
                            bout << "\nUnknown neutron final process: "<< FinalProcess << std::endl;
                    }
                }
                vNeutronFinalProcess[theNeutronFinalProcess]++;
                break;
            default:;
        }
    }
}

TString EdepAlgo::NeutronProcessName(Int_t process) const
{
    switch(process) {
        case None: return TString("None"); break;
        case Unknown: return TString("Unknown"); break;
        case NucleusEmission: return TString("NucleusEmission"); break;
        case Elastic: return TString("Elastic"); break;
        case Inelastic: return TString("Inelastic"); break;
        case Capture: return TString("Capture"); break;
        case Fission: return TString("Fission"); break;
        case HadronAtRest: return TString("HadronAtRest"); break;
        case LeptonAtRest: return TString("LeptonAtRest"); break;
        case ChargeExchange: return TString("ChargeExchange"); break;
        case RadioactiveDecay: return TString("RadioactiveDecay"); break;
        case Transportation1: return TString("Transportation1"); break;
        case Transportation2: return TString("Transportation2"); break;
        default: throw(std::runtime_error(scat("EdepAlgo::NeutronProcessName: Unknown process: ",process)));
    }
}

template <class T>
void EdepAlgo::PrintNeutron(const EdepAlgo::map_type& m, T& OUT)
{
    OUT << " Neutrons shot: " << mNbPrimaryPart[2112] << "  with last reaction leading to following elements\n"
    <<"  (remember H=1, C=6, O=8, Cr=24, Mn=25, Fe=26, Co=27, Ni=28, Ba=56, Ce=58, Gd=64, W=74, Capture=4131, Decay=6201, DeferTrack=9100):\n";
    
    std::vector< pair_type > v(m.begin(),m.end());
    std::sort(v.begin(), v.end(), EdepAlgo::MappedComp);
    for (auto it=v.begin(), end=v.end(); it!=end; ++it) {
        OUT << "\t"<<it->first<<"\t"<<it->second<<" ("<<Double_t(it->second)/Double_t(mNbPrimaryPart[2112])*100.<<"%)\n";
    } OUT << std::endl;
}

void EdepAlgo::Finalise()
{
    bout << " Generator events number: " << NbEvtGenerator <<" ("<<Double_t(NbEvtGenerator)/Double_t(NbEvtTotal)*100.<<"%)"
    << "\n Defered events number: "    << NbEvtDefered   <<" ("<<Double_t(NbEvtDefered)/Double_t(NbEvtTotal)*100.<<"%)\n\n";
    if (NbUnordered>0ull) {
        bout << "*********************************************\n"
        << " => EdepAlgo: unordered events number: " << NbUnordered    <<" ("<<Double_t(NbUnordered)/Double_t(NbEvtTotal)*100.<<"%) <=\n"
        << "*********************************************\n\n";
    }
    bout << " Neutron final processes:\n";
    for (Int_t i=0; i<NbNeutronProcess; i++) {
        bout << "  "<<NeutronProcessName(i)<<": "<<vNeutronFinalProcess[i]<<std::endl;
    } bout << "\n";
    
    if (mNbPrimaryPart[2112] != 0) {
        PrintNeutron<decltype(write)>(mCapturingNuclei,write);
        
        map_type mCapturingElement;
        for (auto it=mCapturingNuclei.begin(), end=mCapturingNuclei.end(); it!=end; ++it) {
            if ( it->first < 1e9) { mCapturingElement.insert(*it); continue; }
            
            int el = it->first - 1e9;
            if (el > 1e9) { el -= 1e9; } // capture, not nucleus emission
            el = static_cast<int>(std::floor(el/1.e4));
            if (mCapturingElement.count(el) == 0) {
                mCapturingElement[el] = it->second;
            }
            else { mCapturingElement[el] += it->second; }
        }
        PrintNeutron<decltype(bout)>(mCapturingElement,bout);
    }
    
    //  TPad* thePad{};
    // Int_t iPad=0;
    try{
        //auto CanData = make_unique<TCanvas>("nCanData", "First study of simulation", 100,100, 800, 500);
        //CanData->Divide(4,2);
        
        //thePad = static_cast<TPad*>(CanData->cd(++iPad));
        //mh["Einj"]->DrawCopy();
        //mh["Einj_defered"]->SetLineColor(4);
        //mh["Einj_defered"]->DrawCopy("SAMES");
        //mh["Einj_prompt"]->SetLineColor(2);
        //mh["Einj_prompt"]->DrawCopy("SAMES");
        //thePad->BuildLegend();
        
        //thePad = static_cast<TPad*>(CanData->cd(++iPad));
        //mh["Edep"]->DrawCopy();
        
        //mh["Edep_defered"]->SetLineColor(4);
        //mh["Edep_defered"]->DrawCopy("SAMES");
        //mh["Edep_prompt"]->SetLineColor(2);
        //mh["Edep_prompt"]->DrawCopy("SAMES");
        //thePad->BuildLegend();
        
        //thePad = static_cast<TPad*>(CanData->cd(++iPad));
        //mh["Edep_defered"]->SetLineColor(4);
        //mh["Edep_defered"]->DrawCopy("SAMES");
        //mh["Edep_prompt"]->SetLineColor(2);
        //mh["Edep_prompt"]->DrawCopy("SAMES");
        //thePad->BuildLegend();
        
        //CanData->cd(++iPad);
        //mh2["Edep_Einj"]->DrawCopy("COLZ");
        
        //CanData->cd(++iPad);
        //mh2["TrueHits_Edep"]->DrawCopy("COLZ");
        
        //iPad=0;
        //auto CanMC = make_unique<TCanvas>("nCanMC", "Second study of simulation", 0,0, 1600, 1000);
        //CanMC->Divide(3,2);
        
        //thePad = static_cast<TPad*>(CanMC->cd(++iPad));
        //mh["Edep"]->DrawCopy();
        //mh["Edep_gamma"]->SetLineColor(4);
        //mh["Edep_gamma"]->DrawCopy("SAMES");
        //mh["Edep_neutron"]->SetLineColor(2);
        //mh["Edep_neutron"]->DrawCopy("SAMES");
        //mh["Edep_positron"]->SetLineColor(3);
        //mh["Edep_positron"]->DrawCopy("SAMES");
        //thePad->BuildLegend();
        
        //thePad = static_cast<TPad*>(CanMC->cd(++iPad));
        //mh["Einj"]->DrawCopy();
        //mh["Einj_gamma"]->SetLineColor(4);
        //mh["Einj_gamma"]->DrawCopy("SAMES");
        //mh["Einj_neutron"]->SetLineColor(2);
        //mh["Einj_neutron"]->DrawCopy("SAMES");
        //mh["Einj_positron"]->SetLineColor(3);
        //mh["Einj_positron"]->DrawCopy("SAMES");
        //thePad->BuildLegend();
        
        //thePad = static_cast<TPad*>(CanMC->cd(++iPad));
        //mh["path_gamma"]->SetLineColor(4);
        //mh["path_gamma"]->DrawCopy("SAMES");
        //mh["path_neutron"]->SetLineColor(2);
        //mh["path_neutron"]->DrawCopy("SAMES");
        //mh["path_positron"]->SetLineColor(3);
        //mh["path_positron"]->DrawCopy("SAMES");
        //thePad->BuildLegend();
        
        //thePad = static_cast<TPad*>(CanMC->cd(++iPad));
        //mh["TimeMC"]->SetLineColor(kBlue);
        //mh["TimeMC"]->DrawCopy();
        //mh["TimeREP"]->SetLineColor(kRed);
        //mh["TimeREP"]->DrawCopy("SAMES");
        //thePad->BuildLegend();
        
        AlgoDir->cd();
        
        for (UInt_t i=0; i<EnerGe::NbVolumes; i++){    //Save the differents Edep Histograms for the volumes
            if(VolMap[i].IsAnalyzed){
                TString VolumeName = VolMap[i].Name;
                AlgoDir->cd(VolMap[i].DirName+"/Edep");
                
                mhVolumes["Edep_"+VolumeName+hNameE]->GetYaxis()->SetTitle("Hits []");
                mhVolumes["Edep_"+VolumeName+hNameE]->Write();
                
                mhVolumes["Edep_log_"+VolumeName]->GetYaxis()->SetTitle("Hits []");
                mhVolumes["Edep_log_"+VolumeName]->Write();
                
                for (UInt_t p=0; p<EdepAlgo::NEdepRange; p++){
                    
                    TString EdepName = Form("0-%g%s",EdepRange[p].second,EdepRange[p].first.Data());
                    TString hName = Form("Edep_%s_%s",VolumeName.Data(),EdepName.Data());
                    mhVolumes[hName]->GetYaxis()->SetTitle("Hits []");
                    mhVolumes[hName]->Write();
                }
                
                //mhVolumes["Edep_electron_"+VolumeName]->Write();
                //mhVolumes["Edep_gamma_"+VolumeName]->Write();
                //mhVolumes["Edep_neutron_"+VolumeName]->Write();
                //mhVolumes["Edep_positron_"+VolumeName]->Write();
                
                AlgoDir->cd(VolMap[i].DirName+"/Einj");
                mhVolumes["Einj_origin_Edep_"+VolumeName+hNameE]->Write();
                mhVolumes["Einj_log_origin_Edep_"+VolumeName+hNameE]->Write();
                
                for (UInt_t p=0; p<EdepAlgo::NEdepRange; p++){
                    
                    TString EdepName = Form("0-%g%s",EdepRange[p].second,EdepRange[p].first.Data());
                    TString hName = Form("Einj_log_origin_Edep_%s_%s",VolumeName.Data(),EdepName.Data());
                    mhVolumes[hName]->GetYaxis()->SetTitle("Hits []");
                    mhVolumes[hName]->Write();
                }
                
                AlgoDir->cd();
            }
        }
        
        mh.clear();
        mhVolumes.clear();
        mh2.clear();
    }
    
    catch(...){
        std::cerr << "error in EdepAlgo::Finalise() Finalise \n" << std::endl;
    }
}

///////////////////////////////////////////////////////////////////////////////////
//////////////// Primaries analysis of simulation //////////////
//////////////////////////////////////////////////////////////////////////////////

void PrimariesAlgo::Initialise(TTree* tree)
{
    ReadParameterDB();
    
    //Initialising unit parameters
    SetConvertCoeff(convert_coef,unit_name,Algo::unit_prim);
    
    NbEvtTotal = tree->GetEntries();
    
    //set the histograms
    
    // 1D histograms for initial energy of primary particle
    AlgoDir->mkdir("Energy");
    mh["Einj"]  = make_unique<TH1D>("hEinj","Einj / evt",NbBinEinj,minEinj,maxEinj);
    mh["Einj"]->GetXaxis()->SetTitle("Energy ["+unit_name+"]");
    Double_t LogMinEinj = 1e-9*convert_coef;
    if(minEinj > 0.) LogMinEinj = TMath::Log10(minEinj);
    mh["Einj_log"] = make_unique<TH1D>("hEinj_log","Log(Einj) / evt",NbBinEinj,LogMinEinj,TMath::Log10(maxEinj));
    mh["Einj_log"]->GetXaxis()->SetTitle("Log_{10}(E/1"+unit_name+")");
    
    //1D histogram PDG code of the injected particles
    mh["PDG"] = make_unique<TH1D>("hPDG","PDG number / evt",80,-40,40); //can be extended to take into account the PDG codes of composite particles (baryon, mesons, etc...)
    mh["PDG"]->GetXaxis()->SetTitle("PDG code");
    
    //1D histogram for the spherical coordinate of the parimary particle "injection" point
    AlgoDir->mkdir("Position");
    Origin.SetX(Algo::Originx);
    Origin.SetY(Algo::Originy);
    Origin.SetZ(Algo::Originz);
    maxR = std::sqrt(std::pow((Algo::maxVx-Algo::Originx),2)+std::pow((Algo::maxVy-Algo::Originy),2)+std::pow((Algo::maxVz-Algo::Originz),2));
    mh["Rinj"] = make_unique<TH1D>("hRinj","Radius of injection point", Algo::NbBinR,0,maxR);
    mh["Rinj"]->GetXaxis()->SetTitle("Radius [mm]");
    mh["CosThetainj"] = make_unique<TH1D>("hThetainj","Cos(theta) angle of injection point", Algo::NbBinR,-1.,1.);
    mh["CosThetainj"]->GetXaxis()->SetTitle("Cos(\\theta)");
    mh["Phiinj"] = make_unique<TH1D>("hPhiinj","Azimuth angle of injection point", Algo::NbBinR,-2,2);
    mh["Phiinj"]->GetXaxis()->SetTitle("\\Phi/\\pi");
    
    //2D histogram
    mh2["EvsCosTheta"] = make_unique<TH2D>("hEvsCosTheta","Einj versus Cos(theta) / evt",NbBinEinj,minEinj,maxEinj,NbBinR,-1,1);
    mh2["EvsCosTheta"]->GetYaxis()->SetTitle("cos(\\theta)");
    mh2["EvsCosTheta"]->GetXaxis()->SetTitle("Energy ["+unit_name+"]");
    
    mh2["E_logvsCosTheta"] = make_unique<TH2D>("hE_logvsCosTheta","Log10(Einj) versus Cos(theta) / evt",NbBinEinj,LogMinEinj,TMath::Log10(maxEinj),NbBinR,-1,1);
    mh2["E_logvsCosTheta"]->GetYaxis()->SetTitle("cos(\\theta)");
    mh2["E_logvsCosTheta"]->GetXaxis()->SetTitle("Log_{10}(E/1"+unit_name+")");
    
    mh2["Vy_Vx"] = make_unique<TH2D>("nhVy_Vx","Vy vs Vx of injection vertex /evt", Algo::NbBinV,Algo::Originx+Algo::maxVx,Algo::Originx+Algo::maxVx, Algo::NbBinV,Algo::Originy+Algo::maxVy,Algo::Originy+Algo::maxVy);
    mh2["Vy_Vx"]->GetYaxis()->SetTitle("Vy [mm]");
    mh2["Vy_Vx"]->GetXaxis()->SetTitle("Vx [mm]");
    
    mh2["Vz_Vx"] = make_unique<TH2D>("nhVz_Vx","Vz vs Vx of injection vertex /evt", Algo::NbBinV,Algo::Originx+Algo::maxVx,Algo::Originx+Algo::maxVx, Algo::NbBinV,Algo::Originz+Algo::maxVz,Algo::Originz+Algo::maxVz);
    mh2["Vz_Vx"]->GetYaxis()->SetTitle("Vz [mm]");
    mh2["Vz_Vx"]->GetXaxis()->SetTitle("Vx [mm]");
    
    mh2["Vz_Vy"] = make_unique<TH2D>("nhVz_Vy","Vz vs Vy of injection vertex /evt", Algo::NbBinV,Algo::Originy+Algo::maxVy,Algo::Originy+Algo::maxVy, Algo::NbBinV,Algo::Originz+Algo::maxVz,Algo::Originz+Algo::maxVz);
    mh2["Vz_Vy"]->GetYaxis()->SetTitle("Vz [mm]");
    mh2["Vz_Vy"]->GetXaxis()->SetTitle("Vy [mm]");
    
    for (auto it=mh.begin(); it!=mh.end(); ++it) { it->second->GetYaxis()->SetTitle("Hits []"); }
}

void PrimariesAlgo::Process(ULong64_t , Int_t T)
{
    // multiple tree=run=file management
    if (CurrentTree != T ) {
        CurrentTree = T;
        UT=0.;
    } // reset storage at tree change
    
    defered=false;
    
    deltaUT = (*MC)->GetTime()-UT; // should be µs
    UT = (*MC)->GetTime(); // should be µs
    
    //counting the number of time unordered events
    if (deltaUT < 0.) {
        ++NbUnordered;
        bout << " *Warning PrimariesAlgo::Process: Unordered events, deltaUT<0.: "<<deltaUT<<std::endl;
        bout << "   Current event: primary type= "<<(*MC)->GetPrimaryType()<<" with particle number= "<<(*MC)->GetNbEvents()<<":";
        for (Int_t j=0; j<(*MC)->GetNbEvents(); j++) { bout << "\t" << (*MC)->GetMCEvent(j)->GetPrimaryParticle()->GetPDG(); }
        bout << std::endl;
    }
    
    //counting the number of 'defered' events
    if ( (*MC)->GetPrimaryType() == (Int_t)NewGene::defered_track_process
        or (*MC)->GetPrimaryType() == (Int_t)OldGene::defered_track_process) {
        NbEvtDefered ++;
        defered=true;
    } else { NbEvtGenerator++; }
    
    //Reweight events (for instance, if the events were generated with the atmospheric neutron generator from which energies are randomly generated from E x Phi(E))
    Double_t edep_w = 1;
    //SetEdepWeight((*MC)->GetPrimaryType(),(*MC)->GetEinj(),edep_w);
    //edep_w = 1;
    
    // histo of primary event properties
    for (Int_t j=0; j<(*MC)->GetNbEvents(); j++) { //loop on the number of particles in ONE beamon (edit: this most often useless, since usually in one beanon, we only have one particle which is generated. Useful however when several particle are generated in one beamon, as for example for radioactive decay generators...)
        MCEvent* theMCEvent = (*MC)->GetMCEvent(j);
        Particle* thePart = theMCEvent->GetPrimaryParticle();
        
        Einj = thePart->GetEinj(); // already in MeV
        PDG = thePart->GetPDG();
        
        mh["Einj"]->Fill(convert_coef*Einj,edep_w);
        mh["Einj_log"]->Fill(TMath::Log10(convert_coef*Einj),edep_w);
        mh["PDG"]->Fill(PDG,edep_w);
        
        R = (thePart->GetPosition(0)-Origin).Mag();
        CosTheta = (thePart->GetPosition(0)-Origin).CosTheta();
        mh["Rinj"]->Fill(R);
        mh["CosThetainj"]->Fill(CosTheta);
        Phi = 1 + (thePart->GetPosition(0)-Origin).Phi()/TMath::Pi(); //It seems root TVector 3 returns Phi angle between -pi and pi, and not between 0 and 2*pi
        mh["Phiinj"]->Fill(Phi);
        
        mh2["EvsCosTheta"]->Fill(convert_coef*Einj,CosTheta,edep_w);
        mh2["E_logvsCosTheta"]->Fill(TMath::Log10(convert_coef*Einj),CosTheta,edep_w);
        
        mh2["Vy_Vx"]->Fill(thePart->GetPosition(0).X(),thePart->GetPosition(0).Y());
        mh2["Vz_Vx"]->Fill(thePart->GetPosition(0).X(),thePart->GetPosition(0).Z());
        mh2["Vz_Vy"]->Fill(thePart->GetPosition(0).Y(),thePart->GetPosition(0).Z());
    }
}

void PrimariesAlgo::Finalise()
{
    if (NbUnordered>0ull) {
        bout << "*********************************************\n"
        << " => PrimariesAlgo: unordered events number: " << NbUnordered    <<" ("<<Double_t(NbUnordered)/Double_t(NbEvtTotal)*100.<<"%) <=\n"
        << "*********************************************\n\n";
    }
    
    AlgoDir->cd();
    
    try{
        
        mh["PDG"]->Write();
        
        AlgoDir->cd("Energy");
        mh["Einj"]->Write();
        mh["Einj_log"]->Write();
        
        AlgoDir->cd("Position");
        mh["Rinj"]->Write();
        mh["CosThetainj"]->Write();
        mh["Phiinj"]->Write();
        mh2["Vy_Vx"]->Write();
        mh2["Vz_Vx"]->Write();
        mh2["Vz_Vy"]->Write();
        
        //for (auto it=mh.begin(); it!=mh.end(); ++it) { it->second->Write(); }
        AlgoDir->cd();
        mh2["EvsCosTheta"]->Write();
        mh2["E_logvsCosTheta"]->Write();
        
        mh.clear();
        mh2.clear();
        
    }
    
    catch(...){
        std::cerr << "error in PrimariesAlgo::Finalise() \n" << std::endl;
    }
}

//////////////////////////////////////////////////////////////////////////////////////
//////////////// Secondaries analysis of simulation ////////////
/////////////////////////////////////////////////////////////////////////////////////

void SecondariesAlgo::Initialise(TTree* tree)
{
    ReadParameterDB();
    
    //Initialising unit parameters
    SetConvertCoeff(convert_coef,unit_name,Algo::unit_second);
    
    NbEvtTotal = tree->GetEntries();
    
    Double_t LogMinEinc = 1e-9*convert_coef;
    if(minEinc > 0.) LogMinEinc = TMath::Log10(minEinc);
    
    //energy of particles when they first get in the volume
    AlgoDir->mkdir("Electrons");
    mh["Einc_positron"]    = make_unique<TH1D>("nEinc_positron","positron / event",NbBinEinc,minEinc,maxEinc);
    mh_log["Einc_log_positron"] = make_unique<TH1D>("nEinc_log_positron","Log(E) / evt",NbBinEinc,LogMinEinc,TMath::Log10(maxEinc));
    mh["Einc_electron"]    = make_unique<TH1D>("nEinc_electron","electron / event",NbBinEinc,minEinc,maxEinc);
    mh_log["Einc_log_electron"] = make_unique<TH1D>("nEinc_log_electron","Log(E) / evt",NbBinEinc,LogMinEinc,TMath::Log10(maxEinc));
    AlgoDir->mkdir("Gammas");
    mh["Einc_gamma"]    = make_unique<TH1D>("nEinc_gamma","gamma / event",NbBinEinc,minEinc,maxEinc);
    mh_log["Einc_log_gamma"] = make_unique<TH1D>("nEinc_log_gamma","Log(E) / evt",NbBinEinc,LogMinEinc,TMath::Log10(maxEinc));
    AlgoDir->mkdir("Neutrons");
    mh["Einc_neutron"]    = make_unique<TH1D>("nEinc_neutron","neutron / event",NbBinEinc,minEinc,maxEinc);
    mh_log["Einc_log_neutron"] = make_unique<TH1D>("nEinc_log_neutron","Log(E) / evt",NbBinEinc,LogMinEinc,TMath::Log10(maxEinc));
    AlgoDir->mkdir("Protons");
    mh["Einc_proton"]  = make_unique<TH1D>("nEinc_proton","proton / event",NbBinEinc,minEinc,maxEinc);
    mh_log["Einc_log_proton"] = make_unique<TH1D>("nEinc_log_proton","Log(E) / evt",NbBinEinc,LogMinEinc,TMath::Log10(maxEinc));
    AlgoDir->mkdir("Muons");
    mh["Einc_muplus"]    = make_unique<TH1D>("nEinc_muplus","mu plus / event",NbBinEinc,minEinc,maxEinc);
    mh_log["Einc_log_muplus"] = make_unique<TH1D>("nEinc_log_muplus","Log(E) / evt",NbBinEinc,LogMinEinc,TMath::Log10(maxEinc));
    mh["Einc_muminus"]    = make_unique<TH1D>("nEinc_muminus","mu minus / event",NbBinEinc,minEinc,maxEinc);
    mh_log["Einc_log_muminus"] = make_unique<TH1D>("nEinc_log_muminus","Log(E) / evt",NbBinEinc,LogMinEinc,TMath::Log10(maxEinc));
    
    for (auto it=mh.begin(); it!=mh.end(); ++it) {
        it->second->GetYaxis()->SetTitle("Hits []");
        it->second->GetXaxis()->SetTitle("Energy ["+unit_name+"]");
    }
    
    for (auto it=mh_log.begin(); it!=mh_log.end(); ++it) {
        it->second->GetYaxis()->SetTitle("Hits []");
        it->second->GetXaxis()->SetTitle("Log(E/1"+unit_name+")");
    }
    
}

void SecondariesAlgo::Process(ULong64_t , Int_t T)
{
    // multiple tree=run=file management
    if (CurrentTree != T ) {
        CurrentTree = T;
        UT=0.;
    } // reset storage at tree change
    
    defered=false;
    
    deltaUT = (*MC)->GetTime()-UT; // should be µs
    UT = (*MC)->GetTime(); // should be µs
    
    //counting the number of time unordered events
    if (deltaUT < 0.) {
        ++NbUnordered;
        bout << " *Warning SecondariesAlgo::Process: Unordered events, deltaUT<0.: "<<deltaUT<<std::endl;
        bout << "   Current event: primary type= "<<(*MC)->GetPrimaryType()<<" with particle number= "<<(*MC)->GetNbEvents()<<":";
        for (Int_t j=0; j<(*MC)->GetNbEvents(); j++) { bout << "\t" << (*MC)->GetMCEvent(j)->GetPrimaryParticle()->GetPDG(); }
        bout << std::endl;
    }
    
    //counting the number of 'defered' events
    if ( (*MC)->GetPrimaryType() == (Int_t)NewGene::defered_track_process
        or (*MC)->GetPrimaryType() == (Int_t)OldGene::defered_track_process) {
        NbEvtDefered ++;
        defered=true;
    } else { NbEvtGenerator++; }
    
    //Reweight events (for instance, if the events were generated with the atmospheric neutron generator from which energies are randomly genrated from E x Phi(E))
    //Reweight events (for instance, if the events were generated with the atmospheric neutron generator from which energies are randomly generated from E x Phi(E))
    Double_t edep_w = 1;
    //SetEdepWeight((*MC)->GetPrimaryType(),(*MC)->GetEinj(),edep_w);
    //  Double_t edep_w = 0;
    //  if ((*MC)->GetPrimaryType() == (Int_t) NewGene::Plane_AtmNeutron || (*MC)->GetPrimaryType() == (Int_t) NewGene::Sphere_AtmNeutron) {edep_w = 1/((*MC)->GetEinj());}
    //  else {edep_w = 1;}
    
    // histo of secondary event properties
    for (Int_t j=0; j<(*MC)->GetNbEvents(); j++){
        MCEvent * myMCEvent = (*MC)->GetMCEvent(j);
        Int_t NSec = myMCEvent->GetSecondaryParticleTCA()->GetEntriesFast();
        
        for (Int_t k=0; k<NSec; k++){
            
            Particle *mySecPart = myMCEvent->GetSecondaryParticle(k);
            Int_t PDG = mySecPart->GetPDG();
            Einj = mySecPart->GetEinj()*convert_coef;
            
            switch(PDG) {
                case -11: // positron
                    if(Einj > minEinc && Einj < maxEinc){
                        mh["Einc_positron"]->Fill(Einj,edep_w);
                        mh_log["Einc_log_positron"]->Fill(TMath::Log10(Einj),edep_w);
                    }
                    
                    break;
                case 22: // gamma
                    if(Einj > minEinc && Einj < maxEinc){
                        mh["Einc_gamma"]->Fill(Einj,edep_w);
                        mh_log["Einc_log_gamma"]->Fill(TMath::Log10(Einj),edep_w);
                    }
                    break;
                case 11: // electron
                    if(Einj > minEinc && Einj < maxEinc){
                        mh["Einc_electron"]->Fill(Einj,edep_w);
                        mh_log["Einc_log_electron"]->Fill(TMath::Log10(Einj),edep_w);
                    }
                    break;
                    
                case 2112: // neutron
                    if(Einj > minEinc && Einj < maxEinc){
                        mh["Einc_neutron"]->Fill(Einj,edep_w);
                        mh_log["Einc_log_neutron"]->Fill(TMath::Log10(Einj),edep_w);
                    }
                    break;
                    
                case 2212: // proton
                    if(Einj > minEinc && Einj < maxEinc){
                        mh["Einc_proton"]->Fill(Einj,edep_w);
                        mh_log["Einc_log_proton"]->Fill(TMath::Log10(Einj),edep_w);
                    }
                    break;
                    
                case 13: //mu-
                    if(Einj > minEinc && Einj < maxEinc){
                        mh["Einc_muminus"]->Fill(Einj,edep_w);
                        mh_log["Einc_log_muminus"]->Fill(TMath::Log10(Einj),edep_w);
                    }
                    break;
                case -13: //mu+
                    if(Einj > minEinc && Einj < maxEinc){
                        mh["Einc_muplus"]->Fill(Einj,edep_w);
                        mh_log["Einc_log_muplus"]->Fill(TMath::Log10(Einj),edep_w);
                    }
                    break;
            }
        }
    }
}

void SecondariesAlgo::Finalise()
{
    if (NbUnordered>0ull) {
        bout << "*********************************************\n"
        << " => SecondariesAlgo: unordered events number: " << NbUnordered    <<" ("<<Double_t(NbUnordered)/Double_t(NbEvtTotal)*100.<<"%) <=\n"
        << "*********************************************\n\n";
    }
    
    try{
        
        TPaveText *myText=new TPaveText(0.8,0.96,1,1,"NB NDC");
        myText->SetTextColor(kBlack);
        myText->SetTextSize(0.03);
        //myText->AddText("Einj = " + InjectedEnergy);
        const char * sNbEvtTot = "";
        std::ostringstream convertNb;
        convertNb << NbEvtTot;
        sNbEvtTot = (convertNb.str()).c_str();
        TString TsNbEvtTot = sNbEvtTot;
        myText->AddText("Nb Events : " + TsNbEvtTot);
        myText->Draw("SAMES");
        
        
        AlgoDir->cd("Electrons");
        //mh["Einc_positron"]->GetYaxis()->SetTitle("Hits []");
        mh["Einc_positron"]->Write();
        mh_log["Einc_log_positron"]->Write();
        
        //mh["Einc_electron"]->GetYaxis()->SetTitle("Hits []");
        mh["Einc_electron"]->Write();
        mh_log["Einc_log_electron"]->Write();
        
        AlgoDir->cd();
        
        AlgoDir->cd("Gammas");
        //mh["Einc_gamma"]->GetYaxis()->SetTitle("Hits []");
        mh["Einc_gamma"]->Write();
        //mh["Einc_log_gamma"]->GetYaxis()->SetTitle("Hits []");
        mh_log["Einc_log_gamma"]->Write();
        AlgoDir->cd();
        
        AlgoDir->cd("Neutrons");
        //mh["Einc_neutron"]->GetYaxis()->SetTitle("Hits []");
        mh["Einc_neutron"]->Write();
        //mh["Einc_log_neutron"]->GetYaxis()->SetTitle("Hits []");
        mh_log["Einc_log_neutron"]->Write();
        AlgoDir->cd();
        
        AlgoDir->cd("Protons");
        mh["Einc_proton"]->Write();
        mh_log["Einc_log_proton"]->Write();
        AlgoDir->cd();
        
        AlgoDir->cd("Muons");
        //mh["Einc_muminus"]->GetYaxis()->SetTitle("Hits []");
        mh["Einc_muminus"]->Write();
        mh_log["Einc_log_muminus"]->Write();
        
        //mh["Einc_muplus"]->GetYaxis()->SetTitle("Hits []");
        mh["Einc_muplus"]->Write();
        mh_log["Einc_log_muplus"]->Write();
        AlgoDir->cd();
        
        mh.clear();
    }
    
    catch(...){
        std::cerr << "error in SecondariesAlgo::Finalise() \n" << std::endl;
    }
}

////////////////////////////////////////////////////////////////////////////////
//////////////// Orsay OV coincidencies  ////////////////
////////////////////////////////////////////////////////////////////////////////

void OrsayOVAlgo::Initialise(TTree *tree)
{
    ReadParameterDB();
    
    SetOrsayOVCrystalIndex();
    
    //Initialising unit parameters for energy deposition
    SetConvertCoeff(edep_convert_coeff,edep_unit_name,Algo::unit_edep);
    
    //Initialising unit parameters for energy injection
    SetConvertCoeff(einj_convert_coeff,einj_unit_name,Algo::unit_prim);
    
    NbEvtTotal = tree->GetEntries();
    NbEvtTot = tree->GetEntries(); //NbEvtTot is a double : needed to scale the histograms
    
    eventGenerator = new TRandom();
    AlgoDir->mkdir("Coincidences");
        
    //Triggered events in each crystal volume
    TString EdepName, hName;
    
    for (UInt_t i=0; i<EnerGe::NbOrsayOVCrystals; i++){
        TString VolumeName = VolumeNameList[OrsayOVCrysIndex[i]];
        AlgoDir->mkdir("Triggered/"+VolumeName+"/Edep");
    
        TString hEdepName="Edep_", Xlegend = "E_{dep}";
        
        //"Permanent" energy deposition histograms (different energy ranges in lin scale)
        for (UInt_t p=3; p<OrsayOVAlgo::NEdepRange; p++){
            //Triggered
            EdepName = Form("0-%g%s",EdepRange[p].second,EdepRange[p].first.Data());
            hName = Form(hEdepName+"triggered_%s_%s",VolumeName.Data(),EdepName.Data());
            mhVolumes[hName] = make_unique<TH1D>("n"+hName,"Edep / "+VolumeName,NbBinE,0,EdepRange[p].second); //units here in MeV !!
            mhVolumes[hName]->GetXaxis()->SetTitle(Xlegend+Form(" [%s]",EdepRange[p].first.Data()));
        }
        
        if (ApplyResol){
            AlgoDir->mkdir("Triggered/"+VolumeName+"/Evis");
            
            TString hvisName = "Evis_";
            TString Xlegend_vis = "E_{vis}";
            
            for (UInt_t p=3; p<OrsayOVAlgo::NEdepRange; p++){
                EdepName = Form("0-%g%s",EdepRange[p].second,EdepRange[p].first.Data());
                hName = Form(hvisName+"triggered_%s_%s",VolumeName.Data(),EdepName.Data());
                mhVolumes[hName] = make_unique<TH1D>("n"+hName,"Edep / "+VolumeName,NbBinE,0,EdepRange[p].second); //units here in MeV !!
                mhVolumes[hName]->GetXaxis()->SetTitle(Xlegend_vis+Form(" [%s]",EdepRange[p].first.Data()));
            }
        }
    }
//
//    //Position of the primary particles "injection" point
//    AlgoDir->mkdir("Triggered/Position");
//
//    Origin.SetX(Algo::Originx);
//    Origin.SetY(Algo::Originy);
//    Origin.SetZ(Algo::Originz);
//
//    maxR = std::sqrt(std::pow((Algo::maxVx-Algo::Originx),2)+std::pow((Algo::maxVy-Algo::Originy),2)+std::pow((Algo::maxVz-Algo::Originz),2));
//
//    //2D histograms
//    mh2["Vy_Vx_triggered"] = make_unique<TH2D>("nhVy_Vx_triggered","Vy vs Vx of injection vertex /evt", Algo::NbBinV, -Algo::maxVx, Algo::maxVx, Algo::NbBinV, -Algo::maxVy, Algo::maxVy);
//    mh2["Vy_Vx_triggered"]->GetYaxis()->SetTitle("Vy [mm]");
//    mh2["Vy_Vx_triggered"]->GetXaxis()->SetTitle("Vx [mm]");
//
//    mh2["Vz_Vx_triggered"] = make_unique<TH2D>("nhVz_Vx_triggered","Vz vs Vx of injection vertex /evt", Algo::NbBinV,-Algo::maxVx,Algo::maxVx, Algo::NbBinV,-Algo::maxVz,Algo::maxVz);
//    mh2["Vz_Vx_triggered"]->GetYaxis()->SetTitle("Vz [mm]");
//    mh2["Vz_Vx_triggered"]->GetXaxis()->SetTitle("Vx [mm]");
//
//    mh2["Vz_Vy_triggered"] = make_unique<TH2D>("nhVz_Vy_triggered","Vz vs Vy of injection vertex /evt", Algo::NbBinV,-Algo::maxVy,Algo::maxVy, Algo::NbBinV,-Algo::maxVz,Algo::maxVz);
//    mh2["Vz_Vy_triggered"]->GetYaxis()->SetTitle("Vz [mm]");
//    mh2["Vz_Vy_triggered"]->GetXaxis()->SetTitle("Vy [mm]");
    
    //Coincidences
    for (int c =0; c<OrsayOVAlgo::NCoincs; c++){
        
        TString coinc_name = Coinc_names[c];
        AlgoDir->mkdir("Coincidences/"+coinc_name);
        
        for (UInt_t i=0; i<EnerGe::NbOrsayOVCrystals; i++){
            TString VolumeName = VolumeNameList[OrsayOVCrysIndex[i]];
            AlgoDir->mkdir("Coincidences/"+coinc_name+"/"+VolumeName+"/Edep");
        
            TString hEdepName="Edep_", Xlegend = "E_{dep}";
            
            //"Permanent" energy deposition histograms (different energy ranges in lin scale)
            for (UInt_t p=3; p<OrsayOVAlgo::NEdepRange; p++){
                //Coincidencies
                EdepName = Form("0-%g%s",EdepRange[p].second,EdepRange[p].first.Data());
                hName = Form(hEdepName+"%s_%s_%s",coinc_name.Data(), VolumeName.Data(), EdepName.Data());
                mhVolumes[hName] = make_unique<TH1D>("n"+hName,"Edep / "+coinc_name+" "+VolumeName,NbBinE,0,EdepRange[p].second); //units here in MeV !!
                mhVolumes[hName]->GetXaxis()->SetTitle(Form("E_{dep} [%s]",EdepRange[p].first.Data()));
            }
            
            if (ApplyResol){
                AlgoDir->mkdir("Coincidences/"+coinc_name+"/"+VolumeName+"/Evis");
                
                TString hvisName = "Evis_";
                TString Xlegend_vis = "E_{vis}";
                
                for (UInt_t p=3; p<OrsayOVAlgo::NEdepRange; p++){
                    EdepName = Form("0-%g%s",EdepRange[p].second,EdepRange[p].first.Data());
                    hName = Form(hvisName+"%s_%s_%s",coinc_name.Data(), VolumeName.Data(),EdepName.Data());
                    mhVolumes[hName] = make_unique<TH1D>("n"+hName,"Evis / "+coinc_name+" "+VolumeName,NbBinE,0,EdepRange[p].second); //units here in MeV !!
                    mhVolumes[hName]->GetXaxis()->SetTitle(Xlegend_vis+Form(" [%s]",EdepRange[p].first.Data()));
                }
            }
        }
//
//        AlgoDir->mkdir("Coincidences/"+coinc_name+"/Position");
//        //Position histograms
//        mh2["Vy_Vx_"+coinc_name] = make_unique<TH2D>("nhVy_Vx_"+coinc_name,"Vy vs Vx of injection vertex /evt", Algo::NbBinV, -Algo::maxVx, Algo::maxVx, Algo::NbBinV, -Algo::maxVy, Algo::maxVy);
//        mh2["Vy_Vx_"+coinc_name]->GetYaxis()->SetTitle("Vy [mm]");
//        mh2["Vy_Vx_"+coinc_name]->GetXaxis()->SetTitle("Vx [mm]");
//
//        mh2["Vz_Vx_"+coinc_name] = make_unique<TH2D>("nhVz_Vx_"+coinc_name,"Vz vs Vx of injection vertex /evt", Algo::NbBinV,-Algo::maxVx,Algo::maxVx, Algo::NbBinV,-Algo::maxVz,Algo::maxVz);
//        mh2["Vz_Vx_"+coinc_name]->GetYaxis()->SetTitle("Vz [mm]");
//        mh2["Vz_Vx_"+coinc_name]->GetXaxis()->SetTitle("Vx [mm]");
//
//        mh2["Vz_Vy_"+coinc_name] = make_unique<TH2D>("nhVz_Vy_"+coinc_name,"Vz vs Vy of injection vertex /evt", Algo::NbBinV,-Algo::maxVy,Algo::maxVy, Algo::NbBinV,-Algo::maxVz,Algo::maxVz);
//        mh2["Vz_Vy_"+coinc_name]->GetYaxis()->SetTitle("Vz [mm]");
//        mh2["Vz_Vy_"+coinc_name]->GetXaxis()->SetTitle("Vy [mm]");
        
    }
    for (auto it=mhVolumes.begin(); it!=mhVolumes.end(); ++it) { it->second->GetYaxis()->SetTitle("Hits []");}
}

void OrsayOVAlgo::Process(ULong64_t , Int_t T)
{
    // multiple tree=run=file management
    if (CurrentTree != T ) {
        CurrentTree = T;
        UT=0.;
    } // reset storage at tree change
    
    defered=false;
    
    Einj = einj_convert_coeff*((*MC)->GetEinj()); // always kinetic energy, originally in MeV
    deltaUT = (*MC)->GetTime()-UT; // should be µs
    UT = (*MC)->GetTime(); // should be µs
    
    if (deltaUT < 0.) {
        ++NbUnordered;
        bout << " *Warning EdepAlgo::Process: Unordered events, deltaUT<0.: "<<deltaUT<<std::endl;
        bout << "   Current event: primary type= "<<(*MC)->GetPrimaryType()<<" with particle number= "<<(*MC)->GetNbEvents()<<":";
        for (Int_t j=0; j<(*MC)->GetNbEvents(); j++) { bout << "\t" << (*MC)->GetMCEvent(j)->GetPrimaryParticle()->GetPDG(); }
        bout << std::endl;
    }
    
    if ( (*MC)->GetPrimaryType() == (Int_t)NewGene::defered_track_process
        or (*MC)->GetPrimaryType() == (Int_t)OldGene::defered_track_process) {
        NbEvtDefered ++;
        defered=true;
    } else { NbEvtGenerator++; }
    
    //Checking veto
    Int_t HitCode = 0;
    
    for (UInt_t i=0; i<EnerGe::NbOrsayOVCrystals; i++){
        Edep = (*MC)->GetEdepVol(OrsayOVCrysIndex[i]);
        if (Edep > OrsayOVCrysTh[i] ) {
            HitCode = HitCode + (Int_t) std::pow(10,2-i);
        }
    }
    
    TString coinc_name = Savein(HitCode);
    
    for (UInt_t i=0; i<EnerGe::NbOrsayOVCrystals; i++){
        
        Edep = edep_convert_coeff*(*MC)->GetEdepVol(OrsayOVCrysIndex[i]);
        TString VolumeName = VolumeNameList[OrsayOVCrysIndex[i]];
        TString EdepName="Edep_";
        TString EvisName="Evis_";
        
        Edep*= 1./(edep_convert_coeff); // Repassage en MeV
        
        if (ApplyResol){
            //if (Edep>0){std::cout<<"Edep = "<<Edep<<std::endl;}
            if (Edep>0){Evis = eventGenerator->Gaus(Edep, sqrt(C_resol[i]*Edep*Edep+B_resol[i]+A_resol[i]*Edep));}
            else{Evis=0;}
            //std::cout<<Edep<<" - " << Evis << " = "<<Edep-Evis<<std::endl;
            //if (Edep>0){std::cout<<"Evis = "<<Edep<<std::endl;}
        }
        
        FillHistograms(EdepName+"triggered_", Edep, VolumeName);
        if(ApplyResol&&(Evis>0)){FillHistograms(EvisName+"triggered_", Evis, VolumeName);}
        
        if (coinc_name!="false"){
            FillHistograms(EdepName+coinc_name+"_", Edep, VolumeName);
            if(ApplyResol&&(Evis>0)){ FillHistograms(EvisName+coinc_name+"_", Evis, VolumeName);}
        }
        
//        MCEvent* theMCEvent = (*MC)->GetMCEvent(0);
//        Particle* thePart = theMCEvent->GetPrimaryParticle();
        
//        if (Edep>0){
//            mh2["Vy_Vx_triggered"]->Fill(thePart->GetPosition(0).X(),thePart->GetPosition(0).Y());
//            mh2["Vz_Vx_triggered"]->Fill(thePart->GetPosition(0).X(),thePart->GetPosition(0).Z());
//            mh2["Vz_Vy_triggered"]->Fill(thePart->GetPosition(0).Y(),thePart->GetPosition(0).Z());
//        }
//
//        if (Edep>0){
//            mh2["Vy_Vx_"+coinc_name]->Fill(thePart->GetPosition(0).X(),thePart->GetPosition(0).Y());
//            mh2["Vz_Vx_"+coinc_name]->Fill(thePart->GetPosition(0).X(),thePart->GetPosition(0).Z());
//            mh2["Vz_Vy_"+coinc_name]->Fill(thePart->GetPosition(0).Y(),thePart->GetPosition(0).Z());
//        }
    }
}
    
void OrsayOVAlgo::Finalise(){
    
    bout << " Generator events number: " << NbEvtGenerator <<" ("<<Double_t(NbEvtGenerator)/Double_t(NbEvtTotal)*100.<<"%)"
    << "\n Defered events number: "    << NbEvtDefered   <<" ("<<Double_t(NbEvtDefered)/Double_t(NbEvtTotal)*100.<<"%)\n\n";
    if (NbUnordered>0ull) {
        bout << "*********************************************\n"
        << " => OrsayOVAlgo: unordered events number: " << NbUnordered    <<" ("<<Double_t(NbUnordered)/Double_t(NbEvtTotal)*100.<<"%) <=\n"
        << "*********************************************\n\n";
    }
    
    TString EdepName, hName;
    
    try{
        AlgoDir->cd();
        
        for (UInt_t i=0; i<EnerGe::NbOrsayOVCrystals; i++){    //Save the differents Edep Histograms for the volumes
            TString VolumeName = VolumeNameList[OrsayOVCrysIndex[i]];
            TString hEdepName="Edep_";
            
            AlgoDir->cd();
            AlgoDir->cd("Triggered/"+VolumeName+"/Edep");
            
            for (UInt_t p=3; p<EdepAlgo::NEdepRange; p++){
                
                TString EdepName = Form("0-%g%s",EdepRange[p].second,EdepRange[p].first.Data());
                TString hName = Form(hEdepName+"triggered_%s_%s",VolumeName.Data(),EdepName.Data());
                mhVolumes[hName]->GetYaxis()->SetTitle("Hits []");
                mhVolumes[hName]->Write();
            }
                
            if (ApplyResol){
                TString hEvisName = "Evis_";
                
                AlgoDir->cd();
                AlgoDir->cd("Triggered/"+VolumeName+"/Evis");
                
                for (UInt_t p=3; p<EdepAlgo::NEdepRange; p++){
                    
                    TString EdepName = Form("0-%g%s",EdepRange[p].second,EdepRange[p].first.Data());
                    TString hName = Form(hEvisName+"triggered_%s_%s",VolumeName.Data(),EdepName.Data());
                    mhVolumes[hName]->GetYaxis()->SetTitle("Hits []");
                    mhVolumes[hName]->Write();
                }
                
            }
        }
        
//        AlgoDir->cd();
//        AlgoDir->cd("Triggered/Position/");
//        mh2["Vy_Vx_triggered"]->Write();
//        mh2["Vz_Vx_triggered"]->Write();
//        mh2["Vz_Vy_triggered"]->Write();
        
        AlgoDir->cd();
        
        //Coincidences
        for (int c =0; c < OrsayOVAlgo::NCoincs; c++){
            
            TString coinc_name = Coinc_names[c];
            
            for (UInt_t i=0; i<EnerGe::NbOrsayOVCrystals; i++){
                TString VolumeName = VolumeNameList[OrsayOVCrysIndex[i]];
                AlgoDir->cd();
                AlgoDir->cd("Coincidences/"+coinc_name+"/"+VolumeName+"/Edep");
                for (UInt_t p=3; p<OrsayOVAlgo::NEdepRange; p++){
                    //Coincidencies
                    TString EdepName = Form("0-%g%s",EdepRange[p].second,EdepRange[p].first.Data());
                    hName = Form("Edep_%s_%s_%s",coinc_name.Data(), VolumeName.Data(), EdepName.Data());
                    mhVolumes[hName]->Write();
                }
                
                if (ApplyResol){
                    AlgoDir->cd();
                    AlgoDir->cd("Coincidences/"+coinc_name+"/"+VolumeName+"/Evis");
                    
                    TString hvisName = "Evis_";
                    TString Xlegend_vis = "E_{vis}";
                    
                    for (UInt_t p=3; p<OrsayOVAlgo::NEdepRange; p++){
                        TString EdepName = Form("0-%g%s",EdepRange[p].second,EdepRange[p].first.Data());
                        hName = Form(hvisName+"%s_%s_%s",coinc_name.Data(), VolumeName.Data(),EdepName.Data());
                        mhVolumes[hName]->Write();
                    }
                }
            }
        }
    }
    
    catch(...){
        std::cerr << "error in OrsayOVAlgo::Finalise() Finalise \n" << std::endl;
    }
}

//Bool_t OrsayOVAlgo::IsAccepted(Int_t HitCode){
//    Bool_t Value = false;
//
//    Int_t AnalysisCode = 10*static_cast<Int_t>(VetoFlag[0])+static_cast<Int_t>(VetoFlag[1]); //Mid LWO is the crystal of interest: never vetoed.
//
//    //std::cout << "OrsayOVAlgo::IsAccepted() - Analysis code: " << AnalysisCode << std::endl;
//
//    Int_t IsTopGe = (Int_t) HitCode/100.;
//    Int_t IsMidLWO = (Int_t) (HitCode-IsTopGe*100)/10.;
//    Int_t IsBotGe = (Int_t) HitCode-IsTopGe*100-IsMidLWO*10;
//
//    switch (AnalysisCode) {
//        case 0:
//            Value = true;
//            break;
//
//        case 1:
//            if(IsBotGe == 0) Value=true;
//            break;
//
//        case 10:
//            if(IsTopGe == 0) Value=true;
//            break;
//
//        case 11:
//            if(IsTopGe==0 && IsBotGe==0) Value=true;
//            break;
//
//        default:
//            Value=true;
//            break;
//    }
//
//    return Value;
//}

TString OrsayOVAlgo::Savein(Int_t HitCode){
    TString Namehisto = "false";
    
    switch (HitCode) {
        case 1:
            Namehisto = "coinc_Bot_only";
            break;
            
        case 10:
            Namehisto = "coinc_Mid_only";
            break;
            
        case 11:
            Namehisto = "coinc_Mid_&_Bot";
            break;
            
        case 100:
            Namehisto = "coinc_Top_only";
            break;
            
        case 101:
            Namehisto = "coinc_Top_&_Bot";
            break;
            
        case 110:
            Namehisto = "coinc_Top_&_Mid";
            break;
            
        case 111:
            Namehisto = "coinc_Top_&_Mid_&_Bot";
            break;
            
        default:
            break;
    }
    
    return Namehisto;
}

void OrsayOVAlgo::FillHistograms(TString BaseName, float Energy, TString VolumeName){
    
    
    if(Energy*1000 > 0. && Energy*1000 < 100.){
        mhVolumes[BaseName+VolumeName+"_0-100keV"]->Fill(Energy*1000);
    }
    
    if(Energy > 0. && Energy < 1.){
        mhVolumes[BaseName+VolumeName+"_0-1MeV"]->Fill(Energy);
    }
    
    if(Energy > 0. && Energy < 10.){
        mhVolumes[BaseName+VolumeName+"_0-10MeV"]->Fill(Energy);
    }
    
    if(Energy > 0. && Energy < 100.){
        mhVolumes[BaseName+VolumeName+"_0-100MeV"]->Fill(Energy);
    }
    
    if(Energy/1000. > 0. && Energy/1000. < 1.){
        mhVolumes[BaseName+VolumeName+"_0-1GeV"]->Fill(Energy/1000.);
    }
}

/////////////////////////////////////////////////////////////////////////
//////////////////////// Vertex study //////////////////////////
////////////////////////////////////////////////////////////////////////

void VertexAlgo::DrawAndWrite(TVirtualPad* pad, const TH1& h, const TString& Option, Bool_t log)
{
    if (log) { pad->cd()->SetLogy(); }
    else { pad->cd(); }
    h.DrawCopy(Option);
    h.Write();
}

////////////////////////////////////////////////////////////////

VertexAlgo::VertexDisplay::VertexDisplay(const TString& name, TDirectory* algodir, const TVector3& vOrigin,
                                         Double_t minX, Double_t maxX, Double_t minY, Double_t maxY, Double_t minZ, Double_t maxZ, Double_t minR, Double_t maxR): theAlgoDir(algodir), Name(name), v0(vOrigin)
{
    theAlgoDir->mkdir(Name);
    hVy_Vx = make_unique<TH2D>("nhVy_Vx"+Name,"Vy vs Vx of "+Name+" vertex /evt", Algo::NbBinV,minX,maxX, Algo::NbBinV,minY,maxY);
    hVz_Vx = make_unique<TH2D>("nhVz_Vx"+Name,"Vz vs Vx of "+Name+" vertex /evt", Algo::NbBinV,minX,maxX, Algo::NbBinV,minZ,maxZ);
    hVr = make_unique<TH1D>("nhVr"+Name,"Vr of "+Name+" vertex /evt", Algo::NbBinR,minR,maxR);
    hVtheta = make_unique<TH1D>("nhVtheta"+Name,"Vtheta of "+Name+" vertex /evt", Algo::NbBinR,-1.,1.);
    SetAxis();
}

VertexAlgo::VertexDisplay::VertexDisplay(const TString& name, TDirectory* algodir, const TVector3& vOrigin): theAlgoDir(algodir), Name(name), v0(vOrigin)
{
    theAlgoDir->mkdir(Name);
    hVy_Vx = make_unique<TH2D>("nhVy_Vx"+Name,"Vy vs Vx of "+Name+" vertex /evt", Algo::NbBinV,Algo::minVx,Algo::maxVx, Algo::NbBinV,Algo::minVy,Algo::maxVy);
    hVz_Vx = make_unique<TH2D>("nhVz_Vx"+Name,"Vz vs Vx of "+Name+" vertex /evt", Algo::NbBinV,Algo::minVx,Algo::maxVx, Algo::NbBinV,Algo::minVz,Algo::maxVz);
    hVr = make_unique<TH1D>("nhVr"+Name,"Vr of "+Name+" vertex /evt", Algo::NbBinR,Algo::minVr,Algo::maxVr);
    hVtheta = make_unique<TH1D>("nhVtheta"+Name,"Vtheta of "+Name+" vertex /evt", Algo::NbBinR,-1.,1.);
    SetAxis();
}

void VertexAlgo::VertexDisplay::SetAxis()
{
    hVy_Vx->GetXaxis()->SetTitle("Vx [mm]");
    hVy_Vx->GetYaxis()->SetTitle("Vy [mm]");
    hVz_Vx->GetXaxis()->SetTitle("Vx [mm]");
    hVz_Vx->GetYaxis()->SetTitle("Vz [mm]");
    hVr->GetXaxis()->SetTitle("R [mm]");
    hVtheta->GetXaxis()->SetTitle("cos(#theta)");
    theAlgoDir->cd();
}

void VertexAlgo::VertexDisplay::Fill(const TVector3& v)
{
    hVy_Vx->Fill((v-v0).x(),(v-v0).y());
    hVz_Vx->Fill((v-v0).x(),(v-v0).z());
    hVr->Fill((v-v0).Mag());
    hVtheta->Fill((v-v0).z()/(v-v0).Mag());
}

void VertexAlgo::VertexDisplay::DrawAndWrite() const
{
    theAlgoDir->cd(Name);
    UInt_t pad = 0u;
    auto CanVertex = make_unique<TCanvas>("nCanVertex_"+Name, "Vertex distribution of "+Name, 0,0, 1200, 600);
    CanVertex->Divide(2,2);
    VertexAlgo::DrawAndWrite(CanVertex->cd(++pad),*hVy_Vx,"COLZ");
    VertexAlgo::DrawAndWrite(CanVertex->cd(++pad),*hVz_Vx,"COLZ");
    VertexAlgo::DrawAndWrite(CanVertex->cd(++pad),*hVr,"");
    VertexAlgo::DrawAndWrite(CanVertex->cd(++pad),*hVtheta,"");
    CanVertex->Write();
    theAlgoDir->cd();
}

void VertexAlgo::Initialise(TTree* )
{
    ReadParameterDB();
    
    Origin.SetX(Algo::Originx);
    Origin.SetY(Algo::Originy);
    Origin.SetZ(Algo::Originz);
    
    //vertex display objects for primary particles
    for (UInt_t i=0; i<vName.size(); i++) {
        mV[vName.at(i)]          = make_unique<VertexDisplay>(vName.at(i),AlgoDir,Origin); //primary particles
        //mV_gene[vName.at(i)]     = make_unique<VertexDisplay>(vName.at(i)+"_gene",AlgoDir);
        //mV_defered[vName.at(i)]  = make_unique<VertexDisplay>(vName.at(i)+"_defered",AlgoDir);
    }
    
    //vertex display objects for secondary particles
    mV_gamma[vName.at(0)]    = make_unique<VertexDisplay>(vName.at(0)+"_gamma",AlgoDir,Origin,Algo::minVsecx,Algo::maxVsecx,Algo::minVsecy,Algo::maxVsecy,Algo::minVz,Algo::maxVz,Algo::minVr,Algo::maxVr);
    mV_positron[vName.at(0)] = make_unique<VertexDisplay>(vName.at(0)+"_positron",AlgoDir,Origin,Algo::minVsecx,Algo::maxVsecx,Algo::minVsecy,Algo::maxVsecy,Algo::minVz,Algo::maxVz,Algo::minVr,Algo::maxVr);
    mV_electron[vName.at(0)] = make_unique<VertexDisplay>(vName.at(0)+"_electron",AlgoDir,Origin,Algo::minVsecx,Algo::maxVsecx,Algo::minVsecy,Algo::maxVsecy,Algo::minVz,Algo::maxVz,Algo::minVr,Algo::maxVr);
    mV_neutron[vName.at(0)]  = make_unique<VertexDisplay>(vName.at(0)+"_neutron",AlgoDir,Origin,Algo::minVsecx,Algo::maxVsecx,Algo::minVsecy,Algo::maxVsecy,Algo::minVz,Algo::maxVz,Algo::minVr,Algo::maxVr);
    mV_muminus[vName.at(0)]  = make_unique<VertexDisplay>(vName.at(0)+"_muminus",AlgoDir,Origin,Algo::minVsecx,Algo::maxVsecx,Algo::minVsecy,Algo::maxVsecy,Algo::minVz,Algo::maxVz,Algo::minVr,Algo::maxVr);
    mV_muplus[vName.at(0)]  = make_unique<VertexDisplay>(vName.at(0)+"_muplus",AlgoDir,Origin,Algo::minVsecx,Algo::maxVsecx,Algo::minVsecy,Algo::maxVsecy,Algo::minVz,Algo::maxVz,Algo::minVr,Algo::maxVr);
    
    
    hVy_Vx = make_unique<TH2D>("nhVy_Vx","Vy vs Vx of injection vertex /evt", Algo::NbBinV,-Algo::maxVx,Algo::maxVx, Algo::NbBinV,-Algo::maxVy,Algo::maxVy);
    hVz_Vx = make_unique<TH2D>("nhVz_Vx","Vz vs Vx of injection vertex /evt", Algo::NbBinV,-Algo::maxVx,Algo::maxVx, Algo::NbBinV,-Algo::maxVz,Algo::maxVz);
    hVz_Vy = make_unique<TH2D>("nhVz_Vy","Vz vs Vy of injection vertex /evt", Algo::NbBinV,-Algo::maxVy,Algo::maxVy, Algo::NbBinV,-Algo::maxVz,Algo::maxVz);
    hVy_Vx2 = make_unique<TH2D>("nhVy_Vx2","Vy vs Vx of first interaction /evt", Algo::NbBinV,-Algo::maxVx,Algo::maxVx, Algo::NbBinV,-Algo::maxVy,Algo::maxVy);
    hVz_Vx2 = make_unique<TH2D>("nhVz_Vx2","Vz vs Vx of first interaction /evt", Algo::NbBinV,-Algo::maxVx,Algo::maxVx, Algo::NbBinV,-Algo::maxVz,Algo::maxVz);
    hVy_Vxf = make_unique<TH2D>("nhVy_Vxf","Vy vs Vx of last interaction /evt", Algo::NbBinV,-Algo::maxVx,Algo::maxVx, Algo::NbBinV,-Algo::maxVy,Algo::maxVy);
    hVz_Vxf = make_unique<TH2D>("nhVz_Vxf","Vz vs Vx of last interaction /evt", Algo::NbBinV,-Algo::maxVx,Algo::maxVx, Algo::NbBinV,-Algo::maxVz,Algo::maxVx);
    
    
    hVy_Vx->GetXaxis()->SetTitle("Vx [mm]");
    hVy_Vx->GetYaxis()->SetTitle("Vy [mm]");
    hVz_Vx->GetXaxis()->SetTitle("Vx [mm]");
    hVz_Vx->GetYaxis()->SetTitle("Vz [mm]");
    hVz_Vy->GetXaxis()->SetTitle("Vy [mm]");
    hVz_Vy->GetYaxis()->SetTitle("Vz [mm]");
    hVy_Vx2->GetXaxis()->SetTitle("Vx [mm]");
    hVy_Vx2->GetYaxis()->SetTitle("Vy [mm]");
    hVz_Vx2->GetXaxis()->SetTitle("Vx [mm]");
    hVz_Vx2->GetYaxis()->SetTitle("Vz [mm]");
    hVy_Vxf->GetXaxis()->SetTitle("Vx [mm]");
    hVy_Vxf->GetYaxis()->SetTitle("Vy [mm]");
    hVz_Vxf->GetXaxis()->SetTitle("Vx [mm]");
    hVz_Vxf->GetYaxis()->SetTitle("Vz [mm]");
    
    //Added parameters (minVz, minVx, minV) for better 3D vertex visualisation
    hVz_Vy_Vx = make_unique<TH3D>("nhVz_Vy_Vx","Vz vs Vy vs Vx of injection vertex /evt", Algo::NbBinV,Algo::minVx,Algo::maxVx, Algo::NbBinV,Algo::minVy,Algo::maxVy, Algo::NbBinV,Algo::minVz,Algo::maxVz);
    hVz_Vy_Vx2 = make_unique<TH3D>("nhVz_Vy_Vx2","Vz vs Vy vs Vx of first intercation /evt", Algo::NbBinV,Algo::minVx,Algo::maxVx, Algo::NbBinV,Algo::minVy,Algo::maxVy, Algo::NbBinV,Algo::minVz,Algo::maxVz);
    hVz_Vy_Vxf = make_unique<TH3D>("nhVz_Vy_Vxf","Vz vs Vy vs Vx of last intercation /evt", Algo::NbBinV,Algo::minVx,Algo::maxVx, Algo::NbBinV,Algo::minVy,Algo::maxVy, Algo::NbBinV,Algo::minVz,Algo::maxVz);
    
    hVz_Vy_Vx->GetXaxis()->SetTitle("Vx [mm]");
    hVz_Vy_Vx->GetYaxis()->SetTitle("Vy [mm]");
    hVz_Vy_Vx->GetZaxis()->SetTitle("Vz [mm]");
    hVz_Vy_Vx2->GetXaxis()->SetTitle("Vx [mm]");
    hVz_Vy_Vx2->GetYaxis()->SetTitle("Vy [mm]");
    hVz_Vy_Vx2->GetZaxis()->SetTitle("Vz [mm]");
    hVz_Vy_Vxf->GetXaxis()->SetTitle("Vx [mm]");
    hVz_Vy_Vxf->GetYaxis()->SetTitle("Vy [mm]");
    hVz_Vy_Vxf->GetZaxis()->SetTitle("Vz [mm]");
}

void VertexAlgo::Process(ULong64_t , Int_t )
{
    
    for (Int_t part=0; part<(*MC)->GetNbEvents(); part++) {
        
        dir = (*MC)->GetMCEvent(part)->GetPrimaryParticle()->GetMomentumDirection(0); //0 because we want it at injection. (not filled for other steps but possible in DCEventAction.cc)
        
        for (UInt_t i=0; i<Particle::NbVertex; i++) {
            v = (*MC)->GetMCEvent(part)->GetPrimaryParticle()->GetPosition(i);
            mV[vName[i]]->Fill(v);
        }
        
        MCEvent * myMCEvent = (*MC)->GetMCEvent(part);
        Int_t NSec = myMCEvent->GetSecondaryParticleTCA()->GetEntriesFast();
        
        for (Int_t k=0; k<NSec; k++){
            
            Particle *mySecPart = myMCEvent->GetSecondaryParticle(k);
            Int_t PDG = mySecPart->GetPDG();
            v = mySecPart->GetPosition(0); //we are only interested in 'injection' vertex here
            
            switch(PDG) {
                case -11: // positron
                    mV_positron[vName[0]]->Fill(v);
                    
                    break;
                case 22: // gamma
                    mV_gamma[vName[0]]->Fill(v);
                    break;
                    
                case 11: // electron
                    mV_electron[vName[0]]->Fill(v);
                    break;
                    
                case 2112: // neutron
                    mV_neutron[vName[0]]->Fill(v);
                    break;
                    
                case 13: //mu-
                    mV_muminus[vName[0]]->Fill(v);
                    break;
                case -13: //mu+
                    mV_muplus[vName[0]]->Fill(v);
                    break;
                    
            }
            
        }
    }
    
    //injection, first interaction and last interaction points
    for (UInt_t k=0; k<3; k++) {
        V[k] = (*MC)->GetMCEvent(0)->GetPrimaryParticle()->GetCoordinate(0,k);
        V2[k] = (*MC)->GetMCEvent(0)->GetPrimaryParticle()->GetCoordinate(1,k);
        Vf[k] = (*MC)->GetMCEvent(0)->GetPrimaryParticle()->GetCoordinate(2,k);
    }
    
    if (  (*MC)->GetPrimaryType() != (Int_t)NewGene::defered_track_process
        and (*MC)->GetPrimaryType() != (Int_t)OldGene::defered_track_process ) {
        hVy_Vx->Fill(V[0]-Origin.x(),V[1]-Origin.y());
        hVz_Vx->Fill(V[0]-Origin.x(),V[2]-Origin.z());
        hVz_Vy->Fill(V[1]-Origin.y(),V[2]-Origin.z());
        hVz_Vy_Vx->Fill(V[0]-Origin.x(),V[1]-Origin.y(),V[2]-Origin.z());
    }
    
    hVy_Vx2->Fill(V2[0]-Origin.x(),V2[1]-Origin.y());
    hVz_Vx2->Fill(V2[0]-Origin.x(),V2[2]-Origin.z());
    hVz_Vy_Vx2->Fill(V2[0]-Origin.x(),V2[1]-Origin.y(),V2[2]-Origin.z());
    
    hVy_Vxf->Fill(Vf[0]-Origin.x(),Vf[1]-Origin.y());
    hVz_Vxf->Fill(Vf[0]-Origin.x(),Vf[2]-Origin.z());
    hVz_Vy_Vxf->Fill(Vf[0]-Origin.x(),Vf[1]-Origin.y(),Vf[2]-Origin.z());
}

void VertexAlgo::Finalise()
{
    for (UInt_t i=0; i<vName.size(); i++) {
        mV[vName.at(i)]->DrawAndWrite();
    }
    
    mV_gamma[vName.at(0)]->DrawAndWrite();
    mV_neutron[vName.at(0)]->DrawAndWrite();
    mV_positron[vName.at(0)]->DrawAndWrite();
    mV_electron[vName.at(0)]->DrawAndWrite();
    mV_muminus[vName.at(0)]->DrawAndWrite();
    mV_muplus[vName.at(0)]->DrawAndWrite();
    
    UInt_t pad = 0u;
    auto CanVertex = make_unique<TCanvas>("nCanVertex", "Vertex distribution", 0,0, 1600, 800);
    CanVertex->Divide(4,2);
    DrawAndWrite(CanVertex->cd(++pad),*hVy_Vx,"COLZ");
    DrawAndWrite(CanVertex->cd(++pad),*hVz_Vx,"COLZ");
    DrawAndWrite(CanVertex->cd(++pad),*hVz_Vy,"COLZ");
    DrawAndWrite(CanVertex->cd(++pad),*hVy_Vx2,"COLZ");
    DrawAndWrite(CanVertex->cd(++pad),*hVz_Vx2,"COLZ");
    DrawAndWrite(CanVertex->cd(++pad),*hVy_Vxf,"COLZ");
    DrawAndWrite(CanVertex->cd(++pad),*hVz_Vxf,"COLZ");
    CanVertex->Write();
    
    pad = 0u;
    auto Can3D = make_unique<TCanvas>("nCan3D", "3D vertex distribution", 0,0, 1600, 800);
    Can3D->Divide(1,1);
    DrawAndWrite(Can3D->cd(++pad),*hVz_Vy_Vx);
    DrawAndWrite(Can3D->cd(++pad),*hVz_Vy_Vx2);
    hVz_Vy_Vxf->Write();
    Can3D->Write();
}
