#include <ProgressBar.hh>

#include <sys/time.h>
#include <time.h>
#include <unistd.h>
#include <cstdlib>
#include <cstdio>
#include <iostream>
#include <cstring>
#include <chrono>

void display_progress_bar(char* disp, const unsigned long whereami, const unsigned long tot, const bool kbs)
{
  int progress=int(100.*double(whereami)/double(tot));
  char* p = disp;
  struct timeval tv;
  static double previous_time = 0., previous = 0.;
  std::sprintf(disp,"processed: %2d%%  ",progress);
  move_to_end(p,disp);
  *p++ = '[';
  for ( int k = 0 ; k < 20 ; k++) {
    if ( k < progress/5) {  *p++='='; }
    else if (k == progress/5) { *p++='>'; }
    else { *p++=' '; }
  }
  *p++=']';
  *p++='\0';
  gettimeofday(&tv,0);
  char buffer[40];
  double speed = ( double(whereami) - previous * double(tot) ) / double(tv.tv_sec + 1.0e-6*tv.tv_usec - previous_time)/1024.;
  if ( kbs) { std::sprintf(buffer,"  %.1f KB/s ",speed); }
  else { std::sprintf(buffer,"  %.1f evts/s ",speed*1024.); }
  strncat(disp,buffer,40);
  std::cout << "\r";
  std::cout << disp;
  std::cout.flush();
  previous = double(whereami)/double(tot);
  previous_time=double(tv.tv_sec+1.0e-6*tv.tv_usec);
}

void display_progress_bar2(char* disp, const unsigned long whereami, const unsigned long tot, const double init_time, const bool kbs)
{
  int progress=int(100.*double(whereami)/double(tot));
  char* p = disp;
  struct timeval tv;
  static double previous_time=0., previous = 0.;
  std::sprintf(disp,"processed: %2d%%  ",progress);
  move_to_end(p,disp);
  *p++ = '[';
  for ( int k = 0 ; k < 20 ; k++) {
    if ( k < progress/5) {  *p++='='; }
    else if (k == progress/5) { *p++='>'; }
    else { *p++=' '; }
  }
  *p++=']';
  *p++='\0';
  gettimeofday(&tv,0);
  char buffer[40];
  double speed = ( double(whereami) - previous * double(tot) ) / double(tv.tv_sec + 1.0e-6*tv.tv_usec - previous_time)/1024.;
  double average = double(whereami) / double(tv.tv_sec + 1.0e-6*tv.tv_usec - init_time)/1024.;
  if ( kbs) { std::sprintf(buffer,"  %.1f evts/s -> %.1f evts/s",speed,average); }
  else { std::sprintf(buffer,"  %.1f evts/s -> %.1f evts/s",speed*1024.,average*1024.); }
  strncat(disp,buffer,40);
  std::cout << "\r";
  std::cout << disp;
  std::cout.flush();
  previous = double(whereami)/double(tot);
  previous_time=double(tv.tv_sec+1.0e-6*tv.tv_usec);
}

void display_progress_bar2_chrono(char* disp, const unsigned long whereami, const unsigned long tot, const PBTP init_time, const bool kbs)
{
  static PBTP previous_time = PBC::now();
  static double previous = 0.;

  char* p = disp;
  int progress=int(100.*double(whereami)/double(tot));
  std::sprintf(disp,"processed: %2d%%  ",progress);
  move_to_end(p,disp);
  *p++ = '[';
  for ( int k = 0 ; k < 20 ; k++) {
    if ( k < progress/5) {  *p++='='; }
    else if (k == progress/5) { *p++='>'; }
    else { *p++=' '; }
  }
  *p++=']';
  *p++='\0';
  PBTP now = PBC::now();
  char buffer[40];
  double speed = ( double(whereami) - previous * double(tot) ) / double((now-previous_time).count())/1024.;
  double average = double(whereami) / double((now-init_time).count())/1024.;
  if ( kbs) { std::sprintf(buffer,"  %.1f evts/s -> %.1f evts/s",speed,average); }
  else { std::sprintf(buffer,"  %.1f evts/s -> %.1f evts/s",speed*1024.,average*1024.); }
  strncat(disp,buffer,40);
  std::cout << "\r";
  std::cout << disp;
  std::cout.flush();
  previous = double(whereami)/double(tot);
  previous_time=now;
}

void display_end_bar(char* disp)
{
  sprintf(disp,"processed: 100%% [====================]");
  std::cout << "\r";
  std::cout << disp;
  std::cout.flush();
  std::cout << "\n";
}

