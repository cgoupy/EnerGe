#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedef;

#pragma link C++ class Algo+;
#pragma link C++ class AlgoList+;
#pragma link C++ class EdepAlgo+; // ROOT does not tolerate map of unique_ptr...
#pragma link C++ class PrimariesAlgo+;
#pragma link C++ class SecondariesAlgo+;
#pragma link C++ class OrsayOVAlgo+;
#pragma link C++ class VertexAlgo+;
#endif
