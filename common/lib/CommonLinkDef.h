#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;

#pragma link C++ function display_progress_bar(char*, int, int, bool);
#pragma link C++ function display_progress_bar2(char*, int, int, double, bool);
#pragma link C++ function display_end_bar(char*);

#endif
