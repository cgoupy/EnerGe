#ifndef PROGRESS_BAR_HH
#define PROGRESS_BAR_HH

#include <chrono>
// stolen from wget code
/* Use move_to_end (p,s) to get p to point the end of the string (the
   terminating \0).  This is faster than s+=strlen(s), but some people
   are confused when they see strchr (s, '\0') in the code.  */
#define move_to_end(p,s) p = strchr (s, '\0');

void display_progress_bar(char* disp, const unsigned long whereami, const unsigned long tot, const bool kbs=true);
void display_progress_bar2(char* disp, const unsigned long whereami, const unsigned long tot, const double init_time, const bool kbs=true);
void display_end_bar(char* disp);
typedef std::chrono::high_resolution_clock PBC; // Progress Bar Clock
typedef std::chrono::time_point<PBC,PBC::duration> PBTP; // Progress Bar Time Point
void display_progress_bar2_chrono(char* disp, const unsigned long whereami, const unsigned long tot, const PBTP init_time, const bool kbs=true);

#endif
