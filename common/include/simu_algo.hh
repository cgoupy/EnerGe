#ifndef SIMU_ALGO_HH
#define SIMU_ALGO_HH

#include <algo.hh>
#include <Index.h>

//#include <RtypesCore.h>
//#include <TObject.h>
#include <TSystem.h>
#include <TString.h>
#include <TVector3.h>
#include <TH1.h>
#include <TH2.h>
#include <TH3.h>
#include <TNtuple.h>
#include <TRandom.h>

#include <map>
#include <vector>
#include <array>
#include <utility>
#include <memory>

class TVirtualPad;
class TDirectory;

////////////////////////////////////////////////////////////////////////////////
//////////////// Edep analysis of simulation ////////////////
////////////////////////////////////////////////////////////////////////////////

class EdepAlgo : public Algo {
public:
    virtual ~EdepAlgo() {}
    
    EdepAlgo(): Algo() {}
    EdepAlgo(std::ofstream&w, TString opt="", MCInfo** Mc=0, TString name="EdepAlgo"):
    Algo(w, opt, Mc, name) {}
    
    EdepAlgo(const char* name, const char* title) = delete;
    EdepAlgo(const TString& name, const TString& title) = delete;
    EdepAlgo(const TNamed& named) = delete;
    EdepAlgo(const Algo& ) = delete;
    
    virtual void Initialise(TTree* tree);
    virtual void Process(ULong64_t I, Int_t T);
    virtual void Finalise();
    
    inline virtual void* GetResult() { return &mh; }
    
private:
    constexpr static Int_t NbNeutronProcess = 13;
    enum NeutronProcess : Int_t {
        None,
        Unknown,
        NucleusEmission, // (n,p) and (n,alpha) and (n,...) and everything considered Inelastic by G4, but with at least a proton emission
        Elastic,
        Inelastic,
        Capture,
        Fission,
        HadronAtRest,
        LeptonAtRest,
        ChargeExchange,
        RadioactiveDecay,
        Transportation1,
        Transportation2
    };
    
    TString NeutronProcessName(Int_t process) const;
    TString edep_unit_name, einj_unit_name;
    TString hNameE;
    
    std::map<Int_t, ULong64_t> mCapturingNuclei, mNbPrimaryPart;
    std::vector<Int_t> vNeutronFinalProcess;
    
    // some type deduction
    using map_type = decltype(mCapturingNuclei);
    using pair_type = std::pair<map_type::key_type, map_type::mapped_type>;
    /** Very stupidly, the following don't work because you get pair< CONST key_type, mapped_type>,
     *      and such pair is not assignable (can't copy), so impossible to define a vector with such type. */
    //   using pair_type = map_type::value_type;
    //   using pair_type = decltype((*mCapturingNuclei.begin()));
    template <class T>
    void PrintNeutron(const EdepAlgo::map_type& m, T& OUT);
    static Bool_t MappedComp(const pair_type& a, const pair_type& b) { return a.second > b.second; }
    
    std::map<TString, std::unique_ptr<TH1D>> mh; // ROOT does not tolerate map of unique_ptr...
    std::map<TString, std::unique_ptr<TH1D>> mhVolumes;
    std::map<TString, std::unique_ptr<TH2D>> mh2;
    
    Bool_t defered=false;
    NeutronProcess theNeutronFinalProcess;
    Int_t PDG, FinalProcess, CurrentTree = -1; // Initialise at -1 so that the Zero() method is called even for the first run
    Double_t UT=0., deltaUT=0., NbEvtTot=0., edep_convert_coeff=0.,einj_convert_coeff=0.;
    Double_t path=0.;
    Float_t Einj=0.f, Edep=0.f;
    
    ULong64_t NbEvtTotal=0ull, NbEvtGenerator=0ull, NbEvtDefered=0ull, NbUnordered=0ull;
    
    ClassDef(EdepAlgo,2);
};

////////////////////////////////////////////////////////////////////////////////
////////////// Primary particle info from simulation ///////////
////////////////////////////////////////////////////////////////////////////////

class PrimariesAlgo : public Algo {
public:
    virtual ~PrimariesAlgo() {}
    
    PrimariesAlgo(): Algo() {}
    PrimariesAlgo(std::ofstream&w, TString opt="", MCInfo** Mc=0, TString name="PrimariesAlgo"):
    Algo(w, opt, Mc, name) {}
    
    PrimariesAlgo(const char* name, const char* title) = delete;
    PrimariesAlgo(const TString& name, const TString& title) = delete;
    PrimariesAlgo(const TNamed& named) = delete;
    PrimariesAlgo(const Algo& ) = delete;
    
    virtual void Initialise(TTree* tree);
    virtual void Process(ULong64_t I, Int_t T);
    virtual void Finalise();
    
    inline virtual void* GetResult() { return &mh; }
    
private:
    
    TString unit_name;
    
    //histograms
    std::map<TString, std::unique_ptr<TH1D>> mh;
    std::map<TString, std::unique_ptr<TH2D>> mh2;
    
    Bool_t defered=false;
    Int_t CurrentTree = -1; // Initialise at -1 so that the Zero() method is called even for the first run
    Double_t UT=0., deltaUT=0., convert_coef=0.;
    //Double_t path = 0.;
    Double_t Einj=0.f;
    Int_t PDG;
    
    ULong64_t NbEvtTotal=0ull, NbEvtGenerator=0ull, NbEvtDefered=0ull, NbUnordered=0ull;
    
    TVector3 Origin;
    Double_t R, maxR;
    Double_t CosTheta, Phi;
    
    ClassDef(PrimariesAlgo,2);
};

//////////////////////////////////////////////////////////////////////////////////////
//////////////// Secondary fluxes from simulation //////////////
////////////////////////////////////////////////////////////////////////////////

class SecondariesAlgo : public Algo {
public:
    virtual ~SecondariesAlgo() {}
    
    SecondariesAlgo(): Algo() {}
    SecondariesAlgo(std::ofstream&w, TString opt="", MCInfo** Mc=0, TString name="SecondariesAlgo"):
    Algo(w, opt, Mc, name) {}
    
    SecondariesAlgo(const char* name, const char* title) = delete;
    SecondariesAlgo(const TString& name, const TString& title) = delete;
    SecondariesAlgo(const TNamed& named) = delete;
    SecondariesAlgo(const Algo& ) = delete;
    
    virtual void Initialise(TTree* tree);
    virtual void Process(ULong64_t I, Int_t T);
    virtual void Finalise();
    
    inline virtual void* GetResult() { return &mh; }
    
private:
    
    TString unit_name;
    
    //histograms for secondary fluxes
    std::map<TString, std::unique_ptr<TH1D>> mh; // ROOT does not tolerate map of unique_ptr...
    std::map<TString, std::unique_ptr<TH1D>> mh_log; // ROOT does not tolerate map of unique_ptr...
    
    Bool_t defered=false;
    Int_t CurrentTree = -1; // Initialise at -1 so that the Zero() method is called even for the first run
    Double_t UT=0., deltaUT=0., NbEvtTot=0., convert_coef=0.;
    Float_t Einj=0.f;
    
    ULong64_t NbEvtTotal=0ull, NbEvtGenerator=0ull, NbEvtDefered=0ull, NbUnordered=0ull;
    
    ClassDef(SecondariesAlgo,2);
};


////////////////////////////////////////////////////////////////////////////////
//////////////// Orsay OV coincidencies  ////////////////
////////////////////////////////////////////////////////////////////////////////
class OrsayOVAlgo : public Algo {
public:
    virtual ~OrsayOVAlgo() {}
    
    OrsayOVAlgo(): Algo() {}
    OrsayOVAlgo(std::ofstream&w, TString opt="", MCInfo** Mc=0, TString name="OrsayOVAlgo"):
    Algo(w, opt, Mc, name) {}
    OrsayOVAlgo(const char* name, const char* title) = delete;
    OrsayOVAlgo(const TString& name, const TString& title) = delete;
    OrsayOVAlgo(const TNamed& named) = delete;
    OrsayOVAlgo(const Algo& ) = delete;
    
    virtual void Initialise(TTree* tree);
    virtual void Process(ULong64_t I, Int_t T);
    virtual void Finalise();
    
    inline virtual void* GetResult() { return &mhVolumes; }
    
private:
    Bool_t IsAccepted(Int_t HitCode);
    TString Savein(Int_t HitCode);
    void FillHistograms(TString BaseName, float Energy, TString VolumeName);
    
private:
    TString edep_unit_name, einj_unit_name;
    TString hNameE;
    
    std::map<Int_t, ULong64_t> mNbPrimaryPart;
    
    std::map<TString, std::unique_ptr<TH1D>> mhVolumes;
    std::map<TString, std::unique_ptr<TH2D>> mh2;
    
    Bool_t defered=false;
    Int_t PDG, FinalProcess, CurrentTree = -1; // Initialise at -1 so that the Zero() method is called even for the first run
    Double_t UT=0., deltaUT=0., NbEvtTot=0., edep_convert_coeff=0.,einj_convert_coeff=0.;
    Float_t Einj=0.f, Edep=0.f, Evis=0.f;
    
    ULong64_t NbEvtTotal=0ull, NbEvtGenerator=0ull, NbEvtDefered=0ull, NbUnordered=0ull;
    
    std::map<Int_t,Double_t> EdepMapPanel;
    TVector3 Origin;
    Double_t R, maxR;
    Double_t CosTheta, Phi;
    
    TRandom* eventGenerator;
    
    ClassDef(OrsayOVAlgo, 2);
};

////////////////////////////////////////////////////////////////
////////////////////// Vertex study ////////////////////////////
////////////////////////////////////////////////////////////////

class VertexAlgo : public Algo {
public:
    virtual ~VertexAlgo() {}
    
    VertexAlgo(): Algo() {}
    VertexAlgo(std::ofstream&w, TString opt="", MCInfo** Mc=0, TString name="VertexAlgo"):
    Algo(w, opt, Mc, name) {}
    VertexAlgo(const char* name, const char* title) = delete;
    VertexAlgo(const TString& name, const TString& title) = delete;
    VertexAlgo(const VertexAlgo& ) = delete;
    
    virtual void Initialise(TTree* );
    virtual void Process(ULong64_t, Int_t);
    virtual void Finalise();
    
    inline virtual void* GetResult() { return 0; }
    
    static void DrawAndWrite(TVirtualPad* pad, const TH1& h, const TString& Option="", Bool_t log=false);
    
    class VertexDisplay /*: public TObject*/ {
    public:
        //virtual ~VertexDisplay();
        VertexDisplay() = delete; //{ throw ("DONT USE ! (but ROOT needs it)"); }
        VertexDisplay(const VertexDisplay&) = delete;
        VertexDisplay(const TString& name, TDirectory* algodir, const TVector3& vOrigin = TVector3());
        VertexDisplay(const TString& name, TDirectory* algodir, const TVector3& vOrigin, Double_t minX, Double_t maxX, Double_t minY, Double_t maxY, Double_t minZ, Double_t maxZ, Double_t minR, Double_t maxR);
        
        void Fill(const TVector3& v);
        void DrawAndWrite() const;
        
    private:
        void SetAxis();
        
        TDirectory* theAlgoDir=nullptr;
        TString Name;
        TVector3 v0;
        std::unique_ptr<TH2D> hVy_Vx, hVz_Vx, hVz_Vy;
        std::unique_ptr<TH1D> hVr, hVtheta;
        
        //ClassDef(VertexDisplay,2);
    };
    
private:
    
    std::vector<TString> vName={"Vinj","Vfirst","Vlast"};
    std::map <TString, std::unique_ptr<VertexDisplay>> mV, mV_neutron, mV_gamma, mV_positron, mV_electron, mV_muminus, mV_muplus, mV_gene, mV_defered; // ROOT does not tolerate map of unique_ptr...
    
    TVector3 v,Origin;
    TVector3 dir;
    TVector3 dirMuon;
    
    std::unique_ptr<TH2D> hVy_Vx, hVz_Vx,hVz_Vy, hVy_Vx2, hVz_Vx2, hVy_Vxf, hVz_Vxf, hVy_Vx_Edep_0_1keV, hVz_Vx_Edep_0_1keV, hVz_Vy_Edep_0_1keV, hVz_Vr_Edep_0_1keV, hVy_Vx_Edep_0_10keV, hVz_Vx_Edep_0_10keV, hVz_Vy_Edep_0_10keV, hVz_Vr_Edep_0_10keV;
    std::unique_ptr<TH3D> hVz_Vy_Vx, hVz_Vy_Vx2, hVz_Vy_Vxf ;
    
    Float_t V[3], V2[3], Vf[3];
    
    ClassDef(VertexAlgo,2);
    
};

#endif
