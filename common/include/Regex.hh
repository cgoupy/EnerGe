#ifndef REGEX_HH
#define REGEX_HH 1

#include <regex>
#include <string>
#include <cstring>
#include <vector>
#include <iosfwd>
#include <iostream>

class Regex {
public:
    Regex(const char* pattern) { theRegex.assign(pattern); }
    Regex(const std::string pattern) { theRegex.assign(pattern.data()); }
    
    inline int Match(const char* const str);
    inline const std::vector<std::string>& Split(const char* const str);
    inline void Substitute(std::string& str, const char* const replace) {str = std::regex_replace(str,theRegex,replace);}
    
    inline void Assign(const char* const pattern) { theRegex.assign(pattern); Clear(); }
    inline void Clear() { theTokens.clear(); theResult = std::cmatch(); }
    inline void Reset() { Clear(); theRegex = std::regex(); }
    
    inline int Nb() const { return theTokens.size(); }
    inline int NbMatch() const { return theTokens.size(); }
    inline int GetNbMatch() const { return theTokens.size(); }
    inline const std::string& at(uint i) const { return theTokens.at(i); } // test on i
    inline const std::string& operator()(uint i) const { return theTokens.at(i); } // test on i
    inline const std::string& operator[](uint i) const { return theTokens[i]; } // no test
    
    // integrated cast to all numeric types
    inline decltype(std::stoi(std::string())) stoi(uint i) const { return std::stoi(theTokens.at(i)); } // test on i
    inline decltype(std::stol(std::string())) stol(uint i) const { return std::stol(theTokens.at(i)); } // test on i
    inline decltype(std::stoul(std::string())) stoul(uint i) const { return std::stoul(theTokens.at(i)); } // test on i
    inline decltype(std::stoll(std::string())) stoll(uint i) const { return std::stoll(theTokens.at(i)); } // test on i
    inline decltype(std::stoull(std::string())) stoull(uint i) const { return std::stoull(theTokens.at(i)); } // test on i
    inline decltype(std::stof(std::string())) stof(uint i) const { return std::stof(theTokens.at(i)); } // test on i
    inline decltype(std::stod(std::string())) stod(uint i) const { return std::stod(theTokens.at(i)); } // test on i
    inline decltype(std::stold(std::string())) stold(uint i) const { return std::stold(theTokens.at(i)); } // test on i
    inline const std::vector<std::string>& GetTokens() const { return theTokens; }
    inline const std::vector<std::string>& GetSplitResults() const { return theTokens; }
    
    inline void PrintTokens(std::ostream& out) const;
    
private:
    std::regex  theRegex;
    std::cmatch theResult;
    std::vector<std::string> theTokens;
    
public: // methods with strings
    inline const std::vector<std::string>&  Split(const std::string& str);
    inline int Match(const std::string& str) { return Match(str.data()); }
    inline void Assign(const std::string& pattern) { theRegex.assign(pattern); Clear(); }
    
    inline void Substitute(const char* str, const char* const replace)   {  str = std::regex_replace(str,theRegex,replace).data();}
    inline void Substitute(const char* str, const std::string& replace)  {  Substitute(str, replace.data()); }
    inline void Substitute(std::string& str, const std::string& replace) {  Substitute(str.data(), replace.data()); }
    inline void Sub(const char* str, const char* const replace)          {  Substitute(str, replace); }
    inline void Sub(const char* str, const std::string& replace)         {  Substitute(str, replace.data()); }
    inline void Sub(std::string& str, const char* const replace)         {  Substitute(str, replace); }
    inline void Sub(std::string& str, const std::string& replace)        {  Substitute(str.data(), replace.data()); }
};

inline int Regex::Match(const char* const str)
{
    Clear();
    if (!std::regex_search(str,theResult,theRegex)) return 0;
#if __cplusplus >= 201402L
    std::copy_if(theResult.begin()+1, theResult.end(), std::back_inserter(theTokens), [](const auto& match) {return match.matched;});
#else
    std::copy_if(theResult.begin()+1, theResult.end(), std::back_inserter(theTokens), [](decltype(*begin(theResult))& match) {return match.matched;});
#endif
    
    return theTokens.size();
}

inline const std::vector< std::string >& Regex::Split(const char* const str)
{
    Clear();
    for ( std::regex_token_iterator<const char*> regit(str, str+strlen(str), theRegex, -1);
         regit!=std::regex_token_iterator<const char*>(); ++regit ) { theTokens.push_back(regit->str()); }
    return theTokens;
}

inline const std::vector< std::string >& Regex::Split(const std::string& str)
{
    Clear();
    for ( std::regex_token_iterator<std::string::const_iterator> regit(str.begin(), str.end(), theRegex, -1);
         regit!=std::regex_token_iterator<std::string::const_iterator>(); ++regit ) { theTokens.push_back(regit->str()); }
    return theTokens;
}

inline void Regex::PrintTokens(std::ostream& out) const
{
    out<<"Nb="<<theTokens.size()<<": \t";
    for (auto &el: theTokens) { out<<"  ["<<el<<"]"; }
    out<<"\n";
}

#endif
