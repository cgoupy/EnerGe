// --------------------------------------------------------------------------//
/**
 * AUTHOR: J. Gaffiot
 * DATE: 2014-10-20
 * CONTACT: jonathan.gaffiot@cea.fr
 */
// --------------------------------------------------------------------------//
/** Inspired by DCParam, the API for the Double Chooz simple "database"
    of numeric and string values, written by Glenn Horton-Smith.
*/
#ifndef PARAMETERS_HH
#define PARAMETERS_HH

#include <Exception.hh>

#include <memory>
#include <map>
#include <vector>
#include <string>

class ParamTable;
#ifdef PARAMBASE__USE_ROOT
class TFile;
#endif

////////////////////////////////////////////////////////////////
////////////////////////// exception ///////////////////////////
////////////////////////////////////////////////////////////////

class parambase_except : public except {
public:
  explicit parambase_except(const std::string& arg): except(arg) {}
  explicit parambase_except(const std::string& arg, std::ofstream& write): except(arg,write) {}

  template <typename T> parambase_except(const T& val): except(val) {}
  template <typename T, typename ... Args> parambase_except(const T& val, Args ... args): except(val, args ...) {}
};

////////////////////////////////////////////////////////////////
////////////////////////// ParamBase ///////////////////////////
////////////////////////////////////////////////////////////////

/** ParamBase provides a simple "database" of numeric and string values:
   a string/value hashtable that is initialized from a simple text file.
   The text file looks something like this:

   @code
   # blank lines and lines beginning with '#' are ignored

   pi  3.14159265358979312  # any space before and any character after the '#' are ignored
   model  glisur
   e  2.71828182845904509e+00  This is 'e' as in exp(1), not the e+ charge.
   q_e 1.60217646200000007e-19 coulomb  # Charge of the positron in coulombs
   @endcode

   The above input would define four parameters: a numeric parameter
   named "pi", a string parameter named "model", and two parameters
   "e" and "q_e" having both numeric and string values.  Note that the
   word "coulomb" following the numeric value on the "q_e" line is
   the only word considered, while the string value of the "e" line is
   the whole phrase "This is 'e' as in exp(1), not the e+ charge.".
   The general format for a non-blank, non-comment line is:

   @code
     key [ numeric_value ] [ string_value ] [ # comments ]
   @endcode

   where brackets surround optional components.
   The string value extends to the end of the line if no '#' symbol is found.
   The space(s) between the last non-white character and the '#' symbol are ignored.

  - To determine wether an entry has a numeric value or not, the first "word" after
    the key is tested with std::stod method. Formats below should be valid:
      64320     6.4320     6,4320
      6.43e20   6.43E20    6,43e20
      6.43e-20  6.43E-20   6,43e-20 -6.43e+20

  - String values are  supported.  In fact, every parameter may
    have both a numeric and a string value.  For keys that are
    primarily numeric, the string value should be used for a
    description of the meaning of the quantity.

  - New parameters may only be defined via ReadFile, not directly through operator[].
    It guards against typos in the argument to operator[] or user input.

  - This class is a singleton : it can not be freely instantiated or deleted.
    Use the GetDataBase* members to get the unique instance of the class per program.
    Creator and destructor are then private.

   Based on DCparam from DoubleChooz software, based on GLG4 package from Glenn Horton-Smith.
   Rewritten with std::string and regex by Jonathan Gaffiot.
*/

class ParamBase {
public:
  ParamBase(const ParamBase &) = delete;
  ParamBase() = delete;
  /// Singleton management
  // Get reference to the read/write database, constructing as necessary. Use this only to fill the database.
  static ParamBase& GetInputDataBase() {
    if (theParamBase == nullptr) { theParamBase = new ParamBase(false); }
    else { theParamBase->IsReadOnly = false; }
    return *theParamBase;
  }
  // Get the pointer to the read/write database, constructing as necessary. Use this only to fill the database.
  static ParamBase* GetInputDataBasePtr() {
    if (theParamBase == nullptr) { theParamBase = new ParamBase(false); }
    else { theParamBase->IsReadOnly = false; }
    return theParamBase;
  }

  // Get reference to the read-only database, constructing as necessary
  static const ParamBase& GetDataBase() {
    if (theParamBase == nullptr) { theParamBase = new ParamBase(true); }
    else { theParamBase->IsReadOnly = true; }
    return *theParamBase;
  }
  // Get the pointer to the read-only database, constructing as necessary
  static ParamBase const * GetDataBasePtr() {
    if (theParamBase == nullptr) { theParamBase = new ParamBase(true); }
    else { theParamBase->IsReadOnly = true; }
    return theParamBase;
  }


  /// Getters, constant methods -> no need to check for IsReadOnly
  std::string GetPath(const char* env_path) const;
  inline std::string GetPath(const std::string& env_path) const { return GetPath(env_path.c_str()); }

  void WriteText(const char *filename, const char* env_path=nullptr) const;
  void WriteText(std::ostream &os) const;

#ifdef PARAMBASE__USE_ROOT
  void WriteRoot(const char *filename) const;
  void WriteRoot(TFile* file) const;
#endif

  template <class T=double>
  T Get(const std::string& key) const;
  const std::string& GetStrValue(const std::string& key) const;

  // bunch of classic accessors
  inline double             operator[](const std::string& key) const { return Get(key); }
  inline const std::string& operator()(const std::string& key) const { return GetStrValue(key); }

  const ParamTable* GetTablePtr(const char* name) const;
  inline const ParamTable* GetTablePtr(const std::string& name) const { return GetTablePtr(name.c_str()); }
  inline const ParamTable& GetTable(const char* name)           const { return *GetTablePtr(name); }
  inline const ParamTable& GetTable(const std::string& name)    const { return *GetTablePtr(name.c_str()); }

  /** Test if key is in the numeric parameter table */
  inline bool HasNumValue(const std::string& key)    const { return (NumMap.count(key)>0); }
  /** Test if key is in the string parameter table */
  inline bool HasStrValue(const std::string& key) const { return (StringMap.count(key)>0 && StringMap.at(key).size()>0); }
  /** Test if key is in the table table */
  inline bool HasTable(const std::string& tablename) const { return (TableMap.count(tablename)>0); }

  /** Get list of keys, in the order in which they were read in. */
  inline const std::vector<std::string>& GetListOfKeys() const { return KeyList; }


  /// Setters, NON constant methods -> DO need to check for IsReadOnly
  enum EOverwrite { kKeep, kOverride };
  void ReadFile(std::string file_path, const char* env_path=nullptr, const char* prefix="", EOverwrite overwrite= kKeep);
  void ReadFile(std::istream &input, std::string prefix="", EOverwrite overwrite= kKeep);

       double& operator[](const std::string& key);
  std::string& operator()(const std::string& key);

  inline void SetNumValue(const std::string& key, double value) { this->operator[](key) = value; }
  inline void SetStrValue(const std::string& key, const std::string& value) { this->operator()(key) = value; }

  void DefineTable(const char *filename, const char *tablename, const char* env_path=nullptr);

private:
  /// Constructors
  ParamBase(bool isreadonly) { IsReadOnly = isreadonly; }
  template <class Collection>
  void UpdateKeyValues(const std::string& key, const Collection& matching_values);
  void ParseDataLine(const std::string& line, const char* regex_pattern, std::string prefix, EOverwrite overwrite);

  /// Members
  bool IsReadOnly; // read-only most of time, when reading values from map
  static ParamBase *theParamBase;  // common instance for database

  std::vector<std::string> KeyList;            // to preserve order
  std::map<std::string, double> NumMap;         // numeric values
  std::map<std::string, std::string> StringMap; // string values
  std::map<std::string, std::unique_ptr<ParamTable> > TableMap;  // tables

};

template <class T>
T ParamBase::Get(const std::string& key) const
{
  if (!NumMap.count(key)) throw parambase_except(" ***Error ParamBase::Get: Attempt to retrieve undefined key ", key);
  return static_cast<T>(NumMap.at(key));
}

template <class Collection>
void ParamBase::UpdateKeyValues(const std::string& key, const Collection& matching_tokens){
  
  KeyList.push_back(key);
  
  try{
    
    NumMap[key] = std::stod(matching_tokens[1]); // case 'key numeric ? [comment]'
    if (matching_tokens.size()==3) StringMap[key] = matching_tokens[2]; // case 'key numeric string [comment]'
    
  }  
  catch(...){ // case 'key string [comment]'
    
    NumMap[key] = 0.;
    StringMap[key] = matching_tokens[1];
    
  }
  
}

////////////////////////////////////////////////////////////////
///////////////////////// ParamTable ///////////////////////////
////////////////////////////////////////////////////////////////

/** A ParamTable is a simple table of numeric values arranged in named
   columns and numbered rows, read from am ascii file and contained entirely in memory.
   (If more capability is needed, it may be time to use an SQL database or a TTree.)

   The ascii file for a table looks something like this:

   @code
   # comments
   # comments
   # comments
   = id  x      y      z     u    v    w
   10   1.0    0.0    1.0    0.0  0.0  -1.0
   11  -0.5    0.866  1.0    0.0  0.0  -1.0
   12  -0.5   -0.866  1.0    0.0  0.0  -1.0
   20   0.866  0.5   -1.0    0.0  0.0   1.0
   21  -0.866  0.5   -1.0    0.0  0.0   1.0
   22   0.0   -1.0   -1.0    0.0  0.0   1.0
   @endcode

   Only one table may be defined per file.

   The first line starting with "=" (prior to the start of data) will be parsed
   for the column names.  Column names are separated by whitespace; column
   names cannot contain white space.

   Typically used in conjuction with ParamBase, which can save each table
   under a key in the global keyword "database".

   Example code fragments:
    Defining a table:
    @code
     ParamBase& db( ParamBase::GetDB() );
     db.DefineTable("data/pmtcoordinates.dat", "pmtcoordinates");
    @endcode

    Accessing a table:
    @code
     ParamBase& db( ParamBase::GetDB() );
     const ParamTable& pmt( db.GetTable("pmtcoordinates") );
     for (int ipmt=0; ipmt<pmt.NRows(); ipmt++)
       make_PMT( pmt["id"][ipmt].Atoi(), pmt["shape"][ipmt] );
    @endcode

    Accessing a table perhaps somewhat more efficiently:
    @code
     ParamBase& db( ParamBase::GetDB() );
     const ParamTable& pmt( db.GetTable("pmtcoordinates") );
     ParamTable::Col_t& id( pmt["id"] );
     std::vector<std::string>& shape( pmt["shape"] );
     for (int ipmt=0; ipmt<pmt.NRows(); ipmt++)
       make_PMT( id[ipmt].Atoi(), shape[ipmt] );
    @endcode
*/
class ParamTable
{
public:
  typedef std::vector<std::string> Col_t;
  inline int GetNbRows()    const { return NbRow; }
  inline int GetNbColumns() const { return ColNames.size(); }
  inline const std::vector<std::string>& GetColumnNames() const { return ColNames; }

  std::vector<double> GetColumnAsNum(const std::string &colname) const;
  inline const Col_t& GetColumn(const std::string &colname) const {
    if ( ColMap.count(colname)==0 ) {
      throw parambase_except(" ***Error ParamTable::operator[]: Unknown column ",colname);
    }
    return ColMap.at(colname);
  }
  inline const Col_t& operator[](const std::string &colname) const { return GetColumn(colname); }
  inline double operator()(const std::string &colname, int iRow) const {
    if (iRow<0 or iRow>=NbRow) { throw parambase_except(" ***Error ParamTable::operator(): No line number: ",iRow); }
    return std::stod(GetColumn(colname).at(iRow));
  }

private:
  // Constructor is private, so that only ParamBase creates ParamTable, thanks to the friend declaration below
  ParamTable(const char* filename);
  ParamTable(const std::string& filename): ParamTable(filename.c_str()) {}
  friend void ParamBase::DefineTable(const char*, const char*, const char*);
  void ParseDataLine(const std::string& line, const char* regex_pattern);
  void FillColumnNames(std::ifstream& input, const char* regex_pattern);

  std::map< std::string, Col_t > ColMap; // map of column vectors
  std::vector<std::string> ColNames;     // column names, in order
  int NbRow=0;
};

#endif /* Parameters_hh */

