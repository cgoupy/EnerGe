#ifndef ALGO_HH
#define ALGO_HH

#include <DualStream.hh>

#include <RtypesCore.h>
#include <TNamed.h>
#include <TString.h>

#include <Index.h>

#include <iostream>
#include <map>

class TDirectory;
class TTree;
class TChain;
class TPMERegexp;
class TClonesArray;
class TString;

class MCInfo;

TString PrintTime(Double_t t);

////////////////////////////////////////////////////////////////
//////////////////////// MOTHER of ALGOs ///////////////////////
////////////////////////////////////////////////////////////////
// abstract base class for all algorithms
class Algo : public TNamed {
public:
    virtual ~Algo() {}
    
    Algo(): TNamed(), write(std::cerr),  bout(nullptr), berr(nullptr,std::cerr),
    option(""), MC(nullptr) { Init(); }
    
    Algo(std::ofstream&w, TString opt="", MCInfo** Mc=nullptr, TString name="Algo"):
    TNamed(name.Data(), name.Data()), write(w), bout(w.rdbuf()), berr(w.rdbuf(),std::cerr),
    option(opt), MC(Mc) { Init(); }
    
    Algo(const char* name, const char* title) = delete;
    Algo(const TString& name, const TString& title) = delete;
    Algo(const TNamed& named) = delete;
    Algo(const Algo& ) = delete;
    
    virtual void Initialise(TTree*  ) =0;
    virtual void Process(ULong64_t I, Int_t T) =0;
    virtual void Finalise() =0;
    
    virtual void* GetResult() =0;
    
    inline virtual Bool_t IsRawAlgo() { return false; }
    inline virtual Bool_t HasHisto()  { return true; }
    inline virtual Bool_t HasDir()    { return HasDirectory; }
    
    // Directory management
    TDirectory* CreateDirectory(TDirectory* RootDir);
    Bool_t Cd();
    
    //Read parameter database
    void ReadParameterDB();
    
    //Set Orsay OV crystals volume index
    void SetOrsayOVCrystalIndex();
    
    //Set conversion energy coefficient for energy related histograms
    void SetConvertCoeff(Double_t &convert_coeff, TString &unit_name, Int_t unit_index);
    
    //Set energy deposition histogram weight
    void SetEdepWeight(Int_t GeneCode, Double_t E, Double_t &Edep_w);
    
    void InitVolMap();
    
private:
    void Init();
    
protected:
    // Directory management
    TDirectory* AlgoDir = nullptr;
    Bool_t HasDirectory = false;
    
    // output and options
    std::ostream &write; //!
    DualStreamBuf bout, berr;
    TString option;
    
    // MC data class
    MCInfo**  MC;
    
    // main options
    //  Bool_t PrintMatrix;
    //  Bool_t WriteHisto;
    //  Bool_t WriteOnOfstream;
    //  Bool_t WriteInProccess;
    //  Bool_t PrintInProcess;
    
    public:
    
    // histo bounds
    static Double_t minE,maxE;
    static Double_t minE1,maxE1;
    static Double_t minE2D,maxE2D;
    static Double_t minEinj,maxEinj;
    static Double_t minEinc,maxEinc;
    
    static Double_t Originx,Originy,Originz,minVz,maxVz,minVx,maxVx,minVy,maxVy, minVsecx, maxVsecx, minVsecy, maxVsecy, minVr, maxVr;
    
    // histo number of bins
    static Int_t NbBinE,NbBinE1,NbBinE2D,NbBinEinj,NbBinEinc;
    static Int_t NbBinBar,NbBinV,NbBinR;
    
    static Int_t unit_edep;
    static Int_t unit_prim;
    static Int_t unit_second;
    
    // List of volumes to be analyzed (for Edep)
    struct GeoVolume {
      
        Bool_t IsAnalyzed;
        TString Name;
        TString DirName;
    };
    
    static std::map<UInt_t,GeoVolume> VolMap;
    static TString VolumeNameList[EnerGe::NbVolumes];
    static const Int_t NEdepRange = 10;
    static std::pair<TString,Double_t> EdepRange[NEdepRange];
    
    //List of volume analyzed and cuts (for OrsayOV analysis)
    static UInt_t OrsayOVCrysIndex[EnerGe::NbOrsayOVCrystals];
//    static Bool_t VetoFlag[EnerGe::NbOrsayOVCrystals-1]; //LWO crystal events are not vetoed
    static Double_t OrsayOVCrysTh[EnerGe::NbOrsayOVCrystals];
    static Bool_t ApplyResol;

    static Double_t A_resol[EnerGe::NbOrsayOVCrystals];
    static Double_t B_resol[EnerGe::NbOrsayOVCrystals];
    static Double_t C_resol[EnerGe::NbOrsayOVCrystals];
    static const Int_t NCoincs = 7;
    static TString Coinc_names[NCoincs];
    
    ClassDef(Algo,3);
};
#endif

