#ifndef ALGO_LIST_HH
#define ALGO_LIST_HH 1

#include <RtypesCore.h>
#include <TString.h>

#include <map>

class Algo;

class AlgoList {
public:
  ~AlgoList() { Clear(); }
  AlgoList() {}
  AlgoList(const AlgoList&) = delete;

  inline static std::map<TString,Algo*>& GetList() { return List; }
  static Algo* GetAlgo(const TString& algo_name);

  Bool_t HasAlgo(const TString& name) { return List.count(name)!=0; }
  void AddAlgo(const TString& name, Algo* algo);
  void Clear();

  inline std::map <TString, Algo* >::iterator Begin() { return List.begin(); }
  inline std::map <TString, Algo* >::iterator End()   { return List.end(); }

private:
  static std::map<TString,Algo*> List;
};

#endif // ALGO_LIST_HH
