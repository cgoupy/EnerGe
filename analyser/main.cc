#include <Parameters.hh>
#include <analyser.hh>

#include <TCanvas.h>
#include <TDirectory.h>
#include <TFile.h>
#include <TH1.h>
#include <TMath.h>
#include <TPRegexp.h>
#include <TString.h>

#include <sys/stat.h>
#include <unistd.h>  // getopt
#include <algorithm>
#include <cstdlib>
#include <exception>
#include <iostream>
#include <limits>
#include <vector>
using namespace std;


void Err(Int_t code, const TString& text)
{
    std::cerr << " ***Error " << code << ": " << text
    << "\nExiting...\n\t¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤\n\n";
    std::exit(code);
}

void Warn(Int_t code, const TString& text)
{
    std::cerr << " *Warning " << code << ": " << text << "\n Continue...\n";
}

Bool_t CheckFile(TString& file, const TString& in)
{
    struct stat buf;
    if (stat(file.Data(), &buf) == -1) {
        if (stat((in + file).Data(), &buf) == -1) {
            cerr << " CheckFile fail: can not 'stat' file: " << in << file
            << "\n Perhaps using the -r option ?\n";
            return false;
        } else {
            file = in + file;
        }
    }
    return true;
}

namespace path{

constexpr unsigned buffer_size(){
    
    return 4096;
    
}

TString get_current_path(){
    
    TString current_path;
    
    char current_directory_name[buffer_size()];
    if (getcwd(current_directory_name, buffer_size())) current_path = current_directory_name;
    else Err(3, "no output path (even default current one)\nPlease pass your output path with the '-p' option");
    
    if (!TPMERegexp("/$").Match(current_path)) current_path += "/";
    
    return current_path;
    
}

std::string setup_data_path(const char* data_path_name, const char* general_path_name){
    
    std::string data_path;
    
    if(std::getenv(general_path_name)) {
        
        data_path = std::getenv(general_path_name);
        data_path += "/analyser/data";
        setenv(data_path_name, data_path.c_str(),1);
        
    }
    else{
        
        std::string error_message{general_path_name};
        throw std::runtime_error(error_message+" must be defined!");
        
    }
    
    return data_path;
    
}

std::string get_data_path(const char* data_path_name, const char* general_path_name){
    
    const char* data_path = std::getenv(data_path_name);
    return data_path  ?  data_path : setup_data_path(data_path_name, general_path_name);
    
}

std::string get_analysis_settings_file_name(
    const char* analysis_settings_file_name,
    const char* default_analysis_settings_file_name) {

    const char* analysis_settings_file = std::getenv(analysis_settings_file_name);
    return analysis_settings_file ?
          analysis_settings_file : default_analysis_settings_file_name;
}

} // namespace path


void print_usage(std::ostream& output){
    
    output << "USE: analyser [-d] [-b] [-p outpath] [-o optionstring] [-f firstrun] [-l "
    "lastrun] [-r rootpath] runlist1 runlist2... infile1.root infile2.root...\n"
    << " '-d' enable die mode, i.e. die after arguments checking\n"
    << " '-b' enable batch mode, i.e. disable progress bar\n"
    << " '-p' must be followed by the output path\n"
    << "   => BEWARE: the default path is the current one\n"
    << " '-o' must be followed by 1 string containing key words, stuck together\n"
    << "   => if no option is given, a default analysis is performed\n"
    << "     The program then searches to match the key words in the string (and it's case sensitive)\n"
    << "     BEWARE: do not use space in key word string, use '_' or ',' or whatever, but no space, no tab\n"
    << " '-f' must be followed by the first run you want to analyze (i.e. cutting in the run list)\n"
    << " '-l' must be followed by the last run you want to analyze (i.e. cutting in the run list)\n"
    << "   => both runs given by '-f' and '-l' will be analyzed\n"
    << " '-r' must be followed by the path of the root files\n"
    << "   => THIS OPTION IS MANDATORY IF ANY RUNLIST WITH NON ABSOLUTE PATH IS USED\n"
    << "     This path will also be used for direct input files if needed\n"
    << " BEWARE: ANY FILE ENDING WITH '.root' after the options will be assumed as an input root file\n"
    << "         ANY OTHER FILE after the options (whatever the extension) will be assumed as a run list\n"
    << " Available key words controlling algos are:\n"
    << "   simu     (or I): analyze simulated data -> enable simultaneously all available algos\n"
    << "   edep     (or E): analyze simulated data -> energy depositions only\n"
    << "   prim     (or P): analyze simulated data -> information about primary particles only\n"
    << "   second   (or S): analyze simulated data -> fluxes of secondary particles only\n"
    << "   vertex   (or V): analyze simulated data -> vertex only\n"
    << "   orsay   (or O): analyze simulated data -> Orsay OV prototype only\n"
    //  << " Available key word controlling print options are:\n"
    //  << "   print_matrix        : enable matrix printing on the standard output\n"
    //  << "   no_write_histo      : disable histos writing in some algo (just canvas remain)\n"
    //  << "   no_write_in_process : disable writing of canvas or histos during the file processing\n"
    //  << "   no_write_on_ofstream: no write on a text log file\n"
    //  << "   no_print_in_process : disable full info printing on the standard ouput\n"
    << " ex:  ./analyser -o I -p /where/you/want/ infile*.root will do a full analysis of simulated data\n"
    << "Exiting...\n\t________________________________________\n\n";
}

Int_t main(Int_t argc, Char_t** argv)
{
    cout << "\t________________________________________\n\n";
    if (argc < 2) {
        print_usage(std::cout);
        return 1;
    }
    
    // variables
    Char_t c;
    Bool_t Batch = false, Die = false;
    Int_t NbEntries = argc - 1, io = 0;
    TString opt, out, in, FirstRun, LastRun;
    vector<TString> RunList;
    vector<pair<TString, Double_t>> InputFiles;
    TPMERegexp regRoot("\\.root$");
    
    // reading the database
    ParamBase* pdb = ParamBase::GetInputDataBasePtr();
    auto DataBasePath = path::get_data_path("ANALYSIS_DATA_PATH", "ENERGE_PATH");
    auto analysis_settings_file_name = path::get_analysis_settings_file_name("ANALYSIS_SETTINGS_FILE", "analyser_settings.dat");
    pdb->ReadFile((DataBasePath + "/" + analysis_settings_file_name).c_str());
    
    // option treatment
    while ((c = getopt(argc, argv, "-dbp:o:f:l:r:")) != -1) {
        switch (c) {
            case '\1':
                break;  // multiple options (input files and run lists)
            case 'd':
                Die = true;
                NbEntries -= 1;
                io = TMath::Max(optind, io);
                break;
            case 'b':
                Batch = true;
                NbEntries -= 1;
                io = TMath::Max(optind, io);
                break;
            case 'o':
                opt = optarg;
                NbEntries -= 2;
                io = TMath::Max(optind, io);
                break;
            case 'p':
                out = optarg;
                NbEntries -= 2;
                io = TMath::Max(optind, io);
                break;
            case 'f':
                FirstRun = optarg;
                NbEntries -= 2;
                io = TMath::Max(optind, io);
                break;
            case 'l':
                LastRun = optarg;
                NbEntries -= 2;
                io = TMath::Max(optind, io);
                break;
            case 'r':
                in = optarg;
                NbEntries -= 2;
                io = TMath::Max(optind, io);
                break;
            case '?':
                if (isprint(optopt)) {
                    cerr << "***Error 2.1: Unknown option: " << optopt << "  " << isprint(optopt) << endl;
                } else {
                    cerr << "***Error 2.2: Unknown no print option" << endl;
                }
                break;
            default:
                cerr << "***Error 2: Unknown optionnal argument (char " << c << ")\n";
                cerr << "Exiting...\n\t¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤¤\n\n";
                return 2;
        }
    }
    
    if (out.IsNull()) out = path::get_current_path();
    if (not in.IsNull() and !TPMERegexp("/$").Match(in)) in += "/";
    
    // check first and last runs if any
    if (not FirstRun.IsNull()) {
        if (!CheckFile(FirstRun, in)) {
            Err(4, "'first' file not found");
        }
        if (!regRoot.Match(FirstRun)) {
            Err(4, "first run does not end with '.root'");
        }
    }
    if (not LastRun.IsNull()) {
        if (!CheckFile(LastRun, in)) {
            Err(5, "'last' file not found");
        }
        if (!regRoot.Match(LastRun)) {
            Err(5, "last run does not end with '.root'");
        }
    }
    
    // read input files and run lists
    if (NbEntries > 0) {
        InputFiles.reserve(NbEntries);
    } else {
        Err(6, "no input file or run list");
    }
    if (io && io != argc - NbEntries) {
        Err(6, "options must be before input files or run lists");
    }
    
    for (Int_t i = argc - NbEntries; i < argc; i++) {
        TString file = argv[i];
        if (!CheckFile(file, in)) {
            Err(7, "input file " + file + " not found");
        }
        if (regRoot.Match(file)) {
            InputFiles.push_back(make_pair(file, -1.0));
        } else {
            RunList.push_back(file);
        }
    }
    
    // dump run lists in input files
    Int_t FirstRunNum = 0, LastRunNum = numeric_limits<Int_t>::max();
    TPMERegexp regRunNum("^(.+\\.(\\d+)\\.root)");// # (\\d+\\.*\\d*)");
    if (not FirstRun.IsNull()) {
        if (not regRunNum.Match(FirstRun)) {
            Err(8, "can not recognize first run number in: " + FirstRun);
        }
        FirstRunNum = regRunNum[2].Atoi();
    }
    if (not LastRun.IsNull()) {
        if (not regRunNum.Match(LastRun)) {
            Err(8, "can not recognize last run number in: " + LastRun);
        }
        LastRunNum = regRunNum[2].Atoi();
    }
    
    for (const auto& el : RunList) {
        cout << " Reading run list:" << el << endl;
        ifstream is;
        TString line;
        is.open(el, std::ios::in);
        if (!is.good()) Err(9, "can not open run list:" + el);
        while (line.ReadLine(is).good()) {
            if (is.fail()) break;
            if (line == "" or line.BeginsWith('#')) continue;
            if (not regRunNum.Match(line)) Err(9, "can not recognize run number in: " + line);
            if (regRunNum[2].Atoi() < FirstRunNum) continue;
            if (regRunNum[2].Atoi() > LastRunNum) break;
            TString f = regRunNum[1];
            if (regRunNum.NMatches() != 3) Err(9, "line " + line + " has the wrong format");
            if (CheckFile(f, in))
                InputFiles.push_back(make_pair(f, regRunNum[3].Atof()));
            else
                Warn(1, "skipping this file");
        }
        is.close();
    }
    cout << endl;
    
    // check input file vectors: emptyness and double files
    Bool_t erase = true;
    if (InputFiles.empty()) {
        Err(10, "no run to analyse");
    }
    while (erase) {
        erase = false;
        for (auto it = InputFiles.begin(), end = InputFiles.end(); it != end; ++it) {
            for (auto it2 = it + 1; it2 != InputFiles.end(); ++it2) {
                if (!(it->first).CompareTo(it2->first)) {
                    Warn(2, "double file: " + (it2->first) +
                         "\nDeleting this (second) file name from the list.");
                    InputFiles.erase(it2);
                    erase = true;
                    break;
                }
            }
            if (erase) {
                break;
            }
        }
    }
    
    // sort the input file vector
    Bool_t Add0 = false;
    
    for (auto& it : InputFiles) {
        if (not TPMERegexp("(-|\\.)\\d+\\.root$").Match(it.first)) {
            cout << "This file seems to be the first of a list: " << it.first
            << "  => Adding '-0' before .root for sort algorithm\n";
            TPMERegexp("\\.root$").Substitute(it.first, TString("-0.root"), false);
            Add0 = true;
        }
    }
    
    sort(InputFiles.begin(), InputFiles.end());
    
    if (Add0) {
        for (auto& it : InputFiles) {
            if (TPMERegexp("-0\\.root$").Substitute(it.first, TString(".root"), false))
                cout << "Reversing the '-0' before .root of " << it.first << "\n";
        }
    }
    
    //#ifdef DEBUG
    cout << "Runs to analyse:\n";
    for (auto& it : InputFiles) cout << " " << it.first << endl;
    //#endif
    
    vector<TString> in_files;
    
    for (auto& it : InputFiles) { in_files.push_back(it.first); }
    
    
    // stop here if 'die' mode (i.e. test of inputs)
    if (Die) {
        cout << "\n=> Successful test of inputs !\n";
        cout << "\t________________________________________\n\n";
        return 0;
    }
    
    // launch the analyser
    // #ifndef DEBUG // do not catch exception when using gdb to be able to see the backtrace
    // try {
    // #endif
    
    Analyzer theA(in_files, opt, out, Batch);
    theA.ProcessAnalysis();
    
    // #ifndef DEBUG
    // } catch (exception& err) {
    //   cerr << "\n\t~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n";
    //   cerr << "\tException launched in analyser: \n"<<err.what();
    //   cerr << "\n\t~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n";
    //   return 1;
    // }
    // #endif
    
    cout << "\t________________________________________\n\n";
    return 0;
}
