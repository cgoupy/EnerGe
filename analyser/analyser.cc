#include <analyser.hh>

#include <DataDefinition.h>
#include <Parameters.hh>
#include <ProgressBar.hh>
#include <String.hh>

#include <algo.hh>
#include <algo_list.hh>
#include <simu_algo.hh>

#include <RtypesCore.h>
#include <TBranch.h>
#include <TChain.h>
#include <TChainElement.h>
#include <TClass.h>
#include <TClonesArray.h>
#include <TEnv.h>
#include <TFile.h>
#include <TH1.h>
#include <TPRegexp.h>
#include <TString.h>
#include <TStyle.h>

#include <sys/time.h>
#include <bitset>
#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

////////////////////////////////////////////////////////////////

// feel free to add option to turn on/off your analysis, and turn on/off the flags
// feel also free to add algo(s), but do not forget to add it in the AddAlgo method else/if list
void Analyzer::SetFlags() {
    
    if (TPMERegexp("edep|E").Match(Option)) {f_edep = true;}
    
    if (TPMERegexp("vertex|V").Match(Option)) {f_vertex = true;}
    
    if (TPMERegexp("prim|P").Match(Option)) {f_prim = true;}
    
    if (TPMERegexp("second|S").Match(Option)) {f_sec = true;}
    
    if (TPMERegexp("simu|I").Match(Option)) {f_simu = true;}
    
    if (TPMERegexp("orsay|O").Match(Option)) {f_orsay = true;}
    
    if (f_simu == true) {
        AddAlgo("Edep", "mc");
        AddAlgo("Primaries", "mc");
        AddAlgo("Secondaries", "mc");
        AddAlgo("Vertex", "mc");
        AddAlgo("OrsayOV", "mc");
    }
    else {
        if (f_vertex == true) {AddAlgo("Vertex","mc");}
        if (f_edep == true)   {AddAlgo("Edep","mc");}
        if (f_prim == true)   {AddAlgo("Primaries","mc");}
        if (f_sec == true)   {AddAlgo("Secondaries","mc");}
        if (f_orsay == true)   {AddAlgo("OrsayOV","mc");}
    }
    
    NbBranches = f_mc;
    
    if (NbBranches) {
        bout << NbBranches << " branch" << (NbBranches > 1 ? "es" : "")
        << " will be linked to process " << (theAlgoList->GetList().size() > 1 ? "these" : "this")
        << " algorithm" << (theAlgoList->GetList().size() > 1 ? "s" : "")
        << "(mc=" << f_mc
        << ")\n\n";
    } else {
        throw analyser_except("Analyzer::SetFlags2: No algorithm planned to be processed");
    }
}

////////////////////////////////////////////////////////////////

void Analyzer::AddAlgo(const TString& name, const TString& config) {
    if (theAlgoList->HasAlgo(name)) {return;}
    
    if (TPMERegexp("mc").Match(config)) {
        f_mc = true;
    }
    
    if (name == "Edep") {
        theAlgoList->AddAlgo(name, new EdepAlgo(write, Option, &MC));
    }
    
    else if (name == "Primaries") {
        theAlgoList->AddAlgo(name, new PrimariesAlgo(write, Option, &MC));
    }
    
    else if (name == "Secondaries") {
        theAlgoList->AddAlgo(name, new SecondariesAlgo(write, Option, &MC));
    }

    else if (name == "Vertex") {
        theAlgoList->AddAlgo(name, new VertexAlgo(write, Option, &MC));
    }
    
    else if (name == "OrsayOV") {
        theAlgoList->AddAlgo(name, new OrsayOVAlgo(write, Option, &MC));
    }
    
    else {
        berr << "***Error Analyzer::AddAlgo3: Unknown algo " << name << "\t=> Skipping...\n";
        return;
    }
    
    bout << "Planning to process " << name << " analysis of data\n";
}

////////////////////////////////////////////////////////////////

Analyzer::Analyzer(const vector<TString> InFiles, const TString opt, const TString out,
                   Bool_t batch)
: InputFiles(InFiles),
Option(opt),
outpath(out),
Batch(batch),
bout(write.rdbuf()),
berr(write.rdbuf(), std::cerr) {
    SetTime(init_time);
    
    gStyle->SetOptStat(1111111);
    gStyle->SetOptFit(11111);
    
    SetSaveFile();
    SetLogFile();
    SetFlags();
    
    SetChain(chain, "data", "NCF");
    
    NbEntries = chain->GetEntries();
#ifdef DEBUG
    cout << "Chain to process: adress=" << chain << "\n";
    chain->ls();
    cout << "chain->GetNbranches " << chain->GetNbranches() << "\n\n";
#endif
    
    theAlgoList = new AlgoList();
}

////////////////////////////////////////////////////////////////

Analyzer::~Analyzer() {
    bout << "Output data ROOT file: " << outpath << save << "\nOutput log text file: " << outpath
    << log << endl;
    
    theAlgoList->Clear();
    delete theAlgoList;
    
    if (f_mc) {
        delete MC;
    }
    
    BranchesList.clear();
    
    delete chain;
    if (f_simu){
        delete friendchain;
    }
    file->Close();
    delete file;
    
    SetTime(final_time);
    bout << " Total analysis time = " << PrintTime(final_time - init_time) << "\n";
    write << "\n\t----------------------------------------\n\n";
    write.close();
}

////////////////////////////////////////////////////////////////

inline void Analyzer::SetTime(Double_t& time) {
    gettimeofday(&tv, 0);
    time = tv.tv_sec + 1e-6 * tv.tv_usec;
}

////////////////////////////////////////////////////////////////

void Analyzer::SetChain(TChain*& ch, const TString& chain_name, const TString& alt_name) {
    ch = new TChain(chain_name);
    if (not CheckChain(ch)) {
        delete ch;
        ch = new TChain(alt_name);
        if (not CheckChain(ch)) {
            throw analyser_except("Can not find Tree (neither '", chain_name, "' neither '", alt_name,
                                  "')");
        }
    }
}

////////////////////////////////////////////////////////////////

Bool_t Analyzer::CheckChain(TChain* ch) {
    for (auto it = InputFiles.begin(), end = InputFiles.end(); it != end; ++it) {
        if (ch->Add((*it), -1) < 1) {
            berr << "***Error SetChain: Missing file or tree in the file: " << (*it) << endl;
            return false;
        }
    }
    
    TIter next(ch->GetListOfFiles());
    TChainElement* chEl = 0;
    while ((chEl = dynamic_cast<TChainElement*>(next()))) {
        TFile f(chEl->GetTitle());
        if (!f.IsOpen()) {
            berr << "***Error CheckChain: file " << f.GetTitle() << " not opened\n";
            return false;
        }
        if (chEl->GetEntries() == 0) {
            berr << "**Warning CheckChain: file " << f.GetTitle() << " is empty\n";
        }
    }
    if (ch->GetEntries() == 0) {
        berr << "***Error CheckChain: chain " << ch->GetName() << " is empty\n";
        return false;
    }
    
    return true;
}

////////////////////////////////////////////////////////////////

void Analyzer::SetLogFile() {
    log = save;
    TPMERegexp("\\.root$").Substitute(log, "_LOG.txt");
    write.open(outpath + log, ios::out);
    if (!write.is_open()) {
        throw analyser_except("Analyzer::SetLogFile: Can not open ofstream for file: ", outpath, log);
    }
    write << "Creating log: " << log << "\n\nParameters and tables:\n";
    ParamBase::GetDataBasePtr()->WriteText(write);
    write << "\n\nProcessed file(s):\n";
    
    for (auto it = InputFiles.begin(), end = InputFiles.end(); it != end; ++it) {
        write << (*it) << endl;
    }
    write << "Save file: " << save << "\n\n";
}

////////////////////////////////////////////////////////////////

void Analyzer::SetSaveFile() {
    save = InputFiles.front();
    
    if (TPMERegexp("\\[|\\]|\\?|\\+|\\^|\\$").Match(save)) {
        throw analyser_except(
                              "Analyzer::SetSaveFile1: Can not create an output file name from this input file name: ",
                              save);
    }
    
    if (TPMERegexp("\\*").Match(save)) {
        TPMERegexp("\\*.*").Substitute(save, "STAR.root");
    } else if (InputFiles.size() > 1) {
        TPMERegexp Last("(\\d+)\\.root$");
        Last.Match(InputFiles.back());
        if (Last.NMatches() > 1) {
            Last.Substitute(save, "$1-" + Last[1] + ".root");
        }
    }
    
    TString opt = Option;
    TPMERegexp("\\W", "g").Substitute(opt, "_");
    opt += "_";
    
    TPMERegexp FileName("([^/]+)$");
    if (FileName.Match(save)) {
//        if (opt.Contains("M")||opt.Contains("mvtb")){
//            const ParamBase &db(ParamBase::GetDataBase());
//            UInt_t Low = static_cast<UInt_t>(db["Low_panel_coincidence"]);
//            UInt_t Middle = static_cast<UInt_t>(db["Middle_panel_coincidence"]);
//            UInt_t Up = static_cast<UInt_t>(db["Up_panel_coincidence"]);
//            UInt_t Small = static_cast<UInt_t>(db["SmallPM_coincidence"]);
//            opt+=Form("coinc_param_L%d_M%d_U%d_S%d_", Low, Middle, Up, Small);
//        }
        save = "Analyzer_" + opt + FileName[0];
    } else {
        throw analyser_except(
                              "Analyzer::SetSaveFile2: Can not catch save file name from this input file name: ", save);
    }
    
    file = new TFile(outpath + save, "RECREATE");
    if (!file->IsOpen()) {
        throw analyser_except("Analyzer::SetSaveFile3: Can not open this TFile: ", outpath, save);
    }
    
    write << "Creating file: " << save << endl;
    ParamBase::GetDataBasePtr()->WriteRoot(file);
}

////////////////////////////////////////////////////////////////

template <class T>
void Analyzer::SetOneBranch(T** DataClass, TBranch** Branch, const TString& Title) {
    Int_t check_branch = -666;
    
    check_branch = chain->SetBranchAddress(Title, DataClass, Branch);
    if (check_branch != 0) {
        throw analyser_except("***Error Analyzer::SetOneBranch: Can not set branch adress for ", Title,
                              ": ", check_branch);
    }
    BranchesList.push_back(Branch);
#ifdef DEBUG
    cout << "Creating " << Title << " branch\n";
#endif
}

////////////////////////////////////////////////////////////////

void Analyzer::SetBranches() {
    chain->GetNbranches();
    BranchesList.reserve(NbBranches);
    
    if (f_mc) {
        MC = new MCInfo("MC", "MCInfo");
        SetOneBranch(&MC, &MCbranch, "MC");
    }
    
#ifdef DEBUG
    bout << "Using cache\n";
#endif
    // 500 Mo, more give awkward warnings (Warning in <TTreeCache::FillBuffer>: There is more data in
    // this cluster (starting at entry XXX to XXX, current=XXX) than usual ...)
    chain->SetCacheSize(500ll * 1024ll * 1024ll);
    
    for (auto itB = BranchesList.begin(), end = BranchesList.end(); itB != end; ++itB) {
        chain->AddBranchToCache((**itB)->GetName(), true);
    }
    
    bout << "\t----------------------------------------\n";
}

////////////////////////////////////////////////////////////////
//
//void Analyzer::SetRunTime() {
//  Timer = new RunTimer(chain->GetTree(), &REP, &REPbranch, true, write);
//  if (chain->GetNtrees() == 1) {
//    run_time = Timer->GetRunTime();
//  } else {
//    bout << "Be careful: trying to compute run time for more than 1 file, check result\n";
//
//    Timer->DumpRunInfoStartTime();
//    Timer->SetPrint(false);
//    run_time = Timer->GetRunTime();
//    bout << "Adding to run time: " << PrintTime(Timer->GetRunTime()) << " for tree 0\n";
//
//    Long64_t N = 1;
//    for (Int_t k = 0; k < chain->GetNtrees() - 1; k++) {
//      N += chain->GetTree()->GetEntriesFast();
//      chain->LoadTree(N);
//      Timer->SetTree(chain->GetTree());
//      run_time += Timer->GetRunTime();
//      bout << "Adding to run time: " << PrintTime(Timer->GetRunTime()) << " for tree " << k + 1
//           << endl;
//    }
//
//    Timer->SetPrint(true);
//    Timer->DumpRunInfoStopTime();
//    entry = chain->LoadTree(0);
//
//    bout << "RunInfo run time = " << PrintTime(run_time) << "\n";
//  }
//
//  if (run_time < 1) {
//    berr << "Run time too small ! Some algo could crash !\nContinue...\n";
//  }
//}

////////////////////////////////////////////////////////////////

void Analyzer::DisplayRunStatistics() {
    SetTime(final_loop_time);
    bout << "Chain finished with " << NbEvts << " evts over " << NbEntries << " entries ("
    << Double_t(NbEvts) / Double_t(NbEntries) * 100. << " %)\n";
    bout << " Total loop time = " << PrintTime(final_loop_time - init_loop_time)
    << "\n\t----------------------------------------\n";
}

////////////////////////////////////////////////////////////////

void Analyzer::ProcessAnalysis() {
    
    TH1::SetDefaultSumw2(kTRUE);  // enable automatic histo uncertainty management
    SetBranches();
    
    //  entry = chain->LoadTree(0);  // needed by the RunTimer in SetRunTime
    
    bout << "\t----------------------------------------\n";
    
    // initialisation
    for (auto itB = BranchesList.begin(), end = BranchesList.end(); itB != end; ++itB) {
        (**itB)->GetEntry(entry);
    }
    for (auto itA = theAlgoList->Begin(), end = theAlgoList->End(); itA != end; ++itA) {
        if (itA->second->HasHisto()) {
            itA->second->CreateDirectory(file);
            itA->second->Cd();
        }
        itA->second->Initialise(chain); //Here, all Algos should be initialized...
        file->cd();
    }
    
    // current file display
    Int_t current_tree = chain->GetTreeNumber();
    bout << "First file: " << chain->GetFile()->GetName() << endl;
    
    // times
    SetTime(init_loop_time);
    SetTime(analysis_start_time);
    
    // for progress bar
    Char_t* buf = new Char_t[80];  // display buffer
    Double_t previous_time = init_loop_time;
    constexpr Double_t delta_t = 0.5;  // in s
    const ULong64_t NbEvt4Refresh = (f_simu == true ? 100ull : 10000ull);
    
    // BEGINNING OF ANALYSIS, LOOP OVER EVENTS
    
    for (ULong64_t i = 0; i < NbEntries; i++) {
        entry = chain->LoadTree(i);
        if (current_tree != chain->GetTreeNumber()) {
            SetTime(analysis_end_time);
            bout << "\nAnalysis time = " << PrintTime(analysis_end_time - analysis_start_time) << "\n";
            current_tree = chain->GetTreeNumber();
            bout << "\nevt " << i << " => Next file: " << chain->GetFile()->GetName() << "\n";
            SetTime(analysis_start_time);
        }
        
        for (auto itB = BranchesList.begin(), end = BranchesList.end(); itB != end; ++itB) { (**itB)->GetEntry(entry); }  // fill branches
        /* /!\ /!\ /!\ TChain::GetEntry() takes 'i' as argument, TBranch::GetEntry() or
         * TTree::GetEntry() takes 'entry' /!\ /!\ /!\ */
        
        for (auto itA = theAlgoList->Begin(), end = theAlgoList->End(); itA != end; ++itA) {itA->second->Process(i, current_tree);}
        NbEvts++;
        
        if (!Batch && i % NbEvt4Refresh == 0ull) {  // progress bar
            gettimeofday(&tv, 0);
            if (tv.tv_sec + 1e-6 * tv.tv_usec - previous_time >= delta_t) {
                display_progress_bar2(buf, i, NbEntries, init_loop_time, false);
                previous_time = tv.tv_sec + 1e-6 * tv.tv_usec;
            }
        }  // end progress bar
    }    // end loop over evts
    
    if (!Batch) {
        display_end_bar(buf);
    }  // for progress bar
    delete[] buf;
    
    SetTime(analysis_end_time);
    bout << "\nAnalysis time = " << PrintTime(analysis_end_time - analysis_start_time)
    << "\n\t----------------------------------------\n";
    DisplayRunStatistics();
    
    for (auto itA = theAlgoList->Begin(), end = theAlgoList->End(); itA != end; ++itA) {
        bout << " Finalising " << itA->second->GetName() << ":\n";
        itA->second->Cd();
        itA->second->Finalise();
        bout << "\t----------------------------------------\n";
    }
    write << "\n\t----------------------------------------\n\n";
}
