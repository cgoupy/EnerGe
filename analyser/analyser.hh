#ifndef ANALYZER_HH
#define ANALYZER_HH

#include <Index.h>
#include <DualStream.hh>
#include <Exception.hh>

#include <TString.h>

#include <bitset>
#include <fstream>
#include <vector>

class TFile;
class TChain;
class TBranch;
class TClonesArray;
class TH1F;

class AlgoList;
class Algo;
class MCInfo;

class Analyzer {
private:
    class analyser_except : public except {
    public:
        virtual ~analyser_except() noexcept {}
        explicit analyser_except(const std::string& arg) : except(arg) {}
        explicit analyser_except(const std::string& arg, std::ofstream& out) : except(arg, out) {}
        template <typename... Args>
        analyser_except(Args... args) : except(args...) {}
    };
    
public:
    ~Analyzer();
    
    Analyzer(const std::vector<TString> InFiles, const TString opt, const TString out,
             Bool_t batch = false);
    Analyzer() = delete;
    Analyzer(const Analyzer&) = delete;
    
    void ProcessAnalysis();
    
private:
    // internal mechanics
    void SetFlags();
    void SetBranches();
    template <class T>
    void SetOneBranch(T** DataClass, TBranch** Branch, const TString& Title);
    void DisplayRunStatistics();
    void SetTime(Double_t& time);
    void AddAlgo(const TString& name, const TString& config = "");
    void SetChain(TChain*& ch, const TString& chain_name, const TString& alt_name);
    Bool_t CheckChain(TChain* ch);
    void SetSaveFile();
    void SetLogFile();
    
    // Attributes
    // for progress bar
    struct timeval tv;
    
    // arguments
    std::vector<TString> InputFiles;
    TString Option, outpath;
    Bool_t Batch;
    
    // chains, files and streams
    TChain *chain = nullptr, *friendchain = nullptr;
    TFile* file = nullptr;
    TString save, log;
    std::ofstream write;
    DualStreamBuf bout, berr;  // print on screen ('ostream cout') and on the file ('ofstream
    // write'), so Both OUT -> bout. And same for error.
    
    // list of algorithms
    AlgoList* theAlgoList = nullptr;
    
    // data classes
    MCInfo* MC = nullptr;
    //  TClonesArray* TTQ = nullptr;
    
    // branches
    UInt_t NbBranches = 0u;
    TBranch* MCbranch = nullptr;
    //TBranch* TTQbranch = nullptr;
    std::vector<TBranch**> BranchesList;  // or a TCollection (or one of it's daughters) ?
    
    // counters
    ULong64_t entry = 0ull;
    ULong64_t NbEntries = 0ull;
    ULong64_t NbEvts = 0ull;
//     ULong64_t NbEvtsCut = 0ull;
    
    // times
    //Double_t run_time = 0.;
    Double_t init_time = 0.;
    Double_t init_loop_time = 0.;
    Double_t final_time = 0.;
    Double_t final_loop_time = 0.;
    Double_t analysis_start_time = 0.;
    Double_t analysis_end_time = 0.;
    
    // general flags
    Bool_t f_simu = false;    // if true, analysis of simulation - both energy depsotion and vertex
    Bool_t f_edep = false;    // if true, analysis of simulation - energy deposition only
    Bool_t f_prim = false;// if true, analysis of simulation - primary particles only
    Bool_t f_sec = false;// if true, analysis of simulation - secondary particles only
    Bool_t f_vertex = false;    // if true, analysis of simulation - vertex only
    Bool_t f_orsay = false; //if true, analysis of simulation - orsayOV only
    
    // branches flags (can add other flags if we want to analyze other kind of data)
    Bool_t f_mc = false;
};

#endif
