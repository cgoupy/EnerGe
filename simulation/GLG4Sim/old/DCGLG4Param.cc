/**@file DCGLG4Param.cc
   Defines C, Fortran, and C++ API for the
   Double Chooz simple "database" of numeric and string values
   (a string/value hashtable that is initialized from a file).
   Inspired by, and intended as a replacement for, GLG4param.
*/

#include <DCGLG4Param.hh>
#include <DCGLG4Table.hh>

#include <fstream>
#include <sstream>
#include <string>
#include <cstdlib>
#include <stdexcept>

using namespace std;

DCGLG4Param * DCGLG4Param::theDCGLG4Param= nullptr;
DCGLG4Param * DCGLG4Param::theOutputDCGLG4Param= nullptr;


/** Expand directories given by environment variable references in a
    filename.  E.g., one may usefully expand "$GLG4DATA/settings.dat".
    If an environment variable is referenced in the string but not
    defined in the environment, it is replaced with the string given
    by "default".  To make the parsing easy, each environment variable
    reference has to begin with $ and end with / or end-of-string.
    This static member function may be used by anyone.
*/
string DCGLG4Param::ExpandFilename(string s, string def)
{
  string::size_type istartv, ilast;
  string outs;
  ilast= 0;
  while(1) {
    istartv= s.find("$", ilast);
    outs.append(s, ilast, istartv);
    if (istartv==s.npos)
      break;
    ilast= s.find_first_of("/\\",istartv);
    if (ilast==s.npos)
      ilast=s.length();
    string varname( s, istartv+1, ilast-istartv-1 );
    const char *val= getenv( varname.c_str() );
    if (val)
      outs.append(val);
    else
      outs.append(def);
  }
  return outs;
}

// Destructor
DCGLG4Param::~DCGLG4Param()
{
  // delete tables
  for (auto it=tmap.begin(); it!=tmap.end(); it++) { delete (*it).second; }
}

/** ReadFile: read a file.  See class documentation for file format.
    If a parameter is defined more than once, e.g., due to calling ReadFile
    multiple times with different input files, the oflag parameter decides
    whether the first or last definition is used.
    @param  is     input stream
    @param  oflag  indicates how to handle parameter redefinitions
 */
void DCGLG4Param::ReadFile(istream &is, EOverride oflag)
{
  if (fIsForOutput) {
    cerr << "Error: DCGLG4Param::ReadFile() on the \"output only\" db\n"
	 << "   --- a terrible idea, no good could ever come from it.\n";
    throw runtime_error( "DCGLG4Param::ReadFile() on output-only db" );
  }
  while (is.good()) {
    // get a line from the file
    string line;
    getline( is, line );
    if ( is.fail() )
      break;

    // put the line in an istringstream for convenient parsing
    istringstream lineStream(line);

    // parse out name
    string name;
    lineStream >> name;

    // skip blank lines and lines beginning with '#'
    if (lineStream.fail() || name.length()==0 || name[0]=='#' )
      continue;

    // parse out numeric value, if any
    double value;
    bool has_numeric_value=false;
    lineStream >> value;
    if (!lineStream.fail())
      has_numeric_value= true;

    // parse out string value, if any
    string stringvalue;
    bool has_string_value=false;
    {
      string::size_type i= line.find(':');
      if (i != line.npos) {
	if (line[i+1]==' ') i++;  // skip one space after colon, but no more
	stringvalue.assign(line, i+1, line.npos);
	if (stringvalue.length()>0)
	  has_string_value= true;
      }
    }

    // check for invalid line
    if ( has_string_value==false && has_numeric_value==false ) {
      cerr << "Error: DSParam::ReadFile: invalid line, no data for " << name << endl;
      continue;
    }

    // Now save value(s).  By convention, any defined parameter will
    // always have an entry in the string value dictionary, possibly
    // zero-length if no string value was supplied.
    // First check: does name already exist in hash?
    if ( smap.count(name) ) {
      // name exists in hash, so override or keep existing value as appropriate
      std::cout << "Info: DCGLG4Param::ReadFile: "
	   << ( oflag==kKeepExistingValue ? "PRESERVING" : "OVERRIDING" )
	   << " previous setting of key: " << name << " from line: "<<line<<endl;
    } else {
      // name does not exist in hash, so add to vector of keys
      fKeyList.push_back(name);
    }
    if ( smap.count(name)==0 || oflag==kOverrideExistingValue ) {
      // insert values into hash
      smap[name]= stringvalue;
      if (has_numeric_value) { dmap[name]= value; }
    }
  }
}

/** ReadFile: read from a file of the given name.
    Environment variables in filename will be expanded by
    ExpandFilename() before the file open is attempted.
 */
void DCGLG4Param::ReadFile(const char *filename, EOverride oflag)
{
  cout << "\n****************************************************************\n";
  cout << " Reading parameters in file: " << filename;
  cout << "\n****************************************************************\n";

  ifstream ifs;
  ifs.open( ExpandFilename(filename).c_str() );
  if (!ifs.good()) {
    std::cout << "DCGLG4Param::ReadFile : could not open " << filename
	 << " (" << ExpandFilename(filename) << ")\n";
    return;
  }
  ReadFile(ifs, oflag);
  ifs.close();
}

void DCGLG4Param::WriteFile(ostream &os)
{
  // write out parameters in the same order in which they were read in
  os.precision(15);
  for (size_t i=0; i!=fKeyList.size(); i++) {
    const string& key( fKeyList[i] );
    string& sval( smap[key] );
    os << key;
    if ( dmap.count( key ) )
      os << ' '  << dmap[key];
    if ( sval.length() > 0 )
      os << " : " << sval;
    os << endl;
  }
  os.flush();
}


double DCGLG4Param::operator[](string key) const
{
  if ( dmap.count(key)==0 ) {
    // if application code is using this method, it's just reading the db,
    // not assigning to it.  This is always an error in DCGLG4Param if the
    // key is not yet defined.
    cerr << "DCGLG4Param:: attempt to retrieve undefined key " << key
	 << " -- please define in parameter file." << endl;
    throw runtime_error( string("attempt to retrieve undefined key ")+key );
  }
  return (*(dmap.find(key))).second;
}

double& DCGLG4Param::operator[](string key)
{
  
  if ( dmap.count(key)==0 ) {
    if (fIsForOutput) {
      // it's okay to create new keys this way in the output db,
      // unless the key already defined in the user-control db
      if (theDCGLG4Param->dmap.count(key)) {
	throw runtime_error(string("DCGLG4Param::attempt to create key ")+key+
	  " in output db, but it was already defined in user-control db.");
      }
      fKeyList.push_back(key);
      return dmap[key];
    }
    cerr << "DCGLG4Param:: attempt to access undefined key " << key
	 << " -- please define in parameter file." << endl;
    throw runtime_error(string("attempt to access undefined key ")+key);
  }
  return dmap[key];
}
  
double DCGLG4Param::GetWithDefault(string key, double defaultValue) // deprecated!
{
  cerr << "Warning: DCGLG4Param::GetWithDefault is deprecated.  " << key << endl;
  if ( dmap.count(key)==0 ) {
    if (!fIsForOutput)
      cerr << "Error: DCGLG4Param::GetWithDefault: key " << key
	   << " not previously defined.  Please define in parameter file.\n"
	   << "Note: above error will be fatal when GetWithDefault is removed."
	   << endl;
    fKeyList.push_back(key);
    dmap[key]= defaultValue;
  }
  return dmap[key];
}

const string & DCGLG4Param::GetStringValue(string key) const
{
  if ( smap.count(key)==0 ) {
    cerr << "DCGLG4Param:: attempt to retrieve undefined key " << key
	 << " -- please define in parameter file." << endl;
    throw runtime_error( string("attempt to retrieve undefined key ")+key );
  }
  return (*(smap.find(key))).second;
}

void DCGLG4Param::SetStringValue(string key, string value)
{
  if ( smap.count(key)==0 ) {
    if ( fIsForOutput ) {
      // okay to create new key in output db
      fKeyList.push_back(key);
    }
    else {
      cerr << "DCGLG4Param:: attempt to set undefined key " << key
	   << " -- please define in parameter file." << endl;
      throw runtime_error( string("attempt to set undefined key ")+key );
    }
  }
  smap[key]= value;
}


const DCGLG4Table& DCGLG4Param::GetTable(const std::string &tablename) const
{
  if ( tmap.count(tablename)==0 )
    throw runtime_error( string("DCGLG4Param: attempt to access undefined table ")+tablename );
  return *(*(tmap.find(tablename))).second;
}


void DCGLG4Param::DefineTable(const char *filename, const char *tablename)
{
  if ( tmap.count(tablename)!=0 )
    throw runtime_error( string("DCGLG4Param: attempt to redefine already defined table ")+tablename );
  DCGLG4Table *t= new DCGLG4Table(filename);
  tmap[tablename]= t;
  smap[tablename]= string("Table read from ")+filename;
  fKeyList.push_back(tablename);
}
