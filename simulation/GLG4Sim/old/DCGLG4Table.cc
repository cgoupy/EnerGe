/**@file DCGLG4Table.cc
   Declares C, Fortran, and C++ API for
   simple table objects for Double Chooz "database".

   @author G. Horton-Smith
*/

#include <DCGLG4Table.hh>
#include <DCGLG4Param.hh>

#include <fstream>
#include <sstream>
#include <string>
#include <stdexcept>

using namespace std;

/** Create a DCGLG4Table object and initialize it from a file.
    It may seem strange that no "ReadFile()" function is provided, and
    no default DCGLG4Table() constructor is provided, but this is intentional:
    no uninitialized tables are allowed, and once initialized, tables are
    intended to be immutable. */
DCGLG4Table::DCGLG4Table(const char *filename)
{
  ifstream is;
  string line;
  string header;
  int ncol=0;
  int nrow=0;
  fNRows= 0;

  cout << "\n****************************************************************\n";
  cout << " Reading table in file: " << filename;
  cout << "\n****************************************************************\n";

  is.open( DCGLG4Param::ExpandFilename(filename).c_str() );
  if (!is.good())
    throw runtime_error( string("DCGLG4Table: Could not open ")+filename );

  // skip header, keeping 
  while (is.good()) {
    // get a line from the file
    getline( is, line );
    if ( is.fail() )
      break;

    // put the line in an istringstream for convenient parsing
    istringstream lineStream(line);

    // parse out first word in line
    string word;
    lineStream >> word;
    
    // skip blank lines
    if (lineStream.fail() || word.length()==0)
      continue;

    // if this line begins with '#', it might be the last header line
    // skip blank lines and lines beginning with '#'
    if ( word[0]=='#' ) {
      header= line;
      continue;
    }

    // this line is non-blank and non-'#', so it is the start of data
    // be sure to keep value of "line" unchanged until data-parse loop starts!
    break;
  }

  // did we get a header line?
  if (header.length()==0)
    throw runtime_error( string("DCGLG4Table: Could not find header in ")+filename );

  // parse header line
  {
    // put the header line in an istringstream for convenient parsing
    istringstream lineStream(header);
    string word;
    while ( !(lineStream >> word).fail() ) {
      if (word[0]=='#')
	word.erase(0,1);
      if (word.length()==0)
	continue;
      fColNames.push_back(word);
      fCVMap[word].reserve(10); // forces column vector to be defined
      // (the reserve() call is a null operation if default capacity > 10)
      ncol++;
    }
    if ((size_t)ncol != fColNames.size())
      throw logic_error("DCGLG4Table: column length mismatch (logically impossible?)");
  }

  // process the data lines
  vector<double> values;
  values.resize(ncol);
  for (nrow=0 ; is.good(); getline(is,line) ) {
    // note first data line has already been read
    
    // put the line in an istringstream for convenient parsing
    istringstream lineStream(line);
    double value;
    int icol;
    for (icol=0; icol<ncol && !(lineStream >> value).fail(); icol++)
      values[icol]= value;

    // did we get no values?
    if (icol==0)
      continue;  // let's assume this was a comment or blank line

    // did we not get all values?
    if (icol!=ncol) {
      ostringstream errs;
      errs << "DCGLG4Table: bad data in " << filename << ": expected "
	   << ncol << " columns, only got " << icol << " values on row "
	   << nrow << ends;
      throw runtime_error( errs.str() );
    }

    // all is well, push back the data!
    for (icol=0; icol<ncol; icol++)
      fCVMap[fColNames[icol]].push_back(values[icol]);
    nrow++;
  }

  // final book keeping
  fNRows= nrow;
  is.close();
}


DCGLG4Table::~DCGLG4Table()
{
  // nothing to do
}


void DCGLG4Table::CheckColName(const string &colname) const
{
  if ( fCVMap.count(colname)==0 )
    throw runtime_error(string("DCGLG4Table: unknown column ")+colname);
}

/*
// C functions
extern "C" {

  void dcparDefineTable(const char *filename, const char *tablename)
  {
    DCGLG4Param *dbp= DCGLG4Param::GetDBPtr();
    dbp->DefineTable(filename, tablename);
  }
  
  const dcparTablePtr_t dcparGetTable(const char *tablename)
  {
    DCGLG4Param *dbp= DCGLG4Param::GetDBPtr();
    return (dcparTablePtr_t)&(dbp->GetTable(tablename));
  }
  
  int dcparGetNRows(const dcparTablePtr_t tableptr) {
    const DCGLG4Table* t= (const DCGLG4Table*)tableptr;
    return t->GetNRows();
  }
  
  const double* dcparGetColumnData(const dcparTablePtr_t tableptr,
				   const char *colname) {
    const DCGLG4Table* t= (const DCGLG4Table*)tableptr;
    return &((*t)[colname][0]);
  }

  // here are the same functions for Fortran linkage on GCC-Fortran
  void dcpardefinetable_(const char *filename, const char *tablename,
			 long lfn, long ltn)
  {
    DCGLG4Param *dbp= DCGLG4Param::GetDBPtr();
    dbp->DefineTable(string(filename,lfn).c_str(), string(tablename,ltn).c_str());
  }
  
  int dcpargetnrows_(const char *tablename, long ltn) {
    const DCGLG4Table* t= &(DCGLG4Param::GetDBPtr()->GetTable(string(tablename,ltn).c_str()));
    return t->GetNRows();
  }
  
  void dcpargetcolumndata_(const char *tablename,
			   const char *colname,
			   double *dest,
			   int maxlen,
			   long ltn, long lcn) {
    const DCGLG4Table* t= &(DCGLG4Param::GetDBPtr()->GetTable(string(tablename,ltn).c_str()));
    const DCGLG4Table::ColumnVector_t& v = (*t)[string(colname,lcn).c_str()];
    for (int i=0; i<maxlen && i < t->GetNRows(); i++)
      dest[i]= v[i];
  }

}
  */
