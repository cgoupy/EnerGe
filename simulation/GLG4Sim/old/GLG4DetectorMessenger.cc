// This file is part of the GenericLAND software library.
// $Id: GLG4DetectorMessenger.cc,v 1.2 2005/03/29 21:13:28 gahs Exp $
//
// GLG4DetectorMessenger.cc by Glenn Horton-Smith, Dec 1999
////////////////////////////////////////////////////////////////
// GLG4DetectorMessenger
////////////////////////////////////////////////////////////////

#include "GLG4DetectorMessenger.hh"
#include "GLG4DetectorConstruction.hh"

#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWith3VectorAndUnit.hh"
#include "G4UIdirectory.hh"
#include "G4ios.hh"
#include "globals.hh"

#include <stdlib.h>   // for strtol
#include "fstream"    // for file streams



GLG4DetectorMessenger::GLG4DetectorMessenger(GLG4DetectorConstruction* mydetector)
: myDetector(mydetector)
{
  // the GLG4Detector directory
  G4UIdirectory* DetectorDir = new G4UIdirectory("/detector/");
  DetectorDir->SetGuidance("Control the detector geometry options.");

  // the select command
  DetectorSelectCmd = new G4UIcommand("/detector/select", this);
  DetectorSelectCmd->SetGuidance
    ("Select which detector you want to build");
  DetectorSelectCmd->SetGuidance
    ("Use with no parameters to get list of available detector styles.");
  DetectorSelectCmd->AvailableForStates(G4State_PreInit);
  DetectorSelectCmd->SetParameter(new G4UIparameter("which", 's', true));

  // the pmtstyle command
  PmtStyleCmd = new G4UIcmdWithAString("/detector/pmtstyle", this);
  PmtStyleCmd->SetGuidance
    ("Select which PMT shape approximation you want:");
  PmtStyleCmd->SetGuidance
    (" torusstack is the default, and is preferred\n"
     " ellipsoid  is an alternative, for comparison only");
  PmtStyleCmd->AvailableForStates(G4State_PreInit);
  PmtStyleCmd->SetParameterName("which", true, false); // omittable
  PmtStyleCmd->SetCandidates("torusstack ellipsoid");

  // the caldevice command
  CalDeviceCmd =
    new G4UIcommand("/detector/calDevice", this);
  CalDeviceCmd->SetGuidance
    ("Select which calibration device you want to build");
  /*  CalDeviceCmd->SetGuidance
    ("Use with no parameters to get list of available calibration devices.");
  */
  CalDeviceCmd->AvailableForStates(G4State_PreInit);
  CalDeviceCmd->SetParameter(new G4UIparameter("which", 's', true)); 

  // the calposition command
  CalPositionCmd =
    new G4UIcmdWith3VectorAndUnit("/detector/calPosition", this);
  CalPositionCmd->SetGuidance("Set the source position");
  CalPositionCmd->AvailableForStates(G4State_PreInit);
  CalPositionCmd->SetParameterName("x","y","z",false);
  CalPositionCmd->SetDefaultUnit("mm");
}


GLG4DetectorMessenger::~GLG4DetectorMessenger()
{
}


void GLG4DetectorMessenger::
SetNewValue(G4UIcommand * command,G4String newValues)
{
  // DetectorSelectCmd
  if ( command == DetectorSelectCmd ) {
    if (newValues.length()==0) {
      std::cout << "Available detector geometries: ";
      for (int i=0; i<myDetector->GetNumDetectorTypes(); i++)
	std::cout << " " << myDetector->GetDetectorTypeName(i);
      std::cout << std::endl;
    }
    else {
      for (int i=0; i<myDetector->GetNumDetectorTypes(); i++) {
	if (newValues.compare(myDetector->GetDetectorTypeName(i))==0) {
	  myDetector->SetWhichDetector(i);
	  return;
	}
      }
      std::cerr << "Unknown detector style " << newValues << std::endl;
    }
  }

  // PmtStyleCmd
  else if ( command == PmtStyleCmd ) {
    if (newValues == "torusstack" || newValues == "")
      myDetector->SetWhichPmtStyle(kPmtStyle_TorusStack);
    else if (newValues == "ellipsoid") 
      myDetector->SetWhichPmtStyle(kPmtStyle_Ellipsoid);
    else {
      std::cerr << "invalid argument " << newValues << " to /detector/pmtstyle"
  	     << std::endl << std::flush;
    }
  }

  // CalDeviceCmd
  else if ( command == CalDeviceCmd ) {
    myDetector->SetWhichCalibrationDevice(newValues);
  }

  // CalPositionCmd
  else if ( command == CalPositionCmd ) {
    myDetector->SetCalibrationPosition
      ( CalPositionCmd->GetNew3VectorValue(newValues) );
  }
    
  // invalid command
  else {
    std::cerr <<  "invalid detector \"set\" command\n" << std::flush;
  }
}

G4String GLG4DetectorMessenger::
GetCurrentValue(G4UIcommand * command)
{ 
  // DetectorSelectCmd
  if ( command == DetectorSelectCmd ) {
    return myDetector->GetDetectorTypeName( myDetector->GetWhichDetector() );
  }

  // PmtStyleCmd
  else if ( command == PmtStyleCmd ) {
    switch (myDetector->GetWhichPmtStyle())
      {
      case kPmtStyle_TorusStack:
	return G4String("torusstack");
      case kPmtStyle_Ellipsoid:
	return G4String("ellipsoid");
      default:
	return G4String("INVALID VALUE in myDetector->whichPmtStyle !!!");
      }
  }

  // CalDeviceCmd
  else if ( command == CalDeviceCmd ) {
    return myDetector->GetWhichCalibrationDevice();
  }

  // CalPositionCmd
  else if ( command == CalPositionCmd ) {
    return CalPositionCmd->
      ConvertToString(myDetector->GetCalibrationPosition(),"mm");
  }
    
  // invalid command
  else {
    return G4String("invalid GLG4DetectorMessenger \"get\" command");
  }
}


