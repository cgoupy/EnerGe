/**@file DCGLG4Param.hh
   Declares C, Fortran, and C++ API for
   Double Chooz simple parameter "database".

   @author G. Horton-Smith
*/
#ifndef __DCGLG4Param_hh__
#define __DCGLG4Param_hh__

/* Next comes the C++ class definition for DCGLG4Param. */

#include <map>
#include <vector>
#include <string>
#include <iostream>

class DCGLG4Table;

/** DCGLG4Param provides a simple "database" of numeric and string values:
   a string/value hashtable that is initialized from a simple text
   file.  Inspired by, and intended as a replacement for, DCGLG4Param.
   The text file looks something like this:

   @code
   # blank lines and lines beginning with # are ignored

   pi  3.14159265358979312
   model : glisur
   e  2.71828182845904509e+00 : This is 'e' as in exp(1), not the e+ charge.
   q_e 1.60217646200000007e-19 coulomb : Charge of the positron in coulombs
   @endcode

   The above input would define four parameters: a numeric parameter
   named "pi", a string parameter named "model", and two parameters
   "e" and "q_e" having both numeric and string values.  Note that the
   word "coulomb" following the numeric value on the "q_e" line is
   allowed but IGNORED.  The general format for a non-blank,
   non-comment line is

   @code
     key [ numeric_value [ future ] ] [ ':' string_value ]
   @endcode

   where square brackets surround optional components.  Text after the
   numeric value and before the colon is allowed but currently
   ignored.  The string value extends to the end of the line.
   
   Design improvements over DCGLG4Param include the following:
   
   - String values are now supported.  In fact, every parameter may
     have both a numeric and a string value.  For keys that are
     primarily numeric, the string value should be used for a
     description of the meaning of the quantity.
     
   - New parameters may only be defined via ReadFile, not directly
     through operator[].  This may seem like a strange "enhancement",
     but it guards against typos in the argument to operator[] or user
     input.
     
   - "GetWithDefault" was a bad idea, and is deprecated.  If you want
      a default, put it in the text file.

   - Argument "ReadFile" can accept and expand a filename like
     "$GLG4DATA/settings.dat".  In this example, $GLG4DATA would be
     replaced with the value of the GLG4DATA environment variable (if any),
     or with the default "data" if GLG4DATA is not defined.  This is
     to avoid replication of that logic in multiple places.
   
   - A static member function "ExpandFilename", callable by anyone, implements
     the above functionality in full generality.  (See function documentation.)

   - C, Fortran, and (soon) Python interface is supported.

   Designs that were considered and not used for various reasons
   include XML-based, Python-script-based, and
   CLHEP-expression-parser--based approaches.
*/
class DCGLG4Param
{
public:
  static DCGLG4Param& GetDB();
  static DCGLG4Param& GetOutputDB();
  static DCGLG4Param* GetDBPtr(bool wantOutputDB=false);

  enum EOverride { kKeepExistingValue, kOverrideExistingValue };
  void ReadFile(const char *filename, EOverride oflag= kKeepExistingValue);
  void ReadFile(std::istream &is, EOverride oflag= kKeepExistingValue);
  void WriteFile(std::ostream &os);

  double  operator[](std::string key) const;
  double& operator[](std::string key);
  double GetWithDefault(std::string name, double defaultValue); // deprecated!
  const std::string& GetStringValue(std::string name) const;
  void   SetStringValue(std::string name, std::string value);
  const DCGLG4Table& GetTable(const std::string &name) const;
  void DefineTable(const char *filename, const char *tablename);
  inline bool   HasValue(std::string key);
  inline bool   HasStringValue(std::string key);
  inline bool   HasTable(std::string key);
  typedef std::vector<std::string> KeyList_t;
  virtual inline const KeyList_t& GetListOfKeys() const;

  static std::string ExpandFilename(std::string fn, std::string def="data");

private:
  DCGLG4Param(bool argIsForOutput): fIsForOutput(argIsForOutput) {}
  DCGLG4Param(DCGLG4Param &) = delete;
  DCGLG4Param() = delete;
  virtual ~DCGLG4Param();

private:
  std::map<std::string,double> dmap;      // numeric values
  std::map<std::string,std::string> smap; // string values
  std::map<std::string,DCGLG4Table*> tmap;    // tables
  KeyList_t fKeyList; // for preserving order
  bool fIsForOutput;    // output == no user control, "write-only" from app
  static DCGLG4Param * theDCGLG4Param;  // common instance for user-control db
  static DCGLG4Param * theOutputDCGLG4Param;  // common instance for output db
};


// inline functions

/** Get the pointer to the common shared DCGLG4Param, constructing as necessary */
inline DCGLG4Param * DCGLG4Param::GetDBPtr(bool wantOutputDB)
{
  if ( wantOutputDB ) {  return &(GetOutputDB()); }
  else { return &(GetDB()); }
}

/** Get reference to the common shared DCGLG4Param, constructing as necessary */
inline DCGLG4Param & DCGLG4Param::GetDB()
{
  if (theDCGLG4Param == nullptr) { theDCGLG4Param= new DCGLG4Param(false); }
  return *theDCGLG4Param;
}

/** Get reference to the common output DCGLG4Param, constructing as necessary */
inline DCGLG4Param & DCGLG4Param::GetOutputDB()
{
  if (theOutputDCGLG4Param == nullptr) { theOutputDCGLG4Param= new DCGLG4Param(true); }
  return *theOutputDCGLG4Param;
}

/** Test if key is in the numeric parameter table */
inline bool  DCGLG4Param:: HasValue(std::string key)
{
  return (dmap.count(key)>0);
}

/** Test if key is in the string parameter table */
inline bool  DCGLG4Param:: HasStringValue(std::string key)
{
  return (smap.count(key)>0 && smap[key].length()>0);
}

/** Test if key is in the table table */
inline bool  DCGLG4Param:: HasTable(std::string key)
{
  return (tmap.count(key)>0);
}

/** Get list of keys, in the order in which they were read in */
inline const DCGLG4Param::KeyList_t& DCGLG4Param::GetListOfKeys() const
{
  return fKeyList;
}


#endif /* __DCGLG4Param_hh__ */
