// This file is part of the GenericLAND software library.
// $Id: GLG4DetectorMessenger.hh,v 1.2 2005/03/29 21:13:23 gahs Exp $
//
// GLG4DetectorMessenger.hh by Glenn Horton-Smith, Dec. 1999
#ifndef __GLG4DetectorMessenger_hh__
#define __GLG4DetectorMessenger_hh__ 1

#include "G4UImessenger.hh"

class G4UIcommand;
class G4UIcmdWithAString;
class G4UIcmdWith3VectorAndUnit;
class GLG4DetectorConstruction;

class GLG4DetectorMessenger: public G4UImessenger
{
  public:
    GLG4DetectorMessenger(GLG4DetectorConstruction * myDetector);
    ~GLG4DetectorMessenger();
    
    void SetNewValue(G4UIcommand * command,G4String newValues);
    G4String GetCurrentValue(G4UIcommand * command);
    
  private:
    GLG4DetectorConstruction * myDetector;
    
    G4UIcmdWithAString*       PmtStyleCmd;
    G4UIcommand*              DetectorSelectCmd;
    G4UIcommand*              CalDeviceCmd;
    G4UIcmdWith3VectorAndUnit*CalPositionCmd;
};

#endif
