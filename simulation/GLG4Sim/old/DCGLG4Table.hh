/**@file DCGLG4Table.hh
   Declares C, Fortran, and C++ API for
   simple table objects for Double Chooz "database".

   @author G. Horton-Smith
*/
#ifndef __DCGLG4Table_hh__
#define __DCGLG4Table_hh__
/*
#if defined( __cplusplus ) || defined( __CINT__ )
extern "C" {
#endif

  // The following are C interfaces to DCGLG4Table objects.
  
  typedef void * dcparTablePtr_t;
  
  void dcparDefineTable(const char *filename, const char *tablename);
  
  const dcparTablePtr_t dcparGetTable(const char *tablename);
  
  int dcparGetNRows(const dcparTablePtr_t tableptr);
  
  const double* dcparGetColumnData(const dcparTablePtr_t tableptr,
				   const char *colname);

#if defined( __cplusplus ) || defined( __CINT__ )
}
#endif
*/

/* Next comes the C++ class definition for DCGLG4Table. */

#if defined( __cplusplus ) || defined( __CINT__ )

#include <map>
#include <vector>
#include <string>
#include <iostream>

/** A DCGLG4Table is a simple table of numeric values arranged in named
   columns and numbered rows, read from am ascii file and contained entirely
   in memory.  It is a subset of the lowest common denominator of
   the functionality of an ntuple, a TTree, or an SQL table.  Some
   common functions of such tables are not included since they are not
   needed in the anticipated use cases.
   (If more capability is needed, it may be time to use an SQL database.)

   The ascii file for a table looks something like this:

   @code
   # comments
   # comments
   # comments
   # id  x      y      z     u    v    w
   10   1.0    0.0    1.0    0.0  0.0  -1.0
   11  -0.5    0.866  1.0    0.0  0.0  -1.0
   12  -0.5   -0.866  1.0    0.0  0.0  -1.0
   20   0.866  0.5   -1.0    0.0  0.0   1.0
   21  -0.866  0.5   -1.0    0.0  0.0   1.0
   22   0.0   -1.0   -1.0    0.0  0.0   1.0
   @endcode

   Only one table may be defined per file.

   The last line starting with "#" prior to the start of data will be parsed
   for the column names.  Column names are separated by whitespace; column
   names cannot contain white space.

   Typically used in conjuction with DCGLG4Param, which can save each table
   under a key in the global keyword "database".

   Example code fragments:
    Defining a table:
    @code
     DCGLG4Param& db( DCGLG4Param::GetDB() );
     db.DefineTable("$DCDBDIR/pmtcoordinates.dat", "pmtcoordinates");
    @endcode

    Accessing a table:
    @code
     DCGLG4Param& db( DCGLG4Param::GetDB() );
     const DCGLG4Table& pmt( db.GetTable("pmtcoordinates") );
     for (int ipmt=0; ipmt<pmt.NRows(); ipmt++)
       make_PMT( pmt["id"][ipmt],
                 pmt["x"][ipmt], pmt["y"][ipmt], pmt["z"][ipmt],
                 pmt["u"][ipmt], pmt["v"][ipmt], pmt["w"][ipmt] );
    @endcode

    Accessing a table perhaps somewhat more efficiently:
    @code
     DCGLG4Param& db( DCGLG4Param::GetDB() );
     const DCGLG4Table& pmt( db.GetTable("pmtcoordinates") );
     DCGLG4Table::ColumnVector_t& id( pmt["id"] );
     DCGLG4Table::ColumnVector_t& x( pmt["x"] );
     DCGLG4Table::ColumnVector_t& y( pmt["y"] );
     DCGLG4Table::ColumnVector_t& z( pmt["z"] );
     DCGLG4Table::ColumnVector_t& u( pmt["u"] );
     DCGLG4Table::ColumnVector_t& v( pmt["v"] );
     DCGLG4Table::ColumnVector_t& w( pmt["w"] );
     for (int ipmt=0; ipmt<pmt.NRows(); ipmt++)
       make_PMT( id[ipmt], x[ipmt], y[ipmt], z[ipmt],
                 u[ipmt], v[ipmt], w[ipmt]);
    @endcode
*/
class DCGLG4Table
{
public:
  DCGLG4Table(const char *filename);
  ~DCGLG4Table();

  inline int GetNRows() const { return fNRows; }
  inline int NRows() const { return fNRows; }

  typedef std::vector<double> ColumnVector_t;
  typedef std::vector<std::string> ColumnNameVector_t;
  
  inline const ColumnVector_t& operator[](const std::string &colname) const
  { CheckColName(colname); return (*(fCVMap.find(colname))).second; }

  inline const ColumnNameVector_t& GetColumnNames() const
  { return fColNames; }

protected:
  void CheckColName(const std::string &colname) const;
  std::map<std::string,ColumnVector_t> fCVMap;  // map of column vectors
  ColumnNameVector_t fColNames;                 // column names, in order
  int fNRows;
};



#endif /* C++ */


#endif /* __DCGLG4Table_hh__ */
