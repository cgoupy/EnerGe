// This file is part of the GenericLAND software library.
// $Id: GLG4InputDataReader.cc,v 1.2 2005/03/29 21:13:29 gahs Exp $
//
// GLG4InputDataReader.cc
// v.0 by Glenn Horton-Smith, Feb 12, 1999

#include <GLG4InputDataReader.hh>

#include <G4String.hh>
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <G4MaterialPropertiesTable.hh>
#include <G4Material.hh>

#include <ctype.h>

using namespace std;

G4int GLG4InputDataReader::ReadMaterials(istream &is)
{
  const G4String funcname("  GLG4InputDataReader::ReadMaterials: ");

  G4Material* currentMaterial = nullptr;
  G4MaterialPropertiesTable * currentMPT = nullptr;
  G4MaterialPropertyVector *currentPV = nullptr;

  MyTokenizer t(is);
  G4int wavelength_opt= 0;
  G4int errorCount=0;

  while (t.nextToken() != MyTokenizer::TT_EOF ) {

    // expect either a pair of numbers or a keyword
    if (t.ttype == MyTokenizer::TT_STRING) {
      if (t.sval == "MATERIAL") {
	if (t.nextToken() == MyTokenizer::TT_STRING) {
	  currentMaterial= G4Material::GetMaterial(t.sval,false);
	  currentPV= nullptr;
	  wavelength_opt= 0;
	  if (currentMaterial == nullptr) {
	    currentMPT= nullptr;
	    cout << funcname << "material not created: "<<t.sval<<"\tSkipping its properties\n";
// 	    errorCount++;    // error message issued in GetMaterial
	  } else {
	    currentMPT= currentMaterial->GetMaterialPropertiesTable();
	    if (currentMPT == nullptr) {
	      currentMPT= new G4MaterialPropertiesTable();
	      currentMaterial->SetMaterialPropertiesTable(currentMPT);
	    }
	  }
	} else {
	  cerr << funcname << " expected string after MATERIAL\n";
	  errorCount++;
	}
      } else if (t.sval == "PROPERTY") {
	if (t.nextToken() == MyTokenizer::TT_STRING) {
	  currentPV= nullptr;
	  wavelength_opt= 0;
	  if (currentMPT != nullptr) {
	    currentPV = currentMPT->GetProperty(t.sval.data());
	    if (currentPV == nullptr) {
	      currentPV= new G4MaterialPropertyVector();
	      currentMPT->AddProperty(t.sval.data(), currentPV);
	    }
	  }
	} else {
	  cerr << funcname << " expected string after PROPERTY\n";
	  errorCount++;
	}
      } else if (t.sval == "OPTION") {
	if (t.nextToken() == MyTokenizer::TT_STRING) {
	  if (t.sval == "wavelength")          { wavelength_opt= 1; }
	  else if (t.sval == "dy_dwavelength") { wavelength_opt= 2; }
	  else if (t.sval == "energy")         { wavelength_opt= 0; }
	  else {
	    cerr << funcname << " unknown option " << t.sval << endl;
	    errorCount++;
	  }
	} else {
	  cerr << funcname << " expected string after OPTION\n";
	  errorCount++;
	}
      } else {
	cerr << funcname << " unknown keyword " << t.sval << endl;
	errorCount++;
      }
    } else if (t.ttype == MyTokenizer::TT_NUMBER) {
      G4double E_value= t.nval;
      if (t.nextToken() == MyTokenizer::TT_NUMBER) {
	G4double p_value= t.nval;
	if (currentMPT != nullptr && currentPV != nullptr) {
	  if (wavelength_opt) {
	    if (E_value != 0.0) {
	      G4double lam = E_value;
	      E_value = 2*pi*hbarc/(lam*nanometer);
	      if (wavelength_opt == 2) { p_value *= lam / E_value; }
	    } else {
	      cerr << funcname << " zero wavelength!\n";
	      errorCount++;
	    }
	  }
	  currentPV->InsertValues(E_value, p_value);
	} /*else {
	  cerr << funcname << " got number pair, but have no pointer to ";
	  if (currentMPT == nullptr) { cerr << "MaterialPropertyTable "; }
	  if (currentPV == nullptr) { cerr << "MaterialPropertyVector "; }
	  cerr << endl;
	  errorCount++;
	}*/
      } else {
	cerr << funcname << " expected second number, but tokenizer state is ";
	t.dumpOn(cerr);
	cerr << endl;
	errorCount++;
      }
    } else {
      cerr << funcname << " expected a number or a string, but tokenizer state is ";
      t.dumpOn(cerr);
      cerr << endl;
      errorCount++;
    }
  }

  return errorCount;
}


void GLG4InputDataReader::MyTokenizer::dumpOn(ostream &os)
{
  os << "GLG4InputDataReader::MyTokenizer[ttype=" << ttype
    << ",nval=" << nval << ",sval=" << sval << "] ";
}


G4int GLG4InputDataReader::MyTokenizer::nextToken(void)
{
  G4int i=0;
  G4bool negateFlag=false;
  do {
    i= isptr->get();
    if (i=='+')
      i= isptr->get();
    if (i=='-') {
      i= isptr->get();
      negateFlag= !negateFlag;
    }
    if (i=='#') {  // comment to end of line
      do {
	i= isptr->get();
      } while (i!=EOF && i!='\n');
    }
    if (i== EOF)
      return (ttype=TT_EOF);
  } while (isspace(i));

  if (isdigit(i) || i=='.') {
    nval= 0.0;
    isptr->putback(i);
    (*isptr) >> nval;
    if (negateFlag)
      nval= -nval;
    return (ttype=TT_NUMBER);
  }
  else if (negateFlag) {
    isptr->putback(i);
    return (ttype='-');
  }
  else if (isalpha(i) || i=='_') {
    isptr->putback(i);
    (*isptr) >> sval;
    return (ttype=TT_STRING);
  }
  else if (i=='"') {
    sval="";
    while (true) {
      i= isptr->get();
      while (i=='\\') {
	i= isptr->get();
	sval.append((char)i);
	i= isptr->get();
      }
      if (i==EOF || i=='"')
	break;
      sval.append((char)i);
    }
    return (ttype=TT_STRING);
  }
  else {
    return (ttype=i);
  }
}
