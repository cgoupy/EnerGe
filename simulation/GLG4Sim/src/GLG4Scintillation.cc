/** @file GLG4Scint.cc
    For GLG4Scint class, providing advanced scintillation process.
    Distantly based on an extensively modified version of G4Scintillation.cc.

    This file is part of the GenericLAND software library.
    $Id: GLG4Scint.cc,v 1.18 2009/08/28 16:46:54 gahs Exp $

    @author Glenn Horton-Smith (Tohoku) 28-Jan-1999
*/

// [see detailed class description in GLG4Scint.hh]

#include <GLG4Scintillation.hh>
#include <Parameters.hh>

#include <G4Timer.hh>
#include <Randomize.hh>
#include <G4UIcmdWithAString.hh>
#include <G4UIdirectory.hh>
#include <G4TrackFastVector.hh>  // for G4TrackFastVectorSize
#include <G4MaterialPropertiesTable.hh>
#include <G4Material.hh>
#include <G4ParticleMomentum.hh>
#include <G4Step.hh>
#include <G4OpticalPhoton.hh>
#include <G4DynamicParticle.hh>


namespace CLHEP { } // in case of old, non-namespace CLHEP version
using namespace CLHEP;
using namespace std;

/////////////////////////
// Class Implementation
/////////////////////////

////////////////
// static data members
////////////////

vector<GLG4Scintillation *> GLG4Scintillation::masterVectorOfGLG4Scint;
// top level of scintillation command
G4UIdirectory* GLG4Scintillation::GLG4ScintDir = nullptr;

// universal maximum number of secondary tracks per step for GLG4Scint
//G4int GLG4Scint::maxTracksPerStep = G4TrackFastVectorSize;
G4int GLG4Scintillation::maxTracksPerStep = 180000;

// universal mean number of true photons per secondary track in GLG4Scint
G4double GLG4Scintillation::meanPhotonsPerSecondary = 1.0;

// universal on/off flag
G4int GLG4Scintillation::isUsed = false;
G4bool GLG4Scintillation::doScintillation = true;
G4bool GLG4Scintillation::doReemission = true;

// energy deposition
G4double GLG4Scintillation::totEdep = 0.0;
G4double GLG4Scintillation::totEdep_quenched = 0.0;
G4double GLG4Scintillation::totNumPhotons = 0.0;
G4int GLG4Scintillation::totNumSecondaries = 0;
G4ThreeVector GLG4Scintillation::scintCentroidSum = G4ThreeVector();


/////////////////
// Constructors
/////////////////

GLG4Scintillation::GLG4Scintillation(const G4String& tablename, G4double lowerMassLimit): myLowerMassLimit(lowerMassLimit)
{
  isUsed++;
  verboseLevel = static_cast<G4int>(ParamBase::GetDataBase()["GLG4scint_verboselevel"]);

  myPhysicsTable = MyPhysicsTable::FindOrBuild( tablename );
  myPhysicsTable -> IncUsedBy();
  if(verboseLevel) { myPhysicsTable->Dump(); }

  // add to ordered list
  if ( masterVectorOfGLG4Scint.size() == 0
    || lowerMassLimit >= masterVectorOfGLG4Scint.back()->myLowerMassLimit
  ) {
    masterVectorOfGLG4Scint.push_back(this);
  } else {
    for (auto it = masterVectorOfGLG4Scint.begin(); it != masterVectorOfGLG4Scint.end(); ++it) {
      if (lowerMassLimit < (*it)->myLowerMassLimit) {
	masterVectorOfGLG4Scint.insert(it, this);
	break;
      }
    }
  }

  // create UI commands if necessary
  if (GLG4ScintDir == nullptr) {
    // the scintillation control commands
    GLG4ScintDir = new G4UIdirectory("/glg4scint/");
    GLG4ScintDir->SetGuidance("scintillation process control.");
    G4UIcommand *cmd;
    cmd= new G4UIcommand("/glg4scint/on", this);
    cmd->SetGuidance("Turn on scintillation");
    cmd= new G4UIcommand("/glg4scint/off", this);
    cmd->SetGuidance("Turn off scintillation");
    cmd= new G4UIcommand("/glg4scint/reemission", this);
    cmd->SetGuidance("Turn on/off reemission of absorbed opticalphotons");
    cmd->SetParameter(new G4UIparameter("status", 's', false));
    cmd= new G4UIcommand("/glg4scint/maxTracksPerStep",this);
    cmd->SetGuidance("Set maximum number of opticalphoton tracks per step\n"
		      "(If more real photons are needed, "
		      "weight of tracked particles is increased.)\n" );
    cmd->SetParameter(new G4UIparameter("maxTracksPerStep", 'i', false));
    cmd= new G4UIcommand("/glg4scint/meanPhotonsPerSecondary",this);
    cmd->SetGuidance("Set mean number of \"real\" photons per secondary\n");
    cmd->SetParameter(new G4UIparameter("meanPhotonsPerSecondary",'d', false));
    cmd= new G4UIcommand("/glg4scint/verbose",this);
    cmd->SetGuidance("Set verbose level");
    cmd->SetParameter(new G4UIparameter("level", 'i', false));
    cmd= new G4UIcommand("/glg4scint/dump",this);
    cmd->SetGuidance("Dump tables");
  }

  cout << " Using GLG4Scint[" << tablename << "]\n";
}

////////////////
// Destructors
////////////////

GLG4Scintillation::~GLG4Scintillation()
{
  --isUsed;
  myPhysicsTable -> DecUsedBy();
  for (auto i=masterVectorOfGLG4Scint.begin(); i != masterVectorOfGLG4Scint.end(); i++) {
    if (*i == this) { masterVectorOfGLG4Scint.erase(i); }
  }
}


////////////
// Methods
////////////

// PostStepDoIt
// -------------
//

#ifdef G4DEBUG
G4double GLG4Scint_tottime = 0.0;
G4int GLG4Scint_num_calls= 0;
G4int GLG4Scint_num_phots= 0;
#endif


// This routine is called for each step of any particle
// in a scintillator.  For accurate energy deposition, must be called
// from user-supplied UserSteppingAction, which also must stack
// any particles created.  A pseudo-Poisson-distributed number of
// photons is generated according to the scintillation yield formula,
// distributed evenly along the track segment and uniformly into 4pi.

G4VParticleChange* GLG4Scintillation::PostPostStepDoIt(const G4Track& aTrack, const G4Step& aStep)
{
#ifdef G4DEBUG
  G4Timer timer;
  timer.Start();
  GLG4Scint_num_calls ++;
#endif
  G4bool flagReemission= false;
{
  // prepare to generate an event, organizing to
  // check for things that cause an early exit.
  aParticleChange.Initialize(aTrack);

  if ( aTrack.GetDefinition() == G4OpticalPhoton::OpticalPhoton() ) {
    flagReemission = doReemission
      && aTrack.GetTrackStatus() == fStopAndKill
      && aStep.GetPostStepPoint()->GetStepStatus() != fGeomBoundary;
    if (!flagReemission) { goto PostStepDoIt_DONE; }
  }

  G4double TotalEnergyDeposit = aStep.GetTotalEnergyDeposit();
  if (TotalEnergyDeposit <= 0.0 && !flagReemission) {  goto PostStepDoIt_DONE; }

  const G4Material* aMaterial = aTrack.GetMaterial();

  // get pointer to the physics entry
  const MyPhysicsTable::OpProp* theEntry = myPhysicsTable->GetOpPropPtr( aMaterial->GetIndex() );
  if (theEntry == nullptr) { goto PostStepDoIt_DONE; }

  // G4PhysicsOrderedFreeVector is very badly written: accessor are not const
  // So we have to explicitely break the constness with const_cast
  G4PhysicsOrderedFreeVector& VolatileScint(const_cast<G4PhysicsOrderedFreeVector&>(theEntry->scint));
  G4PhysicsOrderedFreeVector& VolatileReemission(const_cast<G4PhysicsOrderedFreeVector&>(theEntry->reemission));
  G4PhysicsOrderedFreeVector& VolatileTime(const_cast<G4PhysicsOrderedFreeVector&>(theEntry->time));

  // Retrieve the Light Yield or Scintillation Integral for this material
  G4double ScintillationYield = theEntry->light_yield;
  if (theEntry->scint.GetVectorLength() == 0) { goto PostStepDoIt_DONE; }

  // If no LY defined Max Scintillation Integral == ScintillationYield
  if (!ScintillationYield) {
    // G4PhysicsOrderedFreeVector is badly written: GetMaxValue is not const...
    ScintillationYield = VolatileScint.GetMaxValue();
  }
  // set positions, directions, etc.
  G4StepPoint* pPreStepPoint  = aStep.GetPreStepPoint();
  G4StepPoint* pPostStepPoint = aStep.GetPostStepPoint();

  G4ThreeVector x0 = pPreStepPoint->GetPosition();
  G4ThreeVector p0 = pPreStepPoint->GetMomentumDirection();
  G4double      t0 = pPreStepPoint->GetGlobalTime();

  // Finally ready to start generating the event
  // figure out how many photons we want to make

  G4int numSecondaries;
  G4double weight;

  if (flagReemission) {
    G4MaterialPropertyVector* mpv_scint_reemission= aMaterial->GetMaterialPropertiesTable()->GetProperty("REEMISSION_PROB");
    if (mpv_scint_reemission == nullptr) { goto PostStepDoIt_DONE; }

    G4double p_reemission= mpv_scint_reemission->Value(aTrack.GetKineticEnergy());
    if (G4UniformRand() >= p_reemission) { goto PostStepDoIt_DONE; }

    numSecondaries= 1;
    weight= aTrack.GetWeight();
  } else {
    // apply Birk's law
    G4double birksConstant = theEntry->birksConstant;
    G4double QuenchedTotalEnergyDeposit= TotalEnergyDeposit;
    if ( birksConstant != 0.0 ) {
//       G4double dE_dx = TotalEnergyDeposit / aStep.GetStepLength();
      QuenchedTotalEnergyDeposit /= (1.0 + birksConstant * TotalEnergyDeposit / aStep.GetStepLength());
    }

    // track total edep, quenched edep
    totEdep += TotalEnergyDeposit;
    totEdep_quenched += QuenchedTotalEnergyDeposit;
    scintCentroidSum += QuenchedTotalEnergyDeposit * ( x0 + p0*(0.5*aStep.GetStepLength()) );

    // now we are done if we are not actually making photons here
    if ( !doScintillation ) { goto PostStepDoIt_DONE; }

    // calculate MeanNumPhotons
    G4double MeanNumPhotons = ScintillationYield
      * QuenchedTotalEnergyDeposit
      * (1.0 + birksConstant * (theEntry->ref_dE_dx) );

    if (MeanNumPhotons <= 0.0) { goto PostStepDoIt_DONE; }
    totNumPhotons += MeanNumPhotons;

    // randomize number of TRACKS (not photons)
    // this gets statistics right for number of PE after applying
    // boolean random choice to final absorbed track (change from
    // old method of applying binomial random choice to final absorbed
    // track, which did want poissonian number of photons divided
    // as evenly as possible into tracks)
    // Note for weight=1, there's no difference between tracks and photons.
    G4double MeanNumTracks= MeanNumPhotons/meanPhotonsPerSecondary;

    G4double resolutionScale= theEntry->resolutionScale;
    if (MeanNumTracks > 12.0) {
      numSecondaries= (G4int)(RandGauss::shoot(MeanNumTracks, resolutionScale * sqrt(MeanNumTracks)));
    } else {
      if (resolutionScale > 1.0) {
	MeanNumTracks = RandGauss::shoot (MeanNumTracks,  sqrt( resolutionScale*resolutionScale-1.0 )*MeanNumTracks);
      }
      numSecondaries= (G4int)( RandPoisson::shoot(MeanNumTracks) );
    }

    weight= meanPhotonsPerSecondary;
    if (numSecondaries > maxTracksPerStep) {
      // it's probably better to just set meanPhotonsPerSecondary to
      // a big number if you want a small number of secondaries, but
      // this feature is retained for backwards compatibility.
      weight= weight * numSecondaries/maxTracksPerStep;
      numSecondaries= maxTracksPerStep;
    }
    totNumSecondaries += numSecondaries;

    if (verboseLevel > 1) {
      cout << "GLG4Scintillation::PostPostStepDoIt:\n"
	<< "  step Edep: "<<TotalEnergyDeposit/MeV << " MeV\n"
	<< "  step quenched Edep: "<<QuenchedTotalEnergyDeposit/MeV << " MeV\n"
	<< "  mean number of photons: "<<MeanNumPhotons << "\n"
	<< "  mean number of tracks: "<<MeanNumTracks << "\n"
	<< "  number of secondaries: "<<numSecondaries << "\n";
    }
  }

  // if there are no photons, then we're all done now
  if (numSecondaries <= 0) {
      // return unchanged particle and no secondaries
      aParticleChange.SetNumberOfSecondaries(0);
      goto PostStepDoIt_DONE;
    }

  // Okay, we will make at least one secondary.
  // Notify the proper authorities.
  aParticleChange.SetNumberOfSecondaries(numSecondaries);
  if (!flagReemission) {
    if (aTrack.GetTrackStatus() == fAlive) {
      aParticleChange.ProposeTrackStatus(fSuspend);
    }
  }

  // now look up waveform information we need to add the secondaries
  for (G4int iSecondary = 0; iSecondary < numSecondaries; iSecondary++) {
    // Determine photon momentum
    G4double sampledMomentum;

    if (verboseLevel>2) {
      cout << "GLG4Scintillation::PostPostStepDoIt: ";
    }
    if ( !flagReemission ) {
      /// normal scintillation
      // G4PhysicsOrderedFreeVector is badly written: GetMaxValue and Energy are not const...
      G4double CIIvalue = G4UniformRand() * VolatileScint.GetMaxValue();
      // G4PhysicsOrderedFreeVector is VERY badly written: Energy() is very different from GetEnergy() !!!
      sampledMomentum = VolatileScint.GetEnergy(CIIvalue);

      if (verboseLevel>2) {
	cout << " Normal scintillation\n"
	  << "  VolatileScint length = " << VolatileScint.GetVectorLength() << "\n"
	  << "  CIIvalue             = " << CIIvalue << " u.a.\n"
	  << "  sampled momentum     = " << sampledMomentum/eV << " eV\n"
	  << "  wavelength           = " << twopi*hbarc / sampledMomentum/nm << " nm\n";
      }
    } else {
      /// reemission
      G4bool Useless;
      G4double CIIvalue;
      G4MaterialPropertyVector* MPV_FRR = aMaterial->GetMaterialPropertiesTable()->GetProperty("FLUOR_REEMISSION_RATIO");
      G4double p_fluor_reemission= MPV_FRR ? MPV_FRR->Value(aTrack.GetKineticEnergy()) : 0.0;
      if (G4UniformRand() >= p_fluor_reemission) {
	// The wavelength-shifter reemits
	// G4PhysicsOrderedFreeVector is badly written: GetValue and Energy are not const...
	CIIvalue = G4UniformRand() * VolatileReemission.GetValue(aTrack.GetKineticEnergy(), Useless );
	// G4PhysicsOrderedFreeVector is VERY badly written: Energy() is very different from GetEnergy() !!!
	sampledMomentum = VolatileReemission.GetEnergy(CIIvalue);
	if (verboseLevel>2) {
	  cout << " Wavelength-shifter reemission\n";
	}
      } else {
	// The scintillation spectrum (primary fluor) used for reemission
	// G4PhysicsOrderedFreeVector is badly written: GetValue and Energy are not const...
	CIIvalue = G4UniformRand() * VolatileScint.GetValue(aTrack.GetKineticEnergy(), Useless );
	// G4PhysicsOrderedFreeVector is VERY badly written: Energy() is very different from GetEnergy() !!!
	sampledMomentum= VolatileScint.GetEnergy(CIIvalue);
	if (verboseLevel>2) {
	  cout << " Primary fluor reemission\n";
	}
      }
      if (CIIvalue == 0.0) {
	// return unchanged particle and no secondaries
	aParticleChange.SetNumberOfSecondaries(0);
	goto PostStepDoIt_DONE;
      }

      aParticleChange.ProposeLocalEnergyDeposit( aTrack.GetKineticEnergy() - sampledMomentum );

      if (sampledMomentum > aTrack.GetKineticEnergy()) {
	cerr << "Error in GLG4Scintillation: sampled reemitted photon momentum " << sampledMomentum <<
		" is greater than track energy " << aTrack.GetKineticEnergy() << endl;
      }

      if (verboseLevel>2) {
	cout << "  old momentum               = " << aTrack.GetKineticEnergy()/eV << "eV\n"
	     << "  reemitted sampled momentum = " << sampledMomentum/eV << "eV\n"
	     << "  wavelength                 = " << twopi*hbarc / sampledMomentum/nm << " nm\n";
      }
    }

    // Generate random photon direction
    G4double cost = 1. - 2.*G4UniformRand();
    G4double sint = sqrt(1.-cost*cost);  // FIXED BUG from G4Scint

    G4double phi = 2*pi*G4UniformRand();
    G4double sinp = sin(phi);
    G4double cosp = cos(phi);

    G4double px = sint*cosp;
    G4double py = sint*sinp;
    G4double pz = cost;

    // Create photon momentum direction vector

    G4ParticleMomentum photonMomentum(px, py, pz);

    // Determine polarization of new photon

    G4double sx = cost*cosp;
    G4double sy = cost*sinp;
    G4double sz = -sint;

    G4ThreeVector photonPolarization(sx, sy, sz);

    G4ThreeVector perp = photonMomentum.cross(photonPolarization);

    phi = 2*pi*G4UniformRand();
    sinp = sin(phi);
    cosp = cos(phi);

    photonPolarization = cosp * photonPolarization + sinp * perp;
    photonPolarization = photonPolarization.unit();

    // Generate a new photon:
    G4DynamicParticle* aScintillationPhoton = new G4DynamicParticle(G4OpticalPhoton::OpticalPhoton(), photonMomentum);
    aScintillationPhoton->SetPolarization(photonPolarization.x(), photonPolarization.y(), photonPolarization.z());
    aScintillationPhoton->SetKineticEnergy(sampledMomentum);

    // Generate new G4Track object:
    G4ThreeVector aSecondaryPosition;
    G4double deltaTime;
    if (flagReemission) {
      deltaTime= pPostStepPoint->GetGlobalTime() - t0;
      aSecondaryPosition= pPostStepPoint->GetPosition();
    } else {
      G4double delta= G4UniformRand() * aStep.GetStepLength();
      aSecondaryPosition = x0 + delta * p0;

      // start deltaTime based on where on the track it happened
      deltaTime = delta / ((pPreStepPoint->GetVelocity()+ pPostStepPoint->GetVelocity())/2.);
    }

    // delay for scintillation time
    if ( theEntry->time.GetVectorLength() != 0 ) {
      // G4PhysicsOrderedFreeVector is badly written: GetMaxValue and Energy are not const...
      // G4PhysicsOrderedFreeVector is VERY badly written: Energy() is very different from GetEnergy() !!!
      G4double sampledDelayTime = VolatileTime.GetEnergy(G4UniformRand() * VolatileTime.GetMaxValue());
      deltaTime += sampledDelayTime;
    }

    // set secondary time
    G4double aSecondaryTime = t0 + deltaTime;

    // create secondary track
    G4Track* aSecondaryTrack = new G4Track(aScintillationPhoton, aSecondaryTime, aSecondaryPosition);
    aSecondaryTrack->SetWeight( weight );
    aSecondaryTrack->SetParentID(aTrack.GetTrackID());
    aSecondaryTrack->SetCreatorProcess( nullptr );

    // add the secondary to the ParticleChange object
    aParticleChange.SetSecondaryWeightByProcess( true ); // recommended
    aParticleChange.AddSecondary(aSecondaryTrack);
    aSecondaryTrack->SetWeight( weight );
    // The above line is necessary because AddSecondary() overrides
    // our setting of the secondary track weight, in Geant4.3.1 & earlier.
    // (and also later, at least until Geant4.7 (and beyond?)
    //  -- maybe not required if SetWeightByProcess(true) called,
    //  but we do both, just to be sure)
    if (verboseLevel>2) {
      cout << "GLG4Scintillation::PostPostStepDoIt: create a new photon\n"
	<< "  sampled momentum = " << aSecondaryTrack->GetDynamicParticle()->GetKineticEnergy()/eV << " eV\n"
	<< "  wavelength       = " << twopi*hbarc / aSecondaryTrack->GetDynamicParticle()->GetKineticEnergy()/nm << " nm\n"
	<< "  weight           = " << aSecondaryTrack->GetWeight() << "\n"
	<< "  time (global)    = " << aSecondaryTrack->GetGlobalTime()/ns << " ns\n"
	<< "  position         = " << aSecondaryTrack->GetPosition()/mm << " mm\n";
    }
  }
  // done iSecondary loop
  }
 PostStepDoIt_DONE:
#ifdef G4DEBUG
  timer.Stop();
  GLG4Scint_tottime += timer.GetUserElapsed();
  GLG4Scint_num_phots += aParticleChange.GetNumberOfSecondaries();
#endif

  if (verboseLevel>3) {
    cout << "\n Exiting from GLG4Scintillation::PostPostStepDoIt -- Number of secondaries (i.e. photons): "
      << aParticleChange.GetNumberOfSecondaries() << endl;
  } else if ( verboseLevel>0
    and aParticleChange.GetNumberOfSecondaries()>0
    and flagReemission == false) {
    cout << "\n Exiting from GLG4Scintillation::PostPostStepDoIt -- Number of secondaries (i.e. photons): "
      << aParticleChange.GetNumberOfSecondaries() << endl;
  }

  return &aParticleChange;
}


////////////////////////////////////////////////////////////////
// the generic (static) PostPostStepDoIt
G4VParticleChange* GLG4Scintillation::GenericPostPostStepDoIt(const G4Step *pStep)
{
  G4Track *track= pStep->GetTrack();
  G4double mass= track->GetDynamicParticle()->GetMass();

  auto it = masterVectorOfGLG4Scint.begin();
  for (G4int i= masterVectorOfGLG4Scint.size(); (i--) > 1; ++it ) {
      if ( mass < (*it)->myLowerMassLimit ) {
	return (*(--it))->PostPostStepDoIt(*track,*pStep);
      }
    }
  return (*it)->PostPostStepDoIt(*track, *pStep);
}


G4PhysicsOrderedFreeVector* GLG4Scintillation::Integrate_MPV_to_POFV(G4MaterialPropertyVector* inputVector)
{
  G4PhysicsOrderedFreeVector *aPhysicsOrderedFreeVector = new G4PhysicsOrderedFreeVector();
  if (inputVector == nullptr) { return aPhysicsOrderedFreeVector; }

  // Retrieve the first intensity point in vector of (photon momentum, intensity) pairs
  // G4PhysicsOrderedFreeVector is VERY badly written: Energy() is very different from GetEnergy() !!!
  G4double currentPM = inputVector->Energy(0);
  // Notice the perfect symetry of G4PhysicsOrderedFreeVector, to get the the energy and the value of the same bin.
  G4double currentIN = (*inputVector)[0];
  G4double currentCII = 0.0;

  // Create first (photon momentum, Scintillation Integral pair
  aPhysicsOrderedFreeVector->InsertValues(currentPM , currentCII);

  // Set previous values to current ones prior to loop
  G4double prevPM  = currentPM;
  G4double prevCII = currentCII;
  G4double prevIN  = currentIN;

  // loop over all (photon momentum, intensity)  pairs stored for this material
  for (size_t i=1; i<inputVector->GetVectorLength(); i++) {
    // G4PhysicsOrderedFreeVector is VERY badly written: Energy() is very different from GetEnergy() !!!
    currentPM = inputVector->Energy(i);
    // Notice the perfect symetry of G4PhysicsOrderedFreeVector, to get the the energy and the value of the same bin.
    currentIN = (*inputVector)[i];
    currentCII = 0.5 * (prevIN + currentIN);
    currentCII = prevCII + (currentPM - prevPM) * currentCII;

    aPhysicsOrderedFreeVector->InsertValues(currentPM , currentCII);

    prevPM  = currentPM;
    prevCII = currentCII;
    prevIN  = currentIN;
  }
/* Original version for Geant4.9.x < 4.9.5, where G4MaterialPropertyVector could be used as iterators
     // Retrieve the first intensity point in vector of (photon momentum, intensity) pairs

    inputVector->ResetIterator();
    ++(*inputVector);	// advance to 1st entry

    G4double currentIN = inputVector-> GetProperty();

    if (currentIN >= 0.0) {
      // Create first (photon momentum, Scintillation Integral pair

      G4double currentPM = inputVector-> GetPhotonEnergy();
      G4double currentCII = 0.0;

      aPhysicsOrderedFreeVector-> InsertValues(currentPM , currentCII);

      // Set previous values to current ones prior to loop
      G4double prevPM  = currentPM;
      G4double prevCII = currentCII;
      G4double prevIN  = currentIN;

      // loop over all (photon momentum, intensity) pairs stored for this material

      while(++(*inputVector)) {
	currentPM = inputVector->GetPhotonEnergy();
	currentIN=inputVector->GetProperty();
	currentCII = 0.5 * (prevIN + currentIN);
	currentCII = prevCII + (currentPM - prevPM) * currentCII;

	aPhysicsOrderedFreeVector-> InsertValues(currentPM, currentCII);

	prevPM  = currentPM;
	prevCII = currentCII;
	prevIN  = currentIN;
      }
    }
 */
  return aPhysicsOrderedFreeVector;
}

////////////////////////////////////////////////////////////////
// MyPhysicsTable (nested class) definitions
////////////////////////////////////////////////////////////////

////////////////
// "static" members of the class
// [N.B. don't use "static" keyword here, because it means something
//  entirely different in this context.]
////////////////

GLG4Scintillation::MyPhysicsTable* GLG4Scintillation::MyPhysicsTable::head = nullptr;


////////////////
// constructor
////////////////

GLG4Scintillation::MyPhysicsTable::MyPhysicsTable(const G4String& name): Name(name)
{
  // create new physics tables
  for (size_t i=0 ; i < G4Material::GetNumberOfMaterials(); i++) {
    // look for material properties table entry.
    const G4Material* aMaterial = (*G4Material::GetMaterialTable())[i];

    // ask data[i] to Build itself
    data.emplace_back(OpProp(name, i, aMaterial->GetMaterialPropertiesTable()) );
  }
}

////////////////
// destructor
////////////////

GLG4Scintillation::MyPhysicsTable::~MyPhysicsTable()
{
  if (used_by_count != 0) {
    cerr << "Error, GLG4Scint::MyPhysicsTable is being deleted with used_by_count="
      << used_by_count << endl;
    return;
  }
}

////////////////
// member functions
////////////////

void GLG4Scintillation::MyPhysicsTable::Dump(void) const
{
  cout << " GLG4Scint::MyPhysicsTable {\n  name=" << Name
    << "\n  length=" << data.size() << "\n  used_by_count=" << used_by_count << endl;
    for (size_t i=0; i<data.size(); i++) {
    cout << "  data[" << i << "]= { // " << (*G4Material::GetMaterialTable())[i]->GetName() << endl;
    cout<< "   resolutionScale =" << data[i].resolutionScale << endl
        << "   birksConstant   =" << data[i].birksConstant << endl
	<< "   ref_dE_dx       =" << data[i].ref_dE_dx << endl
	<< "   light yield     =" << data[i].light_yield << endl;

  // G4PhysicsOrderedFreeVector is very badly written: accessor are not const, neither is DumpValues
  // So we have to explicitely break the constness with const_cast
    cout << "   scintillation spectrum=\n";
    const_cast<G4PhysicsOrderedFreeVector&>(data[i].scint).DumpValues();

    cout << "   reemission spectrum=\n";
    const_cast<G4PhysicsOrderedFreeVector&>(data[i].reemission).DumpValues();

    cout << "   time spectrum=\n";
    const_cast<G4PhysicsOrderedFreeVector&>(data[i].time).DumpValues();
    cout << "  }\n";
  }
  cout << " }\n";
}

GLG4Scintillation::MyPhysicsTable* GLG4Scintillation::MyPhysicsTable::FindOrBuild(const G4String& name)
{
  // head should always exist and should always be the default (name=="")
  if (head == nullptr) { head = new MyPhysicsTable(""); }

  MyPhysicsTable* rover = head;
  while (rover) {
    if ( name == (rover->Name) ) { return rover; }
    rover = rover->next;
  }

  rover = new MyPhysicsTable(name);
  rover->next = head->next;  // always keep head pointing to default
  head->next = rover;

  return rover;
}

////////////////
// Build for Entry
////////////////

GLG4Scintillation::MyPhysicsTable::OpProp::OpProp(const G4String& name, G4int iMat, G4MaterialPropertiesTable *aMPT)
{
  // exit, leaving default values, if no material properties
  if (!aMPT) { return; }

  // Retrieve vector of scintillation wavelength intensity
  // for the material from the material's optical
  // properties table ("SCINTILLATION")
  char property_string[80];

  sprintf(property_string, "SCINTILLATION_WLS%s", name.c_str());
  G4MaterialPropertyVector* theReemissionLightVector = aMPT->GetProperty(property_string);

  sprintf(property_string, "SCINTILLATION%s", name.c_str());
  G4MaterialPropertyVector* theScintillationLightVector = aMPT->GetProperty(property_string);

  if (theScintillationLightVector && !theReemissionLightVector) {
    cout<<"\nWarning! Found a scintillator without Re-emission spectrum"
      <<" (probably a scintillator without WLS)\n"
      <<"I will assume that for this material this spectrum is equal "
      <<"to the primary scintillation spectrum...\n";
    theReemissionLightVector=theScintillationLightVector;
  }

  if (theScintillationLightVector) {
    if(aMPT->ConstPropertyExists("LIGHT_YIELD")) {
      light_yield=aMPT->GetConstProperty("LIGHT_YIELD");
    } else {
      cout<<"\nWarning! Found a scintillator without LIGHT_YIELD parameter."
	<<"\nI will assume that for this material this parameter is "
	<<"implicit in the scintillation integral...\n";
    }

    // find the integral
    scint = move(*Integrate_MPV_to_POFV( theScintillationLightVector ));
    reemission = move(*Integrate_MPV_to_POFV( theReemissionLightVector ));

  } else if (MyPhysicsTable::GetDefault() != nullptr) {
    // use default spectra (possibly null)
    scint = MyPhysicsTable::GetDefault()->GetOpProp(iMat).scint;
    reemission = scint;
    // also use corresponding default light_yield (very important)
    light_yield=MyPhysicsTable::GetDefault()->GetOpProp(iMat).light_yield;
  } // else the spectra will be initialized to empty vectors

  // Retrieve vector of scintillation time profile
  // for the material from the material's optical
  // properties table ("SCINTWAVEFORM")

  //strncpy(property_string, ("SCINTWAVEFORM"+name).c_str(),
  //	      sizeof(property_string));
  sprintf(property_string, "SCINTWAVEFORM%s", name.c_str());
  G4MaterialPropertyVector* theWaveForm = aMPT->GetProperty(property_string);

  if (theWaveForm) {
    // do we have time-series or decay-time data?
    if ( theWaveForm->GetMinLowEdgeEnergy() >= 0.0 ) {
      // we have digitized waveform (time-series) data
      // find the integral
      time = move(*Integrate_MPV_to_POFV( theWaveForm ));
    } else {
      // we have decay-time data.
      // sanity-check user's values:
      // issue a warning if they are nonsense, but continue
      if ( theWaveForm->GetMaxLowEdgeEnergy() > 0.0 ) {
	cerr << "GLG4Scint::MyPhysicsTable::Entry::Build(): SCINTWAVEFORM " << name
	<< " has both positive and negative X values. Undefined results will ensue!\n";
      }

      G4int nbins= 1000;
      G4double maxtime= -5.0*(theWaveForm->GetMinLowEdgeEnergy());
      vector<G4double> tval, ival;
      tval.assign(nbins,0.);
      ival.assign(nbins,0.);
      for (G4int bin=0; bin<nbins; bin++) { tval[bin]= bin*maxtime/nbins; }

/** With Geant4.9.x < 4.9.5, the loop was:
      theWaveForm->ResetIterator();
      while (++(*theWaveForm)) {
	G4double ampl = theWaveForm->GetProperty();
	G4double decy = theWaveForm->GetPhotonEnergy();
	for (G4int i=0; i< nbins; i++) { ival[i] += ampl*(1.0-exp(tval[i]/decy)); }
	entry++;
      }
**/

      size_t entry=0;
      while (entry < theWaveForm->GetVectorLength()) {
	for (G4int bin=0; bin<nbins; bin++) {
	  // G4PhysicsOrderedFreeVector is VERY badly written: Energy() is very different from GetEnergy() !!!
	  ival[bin] += (*theWaveForm)[entry]*(1.0-exp(tval[bin]/theWaveForm->Energy(entry)));
	}
	entry++;
      }

      for (G4int bin=0; bin<nbins; bin++) { ival[bin]/= ival[nbins-1]; }
      time = move( G4PhysicsOrderedFreeVector(tval.data(), ival.data(), nbins) );

      // in Geant4.0.0, G4PhysicsOrderedFreeVector makes its own copy
      // of any array passed to its constructor, so ...
    }

  } else if (MyPhysicsTable::GetDefault() != nullptr){
    // use default integral (possibly null)
    time = MyPhysicsTable::GetDefault()->GetOpProp(iMat).time;
  } // else the spectrum will be initialized to empty vectors


  // Retrieve vector of scintillation "modifications"
  // for the material from the material's optical
  // properties table ("SCINTMOD")
  //strncpy(property_string, ("SCINTMOD"+name).c_str(),
  //	      sizeof(property_string));
  sprintf(property_string, "SCINTMOD%s", name.c_str());
  G4MaterialPropertyVector* theScintModVector = aMPT->GetProperty(property_string);
  if (theScintModVector == nullptr) {
    // use default if not particle-specific value given
    theScintModVector = aMPT->GetProperty("SCINTMOD");
  }

  if (theScintModVector) {
    // parse the entries in ScintMod
    //  ResolutionScale= ScintMod(0);
    //  BirksConstant= ScintMod(1);
    //  Ref_dE_dx= ScintMod(2);

/** With Geant4.9.x < 4.9.5, the loop was:
  theScintModVector->ResetIterator();
  while ( ++(*theScintModVector) ) {
    G4double key = theScintModVector->GetPhotonEnergy();
    G4double value = theScintModVector->GetProperty();

    if (key == 0.0) { resolutionScale= value; }
    else if (key == 1.0) { birksConstant= value; }
    else if (key == 2.0) { ref_dE_dx= value; }
    else {
      cerr << "GLG4Scint::MyPhysicsTable::Entry::Build:  Warning, unknown key "
      << key << "in SCINTMOD" << name << endl;
    }
  }
**/

    size_t entry=0;
    while ( entry < theScintModVector->GetVectorLength() ) {
      // G4PhysicsOrderedFreeVector is VERY badly written: Energy() is very different from GetEnergy() !!!
      G4double key = theScintModVector->Energy(entry);
      G4double value = (*theScintModVector)[entry];

      if (key == 0.0) { resolutionScale= value; }
      else if (key == 1.0) { birksConstant= value; }
      else if (key == 2.0) { ref_dE_dx= value; }
      else {
	cerr << "GLG4Scint::MyPhysicsTable::Entry::Build:  Warning, unknown key "
	  << key << "in SCINTMOD" << name << endl;
      }
      entry++;
    }
  }
}


void GLG4Scintillation::SetNewValue(G4UIcommand * command, G4String newValues)
{
  G4String commandName= command -> GetCommandName();
  if (commandName == "on") { doScintillation= true; }
  else if (commandName == "off") { doScintillation= false; }
  else if (commandName == "reemission") {
    char *endptr;
    G4int i= strtol(newValues.c_str(), &endptr, 0);
    if (*endptr != '\0') {  // non-numerical argument
      if ( !(i = strcmp(newValues.c_str(), "on"))) { doReemission = true; }
      else if ( !(i = strcmp(newValues.c_str(), "off"))) { doReemission = false; }
      else {
	cerr << "Command /glg4scint/reemission given unknown parameter: " << newValues
	  << "\n  old value unchanged: " << ( doReemission ? "on\n" : "off\n" );
      }
    } else { doReemission= (i != 0); }
  }
  else if (commandName == "maxTracksPerStep") {
    G4int i= strtol(newValues.c_str(), nullptr, 0);
    if (i > 0) { maxTracksPerStep= i; }
    else { cerr << "Value must be greater than 0, old value unchanged\n"; }
  }
  else if (commandName == "meanPhotonsPerSecondary") {
    G4double d= strtod(newValues.c_str(), nullptr);
    if (d >= 1.0) { meanPhotonsPerSecondary= d; }
    else { cerr << "Value must be >= 1.0, old value unchanged\n"; }
  }
  else if (commandName == "verbose") {
    verboseLevel= strtol(newValues.c_str(), nullptr, 0);
  }
  else if (commandName == "dump") {
    for (auto it = masterVectorOfGLG4Scint.begin(); it != masterVectorOfGLG4Scint.end(); it++) {
      (*it)->DumpInfo();
    }
  } else {
    cerr << "No GLG4Scint command named " << commandName << endl;
  }
  return;
}

G4String GLG4Scintillation::GetCurrentValue(G4UIcommand * command)
{
   G4String commandName= command -> GetCommandName();
   if (commandName == "on" || commandName == "off") {
     return doScintillation ? "on" : "off";
   } else if (commandName == "reemission") {
     return doReemission ? "1" : "0";
   } else if (commandName == "maxTracksPerStep") {
     char outbuff[64];
     sprintf(outbuff, "%d", maxTracksPerStep);
     return G4String(outbuff);
   } else if (commandName == "meanPhotonsPerSecondary") {
     char outbuff[64];
     sprintf(outbuff, "%g", meanPhotonsPerSecondary);
     return G4String(outbuff);
   } else if (commandName == "verbose") {
     char outbuff[64];
     sprintf(outbuff, "%d", verboseLevel);
     return G4String(outbuff);
   } else if (commandName == "dump") {
     return "?/glg4scint/dump not supported";
   } else {
     return (commandName+" is not a valid GLG4Scint command");
   }
}
