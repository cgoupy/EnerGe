// This file is part of the GenericLAND software library.
// $Id: GLG4DeferTrackProc.cc,v 1.3 2007/08/28 20:35:17 gahs Exp $
//
// Process to limit step length to stay within event time
// and defer long tracks (AND tracks which start after event time) using
// defered particle "generator".
//
// Written: G. Horton-Smith, 29-Oct-2001
//
////////////////////////////////////////////////////////////////////////

#include <GLG4DeferTrackProc.hh>

#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <G4Step.hh>
#include <G4VParticleChange.hh>
#include <G4EnergyLossTables.hh>
#include <G4ProcessTable.hh>
#include <GLG4PrimaryGeneratorAction.hh>
#include <geomdefs.hh> // for kInfinity

class G4UImessenger; // for G4ProcessTable.hh

using namespace std;

////////////////////////////////////////////////////////////////

GLG4DeferTrackProc::GLG4DeferTrackProc(const G4String& aName, G4ProcessType aType): G4VProcess(aName,aType)
{
  if (verboseLevel>0) {
    cout << GetProcessName() << " is created "<< endl;
  }

  _generator= GLG4PrimaryGeneratorAction::GetTheGLG4PrimaryGeneratorAction();
  if (_generator == nullptr) {
    G4Exception("GLG4DeferTrackPro","1",FatalException,"No GLG4PrimaryGeneratorAction instance.");
  }
}

////////////////////////////////////////////////////////////////
 
G4double GLG4DeferTrackProc::PostStepGetPhysicalInteractionLength(
                             const G4Track& aTrack,
			     G4double   /* previousStepSize */,
			     G4ForceCondition* condition
			    )
{
  // condition is set to "Not Forced"
  *condition = NotForced;

  // apply maximum time limit
  G4double dTime= (_generator->GetEventWindow() - aTrack.GetGlobalTime());
  // G4double dTime= (100 - aTrack.GetGlobalTime());
  if (dTime <= 0.0) {
    return 1.0/kInfinity;
  }
  G4double beta = (aTrack.GetDynamicParticle()->GetTotalMomentum())/(aTrack.GetTotalEnergy());
  return beta*c_light*dTime;
}

////////////////////////////////////////////////////////////////

G4VParticleChange* GLG4DeferTrackProc::PostStepDoIt(
			     const G4Track& aTrack,
			     const G4Step& /* aStep */
			    )
{
  _generator->DeferTrackToLaterEvent(&aTrack);
  aParticleChange.Initialize(aTrack);
  aParticleChange.ProposeTrackStatus(fStopAndKill);
  return &aParticleChange;
}
