// This file is part of the GenericLAND software library.
// $Id: GLG4PrimaryGeneratorMessenger.cc,v 1.6 2009/09/16 20:50:19 gahs Exp $
//
// GLG4PrimaryGeneratorMessenger.cc by Glenn Horton-Smith, Feb. 1999
// updated Aug. 3-17, 2001, for new GLG4PrimaryGeneratorAction

////////////////////////////////////////////////////////////////
// GLG4PrimaryGeneratorMessenger
////////////////////////////////////////////////////////////////

#include <GLG4PrimaryGeneratorMessenger.hh>
#include <GLG4PrimaryGeneratorAction.hh>

#include <GLG4VertexGen.hh> // for GetState(), SetState()
#include <GLG4PosGen.hh>    // for GetState(), SetState()

#include <Regex.hh>
#include <String.hh>

#include <globals.hh>
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <G4VPhysicalVolume.hh>
#include <G4LogicalVolume.hh>
#include <G4Material.hh>
#include <G4MaterialPropertiesTable.hh>
#include <G4ParticleTable.hh>
#include <G4UIcmdWithAString.hh>
#include <G4UIcmdWithABool.hh>
#include <G4UIdirectory.hh>
#include <G4UImanager.hh>
#include <G4Event.hh>

#include <fstream>  // for file streams
#include <sstream>  // for string streams
#include <iomanip>  // for setw(), etc..
#include <iostream>
#include <exception>
#include <stdexcept>

using namespace std;

GLG4PrimaryGeneratorMessenger::GLG4PrimaryGeneratorMessenger(GLG4PrimaryGeneratorAction* myaction): myAction(myaction)
{
  G4UIdirectory *GenDir= new G4UIdirectory("/generator/");
  GenDir->SetGuidance("Control the primary event generator.");

  ListCmd = new G4UIcommand("/generator/list",this);

  RateCmd = new G4UIcommand("/generator/rates",this);
  RateCmd->SetGuidance("Set/show the event rates (in Hz).");
  RateCmd->SetGuidance("  Choice: index of rate to be set (omit to show all)");
  RateCmd->SetGuidance("          [can also use full name instead of index]");
  RateCmd->SetGuidance("  Value : optional new value for rate (in Hz)");
  RateCmd->SetGuidance("  Flag  : optional new value of \"pileup-only\" condition");
  RateCmd->SetGuidance("          If flag == 1, then events of this type will only");
  RateCmd->SetGuidance("          occur overlapping with other events (within ATWD frame)");

  G4UIparameter *aParam = new G4UIparameter('s');
  RateCmd->SetParameter(aParam);
  aParam->SetParameterName("choice");
  aParam->SetOmittable(true);

  aParam = new G4UIparameter('d');
  RateCmd->SetParameter(aParam);
  aParam->SetParameterName("value");
  aParam->SetParameterRange("value >= 0.0");
  aParam->SetOmittable(true);

  aParam = new G4UIparameter('d');
  RateCmd->SetParameter(aParam);
  aParam->SetParameterName("flag");
  aParam->SetOmittable(true);

  VtxSetCmd= new G4UIcommand("/generator/vtx/set",this);
  VtxSetCmd->SetGuidance("Set vertex generator state");
  VtxSetCmd->SetGuidance("Usage: /generator/vtx/set (vertex_code) [setting]");
  VtxSetCmd->SetGuidance("where (vertex_code) is given in /generator/list");
  VtxSetCmd->SetGuidance("[use either the integer code or the name]");
  VtxSetCmd->SetGuidance("and setting is the state string (in quotes).");
  VtxSetCmd->SetGuidance("Omit [setting] for generator-specific help.");
  VtxSetCmd->SetParameter(new G4UIparameter("event_type", 's', false));
  VtxSetCmd->SetParameter(new G4UIparameter("setting", 's', true));

  PosSetCmd= new G4UIcommand("/generator/pos/set",this);
  PosSetCmd->SetGuidance("Set position generator state");
  PosSetCmd->SetGuidance("Usage: /generator/pos/set (pos_code) [setting]");
  PosSetCmd->SetGuidance("where (pos_code) is given by /generator/list");
  PosSetCmd->SetGuidance("[use either the integer code or the name]");
  PosSetCmd->SetGuidance("and setting is the state string (in quotes).");
  PosSetCmd->SetGuidance("Omit [setting] for generator-specific help.");
  PosSetCmd->SetParameter(new G4UIparameter("event_type", 's', false));
  PosSetCmd->SetParameter(new G4UIparameter("setting", 's', true));

  EventWindowCmd= new G4UIcommand("/generator/event_window", this);
  EventWindowCmd->SetGuidance("Set/show event time window");
  EventWindowCmd->SetParameter(new G4UIparameter("window(ns)",'d',true));

  ChainClipCmd= new G4UIcommand("/generator/chain_clip", this);
  ChainClipCmd->SetGuidance("Set/show chain clipping time");
  ChainClipCmd->SetParameter(new G4UIparameter("chain_clip(ns)",'d',true));

  HzCmd= new G4UIcmdWithABool("/generator/use_hertz", this);
  HzCmd->SetGuidance("set or clear Hertz mode (fixed time interval between events)");
  HzCmd->SetParameterName("use_hertz_mode", false);
}

GLG4PrimaryGeneratorMessenger::~GLG4PrimaryGeneratorMessenger()
{
  delete ListCmd;
  delete RateCmd;
  delete VtxSetCmd;
  delete PosSetCmd;
  delete EventWindowCmd;
  delete ChainClipCmd;
}

void GLG4PrimaryGeneratorMessenger::SetNewValue(G4UIcommand* command, G4String newValues)
{
  if ( command == ListCmd ) {
    cout << "Generator codes and descriptions:\n evt  pos  vtx  name (== posname:vtxname)\n";
    for (G4int i=0; i<myAction->GetNbEventGenerator(); i++) {
      cout << setw(4) << i << " "
           << setw(4) << myAction->GetPositionCodeForEventType(i) << " "
	   << setw(4) << myAction->GetVertexCodeForEventType(i) << "  "
	    << myAction->GetEventTypeName(i) << endl;
    }
    return;
  }

  if ( command == RateCmd ) {
    if (newValues.empty()) { // no argument, just print rate table and end
      cout << "Rates:\n";
      for (G4int i=0; i<myAction->GetNbEventGenerator(); i++) {
      cout << setw(3) << i << " " << setw(7) << myAction->GetEventRate(i)/(1./second)
	  << (myAction->GetEventTriggerCondition(i) ? " * " : "   ")
	  << myAction->GetEventTypeName(i) << endl;
      }
      return;
    }

    G4String FloatPattern("\\d+(?:\\.\\d*)?(?:[eE][+-]?\\d+)?"); // rate can not be negative
    Regex regName(scat("(\\w+):(\\w+)\\s+(",FloatPattern,")(?:\\s+(\\d))?"));
//     Regex regInteger(scat("([0-",myAction->GetNbEventGenerator(),"])\\s+",FloatPattern,"(\\s+(\\d))?"));

    try {
      G4int iPos=-1, iVtx=-1;
      G4double rate = 0.;
      G4int trigger = 0;

      if (regName.Match(newValues)) {
	rate = regName.stod(2);
	trigger = (regName.NbMatch() > 3 ? regName.stoi(3) : 0);

	for (G4int i=0; i<myAction->NbPosGenCodes; i++) {
	  if ( myAction->GetPositionCodeName(i) == regName.at(0) ) { iPos = i; break; }
	}

	for (G4int i=0; i<myAction->NbVertexGenCodes; i++) {
	  if ( myAction->GetVertexCodeName(i) == regName.at(1) ) { iVtx = i; break; }
	}
      } else {
	throw runtime_error(scat("Can not match string: ",newValues));
      }

      if ( iPos < 0 or iPos >= myAction->NbPosGenCodes
	or iVtx < 0 or iVtx >= myAction->NbVertexGenCodes) {
	throw runtime_error(scat("The command must contain a name exactly matching a defined generator:\n   iPos= ",
				 iPos,"  iVtx= ",iVtx,"  pos=",regName.at(0)," vtx=",regName.at(1)," rate=",rate," trigger=",trigger));
      }

      G4int index = myAction->NewEventGenerator(iPos,iVtx,rate*(1./second),trigger);

      cout << setw(3) << "Setting event generator with pos gen n°: " << iPos
        << " and vertex gen n°: " << iVtx
	<< ", called: " << myAction->GetEventTypeName(index)
	<< ", with rate: "<< setw(7) << myAction->GetEventRate(index)/(1./second) << " Hz\n";

      return;
    } catch(exception& err) {
      cerr << "Can not match '"<<newValues<<"': " << err.what()<<endl;
    }
    G4Exception("GLG4PrimaryGeneratorMessenger::SetNewValue","RateCmd1",FatalErrorInArgument,
		scat("Can not recognize event generator name and rate, or code and rate, in: ",newValues).c_str());
  }


  if (command == VtxSetCmd) {
    istringstream is(newValues.c_str());
    G4int index = -1;
    is >> index;
    if (is.fail()) {
      // wasn't an integer, maybe a name.  Look for it. (The stupid way.)
      char id[64];
      is.clear();
      is.get(id,sizeof(id),' ');
      for (G4int i=0; i<myAction->NbVertexGenCodes; i++) {
	if ( strcmp(myAction->GetVertexCodeName(i),id)==0 ) {
	  index= i;
	  break;
	}
      }
    }
    if (index<0 || index>=myAction->NbVertexGenCodes) {
      cerr << "/generator/vtx/set: invalid index or name: arguments are \""
	      << newValues << "\"" << endl;
      return;
    }
    myAction->GetVertexGenerator(index)->SetState( newValues.substr(is.tellg()) );

    return;
  }

  if (command == PosSetCmd) {
    istringstream is(newValues.c_str());
    G4int index = -1;
    is >> index;
    if (is.fail()) {
      // wasn't an integer, maybe a name.  Look for it. (The stupid way.)
      char id[64];
      is.clear();
      is.get(id,sizeof(id),' ');
      for (G4int i=0; i<myAction->NbPosGenCodes; i++) {
	if ( strcmp(myAction->GetPositionCodeName(i),id)==0 ) {
	  index= i;
	  break;
	}
      }
    }
    if (index<0 || index>=myAction->NbPosGenCodes) {
      cerr << "/generator/pos/set: invalid index or name: arguments are \""
	      << newValues << "\"" << endl;
      return;
    }
    myAction->GetPositionGenerator(index)->SetState( newValues.substr(is.tellg()) );
    return;
  }

  if( command == EventWindowCmd ) {
    if (newValues.length()==0) {
      cout << "Current window is " << myAction->GetEventWindow()/ns << " ns\n";
    } else {
      istringstream is(newValues.c_str());
      G4double newWindow= -1.0;
      is >> newWindow;
      if (is.fail() || newWindow <= 0.0) {
	  cerr << "Time window must be positive\n";
      } else {
	myAction->SetEventWindow(newWindow*ns);
      }
    }
  return;
  }

  if( command == ChainClipCmd ) {
    if (newValues.length()==0) {
      cout << "Current clip time is "<< myAction->GetChainClip()/ns << " ns\n";
    } else {
      istringstream is(newValues.c_str());
      G4double newWindow= -1.0;
      is >> newWindow;
      if (is.fail() || newWindow <= 0.0) {
	  cerr << "Time window must be positive\n";
      } else {
	myAction->SetChainClip(newWindow*ns);
      }
    }
  }

  if( command == HzCmd ) {
    myAction->SetHzMode( HzCmd->GetNewBoolValue(newValues) );
    return;
  }

  G4Exception("GLG4PrimaryGeneratorMessenger::SetNewValue","2",FatalErrorInArgument,
    scat("Invalid GLG4 \"set\" command: ",command->GetCommandName(),"  with value: ",newValues).c_str() );
}

G4String GLG4PrimaryGeneratorMessenger::GetCurrentValue(G4UIcommand * command)
{
  if( command == RateCmd ) {
    ostringstream os;
    os << "Rates:\n";
    for (G4int i=0; i<myAction->GetNbEventGenerator(); i++) {
      os << setw(3) << i << " "
	<< setw(7) << myAction->GetEventRate(i)/(1./second)
	<< (myAction->GetEventTriggerCondition(i) ? " * " : "   ")
	<< myAction->GetEventTypeName(i)
	<< endl;
    }
    os << ends;
    G4String rv(os.str());

    return rv;
  }

  if (command == VtxSetCmd) {
    ostringstream os;
    os << "Vertex generator settings:\n";
    for (G4int index=0; index<myAction->NbVertexGenCodes; index++) {
      os << index << "\t" << myAction->GetVertexGenerator(index)->GetState() << '\n';
    }
    os << ends;
    G4String rv(os.str());

    return rv;
  }

  if (command == PosSetCmd) {
    ostringstream os;
    os << "Position generator settings:\n";
    for (G4int index=0; index<myAction->NbPosGenCodes; index++) {
      os << index << "\t" << myAction->GetPositionGenerator(index)->GetState() << '\n';
    }
    os << ends;
    G4String rv(os.str());

    return rv;
  }

  if ( command == EventWindowCmd ) {
    ostringstream os;
    os << myAction->GetEventWindow() << ends;
    G4String rv(os.str());

    return rv;
  }

  if ( command == HzCmd ) {
    return myAction->GetHzMode( ) ? "true" : "false";
  }

  if( command == ChainClipCmd ) {
    ostringstream os;
    os << myAction->GetChainClip() << ends;
    G4String rv(os.str());

    return rv;
  }

  return G4String("invalid GLG4PrimaryGeneratorMessenger \"get\" command");
}
