// This file is part of the GenericLAND software library.
// $Id: GLG4PrimaryGeneratorAction.cc,v 1.8 2009/09/16 20:50:19 gahs Exp $
//
// new GLG4PrimaryGeneratorAction.cc by Glenn Horton-Smith, August 3-17, 2001

////////////////////////////////////////////////////////////////
// GLG4PrimaryGeneratorAction
////////////////////////////////////////////////////////////////

#include <Parameters.hh>	// for ParamBase
#include <Index.h>
using namespace EnerGe;

#include <GLG4Material.hh>

#include <GLG4PrimaryGeneratorAction.hh>
#include <GLG4PrimaryGeneratorMessenger.hh>
#include <GLG4VertexGen.hh>	// for vertex generator
#include <GLG4PosGen.hh>	// for global position generator

#include <globals.hh>
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <G4Event.hh>
#include <G4PrimaryVertex.hh>
#include <G4Track.hh>
#include <Randomize.hh>


#include <cstdio>		// for sprintf
#include <limits>
using namespace std;

// here are the static constants and variables (boring)
GLG4VVertexGen* GLG4PrimaryGeneratorAction::theVertexGenerators[NbVertexGenCodes];
GLG4VPosGen* GLG4PrimaryGeneratorAction::thePositionGenerators[NbPosGenCodes];
GLG4PrimaryGeneratorAction* GLG4PrimaryGeneratorAction::theGLG4PrimaryGeneratorAction=nullptr;
std::vector<GLG4PrimaryGeneratorAction::codepair_t> GLG4PrimaryGeneratorAction::theEventGeneratorCodes;

// static functions not inline (boring)
G4String GLG4PrimaryGeneratorAction::GetEventTypeName(G4int argEventType)
{
  G4String name(thePositionCodeNames[theEventGeneratorCodes.at(argEventType).poscode]);
  name += ":";
  name += (theVertexCodeNames[theEventGeneratorCodes.at(argEventType).vertexcode]);
  return name;
}

const char * GLG4PrimaryGeneratorAction::thePositionCodeNames[NbPosGenCodes] = {
  "NONE",
  "PaintFill",
  "Plane",
  "Point",
  "Sphere",
  "Cylinder",
  "Cuboid"
};

const char* GLG4PrimaryGeneratorAction::theVertexCodeNames[NbVertexGenCodes] = {
  "Gun",
  "GPS",
  "HEPevt",
//  "Delayed_particle",
  "Source_Gamma",
  "Source_Mono_Beta",
  "Source_Multiple_Betas",
  "Source_All_Decays",
  "Cosmogenic_Neutrons",
  "Atmospheric_Neutrons",
  "Atmospheric_Muons",
  "Ambient_Gammas",
  "Ambient_U238",
  "Ambient_Th232",
  "Ambient_K40",
  "Ambient_Gammas_Deconv"
};

// constructor and destructor (mildly interesting because generators created)
GLG4PrimaryGeneratorAction::GLG4PrimaryGeneratorAction()
{
  // initialize messenger and time fields
  myMessenger = new GLG4PrimaryGeneratorMessenger(this);

  // set generator arrays -- classes and initial state are hard-coded
  if (theVertexGenerators[0] == nullptr) {
    // set the position generator array
    thePositionGenerators[iNullPos=0] = new GLG4PosGen_null(); // NONE
    thePositionGenerators[1] = new GLG4PosGen_PointPaintFill(); // user defined PaintFill
    //thePositionGenerators[iDelayPos=2] = new GLG4PosGen_null(); // delayed track stack
    thePositionGenerators[2] = new GLG4PosGen_Plane();
    thePositionGenerators[3] = new GLG4PosGen_Point();
    thePositionGenerators[4] = new GLG4PosGen_Sphere();
    thePositionGenerators[5] = new GLG4PosGen_Cylinder();
    thePositionGenerators[6] = new GLG4PosGen_Cuboid();

    // set the vertex generator array
    theVertexGenerators[0] = new GLG4VertexGen_Gun();
    theVertexGenerators[1] = new GLG4VertexGen_GPS();
    theVertexGenerators[2] = new GLG4VertexGen_HEPEvt();
    //theVertexGenerators[iDelayVtx=3] = new GLG4VertexGen_Stack(this);
    theVertexGenerators[3] = new GLG4VertexGen_SourceGamma();
    theVertexGenerators[4] = new GLG4VertexGen_SourceMonoBeta();
    theVertexGenerators[5] = new GLG4VertexGen_SourceMultipleBetas();
    theVertexGenerators[6] = new GLG4VertexGen_SourceAllDecays();
	theVertexGenerators[7] = new GLG4VertexGen_CosmogenicNeutrons();
	theVertexGenerators[8] = new GLG4VertexGen_AtmosphericNeutrons();
	theVertexGenerators[9] = new GLG4VertexGen_AtmosphericMuons();
    theVertexGenerators[10] = new GLG4VertexGen_AmbientGammas();
    theVertexGenerators[11] = new GLG4VertexGen_AmbientU238();
    theVertexGenerators[12] = new GLG4VertexGen_AmbientTh232();
    theVertexGenerators[13] = new GLG4VertexGen_AmbientK40();
    theVertexGenerators[14] = new GLG4VertexGen_AmbientGammasfromDeconv();
      //Don't think it's useful for now (Matthieu - August 2017)
//    if (ParamBase::GetDataBase()["enable_GLG4DeferTrackProc"]) {
//      theEventGeneratorCodes.push_back({iDelayPos,iDelayVtx});
//      iDelayEvt = 0;
//      myEventRate.push_back(0.0);
//      myTimeToNextEvent.push_back(-1);
//      myEventTriggerCondition.push_back(kGeneratorTriggerDelay);
//    }
  }
}

G4int GLG4PrimaryGeneratorAction::GetTypeOfCurrentEvent()
{
  if (mConstantCode.count(myTypeOfCurrentEvent) == 0) {
      // stack the 2 codes in 1 int, which can then be used in a switch
      G4int tmp = 100 * theEventGeneratorCodes[myTypeOfCurrentEvent].poscode
      + theEventGeneratorCodes[myTypeOfCurrentEvent].vertexcode;
      switch(tmp) {
          case   1: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::GPS;                   break;// { 0, 1 }, // 9 -> GPS
          case   2: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::HEPevent;              break;// { 0, 2 }, // 38 -> HEPevt
          case 101: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::fill_GPS;           break;// { 1, 1 }, // GPS in Target
          case 200: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::simple_gun;            break;// { 2, 0 }, // 3 -> gun, simple source defined in mac file
          case 102: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::fill_HEPevt;              break;// { 1, 2 }, // HEPevt in Target (e.g. NuMC antineutrino)
          case 303: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::defered_track_process; break;// { 3, 3 }, // 51 -> defer track process
          case 201: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::PaintFill_GPS;         break;// { 2, 1 }, // GPS with PaintFill
          case 202: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::PaintFill_HEPevt;      break;// { 2, 2 }  // HEPevt with PaintFill
          case 208: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::Plane_AtmNeutron;      break;// { 2, 8 }  // Atm neutron spectrum with plane pos gen
          case 408: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::Sphere_AtmNeutron;      break;// { 4, 8 }  // Atm neutron spectrum with sphere pos gen
          case 410: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::Sphere_AmbientGamma;     break;// { 4, 10 }  // ambient gamma spectrum with sphere pos gen
          case 411: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::Sphere_AmbientGamma;     break;// { 4, 10 }  // ambient gamma spectrum with sphere pos gen
          case 412: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::Sphere_AmbientGamma;     break;// { 4, 10 }  // ambient gamma spectrum with sphere pos gen
          case 413: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::Sphere_AmbientGamma;     break;// { 4, 10 }  // ambient gamma spectrum with sphere pos gen
          case 414: mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::Sphere_AmbientGamma;     break;// { 4, 10 }  // ambient gamma spectrum with sphere pos gen
          default:  mConstantCode[myTypeOfCurrentEvent] = (G4int)NewGene::unknown;
      }
  }
  return mConstantCode.at(myTypeOfCurrentEvent);
}

GLG4PrimaryGeneratorAction::~GLG4PrimaryGeneratorAction()
{
  for (G4int i=0; i<NbPosGenCodes; i++)    { delete thePositionGenerators[i]; }
  for (G4int i=0; i<NbVertexGenCodes; i++) { delete theVertexGenerators[i]; }
}

G4int GLG4PrimaryGeneratorAction::NewEventGenerator(G4int iPos, G4int iVtx, G4double rate, G4int trigger)
{
  for (size_t i=0; i<theEventGeneratorCodes.size(); i++) {
    if ( theEventGeneratorCodes.at(i).poscode == iPos and theEventGeneratorCodes.at(i).vertexcode == iVtx) {
      G4Exception("GLG4PrimaryGeneratorAction::NewEventGenerator","0",FatalErrorInArgument,
	scat("Generator with codes: (",iPos,",",iVtx,") already exists: ",GetEventTypeName(i),"\n").c_str()
      );
    }
  }

  theEventGeneratorCodes.push_back({iPos,iVtx});
  myEventRate.push_back(rate);
  myTimeToNextEvent.push_back(-1.);
  myEventTriggerCondition.push_back(trigger);

  G4int iNew = theEventGeneratorCodes.size()-1;
  cout << "=== +++ Successfully registrer new generator n° "<<iNew<<": "<<GetEventTypeName(iNew)
    <<"  at rate="<<myEventRate.back()*1e9<<" Hz +++ ===\n";

  return iNew;
}

// A somewhat interesting function: generates random exponential
// with mean 1 in "Bq" mode, simply returns 1 in "Hz" mode
inline G4double GLG4PrimaryGeneratorAction::HzOrBqFactor() const
{
  return myHzMode ? 1.0 : -log(1.0-G4UniformRand());
}

// GeneratePrimaries (this is the interesting part!)
void GLG4PrimaryGeneratorAction::GeneratePrimaries(G4Event* argEvent)
{
  G4int next_event_type= -1;
  G4double min_time_to_next_event = numeric_limits<G4double>::max();

  // find the next event, resetting any "expired" events along the way
  for (size_t i=0; i<theEventGeneratorCodes.size(); i++) {
    if (myTimeToNextEvent[i] < 0.0 && myEventRate[i] > 0.0
	&& myEventTriggerCondition[i] == kGeneratorTriggerNormal) {
      myTimeToNextEvent[i]= HzOrBqFactor()/myEventRate[i];
    }
    if (myTimeToNextEvent[i] >= 0.0
	&& myTimeToNextEvent[i] < min_time_to_next_event) {
      next_event_type= i;
      min_time_to_next_event= myTimeToNextEvent[i];
    }
  }

  // event type -1 means a commonly-made user error
  if (next_event_type == -1) {
    G4Exception("GLG4PrimaryGeneratorAction::GeneratePrimaries","1",FatalErrorInArgument, "No non-zero event rates!");
  }

//   cout << "=====================================================\nevent ID = ";
//   argEvent->Print();
//   cout << "myUniversalTime = "<<myUniversalTime<<endl
//     << "min_time_to_next_event = "<<min_time_to_next_event<<endl
//     << "myHzMode = "<<myHzMode<<endl
//     << "next_event_type = "<<next_event_type<<endl
//     << "myEventRate[next_event_type] = "<<myEventRate[next_event_type]<<endl
//     << endl;

  // update universal time and decrement all time-to-next-events
  myUniversalTimeSincePriorEvent= min_time_to_next_event;
  //G4double NotChangedTime=myUniversalTime;
  myUniversalTime+= min_time_to_next_event;
  for (size_t i=0; i<theEventGeneratorCodes.size(); i++) { myTimeToNextEvent[i] -= min_time_to_next_event; }

  // new time-to-next
  if (myEventRate[next_event_type] == 0.) { myTimeToNextEvent[next_event_type] = numeric_limits<G4double>::max(); }
  else { myTimeToNextEvent[next_event_type] = HzOrBqFactor()/myEventRate[next_event_type]; }
  myTypeOfCurrentEvent = next_event_type;

  // generate the event!
  G4int next_vtx = theEventGeneratorCodes[next_event_type].vertexcode;
  G4int next_pos = theEventGeneratorCodes[next_event_type].poscode;

  // "vertex"
  theVertexGenerators[next_vtx]->GeneratePrimaryVertex( argEvent );
  G4PrimaryVertex* vertex= argEvent->GetPrimaryVertex(0);
  
  // and "position"
  if (vertex != nullptr) {
//    if (next_pos == iDelayPos or next_pos == iNullPos) { // delayed particle have same position has parent, so the position is set to an offset instead of shooting a new one
//      thePositionGenerators[next_pos]->GenerateVertexPositions( vertex, myChainClip, myEventRate[next_event_type], true );
//    } else { // for all others particle generators, the particles are independent
      thePositionGenerators[next_pos]->GenerateVertexPositions( vertex, myChainClip, myEventRate[next_event_type], false );
    //}
  } else {
    cerr << "Warning, no vertex generated by vertex generator: event_type="
      << next_event_type << " vtx_code=" << next_vtx << endl;
    vertex = new G4PrimaryVertex( 0.,0.,0.,0. );
    argEvent->AddPrimaryVertex( vertex );
  }
  // Now that the vertex and its position are generated, we may want to modify the direction
  // of the particle in order to aim at a particular volume. This is done here.
	if(theVertexGenerators[next_vtx]->GetAniType() == "target"){
		theVertexGenerators[next_vtx]->SetDirectionTarget(vertex);
		// The target volume is an attribute of GLG4VertexGen_CosmogenicNeutrons
		//and had been set when reading the macro in GLG4VertexGen_CosmogenicNeutrons::SetState()
		//The macro should have keywords before impulsion  "target [volumeName] " in the line /generator/vtx/set
	}

    if(theVertexGenerators[next_vtx]->GetAniType() == "center"){
        theVertexGenerators[next_vtx]->ForceDirectionTo(vertex, thePositionGenerators[next_pos]->GetDirection());
        //The macro should have keyword "center" in the line /generator/vtx/set
    }
   
  // add pileup events from normal-triggering primary events
  for (size_t i=0; i<theEventGeneratorCodes.size(); i++) {
    while (myTimeToNextEvent[i] >= 0.0 && myTimeToNextEvent[i]<myEventWindow && myEventRate[i] > 0.0) {
      G4int vtx_code= theEventGeneratorCodes[i].vertexcode;
      G4int pos_code= theEventGeneratorCodes[i].poscode;
      G4int nstart= argEvent->GetNumberOfPrimaryVertex();
      theVertexGenerators[vtx_code]->GeneratePrimaryVertex( argEvent );
      G4PrimaryVertex *vn= argEvent->GetPrimaryVertex(nstart);
      if (vn != nullptr) {
	thePositionGenerators[pos_code]->GenerateVertexPositions( vn, myChainClip, myEventRate[i], false, myTimeToNextEvent[i] );
      }
      myTimeToNextEvent[i] += HzOrBqFactor()/myEventRate[i];
    }
  }

  // add pileup events from pileup-triggered events (which may have rates > 1/myEventWindow)
  for (size_t i=0; i<theEventGeneratorCodes.size(); i++) {
    if (myEventTriggerCondition[i] == kGeneratorTriggerPileupOnly && myEventRate[i] > 0.0) {
      G4int vtx_code= theEventGeneratorCodes[i].vertexcode;
      G4int pos_code= theEventGeneratorCodes[i].poscode;
      G4double t= 0.0;
      while ( (t += HzOrBqFactor()/myEventRate[i]) < myEventWindow ) {
	theVertexGenerators[vtx_code]->GeneratePrimaryVertex( argEvent );
	G4PrimaryVertex *vn= argEvent->GetPrimaryVertex(argEvent->GetNumberOfPrimaryVertex());
	if (vn != nullptr) { thePositionGenerators[pos_code]->GenerateVertexPositions( vn, myChainClip, myEventRate[i], false, t ); }
      }
    }
  }
  // done!
}

void GLG4PrimaryGeneratorAction::DeferTrackToLaterEvent(const G4Track * track)
{
  NotifyTimeToNextStackedEvent( track->GetGlobalTime() );
  static_cast<GLG4VertexGen_Stack*>(theVertexGenerators[iDelayVtx])-> StackIt( track );
  // track->SetTrackStatus( fStopAndKill ); // must be done by caller
}

void GLG4PrimaryGeneratorAction::NotifyTimeToNextStackedEvent( G4double t )
{
  if ( myTimeToNextEvent[iDelayEvt] < 0.0
    or t < myTimeToNextEvent[iDelayEvt]
  ) {
    myTimeToNextEvent[iDelayEvt]= t;
  }
}
