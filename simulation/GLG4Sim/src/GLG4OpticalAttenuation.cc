// This file is part of the GenericLAND software library.
// $Id: GLG4OpticalAttenuation.cc,v 1.3 2007/08/28 20:35:17 gahs Exp $
//
//  "Attenuation" (absorption or scattering) of optical photons
//
//   GenericLAND Simulation
//
//   Original: Glenn Horton-Smith, Dec 2001
//
// GLG4OpticalAttenuation.cc
// 

#include <GLG4OpticalAttenuation.hh>

#include <globals.hh>
#include <templates.hh>
#include <G4DynamicParticle.hh>
#include <G4Material.hh>
#include <G4OpticalPhoton.hh>
#include <Randomize.hh>
#include <G4Step.hh>

#include <cfloat> // for DBL_EPSILON

using namespace std;

        /////////////////
        // Hidden static variables and functions
        /////////////////

// values in Cos2ThetaTable are used in equation to invert the equation
// for generating an angular distribution for scalar scattering:
//  dP/d(cos\theta) = (3/4) sin^2\theta
//   P_{cumulative} = (2+3*cos\theta-cos^3\theta)/4
//      cos(\theta) = 4*(P_{cumulative}-1/2) / (3-cos^2\theta)
//                                                ^^^^^^^^^^^ table value used
#define N_COSTHETA_ENTRIES 129
static double Cos2ThetaTable[N_COSTHETA_ENTRIES]; 
static int TableInitialized= 0;

static void InitializeTable(void)
{
  double cos2th=0.0;

  for (int i=0; i<N_COSTHETA_ENTRIES-1; i++) {
    double x= i/(double)(N_COSTHETA_ENTRIES-1);
    double old_cos2th;
    do { // find exact root by iterating to convergence
      old_cos2th= cos2th;
      double costh= 2.0*x/(3.0-cos2th);
      cos2th= costh*costh;
    } while (fabs(old_cos2th-cos2th) > 2*DBL_EPSILON);
    Cos2ThetaTable[i]= cos2th;
  }
  Cos2ThetaTable[N_COSTHETA_ENTRIES-1]= 1.0;
  TableInitialized=1;
}

        /////////////////
        // Constructors and Destructor
        /////////////////

GLG4OpticalAttenuation::GLG4OpticalAttenuation(const G4String& processName)
              : G4OpAbsorption(processName)
{
  if (!TableInitialized)
    InitializeTable();
}

GLG4OpticalAttenuation::~GLG4OpticalAttenuation(){}

        ////////////
        // Methods
        ////////////

// PostStepDoIt
// -------------
//
G4VParticleChange*
GLG4OpticalAttenuation::PostStepDoIt(const G4Track& aTrack, const G4Step& aStep)
{
  aParticleChange.Initialize(aTrack);

  const G4DynamicParticle* aParticle = aTrack.GetDynamicParticle();
  const G4Material* aMaterial = aTrack.GetMaterial();

  G4double thePhotonMomentum = aParticle->GetTotalMomentum();

  G4MaterialPropertiesTable* aMaterialPropertyTable;
  G4MaterialPropertyVector* OpScatFracVector;
  
  G4double OpScatFrac = 0.0;

  aMaterialPropertyTable = aMaterial->GetMaterialPropertiesTable();

  if ( aMaterialPropertyTable ) {
      OpScatFracVector = aMaterialPropertyTable->GetProperty("OPSCATFRAC");
      if ( OpScatFracVector ) {
	OpScatFrac = OpScatFracVector-> Value(thePhotonMomentum);
      }
  }

  if ( OpScatFrac > 0.0 && G4UniformRand() < OpScatFrac ) {
    // photon scattered coherently -- use scalar scattering (Rayleigh):
    // for fully polarized light, angular distribution \prop sin^2 \theta
    // where theta is angle between initial polarization and final
    // momentum vectors.  All light in Geant4 is fully polarized...
    G4double urand= G4UniformRand()-0.5;
    G4double Cos2Theta0=
      Cos2ThetaTable[(int)(fabs(urand)*2.0*(N_COSTHETA_ENTRIES-1)+0.5)];
    G4double CosTheta= 4.0*urand/(3.0-Cos2Theta0);
#ifdef G4DEBUG	  
    if (fabs(CosTheta)>1.0) {
      cerr << "GLG4OpAttenution: Warning, CosTheta=" << CosTheta
	    << " urand=" << urand << endl;
      CosTheta= CosTheta>0.0 ? 1.0 : -1.0;
    }
#endif
    G4double SinTheta= sqrt(1.0-CosTheta*CosTheta);
    G4double Phi= (2.0*G4UniformRand()-1.0)*M_PI;
    G4ThreeVector e2( aParticle->GetMomentumDirection()
		      .cross( aParticle->GetPolarization() ) );
    
    G4ThreeVector NewMomentum=
      ( CosTheta * aParticle->GetPolarization() +
	(SinTheta*cos(Phi)) * aParticle->GetMomentumDirection() +
	(SinTheta*sin(Phi)) * e2 ).unit();

    // polarization is normal to new momentum and in same plane as
    // old new momentum and old polarization
    G4ThreeVector NewPolarization=
      ( aParticle->GetPolarization() - CosTheta*NewMomentum).unit();
    
    aParticleChange.ProposeMomentumDirection(NewMomentum);
    aParticleChange.ProposePolarization(NewPolarization);
  }
  else {
    // photon absorbed (may be re-radiated... but that is GLG4Scint's job)
    aParticleChange.ProposeTrackStatus(fStopAndKill);
    
    if (verboseLevel>3) {
      std::cout << "\n** Photon absorbed! **" << std::endl;
    }
  }
  
  return G4VDiscreteProcess::PostStepDoIt(aTrack, aStep);
}


