/** @file GLG4PMTOpticalModel.cc
    Defines a FastSimulationModel class for handling optical photon
    interactions with PMT: partial reflection, transmission, absorption,
    and hit generation.

    This file is part of the GenericLAND software library.
    $Id: GLG4PMTOpticalModel.cc,v 1.19 2007/10/07 20:37:10 da2el Exp $

    @author Glenn Horton-Smith, March 20, 2001.
    @author Dario Motta, Feb. 23 2005: Formalism light interaction with photocathode.
    @author Alexander Etenko, Nov. 01 2005: overloaded constructor added
*/

#include <Parameters.hh>
#include <String.hh>

#include <GLG4PMTOpticalModel.hh>
#include <GLG4PMTSD.hh>

#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <G4LogicalBorderSurface.hh>
#include <G4OpticalSurface.hh>
#include <G4MaterialPropertiesTable.hh>
#include <G4MaterialPropertyVector.hh>
#include <G4OpticalPhoton.hh>
#include <G4TransportationManager.hh>
#include <G4UIcommand.hh>
#include <G4UIdirectory.hh>
#include <Randomize.hh>
#include <G4ProductionCuts.hh>
#include <G4MaterialCutsCouple.hh>
#include <G4VPhysicalVolume.hh>

#include <complex>
#include <stdexcept>
using namespace std;

G4UIdirectory* GLG4PMTOpticalModel::fgCmdDir = nullptr;
G4int GLG4PMTOpticalModel::TotalIncidentPhoton = 0;

// local helper function
static G4Envelope* GetOrMakeEnvelope(G4LogicalVolume*lv)
{
  if (lv->IsRootRegion()) { return lv->GetRegion(); }
  G4Region* region = new G4Region(lv->GetName()+"_OpticalModel_region");
  region->AddRootLogicalVolume(lv);
  /* no special cuts for PMT, just a place holder to feed Geant4 */
  /**
   * The following line was sometimes causing a seg fault at initialization in G4VRangeToEnergyConverter::Convert,
   * because the default value of the cut is set to 0 with the empty ctor of G4ProductionCuts,
   * and this value is used in a division causing a signal SIGFPE, Arithmetic exception.
   */
//   region->SetProductionCuts(new G4ProductionCuts());
  /**
   * The following line causes a seg fault because at this point the logical volume has not MaterialCutsCouple.
   */
//  region->SetProductionCuts(lv->GetMaterialCutsCouple()->GetProductionCuts());
  return region;
}

// constructor -- also handles all initialization
GLG4PMTOpticalModel::GLG4PMTOpticalModel (G4String modelName, G4VPhysicalVolume* envelope_phys)
  : G4VFastSimulationModel(modelName, GetOrMakeEnvelope(envelope_phys->GetLogicalVolume()))
{
  G4LogicalVolume* envelope_log= envelope_phys->GetLogicalVolume();
  // here we assume that the first daughter volume is the "inner1" volume
  inner1_phys= envelope_log->GetDaughter(0);
  G4LogicalBorderSurface* pc_log_surface= G4LogicalBorderSurface::GetSurface(envelope_phys, inner1_phys);
  if (pc_log_surface == nullptr) {
    G4Exception("GLG4PMTOpticalModel::GLG4PMTOpticalModel","1", FatalErrorInArgument,"No photocathode logical surface!?!");
  }

  // We have to trust that the G4SurfaceProperty returned by
  // pc_log_surface->GetSurfaceProperty() will be a G4OpticalSurface,
  // and cast the pointer accordingly by faith alone!
  // But faith will be checked by a dynamic_cast, JG, 29-05-2015
  G4OpticalSurface* pc_opsurf= dynamic_cast<G4OpticalSurface*>( pc_log_surface->GetSurfaceProperty() );

  Init( envelope_log, pc_opsurf );
}

GLG4PMTOpticalModel::GLG4PMTOpticalModel(G4String modelName, G4LogicalVolume* envelope_log,  G4OpticalSurface* pc_opsurf)
  : G4VFastSimulationModel(modelName, GetOrMakeEnvelope(envelope_log))
{
  Init( envelope_log, pc_opsurf );
}

void GLG4PMTOpticalModel::Init(G4LogicalVolume* envelope_log, G4OpticalSurface* pc_opsurf)
{
  // -- database
  const ParamBase &db ( ParamBase::GetDataBase() );

  try {
    VerboseLevel = static_cast<G4int>(db["GLG4PMT_verboselevel"]);
    luxlevel = static_cast<G4int>(db["luxlevel"]);
    omit_effvsrho = static_cast<G4bool>(db["omit_effvsrho"]);
    global_QE_scaling = static_cast<G4double>(db["global_QE_scaling"]);
  } catch (runtime_error& err) {
    G4Exception("GLG4PMTOpticalModel::Init","1",FatalException,
      scat("Missing parameter in database: ",err.what()).c_str() );
  }
  // get material properties vectors
  // ... material properties of glass

  G4MaterialPropertiesTable* glass_pt= envelope_log->GetMaterial()->GetMaterialPropertiesTable();
  if (glass_pt == nullptr) {
    G4Exception("GLG4PMTOpticalModel::GLG4PMTOpticalModel","1",FatalErrorInArgument,"Glass lacks a properties table!");
  }

  rindex_glass= glass_pt->GetProperty("RINDEX");
  if (rindex_glass == nullptr) {
    G4Exception("GLG4PMTOpticalModel::GLG4PMTOpticalModel","2",FatalErrorInArgument,"Glass does not have RINDEX!");
  }

  // ... material properties of photocathode (first get photocathode surface)
  // here we assume that the first daughter volume is the "inner1" volume
  inner1_phys= envelope_log->GetDaughter(0);

  if (pc_opsurf == nullptr) {
    G4Exception("GLG4PMTOpticalModel::GLG4PMTOpticalModel","3",FatalErrorInArgument,"No photocathode optical surface!?!");
  }
  G4MaterialPropertiesTable* pc_pt= pc_opsurf->GetMaterialPropertiesTable();
  if (pc_pt == nullptr) {
    G4Exception("GLG4PMTOpticalModel::GLG4PMTOpticalModel","4",FatalErrorInArgument,"Photocathode lacks a properties table!");
  }
  inner1_solid= inner1_phys->GetLogicalVolume()->GetSolid();

  rindex_photocathode= pc_pt->GetProperty("RINDEX");
  if (rindex_photocathode == nullptr) {
    G4Exception("GLG4PMTOpticalModel::GLG4PMTOpticalModel","5",FatalErrorInArgument,"Photocathode does not have RINDEX!");
  }
  kindex_photocathode= pc_pt->GetProperty("KINDEX");
  if (kindex_photocathode == nullptr) {
    G4Exception("GLG4PMTOpticalModel::GLG4PMTOpticalModel","6",FatalErrorInArgument,"Photocathode does not have KINDEX!");
  }
  thickness_photocathode= pc_pt->GetProperty("THICKNESS");
  if (thickness_photocathode == nullptr) {
    G4Exception("GLG4PMTOpticalModel::GLG4PMTOpticalModel","7",FatalErrorInArgument,"Photocathode does not have THICKNESS!");
  }
  efficiency_photocathode= pc_pt->GetProperty("EFFICIENCY");
  if (efficiency_photocathode == nullptr) {
    G4Exception("GLG4PMTOpticalModel::GLG4PMTOpticalModel","8",FatalErrorInArgument,"Photocathode does not have EFFICIENCY!");
  }
  effscale_photocathode= pc_pt->GetProperty("EFFSCALE");  // it's okay to be null
  Eff_vs_Rho_photocathode= pc_pt->GetProperty("EFFVSRHO");  // it's okay to be null

  // initialize abs_norm
  if (luxlevel == 3) { Initialize_abs_norm(); }
  if (VerboseLevel > 3) { abs_norm->DumpValues(); }

  // initialize photon_energy to a nonsense value to indicate that the other
  // values are not initialized
  photon_energy= -1.0;

  // add UI commands
  if ( fgCmdDir == nullptr ) {
    fgCmdDir = new G4UIdirectory("/PMTOpticalModel/");
    fgCmdDir->SetGuidance("PMT optical model control.");
    G4UIcommand *cmd;

    cmd= new G4UIcommand("/PMTOpticalModel/verbose",this);
    cmd->SetGuidance("Set verbose level\n"
		     " 0 == quiet\n"
		     " 1 == minimal entrance/exit info\n"
		     " 2 == +print verbose tracking info\n"
		     " >= 10  +lots of info on thin photocathode calcs\n");
    cmd->SetParameter(new G4UIparameter("level", 'i', false));

    cmd= new G4UIcommand("/PMTOpticalModel/luxlevel",this);
    cmd->SetGuidance("Set \"luxury level\" for PMT Optical Model\n"
		     " 0 == standard \"black bucket\": photons stop in PC, maybe make pe, \n"
		     " 1 == shiny translucent brown film: photons only stop if they make a PE, otherwise 50/50 chance of reflecting/transmitting\n"
		     " 2 or greater == full model\n"
		     "The default value is 3.\n"
		     );
    cmd->SetParameter(new G4UIparameter("level", 'i', false));

    cmd= new G4UIcommand("/PMTOpticalModel/omit_effvsrho",this);
    cmd->SetGuidance("Omit collection efficiency as a funciont of rho on the photocathode\n"
		     " 0 == The CE varies over the PMT photocathode as described by the EFFVSRHO material property\n"
		     " 1 == The CE is uniform over the PMT photocathode (effvsrho == 1)\n"
		     "The default value is 0.\n"
		     );
    cmd->SetParameter(new G4UIparameter("omit_effvsrho", 'i', false));

    cmd= new G4UIcommand("/PMTOpticalModel/global_QE_scaling",this);
    cmd->SetGuidance("Set a global (wavelength-independent) scaling factor for the QE\n"
		     "For a wavelength-dependent rescaling, check the EFFSCALE material property vector\n"
		     "The default value is 1.\n"
		     );
    cmd->SetParameter(new G4UIparameter("global_QE_scaling", 'd', false));

  }

  // geometrical tolerance to use
  fMyZTolerance= inner1_solid->GetTolerance();
  // note: the above code for finding fMyZTolerance reproduces what was
  // always done in GLG4PMTOpticalModel, but some day it might be nice to
  // try the following to see if it improves stability at the internal
  // vacuum volume's equatorial interface:
  //  {
  //    // calculate generous Z tolerance as a fraction of object size
  //    G4VoxelLimits voxelLimits;  // Defaults to "infinite" limits.
  //    G4AffineTransform affineTransform; // Defaults to no transform
  //    G4double zmin, zmax;
  //    inner1_solid->CalculateExtent(kZAxis, voxelLimits, affineTransform,
  //                                   zmin, zmax);
  //    fMyZTolerance= 1e-6 * (zmax-zmin);
  //  }
}

// IsApplicable() method overriding virtual function of G4VFastSimulationModel
// returns true if model is applicable to given particle.
// -- see also Geant4 docs
G4bool GLG4PMTOpticalModel::IsApplicable(const G4ParticleDefinition &particleType)
{
  return ( &particleType == G4OpticalPhoton::OpticalPhotonDefinition() );
//  return ( particleType == *G4OpticalPhoton::OpticalPhotonDefinition() );
}


// ModelTrigger() method overriding virtual function of G4VFastSimulationModel
// returns true if model should take over this specific track.
// -- see also Geant4 docs
G4bool GLG4PMTOpticalModel::ModelTrigger(const G4FastTrack &fastTrack)
{
  // we trigger if the track position is above the equator
  // or if it is on the equator and heading up
  if ( fastTrack.GetPrimaryTrackLocalPosition().z() >  fMyZTolerance ) { return true; }
  if ( fastTrack.GetPrimaryTrackLocalPosition().z() > -fMyZTolerance
    && fastTrack.GetPrimaryTrackLocalDirection().z() > 0.0 ) { return true; }
  return false;
}


// DoIt() method overriding virtual function of G4VFastSimulationModel
// does the fast simulation for this track.  It is basically a faster but
// complete tracking code for the two-volume case.  It is a monster.
// -- see also Geant4 docs and comments below
void GLG4PMTOpticalModel::DoIt(const G4FastTrack& fastTrack, G4FastStep& fastStep)
{
  // Logic summary:
  //  1) If track is outside the "inner1" vacuum, then track
  //     is advanced to either the inner1/body interface or to the body/outside
  //     interface, whichever comes first.
  //   a) If track hits the body/outside interface, then we just update
  //      its position and time and return;
  //   b) else, we do the thin-layer reflection/transmission/absorption thing
  //      with n1=n_glass and n3=1.0:
  //    i) Make a binary random decision on whether to absorb a photon.
  //       Probability is equal to "weight" of track times absorption coeff.
  //       (If P > 1.0, weight hit by floor(P+G4UniformRand()).)
  //       If photon is absorbed, then we are done tracking.
  //    ii) A binary random decision is made on whether to reflect or
  //       refract (transmit) the remaining track, if any.
  //  2) If track is in the "inner1" vacuum (either on entry to DoIt or
  //     after transmission in step 1.b.ii), then we advance to the
  //     inner1/body interface (surface of inner1).  Then we do the
  //     thin-layer reflection/transmission/absoprtion thing exactly as
  //     in step 1.b, but with n1=1.0 and n3=n_glass.
  //  3) Steps 1 and 2 are repeated until track hits the body outer surface,
  //     descends below the equator, or is completely absorbed on the
  //     photocathode.  Note it is best to check if step 1 applies even
  //     after doing step 1, to avoid making assumptions about the geometry.
  //     (It could happen someday that we have a photocathode with a negative
  //     curvature someplace.)
  //
  // Tracking elsewhere in the PMT (in the vacuum or glass below the equator,
  // for example) is handled by the usual Geant4 tracking.  If we ever
  // need to handle more than two volumes or more than one surface, then
  // we should use a general Geant4 "navigator" as in ExN05EMShowerModel,
  // but for this simple case, we can be more efficient with this custom
  // coding.  -GHS.

  G4int totalNpe=0;
  G4double dist, dist1;
  G4ThreeVector pos;
  G4ThreeVector dir;
  G4ThreeVector pol;
  G4ThreeVector norm;
  G4double time;
  G4int weight;
  G4double energy;
  G4double n_glass;
  G4VSolid *envelope_solid = fastTrack.GetEnvelopeSolid();
  G4VSensitiveDetector* detector = fastTrack.GetEnvelopeLogicalVolume()->GetSensitiveDetector();

  enum EWhereAmI { kInGlass, kInVacuum } whereAmI;
  int ipmt= -1;

  TotalIncidentPhoton++;

  // find which pmt we are in
  // The following doesn't work anymore (due to new geometry optimization?)
  //  ipmt=fastTrack.GetEnvelopePhysicalVolume()->GetMother()->GetCopyNo();
  // so we do this:
  {
    const G4VTouchable* touch= fastTrack.GetPrimaryTrack()->GetTouchable();

    for (G4int id=0; id<touch->GetHistoryDepth(); id++) {
      if (touch->GetVolume(id)==fastTrack.GetEnvelopePhysicalVolume()) {
      	if ( touch->GetVolume(id)->GetMotherLogical()->GetNoDaughters()>1) {
	  ipmt= touch->GetReplicaNumber(id);
    	}  else {
	  ipmt= touch->GetReplicaNumber(id+1);
	}
	break;
      }
    }
    if (ipmt < 0) {
      G4Exception("GLG4PMTOpticalModel::DoIt","1",FatalException,"Could not find envelope -- where am I !?!");
    }
  }

  // get position and direction in local coordinates
  pos=  fastTrack.GetPrimaryTrackLocalPosition();
  dir=  fastTrack.GetPrimaryTrackLocalDirection();
  pol=  fastTrack.GetPrimaryTrackLocalPolarization();

  // get weight and time
  time= fastTrack.GetPrimaryTrack()->GetGlobalTime();  // "global" is correct
  weight= (G4int)( fastTrack.GetPrimaryTrack()->GetWeight() );

  // get n_glass, n2, k2, etc., for this wavelength
  energy= fastTrack.GetPrimaryTrack()->GetKineticEnergy();
  if ( energy == photon_energy ) { // equal to last energy?
    // use old values
    if ( n1 == 1.0 ) { n_glass= n3; }
    else { n_glass= n1; }
  } else {
    photon_energy= energy;
    wavelength= twopi*hbarc / energy;
    n_glass= rindex_glass->Value( energy );
    n1= n_glass; // just in case we exit before setting n1
    n2= rindex_photocathode->Value( energy );
    k2= kindex_photocathode->Value( energy );
    n3= 1.0;     // just in case we exit before setting n3
    efficiency= (efficiency_photocathode->Value( energy ))*global_QE_scaling;
    if(effscale_photocathode != nullptr) {
      efficiency *= effscale_photocathode->Value( energy );
    }
  }

  // initialize "whereAmI"
  if ( fastTrack.GetPrimaryTrack()->GetVolume() == inner1_phys ) { whereAmI= kInVacuum; }
  else { whereAmI= kInGlass; }

  // print verbose info
  if (VerboseLevel > 1) {
    cout.flush();
    cout << "\n> Enter GLG4PMTOpticalModel, ipmt=" << ipmt << ", in "
	   << ( whereAmI==kInVacuum ? " vacuum" : " glass" )
	   << ", part=" << fastTrack.GetPrimaryTrack()->GetDynamicParticle()->GetDefinition()->GetParticleName()
	   << ", pos=" << pos << ", dir=" << dir << ", weight=" << weight
	   << ", pol=" << pol << ", energy=" << photon_energy
	   << ", wavelength (nm)=" << wavelength/nm
	   << ", (n1,n3,n2,k2,efficiency)=(" << n1 << "," << n3 << ","
	   << n2 << "," << k2 << "," << efficiency << ")\n";
  }

  G4int iloop;
  const G4int max_iloop= 10000;
  for (iloop=0; iloop<max_iloop; iloop++) {

    if ( whereAmI == kInGlass ) {
      // advance to next interface
      dist1= envelope_solid->DistanceToOut( pos, dir );
      dist= inner1_solid->DistanceToIn( pos, dir );
      if (dist1 < dist) {
	// we hit the envelope outer surface, not the inner surface
	dist= dist1;
	if ( dir.z() < 0.0 ) { // headed towards equator?
	  // make sure we don't cross the equator
	  dist1 = - pos.z() / dir.z(); // distance to equator
	  if (dist1 < dist) { dist= dist1; }
	}
	pos += dist*dir;
	time += dist*n_glass/c_light;

	if (VerboseLevel > 2) { // we're passing through the equator
	  cout << "  Passing through the equator\n";
	}
	break;
      }
      pos += dist*dir;
      time += dist*n_glass/c_light;
      n1= n_glass;
      n3= 1.0;
    } else  { // in the "inner1" vacuum
      // advance to next interface
      dist= inner1_solid->DistanceToOut( pos, dir );
      if ( dist < 0.0 ) {
	cerr << "GLG4PMTOpticalModel::DoIt(): Warning, "
	  << "strangeness detected! inner1->DistanceToOut()=" << dist << endl;
	dist= 0.0;
      }
      pos += dist*dir;
      time += dist/c_light;
      if ( pos.z() < fMyZTolerance ) {
	if (VerboseLevel > 2) { // we're passing through the equator
	  cout << "  Passing through the equator\n";
	}
	break;
      }
      n1= 1.0;
      n3= n_glass;
    }

    if (VerboseLevel > 2) {
      cout << " " << iloop << " dist=" << dist << " newpos=" << pos << endl;
    }

    // get outward-pointing normal in local coordinates at this position
    norm= inner1_solid->SurfaceNormal( pos );
    // reverse sign if incident from glass, so normal points into region "3"
    if ( whereAmI == kInGlass ) { norm *= -1.; } // in principle, this is more efficient than norm= -norm;

    // set thickness and cos_theta1
    thickness=  thickness_photocathode->Value( pos.z() );
    cos_theta1= dir * norm;
    if ( fabs(cos_theta1) < 1e-6 && whereAmI == kInVacuum ) {
      // this is a hopelessly shallow grazing angle!
      weight= 0;
      if (VerboseLevel > 2) {
	cout << "GLG4PMTOpticalModel absorbed shallow grazing track\n";
      }
      break;
    }
    if ( cos_theta1 < 0.0 ) {
      cerr << "GLG4PMTOpticalModel::DoIt(): "
	      << " The normal points the wrong way!\n"
	      << "  norm: " << norm << endl
	      << "  dir:  " << dir << endl
	      << "  cos_theta1:  " << cos_theta1 << endl
	      << "  pos:  " << pos << endl
	      << "  whereAmI:  " << (int)(whereAmI) << endl
	      << " Reversing normal!" << endl;
      cos_theta1= -cos_theta1;
      norm= -norm;
    }

    // Now calculate coefficients
    CalculateCoefficients();

    // Calculate Transmission, Reflection, and Absorption coefficients
    G4double Trans,Refl,Abs,An,collection_eff;
    G4double E_s2;
    if ( sin_theta1 > 0.0 ) {
      E_s2= ( pol * dir.cross( norm ) ) / sin_theta1;
      E_s2*= E_s2;
    } else { E_s2= 0.0; }

    Trans= fT_s * E_s2  +  fT_p * (1.0-E_s2);
    Refl= fR_s * E_s2  +  fR_p * (1.0-E_s2);
    Abs= 1.0 - (Trans+Refl);
    if(abs_norm != nullptr) { //abs_norm initialized, luxlevel==2
      An= abs_norm->Value( energy ); //The absorption at normal incidence
    } else { An=Abs; } // simpler models ...

    collection_eff= efficiency/An; // net QE = efficiency for normal inc.

    if(VerboseLevel > 3) {
	cout<<"R,T,A,An,Coll_Eff: "<<Refl<<" "<<Trans<<" "<<Abs<<" "<<An<<" "<<collection_eff<<endl;
    }
    //Check nothing is wrong with collection_eff ...
    if(collection_eff > 1.) {
      cerr<<"Error: collection_eff = "<<collection_eff<<endl;
      cerr<<"QE = "<<efficiency<<" -- "<<"Absorption at normal incidence = "<<An<<endl;
      G4String origin = "GLG4PMTOpticalModel::DoIt";
      G4String code = "collection_eff > 1 ( <= 1 expected )";
      G4String description = "This probably means that you are doing nasty things with QE ... \n";
      G4Exception(origin,code,RunMustBeAborted,description);
    }


    if (Eff_vs_Rho_photocathode != nullptr && !omit_effvsrho) {
      // multiply by EFFVSRHO correction -- relative efficiency vs xy
      collection_eff *= Eff_vs_Rho_photocathode->Value( pos.perp() );
    }

# ifdef G4DEBUG
    if (A < 0.0 || A > 1.0 || collection_eff < 0.0 || collection_eff > 1.0) {
      cerr << "GLG4PMTOpticalModel::DoIt(): Strange coefficients!\n";
      cout<<"T, R, A, An, weight: "<<T<<" "<<R<<" "<<A<<" "<<An<<" "<<weight<<endl;
      cout<<"collection eff, std QE: "<<collection_eff<<" "<<efficiency<<endl;
      cout<<"========================================================="<<endl;
      A= collection_eff= 0.5; // safe values???
    }
# endif

    // Now decide how many pe we make.
    // When weight == 1, probability of a pe is A*collection_eff.
    // There is a certain correlation between "a pe is made" and
    // "the track is absorbed", which is implemented correctly below for
    // the weight == 1 case, and as good as can be done for weight>1 case.
    G4double mean_N_pe = weight*Abs*collection_eff;
    G4double ranno_absorb = G4UniformRand();
    G4int N_pe = (G4int)( mean_N_pe + (1.0-ranno_absorb) );

    if (N_pe > 0) {
      totalNpe += N_pe;
      if ( detector != nullptr && detector->isActive() ) {
	static_cast<GLG4PMTSD*>(detector)->SimpleHit( ipmt, time, energy, pos, dir, pol, N_pe );
	if (VerboseLevel > 0) {
	  cout << " Simple Hit for "<<N_pe<<" pe!\n";
	}
      }
      if (VerboseLevel > 2) {
	cout << "ranno_absorb =  " << ranno_absorb <<endl;
	cout << "=> GLG4PMTOpticalModel made " << N_pe << " pe\n";
      }
    }

    // Now maybe absorb the track.
    // The probability is independent of weight, and the entire
    // track is either absorbed or not as a whole.
    // This is consistent with how the track is treated in other
    // processes (absorption, G4OpBoundary, etc.), and the statistics
    // for the final number of pe detected overall is made consistent
    // by the poissonian statistics of number of tracks implemented in
    // GLG4Scint.  (Actually, for weights > 1, the correlation between
    // number absorbed and number transmitted/reflected and subsequently
    // absorbed cannot be made as big as it should be, so the width of
    // the distribution of the total number of pe per event will be slightly
    // too large.  Only the weight=1 case is "guaranteed" to get everything
    // correct, assuming there are no bugs in the code.)
    if ( ranno_absorb < Abs) {
      weight= 0;
      if (VerboseLevel > 2) {
	cout << "GLG4PMTOpticalModel absorbed track\n";
      }
      break;
    }

    // reflect or refract the unabsorbed track
    if ( G4UniformRand() < Refl/(Refl+Trans) ) { // reflect
      Reflect( dir, pol, norm );
      if (VerboseLevel > 2)
	cout << "GLG4PMTOpticalModel reflects track\n";
    } else { // transmit
      Refract( dir, pol, norm );
      if ( whereAmI == kInGlass ) { whereAmI = kInVacuum; }
      else { whereAmI = kInGlass; }
      if (VerboseLevel > 2) {
	cout << "GLG4PMTOpticalModel transmits track, now in "
	    << ( whereAmI==kInVacuum ? " vacuum" : " glass" ) << endl;
      }
    }
  }

  fastStep.SetPrimaryTrackFinalPosition( pos );
  fastStep.SetPrimaryTrackFinalTime( time );
  fastStep.SetPrimaryTrackFinalMomentum( dir );
  fastStep.SetPrimaryTrackFinalPolarization( pol );
  // fastStep.SetPrimaryTrackPathLength( trackLength ); // does anyone care?
  if (weight <= 0) {
    fastStep.ProposeTrackStatus(fStopAndKill);
    if (weight < 0) {
      cerr << "GLG4PMTOpticalModel::DoIt(): Logic error, weight = " << weight << endl;
    }
  } else {
    // in case multiphoton has been partly absorbed and partly reflected
    fastStep.SetPrimaryTrackFinalEventBiasingWeight( weight );
  }
  if (iloop >= max_iloop) {
    cerr << "GLG4PMTOpticalModel::DoIt(): Too many loops, particle trapped! Killing it.\n";
    fastStep.ProposeTrackStatus(fStopAndKill);
  }

  if (VerboseLevel > 1) {
    cout.flush();
    cout << "> Exit GLG4PMTOpticalModel, ipmt=" << ipmt
	   << ( whereAmI==kInVacuum ? " vacuum" : " glass" )
	   << ", pos=" << pos << ", dir=" << dir << ", weight=" << weight
	   << ", pol=" << pol << ", iloop=" << iloop << ", totalNpe=" << totalNpe << "\n";
  }

  return;
}


// CalculateCoefficients() method used by DoIt() above.
// *** THE PHYSICS, AT LAST!!! :-) ***

void GLG4PMTOpticalModel::CalculateCoefficients()
  // calculate and set fR_s, etc.
{
  if (luxlevel <= 0) {
    // no reflection or transmission, just a black "light bucket"
    // 100% absorption, and QE will be renormalized later
    fR_s= fR_p= 0.0;
    fT_s= fT_p= 0.0;
    return;
  }
  else if (luxlevel == 1) {
    // this is what was calculated before, when we had no good defaults
    // for cathode thickness and complex rindex
    // set normal incidence coefficients: 50/50 refl/transm if not absorb.
    fR_s = fR_p = fT_s = fT_p = 0.5*(1.0 - efficiency);
    // set sines and cosines
    sin_theta1= sqrt(1.0-cos_theta1*cos_theta1);
    sin_theta3= n1/n3 * sin_theta1;
    if (sin_theta3 > 1.0) {
      // total non-transmission -- what to do?
      // total reflection or absorption
      cos_theta3= 0.0;
      fR_s= fR_p= 1.0 - efficiency;
      fT_s= fT_p= 0.0;
      return;
    }
    cos_theta3= sqrt(1.0-sin_theta3*sin_theta3);
    return;
  }
  // else...

  CalculateRT(wavelength,n1,n2,k2,n3,thickness);

  if (VerboseLevel > 3) {
    cout<<"Angles (1,3): "<<360./twopi*asin(sin_theta1)<<" "<<360./twopi*asin(sin_theta3)<<endl;
    cout<<"Rper, Rpar, Tper, Tpar: "<<fR_s<<" "<<fR_p<<" "<<fT_s<<" "<<fT_p<<endl;
  }

}


// Reflect() method, used by DoIt()
void GLG4PMTOpticalModel::Reflect(G4ThreeVector &dir, G4ThreeVector &pol, G4ThreeVector &norm)
{
  dir -= 2.*(dir*norm)*norm;
  pol -= 2.*(pol*norm)*norm;
}

// Refract() method, used by DoIt()
void GLG4PMTOpticalModel::Refract(G4ThreeVector &dir, G4ThreeVector &pol, G4ThreeVector &norm)
{
  dir = (cos_theta3 - cos_theta1*n1/n3)*norm + (n1/n3)*dir;
  pol = (pol-(pol*dir)*dir).unit();
}


// user command handling functions
void GLG4PMTOpticalModel::SetNewValue(G4UIcommand * command, G4String newValues)
{
   // -- database
   ParamBase &db ( ParamBase::GetInputDataBase() );

   G4String commandName= command -> GetCommandName();
   if (commandName == "verbose") {
     VerboseLevel= strtol(newValues.c_str(), nullptr, 0);
   } else if (commandName == "luxlevel") {
     db["pmtopticalmodel.luxlevel"]=luxlevel= strtol(newValues.c_str(), nullptr, 0);
   } else if (commandName == "omit_effvsrho") {
     db["omit_effvsrho"]=omit_effvsrho= strtol(newValues.c_str(), nullptr, 0);
   } else if (commandName == "global_QE_scaling") {
     db["global_QE_scaling"]=global_QE_scaling= strtod(newValues.c_str(), nullptr);
   } else {
     cerr << "No PMTOpticalModel command named " << commandName << endl;
   }
   return;
}

G4String GLG4PMTOpticalModel::GetCurrentValue(G4UIcommand * command)
{
  G4String commandName= command -> GetCommandName();
  if (commandName == "verbose") { return to_string(VerboseLevel); }
  else if (commandName == "luxlevel") { return to_string(luxlevel); }
  else if (commandName == "omit_effvsrho") { return to_string(omit_effvsrho); }
  else if (commandName == "global_QE_scaling") { return to_string(global_QE_scaling); }
  return (commandName+" is not a valid PMTOpticalModel command");
}

void GLG4PMTOpticalModel::Initialize_abs_norm()
{
  G4double emax = efficiency_photocathode->GetMaxLowEdgeEnergy();
  G4double emin = efficiency_photocathode->GetMinLowEdgeEnergy();
  G4double eint = emax-emin;
  G4double eact = 0.;
  G4double t = thickness_photocathode->Value(thickness_photocathode->GetMaxLowEdgeEnergy());
  //t is the thickness at PMT center (just for reference...)
  G4double lam; //wavelength
  G4double An; //Absorption at normal incidence (from glass-side)
  cos_theta1 = 1.; //normal incidence (this parameter will be later reset in DoIt)
  abs_norm = new G4MaterialPropertyVector(); //call empty constructor
  G4int niter=50;

// First coarse initialization to have some data points at all declared wavelengths (you never know ...)

  for(G4int isample = 0; isample <= niter; isample++) {
    eact=emin+(eint/niter*isample);
    lam = twopi*hbarc/eact;
    CalculateRT(lam,rindex_glass->Value(eact),rindex_photocathode->Value(eact),
		kindex_photocathode->Value(eact),1./*vacuum inside the PMT*/,t);
    An= 1.0 - (fT_s+fR_s); //fT_p and fR_p are the same at normal incidence ...
    abs_norm->InsertValues(eact,An);
  }

// Second iteration to have much finer data points for the wavelengths of interest

  emin = (twopi*hbarc)/(350.*nm); // Typical shortest "useful" wavelength ~ 350 nm
  emax = (twopi*hbarc)/(650.*nm); // Typical PMT sensitivity drops to 0 at ~ 650 nm
  eint = emax-emin;
  niter = 300;

  for(G4int isample = 0; isample <= niter; isample++) {
    eact=emin+(eint/niter*isample);
    lam = twopi*hbarc/eact;
    CalculateRT(lam,rindex_glass->Value(eact),rindex_photocathode->Value(eact),
		kindex_photocathode->Value(eact),1./*vacuum inside the PMT*/,t);
    An= 1.0 - (fT_s+fR_s); //fT_p and fR_p are the same at normal incidence ...
    abs_norm->InsertValues(eact,An);
  }
  return;
}


// Here the Optical Model !!
// Correct formalism implemented by Dario Motta (CEA-Saclay) 23 Feb 2005

void GLG4PMTOpticalModel::CalculateRT(G4double lam, G4double n1_, G4double n2_,
                                 G4double k2_, G4double n3_, G4double t)
{
  // declare some useful constants
  G4complex n2comp(n2_,-k2_); //complex photocathode refractive index
  G4complex eta=twopi*n2comp*t/lam;
  G4complex zi(0.,1.); //imaginary unit

  // declare local variables

  G4complex theta1,theta2,theta3,delta;//geometric parameters
  G4complex r12,r23,t12,t21,t23;//reflection- and transmission-related terms
  G4complex ampr,ampt; //relfection and transmission amplitudes

  // first set sines and cosines
  sin_theta1= sqrt(1.0-cos_theta1*cos_theta1);
  sin_theta3= n1_/n3_ * sin_theta1;
  if (sin_theta3 > 1.0) {
    // total non-transmission -- what to do???
    // these variables only used to decide refracted track direction,
    // so doing the following should be okay:
    sin_theta3= 1.0;
  }
  cos_theta3= sqrt(1.0-sin_theta3*sin_theta3);

  // Determine all angles
  theta1=asin(sin_theta1);//incidence angle
  theta2=carcsin((n1_/n2comp)*sin_theta1);//complex angle in the photocathode
  theta3=carcsin((n2comp/n3_)*sin(theta2));//angle of refraction into vacuum
  if (imag(theta3)<0.) theta3=conj(theta3);//needed! (sign ambiguity arcsin)

  delta=eta*cos(theta2);

  //Calculation for the s-polarization

  r12=rfunc(n1_,n2comp,theta1,theta2);
  r23=rfunc(n2comp,n3_,theta2,theta3);
  t12=trfunc(n1_,n2comp,theta1,theta1,theta2);
  t21=trfunc(n2comp,n1_,theta2,theta2,theta1);
  t23=trfunc(n2comp,n3_,theta2,theta2,theta3);

  ampr=r12+(t12*t21*r23*exp(-2.*zi*delta))/(1.+r12*r23*exp(-2.*zi*delta));
  ampt=(t12*t23*exp(-zi*delta))/(1.+r12*r23*exp(-2.*zi*delta));

  //And finally...!
  fR_s=real(ampr*conj(ampr));
  fT_s=real(gfunc(n3_,n1_,theta3,theta1)*ampt*conj(ampt));

  //Calculation for the p-polarization

  r12=rfunc(n1_,n2comp,theta2,theta1);
  r23=rfunc(n2comp,n3_,theta3,theta2);
  t12=trfunc(n1_,n2comp,theta1,theta2,theta1);
  t21=trfunc(n2comp,n1_,theta2,theta1,theta2);
  t23=trfunc(n2comp,n3_,theta2,theta3,theta2);

  ampr=r12+(t12*t21*r23*exp(-2.*zi*delta))/(1.+r12*r23*exp(-2.*zi*delta));
  ampt=(t12*t23*exp(-zi*delta))/(1.+r12*r23*exp(-2.*zi*delta));

  //And finally...!
  fR_p=real(ampr*conj(ampr));
  fT_p=real(gfunc(n3_,n1_,theta3,theta1)*ampt*conj(ampt));

  return;
}
