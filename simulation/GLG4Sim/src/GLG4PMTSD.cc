// This file is part of the GenericLAND software library.
// $Id: GLG4PMTSD.cc,v 1.2 2005/03/29 21:13:30 gahs Exp $
//
//  GLG4PMTSD.cc
//
//   Records total number of hits on each PMT.
//   Uses Geant4-style hit collection to record hit time, place, etc.
//
//  GLG4 version by Glenn Horton-Smith December, 2004.
//  Based on earlier work by O. Tajima and G. Horton-Smith
//

#include <Parameters.hh>
#include <String.hh>

#include <GLG4PMTSD.hh>
#include <GLG4VEventAction.hh>
#include <GLG4Scintillation.hh>  // for doScintilllation and total energy deposition info
#include <GLG4HitPhoton.hh>

#include <Randomize.hh>
#include <G4Track.hh>
#include <G4Step.hh>
#include <G4HCofThisEvent.hh>
#include <G4TouchableHistory.hh>
#include <G4SDManager.hh>
#include <G4VSolid.hh> // for access to solid store

using namespace std;

GLG4PMTSD::GLG4PMTSD(G4String name, G4int arg_max_pmts, G4int arg_pmt_no_offset, G4int arg_my_id_pmt_size):
  G4VSensitiveDetector(name), max_pmts(arg_max_pmts), pmt_no_offset(arg_pmt_no_offset), my_id_pmt_size(arg_my_id_pmt_size)
{
  const ParamBase &db ( ParamBase::GetDataBase() );

  try {
    VerboseLevel = static_cast<G4int>(db["GLG4PMT_verboselevel"]);
  } catch (runtime_error& err) {
    G4Exception("GLG4PMTSD::GLG4PMTSD","1",FatalException,
      scat("Missing parameter in database: ",err.what()).c_str() );
  }
}

// Here is the real "hit" routine, used by GLG4PMTOpticalModel and by ProcessHits
// It is more efficient in some ways.
void GLG4PMTSD::SimpleHit( G4int ipmt,
			 G4double time,
			 G4double kineticEnergy,
			 const G4ThreeVector &hit_position,
			 const G4ThreeVector &hit_momentum,
			 const G4ThreeVector &hit_polarization,
			 G4int iHitPhotonCount )
{
  G4int pmt_index = ipmt - pmt_no_offset;
  if (pmt_index < 0 || pmt_index >= max_pmts) {
    cerr << "Error: GLG4PMTSD::SimpleHit [" << GetName() << "] passed ipmt="
      << ipmt << ", but max_pmts=" << max_pmts << " and offset=" << pmt_no_offset << " !\n";
    return;
  }

  if (VerboseLevel>1) {
    cout << "SimpleHit: iPMT="<<ipmt<<", t="<<time<<", E="<<kineticEnergy<<", pos="<<hit_position
      <<", mom="<<hit_momentum<<", polar="<<hit_polarization<<", weight="<<iHitPhotonCount<<endl;
  }

  // create new GLG4HitPhoton, the way of recording photo hits on PMTs
  GLG4HitPhoton* hit_photon = new GLG4HitPhoton();
  hit_photon->SetPMTID((int)ipmt);
  hit_photon->SetTime( time );
  hit_photon->SetKineticEnergy( kineticEnergy );
  hit_photon->SetPosition( hit_position.x(), hit_position.y(), hit_position.z() );
  hit_photon->SetMomentum( hit_momentum.x(), hit_momentum.y(), hit_momentum.z() );
  hit_photon->SetPolarization( hit_polarization.x(), hit_polarization.y(), hit_polarization.z() );
  hit_photon->SetCount( iHitPhotonCount );

  //  GLG4VEventAction::GetTheHitPhotons()->AddHitPhoton(hit_photon);
  GLG4VEventAction::GetTheHitPMTCollection()->DetectPhoton(hit_photon);
}
