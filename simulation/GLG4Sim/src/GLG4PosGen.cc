/**@file
 This file defines global position generator for primary events.
 
 This file is part of the GenericLAND software library.
 $Id: GLG4PosGen.cc,v 1.5 2009/09/16 21:14:06 gahs Exp $
 
 @author G.Horton-Smith, August 3, 2001
 */

#include <GLG4PosGen.hh>
#include <GLG4VertexGen.hh> // for GLG4VertexGen_HEPEvt

#include <Parameters.hh>
#include <String.hh>
#include <Regex.hh>
#include <Rand.hh>

#include <G4SystemOfUnits.hh>
#include <G4TransportationManager.hh>
#include <G4Navigator.hh>
#include <G4AffineTransform.hh>
#include <G4VoxelLimits.hh>
#include <G4GeometryTolerance.hh>
#include <G4VPhysicalVolume.hh>
#include <G4PhysicalVolumeStore.hh>
#include <G4PrimaryVertex.hh>
#include <G4Material.hh>
#include <G4ThreeVector.hh>
#include <G4String.hh>
#include <Randomize.hh>
#include <geomdefs.hh>
#include <G4VisExtent.hh>
#include <G4Point3D.hh>

#include "Experiment.hh"
#include <sstream>

class GLG4PosGen_PointPaintFill;
using namespace std;

GLG4VPosGen::GLG4VPosGen(const G4String& name): Name(name)
{
  VerboseLevel = static_cast<G4int>(ParamBase::GetDataBase()["GLG4Gen_verboselevel"]);
}

////////////////////////////////////////////////////////////////

void GLG4VPosGen::GenerateVertexPositions(G4PrimaryVertex *argVertex,
                                          G4double max_chain_time, G4double event_rate, G4bool offset, G4double dt)
{
  G4double max_t0_this_chain= max_chain_time;      // initial chain clip
  
  for ( G4PrimaryVertex* v= argVertex; v!=nullptr; v=v->GetNext() ) {
	G4ThreeVector Pos = v->GetPosition();
	GeneratePosition( &Pos ); // initial position
    if (not offset) { v->SetPosition( Pos.x(), Pos.y(), Pos.z() ); }
    else { v->SetPosition( v->GetX0()+Pos.x(), v->GetY0()+Pos.y(), v->GetZ0()+Pos.z() );  }
    
    v->SetT0( v->GetT0() + dt );
    if (v->GetT0() > max_t0_this_chain) {          // reached clip point of chain?
      dt = -v->GetT0();                            // will remove this t0 from now on
      dt += max_t0_this_chain;                     // start at end of this clip
      dt += -log(1.0-G4UniformRand())/event_rate;  // add new random t0
      GeneratePosition( &Pos );                    // new position
      v->SetT0( v->GetT0() + dt );
      max_t0_this_chain= v->GetT0() + max_chain_time;  // new chain clip
    }
  }
}

////////////////////////////////////////////////////////////////

G4String GLG4VPosGen::Strip(const G4String& stripstr)
{
#ifdef DEBUG
  cout << "GLG4VPosGen::Strip: " << stripstr << endl;
#endif
  G4String str = stripstr;
  Regex("^[\\s\"]*").Sub(str,"");
  Regex("[\"]$").Sub(str,"");
  return str;
}

////////////////////////////////////////////////////////////////

G4ThreeVector GLG4VPosGen::GetPointFromString(const G4String& str)
{
  string number("([+-]?\\d+\\.?\\d*([eE][+-]?\\d+)?)");
  Regex reg(scat("\\(",number,",",number,",",number,"\\)"));
  reg.Match(str);
  
  if (reg.Nb() != 3) {
    cout << "  |"<<str << "|  Nb split: " << reg.Nb();
    for (G4int i=0; i<reg.Nb(); i++) { cout << "\t|"<<reg.at(i)<<"|"; } cout << endl;
    G4Exception("GLG4VPosGen::GetPointFromString","1",FatalErrorInArgument,
                scat("Can not recognize a point in database: ",str,"\nExpecting a point with format (x,y,z)").c_str() );
  }
  return G4ThreeVector(reg.stod(0),reg.stod(1),reg.stod(2));
}

////////////////////////////////////////////////////////////////

void GLG4PosGen_PointPaintFill::GeneratePosition( G4ThreeVector *argResult )
{
  switch (Mode) {
    case kFill:  GenerateFill(argResult);  break;
    case kPoint: GeneratePoint(argResult); break;
    case kPaint: GeneratePaint(argResult); break;
    default: G4Exception("GLG4PosGen_PointPaintFill::GeneratePosition","0",FatalErrorInArgument,"No mode selected");
  }
}

void GLG4PosGen_PointPaintFill::GeneratePoint(G4ThreeVector* argResult)
{
  // simplest case: fixed position
  if (VerboseLevel>0) {
    cout << "GLG4PosGen_PointPaintFill::GeneratePoint: at: "<<Pos<<"\n";
  }
  *argResult= Pos;
}

void GLG4PosGen_PointPaintFill::GenerateFill(G4ThreeVector* argResult)
{
  // more complicated case: generate points uniformly in the surrounding volume.
  
  // loop over the following:
  //  generate points in bounding box of solid until we find one inside
  //  convert to global coordinates
  //  if Material == nullptr, then
  //    locate global point and see if it is in target physical volume
  //      (N.B. it might legitimately be in a daughter volume)
  //  if Material != nullptr, then
  //    locate global point and see if the corresponding volume
  //    is made of the stated material (ok if it is a daughter volume)
  // repeat until point found in target volume
  
  G4ThreeVector rpos; // this will hold the position result
  G4Navigator* gNavigator = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking();
  // G4AffineTransform local_to_global = gNavigator->GetLocalToGlobalTransform();
  G4int testLoop = 0;
  
  for (G4int iloop=0, jloop=0; /* test inside loop */ ; iloop++) { // look for internal point
    
    do {
      rpos= G4ThreeVector(BoundingBoxCenter.x()+BoundingBoxDim.x()*(G4UniformRand()-0.5),
                          BoundingBoxCenter.y()+BoundingBoxDim.y()*(G4UniformRand()-0.5),
                          BoundingBoxCenter.z()+BoundingBoxDim.z()*(G4UniformRand()-0.5)); // uniform in bounding box
      
      NbTried++;
      testLoop = Volume->GetLogicalVolume()->GetSolid()->Inside(rpos-translation);
      //in case of filling the World : option to generate the position not too far from the lab
      if(Volume->GetLogicalVolume()->GetName() == "logicRock" && maxDistanceInRock > 0.)
      {
        auto VolConcrete = G4PhysicalVolumeStore::GetInstance()->GetVolume("physConcrete");
        G4double distanceToConcrete = VolConcrete->GetLogicalVolume()->GetSolid()->DistanceToIn(rpos-VolConcrete->GetObjectTranslation());
        if(VolConcrete->GetLogicalVolume()->GetSolid()->DistanceToIn(rpos-VolConcrete->GetObjectTranslation()) >= maxDistanceInRock){
          testLoop = 0;
        }
        if(VerboseLevel > 0){
          cout << "Try n°" << NbTried << " and distance to physConcrete is : " << distanceToConcrete << " mm " << endl;
        }
      }
      jloop ++;
      if (jloop > 100000) {// if rpos still not in the solid after 100000 tries : problem with bounding box
        cout << " Warning : could not locate a point inside the bounding box and inside the solid associated to the logical volume" << endl;
        break;
      }
    } while (!testLoop); // Now we are in our solid! but it can be in a daughter volume as well : we need another test
    G4VPhysicalVolume *pvtest = gNavigator->LocateGlobalPointAndSetup(rpos,nullptr,true); // fast check mode
    //Now we want to check if our point in the bounding box is exactly in our specifical volume in the macro file,
    //so not in a daughter volume and not in the boundingBox\{ourVolume}
    if ( Material == nullptr ) {
      if ( pvtest == Volume ) { break; } // we found it!
    } else {
      if ( pvtest != nullptr
          && pvtest->GetLogicalVolume()->GetMaterial() == Material ) { break; } // we found it!
    }
    if (iloop > 100000) {
      cout << "Warning : Could not locate point in the logical volume" << endl;
      cout << "- maybe daughter and mother volume are the same" << endl;
      cout << "- or material given does not correspond to the material in the logicalVolume" << endl;
      break;} //if after 100000 tries, still not in the volume, we break
  }
  NbFound++;
  *argResult= (rpos);
  if (VerboseLevel>0) {
    cout << "GLG4PosGen_PointPaintFill::GenerateFill: at: "<<rpos<<endl;
  }
}

void GLG4PosGen_PointPaintFill::GeneratePaint(G4ThreeVector* argResult)
{
  // another complicated case: generate points uniformly on the
  // surface of the surrounding volume.  The mathematical technique
  // used here relies on what I think of as "Olber's Law":
  // given a surrounding surface with uniform # sources per unit area and
  // cos(theta) distribution of emitted rays (theta is angle from normal),
  // the brightness per unit area on the surface of a contained volume
  // is uniform (and the incident rays have a cos(theta) distribution,
  // although this is largely irrelevant for us).  This is true even for
  // non-convex solids if one considers all intercepts, such that the
  // interior solid does not shadow itself.
  
  G4ThreeVector rpos; // this will hold the position result
  G4Navigator* gNavigator = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking();
  //G4AffineTransform local_to_global = gNavigator->GetLocalToGlobalTransform();
  G4double kCarTolerance = G4GeometryTolerance::GetInstance()->GetSurfaceTolerance();
  G4VSolid *solid= Volume->GetLogicalVolume()->GetSolid();
  //G4int iloop=0, jloop=0;
  //while (iloop++) {
  for (G4int iloop=0, jloop=0; /* test inside loop */ ; iloop++) {
    // First decide whether we will use a stacked intercept or trace a new ray.
    // The following procedure avoids using consecutive intercepts
    // from the same ray; in order to avoid an ever-increasing list size,
    // it is more likely to use an existing intercept as the number of
    // intercepts grows.
    G4int iint= (G4int)((Intercepts.size()+2)*G4UniformRand());
    if (iint >= (G4int)(Intercepts.size())) {
      G4double Rsphere= 0.50001*BoundingBoxDim.mag() + kCarTolerance;
      G4ThreeVector sphere_center(BoundingBoxCenter[0],
                                  BoundingBoxCenter[1],
                                  BoundingBoxCenter[2]);
      while (iint >= G4int(Intercepts.size())) {
        
        // "infinite" loop test
        jloop++;
        if ( jloop >= 100000 ) {
          cerr << "GLG4PosGen_PointPaintFill::GeneratePaint: " << iloop << "," << jloop
          << " loops spent looking for point within " << PaintThickness
          << "-mm-thick surface layer of " << VolumeName;
          if (Material) { cerr << " with material " << MaterialName; }
          cerr << endl;
          goto breakout2;
        }
        
        // make a new ray and add any intercepts; repeat until we have enough.
        // generate direction:
        // the following cute sequence generates a cos(theta) distribution!
        G4double u,v,w;
        do {
          u= G4UniformRand()*2.0-1.0;
          v= G4UniformRand()*2.0-1.0;
          w= 1.0- (u*u+v*v);
        } while (w < 0.0);
        w= sqrt(w);
        // (end of cute sequence.)
        // generate position on unit sphere:
        G4ThreeVector spos;
        G4double r2;
        do {
          spos= G4ThreeVector(G4UniformRand()*2.0-1.0,
                              G4UniformRand()*2.0-1.0,
                              G4UniformRand()*2.0-1.0);
          r2= spos.mag2();
        } while (r2 > 1.0 || r2 < 0.0625);
        spos*= sqrt(1.0/r2);
        // rotate direction to be relative to normal
        G4ThreeVector e1= spos.orthogonal().unit();
        G4ThreeVector e2= spos.cross(e1);
        G4ThreeVector raydir= u*e1 + v*e2 - w*spos;
        // scale and offset position to be on surrounding sphere
        spos*= Rsphere;
        spos+= sphere_center;
        // now find intercepts
        for (G4int isafety=0; isafety<20; isafety++) {
          // usually will exit loop as soon as no intercept found,
          // unless there is a bug in a geometry routine
          G4double dist= solid->DistanceToIn( spos, raydir );
          if (dist >= 2.0*Rsphere)
          break;
          if (dist < 0.0) {
            cerr << "GLG4PosGen_PointPaintFill: strange DistanceToIn " << dist << " on "
            <<VolumeName<<" at "<<spos<<" in dir "<<raydir<<" loop "<<isafety<<endl;
          }
          if (dist > kCarTolerance) {
            spos += dist*raydir;
            if (PaintThickness == 0.0)
            Intercepts.push_back(spos);
            else
            Intercepts.push_back(spos+solid->SurfaceNormal(spos)*G4UniformRand()*PaintThickness);
          } else {
            spos += kCarTolerance*raydir;
            //cout << kCarTolerance << endl;
          }
          
          dist= solid->DistanceToOut( spos, raydir );
          if (dist >= 2.0*Rsphere)
          break;
          if (dist <= 0.0) {
            cerr << "GLG4PosGen_PointPaintFill: strange DistanceToOut " << dist
            << " on " << VolumeName << " at " << spos << " in dir "
            << raydir << " loop " << isafety << endl;
          }
          if (dist > kCarTolerance) {
            spos += dist*raydir;
            if (PaintThickness == 0.0)
            Intercepts.push_back(spos);
            else
            Intercepts.push_back(spos+solid->SurfaceNormal(spos)*G4UniformRand()*PaintThickness);
          }
          else {
            spos += kCarTolerance*raydir;
          }
        }
        // have now added any and all intercepts for this ray
      }
      // now have enough intercepts to satisfy request for intercept # iint
    }
    rpos= Intercepts[iint] ;
    Intercepts.erase(Intercepts.begin()+iint);
    // the above line is not as inefficient as an old C or early C++
    // programmer might think, due to the "allocator".
    
    //local_to_global.ApplyPointTransform( rpos ); // convert to global coords DOES NOT WORK
    //Made manually using the translation attribute as in GenerateFill, defined in SetState below
    rpos += translation;
    // now apply material restriction, if any
    if ( Material == nullptr )
    break; // no material restriction
    
    G4VPhysicalVolume *pvtest = gNavigator->LocateGlobalPointAndSetup(rpos,0,true); // fast check mode
    if ( pvtest != nullptr && pvtest->GetLogicalVolume()->GetMaterial() == Material )
    break; // we found it!
  }
  
breakout2:
  if (VerboseLevel>0) {
    cout << "GLG4PosGen_PointPaintFill::GeneratePaint: at: "<<rpos<<endl;
  }
  *argResult= (rpos);
}

void GLG4PosGen_PointPaintFill::SetState( const G4String& newValues )
{
  G4String str = Strip(newValues);
  if (str.length() == 0) {
    // print help and current state
    cout << "Current state of this GLG4PosGen_PointPaintFill:\n \"" << GetState() << "\"\n\n";
    cout << "Format of argument to GLG4PosGen_PointPaintFill::SetState: \n"
    " \"x_mm y_mm z_mm [fill [volumeName [materialName]]]\"\n or\n"
    " \"x_mm y_mm z_mm [paint [volumeName [thickness [materialName]]]]\"\n"
    " where x_mm, y_mm, z_mm are the coordinates (in mm) of a point,\n"
    " \"fill\" is an optional keyword to indicate volume-filling mode,\n"
    " \"paint\" is an optional keyword to indicate surface-painting mode,\n"
    " volumeName is the name of the physicalVolume used by paint and fill modes\n"
    " (otherwise the volume at the point is used),\n"
    " thickness is the thickness (in mm) of the layer of paint, and\n"
    " materialName is an optional modifier which, if present, restricts\n"
    " points to daughter or sibling volumes in the selected region which\n"
    " are composed of the given material.\n"
    " Note that the point is used in point mode or else only if no volumeName is provided\n.";
    return;
  }
  if (str=="volume?") {
    if (Mode != kFill || NbTried <= 0) {
      cout << "volume? inquiry can only be used after filling.\n";
    } else {
      cout << "Fill volume information for " << VolumeName << ":" << MaterialName << endl;
      cout << "  ntried= " << NbTried << endl;
      cout << "  nfound= " << NbFound << endl;
      cout << "  bounding box volume: " << BoundingBoxVolume/meter3 << " m^3\n";
      cout << "  filled volume: " << BoundingBoxVolume*NbFound/(G4double)NbTried/meter3 << " m^3\n";
      cout << "  estimated fractional precision: "<< sqrt((NbTried-NbFound)*(G4double)NbFound/(G4double)NbTried)/(G4double)NbTried << endl;
    }
    return;
  }
  
  // set position
  G4double x,y,z;
  istringstream is(str.c_str());
  is >> x >> y >> z;
  if (is.fail()) {
    G4Exception("GLG4PosGen_PointPaintFill::SetState","1",FatalErrorInArgument,
                scat("Could not parse three floats from input string: ",str).c_str() );
  }
  
  Pos.set(x,y,z);
  Mode = kPoint; // default
  Volume = nullptr;
  VolumeName = "!";
  PaintThickness = 0.0;
  Material = nullptr;
  MaterialName.clear();
  NbTried = NbFound = 0;
  Intercepts.clear();
  
  // check for "fill" keyword
  G4String keyword;
  is >> keyword;
  if (not (is.fail() || keyword.length()==0) ) {
    if (keyword == "fill") {
      Mode= kFill;
      is >> VolumeName >> maxDistanceInRock >> MaterialName ;   //distance maximum from concrete when generating first particle in World
      // any failure can be safely ignored
      
    } else if (keyword == "paint") {
      Mode= kPaint;
      is >> VolumeName >> PaintThickness >> MaterialName;
      // any failure can be safely ignored
    }
    if (VolumeName.length()==0) { VolumeName= "!"; }
  }
  
  ParamBase &db ( ParamBase::GetInputDataBase() );
  db["GLG4PosGen_PointPaintFill.x"] = Pos.x();
  db["GLG4PosGen_PointPaintFill.y"] = Pos.y();
  db["GLG4PosGen_PointPaintFill.z"] = Pos.z();
  db["GLG4PosGen_PointPaintFill.mode"] = Mode;
  db["GLG4PosGen_PointPaintFill.thickness"] = PaintThickness;
  db["GLG4PosGen_PointPaintFill.materialIndex"] = -1;
  // In all cases other than kPoint, we need to refer to physical volume.
  // check or set related field variables
  if (Mode != kPoint) {
    if (VolumeName.empty() || VolumeName=="!") {
      Volume = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking()->LocateGlobalPointAndSetup(Pos,0,false);
      if (Volume == nullptr) {
        G4Exception("GLG4PosGen_PointPaintFill::SetState","2",FatalErrorInArgument,
                    scat("Could not find any volume at: ",Pos).c_str() );
      }
      if (VerboseLevel>0) {
        cout << "GLG4PosGen_PointPaintFill::SetState: In physical volume: "<<Volume->GetName()<<"\n";
      }
      VolumeName = Volume->GetName();
    } else {
      Volume = G4PhysicalVolumeStore::GetInstance()->GetVolume(VolumeName,true);
      if (Volume == nullptr) {
        cout << "List of volumes:\n";
        for (size_t i=0; i<G4PhysicalVolumeStore::GetInstance()->size(); i++) {
          cout << G4PhysicalVolumeStore::GetInstance()->at(i) << endl;
        }
        G4Exception("GLG4PosGen_PointPaintFill::SetState","3",FatalErrorInArgument,
                    scat("Could not find any volume with name: ",VolumeName).c_str() );
      }
    }
    
    if (MaterialName.length() > 0 ) {
      Material= G4Material::GetMaterial(MaterialName);
      if (Material == nullptr) {
        G4Exception("GLG4PosGen_PointPaintFill::SetState","4",FatalErrorInArgument,
                    scat("Could not find any material with name: ",MaterialName).c_str() );
      }
      // update database information
      db[ (Name+".materialIndex").c_str() ] = Material->GetIndex();
    }
    
    // get bounding box
    G4double tmax;
    G4VoxelLimits voxelLimits;  // Defaults to "infinite" limits.
    
    G4VSolid *solid= Volume->GetLogicalVolume()->GetSolid(); // this is the solid
    //G4VPhysicalVolume* worldVolume=nullptr;
    
    G4AffineTransform affineTransformNull; //Null translation to use in CalculateExtent
    
    G4String logicalName = Volume->GetLogicalVolume()->GetName();
    
    cout << "Logical volume : " << logicalName << endl;
    
    solid->CalculateExtent(kXAxis, voxelLimits, affineTransformNull, BoundingBoxCenter[0], tmax);
    BoundingBoxDim[0] = tmax-BoundingBoxCenter[0];
    solid->CalculateExtent(kYAxis, voxelLimits, affineTransformNull, BoundingBoxCenter[1], tmax);
    BoundingBoxDim[1] = tmax-BoundingBoxCenter[1];
    solid->CalculateExtent(kZAxis, voxelLimits, affineTransformNull, BoundingBoxCenter[2], tmax);
    BoundingBoxDim[2] = tmax-BoundingBoxCenter[2];
    BoundingBoxVolume= BoundingBoxDim[0]*BoundingBoxDim[1]*BoundingBoxDim[2];
    
    BoundingBoxCenter[0] = BoundingBoxCenter[0] + BoundingBoxDim[0]/2;
    BoundingBoxCenter[1] = BoundingBoxCenter[1] + BoundingBoxDim[1]/2;
    BoundingBoxCenter[2] = BoundingBoxCenter[2] + BoundingBoxDim[2]/2;
    
    /**Manually local to global transform : algorithm to trace back the global translation of the center
     logical volume */
    G4String nameMotherLogicVolume;
    G4VPhysicalVolume* Temp = Volume;
    while(Temp->GetLogicalVolume()->GetName() != "logicWorld" && Temp->GetLogicalVolume()->GetName() != "VHlogic_World") {
      translation += Temp->GetObjectTranslation(); //This forces us to have in Temp the physical volume and not only the logical volume or its name
      nameMotherLogicVolume = Temp->GetMotherLogical()->GetName();
      
      for (size_t i=0; i<G4PhysicalVolumeStore::GetInstance()->size(); i++) {
        
        if(G4PhysicalVolumeStore::GetInstance()->at(i)->GetLogicalVolume()->GetName() == nameMotherLogicVolume){
          Temp = G4PhysicalVolumeStore::GetInstance()->at(i);
        }
      }
    }
    BoundingBoxCenter += translation; //local to global coordinates for the bounding box center
  }
  
  if (VerboseLevel>0) {
    cout << "GLG4PosGen_PointPaintFill::SetState: Surrounding box centered on: "
    <<BoundingBoxCenter<<", of dimensions: "<<BoundingBoxDim<<"\n";
  }
}

G4String GLG4PosGen_PointPaintFill::GetState() const
{
  if (VolumeName.length()==0) {
    cerr << "Warning: zero-length volume name caught in GLG4PosGen_PointPaintFile::GetState()\n";
  }
  
  ostringstream os;
  os << Pos.x() << ' ' << Pos.y() << ' ' << Pos.z();
  if (Mode == kFill) {
    os << " fill " << VolumeName << ' ' << MaterialName;
  } else if (Mode == kPaint) {
    os << " paint " << VolumeName << ' ' << PaintThickness << ' '  << MaterialName;
  } os << ends;
  
  return G4String(os.str());
}

////////////////////////////////////////////////////////////////
///////////////////PLANE POSITION GENERATOR ////////////////////
////////////////////////////////////////////////////////////////

void GLG4PosGen_Plane::GenerateVertexPositions( G4PrimaryVertex *argVertex,
                                               G4double max_chain_time,
                                               G4double /*event_rate*/,
                                               G4bool /*offset*/,
                                               G4double dt
                                               )
{
  // find first legitimate primary particle in vertex (skip over informatons)
  G4PrimaryParticle* pp= argVertex->GetPrimary(0);
  while ( pp!=nullptr
         && abs(pp->GetPDGcode())>= ( GLG4VertexGen_HEPEvt::kPDGcodeModulus * GLG4VertexGen_HEPEvt::kISTHEP_InformatonMin ) ) {
    // this particle had ISTHEP >= 100, it is an informaton
    pp= pp->GetNext();
  }
  
  if (pp == nullptr) {
    G4Exception("GLG4PosGen_Plane::GenerateVertexPositions","1",EventMustBeAborted,"No primary track in vertex!");
  }
  
  // generate orthogonal unit vectors to incident direction
  G4ThreeVector dir( pp->GetMomentum().unit() );
  if (dir.x() == 0.0 && dir.y() == 0.0 && dir.z() == 0.0) {
    G4Exception("GLG4PosGen_Plane::GenerateVertexPositions","2",EventMustBeAborted,
                "Primary track has zero momentum!  I don't know what to do!");
  }
  G4ThreeVector e1(dir.y(), -dir.x(), 0.0);
  G4double tmp= e1.mag2();
  if (tmp == 0.0) { e1.setX(1.0); }
  else { e1*= 1.0/sqrt(tmp); }
  G4ThreeVector e2(dir.cross(e1).unit());
  
  // generate position in rectangle normal to incident direction,
  G4ThreeVector startPos( e1*(Width*(G4UniformRand()-0.5))
                         +e2*(Height*(G4UniformRand()-0.5))
                         +thePos);
  
  // find entrance point to Geant4 world + set the vertex positions
  //I think this part of the code checks whether or not the generated position is outside the "World",
  //and if the associated generated vertex (i.e dir) has any chances to reach the "World".
  //If so, it translates the generated position into the world (MV - Sept 2017)
  
  G4Navigator* gNavigator = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking();
  G4VSolid* worldSolid = gNavigator->GetWorldVolume()->GetLogicalVolume()->GetSolid();
  G4double dist_to_in = worldSolid->DistanceToIn(startPos, dir);
  
  if (dist_to_in < kInfinity) {
    // this track hits the world, so set the vertices there
    startPos += dist_to_in * dir;
    for (G4PrimaryVertex* v= argVertex; v!=nullptr; v=v->GetNext()) {
      if (v->GetT0() > max_chain_time) {   // reached clip point of chain?
        G4Exception("GLG4PosGen_Plane::GenerateVertexPositions","3",JustWarning,
                    scat("Vertex time exceeds clip, but splitting not supported for cosmics: t0=",v->GetT0()," max_chain_time=",max_chain_time).c_str() );
      }
      v->SetPosition( v->GetX0() + startPos.x(),
                     v->GetY0() + startPos.y(),
                     v->GetZ0() + startPos.z() );
      v->SetT0( v->GetT0() + dt );
      // just before we are done, reset navigator, to avoid bugs
      // that confuse muon tracking
      gNavigator->LocateGlobalPointWithinVolume(startPos);
    }
  } else {
    // this track misses the world, so set an impossible effective ISTHEP
    // so Geant4 doesn't try to track them
    for (G4PrimaryVertex* v= argVertex; v!=nullptr; v=v->GetNext()) {
      for (pp= v->GetPrimary(0); pp!=nullptr;  pp=pp->GetNext() ) {
        if (abs(pp->GetPDGcode()) < GLG4VertexGen_HEPEvt::kPDGcodeModulus )
        pp->SetPDGcode(pp->GetPDGcode() < 0 ?
                       pp->GetPDGcode()-GLG4VertexGen_HEPEvt::kPDGcodeModulus:
                       pp->GetPDGcode()+GLG4VertexGen_HEPEvt::kPDGcodeModulus);
      }
    }
  }
}

void GLG4PosGen_Plane::GeneratePosition(G4ThreeVector *)
{
  G4Exception( "GLG4PosGen_Plane::GeneratePosition","0",FatalException,
              "This function should never be called.  Results undefined.");
}

void GLG4PosGen_Plane::SetState( const G4String& newValues )
{
  G4String str = Strip(newValues);
  Regex reg("\\s+");
  
  if (reg.Split(str).size() < 1) {
    G4Exception("GLG4PosGen_Plane::SetState","0",FatalErrorInArgument,
                scat("Can not find center, radius, tmin and tmax arguments in splitting string: ",newValues).c_str() );
    
    cout << "Format of argument to GLG4PosGen_Plane: \"(x,y,z) [optional] Width Length\"\n";
    
    cout << "\t- (x,y,z): virtual plane center (units mm, coordinates expressed in the World frame of reference\n";
    
    cout << "\t- Width: virtual plane width (units in mm)\n";
    
    cout << "\t- Lentgh: virtual plane length (units in mm)\n";
    
  }
  
  //Position of the center of the plane
  thePos = GetPointFromString(reg.at(0));
  
  ParamBase &db ( ParamBase::GetInputDataBase() );
  db[ (Name+".X").c_str() ]= thePos.x();
  db[ (Name+".Y").c_str() ]= thePos.y();
  db[ (Name+".Z").c_str() ]= thePos.z();
  
  //optionnal: defines width and lentgh of virtual plane
  if (reg.Split(str).size() > 2) {
    Width = reg.stod(1);
    Height = reg.stod(2);
    db[ (Name+".width").c_str() ]= Width;
    db[ (Name+".height").c_str() ]= Height;
  }
  
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

GLG4PosGen_Point::GLG4PosGen_Point(const G4String& name): GLG4VPosGen(name)
{
  const ParamBase& db(ParamBase::GetDataBase());
  if (db.HasStrValue("PosGen_center")) {
    thePoint = GetPointFromString(db.GetStrValue("PosGen_center"));
  }
}

void GLG4PosGen_Point::SetState(const G4String& newValues)
{
  thePoint = GetPointFromString(Strip(newValues));
}

void GLG4PosGen_Point::GeneratePosition( G4ThreeVector *argResult )
{
  *argResult = thePoint;
}

////////////////////////////////////////////////////////////////

GLG4PosGen_Sphere::GLG4PosGen_Sphere(const G4String& name): GLG4VPosGen(name)
{
  const ParamBase& db(ParamBase::GetDataBase());
  if (db.HasStrValue("PosGen_center")) {
    theSphereCenter = GetPointFromString(db.GetStrValue("PosGen_center"));
  }
  if (db.HasNumValue("PosGen_par1")) { radius = db["PosGen_par1"]; }
  if (db.HasNumValue("PosGen_par2")) { tmin = db["PosGen_par2"]; }
  if (db.HasNumValue("PosGen_par3")) { tmax = db["PosGen_par3"]; }
  if (db.HasStrValue("PosGen_AngType")) { AngType = db["PosGen_AngType"]; }
  else {AngType = "Uniform";}
  Width = 0.;
  Height = 0.;
  
  thePos = {0., 0., 0.};
  theDir = {0., 0., 0.};
  theSphereCenter = {0., 0., 0.};
}

void GLG4PosGen_Sphere::SetState(const G4String& newValues)
{
  G4String str = Strip(newValues);
  Regex reg("\\s+");
  if (reg.Split(str).size() < 4) {
    G4Exception("GLG4PosGen_Sphere::SetState","0",FatalErrorInArgument,
                scat("Can not find center, radius, tmin and tmax arguments in splitting string: ",newValues).c_str() );
    cout << "Format of argument to GLG4PosGen_Sphere: \"(x,y,z) R tmin tmax\"\n";
    cout << "\t- (x,y,z): sphere center\n";
    cout << "\t- R: sphere radius\n";
    cout << "\t- tmin & tmax: min and max theta angle in spherical coordinates\n";
    cout << "\t- Name of angular law (optionnal, default is \"Uniform\")\n";
    cout << "\t- Width Height: width & height of the plane along which a random point is drawn\n";
  }
  
  theSphereCenter = GetPointFromString(reg.at(0));
  radius = reg.stod(1);
  tmin = reg.stod(2);
  tmax = reg.stod(3);
  
  //optionnal: defines a specific angular law along which we draw randome directions
  if (reg.Split(str).size() > 4) {
    AngType = reg.at(4);
  }
  
  //optionnal: specify the plane dimension overwhich a random point is drawn. Plane should be centered on the random point previously drawn on the sphere.
  if (reg.Split(str).size() > 6){
    Width = reg.stod(5);
    Height = reg.stod(6);
  }
}

void GLG4PosGen_Sphere::GeneratePosition( G4ThreeVector *argResult )
{
  if (AngType == "Uniform" || "AtmMuonMode")
  {*argResult = Random<G4ThreeVector>::Sphere(radius,tmin,tmax,0,theSphereCenter);}
  if (AngType == "MuonMode"){*argResult = Random<G4ThreeVector>::Sphere(radius,tmin,tmax,1,theSphereCenter);}
}

//Stolen and adapated from GLG4PosGen_Plane::GenerateVertexPositions
void GLG4PosGen_Sphere::GenerateVertexPositions(G4PrimaryVertex *argVertex,
                                                G4double max_chain_time,
                                                G4double /*event_rate*/,
                                                G4bool /*offset*/,
                                                G4double dt
                                                )
{
  //Generate position on the sphere
  GeneratePosition(&thePos);
  
  //Change the position of the vertex if "AtmMuonMode" is called (i.e. HK muon generator)
  if (AngType == "AtmMuonMode"){
    //retrieve position of generated muon from argVertex
    thePos = -1. * argVertex->GetPrimary(0)->GetMomentumDirection();//For AtmMuonMode, the vertex position vector is the inverted direction vector of the first muon, i.e. muon direction goes downward, but position vector points upwards (to the sky)
    thePos.unit();
    thePos *= radius;//Direction is a unit vector, hence scale it to the radius
    thePos += theSphereCenter;//Move it to the reference frame of the sphere
  }
  
  //Find first legitimate primary particle in vertex (skip over informatons)
  G4PrimaryParticle* pp= argVertex->GetPrimary(0);

    /** This code section prevented any ion partlce-type to be generated - I don't remember why it is here - MV May 2020*/
    //  while ( pp!=nullptr
    //         && abs(pp->GetPDGcode())>= ( GLG4VertexGen_HEPEvt::kPDGcodeModulus * GLG4VertexGen_HEPEvt::kISTHEP_InformatonMin ) ) {
    //    // this particle had ISTHEP >= 100, it is an informaton
    //    pp= pp->GetNext();
    //  }
    
  if (pp == nullptr) {
    G4Exception("GLG4PosGen_Sphere::GenerateVertexPositions","1",EventMustBeAborted,"No primary track in vertex!");
  }
  
  //generate orthogonal unit vectors to incident direction previsouly drawn on the sphere
  theDir = (theSphereCenter-thePos).unit();
  if (theDir.x() == 0.0 && theDir.y() == 0.0 && theDir.z() == 0.0) {
    G4Exception("GLG4PosGen_Sphere::GenerateVertexPositions","2",EventMustBeAborted,
                "Direction with respect to which plane should be perpendicular has 0 norm! I don't know what to do, check the generator input parameters!");
  }
  G4ThreeVector e1(theDir.y(), -theDir.x(), 0.0);
  G4double tmp= e1.mag2();
  if (tmp == 0.0) { e1.setX(1.0); }
  else { e1*= 1.0/sqrt(tmp); }
  G4ThreeVector e2(theDir.cross(e1).unit());
  
  //generate position in rectangle normal to incident direction
  G4ThreeVector startPos( e1*(Width*(G4UniformRand()-0.5))
                         +e2*(Height*(G4UniformRand()-0.5))
                         +thePos);
  
  // find entrance point to Geant4 world + set the vertex positions
  //I think this part of the code checks whether or not the generated position is outside the "World",
  //and if the associated generated vertex (i.e dir) has any chances to reach the "World".
  //If so, it translates the generated position into the world (MV - Sept 2017)
  
  G4Navigator* gNavigator = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking();
  G4VSolid* worldSolid = gNavigator->GetWorldVolume()->GetLogicalVolume()->GetSolid();
  G4double dist_to_in = worldSolid->DistanceToIn(startPos, theDir);
  
  if (dist_to_in < kInfinity) {
    // this track hits the world, so set the vertices there
    startPos += dist_to_in * theDir;
    for (G4PrimaryVertex* v= argVertex; v!=nullptr; v=v->GetNext()) {
      if (v->GetT0() > max_chain_time) {   // reached clip point of chain?
        G4Exception("GLG4PosGen_Sphere::GenerateVertexPositions","3",JustWarning,
                    scat("Vertex time exceeds clip, but splitting not supported for cosmics: t0=",v->GetT0()," max_chain_time=",max_chain_time).c_str() );
      }
      v->SetPosition(v->GetX0() + startPos.x(),
                     v->GetY0() + startPos.y(),
                     v->GetZ0() + startPos.z() );
      v->SetT0(v->GetT0() + dt);
      // just before we are done, reset navigator, to avoid bugs
      // that confuse muon tracking
      gNavigator->LocateGlobalPointWithinVolume(startPos);
    }
  }
  else {
    // this track misses the world, so set an impossible effective ISTHEP
    // so Geant4 doesn't try to track them
    for (G4PrimaryVertex* v= argVertex; v!=nullptr; v=v->GetNext()) {
      for (pp= v->GetPrimary(0); pp!=nullptr;  pp=pp->GetNext() ) {
        if (abs(pp->GetPDGcode()) < GLG4VertexGen_HEPEvt::kPDGcodeModulus )
        pp->SetPDGcode(pp->GetPDGcode() < 0 ?
                       pp->GetPDGcode()-GLG4VertexGen_HEPEvt::kPDGcodeModulus:
                       pp->GetPDGcode()+GLG4VertexGen_HEPEvt::kPDGcodeModulus);
      }
    }
  }
}

////////////////////////////////////////////////////////////////

GLG4PosGen_Cylinder::GLG4PosGen_Cylinder(const G4String& name): GLG4VPosGen(name)
{
  const ParamBase& db(ParamBase::GetDataBase());
  if (db.HasStrValue("PosGen_center")) {
    center = GetPointFromString(db.GetStrValue("PosGen_center"));
  }
  if (db.HasNumValue("PosGen_par1")) { rho = db["PosGen_par1"]; }
  if (db.HasNumValue("PosGen_par2")) { half_height = db["PosGen_par2"]; }
  if (db.HasStrValue("PosGen_par3")) {
    G4String str(db.GetStrValue("PosGen_par3"));
    switch(HashRunTime(str)) {
      case "x"_hash: case "X"_hash: axis = Random<G4ThreeVector>::X; break;
      case "y"_hash: case "Y"_hash: axis = Random<G4ThreeVector>::Y; break;
      case "z"_hash: case "Z"_hash: axis = Random<G4ThreeVector>::Z; break;
      default: G4Exception("GLG4PosGen_Cylinder::SetState","2",FatalErrorInArgument,
                           scat("Can not recognize cylinder axis, must be X or Y or Z: ",str).c_str() );
    }
  }
}

void GLG4PosGen_Cylinder::SetState(const G4String& newValues)
{
  G4String str = Strip(newValues);
  Regex reg("\\s+");
  
  if (reg.Split(str).size() != 4) {
    cout << "  |"<<str << "|  Nb split: " << reg.Nb();
    for (G4int i=0; i<reg.Nb(); i++) { cout << "\t|"<<reg.at(i)<<"|"; } cout << endl;
    G4Exception("GLG4PosGen_Cylinder::SetState","1",FatalErrorInArgument,
                scat("Can not find center, radius, half height and axis in splitting string: ",newValues).c_str() );
  }
  
  center = GetPointFromString(reg.at(0));
  rho = reg.stod(1);
  half_height = reg.stod(2);
  
  switch(HashRunTime(reg.at(3).data())) {
    case "x"_hash: case "X"_hash: axis = Random<G4ThreeVector>::X; break;
    case "y"_hash: case "Y"_hash: axis = Random<G4ThreeVector>::Y; break;
    case "z"_hash: case "Z"_hash: axis = Random<G4ThreeVector>::Z; break;
    default: G4Exception("GLG4PosGen_Cylinder::SetState","2",FatalErrorInArgument,
                         scat("Can not recognize cylinder axis, must be X or Y or Z: ",reg.at(3)).c_str() );
  }
}

void GLG4PosGen_Cylinder::GeneratePosition( G4ThreeVector *argResult )
{
  *argResult = Random<G4ThreeVector>::Cylinder(rho,half_height,center,axis);
}

////////////////////////////////////////////////////////////////

GLG4PosGen_Cuboid::GLG4PosGen_Cuboid(const G4String& name): GLG4VPosGen(name)
{
  const ParamBase& db(ParamBase::GetDataBase());
  if (db.HasStrValue("PosGen_center")) {
    center = GetPointFromString(db.GetStrValue("PosGen_center"));
  }
  if (db.HasNumValue("PosGen_par1")) { a = db["PosGen_par1"]; }
  if (db.HasNumValue("PosGen_par2")) { b = db["PosGen_par2"]; }
  if (db.HasNumValue("PosGen_par3")) { c = db["PosGen_par3"]; }
}

void GLG4PosGen_Cuboid::SetState(const G4String& newValues)
{
  G4String str = Strip(newValues);
  Regex reg("\\s+");
  if (reg.Split(str).size() != 4) {
    G4Exception("GLG4PosGen_Cuboid::SetState","0",FatalErrorInArgument,
                scat("Can not find center, a,b,c in splitting string: ",str).c_str() );
  }
  
  center = GetPointFromString(reg.at(0));
  a = reg.stod(1);
  b = reg.stod(2);
  c = reg.stod(3);
}

void GLG4PosGen_Cuboid::GeneratePosition( G4ThreeVector *argResult )
{
  *argResult = Random<G4ThreeVector>::Cuboid(a,b,c,center);
}
