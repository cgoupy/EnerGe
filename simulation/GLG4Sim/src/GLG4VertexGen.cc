/** @file
 Source code for GenericLAND global vertex generators for primary events,
 (See note on GenericLAND generators for more information.)
 
 This file is part of the GenericLAND software library.
 $Id: GLG4VertexGen.cc,v 1.19 2009/10/14 02:57:36 gahs Exp $
 
 @author G.Horton-Smith, August 3, 2001
 */

#include <GLG4VertexGen.hh>

#include <Parameters.hh>
#include <Regex.hh>
#include <Rand.hh>
#include <String.hh>

#include <G4VPhysicalVolume.hh>
#include <G4PhysicalVolumeStore.hh>
#include <G4ios.hh>
#include <G4Types.hh>
#include <G4String.hh>
#include <G4ThreeVector.hh>
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <G4RunManager.hh>    // for AbortRun()
#include <G4ParticleDefinition.hh>
#include <G4ParticleTable.hh>
#include <G4ParticleMomentum.hh>
#include <G4IonTable.hh>
#include <G4Ions.hh>
#include <G4OpticalPhoton.hh>
#include <G4Event.hh>
#include <G4Track.hh>
#include <G4HEPEvtParticle.hh>
#include <G4Alpha.hh>
#include <G4Electron.hh>
#include <G4Positron.hh>
#include <G4Gamma.hh>
#include <Randomize.hh>
#include <CLHEP/Random/RandGeneral.h>

#include <GLG4PosGen.hh>      // for Strip() utility function
#include <GLG4PrimaryGeneratorAction.hh>

#include <string.h>  // for strcmp

#include <cstdio>
#include <vector>
#include <math.h>
#include <fstream>
#include <sstream>
#include <iostream>

using namespace std;

////////////////////////////////////////////////////////////////

const char * GLG4VertexGen_Gun::theElementNames[] = {
    "H",                                                                                                "He",
    "Li", "Be",                                                             "B",  "C",  "N",  "O", "F", "Ne",
    "Na", "Mg",                                                             "Al", "Si", "P", "S", "Cl", "Ar",
    "K", "Ca", "Sc", "Ti", "V", "Cr", "Mn", "Fe", "Co", "Ni", "Cu", "Zn", "Ga", "Ge", "As", "Se", "Br", "Kr",
    "Rb", "Sr", "Y", "Zr", "Nb", "Mo","Tc", "Ru", "Rh", "Pd", "Ag", "Cd", "In", "Sn", "Sb", "Te", "I",  "Xe",
    "Cs", "Ba",
    "La", "Ce", "Pr", "Nd", "Pm", "Sm", "Eu", "Gd", "Tb", "Dy", "Ho", "Er", "Tm", "Yb", "Lu",
    "Hf", "Ta", "W", "Re", "Os", "Ir", "Pt", "Au", "Hg", "Tl", "Pb", "Bi", "Po", "At", "Rn",
    "Fr", "Ra",
    "Ac", "Th", "Pa",  "U", "Np", "Pu", "Am", "Cm", "Bk", "Cf", "Es", "Fm", "Md", "No", "Lr",
    "Rf", "Db", "Sg", "Bh", "Hs", "Mt", "Xa"
};

////////////////////////////////////////////////////////////////

GLG4VVertexGen::GLG4VVertexGen(const G4String& name): Name(name)
{
    VerboseLevel = static_cast<G4int>(ParamBase::GetDataBase()["GLG4Gen_verboselevel"]);
}

G4ThreeVector GLG4VVertexGen::GetTargetDir(G4PrimaryVertex* vertex, G4String TargetVolName)
{
    G4ThreeVector dir (0., 0., 0.);
    /*Manually local to global transform : algorithm to trace back the global translation of the center
     logical volume */
    
    if(TargetVolName != "NONE"){ // We calculate the target coordinate only once.
        G4String nameMotherLogicVolume;
        G4VPhysicalVolume* Temp = G4PhysicalVolumeStore::GetInstance()->at(0);
        G4ThreeVector translation = G4ThreeVector(0.,0.,0.);
        
        //finding the physical volume for VolumeTarget
        for (size_t i=0; i<G4PhysicalVolumeStore::GetInstance()->size(); i++) {
            if(G4PhysicalVolumeStore::GetInstance()->at(i)->GetName() == TargetVolName){
                Temp = G4PhysicalVolumeStore::GetInstance()->at(i);
            }
        }
        
        //finding the global coordinates of VolumeTarget
        while(Temp->GetName() != "World" && Temp->GetName() != "VHphysic_World") {
            translation += Temp->GetObjectTranslation(); //This forces us to have in Temp the physical volume and not only the logical volume or its name
            nameMotherLogicVolume = Temp->GetMotherLogical()->GetName();
            for (size_t i=0; i<G4PhysicalVolumeStore::GetInstance()->size(); i++) {
                
                if(G4PhysicalVolumeStore::GetInstance()->at(i)->GetLogicalVolume()->GetName() == nameMotherLogicVolume){
                    Temp = G4PhysicalVolumeStore::GetInstance()->at(i);
                }
            }
        }
        
        dir = (translation - vertex->GetPosition()).unit();
    }
    return dir;
}

//************************************************************//
//////////////////////// GUN GENERATOR /////////////////////////
//************************************************************//

GLG4VertexGen_Gun::GLG4VertexGen_Gun(const G4String& name): GLG4VVertexGen(name)
{
    Particle= G4ParticleTable::GetParticleTable()->FindParticle("geantino");
}

/* Generates one or more particles with a specified momentum, or isotropic
 momentum with specific energy, and specified polarization, or uniformly
 random polarization, based on parameters set via SetState() */
void GLG4VertexGen_Gun::GeneratePrimaryVertex(G4Event *argEvent)
{
    for (G4int imult=0; imult<Multiplicity; imult++) {
        G4PrimaryVertex* vertex= new G4PrimaryVertex( 0.,0.,0.,0. );
        
        if (Particle == nullptr) {
            Particle= G4ParticleTable::GetParticleTable()->FindParticle("geantino");
        }
        
        G4double mass= Particle->GetPDGMass();
        
        G4PrimaryParticle* particle = nullptr;
        
        G4ThreeVector dir;
        if (P.mag2() > 0.0) {
            
            if(AniType=="gauss") {
                // Gaussian-theta beam
                G4double d_sigma = Ani * M_PI / 180.;
                
                G4double d_phi = 2.*M_PI*G4UniformRand();
                G4double d_theta = d_sigma * G4RandGauss::shoot();
                
                G4double d_x = sin(d_theta) * cos(d_phi);
                G4double d_y = sin(d_theta) * sin(d_phi);
                G4double d_z = cos(d_theta);
                
                dir.set( d_x, d_y, d_z );
                
                G4double d_T = P.theta();
                G4double d_P = P.phi();
                
                dir.rotateY(d_T);
                dir.rotateZ(d_P);
                
                G4ThreeVector rmom( dir * sqrt(Ke*(Ke+2.*mass)) );
                particle= new G4PrimaryParticle(Particle, rmom.x(), rmom.y(), rmom.z());
            }
            else if (AniType=="target" || AniType=="center") {
                /*Anisotropy according to the target volume. This is handled by ::SetDirectionTarget, called in
                 * GLG4PrimaryGeneratorAction : because the simulation creates first the vertex (with impulsion
                 * and its direction), before creating its position. No other choice to create a random direction
                 * that will be changed after by this method*/
                particle = new G4PrimaryParticle(Particle, P.x(), P.y(), P.z() );
                dir= P.unit();
            }
            else {
                // fixed momentum and direction
                particle= new G4PrimaryParticle(Particle, P.x(), P.y(), P.z() );
                dir= P.unit();
            }
        }
        else { // P.mag2() <=0, isotropic direction
            /* generate random ThreeVector of unit length isotropically
             distributed on sphere by cutting two circles from a rectangular
             area and mapping circles onto top and bottom of sphere.
             N.B. the square of the radius is a uniform random variable.
             -GAHS.
             */
            if(Ani==0.){
                while (1) {
                    G4double u,v,q2;
                    // try first circle
                    u= 3.5*G4UniformRand()-1.75;
                    v= 2.0*G4UniformRand()-1.0;
                    q2= u*u + v*v;
                    if (q2 < 1.0) {
                        G4double rho_over_q= sqrt(2.0-q2);
                        dir= G4ThreeVector( u*rho_over_q, v*rho_over_q, 1.0-q2 );
                        break;
                    }
                    // try second circle
                    u= (u>0.0) ? u-1.75 : u+1.75;
                    v= (v>0.0) ? v-1.00 : v+1.00;
                    q2= u*u + v*v;
                    if (q2 < 1.0) {
                        G4double rho_over_q= sqrt(2.0-q2);
                        dir= G4ThreeVector( u*rho_over_q, v*rho_over_q, q2-1.0 );
                        break;
                    }
                }
            }
            else { /* Anisotropy */
                if(AniType=="zenith" || AniType.length()==0) {
                    
                    //If it is too big set it to the maximum value
                    if (abs(Ani)>1.) { Ani/=abs(Ani); }
                    
                    G4double phi,cost,st;
                    //	cost=2.0*G4UniformRand()-1.0;
                    //   For 1+a*cos(theta)  -1<a<1
                    cost=(sqrt(1.+Ani*(4.*G4UniformRand()+Ani-2.))-1.)/Ani;
                    phi=CLHEP::twopi*G4UniformRand();
                    st=sqrt(1.-cost*cost);
                    dir=G4ThreeVector(st*cos(phi),st*sin(phi),cost);
                    
                }
                else {
                    cerr << "Invalid AniType is set" << endl;
                    break;
                }
            }
            
            G4double keLocal;
            keLocal= (SJit==0.) ? Ke : Ke+SJit*G4RandGauss::shoot() ;
            
            G4ThreeVector rmom( dir * sqrt(keLocal*(keLocal+2.*mass)) );
            particle = new G4PrimaryParticle(Particle, rmom.x(), rmom.y(), rmom.z() );
        }
        
        // adjust polarization if needed
        if (Particle->GetPDGSpin() == 1.0 && mass == 0.0) {
            // spin 1 mass 0 particles should have transverse polarization
            G4ThreeVector rpol= Polar - dir * (dir*Polar);
            G4double rpolmag= rpol.mag();
            if (rpolmag > 0.0) {
                // use projected orthogonal pol, normalized
                rpol *= (1.0/rpolmag);
            } else {
                // choose random pol
                G4double phi= (G4UniformRand()*2.0-1.0)*M_PI;
                G4ThreeVector e1= dir.orthogonal().unit();
                G4ThreeVector e2= dir.cross(e1);
                rpol= e1*cos(phi)+e2*sin(phi);
            }
            particle->SetPolarization(rpol.x(), rpol.y(), rpol.z());
        } else {
            // use user-set polarization unmodified
            particle->SetPolarization(Polar.x(), Polar.y(), Polar.z());
        }
        
        particle->SetMass(mass); // Geant4 is silly.
        vertex->SetPrimary( particle );
        
        if(TJit!=0.||TCont!=0) {
            G4double d_t_dist = TCont * G4UniformRand() + TJit * G4RandGauss::shoot();
            vertex->SetT0(d_t_dist);
        }
        argEvent->AddPrimaryVertex(vertex);
    }
}

/* Set state of the GLG4VertexGen_Gun, or show current state and
 explanation of state syntax if empty string provided.
 
 Format of argument to GLG4VertexGen_Gun::SetState:
 "pname  momx_MeV momy_MeV momz_MeV KE_MeV  polx poly polz mult ani tjit sjit",
 where
 - pname is the name of a particle type (e-, mu-, U238, ...)
 - mom*MeV is a momentum in MeV/c
 - KE_MeV is an optional override for kinetic energy
 - AnyType is an optional override for setting specific particle momenta
 - pol* is an optional polarization vector.
 - mult is an optional multiplicity of the particles.
 If mom*MeV==0 and KE_MeV!=0, then random isotropic directions chosen.
 If pol*==0, then random transverse polarization for photons.
 - ani is an optional anisotropy -1<ani<1.
 - tjit is an optional time jitter in nsec.
 - sjit is an optional spectral jitter in MeV.
 */
void GLG4VertexGen_Gun::SetState(G4String newValues)
{
#ifdef DEBUG
    cout << "GLG4VertexGen_Gun::SetState: " << newValues << "\n";
#endif
    G4String str = GLG4VPosGen::Strip(newValues);
    if (str.length() == 0) {
        // print help and current state
        cout << "Current state of this GLG4VertexGen_Gun:\n"
        << " \"" << GetState() << "\"\n" << endl;
        cout << "Format of argument to GLG4VertexGen_Gun::SetState: \n"
        " \"pname  momx_MeV momy_MeV momz_MeV KE_MeV AnyType polx poly polz mult\"\n"
        " where pname is the name of a particle type (e-, mu-, U238, ...)\n"
        " mom*MeV is a momentum in MeV/c\n"
        " KE_MeV is an optional override for kinetic energy\n"
        " AnyType is an optional override for setting specific particle momenta \n"
        " pol* is an optional polarization vector.\n"
        " mult is an optional multiplicity of the particles.\n"
        " mom*MeV==0 and KE_MeV!=0 --> random isotropic directions chosen\n"
        " pol*==0 --> random transverse polarization for photons.\n"
        " ani is an optional anisotropy -1<ani<1.\n"
        " tjit is an optional time jitter in nsec.\n"
        " sjit is an optional spectral jitter in MeV.\n\n";
        return;
    }
    
    ParamBase &db ( ParamBase::GetInputDataBase() );
    istringstream is(str.c_str());
    
    //Set particle
    string pname;
    is >> pname;
    cout << "Particle name: " << pname << "\n";
    if (is.fail() || pname.length()==0) return;
    
    G4ParticleDefinition * newTestGunG4Code = G4ParticleTable::GetParticleTable()->FindParticle(pname);
    
    if (newTestGunG4Code == nullptr) {
        // not a particle name
        // see if we can parse it as an ion, e.g., U238 or Bi214
        string elementName;
        G4int A,Z;
        if (pname[1] >= '0' && pname[1] <='9') {
            A= atoi(pname.substr(1).c_str());
            elementName= pname.substr(0,1);
        }
        else {
            A= atoi(pname.substr(2).c_str());
            elementName= pname.substr(0,2);
        }
        if (A > 0) {
            for (Z=1; Z<=numberOfElements; Z++)
                if (elementName == theElementNames[Z-1])
                    break;
            if (Z <= numberOfElements)
                newTestGunG4Code=G4ParticleTable::GetParticleTable()->GetIonTable()->GetIon(Z, A, 0.0);
        }
        if (newTestGunG4Code == nullptr) {
            cerr << "test gun particle type not changed! Could not find " << pname << endl;
            return;
        }
    }
    
    Particle= newTestGunG4Code;
    db[ (Name+".pdgcode").c_str() ] = Particle->GetPDGEncoding();
    
    //Set momentum
    G4double x,y,z;
    is >> x >> y >> z;
    if (is.fail()) { return; }
    P= G4ThreeVector(x*MeV,y*MeV,z*MeV);
    Ke= sqrt(P.mag2()+Particle->GetPDGMass()*Particle->GetPDGMass()) - Particle->GetPDGMass();
    
    //Optional override of kinetic energy
    G4double p_renorm;
    is >> x;
    if (is.fail()) {
        db[ (Name+".px").c_str() ] = P.x();
        db[ (Name+".py").c_str() ] = P.y();
        db[ (Name+".pz").c_str() ] = P.z();
        db[ (Name+".KE").c_str() ] = Ke;
        return;
    }
    if (x <= 0.0) {
        p_renorm = 0.0;
        Ke= 0.0;
    }
    else {
        Ke= x*MeV;
        if (P.mag2() <= 0.0) { p_renorm= 0.0; }
        else { p_renorm= sqrt( Ke*(Ke+2.0*newTestGunG4Code->GetPDGMass()) / P.mag2() ); }
    }
    
    P *= p_renorm;
    db[ (Name+".px").c_str() ] = P.x();
    db[ (Name+".py").c_str() ] = P.y();
    db[ (Name+".pz").c_str() ] = P.z();
    db[ (Name+".KE").c_str() ] = Ke;
    
    //Set Anisotropy Type
    // "zenith"  1 - a*cos(zenith); -1<_Ani<1
    // "gauss" gaussian-theta beam; 0<_Ani<180 (degree)
    // "target VolumeTarget" initial generated momenta will point toward center of 'VolumeTarget'
    // "center" initial generated momenta will point toward the coordinates of the position generator
    
    // If one wants to make the particle aim at a specific volume
    // NB : the action of the method SetDirectionTarget cannot be done in GeneratePrimaryVertex as the others AniType
    // We first have to wait that the position of our particle is generated. This is done in PrimaryGeneratorAction,
    // where we access to the vertex created here, and change its direction in the case AniType == "target".
    // See PrimaryGeneratorAction in ::GeneratePrimaries()
    
    is >> AniType;
    if (is.fail()) { return; }
    
    if (AniType == "target"){
        is >> VolumeTarget;
        if (is.fail()) {return;}
        cout << "Will aim all vertices at "<< VolumeTarget << " center..." << endl;
    }
    
    if (AniType == "center"){
        cout << "Will aim all vertices at position generator center..." << endl;
    }
    
    // set particle polarization
    is >> x >> y >> z;
    if (is.fail()) { return; }
    Polar= G4ThreeVector(x,y,z);
    db[ (Name+".polx").c_str() ] = Polar.x();
    db[ (Name+".poly").c_str() ] = Polar.y();
    db[ (Name+".polz").c_str() ] = Polar.z();
    
    // set multiplicity
    is >> Multiplicity;
    if (is.fail()) { return; }
    
    // set Anisotropy
    is >> Ani;
    if (is.fail()) { return; }
    
    // set time jitter
    is >> TJit;
    if (is.fail()) { return; }
    
    // set spectral jitter
    is >> SJit;
    if (is.fail()) { return; }
    
    // set Time continuity
    is >> TCont;
    if (is.fail()) { return; }
}

G4String GLG4VertexGen_Gun::GetState()
{
    ostringstream os;
    
    if (Particle == nullptr) {
        Particle= G4ParticleTable::GetParticleTable()->FindParticle("geantino");
    }
    os << Particle->GetParticleName() << '\t'
    << P.x() << ' ' << P.y() << ' ' << P.z() << '\t'
    << Ke << '\t'
    << Polar.x() << ' ' << Polar.y() << ' ' << Polar.z() << '\t'
    << Multiplicity << ' ' << Ani << ' ' << TJit << ' ' << SJit << '\t'
    << AniType << ' ' << TCont
    << ends;
    return G4String(os.str());
}

void GLG4VertexGen_Gun::SetDirectionTarget(G4PrimaryVertex* vertex)
{
    G4ThreeVector dir = this->GetTargetDir(vertex,VolumeTarget);
    P = ( dir * P.mag() );
    vertex->GetPrimary(0)->SetMomentum(P.x(), P.y(),P.z()); // we have the G4PrimaryParticle, can change its impulsion
    return;
}

void GLG4VertexGen_Gun::ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir){
    G4ThreeVector dir = Dir.unit();
    P = (dir * P.mag() );
    vertex->GetPrimary(0)->SetMomentum(P.x(), P.y(),P.z());
    return;
}

//************************************************************//
///////////////////// HEP EVENT GENERATOR //////////////////////
//************************************************************//

void GLG4VertexGen_HEPEvt::Open(const char *argFilename)
{
    if ( argFilename == nullptr || argFilename[0]=='\0') {
        G4Exception("GLG4VertexGen_HEPEvt::Open","1",FatalErrorInArgument,"Null file name");
    }
    if (File != nullptr) { Close(); }
    
    FileName = argFilename;
    
    // is a pipe requested?
    if (argFilename[strlen(argFilename)-1]=='|') {
        isPipe = true;
        File = popen(FileName.substr(0,FileName.length()-1).c_str(), "r");
        if (!File) {
            G4Exception("GLG4VertexGen_HEPEvt::Open","2",FatalErrorInArgument,scat("Can not open pipe: ",FileName).c_str() );
        }
    } else {
        isPipe = false;
        File = fopen(argFilename, "r");
        if (!File) {
            G4Exception("GLG4VertexGen_HEPEvt::Open","3",FatalErrorInArgument,scat("Can not open file with FILE: ",FileName).c_str() );
        }
    }
}

void GLG4VertexGen_HEPEvt::Close()
{
    if (File) {
        if (isPipe) {
            G4int status= pclose(File);
            if (status != 0) {
                cerr << "HEPEvt input pipe from " << FileName
                << " gave status " << status << " on close." << endl;
            }
            File= 0;
            isPipe= false;
            FileName+= " [Pipe is now closed!]";
        } else {
            fclose(File);
            File= 0;
            FileName+= " [File is now closed!]";
        }
    }
}

void GLG4VertexGen_HEPEvt::GetDataLine(char *buffer, size_t size)
{
    char firstword[64];
    
    while (true) {
        buffer[0]= '\0';
        if ( fgets(buffer, size, File) != buffer ) {
            // try from beginning of file on failure
            cerr << (feof(File) ? "End of file reached" : "Failure")
            << " on " << FileName << " at " << ftell(File) << endl;
            Close();
            return;
        }
        
        // check for "sentinel" block
        firstword[0]= '\0';
        sscanf(buffer, " %63s", firstword); // leading space means skip whitespace
        if ( firstword[0]=='#' || firstword[0] == '\0' )
            continue;  // This is a comment line or blank line
        if ( firstword[0] >= '0' && firstword[0] <= '9' )
            break;      // found numeric data -- first value is always non-neg G4int!
        if ( strcmp(firstword, "STATE") == 0 ) {
            ParamBase &db ( ParamBase::GetInputDataBase() );
            G4double value;
            G4int nscan;
            while ( fgets(buffer, size, File) == buffer ) {
                firstword[0]= '\0';
                nscan= sscanf(buffer, " %63s %lf", firstword, &value);
                if (firstword[0] == '#' || firstword[0] == '\0') continue;
                if ( strcmp(firstword, "ENDSTATE") == 0 ) break;
                if (nscan == 2)
                    db[ ((Name+".")+firstword).c_str() ]= value;
                else {
                    cerr << "Warning, bad STATE line in " << FileName
                    << ": " << buffer << endl;
                }
            }
            continue;
        }
        else if ( strcmp(firstword, "HEPEVT")==0 ) {
            char sentinel[20];
            sentinel[0]= '\0';
            sscanf(buffer, " HEPEVT DATA SENTINEL IS %19s", sentinel);
            if (sentinel[0]== '\0') {
                cerr << "Warning, line starting with word HEPEVT ignored" << endl;
                continue;
            }
            // skip until sentinel found
            while ( fgets(buffer, size, File) == buffer ) {
                firstword[0]= '\0';
                sscanf(buffer, " %63s", firstword);
                if ( strcmp(firstword, sentinel)==0 )
                    break;
            }
            continue;
        }
        
        // not comment, blank line, STATE, or numeric data
        cerr << "Warning, ignoring garbage line in " << FileName << endl << buffer;
    }
}

/*Generates one or more particles with type, momentum, and
 optional time offset, spatial offset, and polarization,
 based on lines read from file via SetState().
 
 The file has the format
 
 <PRE>
 NHEP
 ISTHEP IDHEP JDAHEP1 JDAHEP2 PHEP1 PHEP2 PHEP3 PHEP5 DT X Y Z PLX PLY PLZ
 ISTHEP IDHEP JDAHEP1 JDAHEP2 PHEP1 PHEP2 PHEP3 PHEP5 DT X Y Z PLX PLY PLZ
 ISTHEP IDHEP JDAHEP1 JDAHEP2 PHEP1 PHEP2 PHEP3 PHEP5 DT X Y Z PLX PLY PLZ
 ... [NHEP times]
 NHEP
 ISTHEP IDHEP JDAHEP1 JDAHEP2 PHEP1 PHEP2 PHEP3 PHEP5 DT X Y Z PLX PLY PLZ
 ISTHEP IDHEP JDAHEP1 JDAHEP2 PHEP1 PHEP2 PHEP3 PHEP5 DT X Y Z PLX PLY PLZ
 ISTHEP IDHEP JDAHEP1 JDAHEP2 PHEP1 PHEP2 PHEP3 PHEP5 DT X Y Z PLX PLY PLZ
 ... [NHEP times]
 </PRE>
 where
 <PRE>
 ISTHEP   == status code
 IDHEP    == HEP PDG code
 JDAHEP   == first daughter
 JDAHEP   == last daughter
 PHEP1    == px in GeV
 PHEP2    == py in GeV
 PHEP3    == pz in GeV
 PHEP5    == mass in GeV
 DT       == vertex _delta_ time, in ns (*)
 X        == x vertex in mm
 Y        == y vertex in mm
 Z        == z vertex in mm
 PLX      == x polarization
 PLY      == y polarization
 PLZ      == z polarization
 </PRE>
 
 PHEP5, DT, X, Y, Z, PLX, PLY, and PLZ are all optional.
 If omitted, the respective quantity is left unchanged.
 If DT is specified, the time offset of this and subsequent vertices
 is increased by DT: (*) note DT is a relative shift from the previous line!
 (This is because there is often a very large dynamic range of time offsets
 in certain types of events, e.g., in radioactive decay chains, and this
 convention allows such events to be represented using a reasonable number
 of significant digits.)
 If X, Y, Z, PLX, PLY, and/or PLZ is specified, then the values replace
 any previously specified.
 */
void GLG4VertexGen_HEPEvt::GeneratePrimaryVertex(G4Event *argEvent)
{
    // this is a modified and adapted version of G4HEPEvt
    // (which itself may be a modified and adapted version of CLHEP/StdHep++...)
    
    if (File == nullptr) {
        cerr << "GLG4VertexGen_HEPEvt::GeneratePrimaryVertex: No file open!\nAborting run for your convenience.\n";
        G4RunManager::GetRunManager()->AbortRun();
        return;
    }
    
    char buffer[400];
    G4int istat;
    G4int NHEP;  // number of entries
    GetDataLine(buffer, sizeof(buffer));
    if (File == nullptr) {
        cerr << "End of file in " << FileName
        << " while expecting NHEP.\nAborting run for your convenience.\n";
        G4RunManager::GetRunManager()->AbortRun();
        return;
    }
    istat= sscanf(buffer, "%d", &NHEP);
    if( istat != 1 ) {
        // this should never happen: GetDataLine() should make sure integer is ok,
        // or else close the file.
        // -- but the test above is cheap and a good cross-check, so leave it.
        cerr << "Bad data in " << FileName << ", expecting NHEP but got:\n"
        << buffer << " --> closing file, aborting run for your convenience.\n";
        Close();
        G4RunManager::GetRunManager()->AbortRun();
        return;
    }
    
    vector<G4HEPEvtParticle*> HPlist;
    vector<G4PrimaryVertex*> vertexList; // same indices as HPList
    vector<G4PrimaryVertex*> vertexSet; // bag of unique vertices
    vertexSet.push_back( new G4PrimaryVertex(0.0,0.0,0.0,0.0) );
    
    G4double vertexX=0.0, vertexY=0.0, vertexZ=0.0, vertexT=0.0;
    
    for( G4int IHEP=0; IHEP<NHEP; IHEP++ ) {
        G4int ISTHEP=0;     // status code
        G4int IDHEP=0;      // HEP PDG code
        G4int JDAHEP1=0;    // first daughter
        G4int JDAHEP2=0;    // last daughter
        G4double PHEP1=0.0; // px in GeV
        G4double PHEP2=0.0; // py in GeV
        G4double PHEP3=0.0; // pz in GeV
        G4double req_novalue=numeric_limits<G4double>::max();
        G4double PHEP5=req_novalue; // mass in GeV
        G4double req_vertexX=req_novalue; // x vertex in mm requested on this line
        G4double req_vertexY=req_novalue; // y vertex in mm "
        G4double req_vertexZ=req_novalue; // z vertex in mm "
        G4double req_vertex_dT=req_novalue; // vertex _delta_ time (in ns, a GLG4HEPEvt convention)
        G4double polx=0.0;  // x polarization
        G4double poly=0.0;  // y polarization
        G4double polz=0.0;  // z polarization
        G4double energy_unit= GeV;
        G4double position_unit= mm;
        G4double time_unit= ns; /* used to be mm/c_light */
        
        GetDataLine(buffer, sizeof(buffer));
        if (File == 0) {
            cerr << "Unexpected end of file in " << FileName
            << ", expecting particle " << IHEP << "/" << NHEP << endl;
            cerr << "Aborting run for your convenience." <<endl;
            G4RunManager::GetRunManager()->AbortRun();
            return;
        }
        istringstream is(buffer);
        is >> ISTHEP >> IDHEP >> JDAHEP1 >> JDAHEP2
        >> PHEP1 >> PHEP2 >> PHEP3 >> PHEP5
        >> req_vertex_dT >> req_vertexX >> req_vertexY >> req_vertexZ  // note order!
        >> polx >> poly >> polz;
        
        if (VerboseLevel) {
            cout<<"HEPEvt vertex: "<<req_vertexX<<" "<<req_vertexY<<" "<<req_vertexZ<<" "<<PHEP1*1000<<" "<<PHEP2*1000<<" "<<PHEP3*1000
            <<" "<<sqrt( PHEP5*PHEP5 + PHEP1*PHEP1+ PHEP2*PHEP2+ PHEP3*PHEP3)*1000<<" "<<req_vertex_dT/1000.<<" "<<IDHEP<<" "<<-12345<<"\n";
        }
        // reset units if "dimensionless" pseudo-particle information
        if (ISTHEP >= kISTHEP_InformatonMin) {
            energy_unit = position_unit = time_unit = 1.0;
        }
        
        // frobnicate IDHEP if ISTHEP != 1
        if (ISTHEP != kISTHEP_ParticleForTracking) {
            if (ISTHEP <= 0 || ISTHEP > kISTHEP_Max) {
                cerr << "Error in GLG4VertexGen_HEPEvt[" << FileName << "]: ISTHEP="<< ISTHEP
                << " not allowed: valid range is 1 to " << kISTHEP_Max << " --> set to " << kISTHEP_Max << endl;
                ISTHEP= kISTHEP_Max;
            }
            if (IDHEP < 0) { IDHEP -= kPDGcodeModulus*ISTHEP; }
            else { IDHEP += kPDGcodeModulus*ISTHEP; }
        }
        
        // create G4PrimaryParticle object
        // create an "ion" (nucleus) if IDHEP>9800000
        G4PrimaryParticle* particle = nullptr;
        if (IDHEP > kIonCodeOffset && IDHEP < kIonCodeOffset+99999) {
            G4int A= IDHEP%1000;
            G4int Z= (IDHEP/1000)%100;
            G4ParticleDefinition * g4code = G4ParticleTable::GetParticleTable()->GetIonTable()->GetIon(Z, A, 0.0);
            
            if (g4code == 0) {
                cerr << "Warning: GLG4HEPEvt could not find Ion Z=" <<Z<< " A=" << A << " code=" << IDHEP << endl;
                particle = new G4PrimaryParticle( IDHEP, PHEP1*energy_unit, PHEP2*energy_unit, PHEP3*energy_unit );
            } else {
                particle = new G4PrimaryParticle( g4code, PHEP1*energy_unit, PHEP2*energy_unit, PHEP3*energy_unit );
            }
        } else if (IDHEP==0 && fabs(PHEP1)+fabs(PHEP2)+fabs(PHEP3)<20e-9) {
            // special case: E less than ~10 eV and pdgcode=0 means optical photon
            particle = new G4PrimaryParticle( G4OpticalPhoton::OpticalPhoton(), PHEP1*energy_unit, PHEP2*energy_unit, PHEP3*energy_unit );
        } else {
            particle = new G4PrimaryParticle( IDHEP, PHEP1*energy_unit, PHEP2*energy_unit, PHEP3*energy_unit );
        }
        if (PHEP5 != req_novalue) { particle->SetMass( PHEP5*energy_unit ); }
        else { particle->SetMass(particle->GetG4code()->GetPDGMass()); }
        // The previous line of code is necessary because Geant4 is silly,
        // and sets "mass" = -1 if we don't set it.  It doesn't mess up
        // the simulation if we fail to do this, because (quoting a
        // comment in G4PrimaryParticle.hh): ``"mass" is just for book
        // keeping. This will not be used but the mass in
        // G4ParticleDefinition will be used.''  But it does mess up any
        // calculation of kinetic energy that relies on this mass, so we
        // might as well set it right.
        
        if (polx != 0.0 || poly != 0.0 || polz != 0.0)
            particle->SetPolarization(polx, poly, polz);
        
        // create G4HEPEvtParticle object
        G4HEPEvtParticle* hepParticle = new G4HEPEvtParticle( particle, ISTHEP, JDAHEP1, JDAHEP2 );
        
        // Store
        HPlist.push_back( hepParticle );
        
        // find or make the right vertex for this time and position
        // as a special case, if a line doesn't have a value for position or time,
        // then use the position and time previously set
        if (req_vertexX==req_novalue && req_vertexY==req_novalue
            && req_vertexZ==req_novalue && req_vertex_dT==req_novalue) {
            // no change, use current vertex
            vertexList.push_back( vertexSet[vertexSet.size()-1] );
        } else {
            // update vertex positions, and see if we can reuse an existing vertex
            if (req_vertexX != req_novalue) { vertexX = req_vertexX * position_unit + Offset.x(); }
            if (req_vertexY != req_novalue) { vertexY = req_vertexY * position_unit + Offset.y(); }
            if (req_vertexZ != req_novalue) { vertexZ = req_vertexZ * position_unit + Offset.z(); }
            if (req_vertex_dT != req_novalue) { vertexT = vertexT + req_vertex_dT * time_unit; }// relative to prev line!
            
            G4ThreeVector position(vertexX, vertexY, vertexZ);
            if (VerboseLevel) {
                cout << "Updated position: "<<position<<endl;
            }
            G4int iv;
            for (iv=vertexSet.size()-1; iv>=0; iv--) {
                if ( vertexSet[iv]->GetT0() == vertexT
                    and vertexSet[iv]->GetPosition() == position ) { break; } // found it
            }
            if (iv >= 0) {// found?
                vertexList.push_back(vertexSet[iv]);
            } else { // not found.
                G4PrimaryVertex* nv= new G4PrimaryVertex(position, vertexT);
                vertexSet.push_back(nv);
                vertexList.push_back(nv);
            }
        }
    }
    
    // check if there is at least one particle
    if( HPlist.empty() ) { return; }
    
    // make connection between daughter particles decayed from
    // the same mother
    for( size_t i=0; i<HPlist.size(); i++ ) {
        if( HPlist[i]->GetJDAHEP1() > 0 ) { //  it has daughters
            G4int jda1 = HPlist[i]->GetJDAHEP1()-1; // FORTRAN index starts from 1
            G4int jda2 = HPlist[i]->GetJDAHEP2()-1; // but C++ starts from 0.
            G4PrimaryParticle* mother = HPlist[i]->GetTheParticle();
            for( G4int j=jda1; j<=jda2; j++ ) {
                G4PrimaryParticle* daughter = HPlist[j]->GetTheParticle();
                if(HPlist[j]->GetISTHEP()>0) {
                    if ( vertexList[i]==vertexList[j] ) {
                        mother->SetDaughter( daughter );
                        HPlist[j]->Done();
                    } else {
                        cerr << "Error in  GLG4VertexGen_HEPEvt[" << FileName << "]: mother "<<i<<" and daughter "<<j
                        <<" are at different vertices, and this cannot be done in Geant4! Must divorce mother and daughter.\n";
                    }
                }
            }
        }
    }
    
    // put initial particles to the vertex (or vertices)
    for( size_t ii=0; ii<HPlist.size(); ii++ ) {
        if( HPlist[ii]->GetISTHEP() > 0 ) { // ISTHEP of daughters had been set to negative
            G4PrimaryParticle* initialParticle = HPlist[ii]->GetTheParticle();
            vertexList[ii]->SetPrimary( initialParticle );
        }
    }
    
    // clear G4HEPEvtParticles
    for(size_t iii=0;iii<HPlist.size();iii++) { delete HPlist[iii]; }
    HPlist.clear();
    
    // Put the vertex (or vertices) to G4Event object
    for (size_t iv=0; iv<vertexSet.size(); iv++) {
        if (vertexSet[iv]->GetNumberOfParticle() > 0) { // avoid empty vertices
            argEvent->AddPrimaryVertex( vertexSet[iv] );
        }
    }
    if (VerboseLevel) {
        cout <<"Events was given the following vertices:\n";
        argEvent->GetPrimaryVertex()->Print();
    }
}

/*Set source of HEPEVT ascii data for this generator, or print
 current source and help.
 Format of string: either "filename" or "shell_command (arguments) |".
 */
void GLG4VertexGen_HEPEvt::SetState(G4String newValues)
{
    G4String str = GLG4VPosGen::Strip(newValues);
    if (str.empty()) {
        // print help and current state
        cout << " Current state of this GLG4VertexGen_HEPEvt:\n \"" << GetState() << "\"\n"
        << " Format of argument to GLG4VertexGen_HEPEvt::SetState: \n"
        << "  either \"filename [(x_offset,y_offset,z_offset)]\" or \"shell_command (arguments) |\"\n";
        return;
    }
    
    if ( Regex("\\s").Match(str) ) {
        Regex reg("\\s+");
        if (reg.Split(str).size() == 2) {
            Offset = GLG4VPosGen::GetPointFromString(reg.at(1));
            cout << "GLG4VertexGen_HEPEvt::SetState: events will be shot with an offset: "<<Offset<<endl;
            str = reg.at(0);
        } else {
            G4Exception("GLG4VertexGen_HEPEvt::SetState","0",FatalErrorInArgument,
                        scat("Incorrect arguments, can not recognize file name + offset point: ", newValues).c_str() );
        }
    }
    cout << "GLG4VertexGen_HEPEvt::SetState: events will be shot from file: "<<str<<endl;
    Open(str);
}

G4String GLG4VertexGen_HEPEvt::GetState()
{
    return G4String(scat(FileName,"  ",Offset));
}

void GLG4VertexGen_HEPEvt::SetDirectionTarget(G4PrimaryVertex* vertex){(void) vertex; return;}

void GLG4VertexGen_HEPEvt::ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir){(void) vertex; (void) Dir; return;}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

void GLG4VertexGen_Stack::GeneratePrimaryVertex(G4Event *argEvent)
{
    // pop and add all vertices within time window
    // G4double eventEndTime= fGLG4PGA->GetUniversalTime() + fGLG4PGA->GetEventWindow();
    
    while ( ! theStack.empty() ) {
        auto b = theStack.begin();
        G4PrimaryVertex* v = *b;
        
        /*    if (v->GetT0() > eventEndTime)
         break;
         v->SetT0( v->GetT0() - fGLG4PGA->GetUniversalTime() );*/
        // bug fix, by JG, March 2012
        /*
         * EventWindow=1000*ns by default, and some neutron can diffuse longer than 1000*ns
         * If the simulated source is mono-neutron, such as the AmBe source,
         * the stack has only one element, and if this neutron diffuse longer than EventWindow,
         * it is possible to clean the stack without adding any vertex to argEvent,
         * leading to no vertex generation. Then in DCEventAction::FillData, a seg fault
         * occur whith evt->GetPrimaryVertex()->GetPrimary()->GetMomentum().
         * So now the "escaping" is just put at the end of EventWindow.
         */
        if ( (v->GetT0()-fGLG4PGA->GetUniversalTime())  > fGLG4PGA->GetEventWindow()) {
            v->SetT0( fGLG4PGA->GetEventWindow()-0.5 );
        } else { v->SetT0( v->GetT0() - fGLG4PGA->GetUniversalTime() ); }
        
        argEvent->AddPrimaryVertex( v );
        theStack.erase(b);
    }
    
    if ( ! theStack.empty() ) {
        G4double nextTime = (*(theStack.begin()))->GetT0();
        fGLG4PGA->NotifyTimeToNextStackedEvent( nextTime - fGLG4PGA->GetUniversalTime() );
    }
}

void GLG4VertexGen_Stack::StackIt(const G4Track* track)
{
    G4PrimaryVertex* vertex = new G4PrimaryVertex(
                                                  track->GetPosition(), track->GetGlobalTime() + fGLG4PGA->GetUniversalTime() );
    
    G4ThreeVector mom(track->GetMomentum());
    G4ThreeVector pol(track->GetPolarization());
    G4PrimaryParticle* particle = new G4PrimaryParticle(track->GetDefinition(), mom.x(), mom.y(), mom.z() );
    particle->SetPolarization(pol.x(), pol.y(), pol.z()); // polarization
    particle->SetMass(track->GetDefinition()->GetPDGMass()); // Is Geant4 silly to make us do this?
    vertex->SetPrimary( particle );
    
    StackedPrimaryVertexInfo * spvi= new StackedPrimaryVertexInfo();
    spvi->_parentEvent = G4EventManager::GetEventManager()->GetConstCurrentEvent()->GetEventID();
    spvi->_parentUT= fGLG4PGA->GetUniversalTime();
    vertex->SetUserInformation(spvi);  // G4PrimaryVertex destructor will delete spvi
    
    theStack.insert(vertex);
}

void GLG4VertexGen_Stack::StackedPrimaryVertexInfo::Print() const
{
    cout << "   GLG4...StackedPrimaryVertexInfo: _parentEvent="
    << _parentEvent << ", _parentUT=" << _parentUT << endl;
}

void GLG4VertexGen_Stack::SetState(G4String /*newValues*/)
{
    cout << "GLG4VertexGen_Stack has no user-settable state." << endl;
    cout << "Current status is as follows:" << endl;
    G4int i=0;
    for (auto it= theStack.begin();
         it!= theStack.end();
         ++it, i++) {
        G4PrimaryVertex* v= *it;
        cout << i << "\t" << v << endl;
    }
    cout << "Number of particles on stack: " << i << endl;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

void GLG4VertexGen_SourceMonoSpectrum::GeneratePrimaryVertex( G4Event *argEvent )
{
    G4PrimaryParticle* NewParticle = new G4PrimaryParticle(theParticle); // no ctor of G4PrimaryParticle accept momentum as G4ThreeVector of G4ParticleMomentum...
    NewParticle->SetMomentumDirection(RandG4_t::Direction()); // do not never ever use 'Set4Momentum', you would redefine the mass (!!!!!)
    NewParticle->SetKineticEnergy(theDesintegrationSpectrum.ShootEnergy());
    //   NewParticle->SetPolarization(RandG4_t::Direction()); // no need to specify polarization
    G4PrimaryVertex* theVertex = new G4PrimaryVertex();
    theVertex->SetPrimary( NewParticle );
    argEvent->AddPrimaryVertex(theVertex);
}

////////////////////////////////////////////////////////////////

GLG4VertexGen_SourceMonoBeta::GLG4VertexGen_SourceMonoBeta(const G4String& spectrum_name, const G4String& name):
GLG4VertexGen_SourceMonoSpectrum(name,G4Electron::Definition()), SpectrumName(spectrum_name)
{
    // the table is a beta spectrum: just read and fill
    if (not spectrum_name.empty()) {
        const ParamTable& DataTable (ParamBase::GetDataBase().GetTable(SpectrumName));
        theDesintegrationSpectrum.FillAndLock(DataTable["Energy"],DataTable["Spectrum"]);
    }
}

void GLG4VertexGen_SourceMonoBeta::SetState(G4String newValues)
{
    SpectrumName = newValues;
    const ParamTable& DataTable (ParamBase::GetDataBase().GetTable(SpectrumName));
    theDesintegrationSpectrum.Reset();
    theDesintegrationSpectrum.FillAndLock(DataTable["Energy"],DataTable["Spectrum"]);
}

////////////////////////////////////////////////////////////////

GLG4VertexGen_SourceGamma::GLG4VertexGen_SourceGamma(const G4String& table_name, Type type, const G4String& name):
GLG4VertexGen_SourceMonoSpectrum(name,G4Gamma::Definition()), TableName(table_name)
{
    // the table is a gamma and/or X spectrum
    if (not table_name.empty()) {
        switch (type) {
            case GX: Fill(TableName,"gamma"); Fill(TableName,"XR"); theType="gamma+X"; break;
            case X: Fill(TableName,"XR");  theType="X"; break;
            case Gamma: Fill(TableName,"gamma");  theType="gamma"; break;
            default: G4Exception("GLG4VertexGen_SourceGamma::GLG4VertexGen_SourceGamma","0",FatalException,"Incorrect type");
        }
        theDesintegrationSpectrum.AccumulateIntensity();
    }
}

void GLG4VertexGen_SourceGamma::SetState(G4String newValues)
{
    Regex reg("^(X|G|GX)\\s+(\\w+)$");
    if (reg.Match(newValues)!=3) {
        G4Exception("GLG4VertexGen_SourceGamma::SetState","1",FatalErrorInArgument,
                    scat("Can not recognize type and table_name ('(X|G|GX) tablename') from: ",newValues).c_str() );
    }
    
    TableName = reg[2];
    theDesintegrationSpectrum.Reset();
    
    // the table is a gamma and/or X spectrum
    switch (HashRunTime(reg[1].data())) {
        case "GX"_hash: Fill(TableName,"gamma"); Fill(TableName,"XR"); theType="gamma+X"; break;
        case "X"_hash: Fill(TableName,"XR");  theType="X"; break;
        case "G"_hash: Fill(TableName,"gamma");  theType="gamma"; break;
        default: G4Exception("GLG4VertexGen_SourceGamma::SetState","2",FatalException,"Incorrect type");
    }
    theDesintegrationSpectrum.AccumulateIntensity();
    
}

void GLG4VertexGen_SourceGamma::Fill(const G4String& table_name, const G4String& type)
{
    const ParamTable& DataTable (ParamBase::GetDataBase().GetTable(table_name));
    for (G4int i=0; i<DataTable.GetNbRows(); i++) {
        if ( DataTable["decay"][i].compare(0,type.length(),type.data()) ) {
            theDesintegrationSpectrum.Push(new GLG4Branch(stod(DataTable["decay"][i]),stod(DataTable["decay"][i])*0.01));
        }
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

GLG4VertexGen_SourceMultipleBetas::GLG4VertexGen_SourceMultipleBetas(const G4String& isotope, const G4String& name):
GLG4VVertexGen(name), theIsotope(isotope), theParticle(G4Electron::Definition())
{
    if (not isotope.empty()) { LoadIsotope(); }
}

void GLG4VertexGen_SourceMultipleBetas::LoadIsotope()
{
    G4String table_name = theIsotope + "_intensity";
    const ParamBase& db(ParamBase::GetDataBase());
    const ParamTable& IntensityTable (ParamBase::GetDataBase().GetTable(table_name));
    
    GLG4Desintegration* theNewDesintegration=nullptr;
    for (G4int i=0; i<IntensityTable.GetNbRows(); i++) {
        if ( IntensityTable["decay"][i] == "beta" ) {
            table_name = theIsotope + "_" + IntensityTable["energy"][i] + "keV";
            
            if (not db.HasTable(table_name)) {
                G4ExceptionDescription ED;
                ED << "Can not find beta branch at energy: "<<IntensityTable["energy"][i]<<". Can not find table: "<<table_name;
                G4Exception("GLG4VertexGen_SourceMultipleBetas::GLG4VertexGen_SourceMultipleBetas","1",FatalErrorInArgument,ED);
            }
            
            theNewDesintegration = new GLG4Desintegration(BetaMinus,
                                                          stod(IntensityTable["energy"][i]),
                                                          stod(IntensityTable["intensity"][i])*0.01,
                                                          stoi(IntensityTable["start_lvl"][i]),
                                                          stoi(IntensityTable["end_lvl"][i]),
                                                          EneGen::NONE); // no atomic excitation
            
            const ParamTable& BranchTable (db.GetTable(table_name));
            theNewDesintegration->FillAndLock(BranchTable["Energy"],BranchTable["Spectrum"]);
            theBranches.Push(theNewDesintegration); // theBranches becomes owner of the desintegration
        }
    }
}

void GLG4VertexGen_SourceMultipleBetas::GeneratePrimaryVertex( G4Event *argEvent )
{
    G4PrimaryParticle* NewParticle = new G4PrimaryParticle(theParticle);
    NewParticle->SetMomentumDirection(RandG4_t::Direction());
    NewParticle->SetKineticEnergy(static_cast<const GLG4Desintegration*>(theBranches.ShootDecay())->ShootEnergy());
    //   NewParticle->SetPolarization(RandG4_t::Direction()); // no need to specify polarization
    G4PrimaryVertex* theVertex = new G4PrimaryVertex();
    theVertex->SetPrimary( NewParticle );
    argEvent->AddPrimaryVertex(theVertex);
}

void GLG4VertexGen_SourceMultipleBetas::SetState(G4String newValues)
{
    theIsotope = newValues;
    theBranches.Reset();
    LoadIsotope();
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

GLG4VertexGen_SourceAllDecays::GLG4VertexGen_SourceAllDecays(const G4String& isotope, const G4String& name):
GLG4VVertexGen(name), theVertex(nullptr), theIsotope(isotope)
{
    if (not isotope.empty()) { LoadIsotope(); }
}

void GLG4VertexGen_SourceAllDecays::LoadIsotope()
{
    const ParamBase& db(ParamBase::GetDataBase());
    RemainingEnergy = db[theIsotope+".Qvalue"];
    MinimalEnergy = RemainingEnergy;
    
    G4String table_name = theIsotope + "_intensity";
    const ParamTable& IntensityTable (db.GetTable(table_name));
    const vector<string>& vDecay( IntensityTable["decay"] );
    
    vector<G4double> vE, vI;
    vector<G4int> vStartLvl, vEndLvl;
    for (size_t i=0; i<IntensityTable["decay"].size(); i++) {
        vE.push_back(stod(IntensityTable["energy"][i]));
        vI.push_back(stod(IntensityTable["intensity"][i])*0.01);
        vStartLvl.push_back(stoi(IntensityTable["start_lvl"][i]));
        vEndLvl.push_back(stoi(IntensityTable["end_lvl"][i]));
    }
    
    GLG4Decay* theNewDecay=nullptr;
    DecayType theType;
    AtomicLevel lvl; // to avoid a very (very) stupid warning
    for (G4int i=0; i<IntensityTable.GetNbRows(); i++) {
        theType = FindType(vDecay[i].data());
        switch (theType) {
            case BetaMinus:
                theNewDecay = new GLG4Desintegration(theType,vE[i],vI[i],vStartLvl[i],vEndLvl[i],EneGen::NONE); // no atomic excitation
                
                table_name = G4String(theIsotope.c_str()) + "_" + IntensityTable["energy"][i] + "keV";
                if (not db.HasTable(table_name)) {
                    G4ExceptionDescription ED;
                    ED << "Can not find beta branch at energy: "<<vE[i]<<". Can not find table: "<<table_name;
                    G4Exception("GLG4VertexGen_SourceAllDecays::GLG4VertexGen_SourceAllDecays","2",FatalErrorInArgument,ED);
                }
                
                dynamic_cast<GLG4Desintegration*>(theNewDecay)->FillAndLock(db.GetTable(table_name)["Energy"],db.GetTable(table_name)["Spectrum"]);
                break;
            case CE:
                theNewDecay = new GLG4Deexcitation(theType, vE[i], vI[i], vStartLvl[i], vEndLvl[i],
                                                   FindMainLevel(vDecay[i].data()));
                break;
            case Gamma:
                theNewDecay = new GLG4Deexcitation(theType, vE[i], vI[i], vStartLvl[i], vEndLvl[i], EneGen::NONE);
                break;
            case X:
                theNewDecay = new GLG4Deexcitation(theType, vE[i], vI[i],
                                                   FindSecondaryLevel(vDecay[i].data()),
                                                   FindMainLevel(vDecay[i].data()), EneGen::NONE);
                break;
            case Auger:
                lvl = FindMainLevel(vDecay[i].data());
                theNewDecay = new GLG4Deexcitation(theType, vE[i], vI[i], G4int(lvl)+1, G4int(lvl), AtomicLevel(lvl+1));
                break;
            case Alpha:    // to avoid an other very (very) stupid warning
            case BetaPlus: // same
            default: G4ExceptionDescription ED;
                ED << "Can not recognize decay type: "<<theType<<"\n  Forgotten to be implemented ? Should throw exception in FindType.";
                G4Exception("GLG4VertexGen_SourceAllDecays::GLG4VertexGen_SourceAllDecays","1",FatalErrorInArgument,ED);
        }
        theSpectra[theType].Push(theNewDecay);
        if ( theNewDecay->GetEnergy() < MinimalEnergy ) { MinimalEnergy = theNewDecay->GetEnergy(); }
    }
    
    for (auto it=theSpectra.begin(); it!=theSpectra.end(); ++it) {
        // list nuclear levels and associate the deexcitations
        if (it->first == CE or it->first == Gamma) {
            for (size_t i=0; i<it->second.GetSize(); i++) {
                GLG4Deexcitation* theDeexcitation = new GLG4Deexcitation(*dynamic_cast<GLG4Deexcitation*>(it->second(i)));
                theNuclearLevels[theDeexcitation->GetStartLevel()].Push(theDeexcitation);
            }
        }
        
        // list atomic levels and associate the deexcitations
        if (it->first == X or it->first == Auger) {
            for (size_t i=0; i<it->second.GetSize(); i++) {
                GLG4Deexcitation* theDeexcitation = new GLG4Deexcitation(*dynamic_cast<GLG4Deexcitation*>(it->second(i)));
                theAtomicLevels[theDeexcitation->GetEndLevel()].Push(theDeexcitation);
            }
        }
    }
    
    for (auto it=theSpectra.begin(); it!=theSpectra.end(); ++it) {
        it->second.AccumulateIntensity();
#ifdef DEBUG
        G4cout << "Decay type: "<<it->first<<"\n"; it->second.Dump(); G4cout << "\n";
#endif
    }
    
    for (auto it=theNuclearLevels.begin(); it!=theNuclearLevels.end(); ++it) {
        it->second.AccumulateIntensity();
#ifdef DEBUG
        G4cout << "Nuclear level: "<<it->first<<"\n"; it->second.Dump(); G4cout << "\n";
#endif
    }
    
    for (auto it=theAtomicLevels.begin(); it!=theAtomicLevels.end(); ++it) {
        it->second.AccumulateIntensity();
#ifdef DEBUG
        G4cout << "Atomic level: "<<it->first<<"\n"; it->second.Dump(); G4cout << "\n";
#endif
    }
}

void GLG4VertexGen_SourceAllDecays::GeneratePrimaryVertex( G4Event *argEvent )
{
    theVertex = new G4PrimaryVertex();
    const GLG4Desintegration* theBetaBranch = static_cast<const GLG4Desintegration*>(theSpectra[BetaMinus].ShootDecay());
#ifdef DEBUG
    G4cout << "the initial Beta branch:\n";
    theBetaBranch->Dump();
#endif
    NewParticle(theBetaBranch);
    NuclearDeexcitation(theBetaBranch->GetEndLevel());
    
    if (RemainingEnergy > 0) { NewParticle(nullptr); } // null argument means "flush energy in an e-"
    
    argEvent->AddPrimaryVertex(theVertex);
}

void GLG4VertexGen_SourceAllDecays::NuclearDeexcitation(G4int level)
{
    if ( level == 0 ) { return; }
    const GLG4Deexcitation* aDeexcitation = static_cast<const GLG4Deexcitation*>(theNuclearLevels[level].ShootDecay());
#ifdef DEBUG
    G4cout << "a nuclear deexcitation:\n";
    aDeexcitation->Dump();
#endif
    NewParticle(aDeexcitation);
    NuclearDeexcitation(aDeexcitation->GetEndLevel());
    if (aDeexcitation->GetDecayType() == CE) { AtomicDeexcitation(aDeexcitation->GetAtomicExcitationLevel()); }
}

void GLG4VertexGen_SourceAllDecays::AtomicDeexcitation(G4int level)
{
    if (RemainingEnergy < 0) { // should not be possible, but let's check it
        G4Exception("GLG4VertexGen_SourceAllDecays::AtomicDeexcitation","0",EventMustBeAborted,"Negative remaining energy");
    }
    
    // in case several atomic deexcitation are stacked
    if (RemainingEnergy == 0) { return; }
    
    // inside or very close from continuum (level not resolved) => all remaining energy is given to an e- to conserve energy
    if (RemainingEnergy < MinimalEnergy or level == continuum) {
        NewParticle(0); // null argument means "flush energy in an e-"
        return;
    }
    
    // first check if there is a known transition from this level
    if (theAtomicLevels.count(level) == 0) { return; } // nope, probably too close from continuum => energy will be flushed with RemainingEnergy
    
    const GLG4Deexcitation* aDeexcitation = static_cast<const GLG4Deexcitation*>(theAtomicLevels[level].ShootDecay());
    NewParticle(aDeexcitation);
    
    AtomicDeexcitation(aDeexcitation->GetStartLevel());
    if (aDeexcitation->GetDecayType() == Auger) { AtomicDeexcitation(aDeexcitation->GetAtomicExcitationLevel()); }
}

void GLG4VertexGen_SourceAllDecays::NewParticle(const GLG4Decay* aDecay)
{
    G4PrimaryParticle* theParticle=nullptr;
    if (aDecay == nullptr) { // null argument means "flush energy in an e-"
        theParticle = new G4PrimaryParticle(G4Electron::Definition());
        theParticle->SetKineticEnergy(RemainingEnergy*keV);
        RemainingEnergy = 0.;
    } else {
        theParticle = new G4PrimaryParticle(aDecay->GetParticle());
        theParticle->SetKineticEnergy(aDecay->ShootEnergy()*keV);
        RemainingEnergy -= aDecay->GetEnergy();
    }
    
#ifdef DEBUG
    G4cout << "New particle: "<<theParticle->GetParticleDefinition()->GetParticleName()<<"\tE="<<theParticle->GetKineticEnergy()/keV << "\n";
#endif
    
    theParticle->SetMomentumDirection(RandG4_t::Direction());
    //  theParticle->SetPolarization(RandG4_t::Direction()); // no need to specify polarization
    theVertex->SetPrimary(theParticle );
}

DecayType GLG4VertexGen_SourceAllDecays::FindType(const G4String& type_name) const
{
    if (type_name == "beta" )              { return BetaMinus; }
    else if (type_name == "gamma")         { return Gamma; }
    else if (type_name.contains("CE") )    { return CE; }
    else if (type_name.contains("XR") )    { return X; }
    else if (type_name.contains("Auger") ) { return Auger; }
    else {
        G4ExceptionDescription ED;
        ED << "Can not recognize type: "<<type_name;
        G4Exception("GLG4VertexGen_SourceAllDecays::FindType","0",FatalException,ED);
    }
    return Error; // to avoid a stupid warning too
}

AtomicLevel GLG4VertexGen_SourceAllDecays::FindMainLevel(const G4String& type_name) const
{
    if ( not type_name.contains("Auger")
        and not type_name.contains("CE")
        and not type_name.contains("X")
        ) {
        G4ExceptionDescription ED;
        ED << "Can not recognize the deexcitation type: "<<type_name;
        G4Exception("GLG4VertexGen_SourceAllDecays::FindMainLevel","1",JustWarning,ED);
        return EneGen::NONE;
    }
    
    if ( type_name.contains("K") ) { return EneGen::K; }
    if ( type_name.contains("L") ) { return EneGen::L; }
    if ( type_name.contains("M") ) { return EneGen::M; }
    
    G4ExceptionDescription ED;
    ED << "Can not recognize the main atomic level in deexcitation: "<<type_name;
    G4Exception("GLG4VertexGen_SourceAllDecays::FindMainLevel","2",FatalErrorInArgument,ED);
    return EneGen::NONE; // to avoid a stupid warning too
}

AtomicLevel GLG4VertexGen_SourceAllDecays::FindSecondaryLevel(const G4String& type_name) const
{
    if (not type_name.contains("X") ) { return EneGen::NONE; }
    
    AtomicLevel theLevel = FindMainLevel(type_name);
    
    G4int shift=0;
    if ( type_name.contains("a") ) { shift=1; }
    else if ( type_name.contains("b") ) { shift=2; }
    else if ( type_name.contains("c") ) { shift=3; }
    else {
        G4ExceptionDescription ED;
        ED << "Can not recognize the secondary atomic level in X deexcitation : "<<type_name
        <<"\nAssuming a secondary level just above the main level";
        G4Exception("GLG4VertexGen_SourceAllDecays::FindSecondaryLevel","1",JustWarning,ED);
        shift = 1;
    }
    
    if ( shift + theLevel >= continuum ) { return continuum; }
    return AtomicLevel(shift + theLevel);
}

void GLG4VertexGen_SourceAllDecays::SetState(G4String newValues)
{
    theSpectra.clear();
    theNuclearLevels.clear();
    theAtomicLevels.clear();
    theIsotope = newValues;
    LoadIsotope();
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

GLG4Decay::GLG4Decay(DecayType type, G4double E, G4double I, G4int start_lvl, G4int end_lvl, AtomicLevel ato_lvl):
GLG4Branch(E,I), theType(type), start_level(start_lvl), end_level(end_lvl), atomic_excitation_level(ato_lvl)
{
    switch(type) {
        case Alpha: theParticle = G4Alpha::Definition(); break; // alpha is considered as an ion
        case BetaPlus: theParticle = G4Positron::Definition(); break;
        case BetaMinus:
        case CE:
        case Auger: theParticle = G4Electron::Definition(); break;
        case Gamma:
        case X: theParticle = G4Gamma::Definition(); break;
        default:
            G4ExceptionDescription ED;
            ED << "Unknown particle type: "<< type << "\nNot yet implemented ?";
            G4Exception("GLG4Decay::GLG4Decay","0",FatalException,ED);
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

void GLG4EnergySpectrum::Push(GLG4EneBin* EneBin)
{
    if (isAccumulated) {
        G4Exception("GLG4EnergySpectrum::Push","0",FatalException,"Spectrum already accumulated");
    }
    
    theSpectrum.push_back(EneBin);
}

void GLG4EnergySpectrum::FillAndLock(const vector< G4double >& vE, const vector< G4double >& vI)
{
    if (isAccumulated) {
        G4Exception("GLG4EnergySpectrum::Fill","1",JustWarning,"Spectrum already accumulated: reset spectrum before filling");
        Reset();
    }
    
    if (vE.size() != vI.size()) {
        G4Exception("GLG4EnergySpectrum::Fill","2",FatalErrorInArgument,
                    "Can not fill the energy spectrum: energy and intensity table have different sizes");
    }
    
    for (size_t i=0; i<vE.size(); i++) {
        theSpectrum.push_back(new GLG4EneBin(vE.at(i),vI.at(i)));
    }
    
    AccumulateIntensity();
}

void GLG4EnergySpectrum::FillAndLock(const vector< string >& vE, const vector< string >& vI)
{
    vector< G4double > NewE, NewI;
    for (size_t i=0; i!=vE.size(); i++) { NewE.push_back(stod(vE.at(i))); }
    for (size_t i=0; i!=vI.size(); i++) { NewI.push_back(stod(vI.at(i))); }
    FillAndLock(NewE,NewI);
}

void GLG4EnergySpectrum::AccumulateIntensity()
{
    if (isAccumulated == true) {
        G4Exception("GLG4EnergySpectrum::AccumulateIntensity","0",FatalException,"Spectrum already accumulated");
        return;
    }
    
#if DEBUG>1
    cout << "\n\n AccumulateIntensity:\n";
    cout << " BEFORE\n";
    for (size_t i=0; i<theSpectrum.size(); i++) { theSpectrum.at(i)->Dump(); }
#endif
    
    sort(theSpectrum.begin(),theSpectrum.end(),GLG4EneBin::SortByIntensity);
#if DEBUG>1
    cout << " SORT\n";
    for (size_t i=0; i<theSpectrum.size(); i++) { theSpectrum.at(i)->Dump(); }
#endif
    
    reverse(theSpectrum.begin(),theSpectrum.end());
#if DEBUG>1
    cout << " REVERSE\n";
    for (size_t i=0; i<theSpectrum.size(); i++) { theSpectrum.at(i)->Dump(); }
#endif
    
    for (size_t i=1; i<theSpectrum.size(); i++) {
        theSpectrum.at(i)->AddToIntensity(theSpectrum.at(i-1)->GetIntensity());
    }
#if DEBUG>1
    cout << " ADD\n";
    for (size_t i=0; i<theSpectrum.size(); i++) { theSpectrum.at(i)->Dump(); }
#endif
    
    if (theSpectrum.back()->GetIntensity()!=1.) {
        for (size_t i=0; i<theSpectrum.size(); i++) {
            theSpectrum.at(i)->RenormalizeIntensity(theSpectrum.back()->GetIntensity());
        }
    }
#if DEBUG>1
    cout << " RENORMALIZE\n";
    for (size_t i=0; i<theSpectrum.size(); i++) { theSpectrum.at(i)->Dump(); }
#endif
    
    isAccumulated = true;
}

void GLG4EnergySpectrum::Reset()
{
    //for (auto it=theSpectrum.begin(), end=theSpectrum.end(); it!=end; ++it) { delete *it; }
    for (auto it : theSpectrum) {delete it;}
    theSpectrum.clear();
    isAccumulated = false;
}

//************************************************************//
///////////////////// COSMOGENIC NEUTRONS GENERATOR //////////////////////
//************************************************************//

//initialization of static variables
G4ThreeVector GLG4VertexGen_CosmogenicNeutrons::dirMuon = G4ThreeVector(0, 0, 0);
G4double GLG4VertexGen_CosmogenicNeutrons::Emuon = 0.;

GLG4VertexGen_CosmogenicNeutrons::GLG4VertexGen_CosmogenicNeutrons(const G4String& name): GLG4VVertexGen(name)
{
    Particle= G4ParticleTable::GetParticleTable()->FindParticle("geantino");
}

/** Generates one or more particles with a specified momentum, or isotropic
 momentum with specific energy, and specified polarization, or uniformly
 random polarization, based on parameters set via SetState() */
void GLG4VertexGen_CosmogenicNeutrons::GeneratePrimaryVertex(G4Event *argEvent)
{
    for (G4int imult=0; imult<Multiplicity; imult++) {
        G4PrimaryVertex* vertex= new G4PrimaryVertex( 0.,0.,0.,0. );
        
        if (Particle == nullptr) {
            Particle= G4ParticleTable::GetParticleTable()->FindParticle("geantino");
        }
        G4double mass= Particle->GetPDGMass();
        
        G4PrimaryParticle* particle = nullptr;
        
        //We set the energy according to the energy distribution for neutrons from cosmogenic muons
        this->SetEnergy();
        
        if (AniType=="fixed") { //check if direction given
            // fixed direction
            G4double p_renorm;
            
            if (Ke <= 0.0) {
                p_renorm = 0.0;
                Ke= 0.0;
            }
            else {
                p_renorm = sqrt( Ke*(Ke+2.0*mass) / P.mag2() ); }
            P *= p_renorm;
            particle = new G4PrimaryParticle(Particle, P.x(), P.y(), P.z() );
            dir= P.unit();
        }
        else {
            // isotropic direction
            /* generate random ThreeVector of unit length isotropically
             distributed on sphere by cutting two circles from a rectangular
             area and mapping circles onto top and bottom of sphere.
             N.B. the square of the radius is a uniform random variable.
             -GAHS.
             */
            if(AniType=="iso" || AniType.length() == 0){
                while (1) {
                    G4double u,v,q2;
                    // try first circle
                    u= 3.5*G4UniformRand()-1.75;
                    v= 2.0*G4UniformRand()-1.0;
                    q2= u*u + v*v;
                    if (q2 < 1.0) {
                        G4double rho_over_q= sqrt(2.0-q2);
                        dir= G4ThreeVector( u*rho_over_q, v*rho_over_q, 1.0-q2 );
                        break;
                    }
                    // try second circle
                    u= (u>0.0) ? u-1.75 : u+1.75;
                    v= (v>0.0) ? v-1.00 : v+1.00;
                    q2= u*u + v*v;
                    if (q2 < 1.0) {
                        G4double rho_over_q= sqrt(2.0-q2);
                        dir= G4ThreeVector( u*rho_over_q, v*rho_over_q, q2-1.0 );
                        break;
                    }
                }
            }
            else if ( /* Anisotropy */ AniType=="zenith") {
                
                //If it is too big set it to the maximum value
                if (abs(Ani)>1.) { Ani/=abs(Ani); }
                
                G4double phi,cost,st;
                //	cost=2.0*G4UniformRand()-1.0;
                //   For 1+a*cos(theta)  -1<a<1
                cost=(sqrt(1.+Ani*(4.*G4UniformRand()+Ani-2.))-1.)/Ani;
                phi=CLHEP::twopi*G4UniformRand();
                st=sqrt(1.-cost*cost);
                dir=G4ThreeVector(st*cos(phi),st*sin(phi),cost);
            }
            else if (AniType=="cosmo") {
                /*Anisotropy according to the angle spectrum for cosmo muons induced neutrons*/
                this->SetDirectionCosmo();
            }
            else if (AniType=="target") {
                /*Anisotropy according to the target volume. This is handled by ::SetDirectionTarget, called in
                 * GLG4PrimaryGeneratorAction : because the simulation creates first the vertex (with impulsion
                 * and its direction), before creating its position. No other choice to create a random direction
                 * that will be changed after by this method*/
                dir=G4ThreeVector(0.,0.,1.); //dir has to be nonzero
                
            }
            else {
                cerr << "Invalid AniType is set" << endl;
                break;
            }
            //      G4ThreeVector P = ( dir * sqrt(ke*(ke+2.*mass)) );
            dir=dir.unit(); //just in case
            P = ( dir * sqrt(Ke*(Ke+2.*mass)) );
            particle = new G4PrimaryParticle(Particle, P.x(), P.y(), P.z() );
        }
        
        // adjust polarization if needed
        if (Particle->GetPDGSpin() == 1.0 && mass == 0.0) {
            // spin 1 mass 0 particles should have transverse polarization
            G4ThreeVector rpol= Polar - dir * (dir*Polar);
            G4double rpolmag= rpol.mag();
            if (rpolmag > 0.0) {
                // use projected orthogonal pol, normalized
                rpol *= (1.0/rpolmag);
            } else {
                // choose random pol
                G4double phi= (G4UniformRand()*2.0-1.0)*M_PI;
                G4ThreeVector e1= dir.orthogonal().unit();
                G4ThreeVector e2= dir.cross(e1);
                rpol= e1*cos(phi)+e2*sin(phi);
            }
            particle->SetPolarization(rpol.x(), rpol.y(), rpol.z());
        } else {
            // use user-set polarization unmodified
            particle->SetPolarization(Polar.x(), Polar.y(), Polar.z());
        }
        
        particle->SetMass(mass); // Geant4 is silly.
        vertex->SetPrimary( particle );
        
        if(TJit!=0.||TCont!=0) {
            G4double d_t_dist = TCont * G4UniformRand() + TJit * G4RandGauss::shoot();
            vertex->SetT0( d_t_dist);
        }
        argEvent->AddPrimaryVertex(vertex);
    }
}

/** Set state of the GLG4VertexGen_CosmogenicNeutrons, or show current state and
 explanation of state syntax if empty string provided.
 
 Format of argument to GLG4VertexGen_CosmogenicNeutrons::SetState:
 "unit_momx unit_momy unit_momz Emuon polx poly polz mult ani tjit sjit",
 where
 - mom is a unit momemtum (for direction)
 - pol* is an optional polarization vector.
 - mult is an optional multiplicity of the particles.
 - Emuon mean energy of the muons
 If mom*==0, then random isotropic directions chosen.
 If pol*==0, then random transverse polarization for photons.
 - ani is an optional anisotropy -1<ani<1.
 - tjit is an optional time jitter in nsec.
 - sjit is an optional spectral jitter in MeV.
 
 */
void GLG4VertexGen_CosmogenicNeutrons::SetState(G4String newValues)
{
    //#ifdef DEBUG
    cout << "GLG4VertexGen_CosmogenicNeutrons::SetState: " << newValues << "\n";
    //#endif
    G4String str = GLG4VPosGen::Strip(newValues);
    if (str.length() == 0) {
        // print help and current state
        cout << "Current state of this GLG4VertexGen_CosmogenicNeutrons:\n"
        << " \"" << GetState() << "\"\n" << endl;
        cout << "Format of argument to GLG4VertexGen_CosmogenicNeutrons::SetState: \n"
        " \" unit_momx unit_momy unit_momz Emuon 'Emin' EminValue polx poly polz mult\"\n"
        " where unit_mom is the momentum direction\n"
        " Emuon mean energy of the muons\n"
        " 'Emin' followed by its value in order to shoot energies with minimal value.\n"
        " pol* is an optional polarization vector.\n"
        " mult is an optional multiplicity of the particles.\n"
        " mom==0 --> random isotropic directions chosen\n"
        " pol*==0 --> random transverse polarization for photons.\n"
        " ani is an optional anisotropy -1<ani<1.\n"
        " tjit is an optional time jitter in nsec.\n"
        " sjit is an optional spectral jitter in MeV.\n\n"
        " you can add \'target Volumetarget\' at the beginning in order to make the vertex pointing at the center of a volume.\n\n";;
        return;
    }
    ParamBase &db ( ParamBase::GetInputDataBase() );
    istringstream is(str.c_str());
    
    // set particle
    P = G4ThreeVector(0.,0.,0.);
    string pname;
    pname = "neutron";
    cout << "Particle name: " << pname << "\n";
    
    G4ParticleDefinition * newTestCosmogenicNeutronsG4Code = G4ParticleTable::GetParticleTable()->FindParticle(pname);
    if (newTestCosmogenicNeutronsG4Code == nullptr) {
        // if not a particle name : problem with particleTable
        cout << "Could not find the particle Neutron in the ParticleTable"  << endl;
    }
    
    Particle= newTestCosmogenicNeutronsG4Code;
    db[ (Name+".pdgcode").c_str() ] = Particle->GetPDGEncoding();
    
    //Set Parameters for energy
    Emuon = epsilonMu*(1-exp(-b*h))/(gammaMu-2);     	//Muons mean energy in GeV
    Emuon = 40.0;
    //Muon energy is calculated according to formula including depth
    // around 21 Gev. However DoubleChooz publications measured around 30 GeV.
    
    //Worst case scenario : comment the following line
    Emuon = 40;
    
    Enmax = Emuon;
    deltaE = Enmax - Enmin;
    Eneutron = 0;
    
    // If one wants to make the particle aim at a specific volume
    // NB : the action of the method SetDirectionTarget cannot be done in GeneratePrimaryVertex as the others AniType
    // We first have to wait that the position of our particle is generated. This is done in PrimaryGeneratorAction,
    // where we access to the vertex created here, and change its direction in the case AniType == "target".
    // See PrimaryGeneratorAction in ::GeneratePrimaries()
    G4String isTarget;
    is >> isTarget;
    if (is.fail() || isTarget != "target"){
        return; }
    
    is >> VolumeTarget;
    if (is.fail()) {return;}
    if(!VolumeTarget.empty()){AniType = "target";}
    
    // optional set unit momentum (all the particles will have the same direction)
    G4double x,y,z;
    is >> x >> y >> z;
    if (is.fail()) { // if not momemtum given : direction would be generated in GeneratePrimaryVertex
        return; }
    
    P= G4ThreeVector(x*MeV,y*MeV,z*MeV); //momentum used here so that even if unit momemtum is not given, we still get direction
    dir = P.unit();
    if(P.mag2() > 0.0) {AniType = "fixed";}
    
    // optional overwrite of muon mean energy
    G4double x1;
    is >> x1 ;
    if (is.fail()) { // not all the optional parameters given
        return;}
    
    Emuon = x1;
    Enmax = Emuon;
    deltaE = Enmax - Enmin;
    
    // optional overwrite of minimal energy generated
    G4String isElimitmin;
    is >> isElimitmin;
    if (is.fail() || isElimitmin!= "Emin"){
        return; }
    G4double x2;
    is >> x2 ; //we take the value of Elimitmin
    if (is.fail()) {return;}
    Elimitmin = x2;
    
    // optional set particle polarization
    is >> x >> y >> z;
    if (is.fail()) { return; }
    Polar= G4ThreeVector(x,y,z);
    
    // set multiplicity
    is >> Multiplicity;
    if (is.fail()) { return; }
    
    // set Anisotropy
    is >> Ani;
    if (is.fail()) { return; }
    
    // set time jitter
    is >> TJit;
    if (is.fail()) { return; }
    
    // set spectral jitter
    is >> SJit;
    if (is.fail()) { return; }
    
    // set Anisotropy Type
    // "zenith"  1 - a*cos(zenith); -1<_Ani<1
    // "gauss" gaussian-theta beam; 0<_Ani<180 (degree)
    is >> AniType;
    if (is.fail()) { return; }
    
    // set Time continuity
    is >> TCont;
    if (is.fail()) { return; }
}

G4String GLG4VertexGen_CosmogenicNeutrons::GetState()
{
    ostringstream os;
    
    if (Particle == nullptr) {
        Particle= G4ParticleTable::GetParticleTable()->FindParticle("geantino");
    }
    os << Particle->GetParticleName() << '\t'
    << P.x() << ' ' << P.y() << ' ' << P.z() << '\t'
    << Ke << '\t'
    << Polar.x() << ' ' << Polar.y() << ' ' << Polar.z() << '\t'
    << Multiplicity << ' ' << Ani << ' ' << TJit << ' ' << SJit << '\t'
    << AniType << ' ' << TCont
    << ends;
    return G4String(os.str());
}

// Method to shoot a random energy for the particle according to the energy spectrum for cosmo muons-induced neutrons
void GLG4VertexGen_CosmogenicNeutrons::SetEnergy()
{
    /*
     std::vector<double> dNdEmuon(nBinE);
     for(int i = 0; i < nBinE ; i++){
     Emuon = (i+0.5)*deltaE/nBinE;
     dNdEmuon[i] = exp(-b*h*(gammaMu-1))*pow((Emuon + epsilonMu*(1-exp(-b*h))),-gammaMu);
     }
     RandGeneral randGeneralMuon(dNdEmuon.data(), nBinE);
     G4double randomMuon = randGeneralMuon.shoot();
     
     Emuon = EmuonMin + (EmuonMax - EmuonMin)*randomMuon; //in GeV
     */
    //Emuon is at mean energy
    
    //Now that we have a muon energy according to the distribution, we compute the neutron induced energy
    
    B = 0.52 - 0.58*exp(-0.0099*Emuon);
    std::vector<double> dNdE(nBinE);
    
    for(unsigned i = 0; i < dNdE.size() ; ++i){
        Eneutron = (i+0.5)*deltaE/nBinE;
        dNdE[i] = (exp(-a0*Eneutron))/Eneutron + B*exp(-a1*Eneutron);
    }
    
    CLHEP::RandGeneral randGeneral(dNdE.data(), nBinE);
    G4double random = randGeneral.shoot();
    
    //We check if limit inf for energy generation is set
    G4double result = (Enmin + (Enmax - Enmin) * random);
    while(result < Elimitmin){
        random = randGeneral.shoot();
    }
    
    Ke = (Enmin + (Enmax - Enmin) * random)*1000; //*1000 for GeV to MeV
    return;
}

void GLG4VertexGen_CosmogenicNeutrons::SetDirectionCosmo()
{
    //Angle of cosmogenic muons
    
    std::vector<double> dNdcosThetaMuon(nBinAng);
    
    for(unsigned i = 0; i < dNdcosThetaMuon.size() ; ++i){
        cosThetaMuon = (i+0.5)*1./dNdcosThetaMuon.size();
        dNdcosThetaMuon[i] = cosThetaMuon*cosThetaMuon;
    }
    CLHEP::RandGeneral randGeneralMuon(dNdcosThetaMuon.data(), dNdcosThetaMuon.size());
    G4double randMuon = randGeneralMuon.shoot();
    
    
    cosThetaMuon = randMuon;
    
    G4double sinThetaMuon;
    //We have cos theta according to the distribution dNdcosTheta.
    
    sinThetaMuon = sqrt(1-cosThetaMuon*cosThetaMuon); //we do not need to make a random to take this one or its opposite, the sign will be in the random choice of phi
    
    G4double phi = CLHEP::twopi*G4UniformRand();
    dirMuon = G4ThreeVector(sin(phi)*sinThetaMuon,cos(phi)*sinThetaMuon, -cosThetaMuon);
    //the "-" so that the direction aims to the ground
    //Now we have the direction of the muon. Now we compute rotation matrix from the original base, to the muon base
    //with the following convention : dirMuon = matrixMuon*(0 0 1)
    
    //we set the new axis : dirMuon is the new Z,
    G4ThreeVector dirXmuon = dirMuon.orthogonal().unit();
    G4ThreeVector dirYmuon = dirMuon.cross(dirXmuon);
    
    CLHEP::HepRotation matrixMuon;
    matrixMuon.rotateAxes(dirXmuon, dirYmuon, dirMuon);
    
    //Calculating coordinates of the neutron relative to muon frame
    
    //Set Parameters for angular distribution
    Btheta = 0.482*pow(Emuon,0.045);
    Ctheta = 0.832*pow(Emuon,-0.152);
    
    std::vector<double> dNdcosTheta(nBinAng);
    
    for(unsigned i = 0; i < dNdcosTheta.size() ; ++i){
        
        cosTheta =  cosThetaMin + (i+0.5)*deltaCosTheta/dNdcosTheta.size();
        dNdcosTheta[i] = 1/(pow((1-cosTheta), Btheta)+Ctheta);
        
    }
    CLHEP::RandGeneral randGeneral(dNdcosTheta.data(), dNdcosTheta.size());
    G4double rand = randGeneral.shoot();
    cosTheta = cosThetaMin + 2.*rand;
    
    G4double sinTheta = sqrt(1-cosTheta*cosTheta);
    G4double phi2 = CLHEP::twopi*G4UniformRand();
    dir = G4ThreeVector(sin(phi2)*sinTheta,cos(phi2)*sinTheta, cosTheta);
    //This is the direction of the neutron in the muon frame.
    
    //Setting dir as image of dirMuon by the rotation matrix of
    dir = matrixMuon * dir;
    
    return;
}

void GLG4VertexGen_CosmogenicNeutrons::SetDirectionTarget(G4PrimaryVertex* vertex)
{
    dir = this->GetTargetDir(vertex,VolumeTarget);
    P = ( dir * P.mag() );
    vertex->GetPrimary(0)->SetMomentum(P.x(), P.y(),P.z()); // we have the G4PrimaryParticle, can change its impulsio
    return;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

GLG4VertexGen_UserSpec::GLG4VertexGen_UserSpec(const G4String& name, const char* filename, const G4String& pname): GLG4VVertexGen(name), FileName(filename), PName(pname)
{
    Particle = G4ParticleTable::GetParticleTable()->FindParticle("geantino");
    
    ReadDataFile();
    
    //set min and max energies over which spectrum ranges
    Enmin = dNdE[0].first;
    Enmax = dNdE[nBinE-1].first;
    Elimitmin = Enmin;
    Elimitmax = Enmax;
//    std::cout << Enmin << " " << Enmax << std::endl;
//    std::cout << nBinE << std::endl;
}

/** Generates one or more particles with a specified momentum, or isotropic
 momentum with specific energy, and specified polarization, or uniformly
 random polarization, based on parameters set via SetState() */
void GLG4VertexGen_UserSpec::GeneratePrimaryVertex(G4Event *argEvent)
{
    for (G4int imult=0; imult<Multiplicity; imult++) {
        G4PrimaryVertex* vertex= new G4PrimaryVertex( 0.,0.,0.,0. );
        
        if (Particle == nullptr) {
            Particle= G4ParticleTable::GetParticleTable()->FindParticle("geantino");
        }
        G4double mass= Particle->GetPDGMass();
        
        G4PrimaryParticle* particle = nullptr;
        
        //We set the energy according to the energy distribution for neutrons from the atmosphere
        this->SetEnergy();
        
        if (AniType=="fixed") { //check if direction given
            // fixed direction
            G4double p_renorm;
            
            if (Ke <= 0.0) {
                p_renorm = 0.0;
                Ke= 0.0;
            }
            else {p_renorm = sqrt( Ke*(Ke+2.0*mass) / P.mag2() ); }
            P *= p_renorm;
            particle = new G4PrimaryParticle(Particle, P.x(), P.y(), P.z() );
            dir= P.unit();
        }
        else {
            // isotropic direction
            /* generate random ThreeVector of unit length isotropically
             distributed on sphere by cutting two circles from a rectangular
             area and mapping circles onto top and bottom of sphere.
             N.B. the square of the radius is a uniform random variable.
             -GAHS.
             */
            if(AniType=="iso" || AniType.length() == 0){
                while (1) {
                    G4double u,v,q2;
                    // try first circle
                    u= 3.5*G4UniformRand()-1.75;
                    v= 2.0*G4UniformRand()-1.0;
                    q2= u*u + v*v;
                    if (q2 < 1.0) {
                        G4double rho_over_q= sqrt(2.0-q2);
                        dir= G4ThreeVector( u*rho_over_q, v*rho_over_q, 1.0-q2 );
                        break;
                    }
                    // try second circle
                    u= (u>0.0) ? u-1.75 : u+1.75;
                    v= (v>0.0) ? v-1.00 : v+1.00;
                    q2= u*u + v*v;
                    if (q2 < 1.0) {
                        G4double rho_over_q= sqrt(2.0-q2);
                        dir= G4ThreeVector( u*rho_over_q, v*rho_over_q, q2-1.0 );
                        break;
                    }
                }
            }
            else if ( /* Anisotropy */ AniType=="zenith") {
                
                //If it is too big set it to the maximum value
                if (abs(Ani)>1.) { Ani/=abs(Ani); }
                
                G4double phi,cost,st;
                //	cost=2.0*G4UniformRand()-1.0;
                //   For 1+a*cos(theta)  -1<a<1
                cost=(sqrt(1.+Ani*(4.*G4UniformRand()+Ani-2.))-1.)/Ani;
                phi=CLHEP::twopi*G4UniformRand();
                st=sqrt(1.-cost*cost);
                dir=G4ThreeVector(st*cos(phi),st*sin(phi),cost);
            }
            else if (AniType=="center" || AniType == "target") {
                /*Anisotropy according to the target volume. This is handled by ::SetDirectionTarget, called in
                 * GLG4PrimaryGeneratorAction : because the simulation creates first the vertex (with impulsion
                 * and its direction), before creating its position. No other choice to create a random direction
                 * that will be changed after by this method*/
                dir=G4ThreeVector(0.,0.,1.); //dir has to be nonzero, will be changed later
            }
            else {
                cerr << "Invalid AniType is set" << endl;
                break;
            }
            dir=dir.unit(); //just in case
            P = ( dir * sqrt(Ke*(Ke+2.*mass)) );
            particle = new G4PrimaryParticle(Particle, P.x(), P.y(), P.z() );
        }
        
        // adjust polarization if needed
        if (Particle->GetPDGSpin() == 1.0 && mass == 0.0) {
            // spin 1 mass 0 particles should have transverse polarization
            G4ThreeVector rpol= Polar - dir * (dir*Polar);
            G4double rpolmag= rpol.mag();
            if (rpolmag > 0.0) {
                // use projected orthogonal pol, normalized
                rpol *= (1.0/rpolmag);
            } else {
                // choose random pol
                G4double phi= (G4UniformRand()*2.0-1.0)*M_PI;
                G4ThreeVector e1= dir.orthogonal().unit();
                G4ThreeVector e2= dir.cross(e1);
                rpol= e1*cos(phi)+e2*sin(phi);
            }
            particle->SetPolarization(rpol.x(), rpol.y(), rpol.z());
        } else {
            // use user-set polarization unmodified
            particle->SetPolarization(Polar.x(), Polar.y(), Polar.z());
        }
        
        particle->SetMass(mass); // Geant4 is silly.
        vertex->SetPrimary( particle );
        
        if(TJit!=0.||TCont!=0) {
            G4double d_t_dist = TCont * G4UniformRand() + TJit * G4RandGauss::shoot();
            vertex->SetT0( d_t_dist);
        }
        argEvent->AddPrimaryVertex(vertex);
    }
}

/** Set state of the GLG4VertexGen_UserSpec, or show current state and
 explanation of state syntax if empty string provided.
 
 Format of argument to GLG4VertexGen_UserSpec::SetState:
 "unit_momx unit_momy unit_momz Emin  'keywords' polx poly polz mult ani tjit sjit",
 where
 - mom is a unit momemtum (for direction)
 - Emin is the minimum energy from which we sample neutron energies
 - 'keywords' is optionnal
 - 'target VolName': automatically shoot neutron towards center of volume desgined by VolName
 - 'center': shoot neutrons towards the position of the postion generator, if exists (for instance, Gun pos gen)
 - pol* is an optional polarization vector.
 - mult is an optional multiplicity of the particles.
 If mom*==0, then random isotropic directions chosen.
 If pol*==0, then random transverse polarization for photons.
 - ani is an optional anisotropy -1<ani<1.
 - tjit is an optional time jitter in nsec.
 - sjit is an optional spectral jitter in MeV.
 **/

void GLG4VertexGen_UserSpec::SetState(G4String newValues)
{
    //#ifdef DEBUG
    cout << Name << "::SetState: " << newValues << "\n";
    //#endif
    G4String str = GLG4VPosGen::Strip(newValues);
    if (str.length() == 0) {
        // print help and current state
        cout << "Current state of this " << Name << endl;
        cout << " \"" << GetState() << "\"\n" << endl;
        cout << "Format of argument to " << Name << "::SetState: \n"
        " \" unit_momx unit_momy unit_momz 'Emin' EminValue 'target' VolumeTarget polx poly polz mult\"\n"
        " where unit_mom is the momentum direction\n"
        " \'Emin' followed by its value in order to shoot energies above a minimal value.\n"
        " \'target Volumetarget\' in order to make the vertex pointing at the center of a volume.\n"
        " \'center\' in order to shoot particle toward generator center, if it exists (!)"
        " pol* is an optional polarization vector.\n"
        " mult is an optional multiplicity of the particles.\n"
        " mom==0 --> random isotropic directions chosen\n"
        " pol*==0 --> random transverse polarization for photons.\n"
        " ani is an optional anisotropy -1<ani<1.\n"
        " tjit is an optional time jitter in nsec.\n"
        " sjit is an optional spectral jitter in MeV.\n\n";
        return;
    }
    ParamBase &db ( ParamBase::GetInputDataBase() );
    istringstream is(str.c_str());
    
    // set particle
    P = G4ThreeVector(0.,0.,0.);
    cout << "Particle name: " << PName << "\n";
    
    G4ParticleDefinition * newTestAtmosphericNeutronsG4Code = G4ParticleTable::GetParticleTable()->FindParticle(PName);
    if (newTestAtmosphericNeutronsG4Code == nullptr) {
        // if not a particle name : problem with particleTable
        cout << "Could not find the particle" << PName << " in the ParticleTable"  << endl;
    }
    
    Particle = newTestAtmosphericNeutronsG4Code;
    db[ (Name+".pdgcode").c_str() ] = Particle->GetPDGEncoding();
    
    // optional set unit momentum (all the particles will have the same direction)
    G4double x,y,z;
    is >> x >> y >> z;
    if (is.fail()) { // if no momemtum is given : direction would be generated in GeneratePrimaryVertex
        return; }
    P= G4ThreeVector(x*MeV,y*MeV,z*MeV); //momentum used here so that even if unit momemtum is not given, we still get direction
    dir = P.unit();
    if(P.mag2() > 0.0) {AniType = "fixed";}
    else {AniType = "iso";}
    
    // optional overwrites
    // If one wants to make the particle aim at a specific volume
    // NB : the action of the method SetDirectionTarget cannot be done in GeneratePrimaryVertex as the others AniType
    // We first have to wait that the position of our particle is generated. This is done in PrimaryGeneratorAction,
    // where we access to the vertex created here, and change its direction in the case AniType == "target".
    // See PrimaryGeneratorAction in ::GeneratePrimaries()
    G4String keyword;
    is >> keyword;
    
    if (not(is.fail() || keyword.length() == 0)) {
        if (keyword == "Emin"){
            G4double x2;
            is >> x2 ; //we take the value of Elimitmin
            if (is.fail()) {return;}
            TreatKeyWord(EMIN, x2);
            //   ComputeCDF();
            
            //check then for next keyword
            is >> keyword;
            if (not(is.fail() || keyword.length() == 0)) {
                
                if (keyword == "Emax") {
                    G4double x3;
                    is >> x3 ; //we take the value of Elimitmin
                    if (is.fail()) {return;}
                    TreatKeyWord(EMAX, x3);
                    
                    //check then for target keyword
                    is >> keyword;
                    if (not(is.fail() || keyword.length() == 0)) {
                        
                        if (keyword == "target"){
                            G4String VolName{""};
                            is >> VolName;
                            
                            if (not(is.fail() || VolName.empty())) {
                                TreatKeyWord(TARGET,VolName);
                                //return;
                            }
                        }
                        
                        else if (keyword == "center"){
                            TreatKeyWord(CENTER,"");
                            //return;
                        }
                    }
                }
                
                else if (keyword == "target") {
                    G4String VolName{""};
                    is >> VolName;
                    
                    if (not(is.fail() || VolName.empty())) {
                        TreatKeyWord(TARGET,VolName);
                        //return;
                    }
                }
                
                else if (keyword == "center") {
                    TreatKeyWord(CENTER,"");
                    //return;
                }
            }
        }
        
        
        else if (keyword == "Emax"){
            G4double x2;
            is >> x2 ; //we take the value of Elimitmin
            if (is.fail()) {return;}
            TreatKeyWord(EMAX, x2);
            //   ComputeCDF();
            
            //check then for next keyword
            is >> keyword;
            if (not(is.fail() || keyword.length() == 0)) {
                
                if (keyword == "target") {
                    G4String VolName{""};
                    is >> VolName;
                    
                    if (not(is.fail() || VolName.empty())) {
                        TreatKeyWord(TARGET,VolName);
                        //return;
                    }
                }
                
                else if (keyword == "center") {
                    TreatKeyWord(CENTER,"");
                    //return;
                }
                
                //else return;
            }
        }

                
        else if (keyword == "target") {
            G4String VolName{""};
            is >> VolName;
            
            if (not(is.fail() || VolName.empty())) {
                TreatKeyWord(TARGET,VolName);
                //return;
            }
        }
        
        else if (keyword == "center") {
            TreatKeyWord(CENTER,"");
           // return;
        }
        //else return;
    }
    
    ComputeCDF();
    
    // optional set particle polarization
    is >> x >> y >> z;
    if (is.fail()) { return; }
    Polar= G4ThreeVector(x,y,z);
    
    // set multiplicity
    is >> Multiplicity;
    if (is.fail()) { return; }
    
    // set Anisotropy
    is >> Ani;
    if (is.fail()) { return; }
    
    // set time jitter
    is >> TJit;
    if (is.fail()) { return; }
    
    // set spectral jitter
    is >> SJit;
    if (is.fail()) { return; }
    
    // set Anisotropy Type
    // "zenith"  1 - a*cos(zenith); -1<_Ani<1
    // "gauss" gaussian-theta beam; 0<_Ani<180 (degree)
    is >> AniType;
    if (is.fail()) { return; }
    
    // set Time continuity
    is >> TCont;
    if (is.fail()) { return; }
}

void GLG4VertexGen_UserSpec::ComputeCDF()
{
    
    //std::cout << "ComputeCDF()\n";
    
    //Computing CDF from spectrum data points
    Double_t integral_pdf = 0.;
    
    if (Elimitmin == 0){
        CDF.push_back(make_pair(0.,0.));
    }
    
    for(unsigned i = 1; i < nBinE; i++){
        
        if (dNdE[i].first >= Elimitmin && dNdE[i-1].first < Elimitmin) {
            CDF.push_back(make_pair(dNdE[i-1].first, 0.));
        }
        
        if(dNdE[i].first > Elimitmin && dNdE[i-1].first >= Elimitmin && dNdE[i].first <= Elimitmax && dNdE[i-1].first < Elimitmax){
            integral_pdf += (dNdE[i].second-dNdE[i-1].second)/2.*(dNdE[i].first-dNdE[i-1].first) + dNdE[i-1].second*(dNdE[i].first-dNdE[i-1].first);
            CDF.push_back(make_pair(dNdE[i].first, integral_pdf));
        }
    }
    
    //normalization of the CDF
    for (auto &el : CDF) {
        el.second/=integral_pdf;
        //std::cout << el.first << " - " <<el.second << std::endl;
    }
}

G4String GLG4VertexGen_UserSpec::GetState()
{
    ostringstream os;
    
    if (Particle == nullptr) {
        Particle= G4ParticleTable::GetParticleTable()->FindParticle("geantino");
    }
    os << Particle->GetParticleName() << '\t'
    << P.x() << ' ' << P.y() << ' ' << P.z() << '\t'
    << Ke << '\t'
    << Polar.x() << ' ' << Polar.y() << ' ' << Polar.z() << '\t'
    << Multiplicity << ' ' << Ani << ' ' << TJit << ' ' << SJit << '\t'
    << AniType << ' ' << TCont
    << ends;
    return G4String(os.str());
}

void GLG4VertexGen_UserSpec::ReadDataFile(){
    G4String path = getenv("ENERGE_DATA");
    
    ifstream file(path+"/Physics/"+FileName, ios::in);
    
    if(file){
        string line="";
        G4double tempE;
        G4double tempdNdE;
        int i=0;
        while(getline(file, line)){
            istringstream iss(line);
            iss >> tempE;
            iss >> tempdNdE;
            //tempdNdE/=tempEneutron; // the file actually gives E*dNdE
            
            dNdE.push_back(make_pair(tempE,tempdNdE));
            i++;
//            std::cout << tempE << " - " << tempdNdE << std::endl;
        }
        nBinE = dNdE.size();
    }
    else
    {
        G4String Exp = Name+"::ReadDataFile()";
        G4Exception(Exp,"1",FatalException,
                    "Error : data file for energy spectrum points not opened!");
    }
    file.close();
}

void GLG4VertexGen_UserSpec::SetEnergy()
{
    G4double result = -1.;
    unsigned i = 0;
    Double_t alea = 0.;
    
    while (result < Elimitmin){
        
        i=0;
        alea = G4UniformRand();
        
        while (CDF[i].second < alea) {
            i++;
        }
        
        //So now CDF[i-1] is our lower value and we have to interpolate the random value between two points
        if (i > 0 && i < nBinE){
            result = (alea-CDF[i-1].second)*(CDF[i].first-CDF[i-1].first)/(CDF[i].second-CDF[i-1].second) + CDF[i-1].first; //linear interpolation
        }
        
        else if (i == 0) result = dNdE[0].first;
        else if (i > nBinE) result = dNdE[nBinE-1].first;
    }
    
    Ke = result;
    //std::cout << "SetEnergy() " << result << std::endl;
    return;
}

void GLG4VertexGen_UserSpec::SetDirectionTarget(G4PrimaryVertex* vertex)
{
    dir = this->GetTargetDir(vertex,VolumeTarget);
    P = ( dir * P.mag() );
    vertex->GetPrimary(0)->SetMomentum(P.x(), P.y(),P.z()); // we have the G4PrimaryParticle, can change its momentum
}

void GLG4VertexGen_UserSpec::ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir)
{
    G4ThreeVector dir_ = Dir.unit();
    P = (dir_ * P.mag() );
    vertex->GetPrimary(0)->SetMomentum(P.x(),P.y(),P.z());
}

void GLG4VertexGen_UserSpec::TreatKeyWord(KeyWord Key, const Double_t& Input){
    
    switch (Key) {
        case EMIN:
            Elimitmin = Input;
            if (Elimitmin < Enmin) {
                Elimitmin = Enmin;
                cout << "Warning: requested Emin too small, set Emin to " << Enmin << " MeV" << endl;
            }
            else {cout << "Set Emin to " << Elimitmin << " MeV" << endl;}
            break;
            
        case EMAX:
            Elimitmax = Input;
            if (Elimitmax > Enmax) {
                Elimitmax = Enmax;
                cout << "Warning: requested Emax too high, set Emax to " << Enmax << " MeV" << endl;
            }
            else {cout << "Set Emax to " << Elimitmax << " MeV" << endl;}
            break;
            
        default:
            break;
    }
    
}

void GLG4VertexGen_UserSpec::TreatKeyWord(KeyWord Key, const G4String& Input){
    
    switch (Key) {
        case TARGET:
            VolumeTarget = Input;
            AniType = "target";
            cout << "Aim all vertices at " << VolumeTarget << " direction..." << endl;
            break;
            
        case CENTER:
            AniType = "center";
            cout << "Will aim all vertices at position generator center..." << endl;
            break;
            
        default:
            break;
    }
}

//************************************************************//
///////////////////// ATMOSPHERIC NEUTRONS GENERATOR //////////////////////
//************************************************************//

GLG4VertexGen_AtmosphericNeutrons::GLG4VertexGen_AtmosphericNeutrons(const G4String& name, const char* filename, const G4String& pname): GLG4VertexGen_UserSpec(name,filename,pname)
{
}

//************************************************************//
//////////////////////////////////AMBIENT GAMMA  GENERATORS ////////////////////////
//************************************************************//

GLG4VertexGen_AmbientGammas::GLG4VertexGen_AmbientGammas(const G4String& name, const char* filename, const G4String& pname): GLG4VertexGen_UserSpec(name,filename,pname)
{
}

GLG4VertexGen_AmbientU238::GLG4VertexGen_AmbientU238(const G4String& name, const char* filename, const G4String& pname): GLG4VertexGen_AmbientGammas(name,filename,pname){
}

GLG4VertexGen_AmbientTh232::GLG4VertexGen_AmbientTh232(const G4String& name, const char* filename, const G4String& pname): GLG4VertexGen_AmbientGammas(name,filename,pname){
}

GLG4VertexGen_AmbientK40::GLG4VertexGen_AmbientK40(const G4String& name, const char* filename, const G4String& pname): GLG4VertexGen_AmbientGammas(name,filename,pname){
}

GLG4VertexGen_AmbientGammasfromDeconv::GLG4VertexGen_AmbientGammasfromDeconv(const G4String& name, const char* filename, const G4String& pname): GLG4VertexGen_AmbientGammas(name,filename,pname){
}
//************************************************************//
////////////////// ATMOSPHERIC MUON GENERATOR ///////////////////
//************************************************************//

GLG4VertexGen_AtmosphericMuons::GLG4VertexGen_AtmosphericMuons(const G4String& name): GLG4VVertexGen(name), NbEBins(1), EMin(1.), EMax(10.), NbABins(360), NbZBins(90), Target("")
{
    //G4cout << "Create AtmosphericMuons generator" << G4endl;
    //    Particle= G4ParticleTable::GetParticleTable()->FindParticle("geantino");
}

G4bool GLG4VertexGen_AtmosphericMuons::IsInitialized = false;
std::vector<GLG4VertexGen_AtmosphericMuons::MuonData> GLG4VertexGen_AtmosphericMuons::CDF = std::vector<GLG4VertexGen_AtmosphericMuons::MuonData>();


/** Generates one or more particles with a specified momentum, or isotropic
 momentum with specific energy, and specified polarization, or uniformly
 random polarization, based on parameters set via SetState() */
void GLG4VertexGen_AtmosphericMuons::GeneratePrimaryVertex(G4Event *argEvent)
{
    if(!IsInitialized){CalculateCDF();};
    
    //Sample particle charge and energy|zenith independently
    
    //First sample particle charge
    //mu+/mu- ratio at surface below 100GeV/c acc. to CMS arXiv:1005.5332v1
    const G4double R = 1.2766;
    G4double fM = 1./(1.+R);
    //	G4double fP = 1./(1./R + 1.);
    //	cout << " f-=" << fM << " f+=" << fP << '\n';
    G4ParticleDefinition *mu = nullptr;
    if(G4UniformRand() < fM){
        //it's a mu-
        mu = G4ParticleTable::GetParticleTable()->FindParticle("mu-");
    }else{
        //it's a mu+
        mu = G4ParticleTable::GetParticleTable()->FindParticle("mu+");
    }
    
    //Second sample energy and zenith
    //Get random number from flat PDF ...
    G4double rnd = G4UniformRand();
    //	cout << " rnd=" << rnd << '\n';
    //... and transform it acc. to muon CDF
    //according to: http://www.bl.physik.uni-muenchen.de/~schubert/doc/geant4/html/G4PhysicsOrderedFreeVector_8cc-source.html
    G4int n1 = 0;
    G4int n2 = CDF.size() / 2;
    G4int n3 = CDF.size() - 1;
    while (n1 != n3 - 1) {
        //		cout << " n1=" << n1 << " prob=" << CDF[n2].probability << '\n';
        if (rnd > CDF[n2].probability) {
            n1 = n2;
        } else {
            n3 = n2;
        }
        n2 = n1 + (n3 - n1 + 1) / 2;
    }
    //	cout << " n1=" << n1 << '\n';
    
    //Construct direction vector from (azimuth, zenith) pair
    G4double theta = CDF[n1].zenith;
    G4double phi = CDF[n1].azimuth;
    G4double energy = CDF[n1].energy;
    G4double dirX = -sin(theta) * cos(phi);//Invert the direction, i.e. multiply by -1, because the muon have to go towards the 'observer'!
    G4double dirY = -sin(theta) * sin(phi);
    G4double dirZ = -cos(theta);
    //cout << " phi=" << CDF[n1].azimuth << " theta=" << CDF[n1].zenith << " -> " << dirX << "|" << dirY << "|" << dirZ << '\n';
    
#ifdef DEBUG
    cout << " New primary particle is a " << mu->GetParticleName() << " Unit momentum=(" << dirX << " | " << dirY << " | " << dirZ << ") energy/GeV=" << energy/GeV << '\n';
#endif
    
    G4PrimaryParticle *particle = new G4PrimaryParticle(mu);
    particle->SetMomentum(dirX, dirY, dirZ);
    particle->SetKineticEnergy(energy);
    G4PrimaryVertex* vertex = new G4PrimaryVertex( 0.,0.,0.,0. );
    vertex->SetPrimary(particle);
    vertex->SetPosition(dirX, dirY, dirZ);
    //	vertex->Print();
    argEvent->AddPrimaryVertex(vertex);
}

void GLG4VertexGen_AtmosphericMuons::CalculateCDF() {
    G4cout << GetState() << G4endl;
    
    //Calculate angular step size
    const G4double AMax = 360.;
    G4double dA = AMax/NbABins; //Azimuth step size
    G4double dZ = (ZMax-ZMin)/NbZBins;//Zenith step size
    
    //Total differential flux, needed for normalization
    G4double totalFlux = 0.;
    
    //Sample the energy-direction parameter space in three loops
    //Sample energy range in logarithmic intervals
    G4double eWidth = (log10(EMax) - log10(EMin))
    / (NbEBins);
    for (G4int e = 0; e < NbEBins; ++e) {
        G4double energyMin = pow(10, (log10(EMin) + e * eWidth));      //Lower edge of energy bin
        G4double energyMax = pow(10, (log10(EMin) + (e + 1) * eWidth));//Upper edge of energy bin
        G4double energy = energyMin + (energyMax-energyMin)/2.;        //Center of energy bin
        
        //Sample azimuth range
        for (G4int a = 0; a < NbABins; ++a) {
            G4double azimuthMin = a*dA*deg;                             //Lower edge of azimuth bin
            G4double azimuthMax = (a+1)*dA*deg;                         //Upper edge of azimuth bin
            G4double azimuth = azimuthMin + (azimuthMax-azimuthMin)/2.; //Center of azimuth bin
            
            //Sample zenith range
            for (G4int z = 0; z < NbZBins; ++z) {
                G4double zenithMin = (z*dZ+ZMin)*deg;                          //Lower edge of zenith bin
                G4double zenithMax = ((z+1)*dZ+ZMin)*deg;                      //Upper edge of zenith bin
                G4double zenith = zenithMin + (zenithMax-zenithMin)/2.; //Center of zenith bin
                
                
                G4double intMuonFlux = CalculateIntMuonFlux(energyMin/GeV, energyMax/GeV, zenithMin/rad, zenithMax/rad, azimuthMin/rad, azimuthMax/rad);
                
#ifdef DEBUG
                cout << " --\n";
                cout << "  energy / GeV:....................." << energy/GeV << '\n';
                cout << "  azimuth / °:......................" << azimuth/deg << '\n';
                cout << "  zenith / °:......................." << zenith/deg << '\n';
                cout << "  integral muon flux / cm-2 s-1:...." << intMuonFlux << '\n';
#endif
                
                //Remove all combination with zero probability
                if (intMuonFlux != 0.0) {
                    CDF.push_back(MuonData(intMuonFlux, energy, zenith, azimuth));
                    totalFlux += intMuonFlux;
                }
            }//end zenith loop
        }//end azimuth loop
    }//end energy loop
    
    //Store total muon flux in parameter data base
    //(so that user can later re-weight the simulated data)
    ParamBase::GetInputDataBase().SetNumValue("total_muon_flux",totalFlux);
    
    //Build the actual CDF by first adding up the fluxes ...
    CDF[0].probability = CDF[0].flux;
    for(size_t i = 1; i < CDF.size(); ++i){
        CDF[i].probability = CDF[i].flux + CDF[i-1].probability;
    }
    //and then normalizing them to 1
    for(auto& element: CDF){
        element.probability /= totalFlux;
    }
    
    cout << "GLG4VertexGen_AtmosphericMuons::CalculateCDF:\n";
    cout << "  Total muon flux / cm-2 s-1: " << totalFlux << '\n';
#ifdef DEBUG
    cout << "  Energy / GeV | Azimuth / ° | Zenith / ° | Probability | Flux / 1/(cm2*s) \n";
    for(auto& element: CDF){
        cout << "  " << element.energy/GeV << " | " << element.azimuth/deg << " | " << element.zenith/deg << " | " << element.probability << " | " << element.flux << "\n";
    }
#endif
    
    //Flag successfully building of CDF table
    IsInitialized = true;
}

/**
 * @brief Calculate the integral atmospheric muon flux
 *
 * Integrate the differential muon flux over [energyLow, energyUp]x[azimuthLow, azimuthUp]x[zenithLow, zenithUp]
 * by applying the trapezoidal rule.
 * @param energyLow Lower boundary of energy in GeV
 * @param energyUp Upper boundary of energy in GeV
 * @param zenithLow Lower boundary of zenith angle in rad
 * @param zenithUp Upper boundary of zenith angle in rad
 * @param azimuthLow Lower boundary of azimuth angle in rad
 * @param azimuthUp Upper boundary of azimuth angle in rad
 * @return Integral muon flux at ground in s^-1 cm^-2
 */
G4double GLG4VertexGen_AtmosphericMuons::CalculateIntMuonFlux(G4double energyLow, G4double energyUp, G4double zenithLow, G4double zenithUp, G4double azimuthLow, G4double azimuthUp){
    //int_{E_min}^{E_max} int_Omega f(E,theta) sin(theta) dOmega dE
    //=int_{E_min}^{E_max} int_{theta_min}^{theta_max} int_{phi_min}^{phi_max} f(E,theta) sin(theta) dphi dtheta dE
    //=int_{E_min}^{E_max} int_{theta_min}^{theta_max} f(E,theta) sin(theta) dtheta dE
    //=(phi_max-phi_min) * int_{theta_min}^{theta_max} (f(E_max,theta)+f(E_min,theta))/2 (E_max-E_min) sin(theta) dtheta
    //=(theta_max-theta_min) * (phi_max-phi_min) * (E_max-E_min) * ((f(E_max,theta_max)+f(E_min,theta_max))/2  sin(theta_max))+(f(E_max,theta_min)+f(E_min,theta_min))/2  sin(theta_min)))/2
    return (energyUp-energyLow)
    *(azimuthUp-azimuthLow)
    *(zenithUp-zenithLow)
    *(
      (
       CalculateDiffMuonFlux(energyUp, zenithUp)
       +CalculateDiffMuonFlux(energyLow, zenithUp)
       )*sin(zenithUp)
      +(
        CalculateDiffMuonFlux(energyUp, zenithLow)
        +CalculateDiffMuonFlux(energyLow, zenithLow)
        )*sin(zenithLow)
      )/4.;
}

/**
 * @brief Calculate the differential atmospheric muon flux
 *
 * Based on [A. Tang et al., 2006, arXiv:hep-ph/0604078v2].
 *
 * @param energy Muon energy at surface in GeV
 * @param zenith Zenith angle at ground in rad
 * @return Differential muon flux at ground in GeV^-1 sr^-1 s^-1 cm^-2
 */
G4double GLG4VertexGen_AtmosphericMuons::CalculateDiffMuonFlux(G4double energy,
                                                               G4double zenith) {
    //Atmospheric muon flux at low energies acc. to [A. Tang et al., arXiv:hep-ph/0604078v2]
    const double rC = 1e-4;	//eq.4
    const double muIndex = -2.70;
    const double A0 = 0.14;
    const double p1 = 0.102573;
    const double p2 = -0.068287;
    const double p3 = 0.958633;
    const double p4 = 0.0407253;
    const double p5 = 0.81728;
    
    double x = cos(zenith);
    double czenithStar = sqrt((x * x + p1 * p1 + p2 * pow(x, p3) + p4 * pow(x, p5))/ (1. + p1 * p1 + p2 + p4));//eq.10
    
    double diffFlux = -1.;
    double energyPrim = -1.;
    double delta = -1.;
    double energyTilde = -1.;
    double A = -1.;
    int section = -1;
    
    //Lambda expressions for eqs.3,5,6,7.
    //Use lambdas to (1) prevent typing the same equation several times and (2) don't polluting the class namespace with auxiliary functions not used outside this functions
    auto eq3 = [](double A, double gamma, double energy, double energyTilde, double czenithStar, double rC) {return A * pow(energy, gamma) * (1./(1+(1.1*energyTilde*czenithStar/115.)) + 0.054/(1.+(1.1*energyTilde*czenithStar/850.)) + rC);};
    auto eq5 = [](double czenithStar) {return 2.06e-3 * (950./czenithStar-90.);};
    auto eq6 = [](double energy, double delta) {return energy + delta;};
    //eq.7, bug in paper: has to be energyTilde instead of energy
    auto eq7 = [](double energyTilde, double czenith, double czenithStar) {return 1.1 * pow((90. * sqrt(czenith+0.001)/1030.),4.5/(energyTilde * czenithStar));};
    if (energy > 100. / czenithStar) {
        diffFlux = eq3(A0, muIndex, energy, energy, czenithStar, rC);
        section = 1;
    }
    if ((energy <= 100. / czenithStar) && (energy > 1. / czenithStar)) {
        delta = eq5(czenithStar);
        energyTilde = eq6(energy, delta);
        A = eq7(energyTilde, x, czenithStar);
        diffFlux = eq3(A0 * A, muIndex, energy, energyTilde, czenithStar, rC);
        section = 2;
    }
    if (energy <= 1. / czenithStar) {
        
        energyPrim = (3. * energy + 7. / czenithStar) / 10.;//eq.9
        
        delta = eq5(czenithStar);
        energyTilde = eq6(energyPrim, delta);
        A = eq7(energyTilde, x, czenithStar);
        diffFlux = eq3(A0 * A, muIndex, energyPrim, energyTilde, czenithStar, rC);
        section = 3;
    }
    
#ifdef DEBUG
    cout << "  section:....... " << section << '\n';
    cout << "  x:............. " << x << '\n';
    cout << "  zenithStar:.... " << czenithStar << '\n';
    cout << "  energyPrim:.... " << energyPrim << '\n';
    cout << "  delta:......... " << delta << '\n';
    cout << "  energyTilde:... " << energyTilde << '\n';
    cout << "  A:............. " << A << '\n';
    cout << "  diffFlux:...... " << diffFlux << '\n';
#endif
    
    return diffFlux;
}

/** Set state of the GLG4VertexGen_AtmosphericMuons, or show current state and
 explanation of state syntax if empty string provided.
 
 Format of argument to GLG4VertexGen_AtmosphericMuons::SetState:
 "ebin emin emax abin zbin vol",
 where
 - ebin is the number of logarithmic bins for the energy range
 - emin is the lower bound in GeV of the energy range
 - emax is the upper bound in GeV of the energy range
 - abin is the number of linear bins for the azimuth range
 - zbin is the number of linear bins for the zenith range
 - vol is a target volume
 */
void GLG4VertexGen_AtmosphericMuons::SetState(G4String newValues)
{
#ifdef DEBUG
    cout << "GLG4VertexGen_AtmosphericMuons::SetState: " << newValues << "\n";
#endif
    G4String str = GLG4VPosGen::Strip(newValues);
    if (str.length() == 0) {
        // print help and current state
        cout << "Current state of this GLG4VertexGen_AtmosphericMuons:\n"
        << " \"" << GetState() << "\"\n" << endl;
        cout << "Format of argument to GLG4VertexGen_AtmosphericMuons::SetState: \n"
        " \"ebin emin emax abin zbin vol\"\n"
        " where ebin is the number of logarithmic bins for the energy range.\n"
        " emin is the lower bound in GeV of the energy range.\n"
        " emax is the upper bound in GeV of the energy range.\n"
        " abin is the number of linear bins for the azimuth range.\n"
        " zmin is the minimal azimuthal angle in deg.\n"
        " zmax is the minimal azimuthal angle in deg.\n"
        " zbin is the number of linear bins for the zenith range.\n"
        " vol is a target volume.\n\n";
        return;
    }
    
    istringstream is(str.c_str());
    
    // set number of energy bins, minimal energy, and maximal energy
    is >> NbEBins >> EMin >> EMax;
    if (is.fail()) { return; }
    if (NbEBins < 1) { return; }
    if ((EMax < EMin) || (EMax == EMin)) { return; }
    EMax *= GeV; //Energies are in GeV
    EMin *= GeV;
    
    // set number of azimuth bins
    is >> NbABins;
    if (is.fail()) { return; }
    if (NbABins < 1) { return; }
    
    // set azimuthal angle min and max and number of zenith bins
    is >> ZMin >> ZMax >> NbZBins;
    if (is.fail()) { return; }
    if (ZMax < ZMin) { return; }
    if (ZMax == ZMin) { NbZBins=1; }
    if (NbZBins < 1) { return; }
    
    // set target volume
    is >> Target;
    if (is.fail()) { return; }
}

G4String GLG4VertexGen_AtmosphericMuons::GetState()
{
    ostringstream os;
    
    os << NbEBins << '\t'
    << EMin << ' ' << EMax << '\t'
    << NbABins << '\t'
    << NbZBins << '\t'
    << Target << '\t'
    << ends;
    return G4String(os.str());
}

void GLG4VertexGen_AtmosphericMuons::SetDirectionTarget(G4PrimaryVertex* vertex)
{
    //Change only the momentum direction but not the magnitude
    G4ThreeVector dir = this->GetTargetDir(vertex,Target);
    vertex->GetPrimary(0)->SetMomentum(dir.x(), dir.y(),dir.z());
    return;
}

void GLG4VertexGen_AtmosphericMuons::ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir){
    G4ThreeVector dir = Dir.unit();
    vertex->GetPrimary(0)->SetMomentum(dir.x(), dir.y(),dir.z());
    return;
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

void GLG4VertexGen_GPS::ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir){(void) vertex; (void) Dir; return;}
void GLG4VertexGen_GPS::SetDirectionTarget(G4PrimaryVertex* vertex){(void) vertex; return;}
