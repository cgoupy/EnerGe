#include <GLG4Material.hh>

#include <String.hh>

#include <Parameters.hh>
#include <DCGLG4BuildOpticalProperties.hh>

#include <globals.hh>
#include <G4NistManager.hh>
#include <G4PhysicalConstants.hh>
#include <G4SystemOfUnits.hh>
#include <G4Material.hh>
#include <G4Element.hh>
#include <G4Isotope.hh>
#include <G4MaterialPropertiesTable.hh>
#include <G4OpticalSurface.hh>
#include <G4VisAttributes.hh>
#include <G4Colour.hh>
#include <TMatrixT.h>

#include <iostream>

using namespace std;

//////////////////////////////////////////////////////////////////////////////////

GLG4Material* GLG4Material::theMaterials = nullptr;
std::map<G4Material*, G4VisAttributes*> GLG4Material::mVis;

//////////////////////////////////////////////////////////////////////////////////

void GLG4Material::UpdatePhotocathodeMPT()
{
    Photocathode_opsurf->SetMaterialPropertiesTable(
                                                    G4Material::GetMaterial("photocathode") -> GetMaterialPropertiesTable()
                                                    );
}

void GLG4Material::AddVisAttribute(G4Material* material, const G4Colour& colour)
{
    if ( mVis.count(material) == 0 ) {
        G4VisAttributes* visattr = new G4VisAttributes(colour);
        visattr->SetVisibility(true);
        visattr->SetForceSolid(0);
        mVis[material] = visattr;
    } else {
        G4Exception("GLG4Material::AddVisAttribute","0",JustWarning,
                    scat("Material has already a VisAttribute: ", material->GetName()).c_str() );
    }
}


//////////////////////////////////////////////////////////////////////////////////

GLG4Material::GLG4Material()
{
    G4MaterialPropertiesTable* MPT = nullptr;
    G4String symbol;
    G4double M, density, fractionmass, mol; //M = mass of a mole;
    G4int Z, A, nelements, natoms;
    
    ///////////////////////////// Default vis attribute ////////////////////////////
    
    AddVisAttribute(nullptr,G4Colour::Green());
    
    //////////////////////////////// Optical surface ///////////////////////////////
    
    Photocathode_opsurf =  new G4OpticalSurface("Photocathode_opsurf");
    Photocathode_opsurf->SetType(dielectric_metal); // ignored if RINDEX defined
    
    /////////////////////////////////// Elements ///////////////////////////////////
    
    G4Element* H  = new G4Element("Hydrogen",	symbol="H",  Z= 1,  M=   1.01 *g/mole);
    G4Element* elTSHPol = new G4Element("TS_H_of_Polyethylene", "H_POLYETHYLENE", 1.0, 1.01*g/mole); //to be used with thermal neutron libraries
    G4Element* elTSHWater = new G4Element("TS_H_of_Water", "H_WATER", 1.0, 1.01*g/mole); //to be used with thermal neutron libraries
    G4Element* Li = new G4Element("Lithium",    symbol="Li", Z= 3,  M=   6.94 *g/mole); // --> TO CHECK
    G4Element* Be = new G4Element("Beryllium",  symbol="Be", Z= 4,  M=   9.01 *g/mole);
    G4Element* B  = new G4Element("Boron",		symbol="B",  Z= 5,  M=  10.81 *g/mole);
    G4Element* C  = new G4Element("Carbon",		symbol="C",  Z= 6,  M=  12.01 *g/mole);
    G4Element* N  = new G4Element("Nitrogen",	symbol="N",  Z= 7,  M=  14.01 *g/mole);
    G4Element* F  = new G4Element("Fluorine",	symbol="F",  Z= 9,  M=  18.998*g/mole);
    G4Element* Na = new G4Element("Sodium",		symbol="Na", Z= 11, M=  22.99 *g/mole);
    G4Element* Mg = new G4Element("Magnesium",	symbol="Mg", Z= 12, M=  24.31 *g/mole);
    G4Element* Al = new G4Element("Aluminium",	symbol="Al", Z= 13, M=  26.98 *g/mole);
    G4Element* K  = new G4Element("Potassium",	symbol="K",  Z= 19, M=  39.098*g/mole);
    G4Element* Cr = new G4Element("Chromium",	symbol="Cr", Z= 24, M=  51.996*g/mole);
    G4Element* Fe = new G4Element("Iron",		symbol="Fe", Z= 26, M=  55.85 *g/mole);
    G4Element* Ni = new G4Element("Nickel",		symbol="Ni", Z= 28, M=  58.693*g/mole);
    G4Element* Cu = new G4Element("Copper",		symbol="Cu", Z= 29, M=  63.546 *g/mole);
    G4Element* Mo = new G4Element("Molybdenum",	symbol="Mo", Z= 42, M=  95.94 *g/mole);
    G4Element* Sn = new G4Element("Tin",		symbol="Sn", Z= 50, M= 118.710 *g/mole);
    G4Element* Pb = new G4Element("Lead",		symbol="Pb", Z= 82, M= 207.19 *g/mole);
    G4Element* U  = new G4Element("Uranium",	symbol="U",  Z= 92, M= 237.449*g/mole); // 235043930.131×0.2+238050788.423×0.8 from atomic mass evaluation 2012
    
    //	Gadolinium isotopes
    G4Isotope* Gd152 = new G4Isotope("Gd152", Z= 64, A= 152, M= 151.919798822*g/mole); // M from atomic mass evaluation 2012
    G4Isotope* Gd154 = new G4Isotope("Gd154", Z= 64, A= 154, M= 153.920873398*g/mole); // M from atomic mass evaluation 2012
    G4Isotope* Gd155 = new G4Isotope("Gd155", Z= 64, A= 155, M= 154.922629796*g/mole); // M from atomic mass evaluation 2012
    G4Isotope* Gd156 = new G4Isotope("Gd156", Z= 64, A= 156, M= 155.922130562*g/mole); // M from atomic mass evaluation 2012
    G4Isotope* Gd157 = new G4Isotope("Gd157", Z= 64, A= 157, M= 156.923967870*g/mole); // M from atomic mass evaluation 2012
    G4Isotope* Gd158 = new G4Isotope("Gd158", Z= 64, A= 158, M= 157.924111646*g/mole); // M from atomic mass evaluation 2012
    G4Isotope* Gd160 = new G4Isotope("Gd160", Z= 64, A= 160, M= 159.927061537*g/mole); // M from atomic mass evaluation 2012
    
    G4Element* Gd = new G4Element("Gadolinium", symbol="Gd",7);
    Gd->AddIsotope(Gd152,  0.20*perCent); // beware : it is abundance, not fraction mass
    Gd->AddIsotope(Gd154,  2.18*perCent);
    Gd->AddIsotope(Gd155, 14.8*perCent);
    Gd->AddIsotope(Gd156, 20.47*perCent);
    Gd->AddIsotope(Gd157, 15.65*perCent);
    Gd->AddIsotope(Gd158, 24.84*perCent);
    Gd->AddIsotope(Gd160, 21.86*perCent);
    
    //Silicon isotopes
    G4Isotope* Si28 = new G4Isotope("Si28", Z=14, A=28, M=27.97692653499*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Si29 = new G4Isotope("Si29", Z=14, A=29, M=28.97649466525*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Si30 = new G4Isotope("Si30", Z=14, A=30, M=29.973770136*g/mole);   // M from atomic mass evaluation 2016

    G4Element* Si = new G4Element("Silicon", symbol="Si", 3);
    Si->AddIsotope(Si28, 92.23*perCent);
    Si->AddIsotope(Si29,  4.68*perCent);
    Si->AddIsotope(Si30,  3.09*perCent);
    
    //Germanium isotopes
    G4Isotope* Ge70 = new G4Isotope("Ge70", Z=32, A=70, M=69.924248706*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Ge72 = new G4Isotope("Ge72", Z=32, A=72, M=71.922075826*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Ge73 = new G4Isotope("Ge73", Z=32, A=73, M=72.923458956*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Ge74 = new G4Isotope("Ge74", Z=32, A=74, M=73.921177762*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Ge76 = new G4Isotope("Ge76", Z=32, A=76, M=75.921402726*g/mole); // M from atomic mass evaluation 2016
    
    G4Element* Ge = new G4Element("Germanium", symbol="Ge", 5);
    Ge->AddIsotope(Ge70,  20.52*perCent);
    Ge->AddIsotope(Ge72,  27.45*perCent);
    Ge->AddIsotope(Ge73, 7.76*perCent);
    Ge->AddIsotope(Ge74, 36.52*perCent);
    Ge->AddIsotope(Ge76, 7.75*perCent);
    
    //Ca isotopes
    G4Isotope* Ca40 = new G4Isotope("Ca40", Z=20, A=40, M=39.962590865*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Ca42 = new G4Isotope("Ca42", Z=20, A=42, M=41.958617828*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Ca43 = new G4Isotope("Ca43", Z=20, A=43, M=42.958766430*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Ca44 = new G4Isotope("Ca44", Z=20, A=44, M=43.955481543*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Ca46 = new G4Isotope("Ca46", Z=20, A=46, M=45.953687988*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Ca48 = new G4Isotope("Ca48", Z=20, A=48, M=47.952522904*g/mole); // M from atomic mass evaluation 2016
    
    G4Element* Ca = new G4Element("Calcium", symbol="Ca", 6);
    Ca->AddIsotope(Ca40,  96.941*perCent);
    Ca->AddIsotope(Ca42,  0.647*perCent);
    Ca->AddIsotope(Ca43, 0.135*perCent);
    Ca->AddIsotope(Ca44, 2.086*perCent);
    Ca->AddIsotope(Ca46, 0.004*perCent);
    Ca->AddIsotope(Ca48, 0.187*perCent);
    
    //Zn isotopes
    G4Isotope* Zn64 = new G4Isotope("Zn64", Z=30, A=64, M=63.929141772*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Zn66 = new G4Isotope("Zn66", Z=30, A=66, M=65.926033704*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Zn67 = new G4Isotope("Zn67", Z=30, A=67, M=66.927127482*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Zn68 = new G4Isotope("Zn68", Z=30, A=68, M=67.924844291*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* Zn70 = new G4Isotope("Zn70", Z=30, A=70, M=69.925319181*g/mole); // M from atomic mass evaluation 2016
    
    G4Element* Zn = new G4Element("Zinc", symbol="Zn", 5);
    Zn->AddIsotope(Zn64,  49.2*perCent);
    Zn->AddIsotope(Zn66,  27.7*perCent);
    Zn->AddIsotope(Zn67, 4.0*perCent);
    Zn->AddIsotope(Zn68, 18.5*perCent);
    Zn->AddIsotope(Zn70, 0.6*perCent);
    
    //W isotopes
    G4Isotope* W180 = new G4Isotope("W180", Z=74, A=180, M=179.946713435*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* W182 = new G4Isotope("W182", Z=74, A=182, M=181.948205721*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* W183 = new G4Isotope("W183", Z=74, A=183, M=182.950224500*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* W184 = new G4Isotope("W184", Z=74, A=184, M=183.950933260*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* W186 = new G4Isotope("W186", Z=74, A=186, M=185.954365215*g/mole); // M from atomic mass evaluation 2016
    
    G4Element* W = new G4Element("Tungsten", symbol="W", 5);
    W->AddIsotope(W180,  0.12*perCent);
    W->AddIsotope(W182,  26.50*perCent);
    W->AddIsotope(W183, 14.31*perCent);
    W->AddIsotope(W184, 30.64*perCent);
    W->AddIsotope(W186, 28.43*perCent);
    
    //O isotopes
    G4Isotope* O16 = new G4Isotope("O16", Z=8, A=16, M=15.99491461960*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* O17 = new G4Isotope("O17", Z=8, A=17, M=16.99913175664*g/mole); // M from atomic mass evaluation 2016
    G4Isotope* O18 = new G4Isotope("O18", Z=8, A=18, M=17.99915961284*g/mole); // M from atomic mass evaluation 2016
    
    G4Element* O  = new G4Element("Oxygen", symbol="O", 3);
    O->AddIsotope(O16,  99.76*perCent);
    O->AddIsotope(O17,  0.04*perCent);
    O->AddIsotope(O18, 0.20*perCent);
    
    
    ////////////////////////// Basic materials ///////////////////////////////
    
    //necessary for cryostat
    G4Material* fMaterialScintillator = new G4Material("Scintillator",1.032*g/cm3,2);
    fMaterialScintillator->AddElement(C,9);
    fMaterialScintillator->AddElement(H,10);
    
    //PMT vacuum
    G4Material* PMT_Vac = new G4Material("PMT_Vac", density= 1e-3 * kGasThreshold, nelements= 1, kStateGas,
                                         STP_Temperature, STP_Pressure * (1e-3*kGasThreshold) / (1.29e-3*g/cm3));
    PMT_Vac -> AddElement(N, natoms=1);
    AddVisAttribute(PMT_Vac,G4Colour::White());
    
    G4Material* Air = new G4Material("Air", density= 1.290*mg/cm3, nelements= 2);
    Air -> AddElement(N, fractionmass=0.7);
    Air -> AddElement(O, fractionmass=0.3);
    AddVisAttribute(Air,G4Colour::White());
    
    //Vacuum
//    G4double temperature= 300*kelvin;
//    G4double pressure= 2.0e-7*bar;
//    G4Material* Vac = new G4Material("Vac", density=2.376e-15*g/cm3, nelements=1,kStateGas,temperature,pressure);
//    Vac->AddElement(N, natoms=1);

    //Note (MV Feb 2022): redefinition of vaccum as suggested by L. Thulliez. Solves the 'TRACK003' exception issued by : G4ParticleChange::CheckIt
    G4NistManager* nistManager = G4NistManager::Instance();
    G4Material* Vac = nistManager->FindOrBuildMaterial("G4_Galactic");
    Vac->SetName("Vac");
    AddVisAttribute(Vac,G4Colour::White());
    
    G4Material* Nitrogen = new G4Material("Nitrogen", density= 1.251*mg/cm3, nelements= 1);
    Nitrogen ->  AddElement(N, 1);
    AddVisAttribute(Nitrogen,G4Colour::White());
    
    G4Material* Water = new G4Material("Water", density= 1.0*g/cm3, nelements= 2);
    Water -> AddElement(H, natoms=2);
    Water -> AddElement(O, natoms=1);
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    Water -> SetMaterialPropertiesTable(MPT);	// Attach this MPT to the water (kind of need it when we construct fluids - otherwise seg fault)
    
    G4Material* Water_TS = new G4Material("Water_TS", density= 1.0*g/cm3, nelements= 2, kStateLiquid, 293.15*kelvin); //(Thermal neutron scattering version)
    Water_TS->AddElement(elTSHWater, 1);
    Water_TS -> AddElement(O, natoms=1);
    MPT = new G4MaterialPropertiesTable();  // Allocate memory for a new Material Property Table
    Water_TS -> SetMaterialPropertiesTable(MPT);  // Attach this MPT to the water (kind of need it when we construct fluids - otherwise seg fault)
    
    G4Material* GdWater = new G4Material("GdLoadedWater", density= 1.0*g/cm3, nelements= 3);
    GdWater -> AddElement(H, fractionmass= 11.1*perCent);
    GdWater -> AddElement(O, fractionmass= 88.8*perCent);
    GdWater -> AddElement(Gd,fractionmass= 0.1*perCent);
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    GdWater -> SetMaterialPropertiesTable(MPT);	// Attach this MPT to the GdWater (kind of need it when we construct fluids - otherwise seg fault)
    
    //Borated Water : around 20kg of bore for 1m3 of water
    G4Material* BoratedWater_2pc = new G4Material("BoratedWater_2pc", density = 1.0*g/cm3, nelements= 3);
    BoratedWater_2pc -> AddElement(H, fractionmass= 10.9*perCent);
    BoratedWater_2pc -> AddElement(O, fractionmass= 87.1*perCent);
    BoratedWater_2pc -> AddElement(B,fractionmass= 2.*perCent);
    
    G4Material* Glass = new G4Material("Glass", density= 2.2*g/cm3, nelements= 2);
    Glass -> AddElement(Si, natoms=1);
    Glass -> AddElement(O,  natoms=2);
    
    // Would need at some point to be precisely defined, i.e. describe different kind of steel alloys.
    G4Material* Steel = new G4Material("Steel", density= 7.87*g/cm3,  nelements= 3);
    Steel -> AddElement(Fe, fractionmass=0.71);
    Steel -> AddElement(Cr, fractionmass=0.19);
    Steel -> AddElement(Ni, fractionmass=0.10);
    AddVisAttribute(Steel,0.0,0.5,1.0);
    
    auto* Iron = new G4Material("Iron", density= 7.874*g/cm3, nelements= 1);
    Iron->AddElement(Fe, natoms=1);
    AddVisAttribute(Iron,0.0,0.8,1.0);
    
    G4Material* Lead = new G4Material("Lead", density= 11.35*g/cm3, nelements= 1);
    Lead -> AddElement(Pb, natoms=1);
    AddVisAttribute(Lead,0.62,0.62,0.62);
    
    G4Material* Aluminium = new G4Material("Aluminium", density= 2.70*g/cm3, nelements= 1);
    Aluminium -> AddElement(Al, natoms=1);
    AddVisAttribute(Aluminium,0.84,0.84,0.84);
    
    G4Material* Copper = new G4Material("Copper", density= 8.96*g/cm3, nelements= 1);
    Copper -> AddElement(Cu, natoms=1);
    AddVisAttribute(Copper,1.0,0.5,0.0);
    
    G4Material* Tin = new G4Material("Tin", density= 7.28*g/cm3, nelements= 1);
    Tin -> AddElement(Sn, natoms=1);
    AddVisAttribute(Tin,0.57,0.57,0.57);

    G4Material* Tungsten = new G4Material("Tungsten", density= 19.25*g/cm3, nelements= 1);
    Tungsten -> AddElement(W, natoms=1);
    AddVisAttribute(Tungsten,1.0,0.5,0.0);
    
    G4Material* Beryllium = new G4Material("Beryllium", density= 1.848*g/cm3, nelements= 1);
    Beryllium -> AddElement(Be, natoms=1);
    AddVisAttribute(Beryllium,0.5,0.5,0.0);
    
    G4Material* Silicon = new G4Material("Silicon", density= 2.329*g/cm3, nelements =1);     // Density at ambiant temperature
    Silicon -> AddElement(Si, natoms=1);
    AddVisAttribute(Silicon,0.75,0.75,0.75);
    
    G4Material* Germanium = new G4Material("Germanium", density= 5.323*g/cm3, nelements =1); //Density at ambiant temperature
    Germanium -> AddElement(Ge, natoms=1);
    AddVisAttribute(Germanium,0.,1.0,1.0);
    
    G4Material* Zinc = new G4Material("Zinc", density= 7.134*g/cm3, nelements= 1); //Density at ambiant temperature
    Zinc -> AddElement(Zn, natoms=1);
    AddVisAttribute(Zinc,0.5,0.,0.);
    
    G4Material* CaWO4 = new G4Material("CaWO4", density= 6.06*g/cm3, nelements= 3); //ref http://www.sciencedirect.com/science/article/pii/S0370269305001796
    CaWO4 -> AddElement(Ca, natoms=1);
    CaWO4 -> AddElement(W, natoms=1);
    CaWO4 -> AddElement(O, natoms=4);
    AddVisAttribute(CaWO4,0.,0.5,0.);
    
    G4Material* Al2O3 = new G4Material("Al2O3", density= 3.98*g/cm3, nelements= 2); //TO CHECK
    Al2O3 -> AddElement(Al, natoms=2);
    Al2O3 -> AddElement(O, natoms=3);
    AddVisAttribute(Al2O3,0.,0.,0.5);
    
    //natural Li2W04(Mo) crystal
    G4Material* LWO = new G4Material("LWO", density= 4.39*g/cm3,  nelements= 4); //ref https://www.sciencedirect.com/science/article/pii/S0022024816306017
    LWO -> AddElement(W, fractionmass=0.664);
    LWO -> AddElement(O, fractionmass=0.251);
    LWO -> AddElement(Mo, fractionmass=0.030);
    LWO -> AddElement(Li, fractionmass=0.055);
    AddVisAttribute(LWO,0.,0.5,0.);
    
    // TODO mix properly water and fuel to get the average composition and density
    G4Material* Fuel = new G4Material("Fuel", density= 3*g/cm3, nelements= 5);
    Fuel->AddElement(U, natoms=3);
    Fuel->AddElement(Si, natoms=2);
    Fuel->AddElement(Al, natoms=1);
    Fuel->AddElement(H, natoms=2);
    Fuel->AddElement(O, natoms=1);
    AddVisAttribute(Fuel,1.0,0.5,0.0);
    
    // Acrylic is PMMA, of general formula (C5H8O2)n
    G4Material* Acrylic = new G4Material("Acrylic", density= 1.19*g/cm3, nelements= 3);
    Acrylic -> AddElement(C, natoms=5);
    Acrylic -> AddElement(H, natoms=8);
    Acrylic -> AddElement(O, natoms=2);
    
    G4Material* BlackAcryl = new G4Material("blackAcryl", density= 1.19*g/cm3, nelements= 3);
    BlackAcryl -> AddElement(C, natoms=5);
    BlackAcryl -> AddElement(H, natoms=8);
    BlackAcryl -> AddElement(O, natoms=2);
    
    // Teflon is PTFE, of general formula (C2F4)n
    G4Material* Teflon_PTFE = new G4Material("Teflon_PTFE", density=2.2*g/cm3, nelements= 2);
    Teflon_PTFE -> AddElement(C, natoms=2);
    Teflon_PTFE -> AddElement(F, natoms=4);
    
    // utter fiction, needed for ALS guide tube
    G4Material* Teflon_trans = new G4Material("Teflon_trans", density=2.2*g/cm3, nelements= 2);
    Teflon_trans -> AddElement(C, natoms=2);
    Teflon_trans -> AddElement(F, natoms=4);
    
    // to simulate a black wall...
    G4Material* BlackWall = new G4Material("BlackWall", density=2.2*g/cm3, nelements= 2);
    BlackWall -> AddElement(C, natoms=2);
    BlackWall -> AddElement(F, natoms=4);
    
    //Boron carbide
    G4Material* Boron_Carbide = new G4Material("Boron_Carbide", density=2.52*g/cm3, nelements= 2);
    Boron_Carbide -> AddElement(C, natoms=1);
    Boron_Carbide -> AddElement(B, natoms=4);
    AddVisAttribute(Boron_Carbide,1.0,1.0,0.3);
    
    //Polyvinyltoluene (density from Saint-Gobain BC-408)
    G4Material* PVT = new G4Material("PVT",density= 1.023*g/cm3, nelements= 2);
    PVT->AddElement(C, 9);
    PVT->AddElement(H, 10);
    AddVisAttribute(PVT,1.0,1.0,0.0);

    //Bronze
    G4Material* Bronze = new G4Material("Bronze", density= 8.8*g/cm3, nelements=2);
    Bronze->AddMaterial(Copper, 88*perCent);
    Bronze->AddMaterial(Tin,    12*perCent);
    AddVisAttribute(Bronze,0.8,0.5,0.2);

    /////////////////////////////////////////////////////////////
    //////////////// Polyethylene materials //////
    /////////////////////////////////////////////////////////////
    
    //Edit: Boron fraction was calculated by keeping constant H/C mass fraction ratio
    //Any oxygen elements in borated polyethylene plastics? [Matthieu - Jan 2018]
    
    G4Material* Pol = new G4Material("Pol",density= 0.96*g/cm3, nelements= 2);
    //    Pol->AddElement(H, fractionmass= 14.86*perCent);
    //    Pol->AddElement(C, fractionmass= 85.14*perCent);
    Pol->AddElement(H, 2);
    Pol->AddElement(C, 1);
    AddVisAttribute(Pol,1.0,1.0,0.0);
    
    //To be used when thermal neutron scattering package is activated in G4 - see for example http://hypernews.slac.stanford.edu/HyperNews/geant4/get/hadronprocess/1471.html
    G4Material* Pol_TS = new G4Material("Pol_TS",density= 0.96*g/cm3, nelements= 2, kStateSolid, 293.15*kelvin);
    //    Pol_TS->AddElement(elTSHPol, fractionmass= 14.86*perCent);
    //    Pol_TS->AddElement(C, fractionmass= 85.14*perCent);
    Pol_TS->AddElement(elTSHPol, 2);
    Pol_TS->AddElement(C, 1);
    AddVisAttribute(Pol_TS,1.0,1.0,0.0);
    
    
    // Polyethylene with boron at 1.75% - Has borated polyethylene any oxygen elements? FIXME
    G4Material* PolBor1_75pc = new G4Material("PolBor1_75pc",density= 0.99*g/cm3, nelements= 3);
    PolBor1_75pc->AddElement(H, fractionmass= 14.6*perCent);
    PolBor1_75pc->AddElement(C, fractionmass= 83.65*perCent);
    PolBor1_75pc->AddElement(B, fractionmass= 1.75 *perCent);
    AddVisAttribute(PolBor1_75pc,1.0,1.0,0.2);
    
    // Polyethylene with boron at 1.75% - Has borated polyethylene any oxygen elements? FIXME (Thermal neutron scattering version)
    G4Material* PolBor1_75pc_TS = new G4Material("PolBor1_75pc_TS",density= 0.99*g/cm3, nelements= 2, kStateSolid, 293.15*kelvin);
    //    PolBor1_75pc_TS->AddElement(elTSHPol, fractionmass= 14.6*perCent);
    //    PolBor1_75pc_TS->AddElement(C, fractionmass= 83.65*perCent);
    PolBor1_75pc_TS->AddMaterial(Pol_TS, fractionmass= 98.25*perCent);
    PolBor1_75pc_TS->AddElement(B, fractionmass= 1.75 *perCent);
    AddVisAttribute(PolBor1_75pc_TS,1.0,1.0,0.2);
    
    
    // Polyethylene with boron at 3% - Has borated polyethylene any oxygen elements? FIXME
    G4Material* PolBor3pc = new G4Material("PolBor3pc",density= 1.01*g/cm3, nelements= 3);
    PolBor3pc->AddElement(H, fractionmass= 14.424*perCent);
    PolBor3pc->AddElement(C, fractionmass= 82.586*perCent);
    PolBor3pc->AddElement(B, fractionmass= 3.00*perCent);
    AddVisAttribute(PolBor3pc,1.0,1.0,0.4);
    
    // Polyethylene with boron at 3% - Has borated polyethylene any oxygen elements? FIXME (Thermal neutron scattering version)
    G4Material* PolBor3pc_TS = new G4Material("PolBor3pc_TS",density= 1.01*g/cm3, nelements= 2, kStateSolid, 293.15*kelvin);
    //    PolBor3pc_TS->AddElement(elTSHPol, fractionmass= 14.424*perCent);
    //    PolBor3pc_TS->AddElement(C, fractionmass= 82.586*perCent);
    PolBor3pc_TS->AddMaterial(Pol_TS, fractionmass= 97.00*perCent);
    PolBor3pc_TS->AddElement(B, fractionmass= 3.00*perCent);
    AddVisAttribute(PolBor3pc_TS,1.0,1.0,0.4);
    
    
    // Polyethylene with boron at 3.5% - Has borated polyethylene any oxygen elements? FIXME
    G4Material* PolBor3_5pc = new G4Material("PolBor3_5pc",density= 1.01*g/cm3, nelements= 3);
    PolBor3_5pc->AddElement(H, fractionmass= 14.340*perCent);
    PolBor3_5pc->AddElement(C, fractionmass= 82.160*perCent);
    PolBor3_5pc->AddElement(B, fractionmass= 3.50 *perCent);
    AddVisAttribute(PolBor3_5pc,1.0,1.0,0.4);
    
    
    // Polyethylene with boron at 3.5% - Has borated polyethylene any oxygen elements? FIXME (Thermal neutron scattering version)
    G4Material* PolBor3_5pc_TS = new G4Material("PolBor3_5pc_TS",density= 1.01*g/cm3, nelements= 2, kStateSolid, 293.15*kelvin);
    //    PolBor3_5pc_TS->AddElement(elTSHPol, fractionmass= 14.340*perCent);
    //    PolBor3_5pc_TS->AddElement(C, fractionmass= 82.160*perCent);
    PolBor3_5pc_TS->AddMaterial(Pol_TS, fractionmass=96.50*perCent);
    PolBor3_5pc_TS->AddElement(B, fractionmass= 3.50 *perCent);
    AddVisAttribute(PolBor3_5pc_TS,1.0,1.0,0.4);
    
    
    // Polyethylene with boron at 5% - Has borated polyethylene any oxygen elements? FIXME (Thermal neutron scattering version)
    // density value from : https://www.ysplastics.com/wp-content/uploads/2021/09/YS-Material-Data-Sheet-for-PE-Borated-UH05.pdf) => implies d=0.978 for Pol when associated to Boron - d=0.978 is taken to estimate the density of other % borated Polyethylenes.
    G4Material* PolBor5pc_TS = new G4Material("PolBor5pc_TS",density= 1.03*g/cm3, nelements= 2, kStateSolid, 293.15*kelvin);
    PolBor5pc_TS->AddMaterial(Pol_TS, fractionmass=95.0*perCent);
    PolBor5pc_TS->AddElement(B, fractionmass= 5.00 *perCent);
    AddVisAttribute(PolBor5pc_TS,1.0,1.0,0.4);
    
    
    // Polyethylene with boron at 10% - Has borated polyethylene any oxygen elements? FIXME
    G4Material* PolBor10pc = new G4Material("PolBor10pc",density= 1.09*g/cm3, nelements= 2);
    //    PolBor10pc->AddElement(H, fractionmass= 13.374*perCent);
    //    PolBor10pc->AddElement(C, fractionmass= 76.626*perCent);
    PolBor10pc->AddMaterial(Pol, fractionmass=90.00*perCent);
    PolBor10pc->AddElement(B, fractionmass= 10.00 *perCent);
    AddVisAttribute(PolBor10pc,1.0,1.0,0.6);
    
    
    // Polyethylene with boron at 10% - Has borated polyethylene any oxygen elements? FIXME (Thermal neutron scattering version)
    G4Material* PolBor10pc_TS = new G4Material("PolBor10pc_TS",density= 1.09*g/cm3, nelements= 2, kStateSolid, 293.15*kelvin);
    //    PolBor10pc_TS->AddElement(elTSHPol, fractionmass= 13.374*perCent);
    //    PolBor10pc_TS->AddElement(C, fractionmass= 76.626*perCent);
    PolBor10pc_TS->AddMaterial(Pol_TS, fractionmass=90.00*perCent);
    PolBor10pc_TS->AddElement(B, fractionmass= 10.00 *perCent);
    AddVisAttribute(PolBor10pc_TS,1.0,1.0,0.4);
    
    // Polyethylene with natural lithium at 7.5% (http://johncaunt.com/products/lithium-polyethylene/)
    G4Material* PolLit7_5pc = new G4Material("PolLit7_5pc",density= 1.06*g/cm3, nelements= 2, kStateSolid, 293.15*kelvin);
    PolLit7_5pc->AddMaterial(Pol, fractionmass=92.44*perCent);
    PolLit7_5pc->AddElement(Li, fractionmass=7.56*perCent);
    AddVisAttribute(PolLit7_5pc,1.0,1.0,0.4);
    
    // Polyethylene with natural lithium at 7.5% (Thermal neutron scattering version)
    G4Material* PolLit7_5pc_TS = new G4Material("PolLit7_5pc_TS",density= 1.06*g/cm3, nelements= 2, kStateSolid, 293.15*kelvin);
    PolLit7_5pc_TS->AddMaterial(Pol_TS, fractionmass=92.44*perCent);
    PolLit7_5pc_TS->AddElement(Li, fractionmass=7.56*perCent);
    AddVisAttribute(PolLit7_5pc_TS,1.0,1.0,0.4);
    
    /////////////////////////////////////////////////////////////
    
    //concrete
    G4Material* Concrete = new G4Material("Concrete", density= 2.5*g/cm3, nelements= 6);
    Concrete->AddElement(O,  fractionmass= 52. *perCent);  // What ??? FIXME
    Concrete->AddElement(Si, fractionmass= 32.5*perCent); // What ??? FIXME
    Concrete->AddElement(Ca, fractionmass= 6.  *perCent);
    Concrete->AddElement(Na, fractionmass= 1.5 *perCent);
    Concrete->AddElement(Fe, fractionmass= 4.  *perCent);
    Concrete->AddElement(Al, fractionmass= 4.  *perCent);
    AddVisAttribute(Concrete,1.0,0.0,1.0);
    
    //Wood (very rough composition, taken from Wikipedia - density arbitrarily chosen to be < 1)
    G4Material* Wood = new G4Material("Wood", density= 0.7*g/cm3, nelements= 5);
    Wood->AddElement(C,  fractionmass= 50. *perCent);
    Wood->AddElement(O, fractionmass= 42*perCent);
    Wood->AddElement(H, fractionmass= 6.  *perCent);
    Wood->AddElement(Na, fractionmass= 1 *perCent);
    Wood->AddElement(Ca, fractionmass= 1.  *perCent);
    AddVisAttribute(Wood,1.0,0.0,1.0);
    
    //Rock  - taken from DC simu
    G4Material* Corundum = new G4Material("Corundum", density= 2.81*g/cm3, nelements= 2);
    Corundum->AddElement(Al,2);
    Corundum->AddElement(O,3);
    
    G4Material* Quartz = new G4Material("Quartz", density= 2.81*g/cm3, nelements= 2);
    Quartz->AddElement(Si, natoms=1);
    Quartz->AddElement(O, natoms=2);
    
    G4Material* IronOxide = new G4Material("IronOxide",density=5.745*g/cm3,nelements=2);
    IronOxide->AddElement(Fe,1);
    IronOxide->AddElement(O,1);
    
    G4Material* MgOxide = new G4Material("MgOxide",density=3.6*g/cm3,nelements=2);
    MgOxide->AddElement(Mg,1);
    MgOxide->AddElement(O,1);
    
    G4Material* KOxide = new G4Material("KOxide",density=2.35*g/cm3,nelements=2);
    KOxide->AddElement(K,2);
    KOxide->AddElement(O,1);
    
    G4Material* ChoozRock = new G4Material("ChoozRock", density=2.81*g/cm3, nelements=5);
    ChoozRock->AddMaterial(Quartz,fractionmass= 58.0*perCent);
    ChoozRock->AddMaterial(Corundum,fractionmass= 19.0*perCent);
    ChoozRock->AddMaterial(IronOxide,fractionmass= 17.0*perCent);
    ChoozRock->AddMaterial(MgOxide,fractionmass= 4.0*perCent);
    ChoozRock->AddMaterial(KOxide,fractionmass= 2.0*perCent);
    AddVisAttribute(ChoozRock,1.0,0.0,1.0);
    
    // Define the DC mu_metal material used for the individual PMT magnetic shield
    // From the Internet, density is about 8.6/8.8 for magnetic shielding metal
    // Real Mu-Metal (proprietary name) is about Ni77 Fe16 Cu5 Cr2,
    // but other shielding metal exists : for instance
    // Supermalloy (better than Mu-metal) is about Ni79 Fe16 Mo5 | JG, janv 2010 FIXME
    G4Material* Mu_metal = new G4Material("Mu_metal", density= 8.6*g/cm3, nelements= 3);
    Mu_metal -> AddElement(Ni, fractionmass= 0.79);
    Mu_metal -> AddElement(Fe, fractionmass= 0.16);
    Mu_metal -> AddElement(Mo, fractionmass= 0.05);
    
    G4Material* Tyvek = new G4Material("Tyvek", density= 0.96*g/cm3, nelements= 2);
    Tyvek -> AddElement(H, natoms=2);
    Tyvek -> AddElement(C, natoms=1);
    
    // --- VM2000 - copy of Tyvek:  (...-CH2-CH2-...)*n
    // only optical properties will be used
    // stolen from DC code, MF, Feb 2010.
    G4Material* VM2000 = new G4Material("VM2000", density = 1.29*g/cm3, nelements = 2);
    VM2000 -> AddElement(H, natoms=2);
    VM2000 -> AddElement(C, natoms=1);
    
    G4Material* Photocathode_mat = new G4Material("photocathode", density= 5.0*g/cm3, nelements= 1); // not used in GLG4 simulation
    Photocathode_mat -> AddElement(K, natoms=1);	// Wait, what ?
    
    ///////////////////////////////// Liquid components /////////////////////////////////

    // Liquid Nitrogen
    G4Material* LN2 = new G4Material("LN2", density=0.808*g/cm3, nelements=1, kStateLiquid, 77.*kelvin, 1.0*atmosphere);
    LN2->AddElement(N, natoms=1);
    AddVisAttribute(LN2,G4Colour::White());
    
    // -------------------------------- Mineral Oil  (CH2)n --------------------------------
    // given a representative chemical formulation (C20H44) for consistency with
    // the description of other materials (DM)
    // typical from the Aldrich specification
    G4Material* MineralOil = new G4Material("MineralOil", density= 0.84*g/cm3, nelements= 2);
    MineralOil -> AddElement(C, natoms=20);
    MineralOil -> AddElement(H, natoms=44);
    MineralOil ->SetChemicalFormula("OIL");	// Use the chemical formula as a useful label
    mol = C->GetA()*20 + H->GetA()*44;		// Calculate the molecular weight
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    MPT -> AddConstProperty("MOL",mol/g);		// Fill with the molecular weight
    MineralOil -> SetMaterialPropertiesTable(MPT);// Attach this MPT to the PC
    
    // ----------- Shell Mineral Oil "Ondina909" Added by Dario on May 1st 2007 ------------
    // given a representative chemical formulation (C20H44) for consistency with the description of other materials
    // Shell specification
    G4Material* Ondina909 = new G4Material("Ondina909", density= 0.825*g/cm3, nelements= 2);
    Ondina909 -> AddElement(C, natoms=20);
    Ondina909 -> AddElement(H, natoms=44);
    Ondina909 -> SetChemicalFormula("OIL");	// Use the chemical formula as a useful label
    mol = C->GetA()*20 + H->GetA()*44;		// Calculate the molecular weight
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    MPT -> AddConstProperty("MOL",mol/g);		// Fill with the molecular weight
    Ondina909 -> SetMaterialPropertiesTable(MPT);	// Attach this MPT to the PC
    
    // ------------------------------------ Dodecane ------------------------------------
    G4Material* Dodecane = new G4Material("Dodecane", density= 0.749*g/cm3, nelements= 2);
    Dodecane -> AddElement(C, natoms=12);
    Dodecane -> AddElement(H, natoms=26);
    Dodecane -> SetChemicalFormula("OIL");	// Use the chemical formula as a label
    mol = C->GetA()*12 + H->GetA()*26;		// Calculate the molecular weight
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    MPT -> AddConstProperty("MOL",mol/g);		// Fill with the molecular weight
    Dodecane -> SetMaterialPropertiesTable(MPT);	// Attach this MPT to the PC
    
    // ------------ Pseudo-cumene (C9 H12) also called 1,2,4-Trimethylbenzene ------------
    G4Material* Pseudocumene = new G4Material("PC", density= 0.8758 *g/cm3, nelements= 2);
    Pseudocumene -> AddElement(C, natoms=9);
    Pseudocumene -> AddElement(H, natoms=12);
    Pseudocumene -> SetChemicalFormula("AROMATIC");// Use the chemical formula as a label
    mol = C->GetA()*9 + H->GetA()*12;		// Calculate the molecular weight
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    MPT -> AddConstProperty("MOL",mol/g);		// Fill with the molecular weight
    Pseudocumene ->SetMaterialPropertiesTable(MPT);// Attach this MPT to the pseudocumene
    
    // ---------------------------------------- PXE ----------------------------------------
    G4Material* PXE = new G4Material("PXE", density= 0.99*g/cm3, nelements= 2);
    PXE -> AddElement(C, natoms=16);
    PXE -> AddElement(H, natoms=18);
    PXE -> SetChemicalFormula("AROMATIC");	// Use the chemical formula as a label
    mol = C->GetA()*16 + H->GetA()*18;		// Calculate the molecular weight
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    MPT -> AddConstProperty("MOL",mol/g);		// Fill with the molecular weight
    PXE -> SetMaterialPropertiesTable(MPT);	// Attach this MPT to the PXE
    
    // ---------------------------------------- LAB ----------------------------------------
    G4Material* LAB = new G4Material("LAB", density= 0.86*g/cm3, nelements= 2); // FIXME! Manufacturer data. needs to be measured!
    LAB -> AddElement(C, natoms=18);		// Average value! Slightly too high, should be 17.7 really
    LAB -> AddElement(H, natoms=30);		// Average value! Slightly too high, should be 29.4
    LAB -> SetChemicalFormula("AROMATIC");	// Use the chemical formula as a label
    mol = C->GetA()*18 + H->GetA()*30;		// Calculate the molecular weight
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    MPT -> AddConstProperty("MOL",mol/g);		// Fill with the molecular weight
    LAB -> SetMaterialPropertiesTable(MPT);	// Attach this MPT to the PXE
    
    // ---------------- PPO (C15 H11 N 0) also called DPO, 2,5-diphenyloxazole ----------------
    G4Material* PPO = new G4Material("PPO",density= 1.06 *g/cm3, nelements= 4);
    PPO -> AddElement(C, natoms=15);
    PPO -> AddElement(H, natoms=11);
    PPO -> AddElement(N, natoms=1);
    PPO -> AddElement(O, natoms=1);
    PPO -> SetChemicalFormula("FLUOR");		// Use the chemical formula as a label
    mol = C->GetA()*15 + H->GetA()*11 + N->GetA()*1 + O->GetA()*1;	// Calculate the molecular weight
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    MPT -> AddConstProperty("MOL",mol/g);		// Fill with the molecular weight
    PPO -> SetMaterialPropertiesTable(MPT);	// Attach this MPT to the PC
    
    // ---------------- BPO (C21 H15 N O) like PPO, with one more phenyl ring ----------------
    G4Material* BPO = new G4Material("BPO", density= 1.06 *g/cm3, nelements= 4);	// density set to the PPO value
    BPO -> AddElement(C, natoms=21);
    BPO -> AddElement(H, natoms=15);
    BPO -> AddElement(N, natoms=1);
    BPO -> AddElement(O, natoms=1);
    BPO -> SetChemicalFormula("FLUOR");		// Use the chemical formula as a label
    mol = C->GetA()*21 + H->GetA()*15 + N->GetA()*1 + O->GetA()*1;	// Calculate the molecular weight
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    MPT -> AddConstProperty("MOL",mol/g);		// Fill with the molecular weight
    BPO -> SetMaterialPropertiesTable(MPT);	// Attach this MPT to the PC
    
    
    // ---------------------------------------- Bis-MSB ----------------------------------------
    G4Material* BisMSB = new G4Material("Bis-MSB",density= 1.3*g/cm3, nelements= 2);	// density unknown
    BisMSB -> AddElement(C, natoms=24);
    BisMSB -> AddElement(H, natoms=22);
    BisMSB -> SetChemicalFormula("WLS");		// Use the chemical formula as a label
    mol = C->GetA()*24 + H->GetA()*22;		//Calculate the molecular weight
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    MPT -> AddConstProperty("MOL",mol/g);		// Fill with the molecular weight
    BisMSB -> SetMaterialPropertiesTable(MPT);	// Attach this MPT to the Bis-MSB
    
    /////////////////////////////////// Gd stuff ///////////////////////////////////
    
    // ------------------------ Gd-acac : Gd(acac)_3 + H2O (monohydrated) ------------------------
    G4Material* Gd_acac = new G4Material("Gd-acac",density= 1.3*g/cm3, nelements= 4);	// density unknown
    Gd_acac -> AddElement(C, natoms=15);		//3x5 from acac
    Gd_acac -> AddElement(H, natoms=23);		//3x7 from acac + 2 from water
    Gd_acac -> AddElement(O, natoms=7);		//3x2 from acac + 1 from water
    Gd_acac -> AddElement(Gd,natoms=1);
    Gd_acac -> SetChemicalFormula("GDCOMPLEX");	// Use the chemical formula as a label
    mol = C->GetA()*15 + H->GetA()*23 + O->GetA()*7 + Gd->GetA();	// Calculate the molecular weight
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    MPT -> AddConstProperty("MOL",mol/g);		// Fill with the molecular weight
    Gd_acac -> SetMaterialPropertiesTable(MPT);	// Attach this MPT to the Gd-acac
    
    // ---------------- Gd-dpm : Gd(dpm)_3 (assumed no water after sublimation) ----------------
    G4Material* Gd_dpm = new G4Material("Gd-dpm", density= 1.3*g/cm3, nelements= 4);	// density unknown
    Gd_dpm -> AddElement(C, natoms=33);		//3x11 from dpm
    Gd_dpm -> AddElement(H, natoms=57);		//3x19 from dpm
    Gd_dpm -> AddElement(O, natoms=6);		//3x2 from dpm
    Gd_dpm -> AddElement(Gd,natoms=1);
    Gd_dpm -> SetChemicalFormula("GDCOMPLEX");	// Use the chemical formula as a label
    mol = C->GetA()*33 + H->GetA()*57 + O->GetA()*6 + Gd->GetA();	// Calculate the molecular weight
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    MPT -> AddConstProperty("MOL",mol/g);		// Fill with the molecular weight
    Gd_dpm -> SetMaterialPropertiesTable(MPT);	// Attach this MPT to the Gd-acac
    
    // -------------------------------- Gd-cbx : Gd(TMHA)_3 --------------------------------
    G4Material* Gd_cbx = new G4Material("Gd-cbx", density= 1.3*g/cm3, nelements= 4);	// density unknown
    Gd_cbx -> AddElement(C, natoms=27);		//3x9 from TMHA
    Gd_cbx -> AddElement(H, natoms=51);		//3x17 from TMHA
    Gd_cbx -> AddElement(O, natoms=6);		//3x2 from TMHA
    Gd_cbx -> AddElement(Gd,natoms=1);
    Gd_cbx -> SetChemicalFormula("GDCOMPLEX");	// Use the chemical formula as a label
    mol = C->GetA()*27 + H->GetA()*51 + O->GetA()*6 + Gd->GetA();	// Calculate the molecular weight
    MPT = new G4MaterialPropertiesTable();	// Allocate memory for a new Material Property Table
    MPT -> AddConstProperty("MOL",mol/g);		// Fill with the molecular weight
    Gd_cbx -> SetMaterialPropertiesTable(MPT);	// Attach this MPT to the Gd-acac
    
    ////////////////////////////////// Scintillator ////////////////////////////////
#ifdef USE_SCINTILLATOR
    G4Material* BufferOil = ConstructOrganicFluid("BufferOil"); // always constructed
    G4Material* InnerVetoScintillator = ConstructOrganicFluid("InnerVetoLiquid");
    
    const ParamBase &db( ParamBase::GetDataBase() );
    //Ch00zing inner veto liquid
    G4int IV_liquidChoice = G4int (db["inner_veto_liquid"]);
    switch (IV_liquidChoice) {
        case 0: IVLiquid = BufferOil; break; // BufferOil is already constructed
        case 1:
            Water->GetMaterialPropertiesTable()->AddConstProperty("LIGHT_YIELD",0);
            IVLiquid = Water;
            break;
        case 2: IVLiquid = InnerVetoScintillator; break;
        default:
            G4Exception("GLG4Material::GLG4Material","1",FatalErrorInArgument,
                        scat(" Impossible inner veto liquid choice: ",IV_liquidChoice).c_str() );
    }
    AddVisAttribute(IVLiquid,0.1,0.,0.);
    
    cout<<"\n Used inner veto liquid: " << IVLiquid->GetName()<<endl;
    cout<<" Used inner veto liquid density: "<< IVLiquid->GetDensity()/(g/cm3)<<endl;
    cout<<" Used inner veto liquid light yield: "<< IVLiquid->GetMaterialPropertiesTable()->GetConstProperty("LIGHT_YIELD")<<endl;
#endif
}

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
// construct organic fluids: scintillators, oils... from the fluids_card.dat
G4Material* GLG4Material::ConstructOrganicFluid(const G4String& liq_name)
{
    const ParamBase &db ( ParamBase::GetDataBase() );  // -- database
    
    G4MaterialPropertiesTable *MPT_Liquid = new G4MaterialPropertiesTable();
    G4Material *Liquid=nullptr;
    G4String chemical_formula;
    G4int nelements=0;
    
    G4String aromatic_name, oil1_name, oil2_name, fluor_name, wls_name, Gd_name;
    G4Material *Aromatic=nullptr, *Oil1=nullptr, *Oil2=nullptr, *Fluor=nullptr, *WLS=nullptr, *Gd=nullptr;
    G4double AromaticGramPcc=0., Oil1GramPcc=0., Oil2GramPcc=0., FluorGramPcc=0., WLSGramPcc=0., GdGramPcc=0.;
    
    // Convert IDs to names
    switch ((G4int) db[liq_name + "_aromatic_id"]) {
        case 1:  aromatic_name = "PXE";	break;
        case 2:  aromatic_name = "PC";	break;
        case 3:  aromatic_name = "LAB";	break;
        default: aromatic_name = "none";
    }
    
    switch ((G4int) db[liq_name + "_oil1_id"]) {
        case 1:  oil1_name  = "Dodecane";	break;
        case 2:  oil1_name  = "MineralOil";	break;
        case 3:  oil1_name  = "Ondina909";	break;
        default: oil1_name  = "none";
    }
    
    switch ((G4int) db[liq_name + "_oil2_id"]) {
        case 1:  oil2_name  = "Dodecane";	break;
        case 2:  oil2_name  = "MineralOil";	break;
        case 3:  oil2_name  = "Ondina909";	break;
        default: oil2_name  = "none";
    }
    
    switch ((G4int) db[liq_name + "_fluor_id"]) {
        case 1:  fluor_name = "PPO";	break;
        case 2:  fluor_name = "BPO";	break;
        default: fluor_name = "none";
    }
    
    switch ((G4int) db[liq_name + "_wls_id"]) {
        case 1:  wls_name = "Bis-MSB";	break;
        case 2:  wls_name = "BPO";		break;
        default: wls_name = "none";
    }
    
    switch ((G4int) db[liq_name + "_Gd_id"]) {
        case 1:  Gd_name = "Gd-dpm";	break;
        case 2:  Gd_name = "Gd-acac";	break;
        case 3:  Gd_name = "Gd-cbx";	break;
        default: Gd_name = "none";
    }
    
    // Load single components and convert database concentrations in units of g/cm3
    if (aromatic_name!="none") {
        Aromatic = G4Material::GetMaterial(aromatic_name);
        AromaticGramPcc = db[liq_name + "_aromatic_conc"]*Aromatic->GetDensity(); //from volume fraction to g/cm3
        nelements++;
        if ( Aromatic == nullptr ) {
            G4Exception("GLG4Material::ConstructLiquidProperties","1",FatalException,
                        scat("Unable to locate Aromatic Material of liquid index: ",liq_name).c_str() );
        }
    }
    
    if (oil1_name!="none") {
        Oil1 = G4Material::GetMaterial(oil1_name);
        Oil1GramPcc = db[liq_name + "_oil1_conc"]*Oil1->GetDensity(); // from volume fraction to g/cm3
        nelements++;
        if ( Oil1 == nullptr ) {
            G4Exception("GLG4Material::ConstructLiquidProperties","2",FatalException,
                        scat("Unable to locate Oil1 Material of liquid index: ",liq_name).c_str() );
        }
    }
    
    if (oil2_name!="none") {
        Oil2 = G4Material::GetMaterial(oil2_name);
        Oil2GramPcc = db[liq_name + "_oil2_conc"]*Oil2->GetDensity(); // from volume fraction to g/cm3
        nelements++;
        if ( Oil2 == nullptr ) {
            G4Exception("GLG4Material::ConstructLiquidProperties","3",FatalException,
                        scat("Unable to locate Oil2 Material of liquid index: ",liq_name).c_str() );
        }
    }
    
    if (fluor_name!="none") {
        Fluor = G4Material::GetMaterial(fluor_name);
        FluorGramPcc = db[liq_name + "_fluor_conc"]*g/cm3;
        nelements++;
        if ( Fluor == nullptr ) {
            G4Exception("GLG4Material::ConstructLiquidProperties","4",FatalException,
                        scat("Unable to locate Fluor Material of liquid index: ",liq_name).c_str() );
        }
    }
    
    if (wls_name!="none") {
        WLS = G4Material::GetMaterial(wls_name);
        WLSGramPcc = db[liq_name + "_wls_conc"]*g/cm3;
        nelements++;
        if ( WLS == nullptr ) {
            G4Exception("GLG4Material::ConstructLiquidProperties","5",FatalException,
                        scat("Unable to locate WLS Material of liquid index: ",liq_name).c_str() );
        }
    }
    
    if (Gd_name!="none") {
        Gd = G4Material::GetMaterial(Gd_name);
        GdGramPcc = (Gd->GetMaterialPropertiesTable()->GetConstProperty("MOL")
                     * g/G4Element::GetElement("Gadolinium")->GetA())
        * db[liq_name + "_Gd_conc"] * g/cm3;
        nelements++;
        if ( Gd == nullptr ) {
            G4Exception("GLG4Material::ConstructLiquidProperties","6",FatalException,
                        scat("Unable to locate Gd Material of liquid index: ",liq_name).c_str() );
        }
    }
    
    // Calculating scintillator density (underlying assumptions are incorrect, but better than nothing ...)
    
    G4double NormVolume=1.;
    if(fluor_name!="none") { NormVolume += FluorGramPcc / Fluor->GetDensity(); }
    if(wls_name!="none")   { NormVolume += WLSGramPcc / WLS->GetDensity(); }
    if(Gd_name!="none")    { NormVolume += GdGramPcc / Gd->GetDensity(); }
    G4double density = (AromaticGramPcc + Oil1GramPcc + Oil2GramPcc + FluorGramPcc + WLSGramPcc + GdGramPcc) / NormVolume;
    
    //Construction by addition of components
    Liquid = new G4Material(liq_name,density,nelements);
    if (aromatic_name!="none") { Liquid->AddMaterial(Aromatic, AromaticGramPcc/(NormVolume*density)); }
    if (oil1_name!="none")     { Liquid->AddMaterial(Oil1, Oil1GramPcc/(NormVolume*density)); }
    if (oil2_name!="none")     { Liquid->AddMaterial(Oil2, Oil2GramPcc/(NormVolume*density)); }
    if (fluor_name!="none")    { Liquid->AddMaterial(Fluor, FluorGramPcc/(NormVolume*density)); }
    if (wls_name!="none")      { Liquid->AddMaterial(WLS, WLSGramPcc/(NormVolume*density)); }
    if (Gd_name!="none")       { Liquid->AddMaterial(Gd, GdGramPcc/(NormVolume*density)); }
    
    // Assign a chemical formula. This will be only used to keep track of the
    // original components (Geant4 doeas not store any information on the single
    // materials used to define a mixture)
    chemical_formula = aromatic_name+"+"+oil1_name+"+"+oil2_name+"+"+fluor_name+"+"+wls_name+"+"+Gd_name;
    
    // erase the 'none' from the chemical formula
    G4String::size_type pos;
    //consider case of "none" after good components ...
    while ((pos = chemical_formula.find("+none",0)) != G4String::npos) { chemical_formula.erase (pos,5); }
    
    //consider case of "none" before good components ...
    while ((pos = chemical_formula.find("none+",0)) != G4String::npos) { chemical_formula.erase (pos,5); }
    
    //now the chemical formula has been cleaned up
    //   const G4String chf = chemical_formula;  //Formula assigned to constant string chf
    Liquid->SetChemicalFormula(chemical_formula);
    // Write the concentrations of the single components in the MaterialPropertiesTable
    // Unit used for concentration is mg/cm3, equivalent to g/l
    
    // Fill with the composition
    if (aromatic_name!="none") { MPT_Liquid->AddConstProperty("CONCENTRATION_AROMATIC",AromaticGramPcc/(mg/cm3)); }
    if (oil1_name!="none")     { MPT_Liquid->AddConstProperty("CONCENTRATION_OIL1",Oil1GramPcc/(mg/cm3)); }
    if (oil2_name!="none")     { MPT_Liquid->AddConstProperty("CONCENTRATION_OIL2",Oil2GramPcc/(mg/cm3)); }
    if (fluor_name!="none")    { MPT_Liquid->AddConstProperty("CONCENTRATION_FLUOR",FluorGramPcc/(mg/cm3)); }
    if (wls_name!="none")      { MPT_Liquid->AddConstProperty("CONCENTRATION_WLS",WLSGramPcc/(mg/cm3)); }
    if (Gd_name!="none")       { MPT_Liquid->AddConstProperty("CONCENTRATION_GDCOMPLEX",GdGramPcc/(mg/cm3)); }
    
    // Retrieve the fetch-factor (used to arbitrarily re-normalize the photon statistics at production)
    // and retrieve the Light Yield from database
    G4double ly = db["fetch"]*db[scat(liq_name,"_LY")];
    
    MPT_Liquid->AddConstProperty("LIGHT_YIELD",ly);
    // Assign this MPT to the Target Scintillator material
    Liquid->SetMaterialPropertiesTable(MPT_Liquid);
    
    if (static_cast<G4int>(db["construction_verboselevel"])) {
        cout << "\n---- Dump "<<liq_name<<" composition: ----\n";
        cout << liq_name<<" light yield: "<<ly<<endl;
        cout << "Density: "<<Liquid->GetDensity()/(g/cm3)<<endl;
        Liquid->GetMaterialPropertiesTable()->DumpTable();
        cout << endl;
    }
    
    return Liquid;
}
