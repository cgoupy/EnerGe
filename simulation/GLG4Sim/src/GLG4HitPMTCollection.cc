/** @file GLG4HitPMTCollection.cc
    For GLG4HitPMTCollection class.
    
    This file is part of the GenericLAND software library.
    $Id: GLG4HitPMTCollection.cc,v 1.2 2005/03/29 21:13:29 gahs Exp $

    @author Glenn Horton-Smith, December 2004
*/

#include <GLG4HitPMTCollection.hh>
#include <GLG4HitPhoton.hh>

#include <algorithm>
#include <iostream>

using namespace std;

/** clear out AND DELETE HitPMTs (and HitPhotons) that were detected,
    resetting this HitPMTCollection to be empty */
void GLG4HitPMTCollection::Clear()
{
  for(size_t i=0; i<fPMT.size(); i++){
    fPMT[i]->Clear();
    delete fPMT[i];
  }
  fPMT.clear();
  fHitmap.clear();
}

/** find or make appropriate HitPMT, and DetectPhoton in that HitPMT */
void GLG4HitPMTCollection::DetectPhoton(GLG4HitPhoton* new_photon)
{
  GLG4HitPMT* hitpmtptr= GetPMT_ByID( new_photon->GetPMTID() );

  if( hitpmtptr != nullptr ) {
    // found a HitPMT with this ID
    hitpmtptr->DetectPhoton(new_photon);
  } else {
    // make a HitPMT with this ID
    fPMT.push_back( new GLG4HitPMT((short)new_photon->GetPMTID()) );
    fPMT.back()->DetectPhoton(new_photon);
    fHitmap.insert( make_pair( (short)new_photon->GetPMTID(), (GLG4HitPMT*)fPMT.back() ) );
  }
}

void GLG4HitPMTCollection::SortTimeAscending()
{ 
  for(size_t i=0; i<fPMT.size(); i++) { fPMT[i]->SortTimeAscending(); }
  sort(fPMT.begin(), fPMT.end(), GLG4HitPMT::Compare_HitPMTPtr_TimeAscending ); 
}

/** return the number of HitPMTs in internal collection */
int GLG4HitPMTCollection::GetEntries() const
{
  return fPMT.size();
}

/** return the i-th HitPMT in internal collection */
GLG4HitPMT* GLG4HitPMTCollection::GetPMT(int i) const
{
  return fPMT[i];
}

/** return pointer to HitPMT with given id if in collection,
    or nullptr if no such HitPMT is in collection */
GLG4HitPMT* GLG4HitPMTCollection::GetPMT_ByID(int id) const
{
  auto p = fHitmap.find((short)id);
  if( p != fHitmap.end() ) { return p->second; } // found a HitPMT with this ID
  else { return nullptr; } // no HitPMT
}

/// print out HitPMTs
void GLG4HitPMTCollection::Print(ostream &os) const
{
  for(size_t i=0; i<fPMT.size(); i++) { fPMT[i]->Print(os); }
}
