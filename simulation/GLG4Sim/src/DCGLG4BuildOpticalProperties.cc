// +======================================================================+
// +                                                                      +
// + Author ........: Dario Motta                                         +
// + Institute .....: C.E.A. Saclay                                       +
// + Creation date .: 20/12/2004                                          +
// + Last editing ..: 22/05/2007                                          +
// +                                                                      +
// + Subject .......: Routine to read the material table and create an    +
// +                  optical-data file filling optical properties        +
// +                  accordingly. Compared to the GLG4 software,         +
// +                  which reads from a "static" material datafile, this +
// +                  routine converts the user's material choise into    +
// +                  the corresponding optical properties, so that the   +
// +                  former can be changed and the optical properties    +
// +                  will change accordingly. This routine assumes you   +
// +                  have the required basic input data-files in ../data +
// +======================================================================+
// edited by Jonathan Gaffiot, Nov 2010 (add "als", 2 last arguments to BuildProperties)

#include <DCGLG4BuildOpticalProperties.hh>

#include <G4Material.hh>
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>

#include <Parameters.hh>

#include <ctype.h>
#include <fstream>

using namespace std;

G4int DCGLG4BuildOpticalProperties::BuildProperties(ofstream &os, const G4String& scint_name)
{
  MyTools tools;
  static const char funcname[]= "DCGLG4BuildOpticalProperties::BuildProperties";
  G4Material *CurrentMaterial=nullptr, *CurrentSubMaterial=nullptr;
  G4MaterialPropertiesTable *CurrentMPT=nullptr, *CurrentSubMPT=nullptr;
  G4String mixture, component[NCOMP]; //6 max number of components (conservative...)
  G4String dirmaterialdata, dirextinction, dirscint, dirreemission, dirsettings;
  G4String exlabel="molext", emlabel="em", flyieldlabel="fy", data_suffix=".txt", sep="_";
  G4String complabel, conc_prop;

  ostringstream oilindexstr;
  G4String DataFileName;
  ifstream DataFile;

  G4double lam[NDATAEXT],fluorabsp[NDATAEXT],wlsabsp[NDATAEXT];
  G4double ext[NCOMP][NDATAEXT],mu[NCOMP][NDATAEXT]; //up to 6 components, ndataext wavelengths
  G4double mutot[NDATAEXT],invmutot;
  G4double fluorfy[NDATAEXT],wlsfy[NDATAEXT],reemissionp[NDATAEXT];
  G4double muimpurity[NDATAEXT], l_bias[NDATAEXT];
  G4double c,mol;
  G4int start=0,end=0,strlen=0,compcount=0;
  G4int errorCount=0;
  G4int ioil, ifluor, iwls;
  G4int VerboseLevel; // options: 0,1,2,3
  G4bool fluoristhere, wlsisthere;

  constexpr G4int Nliq = 2;
  G4String liquids[Nliq] = {"BufferOil",scint_name};

// Define all needed directories

  dirmaterialdata=G4String(getenv("ENERGE_DATA"))+"/ScintComponents/";
  dirextinction = dirmaterialdata+"Extinction/";
  dirreemission = dirmaterialdata+"Reemission/";
  dirscint = dirmaterialdata+"Scintillation/";
  dirsettings = dirmaterialdata+"Settings/";

// Building optical properties
// Outermost loop: liquids loop (driven by "liquids" G4string array)

// Note: the algorithm is far from being optimized: all materials (PXE, Dodecane...) are
// opened and read at each cycle, regardless whether they have already been accessed
// at some previous cycle of the loop... This is to mantain the highest generality and
// simplicity, but can be improved...

  // -- database
  const ParamBase &db ( ParamBase::GetDataBase() );

  // -- retrieve verbosity
  VerboseLevel = (G4int) db["optics_verboselevel"];

//If required, calculate the bias attenuation lengths and store for future subtraction
  if(db["attlength_correction"]) tools.MakeLBias(l_bias,VerboseLevel);

  for(G4int l=0;l<Nliq;l++) {
    CurrentMaterial = G4Material::GetMaterial(liquids[l]);
    if(VerboseLevel > 0) {
     cout<<"\n=============================================================="<<endl;
     cout<<".......... Processing "<<CurrentMaterial->GetName()<<" .........."<<endl;
     cout<<"==============================================================\n\n";
    }
    CurrentMPT = CurrentMaterial->GetMaterialPropertiesTable();
    mixture = CurrentMaterial->GetChemicalFormula();
    compcount=0;

// Loop over the mixture name, to sort out all components
    do {
    end=mixture.find("+",start);
    strlen=end-start;
    component[compcount]=mixture.substr(start,strlen);
    if (VerboseLevel > 1) cout<<"Found component: "<<component[compcount]<<endl;
    start=end+1;
    compcount++;
    } while (end!=-1);
// end Loop mixture name

// Loop over the components (SubMaterials) of the current detector liquid
    ioil=0;
    for(G4int i=0;i<compcount;i++) {
     CurrentSubMaterial = G4Material::GetMaterial(component[i]);
     CurrentSubMPT = CurrentSubMaterial->GetMaterialPropertiesTable();
     complabel=CurrentSubMaterial->GetChemicalFormula(); //General definition of this component
     if(complabel=="OIL") {
      ioil++;
      oilindexstr<<complabel<<ioil;
      complabel=oilindexstr.str();
      oilindexstr.str(""); //clear the stream for the next use
     }
     if (VerboseLevel > 1) {
      cout<<"\n***************************************************"<<endl;
      cout<<"Processing "<<CurrentSubMaterial->GetName()<<" ..."<<endl;
      cout<<CurrentSubMaterial->GetName()<<" is registered as "<<complabel<<endl;
     }

     conc_prop="CONCENTRATION_"+complabel;
     c=(CurrentMPT->GetConstProperty(conc_prop))*(mg/cm3);
     mol=(CurrentSubMPT->GetConstProperty("MOL"))*g;
     if (VerboseLevel > 2) {
      cout<<"Concentration:    "<<c/(mg/cm3)<<" g/l"<<endl;
      cout<<"Molecular weight: "<<mol/g<<" g/mol"<<endl;
     }
     DataFileName=dirextinction+component[i]+sep+exlabel+data_suffix;
     if (VerboseLevel > 1) cout<<"\nFile to open: "<<DataFileName<<endl;

     DataFile.open(DataFileName.c_str());

     if (DataFile.is_open()) {
       if (VerboseLevel > 1) cout <<"File opened, reading "<<exlabel<<" data..."<<endl;
       for (G4int j=0;DataFile.good()&&j<NDATAEXT;j++) {
	 DataFile>>lam[j];lam[j]*=nm;
	 DataFile>>ext[i][j];ext[i][j]*=1000.*cm2; //units equivalent to mol*liter/(g*cm);
       }
     }
     else {
       cerr <<endl<<funcname<<" error. Unable to open file: "<<DataFileName<<endl;
       cerr <<"Proceeding..."<<endl;
       errorCount++;
     }

// wavelengths loop for the single component
     if (VerboseLevel > 1) cout<<"Calculating the partial attenuation lengths of "
                                 <<component[i]<<endl;
     if (VerboseLevel > 3) cout<<"\nlam (nm) | att. length (m)"<<endl;
     for (G4int j=0;j<NDATAEXT;j++) {
       mu[i][j]=(0.4343*mol/(c*ext[i][j]));
       if (VerboseLevel > 3) cout<<lam[j]/nm<<"         "<<mu[i][j]/m<<endl;
     }
// end wavelengths loop
     DataFile.close();
     DataFile.clear();
    }
// end components loop
    //ero qui
// loop to calculate the total attenuation length
    if (VerboseLevel > 0) {
     cout<<"\n==================================================="<<endl;
     cout<<"\nCalculating the total attenuation lengths"<<endl;
    }
    if (VerboseLevel > 2) cout<<"\nlam (nm) | att. length (m)"<<endl;
    for (G4int j=0;j<NDATAEXT;j++) {
     invmutot=0.;
     for (G4int i=0;i<compcount;i++) {
      invmutot+=1./mu[i][j];
     }
     mutot[j]=1./invmutot;
     if (VerboseLevel > 2) cout<<lam[j]/nm<<"        "<<mutot[j]/m<<endl;
    }

// Correct for the bias due to the lambda-variation of n
    if(db["attlength_correction"]) {
     if (VerboseLevel > 1) cout<<"\nCorrecting for the bias due to the variation of n"<<endl;
     if (VerboseLevel > 2) {
      cout<<"Corrected attenuation lengths (m):"<<endl;
      cout<<"\nlam (nm) | att. length (m)"<<endl;
     }
     for (G4int j=0;j<NDATAEXT;j++) {
      mutot[j]=1/(1/mutot[j]-1/l_bias[j]);
      if(mutot[j]<0) mutot[j] = 200000.; //check to make sure we don't crash
      if (VerboseLevel > 2) cout<<lam[j]/nm<<"        "<<mutot[j]/m<<endl;
     }
    }
// Add contribution impurities, if simulated ...
    if(db[liquids[l]+"_degradation"]) {
     if (VerboseLevel > 1) cout<<"\nAdding the contribution from impurities in "
                                 <<CurrentMaterial->GetName()<<endl;
     DataFileName=dirextinction+liquids[l]+"_att_impurity.data";
     DataFile.open(DataFileName.c_str());
      if (DataFile.is_open()) {
       if (VerboseLevel > 1) cout <<"File opened, reading impurity data..."<<endl;
       G4double lambda;
       for (G4int j=0;!DataFile.eof();j++) {
        DataFile>>lambda;
        DataFile>>muimpurity[j];muimpurity[j]*=mm;
       }
      }
      else {
       cerr <<endl<<funcname<<" error. Unable to open file: "<<DataFileName<<endl;
       cerr <<"Proceeding..."<<endl;
       errorCount++;
      }
     for (G4int j=0;j<NDATAEXT;j++) {
      invmutot=1./mutot[j];
      invmutot+=1./muimpurity[j];
      mutot[j]=1./invmutot;
      if (VerboseLevel > 2) cout<<lam[j]/nm<<"  "<<mutot[j]/m<<endl;
     }
     DataFile.close();
     DataFile.clear();
    }

// end loop to calculate the total attenuation length
    //ero qui 1
// loop to calculate the probability of absorption by FLUOR
   fluoristhere=0;
   ifluor=tools.GetComponentIndex(mixture, "FLUOR");
   if (ifluor!=-1) {
    fluoristhere=1;
    if (VerboseLevel > 1)
     cout<<"\nA FLUOR is defined for this liquid. Calculating the relative absorption probability...\n";

    for (G4int j=0;j<NDATAEXT;j++) {
     fluorabsp[j]=mutot[j]/mu[ifluor][j];
    }
   }
// end loop to calculate the probability of absorption by FLUOR

// loop to calculate the probability of absorption by WLS
   wlsisthere=0;
   iwls=tools.GetComponentIndex(mixture, "WLS");
   if (iwls!=-1) {
    wlsisthere=1;
    if (VerboseLevel > 1)
     cout<<"\nA WLS is defined for this liquid. Calculating the relative absorption probability...\n";
    if (VerboseLevel > 2) cout<<"\nlam (nm) | P_abs_FLUOR | P_abs_WLS"<<endl;

    for (G4int j=0;j<NDATAEXT;j++) {
     wlsabsp[j]=mutot[j]/mu[iwls][j];
     if (VerboseLevel > 2) cout<<lam[j]/nm<<"         "<<fluorabsp[j]<<"     "<<wlsabsp[j]<<endl;
    }
   }

// end loop to calculate the probability of absorption by WLS

// calculation of the gloabal re-emission probabilities

  if (VerboseLevel > 1) cout<<"\nCalculating the total re-emission probabilities"<<endl;

   // Processing fluor
   if (fluoristhere) { //The liquid has a primary fluor
    DataFileName=dirreemission+component[ifluor]+sep+flyieldlabel+data_suffix;
    if (VerboseLevel > 1) cout<<"\nReading fluorescence yields from database ... \nFile to open: "<<DataFileName<<endl;
    DataFile.open(DataFileName.c_str());
    if (DataFile.is_open()) {
     if (VerboseLevel > 1) cout <<"File opened, reading "<<flyieldlabel<<" data..."<<endl;
     G4int currentlam,prevlam=0,j=0;
     G4double currentfy=0.,prevfy=0.;
     while (!DataFile.eof()) {
      DataFile>>currentlam;
      DataFile>>currentfy;
      if(prevlam && (currentfy==prevfy)) {
       for (G4int k=0;k<currentlam-prevlam;k++) { fluorfy[j+k]=currentfy; }
       j+=currentlam-prevlam;
      } else {
       fluorfy[j]=currentfy;
       j++;
      }
      prevlam=currentlam;
      prevfy=currentfy;
     }
    } else {
     cerr <<endl<<funcname<<" error. Unable to open file: "<<DataFileName<<endl;
     cerr <<"Proceeding..."<<endl;
     errorCount++;
    }
    if (VerboseLevel > 2) {
     cout<<"lam (nm) | FY"<<endl;
     for (G4int k=0;k<NDATAEXT;k++){
      cout<<lam[k]/nm<<"        "<<fluorfy[k]<<endl;
     }
    }
    DataFile.clear();
    DataFile.close();
  }
   //ero qui 3
   // Processing wls

  if (wlsisthere) { //The liquid has a secondary fluor, too
   DataFileName=dirreemission+component[iwls]+sep+flyieldlabel+data_suffix;
   if (VerboseLevel > 1) cout<<"\nReading fluorescence yields from database ... \nFile to open: "<<DataFileName<<endl;
   DataFile.open(DataFileName.c_str());
   if (DataFile.is_open()) {
    if (VerboseLevel > 1) cout <<"File opened, reading "<<flyieldlabel<<" data..."<<endl;
    G4int currentlam,prevlam=0,j=0;
    G4double currentfy=0.,prevfy=0.;
    while (!DataFile.eof()) {
     DataFile>>currentlam;
     DataFile>>currentfy;
     if(prevlam && (currentfy==prevfy)) {
      for (G4int k=0;k<currentlam-prevlam;k++) { wlsfy[j+k]=currentfy; }
      j+=currentlam-prevlam;
     } else {
      wlsfy[j]=currentfy;
      j++;
     }
     prevlam=currentlam;
     prevfy=currentfy;
    }
   } else {
    cerr <<endl<<funcname<<" error. Unable to open file: "
           <<DataFileName<<endl;
    cerr <<"Proceeding..."<<endl;
    errorCount++;
   }
   if (VerboseLevel > 2) {
    cout<<"lam (nm) | FY"<<endl;
    for (G4int k=0;k<NDATAEXT;k++){
     cout<<lam[k]/nm<<"        "<<wlsfy[k]<<endl;
    }
   }
   DataFile.clear();
   DataFile.close();

   }

  // Final calculation re-emission probabilities
  if (VerboseLevel > 0) cout<<"\nCalculating the total re-emission probability..."<<endl;
  if(fluoristhere) { //otherwise no re-emission
   if (VerboseLevel > 2) cout<<"lam (nm) |  P"<<endl;
   for (G4int j=0;j<NDATAEXT;j++) {

//The formula below is correct if the input primary spectrum is the one measured
//in similar experimental conditions as the light yield measurement, where light
//typically crosses few mm before detection.
//If you miss this data and thus use the pure non-shifted fluor spectrum, then
//you have to correct the scintillator light yield to compensate for the fake
//light loss due to inefficient wavelength-shift within the first few mm
//of light propagation. This correction you have to evaluate yourself!
     reemissionp[j]=fluorabsp[j]*fluorfy[j]+wlsisthere*wlsabsp[j]*wlsfy[j];
    if (VerboseLevel > 2) cout<<lam[j]/nm<<"        "<<reemissionp[j]<<endl;
   }
  }
  //ero qui 4
// Write all Info into optics data-file

  // write Material Header
  os<<"\n################"<<endl;
  os<<"\nMATERIAL "<<"\""<<CurrentMaterial->GetName()<<"\""<<endl;

  // write Absorption lengths
    os<<"PROPERTY ABSLENGTH"<<endl;
    os<<"OPTION wavelength"<<endl;
    os<<"# UNITS  nm\\E mm"<<endl;
    os<<"200 0.1e-3  # (G4OpAbsorption / GetP.I.L. bug workaround)"<<endl;
    for (G4int j=0;j<NDATAEXT;j++) {
     os<<lam[j]/nm<<" "<<mutot[j]/mm<<endl;
    }
    os<<"800 0.1e-3  # (G4OpAbsorption / GetP.I.L. bug workaround)"<<endl;

  // load and write the primary scintillation spectrum
  if(fluoristhere) {
   G4double lambda,em; //Not necessary to store these in a global-scope vector
   if (VerboseLevel > 0) cout<<"\nLoading the primary emission spectrum..."<<endl;
   if(wlsisthere) //There can be fluor->wls non-radiative energy transfer
    DataFileName=dirscint+component[ifluor]+"+"+component[iwls]+sep+emlabel
    +data_suffix;
   else
    DataFileName=dirscint+component[ifluor]+sep+emlabel+data_suffix;

   if (VerboseLevel > 1) cout<<"\nFile to open: "<<DataFileName<<endl;
   DataFile.open(DataFileName.c_str());
    if (DataFile.is_open()) {
     if (VerboseLevel > 1) cout <<"File opened, reading "<<emlabel<<" data..."<<endl;
     os<<"\nPROPERTY SCINTILLATION"<<endl;
     os<<"# UNITS  nm\\E arbitrary"<<endl;
     os<<"OPTION dy_dwavelength"<<endl;
     for (G4int j=0;!DataFile.eof();j++) {
       DataFile>>lambda;lambda*=nm;
       DataFile>>em;
       os<<lambda/nm<<"  "<<em<<endl;
     }
    }
    else {
     cerr <<endl<<funcname<<" error. Unable to open file: "<<DataFileName<<endl;
     cerr <<"Proceeding..."<<endl;
     errorCount++;
    }
    DataFile.clear();
    DataFile.close();
   }
  // End Loading and writing the primary spectrum

  // load and write the secondary scintillation spectrum
  if(wlsisthere) {
   G4double lambda,em; //Not necessary to store these information in a global vector
   if (VerboseLevel > 0) cout<<"\nLoading the secondary emission spectrum..."<<endl;
   DataFileName=dirscint+component[iwls]+sep+emlabel+data_suffix;
   if (VerboseLevel > 1) cout<<"\nFile to open: "<<DataFileName<<endl;
   DataFile.open(DataFileName.c_str());
    if (DataFile.is_open()) {
     if (VerboseLevel > 1) cout <<"File opened, reading "<<emlabel<<" data..."<<endl;
     os<<"\nPROPERTY SCINTILLATION_WLS"<<endl;
     os<<"# UNITS  nm\\E arbitrary"<<endl;
     os<<"OPTION dy_dwavelength"<<endl;
     for (G4int j=0;!DataFile.eof();j++) {
       DataFile>>lambda;lambda*=nm;
       DataFile>>em;
       os<<lambda/nm<<"  "<<em<<endl;
     }
    }
    else {
     cerr <<endl<<funcname<<" error. Unable to open file: "<<DataFileName<<endl;
     cerr <<"Proceeding..."<<endl;
     errorCount++;
    }
    DataFile.clear();
    DataFile.close();
  }
  // End Loading and writing the secondary spectrum

  //ero qui 5
  // write Re-emission probabilities
  if (fluoristhere) {
   os<<"\nPROPERTY REEMISSION_PROB"<<endl;
   os<<"OPTION wavelength"<<endl;
   os<<"200 1.0  # Put this in for mysterious bug, shouldn't affect anything (LLH)"<<endl;
   for (G4int j=0;j<NDATAEXT;j++) {
    os<<lam[j]/nm<<" "<<reemissionp[j]<<endl;
   }
   os<<"800 0"<<endl;
  }

  //cout<<" meta build properties"<<endl;
  // write Fluor-WLS re-emission repartition
   if (wlsisthere) {
    os<<"\nPROPERTY FLUOR_REEMISSION_RATIO"<<endl;
    os<<"OPTION wavelength"<<endl;
    os<<"200 1.0"<<endl;
    for (G4int j=0;reemissionp[j];j++) {
     os<<lam[j]/nm<<" "<<fluorabsp[j]*fluorfy[j]/reemissionp[j]<<endl;
    }
    os<<"800 0"<<endl;
   }

   //cout<<" meta build properties1"<<endl;
  }
  //cout<<" meta build properties2"<<endl;
  //cout<<" fine build properties"<<endl;
// end liquids loop
  return errorCount;
}

int DCGLG4BuildOpticalProperties::MyTools::GetComponentIndex(G4String &mixture, G4String CompType)
{
  G4Material* Component=nullptr;
  G4String compname,itstype;
  G4int start=0,end=0,strlen=0,compcount=0;

// Loop over the mixture name, to sort out all components
  do {
    end=mixture.find("+",start);
    strlen=end-start;
    compname=mixture.substr(start,strlen);
    Component = G4Material::GetMaterial(compname);
    itstype=Component->GetChemicalFormula();
    start=end+1;
    if(CompType==itstype) return compcount; //return this index
    compcount++;
  } while (end!=-1);
// end Loop mixture name

  return -1; // Component not found...
}

void DCGLG4BuildOpticalProperties::MyTools::MakeLBias(G4double* l_bias,G4int VerboseLevel)
{
  if (VerboseLevel > 1) { cout<<"\nCalculating the bias attenuation lengths due to the variation of n\n"; }
  const ParamBase &db ( ParamBase::GetDataBase() );

  G4double lam_ref,l_cell,a_bias,b_bias,lambda;
  lam_ref = db["lam_ref_attlength"]*nm;
  l_cell = db["l_cell_attlength"]*mm;
  a_bias = db["a_bias_attlength"]/nm;
  b_bias = db["b_bias_attlength"]/(nm*nm);

  if (VerboseLevel > 2) { cout<<"\nWavelength |  L_bias\n\n"; }
  for (G4int j=0;j<NDATAEXT;j++) {
    lambda = (j+MINLAM)*nm;
    if (lambda==lam_ref) { l_bias[j] = 0.*mm; }
    else { l_bias[j]=0.4343*l_cell/((lambda-lam_ref)*(a_bias+b_bias*((lambda-lam_ref)))); }
    if (VerboseLevel > 2) { cout<<lambda/nm<<"          "<<l_bias[j]/m<<endl; }
  }

  return;
}
