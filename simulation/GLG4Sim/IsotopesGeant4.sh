#!/bin/bash

###########################################
#		DESCRIPTION               #
###########################################

#	This script takes in argument a text file of the form of mass16.txt at https://www-nds.iaea.org/amdc/  to put in the same folder
#	The second argument needed is the name of the atom from which you seek the isotopes masses.
#	Example of utilisation		./IsotopesGeant4 mass16.txt Zn

#	The result is a textfile called isotopesZn.txt containing the lines to be put in Geant4 in GLG4material.cc to declare the isotpes of Zn.

#	It reads the text file and decomposes each interesting lines and create an other line corresponding to C++ code in Geant4.



#	reading the parameters

nbParameters=$#

if [ $nbParameters -eq "0" ]
then
	echo "Error : no parameters ./IsotopesGeant4.sh database.txt atom A_isotopes separated by a space"
fi

file=$1
shift
atom=$1
shift


cat $file | grep $atom > temp.txt
sed -i -e "s/ /,/g" temp.txt

rm -f code_$atom.txt

echo "//$atom isotopes" >> code_$atom.txt

for i in "$@"
do
	isotope=$i
	while read line
	do

	#	CAUTION
	# using only 'cut -f' is not always adapted because of some lines containing more fields than others.
	# The only constant way independant of the line seems to be using a 'cut-c'
	# Caution with the number of caracters cutted, and the version of the filetext used.
	# This part may need some changes and is not really nicely written.

		Z=`echo $line | cut -c 10- | tr -s ',' | cut -f2 -d,`
		A=`echo $line | cut -c 15- | tr -s ',' | cut -f2 -d,`

		if [ $A = $isotope ]
		then
			atomicMass=`echo $line | cut -c 96- | tr -s ',' | cut -f2 -d,`
			decimals=`echo $line | cut -c 100- | tr -s ',' | cut -f2 -d, | sed "s/\.//g" `

			echo "	G4Isotope* $atom$A = new G4Isotope(\"$atom$A\", Z=$Z, A=$A, M=$atomicMass.$decimals*g/mole); // M from atomic mass evaluation 2016 " >> code_$atom.txt
		fi

	done < temp.txt
done


rm -f temp.txt

echo "opening the file using gedit (can be modified in the script) and then errasin the result file"

# Choose your text editor
gedit code_$atom.txt

rm -f code_$atom.txt

