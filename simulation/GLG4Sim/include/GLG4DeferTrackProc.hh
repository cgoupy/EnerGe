// This file is part of the GenericLAND software library.
// $Id: GLG4DeferTrackProc.hh,v 1.2 2005/03/29 21:13:23 gahs Exp $
//
// Process to limit step length to stay within event time
// and defer long tracks (AND tracks which start after event time) using
// defered particle "generator".
//
// Written: G. Horton-Smith, 29-Oct-2001
//
#ifndef GLG4DeferTrackProc_h
#define GLG4DeferTrackProc_h 1

#include <globals.hh>
#include <G4VProcess.hh>

////////////////////////////////////////////////////////////////

class GLG4PrimaryGeneratorAction;
class G4HadronCaptureProcess;

class GLG4DeferTrackProc : public G4VProcess 
{
public:
  GLG4DeferTrackProc(const G4String& processName="DeferTrackProc", G4ProcessType aType = fUserDefined);
  GLG4DeferTrackProc(GLG4DeferTrackProc&) = delete;
  GLG4DeferTrackProc& operator=(const GLG4DeferTrackProc& ) = delete;

  //with description
  virtual G4double PostStepGetPhysicalInteractionLength(const G4Track& track, G4double   previousStepSize, G4ForceCondition* condition);
  virtual G4VParticleChange* PostStepDoIt(const G4Track& , const G4Step&  );

public:
  //without description 
  //  no operation in  AtRestGPIL
  inline virtual G4double AtRestGetPhysicalInteractionLength(const G4Track& , G4ForceCondition*) { return -1.0; }
  //  no operation in  AtRestDoIt
  inline virtual G4VParticleChange* AtRestDoIt(const G4Track& , const G4Step& ) { return nullptr; }
  //  no operation in  AlongStepGPIL
  inline virtual G4double AlongStepGetPhysicalInteractionLength(const G4Track&, G4double, G4double, G4double&, G4GPILSelection*) { return -1.0; }
  //  no operation in  AlongStepDoIt
  inline virtual G4VParticleChange* AlongStepDoIt(const G4Track& , const G4Step& ) { return nullptr; }

private:
  GLG4PrimaryGeneratorAction* _generator;
};

#endif

