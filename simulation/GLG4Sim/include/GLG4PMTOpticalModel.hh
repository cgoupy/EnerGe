/** @file GLG4PMTOpticalModel.hh
    Defines a FastSimulationModel class for handling optical photon
    interactions with PMT: partial reflection, transmission, absorption,
    and hit generation.

    This file is part of the GenericLAND software library.
    $Id: GLG4PMTOpticalModel.hh,v 1.8 2007/08/28 20:35:17 gahs Exp $

    @author Glenn Horton-Smith, March 20, 2001.
    @author Dario Motta, Feb. 23 2005: Formalism light interaction with photocathode.
    @author Alexander Etenko, Nov. 01 2005: overloaded constructor added
*/

#ifndef GLG4PMTOpticalModel_hh__
#define GLG4PMTOpticalModel_hh__

#include <G4VFastSimulationModel.hh>
#include <G4UImessenger.hh>

#include <cmath>
#include <complex>

class G4UIcommand;
class G4UIdirectory;
class G4VPhysicalVolume;
class G4OpticalSurface;
class G4PhysicsOrderedFreeVector;
typedef G4PhysicsOrderedFreeVector G4MaterialPropertyVector;

class GLG4PMTOpticalModel : public G4VFastSimulationModel, public G4UImessenger
{
public:
  //-------------------------
  // Constructor, destructor
  //-------------------------
  GLG4PMTOpticalModel(G4String, G4VPhysicalVolume*);
  GLG4PMTOpticalModel(G4String, G4LogicalVolume*, G4OpticalSurface*);
  GLG4PMTOpticalModel(G4String) = delete;
  GLG4PMTOpticalModel() = delete;

  //------------------------------
  // Virtual methods of the base
  // class to be coded by the user
  //------------------------------

  G4bool IsApplicable(const G4ParticleDefinition&);
  G4bool ModelTrigger(const G4FastTrack &);
  void DoIt(const G4FastTrack&, G4FastStep&);

  static void ResetTotalIncidentPhoton() { TotalIncidentPhoton = 0; }
  static G4int GetTotalIncidentPhoton()  { return TotalIncidentPhoton; }

  // following two methods are for G4UImessenger
  void SetNewValue(G4UIcommand * command,G4String newValues);
  G4String GetCurrentValue(G4UIcommand * command);

private:
  // material property vector pointers, initialized in constructor,
  // so we don't have to look them up every time DoIt is called.
  // Note: The "MaterialPropertyVector"s are owned by the material, not us.
  G4MaterialPropertyVector * rindex_glass;        // function of photon energy
  G4MaterialPropertyVector * rindex_photocathode; // function of photon energy
  G4MaterialPropertyVector * kindex_photocathode; // n~ = n+ik, as in M.D.Lay
  G4MaterialPropertyVector * efficiency_photocathode; // necessary? Why not ? [D.M.]
  G4MaterialPropertyVector * effscale_photocathode; // (useful for fluctuation post-processing)
  G4MaterialPropertyVector * thickness_photocathode; // function of Z (mm)
  G4MaterialPropertyVector * Eff_vs_Rho_photocathode;  // f'n of sqrt(X^2+Y^2)
  G4MaterialPropertyVector * abs_norm;  // Absorbance at normal incidence (to scale QE);

  G4double global_QE_scaling; //a wavelength independent scaling
  G4bool   omit_effvsrho; //switch to control the (in)activation of effvsrho

  // interior solid (vacuum), also initialized in constructor,
  // so we don't have to look it up each time DoIt is called.
  // Note it is implicitly assumed everywhere in the code that this solid
  // is vacuum-filled and placed in the body with no offset or rotation.
  G4VSolid * inner1_solid;
  G4VPhysicalVolume * inner1_phys;

  // "luxury level" -- how fancy should the optical model be?
  G4int luxlevel;

  // verbose level -- how verbose to be (diagnostics and such)
  G4int VerboseLevel;

  // directory for commands
  static G4UIdirectory* fgCmdDir;

  // "current values" of many parameters, for efficiency
  // [I claim it is quicker to access these than to
  // push them on the stack when calling CalculateCoefficients, Reflect, etc.]
  // The following are set by DoIt() prior to any CalculateCoefficients() call.
  G4double photon_energy; // energy of current photon
  G4double wavelength;    // wavelength of current photon
  G4double n1;            // index of refraction of curr. medium at wavelength
  G4double n2, k2;       // index of refraction of photocathode at wavelength
  G4double n3;            // index of refraction of far side at wavelength
  G4double efficiency;    // efficiency of photocathode at wavelength (?)
  G4double thickness;     // thickness of photocathode at current position
  G4double cos_theta1;    // cosine of angle of incidence
  // The following are set by CalculateCoefficients()
  // and used by DoIt(), Refract(), and Reflect():
  G4double sin_theta1;    // sine of angle of incidence
  G4double sin_theta3;    // sine of angle of refraction
  G4double cos_theta3;    // cosine of angle of refraction
  G4double fR_s;           // reflection coefficient for s-polarized light
  G4double fT_s;           // transmission coefficient for s-polarized light
  G4double fR_p;           // reflection coefficient for p-polarized light
  G4double fT_p;           // transmission coefficient for p-polarized light

  static G4int TotalIncidentPhoton;

  G4double fMyZTolerance;  // for G4 compatibility, future flexibility

  // private methods
  void CalculateCoefficients(); // calculate and set fR_s, etc.
  void Reflect(G4ThreeVector &dir, G4ThreeVector &pol, G4ThreeVector &norm);
  void Refract(G4ThreeVector &dir, G4ThreeVector &pol, G4ThreeVector &norm);
  void Init(G4LogicalVolume* envelope_log, G4OpticalSurface* pc_opsurf);
  void Initialize_abs_norm();
  void CalculateRT(G4double lam, G4double n1, G4double n2,
                   G4double k2, G4double n3, G4double t);

  // complex mathematical functions
  inline G4complex carcsin(G4complex theta) { //complex sin^-1
    G4complex zi(0.,1.); return (1./zi)*(log(zi*theta+sqrt(1.-theta*theta)));
  }
  inline G4complex rfunc(G4complex ni, G4complex nj, G4complex ti, G4complex tj) {
    return (ni*cos(ti)-nj*cos(tj))/(ni*cos(ti)+nj*cos(tj));
  }
  inline G4complex gfunc(G4complex ni, G4complex nj, G4complex ti, G4complex tj) {
    return (ni*cos(ti))/(nj*cos(tj));
  }
  inline G4complex trfunc(G4complex ni, G4complex nj, G4complex ti, G4complex tj, G4complex tk) {
    return 2.*(ni*cos(ti))/(ni*cos(tj)+nj*cos(tk));
  }
};

#endif
