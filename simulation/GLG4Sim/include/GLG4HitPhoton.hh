#ifndef __GLG4HitPhoton_hh__
#define __GLG4HitPhoton_hh__
/** @file GLG4HitPhoton.hh
    Declares GLG4HitPhoton class and helper functions.
    
    This file is part of the GenericLAND software library.
    $Id: GLG4HitPhoton.hh,v 1.2 2005/03/29 21:13:24 gahs Exp $
    
    @author Glenn Horton-Smith
*/

#include <G4Types.hh>
#include <iostream>

/** GLG4HitPhoton stores information about a photon that makes a
    photoelectron in a PMT.  With count>1, it records multiple
    p.e. made at the same time at a PMT.

    The general contract for GLG4HitPhoton is as follows:
      - remember PMT ID, Time, position, KE, momentum, polarization, and count
      - provide Get/Set for all of the above, plus AddCount for count
      - initialize count=1, time & id = some invalid value

    This is almost the same "general contract" that was implemented
    for KLG4sim's KLHitPhoton by O. Tajima and G. Horton-Smith, but
    the code was rewritten for GLG4sim in December 2004.

    @author Glenn Horton-Smith
*/

class GLG4HitPhoton {
public:
  GLG4HitPhoton() { }

  inline void SetPMTID(G4int id) { fPMTID= id; }
  inline void SetTime(G4double t) { fTime= t; }
  void SetKineticEnergy(G4double KE);
  void SetWavelength(G4double wl);
  void SetPosition(G4double x, G4double y, G4double z);
  void SetMomentum(G4double x, G4double y, G4double z);
  void SetPolarization(G4double x, G4double y, G4double z);
  inline  void SetCount(G4int count)  { fCount= count; }
  inline void AddCount(G4int dcount) { fCount+= dcount; }

  inline G4int GetPMTID() const { return fPMTID; }
  inline G4double GetTime() const { return fTime; }
  G4double GetKineticEnergy() const;
  G4double GetWavelength() const;
  template <class T> inline void GetPosition(T &x, T &y, T &z) const;
  template <class T> inline void GetMomentum(T &x, T &y, T &z) const;
  template <class T> inline void GetPolarization(T &x, T &y, T &z) const;
  inline G4int GetCount() const { return fCount; }
  void Print(std::ostream &) const;

private:
  G4double fTime;        /// time of hit 
  G4int fPMTID;          /// ID number of PMT the HitPhoton hit
  G4float fKE;           /// kinetic energy 
  G4float fPosition[3];  /// x,y,z components of position
  G4float fMomentum[3];  /// x,y,z components of momentum (normalized?)
  G4float fPolarization[3]; /// x,y,z components of polarization
  G4int fCount;          /// count of photons, often 1
};

template <class T> inline void 
GLG4HitPhoton::GetPosition(T &x, T &y, T &z) const {
  x= fPosition[0];
  y= fPosition[1];
  z= fPosition[2];
}

template <class T> inline void 
GLG4HitPhoton::GetMomentum(T &x, T &y, T &z) const {
  x= fMomentum[0];
  y= fMomentum[1];
  z= fMomentum[2];
}

template <class T> inline void 
GLG4HitPhoton::GetPolarization(T &x, T &y, T &z) const {
  x= fPolarization[0];
  y= fPolarization[1];
  z= fPolarization[2];
}


/** comparison function for sorting GLG4HitPhoton pointers
 */
inline G4bool Compare_HitPhotonPtr_TimeAscending(const GLG4HitPhoton *a, const GLG4HitPhoton *b) {
  return a->GetTime() < b->GetTime();
}

#endif // __GLG4HitPhoton_hh__
