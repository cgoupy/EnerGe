// This file is part of the Double Chooz software library.
//
// Author: Dario Motta (CEA/Saclay)
// Last edited: Jul 20 2006
// edited by Jonathan Gaffiot, Nov 2010 (add "als", 2 last arguments to BuildProperties)

#ifndef DCGLG4BuildOpticalProperties_H
#define DCGLG4BuildOpticalProperties_H

#include <fstream>
#include <globals.hh>

class DCGLG4BuildOpticalProperties {
public:
  class MyTools {
   public:
    MyTools() {};
    int GetComponentIndex(G4String &mixture, G4String CompType); 
    void MakeLBias(G4double* l_bias, G4int VerboseLevel);
  };

  static int BuildProperties(std::ofstream &os, const G4String& scint_name);

private:
  constexpr static G4int MINLAM=340; //lower bound of the considered wavelength range (nm)
  constexpr static G4int MAXLAM=600; //upper bound of the considered wavelength range (nm)
  constexpr static G4int NDATAEXT=MAXLAM-MINLAM+1; //Number of extinction data (340 nm -> 600 nm with 1 nm step)
  constexpr static G4int NCOMP=6; // 6 max number of components (conservative...)
};

#endif
