// This file is part of the GenericLAND software library.
// $Id: GLG4InputDataReader.hh,v 1.2 2005/03/29 21:13:24 gahs Exp $
//
// GLG4InputDataReader.hh
// v.0 by Glenn Horton-Smith, Feb 12, 1999
// adapted for namespace support in Geant4.1, 4.2 -- 14-Jul-2000

#ifndef GLG4InputDataReader_H
#define GLG4InputDataReader_H 1

#include <iostream>
#include <globals.hh>

class GLG4InputDataReader {
public:
  class MyTokenizer {
  private:
    std::istream *isptr;
  public:
    MyTokenizer(std::istream &is): isptr(&is), nval(0.) {}

    enum { TT_EOF=-1, TT_STRING='a', TT_NUMBER='0' };

    int ttype;

    G4double nval;
    G4String sval;

    int nextToken(void);

    void dumpOn(std::ostream &os);
  };

  static int ReadMaterials(std::istream &is);
};

#endif // GLG4InputDataReader_H
