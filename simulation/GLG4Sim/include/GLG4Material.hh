#ifndef GLG4_MATERIAL_HH
#define GLG4_MATERIAL_HH 1

#include <G4Material.hh>
#include <G4Colour.hh>

#include <map>

class G4String;
class G4OpticalSurface;
class G4MaterialPropertiesTable;
class G4VisAttributes;

class GLG4Material
{
public:
  static void CreateMaterials() {
    if (theMaterials == nullptr) { theMaterials = new GLG4Material(); }
  }
  static GLG4Material* Get() {
    CreateMaterials();
    return theMaterials;
  }

  static G4Material* GetMaterial(const G4String& material) {
    CreateMaterials();
    return G4Material::GetMaterial(material);
  }

  static G4VisAttributes const * GetVisAttribute(G4Material* const material=nullptr) {
    CreateMaterials();
    if (mVis.count(material)) { return mVis.at(material); }
    return nullptr;
  }
  static G4VisAttributes const * GetVisAttribute(const G4String&  material) {
    CreateMaterials();
    if (mVis.count(G4Material::GetMaterial(material,true))) { return mVis.at(G4Material::GetMaterial(material)); }
    return nullptr;
  }

  inline G4OpticalSurface* GetPhotocathodeOpSurf() { return Photocathode_opsurf; }
  inline G4Material* GetTargetLiquid() const { return TargetLiquid; }
  inline G4Material* GetIVLiquid() const { return IVLiquid; }

  void UpdatePhotocathodeMPT();

private:
  GLG4Material();
  GLG4Material(const GLG4Material& ) = delete;

  G4Material* ConstructOrganicFluid(const G4String& liq_name);
  void AddVisAttribute(G4Material* material, const G4Colour& colour);
  inline void AddVisAttribute(G4Material* material, G4double red, G4double green, G4double blue) {
    AddVisAttribute(material,G4Colour(red,green,blue));
  }

  static GLG4Material* theMaterials;
  static std::map<G4Material*, G4VisAttributes*> mVis;

  G4Material* TargetLiquid=nullptr;
  G4Material* IVLiquid=nullptr;
  G4OpticalSurface* Photocathode_opsurf=nullptr;
};

#endif // GLG4_MATERIAL_HH
