// This file is part of the GenericLAND software library.
// $Id: GLG4PMTSD.hh,v 1.2 2005/03/29 21:13:25 gahs Exp $
//
//  GLG4PMTSD.hh
//
//   Records total number of hits and "waveform" on each PMT,
//   with no PMT TTS or digitizer bandwidth effects included.
//   (Convolve the hit waveform with the TTS+digitizer single-hit function
//   to get the resulting digitized waveform.)
//
//   Bypasses Geant4 hit collection mechanism.
//
//   Author:  Glenn Horton-Smith, 2000/01/28 
//

#ifndef GLG4PMTSD_h
#define GLG4PMTSD_h 1

#include <G4VSensitiveDetector.hh>

class G4Step;
class G4HCofThisEvent;
class G4TouchableHistory;

class GLG4PMTSD : public G4VSensitiveDetector
{
protected:
  G4int max_pmts;
  G4int pmt_no_offset;
  G4int my_id_pmt_size;

public:
// member functions
  virtual ~GLG4PMTSD() {}
  GLG4PMTSD(G4String name, G4int max_pmts=16, G4int pmt_no_offset=0, G4int my_id_pmt_size= -1);
  GLG4PMTSD(const G4VSensitiveDetector& ) = delete;
  GLG4PMTSD(G4String ) = delete;

  inline virtual void Initialize(G4HCofThisEvent* ) {}
  inline virtual void EndOfEvent(G4HCofThisEvent* ) {}
  inline virtual void Clear() {}
  inline virtual void DrawAll() {}
  inline virtual void PrintAll() {}

  void SimpleHit( G4int ipmt,
		  G4double time,
		  G4double kineticEnergy,
		  const G4ThreeVector & position,
		  const G4ThreeVector & momentum,
		  const G4ThreeVector & polarization,
		  G4int iHitPhotonCount );

protected:
  // NOTE: ProcessHits should never be called anymore, because
  // hits are decided by GLG4PMTOpticalModel, which calls GLG4PMTSD::SimpleHit()
  // in order to avoid having to create a bogus G4Step object.
  ////////////////////////////////////////////////////////////////
  inline virtual G4bool ProcessHits(G4Step* , G4TouchableHistory* ) { return false; }

  G4int VerboseLevel=0;
};

#endif
