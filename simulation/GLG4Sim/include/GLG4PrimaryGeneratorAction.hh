// This file is part of the GenericLAND software library.
// $Id: GLG4PrimaryGeneratorAction.hh,v 1.5 2009/09/16 20:50:19 gahs Exp $
//
// new GLG4PrimaryGeneratorAction.hh by Glenn Horton-Smith, August 3-17, 2001

#ifndef __GLG4PrimaryGeneratorAction_hh__
#define __GLG4PrimaryGeneratorAction_hh__ 1
////////////////////////////////////////////////////////////////
// GLG4PrimaryGeneratorAction
////////////////////////////////////////////////////////////////

#include <G4Types.hh>
#include <G4VUserPrimaryGeneratorAction.hh>  // for user primary vertex gen.
#include <G4SystemOfUnits.hh>

#include <vector>
#include <map>

class GLG4PrimaryGeneratorMessenger;
class GLG4Material;
class G4Event;
class G4Track;
class G4String;
class GLG4VPosGen;
class GLG4VVertexGen;

class GLG4PrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
public:
  enum {
    kGeneratorTriggerNormal=0,
    kGeneratorTriggerPileupOnly=1,
    kGeneratorTriggerDelay=2
  };
  enum {
    NbPosGenCodes = 7,
    NbVertexGenCodes= 15
  };

public:
  virtual ~GLG4PrimaryGeneratorAction();

  virtual void GeneratePrimaries(G4Event * argEvent); // generate primary particles

  void DeferTrackToLaterEvent(const G4Track * track); // postpone to later
  void NotifyTimeToNextStackedEvent( G4double t ); // note time to stacked evt

  G4int NewEventGenerator(G4int iPos, G4int iVtx, G4double rate, G4int trigger = kGeneratorTriggerNormal);

  G4int GetTypeOfCurrentEvent();
  inline G4double GetUniversalTime()                  const { return myUniversalTime; }
  inline G4double GetUniversalTimeSincePriorEvent()   const { return myUniversalTimeSincePriorEvent; }
  inline G4double GetEventRate(G4int i)               const { return myEventRate[i]; }
  inline G4int    GetEventTriggerCondition(G4int iev) const { return myEventTriggerCondition[iev];}
  inline G4double GetEventWindow()                    const { return myEventWindow; }
  inline G4double GetChainClip()                      const { return myChainClip; }
  inline G4bool   GetHzMode()                         const { return myHzMode; }

  inline void SetEventWindow(G4double argEventWindow) { myEventWindow = argEventWindow; }
  inline void SetChainClip(G4double argChainClip) { myChainClip = argChainClip; }
  inline void SetHzMode(G4bool useHz) { myHzMode = useHz; }

  static const char *GetPositionCodeName(G4int argPosCode)     { return thePositionCodeNames[argPosCode]; }
  static const char *GetVertexCodeName(G4int argVertexCode)    { return theVertexCodeNames[argVertexCode]; }
  static G4int GetPositionCodeForEventType(G4int argEventType) { return theEventGeneratorCodes[argEventType].poscode; }
  static G4int GetVertexCodeForEventType(G4int argEventType)   { return theEventGeneratorCodes[argEventType].vertexcode; }
  static G4int GetNbEventGenerator()                           { return theEventGeneratorCodes.size(); }

  static G4String        GetEventTypeName(G4int argEventType);
  static GLG4VVertexGen* GetVertexGenerator(G4int i)   { return theVertexGenerators[i]; }
  static GLG4VPosGen*    GetPositionGenerator(G4int i) { return thePositionGenerators[i]; }

  static GLG4PrimaryGeneratorAction* GetTheGLG4PrimaryGeneratorAction() {
    if ( theGLG4PrimaryGeneratorAction == nullptr ) { theGLG4PrimaryGeneratorAction = new GLG4PrimaryGeneratorAction(); }
    return theGLG4PrimaryGeneratorAction;
  }

private:
  // singleton -> ctor private
  GLG4PrimaryGeneratorAction();
  static GLG4PrimaryGeneratorAction* theGLG4PrimaryGeneratorAction;

  G4double HzOrBqFactor() const;

  GLG4PrimaryGeneratorMessenger *myMessenger;

  G4bool   myHzMode = false;
  G4int iNullPos, iDelayVtx, /*iDelayPos,*/ iDelayEvt;
  G4int    myTypeOfCurrentEvent = -1;
  G4double myUniversalTime = 0.;
  G4double myUniversalTimeSincePriorEvent = 0.;
  G4double myEventWindow = 1000.*ns;
  G4double myChainClip = 100.0*second;
  std::vector<G4int>    myEventTriggerCondition;
  std::vector<G4double> myEventRate;
  std::vector<G4double> myTimeToNextEvent;

  static GLG4VVertexGen* theVertexGenerators[NbVertexGenCodes];
  static GLG4VPosGen*    thePositionGenerators[NbPosGenCodes];
  static const char*     theVertexCodeNames[NbVertexGenCodes];
  static const char*     thePositionCodeNames[NbPosGenCodes];

  typedef struct codepair_s { G4int poscode, vertexcode; } codepair_t;
  static std::vector<codepair_t> theEventGeneratorCodes;
  std::map<G4int, G4int> mConstantCode;
};

#endif
