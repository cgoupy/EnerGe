#ifndef __GLG4VertexGen_h__
#define __GLG4VertexGen_h__ 1
/** @file
 Declares global vertex generator classes for primary events,
 Heavily adapated from the GenericLAND software library
 
 @author G.Horton-Smith 2001
 @author M. Vivier 2017
 */

#include <Rand.hh>
#include <String.hh>

#include <G4ios.hh>
#include <G4Types.hh>
#include <G4String.hh>
#include <G4ThreeVector.hh>
#include <G4SystemOfUnits.hh>
#include <G4VUserPrimaryVertexInformation.hh> // needed for user vertex info added by stacker
#include <G4GeneralParticleSource.hh>

#include <RtypesCore.h>
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>

#include <cstdio>  // for FILE
#include <set>     // for multiset
#include <algorithm>
#include <vector>
#include <map>

class G4Event;
class G4Track;
class G4PrimaryVertex;
class G4ParticleDefinition ;
class GLG4PrimaryGeneratorAction;

//Type alias for Random<G4ThreeVector>
using RandG4_t = Random<G4ThreeVector>;

// some identifiers of decay processes
namespace EneGen {
enum DecayType {
    Error,
    Alpha,
    BetaMinus,
    BetaPlus,
    Gamma,
    X,
    CE,
    Auger
};

enum AtomicLevel {
    NONE,
    K,
    L,
    M,
    continuum
}; // shells N,O,P,Q between shell M and continuum are neglected
}
using namespace EneGen;

/** Virtual base class for vertex generators */
class GLG4VVertexGen {
public:
    virtual ~GLG4VVertexGen() {}
    GLG4VVertexGen(const G4String& name="Vertex generator: virtual");
    virtual void GeneratePrimaryVertex( G4Event *argEvent )=0; // adds vertices.
    virtual void SetState( G4String newValues ) = 0; // sets filename or other information needed by vertex generator
    virtual G4String GetState() = 0; // returns the current state information in a form that can be understood by SetState (and, hopefully, a well-informed human)
    
    G4ThreeVector GetTargetDir(G4PrimaryVertex* vertex, G4String TargetVolName);
    virtual void SetDirectionTarget(G4PrimaryVertex* vertex)=0;
    virtual void ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir) =0;
    virtual inline G4String GetAniType() {return AniType;}
    
    
protected:
    G4String Name; // used for ParamBase key prefix
    G4int VerboseLevel = 0;
    G4String AniType;
};

/** Vertex generator that can generate a primary vertex with one or more
 particles of a given type, direction, energy, and polarization.
 Allows for randomly isotropic direction and random transverse polarization
 of spin-1, mass=0 particles */
class GLG4VertexGen_Gun : public GLG4VVertexGen {
public:
    virtual ~GLG4VertexGen_Gun() {}
    GLG4VertexGen_Gun(const G4String& name="Vertex generator: Gun");
    virtual void GeneratePrimaryVertex(G4Event *argEvent ); // generates a primary vertex with given particle type, direction, energy, and consistent polarization.
    virtual void SetState(G4String newValues );
    // format: particle_name  dir_x dir_y dir_z  kinetic_energy  polx poly polz
    // If dir_x==dir_y==dir_z==0, the directions are isotropic.
    // If particle has mass==0 and spin==1, final polarization will be
    // projected into plane perpendicular to momentum and made a unit vector;
    // if polarization has zero magnitude, a polarization is chosen randomly.
    
    virtual G4String GetState(); // returns current state formatted as above
    
    virtual void SetDirectionTarget(G4PrimaryVertex* vertex);
    virtual inline G4String GetAniType() {return AniType;}
    virtual void ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir);
    
public:
    // the following useful static const data should be universally accessible
    // (Copied from the G4IonTable source code, where it is privatized
    // with no accessor functions.)
    static const Int_t numberOfElements = 110;
    static const char * theElementNames[numberOfElements];
    
private:
    G4int Multiplicity=1;
    G4double Ke=0., Ani=0.;
    G4double TJit=0., SJit=0., TCont=0.;
    G4ThreeVector Polar, P;
    G4String VolumeTarget = "NONE";
    G4String AniType{"zenith"};
    
    G4ParticleDefinition *Particle=nullptr;
};

/** vertex generator that generates a primary vertex based on
 information from an ASCII stream.
 The ASCII stream contains information in a HEPEVT-like style.
 */
class GLG4VertexGen_HEPEvt : public GLG4VVertexGen {
public:
    virtual ~GLG4VertexGen_HEPEvt()  { Close(); }
    GLG4VertexGen_HEPEvt(const G4String& name="Vertex generator: HEPEvt"): GLG4VVertexGen(name) {}
    virtual void GeneratePrimaryVertex( G4Event *argEvent );
    // Generates a primary vertex based on information from an ASCII stream.
    // The ASCII stream contains information in a HEPEVT style.
    virtual void SetState( G4String newValues );
    // The argument is a Perl "open" style filename argument:
    // if the filename ends with '|', the filename is interpreted as
    // a command which pipes output to us; otherwise, if the filename begins
    // with '<' or an ordinary character, the file is opened for input.
    // The "pipe" style can be used to read a gzip'ped file or to start
    // a program which feeds events to us directly.
    virtual G4String GetState();
    // returns current state formatted as above
    
    virtual void SetDirectionTarget(G4PrimaryVertex* vertex);
    virtual void ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir);
    
    void Open(const char *argFilename);
    void GetDataLine(char *buffer, size_t size);
    void Close();
    
    enum { kIonCodeOffset = 9800000,       // nuclei have codes like 98zzaaa
        kPDGcodeModulus=10000000,       // PDG codes are 7 digits long
        kISTHEP_ParticleForTracking=1,  // only ISTHEP==1 are for tracking
        kISTHEP_InformatonMin=100,      // 100 and above are "informatons"
        kISTHEP_Max=213                 // <= MAXINT / kPDGcodeModulus
    };
private:
    FILE *   File = nullptr;
    G4String FileName{"(no filename)"};
    G4ThreeVector Offset;
    G4bool isPipe = false;
};

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
//This generator stacks several tracks in one primary vertex
//It is used when the 'GLG4_DeferTrack' process is activated - MV May 2020
class GLG4VertexGen_Stack : public GLG4VVertexGen {
public:
    virtual ~GLG4VertexGen_Stack() {}
    GLG4VertexGen_Stack(GLG4PrimaryGeneratorAction* argGLG4PGA,
                        const G4String& name="Vertex generator: Stack"):
    GLG4VVertexGen(name), fGLG4PGA(argGLG4PGA) {}
    virtual void GeneratePrimaryVertex( G4Event *argEvent );// Generates a primary vertex from stacked tracks.
    virtual void SetState( G4String newValues ); // Does nothing.
    virtual G4String GetState() { return G4String(); } // returns ""
    
//    virtual void SetDirectionTarget(G4PrimaryVertex* vertex) {(void) vertex; return;}
//    virtual void ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir){(void) vertex; (void) Dir; return;}
    
    void StackIt(const G4Track* track);
    
    G4int GetStackSize() { return theStack.size(); }
    
    struct LessThan_for_VertexTime {
        G4bool operator()(G4PrimaryVertex* a, G4PrimaryVertex* b) const { return ( a->GetT0() < b->GetT0() ); }
    };
    
    class StackedPrimaryVertexInfo : public G4VUserPrimaryVertexInformation {
    public:
        G4int _parentEvent;   ///< G4 Event number of "parent"
        double _parentUT;   ///< GLG4PGA "universal time" of "parent"
        
    public:
        virtual void Print() const;
    };
    
private:
    GLG4PrimaryGeneratorAction* fGLG4PGA;
    std::multiset<G4PrimaryVertex*,LessThan_for_VertexTime> theStack;
};

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
class GLG4VertexGen_GPS : public GLG4VVertexGen {
public:
    virtual ~GLG4VertexGen_GPS() {}
    GLG4VertexGen_GPS(const G4String& name="Vertex generator: GPS") : GLG4VVertexGen(name) {}
    
    inline virtual void GeneratePrimaryVertex( G4Event *argEvent ) {
        particleGun.GeneratePrimaryVertex(argEvent);
    }
    // generates a primary vertex with given particle type, direction, energy,
    // and consistent polarization.
    inline virtual void SetState( G4String ) { }
    inline virtual G4String GetState() { particleGun.ListSource(); return "\n"; }
    
    
    virtual void SetDirectionTarget(G4PrimaryVertex* vertex);
    virtual void ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir);
    
private:
    G4GeneralParticleSource particleGun;
};

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
class GLG4SpecPart {
public:
    GLG4SpecPart(G4double E, G4double I): Energy(E), Intensity(I) {}
    virtual ~GLG4SpecPart() {}
    
public:
    inline static G4bool CompIntensityToDouble(GLG4SpecPart* const a, G4double d) { return a->Intensity < d; }
    inline static G4bool SortByIntensity(GLG4SpecPart* const a, GLG4SpecPart* const b) { return a->Intensity < b->Intensity; }
    
    inline virtual G4double ShootEnergy()  const { return Energy; }
    inline virtual G4double GetEnergy()    const { return Energy; }
    inline virtual G4double GetIntensity() const { return Intensity; }
    
    inline void AddToIntensity(G4double I) { Intensity += I; }
    inline void RenormalizeIntensity(G4double factor) { Intensity /= factor; }
    
    inline virtual void Dump() const {
        G4cout << "  " << Energy << " keV\t =>  " << Intensity*100 << " %\n";
    }
    
protected:
    G4double Energy;
    G4double Intensity;
    
protected:
    GLG4SpecPart() {}
};

using GLG4EneBin = GLG4SpecPart;
using GLG4Branch = GLG4SpecPart;

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
class GLG4Decay : public GLG4Branch
{
public:
    virtual ~GLG4Decay() {}
    GLG4Decay(DecayType type, G4double E, G4double I, G4int start_lvl, G4int end_lvl, AtomicLevel ato_lvl);
    GLG4Decay(G4double E, G4double I) = delete; // prevent decay with no type, no levels
    GLG4Decay() = delete;
    
public:
    inline DecayType GetDecayType() const { return theType; }
    inline G4ParticleDefinition* GetParticle() const { return theParticle; }
    inline G4int GetStartLevel() const { return start_level; }
    inline G4int GetEndLevel()   const { return end_level; }
    inline AtomicLevel GetAtomicExcitationLevel() const { return atomic_excitation_level; }
    
    inline virtual void Dump() const {
        G4cout<<"  Type: "<<theType<<", "<< Energy << " keV, " << Intensity*100 << " %, from "
        <<start_level<<" to "<<end_level<<", atomic excitation lvl="<<atomic_excitation_level<<"\n";
    }
    
protected:
    DecayType theType;
    G4ParticleDefinition* theParticle;
    
    G4int start_level, end_level;
    AtomicLevel atomic_excitation_level;
};

using GLG4Deexcitation = GLG4Decay;

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
class GLG4EnergySpectrum
{
public:
    GLG4EnergySpectrum(): isAccumulated(false) {}
    virtual ~GLG4EnergySpectrum() { Reset(); }
    
    inline const GLG4Branch* ShootDecay() const {
        return *std::lower_bound(theSpectrum.begin(),theSpectrum.end(),RandG4_t::Uniform(),
                                 GLG4SpecPart::CompIntensityToDouble);
    }
    inline virtual G4double ShootEnergy() const {
        G4cout << "ShootEnergy: ShootDecay()\n";
        return ShootDecay()->ShootEnergy();
    }
    
    void AccumulateIntensity();
    void Push(GLG4EneBin* EneBin);
    void FillAndLock(const std::vector<G4double>& vE, const std::vector<G4double>& vI);
    void FillAndLock(const std::vector<std::string>& vE, const std::vector<std::string>& vI);
    
    void Reset();
    inline G4bool IsLocked() const { return isAccumulated; }
    inline G4bool IsAccumulated() const { return isAccumulated; }
    
    inline std::size_t GetSize() const { return theSpectrum.size(); }
    inline GLG4Branch* operator()(G4int i) const { return theSpectrum.at(i); }
    
    inline virtual void Dump() const {
        for (auto it=theSpectrum.begin(), end=theSpectrum.end(); it!=end; ++it) { (*it)->Dump(); }
    }
    
protected:
    std::vector< GLG4SpecPart* > theSpectrum;
    G4bool isAccumulated;
};

////////////////////////////////////////////////////////////////
//Don't know yet how to properly use this. Can be probably used to simulate various spectra from radioactive sources - MV May 2020
class GLG4Desintegration : public GLG4Decay, public GLG4EnergySpectrum
{
public:
    GLG4Desintegration(DecayType type, G4double E, G4double I, G4int start_lvl, G4int end_lvl, AtomicLevel ato_lvl):
    GLG4Decay(type,E,I,start_lvl,end_lvl,ato_lvl), GLG4EnergySpectrum() {}
    virtual ~GLG4Desintegration() {}
    
    inline virtual G4double ShootEnergy() const { return GLG4EnergySpectrum::ShootEnergy(); }
    inline virtual void Dump() const {
        G4cout << " Desintegration: "; GLG4Decay::Dump();
        //     G4cout <<"\n  Spectrum:\n"; GLG4EnergySpectrum::Dump();
    }
};

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

class GLG4VertexGen_SourceMonoSpectrum : public GLG4VVertexGen {
public:
    virtual void GeneratePrimaryVertex( G4Event *argEvent );
    virtual void SetState( G4String newValues ) = 0;
    virtual G4String GetState() = 0;
    
    virtual void SetDirectionTarget(G4PrimaryVertex* vertex) {(void) vertex; return;}
    virtual void ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir){(void) vertex; (void) Dir; return;};
    
protected:
    GLG4EnergySpectrum theDesintegrationSpectrum;
    G4ParticleDefinition* theParticle;
    
protected: // do not allow to instantiate GLG4VertexGen_SourceMonoDecay, just derived class
    GLG4VertexGen_SourceMonoSpectrum(const G4String& name="Energy generator: source with a unique desintegration spectrum",
                                     G4ParticleDefinition*part=nullptr): GLG4VVertexGen(name), theParticle(part) {}
    GLG4VertexGen_SourceMonoSpectrum(const GLG4VertexGen_SourceMonoSpectrum&): GLG4VVertexGen() {}
};

////////////////////////////////////////////////////////////////

class GLG4VertexGen_SourceMonoBeta : public GLG4VertexGen_SourceMonoSpectrum {
public:
    GLG4VertexGen_SourceMonoBeta(const G4String& spectrum_name="",
                                 const G4String& name="Energy generator: source with a unique beta minus desintegration spectrum");
    
    virtual void SetState( G4String newValues );
    virtual G4String GetState() { return scat("Source mono beta: ",SpectrumName); }
    
private:
    G4String SpectrumName;
};

////////////////////////////////////////////////////////////////

class GLG4VertexGen_SourceGamma : public GLG4VertexGen_SourceMonoSpectrum {
public:
    enum Type {Gamma, X, GX};
    GLG4VertexGen_SourceGamma(const G4String& table_name="", Type type=Gamma,
                              const G4String& name="Energy generator: source with a unique gamma deexcitation spectrum");
    
    virtual void SetState( G4String newValues );
    virtual G4String GetState() { return scat("Source gamma/X: ",theType,", from: ",TableName); }
    
private:
    void Fill(const G4String& table_name, const G4String& type);
    G4String TableName, theType;
};

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

class GLG4VertexGen_SourceMultipleBetas : public GLG4VVertexGen {
public:
    GLG4VertexGen_SourceMultipleBetas(const G4String& isotope="", const G4String& name=
                                      "Energy generator: source with several independant branches of desintegration or deexcitation");
    
    virtual void GeneratePrimaryVertex( G4Event *argEvent );
    virtual void SetState( G4String newValues );
    virtual G4String GetState() { return scat("Source multiple beta from: ",theIsotope); }
    
    virtual void SetDirectionTarget(G4PrimaryVertex* vertex) {(void) vertex; return;}
    virtual void ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir) {(void) vertex; (void) Dir; return;}
    
protected:
    void LoadIsotope();
    
    G4String theIsotope;
    GLG4EnergySpectrum theBranches;
    G4ParticleDefinition* theParticle; // not really usefull, the particle is also stored in branches, but simpler
};

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

class GLG4VertexGen_SourceAllDecays : public GLG4VVertexGen {
public:
    GLG4VertexGen_SourceAllDecays(const G4String& isotope="", const G4String& name=
                                  "Energy generator: source with several branches of desintegration or deexcitation correlated by levels");
    
    virtual void GeneratePrimaryVertex( G4Event *argEvent );
    virtual void SetState( G4String newValues );
    virtual G4String GetState() { return scat("Source all decays from: ",theIsotope); }
    
    virtual void SetDirectionTarget(G4PrimaryVertex* vertex) {(void) vertex; return;}
    virtual void ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir) {(void) vertex; (void) Dir; return;}
    
protected:
    void LoadIsotope();
    
    void NewParticle(const GLG4Decay* aDecay);
    DecayType FindType(const G4String& type_name) const;
    AtomicLevel FindMainLevel(const G4String& type_name) const;
    AtomicLevel FindSecondaryLevel(const G4String& type_name) const;
    
    void NuclearDeexcitation(G4int level);
    void AtomicDeexcitation(G4int level);
    
    std::map<DecayType, GLG4EnergySpectrum> theSpectra;
    std::map<G4int, GLG4EnergySpectrum> theNuclearLevels, theAtomicLevels;
    
    G4PrimaryVertex* theVertex;
    G4String theIsotope;
    G4double RemainingEnergy, MinimalEnergy;
};

////////////////////////////////////////////////////////////////
//////////////// COSMOGENIC NEUTRON GENERATOR //////////////////
////////////////////////////////////////////////////////////////
/**
 @author M. Vivier
 @brief
 - Simulate production of neutrons from atmospheric muon interaction in deep underground lab.
 - Modeling taken from Mei & Hime, Phys. Rev. D 73 053004 (2006)
 - This should be probably rewritten at some point... (MV - April 2018)
*/
class GLG4VertexGen_CosmogenicNeutrons : public GLG4VVertexGen {
public:
    virtual ~GLG4VertexGen_CosmogenicNeutrons() {}
    GLG4VertexGen_CosmogenicNeutrons(const G4String& name="Vertex generator: CosmogenicNeutron");
    virtual void GeneratePrimaryVertex( G4Event *argEvent );
    virtual void SetState( G4String newValues );
    // format: particle_name  dir_x dir_y dir_z  kinetic_energy  polx poly polz
    // If dir_x==dir_y==dir_z==0, the directions are isotropic.
    // If particle has mass==0 and spin==1, final polarization will be
    // projected into plane perpendicular to momentum and made a unit vector;
    // if polarization has zero magnitude, a polarization is chosen randomly.
    virtual G4String GetState();
    virtual void SetEnergy();
    virtual void SetDirectionCosmo();
    
    virtual void SetDirectionTarget(G4PrimaryVertex* vertex);
    inline virtual void ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir){(void) vertex; (void) Dir; return;}
    
    virtual inline G4String GetAniType() {return AniType;}
    
public:
    
    static G4ThreeVector dirMuon;
    static G4double Emuon;
    
private:
    G4int Multiplicity=1;
    G4double Ke=0., Ani=0.;
    G4double TJit=0., SJit=0., TCont=0.;
    G4ThreeVector Polar;
    G4ThreeVector P = G4ThreeVector(0.*MeV,0.*MeV, 0.*MeV);
    G4ThreeVector dir = G4ThreeVector(0.,0.,0.);
    
    G4String VolumeTarget = "NONE";
    G4String AniType{"cosmo"};
    G4ParticleDefinition *Particle=nullptr;
    
    
    //Setting Parameters for Energy distribution
    //for cosmogenic Muons
    G4double b = 0.4; // /km.w.e.
    G4double gammaMu = 3.77;
    G4double epsilonMu = 693.; //GeV
    G4double h = 0.120; // water equivalent depth of the near laboratory at Chooz (in kwe)
    //     G4double EmuonMin=0.;
    //     G4double EmuonMax=60.;  //Max energy of muons in GeV (cut)
    
    
    //for neutron induced
    unsigned nBinE = 100000;	//Energy binning
    G4double a0=7.;	  //Fit parameters
    G4double a1=2.;
    G4double Enmin=0.;
    G4double Elimitmin=0.;
    G4double Enmax=60.; //Energy windo in GeV
    G4double deltaE;
    G4double Eneutron=0.;
    G4double B;
    
    //Setting Parameters for Angular distribution
    unsigned nBinAng = 100000;	//Energy binning
    G4double cosThetaMin=-1.;
    //     G4double cosThetaMax=1.;
    G4double deltaCosTheta =2.;
    G4double cosTheta=-1.;
    G4double cosThetaMuon=-1.;
    G4double Btheta;
    G4double Ctheta;
};

/**
 @author M. Vivier
 @brief
 - Generator which sample energies from a user-defined spectrum and stored in a data file
 - Mother class for the atmospheric neutrons and the ambient gammas generators
 */
class GLG4VertexGen_UserSpec : public GLG4VVertexGen {
public:
    virtual ~GLG4VertexGen_UserSpec() {dNdE.clear(); CDF.clear();}
    GLG4VertexGen_UserSpec(const G4String& name="GLG4VertexGen_UserSpec", const char* filename="", const G4String& pname="");
    virtual void GeneratePrimaryVertex( G4Event *argEvent ); // generates a primary vertex with given particle type, direction, energy and consistent polarization.
    
    virtual void SetState(G4String newValues);
    // format: particle_name  dir_x dir_y dir_z  Emin 'target VolTarget' or 'center'
    // If dir_x==dir_y==dir_z==0, the directions are isotropic.
    //Emin: minimal energy from which we sample neutron eneregy over atmospheric spectrum
    // Keywords:
    //  - target VolTarget: will shoot particles automatically toward the center of designed 'VolTarget'
    //  - center: wiull shoot particle toward the center of position generator, if exists
    // If particle has mass==0 and spin==1, final polarization will be
    // projected into plane perpendicular to momentum and made a unit vector;
    // if polarization has zero magnitude, a polarization is chosen randomly.
    
    G4String GetState();    //returns current state formatted as above
    
    void ReadDataFile();
    void ComputeCDF();
    void SetEnergy();
    
    virtual void SetDirectionTarget(G4PrimaryVertex* vertex);
    virtual void ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir);
    
    virtual inline G4String GetAniType() {return AniType;}
    
public:
    enum KeyWord {
        EMIN,
        EMAX,
        TARGET,
        CENTER
    };
    
    void TreatKeyWord(KeyWord Key, const Double_t& Input);
    void TreatKeyWord(KeyWord Key, const G4String& Input);
    
protected:
    G4String FileName;
    G4String PName;
    
    G4int Multiplicity=1;
    G4double Ke=0., Ani=0.;
    G4double TJit=0., SJit=0., TCont=0.;
    G4ThreeVector Polar;
    G4ThreeVector P = G4ThreeVector(0.*MeV,0.*MeV, 0.*MeV);
    G4ThreeVector dir = G4ThreeVector(0.,0.,0.);
    
    G4String VolumeTarget{"NONE"};
    G4String AniType{"iso"};
    G4ParticleDefinition *Particle=nullptr;
    
    G4double Enmin=0.;
    G4double Enmax=0.;
    G4double Elimitmin=0.;
    G4double Elimitmax=0.;
    unsigned nBinE=0;
    std::vector<std::pair<Double_t, Double_t>> dNdE;
    std::vector<std::pair<Double_t, Double_t>> CDF;
};

////////////////////////////////////////////////////////////////
//////////////// ATMOSPHERIC NEUTRON GENERATOR //////////////////
////////////////////////////////////////////////////////////////
/**
 @author M. Vivier
 @brief
 - Uses spectrum data points digitized from the figure 4 of publication of M.S. Gordon et al. (Proceedings of the IEEE Nuclear and Space Radiation Effect conference, 2004)
*/
class GLG4VertexGen_AtmosphericNeutrons : public GLG4VertexGen_UserSpec {
public:
    virtual ~GLG4VertexGen_AtmosphericNeutrons() {}
    GLG4VertexGen_AtmosphericNeutrons(const G4String& name="GLG4VertexGen_AtmosphericNeutrons", const char* filename="AtmosphericNeutronFluxData.dat", const G4String& pname="neutron");
};

////////////////////////////////////////////////////////////////
//////////////// AMBIENT GAMMAS  GENERATORS //////////////////
////////////////////////////////////////////////////////////////
/**
 @author M. Vivier
 @brief
 - Uses spectrum data points obtained through a simulation of the radioactive decay of K40, Th232 and U238 in the building materials surrounding the VNS room at Chooz
*/
class GLG4VertexGen_AmbientGammas : public GLG4VertexGen_UserSpec {
public:
    virtual ~GLG4VertexGen_AmbientGammas() {}
    GLG4VertexGen_AmbientGammas(const G4String& name="GLG4VertexGen_AmbientGammas", const char* filename="AmbientGammaSpectrum.dat", const G4String& pname="gamma");
};

// Individual U238, Th232 and K40 decays in the VNS walls C.Goupy 2023
class GLG4VertexGen_AmbientU238 : public GLG4VertexGen_AmbientGammas {
public:
    virtual ~GLG4VertexGen_AmbientU238() {}
    GLG4VertexGen_AmbientU238(const G4String& name="GLG4VertexGen_AmbientU238", const char* filename="AmbientU238Spectrum.dat", const G4String& pname="gamma");
};

class GLG4VertexGen_AmbientTh232 : public GLG4VertexGen_AmbientGammas {
public:
    virtual ~GLG4VertexGen_AmbientTh232() {}
    GLG4VertexGen_AmbientTh232(const G4String& name="GLG4VertexGen_AmbientTh232", const char* filename="AmbientTh232Spectrum.dat", const G4String& pname="gamma");
};

class GLG4VertexGen_AmbientK40 : public GLG4VertexGen_AmbientGammas {
public:
    virtual ~GLG4VertexGen_AmbientK40() {}
    GLG4VertexGen_AmbientK40(const G4String& name="GLG4VertexGen_AmbientK40", const char* filename="AmbientK40Spectrum.dat", const G4String& pname="gamma");
};

class GLG4VertexGen_AmbientGammasfromDeconv : public GLG4VertexGen_AmbientGammas {
public:
    virtual ~GLG4VertexGen_AmbientGammasfromDeconv() {}
    GLG4VertexGen_AmbientGammasfromDeconv(const G4String& name="GLG4VertexGen_AmbientGammasfromDeconv", const char* filename="AmbientGammaSpectrum_FabDeconv.dat", const G4String& pname="gamma");
};
////////////////////////////////////////////////////////////////
//////////////// ATMOSPHERIC MUON GENERATOR ////////////////////
////////////////////////////////////////////////////////////////
/**
 * @brief
 * @author Holger Kluck (holger.kluck@oeaw.ac.at)
 */
class GLG4VertexGen_AtmosphericMuons : public GLG4VVertexGen {
public:
    virtual ~GLG4VertexGen_AtmosphericMuons() {}
    GLG4VertexGen_AtmosphericMuons(const G4String& name="Vertex generator: AtmosphericMuons");
    virtual void GeneratePrimaryVertex( G4Event *argEvent );
    // adds vertices.
    virtual void SetState( G4String newValues );
    // sets filename or other information needed by vertex generator
    virtual G4String GetState();
    // returns the current state information in a form that can be understood
    // by SetState (and, hopefully, a well-informed human)
    
    virtual void SetDirectionTarget(G4PrimaryVertex* vertex);
    virtual void ForceDirectionTo(G4PrimaryVertex* vertex, G4ThreeVector Dir);
    
protected:
    struct MuonData{
        inline MuonData(G4double f, G4double e, G4double z, G4double a) : probability(0.), flux(f), energy(e), zenith(z), azimuth(a){};
        G4double probability;//Normalized probability of given energy-direction bin
        G4double flux;       //integral muon flux over the energy-direction bin
        G4double energy;     //muon energy
        G4double zenith;     //muon direction
        G4double azimuth;    //muon direction
    };
    
private:
    G4int NbEBins; //Number of energy bins
    G4double EMin; //Minimal energy
    G4double EMax; //Maximal energy
    G4int NbABins; //Number of azimuth bins
    G4double ZMin = 0.; //Minimal Zenith angle
    G4double ZMax = 90.; //Maximal Zenith angle
    G4int NbZBins; //Number of zenith bins
    G4String Target;//Target volume
    
    static G4bool IsInitialized; //True if the CDF is aready generated
    static std::vector<MuonData> CDF; //Tabulated CDF of muons
    void CalculateCDF();//Build a look-up table with the CDF of the muon spectrum
    G4double CalculateDiffMuonFlux(G4double energy, G4double zenith);//Calculate the differential muon flux for a given energy and zenith angle
    G4double CalculateIntMuonFlux(G4double energyLow, G4double energyUp, G4double zenithLow, G4double zenithUp, G4double azimuthLow, G4double azimuthUp);//Calculate the integral muon flux for a given parameter bin [energyLow, energyUp]x[azimuthLow, azimuthUp]x[zenithLow, zenithUp]
};

#endif
