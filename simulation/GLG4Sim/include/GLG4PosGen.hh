// This file is part of the GenericLAND software library.
//
// GenericLAND global position generator for primary events,
// by G.Horton-Smith, August 3, 2001
// (See note on GenericLAND generators for more information.)

#ifndef GLG4POSGEN_HH
#define GLG4POSGEN_HH 1

#include <String.hh>
#include <Rand.hh>

#include <G4Types.hh>
#include <G4String.hh>
#include <G4ThreeVector.hh>

#include <vector>
#include <G4AffineTransform.hh>

class G4PrimaryVertex;
class G4VPhysicalVolume;
class G4Material;

class GLG4VPosGen {
    public:
    GLG4VPosGen(const G4String& name="Position generator: virtual");
    virtual ~GLG4VPosGen() { }
    
    virtual void GenerateVertexPositions( G4PrimaryVertex *argVertex,
                                         G4double max_chain_time,
                                         G4double event_rate,
                                         G4bool offset=false,
                                         G4double dt=0.0
                                         );
    // set the positions of vertices, and splits chains at max_chain_time
    // ("split" means a new random position is generated and a random time offset added according to event_rate),
    // Optionally:
    //  - add an extra time offset to each vertex
    //  - add an offset to previous value of vertices instead of setting them
    
    virtual void GeneratePosition(G4ThreeVector* argResult) = 0;
    // generates random position, for usual case where there is no
    // dependence on particle types, momenta, etc.
    
    virtual void SetState(const G4String& newValues) = 0;
    // sets filename or other information needed by global position generator
    
    virtual G4String GetState() const = 0;
    // returns the current state information in a form that can be understood
    // by SetState (and, hopefully, a well-informed human)
    
    static G4String Strip(const G4String& stripstr);
    // strips leading and trailing characters from s
    
    // return a G4ThreeVector from a formatted string (x,y,z)
    static G4ThreeVector GetPointFromString(const G4String& str);
    
    //To retrieve position of point-like generator position
    virtual G4ThreeVector GetGeneratorPosition()=0;
    
    //To retrieve position of generated point
    virtual G4ThreeVector GetPosition()=0;
    
    //To later force the direction along which we'd like to shoot particle (vertex)
    //and which may depend on the coordinates of the generated position
    virtual G4ThreeVector GetDirection()=0;
    
    protected:
    G4String Name; // used for ParamBase key prefix
    G4int VerboseLevel = 0;
    G4double maxDistanceInRock = 0;
};

////////////////////////////////////////////////////////////////

class GLG4PosGen_null : public GLG4VPosGen {
    // Does nothing.
    // Useful with a vertex generator which already sets positions.
    public:
    GLG4PosGen_null(const G4String& name="Position generator: null") : GLG4VPosGen(name) {}
    inline virtual void GeneratePosition( G4ThreeVector* ) { }
    inline virtual void SetState(const G4String& )  { }
    inline virtual G4String GetState() const { return "GLG4PosGen_null has no state"; }
    inline virtual G4ThreeVector GetGeneratorPosition(){return G4ThreeVector(0., 0., 0.);}
    inline virtual G4ThreeVector GetPosition() {return G4ThreeVector(0., 0., 0.);}
    inline virtual G4ThreeVector GetDirection() {return G4ThreeVector(0., 0., 0.);}
};

////////////////////////////////////////////////////////////////

class GLG4PosGen_PointPaintFill : public GLG4VPosGen {
    public:
    GLG4PosGen_PointPaintFill(const G4String& name="Position generator: PointPaintFill"): GLG4VPosGen(name) {}
    virtual void GeneratePosition( G4ThreeVector *argResult );
    // Generates a position either at a fixed point in the global coordinates
    // or uniformly filling a volume, or at the surface of a volume.
    // The volume is given by its name, or if no name is provided, by the volume containing a point).
    // - Fixed point is fastest.
    // - A random point in a compact physical volume is also pretty fast.
    // - A volume which only sparsely fills its geometric "extent" may
    //   require many iterations to find an internal point -- this will be slow.
    virtual void SetState(const G4String& newValues);
    // newValues == x y z coordinates in mm (separated by white space),
    // optionally followed by keyword "fill" for volume-filling mode,
    // optionally followed by name of physical volume expected at that position;
    // or keyword "paint" for surface-painting mode,
    // optionally followed by name of physical volume expected at that position,
    // optionally followed by thickness of coat of "paint" (external to volume).
    // optionally followed by name of material to which to restrict "paint".
    virtual G4String GetState() const;
    
    //useless for this generator, for now
    virtual G4ThreeVector GetGeneratorPosition() {return G4ThreeVector(0., 0., 0.);}
    
    //useless for this generator, for now
    inline virtual G4ThreeVector GetDirection() {return G4ThreeVector(0., 0., 0.);}
    
    inline virtual G4ThreeVector GetPosition() {return Pos;}
    
    // returns current state in format above
    private:
    enum { kPoint=0, kFill, kPaint };
    
    void GeneratePoint( G4ThreeVector *argResult );
    void GenerateFill ( G4ThreeVector *argResult );
    void GeneratePaint( G4ThreeVector *argResult );
    
    G4int Mode = kPoint;
    G4int NbTried=0,NbFound=0;
    G4double PaintThickness=0.;
    G4ThreeVector Pos;
    std::vector<G4ThreeVector> Intercepts;
    
    G4String VolumeName{"!"};
    G4VPhysicalVolume* Volume=nullptr;
    
    G4String MaterialName;
    G4Material* Material=nullptr;
    
    G4double BoundingBoxVolume=0.;
    G4ThreeVector BoundingBoxCenter, BoundingBoxDim;
    
    G4ThreeVector translation = G4ThreeVector(0.,0.,0.);
};

////////////////////////////////////////////////////////////////

class GLG4PosGen_Plane : public GLG4VPosGen {
    public:
    virtual ~GLG4PosGen_Plane() {}
    GLG4PosGen_Plane(const G4String& name="Position generator: plane"):
    GLG4VPosGen(name), X(0.0), Y(0.0), Z(0.0), Width(0.0), Height(0.0) {}
    
    virtual void GenerateVertexPositions(G4PrimaryVertex *argVertex,
                                         G4double max_chain_time,
                                         G4double event_rate,
                                         G4bool offset=false,
                                         G4double dt=0.0
                                         );
    // external flux uniformly distributed over area normal to
    // incident direction of first track in vertex
    virtual void GeneratePosition(G4ThreeVector *);  // (not used)
    
    // newValues == "x y z width height"
    //  (x,y,z) = position of the center of the rectangular area
    //  width == width of rectangular area normal to incident direction (mm)
    //  height == height of rectangular area normal to incident direction (mm)
    // The rectangle is rotated so that the "width" direction vector lies in
    // the XY plane for non-zero polar angle of the incident track.
    // If a track is generated which completely misses the detector,
    // the PDG code of the vertex tracks are modified to make them have
    // an impossible effective ISTHEP codes of 1, so Geant4 does not attempt
    // to track them.
    // This means the generated external flux in Hz/mm**2 is always
    //     flux = (rate/(width*height)),
    // regardless of the geometry of the detector, where "rate" is the rate
    // set via /generators/rate.  The "rate" must be chosen appropriately
    // for the area of the rectangle.
    virtual void SetState(const G4String& newValues);
    
    inline virtual G4ThreeVector GetGeneratorPosition() {return G4ThreeVector(0., 0., 0.);};
    
    inline virtual G4ThreeVector GetDirection() {return G4ThreeVector(0., 0., 0.);}
    
    inline virtual G4ThreeVector GetPosition() {return thePos;}
    
    // returns current state in format above
    virtual G4String GetState() const { return scat("  ",X,"  ",Y,"  ",Z,"  ",Width," ",Height); }
    
    private:
    G4double X, Y, Z, Width, Height;
    G4ThreeVector thePos;
};

////////////////////////////////////////////////////////////////

class GLG4PosGen_Point : public GLG4VPosGen
{
    public:
    GLG4PosGen_Point(const G4String& name="Position generator: point");
    virtual ~GLG4PosGen_Point() {}
    
    virtual void GeneratePosition( G4ThreeVector *argResult );
    virtual G4String GetState() const { return scat("  ",thePoint); }
    virtual void SetState(const G4String& newValues); // newValues must be a formatted string "(x,y,z)"
    inline virtual G4ThreeVector GetGeneratorPosition() {return thePoint;}
    inline virtual G4ThreeVector GetPosition() {return thePoint;}
    inline virtual G4ThreeVector GetDirection() {return G4ThreeVector(0., 0., 0.);}
    
    private:
    G4ThreeVector thePoint;
};

////////////////////////////////////////////////////////////////

class GLG4PosGen_Sphere : public GLG4VPosGen
{
    public:
    GLG4PosGen_Sphere(const G4String& name="Position generator: sphere");
    virtual ~GLG4PosGen_Sphere() {}
    
    virtual void GenerateVertexPositions(G4PrimaryVertex *argVertex,
                                         G4double max_chain_time,
                                         G4double event_rate,
                                         G4bool offset=false,
                                         G4double dt=0.0
                                         );
    
    virtual void GeneratePosition( G4ThreeVector *argResult );
    virtual G4String GetState() const { return scat("  center=",theSphereCenter," r=",radius, " tmin=",tmin," tmax=", tmax, " Angular law:", AngType); }
    virtual void SetState(const G4String& newValues); // newValues must be a formatted string "(x,y,z) r tmin tmax AngType"
    inline virtual G4ThreeVector GetGeneratorPosition() {return theSphereCenter;}
    inline G4ThreeVector GetPosition() {return thePos;}
    inline G4ThreeVector GetDirection() {return theDir;}
    
    private:
    G4ThreeVector theSphereCenter;
    G4ThreeVector thePos;
    G4ThreeVector theDir;
    G4double radius, tmin, tmax;
    G4String AngType;
    G4double Width, Height;
};

////////////////////////////////////////////////////////////////

class GLG4PosGen_Cylinder : public GLG4VPosGen
{
    public:
    GLG4PosGen_Cylinder(const G4String& name="Position generator: cylinder");
    virtual ~GLG4PosGen_Cylinder() {}
    
    virtual void GeneratePosition( G4ThreeVector *argResult );
    virtual G4String GetState() const { return scat("  center=",center," r=",rho," hh=",half_height," axis:",axis); }
    virtual void SetState(const G4String& newValues); // newValues must be a formatted string "(x,y,z) rho half_height axis"
    inline virtual G4ThreeVector GetGeneratorPosition() {return center;}
    inline virtual G4ThreeVector GetPosition() {return G4ThreeVector(0., 0., 0.);}
    inline virtual G4ThreeVector GetDirection() {return G4ThreeVector(0., 0., 0.);}
    
    private:
    G4ThreeVector center;
    G4double rho, half_height;
    Random<G4ThreeVector>::Axis axis;
};

////////////////////////////////////////////////////////////////

class GLG4PosGen_Cuboid : public GLG4VPosGen
{
    public:
    GLG4PosGen_Cuboid(const G4String& name="Position generator: cuboid");
    virtual ~GLG4PosGen_Cuboid() {}
    
    virtual void GeneratePosition( G4ThreeVector *argResult );
    virtual G4String GetState() const { return scat("  center=",center," dim=",a,"x",b,"x",c); }
    virtual void SetState(const G4String& newValues); // newValues must be a formatted string "(x,y,z) a b c"
    inline virtual G4ThreeVector GetGeneratorPosition() {return center;}
    inline virtual G4ThreeVector GetPosition() {return G4ThreeVector(0., 0., 0.);}
    inline virtual G4ThreeVector GetDirection() {return G4ThreeVector(0., 0., 0.);}
    
    private:
    G4ThreeVector center;
    G4double a,b,c;
};

#endif // GLG4POSGEN_HH
