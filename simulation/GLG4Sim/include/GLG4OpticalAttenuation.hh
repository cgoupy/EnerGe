// This file is part of the GenericLAND software library.
// $Id: GLG4OpticalAttenuation.hh,v 1.2 2005/03/29 21:13:24 gahs Exp $
//
//  "Attenuation" (absorption or scattering) of optical photons
//
//   GenericLAND Simulation
//
//   Original: Glenn Horton-Smith, Dec 2001
//
// GLG4OpticalAttenuation.hh
// 

#ifndef GLG4OpticalAttenuation_h
#define GLG4OpticalAttenuation_h 1

/////////////
// Includes
/////////////

#include <G4OpAbsorption.hh>

/////////////////////
// Class Definition
/////////////////////

class GLG4OpticalAttenuation : public G4OpAbsorption
{

private:

        //////////////
        // Operators
        //////////////

        // GLG4OpticalAttenuation& operator=(const GLG4OpticalAttenuation &right);

public: // Without description

        ////////////////////////////////
        // Constructors and Destructor
        ////////////////////////////////

        GLG4OpticalAttenuation(const G4String& processName = "Attenuation");

        // GLG4OpticalAttenuation(const GLG4OpticalAttenuation &right);

	~GLG4OpticalAttenuation();

	////////////
	// Methods
        ////////////

        // G4bool IsApplicable(const G4ParticleDefinition& aParticleType);
        // Returns true -> 'is applicable' only for an optical photon.

	// G4double GetMeanFreePath(const G4Track& aTrack,
	// 			    G4double ,
	// 			    G4ForceCondition* );
        // Returns the absorption length for bulk absorption of optical
        // photons in media with a specified attenuation length. 

public: // With description

	G4VParticleChange* PostStepDoIt(const G4Track& aTrack,
 				        const G4Step&  aStep);
        // This is the method implementing attenuation of optical 
        // photons.  Fraction of photons scattered or absorbed is
        // determined by the MaterialProperyVector "OPSCATFRAC".

};

#endif /* GLG4OpticalAttenuation_h */
