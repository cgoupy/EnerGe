#ifndef __GLG4HitPMT_hh__
#define __GLG4HitPMT_hh__

/** @file GLG4HitPMT.hh
    Declares GLG4HitPMT class and helper functions.
    
    This file is part of the GenericLAND software library.
    $Id: GLG4HitPMT.hh,v 1.2 2005/03/29 21:13:23 gahs Exp $
    
    @author Glenn Horton-Smith, December 2004
*/

#include <G4Types.hh>
#include <vector>

class GLG4HitPhoton;

/** GLG4HitPMT stores information about a PMT that detected one or more
    photoelectrons.

    The general contract for GLG4HitPMT is as follows:
      - remember ID and all photons that made photoelectrons in this PMT
      - provide Clear, DetectPhoton, SortTimeAscending, GetID, GetEntries,
        GetPhoton, and Print(ostream &) functions
      - take "ownership" of the photons registered by DetectPhoton,
        i.e., delete them when Clear or destrictor is called

    This is almost the same "general contract" that was implemented
    for KLG4sim's KLHitPMT by O. Tajima and G. Horton-Smith, but the
    code was rewritten for GLG4sim in December 2004.

    @author Glenn Horton-Smith
*/

class GLG4HitPMT {
public:
  GLG4HitPMT(G4int ID): fID(ID) {}
  ~GLG4HitPMT() { Clear(); }

  void Clear();
  void DetectPhoton(GLG4HitPhoton*);
  void SortTimeAscending();

  inline G4int GetID() const { return fID; }
  inline G4int GetEntries() const { return fPhotons.size(); }
  inline GLG4HitPhoton* GetPhoton(G4int i) const { return fPhotons[i]; }

  void Print(std::ostream &, G4bool fullDetailsMode=false);

  static G4bool Compare_HitPMTPtr_TimeAscending(const GLG4HitPMT *a, const GLG4HitPMT *b);
  static const std::size_t kApproxMaxIndividualHitPhotonsPerPMT;
  static const G4double kMergeTime;
  
private:
  G4int fID;
  std::vector<GLG4HitPhoton*> fPhotons;
};

#endif // __GLG4HitPMT_hh__
