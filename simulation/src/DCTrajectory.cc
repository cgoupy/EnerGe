#include <DCTrajectory.hh>

#include <G4Trajectory.hh>
#include <G4OpticalPhoton.hh>
#include <G4Gamma.hh>
#include <G4Neutron.hh>
#include <G4Proton.hh>
#include <G4Positron.hh>
#include <G4Electron.hh>
#include <G4ThreeVector.hh>
#include <G4Polyline.hh>
#include <G4Colour.hh>
#include <G4VisAttributes.hh>
#include <G4VVisManager.hh>
#include <G4ParticleDefinition.hh> 
#include <G4Track.hh>

G4Allocator<DCTrajectory> DCTrajectoryAllocator;

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
DCTrajectory::DCTrajectory(const G4Track* aTrack): G4Trajectory(aTrack)
{
  particleDefinition=aTrack->GetDefinition();
  //  std::cout << aTrack->GetDefinition()->GetParticleName() << "\n";
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
DCTrajectory::DCTrajectory(DCTrajectory &right): G4Trajectory(right),wls(right.wls),drawit(right.drawit)
{
  particleDefinition=right.particleDefinition;
}

//_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_-_
void DCTrajectory::DrawTrajectory() const{
  //Taken from G4VTrajectory and modified to select colours based on particle
  //type and to selectively eliminate drawing of certain trajectories.

  //  if(!forceDraw && (!drawit || forceNoDraw))
  //    return;
 
  G4VVisManager* pVVisManager = G4VVisManager::GetConcreteInstance();
  if (!pVVisManager) return;

  G4Polyline trajectoryLine;

  for (G4int i = 0; i < GetPointEntries() ; i++) {
    G4VTrajectoryPoint* aTrajectoryPoint = GetPoint(i);
    const std::vector<G4ThreeVector>* auxiliaries = aTrajectoryPoint->GetAuxiliaryPoints();
    if (auxiliaries) {
      for (size_t iAux = 0; iAux < auxiliaries->size(); ++iAux) {
        const G4ThreeVector pos((*auxiliaries)[iAux]);
	trajectoryLine.push_back(pos);
      }
    }
    const G4ThreeVector pos(aTrajectoryPoint->GetPosition());
    trajectoryLine.push_back(pos);
  }

  G4Colour colour;

  if (particleDefinition==G4OpticalPhoton::Definition()) {
    colour = G4Colour::Cyan();
  } else if (particleDefinition==G4Gamma::Definition()) {
    colour = G4Colour::Green();
  } else if (particleDefinition==G4Neutron::Definition()) {
    colour = G4Colour::Blue();
  } else if (particleDefinition==G4Proton::Definition()) {
    colour = G4Colour::Magenta();
  } else if (particleDefinition==G4Positron::Definition()) {
    colour = G4Colour::White();
  } else if (particleDefinition==G4Electron::Definition()) {
    colour = G4Colour::Yellow();
  } else {
    colour = G4Colour::Brown();
  }
  
  G4VisAttributes trajectoryLineAttribs(colour);
  trajectoryLine.SetVisAttributes(&trajectoryLineAttribs);
  pVVisManager->Draw(trajectoryLine);
}





