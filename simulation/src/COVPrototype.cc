#include "COVPrototype.hh"

#include <G4SubtractionSolid.hh>
#include <G4UnionSolid.hh>
#include <G4Tubs.hh>
#include <G4Orb.hh>

#include <G4VisAttributes.hh>
#include <G4Colour.hh>

#include <G4SystemOfUnits.hh>

#include <iomanip>
#include <cstdio>

G4VSolid* LWO_holder(const G4String& name, G4double Boxes_gap);

COVPrototype::COVPrototype():
overlap_test(ParamBase::GetDataBase().Get<bool>("overlap_test")),
construction_verbose_level(ParamBase::GetDataBase().Get<int>("construction_verboselevel")),
detector_choice(ParamBase::GetDataBase().Get<int>("detector_choice")),
copper_boxes(ParamBase::GetDataBase().Get<int>("copper_boxes")){}

void COVPrototype::Build(G4LogicalVolume* mother, G4ThreeVector& Pos){
    
    ///Cu Boxes

    G4double Cu_Box_outer_radius = 43*CLHEP::mm;
    G4double Cu_Box_height = 27*CLHEP::mm;
    G4double Cu_Box_thickness = 3*CLHEP::mm;
    G4double Cu_Cap_outer_radius = 43*CLHEP::mm;
    G4double Cu_Cap_thickness = 1*CLHEP::mm;
    G4double Cu_Cap_hole_radius = 1*CLHEP::mm;
    G4double Boxes_gap = 39*CLHEP::mm;
    G4double Total_height = 2*Cu_Box_height+Boxes_gap;
    
    ///Crystals
    G4double Ge_height = 20*CLHEP::mm;
    G4double Ge_radius = 35*CLHEP::mm;
    G4double LWO_height = 25*CLHEP::mm;
    G4double LWO_radius = 12.5*CLHEP::mm;
    G4double Cu_tape_thickness = 50*CLHEP::um;
    G4double Cu_tape_radius = 2*CLHEP::mm;
    
    //Top crystal
    new Volume<G4Tubs>("Top_COVProto", "Germanium", mother, Pos + G4ThreeVector(0, 0, Boxes_gap/2.+Cu_Box_height/2.), 0, Ge_radius, Ge_height/2., 0*CLHEP::deg, 360*CLHEP::deg);
    
    //Middle crystal
    new Volume<G4Tubs>("LWO_COVProto", "LWO", mother, Pos, 0, LWO_radius, LWO_height/2., 0*CLHEP::deg, 360*CLHEP::deg);
    
    //Bottom crystal
    new Volume<G4Tubs>("Bot_COVProto", "Germanium", mother, Pos + G4ThreeVector(0, 0, -Boxes_gap/2.-Cu_Box_height/2.), 0, Ge_radius, Ge_height/2., 0*CLHEP::deg, 360*CLHEP::deg);
    
    //Cu tape
    new Volume<G4Tubs>("Cu_tape", "Copper", mother, Pos + G4ThreeVector(0, 0, -Boxes_gap/2.-Cu_Box_height-Cu_Box_thickness-50*CLHEP::um), 0, Cu_tape_radius, Cu_tape_thickness/2., 0*CLHEP::deg, 360*CLHEP::deg);
    
    if((construction_verbose_level > 0)&&(!copper_boxes)) {
        std:: cout << "\t +++++ COV prototype Geometry ++++\n";
        std::cout << " - Germanium Crystal : Radius = " << Ge_radius/cm << " cm - Height = " << Ge_height/cm << " cm" << std::endl;
        std::cout << " - LWO Crystal : Radius = " << LWO_radius/cm << " cm - Height = " << LWO_height/cm << " cm" << std::endl;
        
        std::cout << " ------------ No copper holders " << std::endl;
    }
    
    if (copper_boxes){
        //TopBox
        G4VSolid* Top_Box_sol = new G4Tubs("Top_Box_sol",  Cu_Box_outer_radius-Cu_Box_thickness, Cu_Box_outer_radius, Cu_Box_height/2., 0*deg, 360*deg);
        G4LogicalVolume* Top_Box_log = new G4LogicalVolume(Top_Box_sol, GLG4Material::GetMaterial("Copper"),"Top_Box_log");
        new G4PVPlacement(0, Pos + G4ThreeVector(0, 0, Boxes_gap/2.+Cu_Box_height/2.),Top_Box_log,"VHphysic_Top_Box",mother,false,0, overlap_test);
        Top_Box_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
        
        G4VSolid* Top_Cap_sol = new G4Tubs("Top_Cap_sol", 0, Cu_Cap_outer_radius, Cu_Cap_thickness/2., 0*deg, 360*deg);
        G4LogicalVolume* Top_Cap_log = new G4LogicalVolume(Top_Cap_sol, GLG4Material::GetMaterial("Copper"),"Top_Box_log");
        new G4PVPlacement(0, Pos + G4ThreeVector(0, 0, +Boxes_gap/2.+Cu_Box_height+Cu_Cap_thickness/2.),Top_Cap_log,"VHphysic_Top_Cap",mother,false,0, overlap_test);
        Top_Cap_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
        
        //BotBox
        G4VSolid* Bot_Box_sol = new G4Tubs("Bot_Box_sol", Cu_Box_outer_radius-Cu_Box_thickness, Cu_Box_outer_radius, Cu_Box_height/2., 0*deg, 360*deg);
        G4LogicalVolume* Bot_Box_log = new G4LogicalVolume(Bot_Box_sol, GLG4Material::GetMaterial("Copper"), "Bot_Box_log");
        new G4PVPlacement(0, Pos + G4ThreeVector(0, 0, -Boxes_gap/2.-Cu_Box_height/2.), Bot_Box_log,"VHphysic_Bot_Box",mother,false,0, overlap_test);
        Bot_Box_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
        
        G4VSolid* Bot_Cap_sol = new G4Tubs("Bot_Cap_sol", Cu_Cap_hole_radius, Cu_Cap_outer_radius, Cu_Cap_thickness/2., 0*deg, 360*deg);
        G4LogicalVolume* Bot_Cap_log = new G4LogicalVolume(Bot_Cap_sol, GLG4Material::GetMaterial("Copper"),"Bot_Cap_log");
        new G4PVPlacement(0, Pos + G4ThreeVector(0, 0, -Boxes_gap/2.-Cu_Box_height-Cu_Cap_thickness/2.),Bot_Cap_log,"VHphysic_Bot_Cap",mother,false,0, overlap_test);
        Bot_Cap_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
        
        /// LWO holder
        G4VSolid* LWO_holder_sol= LWO_holder("LWO_holder", Boxes_gap);
        G4ThreeVector Pos_LWO_holder = Pos-G4ThreeVector(0.,0.,Boxes_gap/2.-1*mm/2.);
        
        G4LogicalVolume* LWO_holder_log = new G4LogicalVolume(LWO_holder_sol, GLG4Material::GetMaterial("Copper"),"LWO_holder_log");
        new G4PVPlacement(nullptr,Pos_LWO_holder,LWO_holder_log,"VHphysic_LWO_holder",mother,false,0,overlap_test);
        
        LWO_holder_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
        
        //Copper foil
        new Volume<G4Tubs>("Copper_foil", "Copper", mother, Pos, Cu_Box_outer_radius, Cu_Box_outer_radius+0.5, Total_height/2., 0*CLHEP::deg, 360*CLHEP::deg);
        
        if(construction_verbose_level > 0) {
            std:: cout << "\t +++++ COV prototype Geometry ++++\n";
            std::cout << " - Germanium Crystal : Radius = " << Ge_radius/cm << " cm - Height = " << Ge_height/cm << " cm" << std::endl;
            std::cout << " - LWO Crystal : Radius = " << LWO_radius/cm << " cm - Height = " << LWO_height/cm << " cm" << std::endl;
            
            std::cout << " ------------ Copper holders Mass : " << std::endl;
            std::cout << "- Top + Cap: m = "<< Top_Box_log->GetMass(true, false, GLG4Material::GetMaterial("Copper"))/kg + Top_Cap_log->GetMass(true, false, GLG4Material::GetMaterial("Copper"))/kg<<" kg"<<std::endl;
            std::cout << "- Bottom + Cap: m = "<< Bot_Box_log->GetMass(true, false, GLG4Material::GetMaterial("Copper"))/kg + Bot_Cap_log->GetMass(true, false, GLG4Material::GetMaterial("Copper"))/kg<<" kg"<< std::endl;
            std::cout << "-- Total:  m = "<< Top_Box_log->GetMass(true, false, GLG4Material::GetMaterial("Copper"))/kg + Top_Cap_log->GetMass(true, false, GLG4Material::GetMaterial("Copper"))/kg + Bot_Box_log->GetMass(true, false, GLG4Material::GetMaterial("Copper"))/kg + Bot_Cap_log->GetMass(true, false, GLG4Material::GetMaterial("Copper"))/kg << " kg"<< std::endl;
        }
    }
    
}

G4VSolid* LWO_holder(const G4String& name, G4double Boxes_gap){
    G4double Out_disk_outer_radius = 40*CLHEP::mm;
    G4double Out_disk_inner_radius = 30*CLHEP::mm;
    G4double In_disk_outer_radius = 20*CLHEP::mm;
    G4double In_disk_inner_radius = 12*CLHEP::mm;
    G4double Disk_thickness = 1*CLHEP::mm;
    
    G4VSolid* Out_LWO_Disk_sol = new G4Tubs("Out_LWO_Disk_sol", Out_disk_inner_radius, Out_disk_outer_radius, Disk_thickness/2., 0*deg, 360*deg);
    G4VSolid* In_LWO_Disk_sol = new G4Tubs("In_LWO_Disk_sol", In_disk_inner_radius, In_disk_outer_radius, Disk_thickness/2., 0*deg, 360*deg);
    G4UnionSolid *Two_disks_sol = new G4UnionSolid("Two_disks",Out_LWO_Disk_sol,In_LWO_Disk_sol,nullptr,{0.,0.,0});
    
    //disk "connectors"
    G4VSolid* Disk_connector = new G4Tubs("Disk_Connector", In_disk_outer_radius-1*mm, Out_disk_inner_radius+1*mm, Disk_thickness/2., 0*deg, 25*deg); //Add 1mm to the size to fix an error in union from G4.
    G4RotationMatrix *roZ = new G4RotationMatrix(0,0,120*deg);
    G4UnionSolid* Two_Disks_connectors = new G4UnionSolid("Two_Disks_connectors",Disk_connector,Disk_connector,roZ,{0.,0.,0});
    roZ = new G4RotationMatrix(0,0,240.*CLHEP::deg);
    G4UnionSolid* Three_Disks_connectors = new G4UnionSolid("Three_Disks_connectors",Two_Disks_connectors,Disk_connector,roZ,{0.,0.,0});

    G4UnionSolid *Disks_LWO_holder_sol = new G4UnionSolid("Disks_LWO_holder_sol",Two_disks_sol,Three_Disks_connectors,nullptr,{0.,0.,0});

    G4UnionSolid *LWO_holder_sol = new G4UnionSolid(name,Disks_LWO_holder_sol,Disks_LWO_holder_sol,nullptr,{0.,0.,Boxes_gap-Disk_thickness});
    
    // LWO screws
    G4VSolid* LWO_screw = new G4Tubs("LWO_screw", 0., 1., (Boxes_gap-Disk_thickness)/2., 0*deg, 360*deg);
    LWO_holder_sol = new G4UnionSolid(name,LWO_holder_sol,LWO_screw,nullptr,{1.2*13.5,1.2*4.,Boxes_gap/2.});
    LWO_holder_sol = new G4UnionSolid(name,LWO_holder_sol,LWO_screw,nullptr,{1.2*-10.,1.2*9.5,Boxes_gap/2.});
    LWO_holder_sol = new G4UnionSolid(name,LWO_holder_sol,LWO_screw,nullptr,{1.2*-4,1.2*-13.,Boxes_gap/2.});
    
    LWO_holder_sol = new G4UnionSolid(name,LWO_holder_sol,LWO_screw,nullptr,{1.8*13.5,1.8*4.,Boxes_gap/2.});
    LWO_holder_sol = new G4UnionSolid(name,LWO_holder_sol,LWO_screw,nullptr,{1.8*-10.,1.8*9.5,Boxes_gap/2.});
    LWO_holder_sol = new G4UnionSolid(name,LWO_holder_sol,LWO_screw,nullptr,{1.8*-4,1.8*-13.,Boxes_gap/2.});
    
    return LWO_holder_sol;
}
