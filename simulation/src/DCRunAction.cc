//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: ntest2RunAction.cc,v 1.18 2006/06/29 17:49:11 gunter Exp $
// GEANT4 tag $Name: geant4-08-03-patch-01 $
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#include <DCRunAction.hh>

#include <DataDefinition.h>
#include <Parameters.hh>
#include <String.hh>
#include <make_unique.hh>

#include <globals.hh>
#include <G4String.hh>
#include <G4Run.hh>
#include <G4RunManager.hh>
#include <G4UnitsTable.hh>
#include <G4SystemOfUnits.hh>
#include <G4Step.hh>

#include <TFile.h>
#include <TTree.h>
#include <TClonesArray.h>
#include <TH1D.h>
#include <TGraph.h>
#include <TObjString.h>
#include <TVectorT.h>
#include <TVectorDfwd.h>

#include <iostream>
#include <fstream>

#include <G4EmCalculator.hh>
#include <G4ParticleTable.hh> 
#include <G4ParticleDefinition.hh>
#include <G4Neutron.hh>
#include <G4Gamma.hh>
#include <G4Electron.hh>
#include <G4Positron.hh>
#include <G4Proton.hh>
#include <G4ProcessType.hh>
#include <G4EmProcessSubType.hh>
//#include <G4HadronicProcessType.hh>

#include <GLG4Material.hh>
#include <G4LogicalVolumeStore.hh>
#include <G4RegionStore.hh>
#include <G4Region.hh>

template <class T=TString>
T ParticleName(int PDG){
    
    return T(G4ParticleTable::GetParticleTable()->FindParticle(PDG)->GetParticleName());
}

template <class String>
int ProcessNumber(const String& procesName){
    
    int processNumber = 0;
    
    if(procesName == "hadElastic") processNumber =  1e7;
    else if(procesName == "neutronInelastic") processNumber =  2e7;
    else if(procesName == "nCapture") processNumber =  3e7;
    else if(procesName == "nFission") processNumber =  4e7;
    
    return processNumber;
}

int NeutronKey(int PDG, int process){
    
    return PDG + process;
}

DCRunAction::~DCRunAction()
{
    std::cout << " End of processing...\n";
}

DCRunAction::DCRunAction(const G4String& out_file, const G4String& ): FileName(out_file)
{
    std::cout<<"=======================================================\n";
    std::cout<<" +++ Setting the output file +++\n";
    // test output file name
    if ( FileName.IsNull() ) {
        G4Exception("DCRunAction::DCRunAction","1",JustWarning,
                    "No output file name provided, default is  => test_EnerGe_sim.root <=");
        FileName = "test_EnerGe_sim.root";
    }
    std::cout<<" Output file name: "<<FileName<<":\n";
    
    // Set modulo for printing number of gen. events
    G4RunManager::GetRunManager()->SetPrintProgress(50000);
    
    // Initialising the data container
    MCINFO = new MCInfo("MC","MCInfo");
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DCRunAction::BeginOfRunAction(const G4Run*)
{
    std::cout << "\n\n################################\n";
    std::cout << "\tBegin Of Run\n";
    std::cout << "################################\n\n";
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DCRunAction::EndOfRunAction(const G4Run*)
{
    Double_t NbEvtTotal = Double_t(NbEvtGenerator + NbEvtDelayed + NbEvtOther);
    
    std::cout << "\n\n################################\n\tEnd Of Run\n################################\n"
    << "\n  Generator events number: " << NbEvtGenerator << " ("<<Double_t(NbEvtGenerator)/NbEvtTotal*100.<<"%)"
    << "\n  Delayed events number: " << NbEvtDelayed << " ("<<Double_t(NbEvtDelayed)/NbEvtTotal*100.<<"%)"
    << "\n  Other events number: " << NbEvtOther << " ("<<Double_t(NbEvtOther)/NbEvtTotal*100.<<"%)\n\n";
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DCRunAction::Clear()
{
    MCINFO->Clear();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DCRunAction_ROOT::DelTree::operator()(TTree* t)
{
    t->AutoSave("FlushBasket");
    delete t;
}

DCRunAction_ROOT::~DCRunAction_ROOT()
{
    std::cout << " Closing ROOT file\n";
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DCRunAction_ROOT::DCRunAction_ROOT(const G4String& out_file, const G4String& mac_file): DCRunAction(out_file, mac_file)
{
    // Create the Root output file name
    SaveRootFile = make_unique<TFile>(FileName,"RECREATE");
    if(!SaveRootFile->IsOpen()) {
        G4Exception("DCRunAction_ROOT::DCRunAction_ROOT","1",FatalErrorInArgument,
                    scat("Can not open Root file: ",FileName).c_str() );
    }
    
    // Save the mac file
    if(!mac_file.empty()) {
        std::ifstream MacFile(mac_file);
        if(!MacFile)
            G4Exception("DCRunAction_ROOT::DCRunAction_ROOT","2",FatalErrorInArgument, scat("Can not open .mac file: ",mac_file).c_str());
        
        TString Str;
        Str.ReadFile(MacFile);
        TObjString ObjStr(Str);
        ObjStr.Write("MacFile");
    }
    
    // Save the data base
    ParamBase::GetDataBase().WriteRoot(SaveRootFile.get());
    
    // creation of the data structure --
    theTree = make_unique<TTree,DelTree>("data","EnerGe Event Tree");
    theTree->Branch("MC","MCInfo",&MCINFO);
    
    SaveRootFile->ls();
    std::cout<<"============== End of setting output file =============\n";
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

Int_t DCRunAction_ROOT::FillEvent()
{
    SaveRootFile->cd();
    Int_t val = theTree->Fill();
    Clear();
    return val;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


//Record energy deposition for each step : energy recorded in root histogram gathered in the directory EnergyMaps
//void DCRunAction_ROOT::RecordEnergyDeposition(G4int PDG, const G4Step* step)
//{
//    G4Track *aTrack = step->GetTrack();
//    auto aPartName = ParticleName(PDG);
//    G4StepPoint* point1 = step->GetPreStepPoint();
//    G4StepPoint* point2 = step->GetPostStepPoint();
//    TString aProcessName(point2->GetProcessDefinedStep()->GetProcessName());
//    G4String VolumeName = point1->GetTouchableHandle()->GetVolume()->GetName();
//    
//    //Creating the different histograms : for each interaction except for transportation
//    //mEdep : record the energy deposition depending on the type of particle (one histo for each particle)
//    //mEvertex : record the kinetic vertex energy according to the type of particle
//    //mEkinPre : record the kinetic energy of the incident particle before interaction
//    //mEkinPost : record the kinetic energy of the particle after interaction
//    //mEstep : record the kinetic energy change of the particle
//    //mEdepNeutron : record the energy deposition of neutron depending on the type of interaction (see below)
//    //mEkinNeutron : record the pres step kinetic energy of the neutron depending on the type of interaction (see below)
//    if (!mEdep.count(PDG) && VolumeName == "crystal") { //count elements with specific key = PDG
//        mEdep.emplace(PDG, make_unique<TH1D>(
//                                             "nEdep_Crystal_"+aPartName,"Total energy deposition of: "+aPartName,10000,0.,10. ));
//        mEvertex.emplace(PDG, make_unique<TH1D>(
//                                                "nEvertex_Crystal_"+aPartName,"Kinetic energy at creation vertex of: "+aPartName,10000,0.,10. ));
//        mEkinPre.emplace(PDG, make_unique<TH1D>(
//                                                "nEkinPre_Crystal_"+aPartName,"PreStep Kinetic energy of track of: "+aPartName,10000,0.,10. ));
//        mEkinPost.emplace(PDG, make_unique<TH1D>(
//                                                 "nEkinPost_Crystal_"+aPartName,"PostStep Kinetic energy of track of: "+aPartName,10000,0.,10. ));
//        mEstep.emplace(PDG, make_unique<TH1D>(
//                                              "nEstep_Crystal_"+aPartName,"Difference between pre-step point and post-step point kinetic energy of: "+aPartName,10000,0.,10. ));
//    }
//    
//    auto processNumber = ProcessNumber(aProcessName);
//    auto neutronKey = NeutronKey(PDG, processNumber); //key for the map mEdepNeutron including the type of process
//    if (!mEdepNeutron.count(neutronKey) && VolumeName == "crystal" && PDG == 2112){ //energy deposition by neutron
//        mEdepNeutron.emplace(neutronKey, make_unique<TH1D>(
//                                                           "nEdepNeutron_Crystal_"+aPartName+"_"+aProcessName,
//                                                           "Energy deposistion of : "+aPartName+" through the process "+aProcessName, 10000,0.,10.));
//    }
//    if (!mEkinNeutron.count(neutronKey) && VolumeName == "crystal" && PDG == 2112){ //energy deposition by neutron
//        mEkinNeutron.emplace(neutronKey, make_unique<TH1D>(
//                                                           "nEkinNeutron_Crystal_"+aPartName+"_"+aProcessName,
//                                                           "PreStep Kinetic Energy of : "+aPartName+" for the process "+aProcessName, 10000,0.,10.));
//    }
//    //mEdepNeutron also contains all the true energy deposition through the different process involving nuclear recoil
//    //whereas Edep record only the energy deposition saved in step->GetTotalEnergyDeposition()
//    //see below for more information
//    if (!mEdepNeutron.count(PDG) && PDG == 2112 && VolumeName == "crystal"){
//        mEdepNeutron.emplace(PDG, make_unique<TH1D>(
//                                                    "nEdepNeutron_Crystal_"+aPartName,"Brut energy deposition of: "+aPartName,10000,0.,10. ));
//    }
//    if (!mEkinNeutron.count(PDG) && PDG == 2112 && VolumeName == "crystal"){
//        mEkinNeutron.emplace(PDG, make_unique<TH1D>(
//                                                    "nEkinNeutron_Crystal_"+aPartName,"PreStep Kinetic energy of: "+aPartName,10000,0.,10. ));
//    }
//    
//    //filling histograms
//    if (VolumeName == "crystal"){
//        mEkinPre.at(PDG)->Fill(point1->GetKineticEnergy());
//        mEkinPost.at(PDG)->Fill(point2->GetKineticEnergy());
//        mEvertex.at(PDG)->Fill(aTrack->GetVertexKineticEnergy());
//        mEdep.at(PDG)->Fill(step->GetTotalEnergyDeposit());
//        mEstep.at(PDG)->Fill(point1->GetKineticEnergy()-point2->GetKineticEnergy());
//    }
//    
//    //filling EdepNeutron histograms : record the brut recoil energy of the nucleus following one of the three neutron interaction
//    //elastic scattering - inelastic scattering - neutron capture
//    //filling EkinNeutron histograms : record preStep kinetic Energy of the neutron
//    if (VolumeName == "crystal" && PDG == 2112)
//    {
//        mEkinNeutron.at(PDG)->Fill(point1->GetKineticEnergy());
//        mEkinNeutron.at(neutronKey)->Fill(point1->GetKineticEnergy());
//        
//        switch(processNumber){
//            case 10000000 : //elastic scattering
//                /* in the case of an elastic scattering interaction : two energy depositions are possible
//                 *  first : the neutron deposit directly the energy into the lattice
//                 *  second : the neutron gives kinetic energy to a nucleus of the lattice, then the nucleus deposit the
//                 *            energy into the lattice through an ionIonisation interaction
//                 * to include both cases : we record the kinetic energy loss of the neutron */
//                mEdepNeutron.at(PDG)->Fill(point1->GetKineticEnergy()-point2->GetKineticEnergy());
//                mEdepNeutron.at(neutronKey)->Fill(point1->GetKineticEnergy()-point2->GetKineticEnergy());
//                break;
//            case 20000000 : //inelastic scattering
//                /* in the case of an inelastic scattering interaction : the neutron may give kinetic energy to the nucleus
//                 *  of the lattice : then the nucleus deposit its kinetic energy through ionIonisation interaction
//                 * therefore, we need to record the kinetic energy of the nucleus being hit
//                 * we find the track cooresponding to the nucleus hitten and record its kinetic energy (which he will deposit later entirely)
//                 * to do so : we check the creator process as there can only be one neutronInelastic interaction for the track (the track is killed)
//                 */
//            {
//                auto it=step->GetSecondary()->begin();
//                for (auto end=step->GetSecondary()->end(); it!=end; ++it) {
//                    if ( (*it)->GetDynamicParticle() == nullptr
//                        || (*it)->GetDynamicParticle()->GetParticleDefinition()->GetAtomicNumber() < 3
//                        // < 3 not to count Protons and Alpha from (n,p) and (n,alpha) interactions
//                        || (*it)->GetDynamicParticle()->GetParticleDefinition() == G4Neutron::NeutronDefinition()
//                        || (*it)->GetCreatorProcess() == nullptr
//                        || (*it)->GetCreatorProcess()->GetProcessType() != fHadronic // see G4ProcessType.hh
//                        || (*it)->GetCreatorProcess()->GetProcessSubType() != fHadronInelastic // see G4HadronicProcessType.hh
//                        ) { continue; }
//                    mEdepNeutron.at(PDG)->Fill((*it)->GetKineticEnergy());
//                    mEdepNeutron.at(neutronKey)->Fill((*it)->GetKineticEnergy());
//                }
//            }
//                break;
//            case 30000000 : //neutron capture
//                // same as inelastic scattering : the nucleus hitten can get kinetic energy from the interaction
//            {
//                auto it=step->GetSecondary()->begin();
//                for (auto end=step->GetSecondary()->end(); it!=end; ++it) {
//                    if ( (*it)->GetDynamicParticle() == nullptr
//                        || (*it)->GetDynamicParticle()->GetParticleDefinition()->GetAtomicNumber() == 0
//                        || (*it)->GetDynamicParticle()->GetParticleDefinition() == G4Neutron::NeutronDefinition()
//                        || (*it)->GetCreatorProcess() == nullptr
//                        || (*it)->GetCreatorProcess()->GetProcessType() != fHadronic // see G4ProcessType.hh
//                        || (*it)->GetCreatorProcess()->GetProcessSubType() != fCapture // see G4HadronicProcessType.hh
//                        ) { continue; }
//                    mEdepNeutron.at(PDG)->Fill((*it)->GetKineticEnergy());
//                    mEdepNeutron.at(neutronKey)->Fill((*it)->GetKineticEnergy());
//                }
//            }
//                break;
//            default :
//                break;
//        }
//    }
//}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DCRunAction_ROOT::EndOfRunAction(const G4Run*)
{
    DCRunAction::EndOfRunAction(nullptr);
    
    //    SaveRootFile->cd();
    //    SaveRootFile->mkdir("EnergyMaps/Edep");
    //    SaveRootFile->cd("EnergyMaps/Edep");
    //    for (auto &el : mEdep)    { el.second->Write(); el.second.release(); }
    //    SaveRootFile->mkdir("EnergyMaps/Evertex");
    //    SaveRootFile->cd("EnergyMaps/Evertex");
    //    for (auto &el : mEvertex) { el.second->Write(); el.second.release(); }
    //    SaveRootFile->mkdir("EnergyMaps/EkinPre");
    //    SaveRootFile->cd("EnergyMaps/EkinPre");
    //    for (auto &el : mEkinPre)    { el.second->Write(); el.second.release(); }
    //    SaveRootFile->mkdir("EnergyMaps/EkinPost");
    //    SaveRootFile->cd("EnergyMaps/EkinPost");
    //    for (auto &el : mEkinPost)    { el.second->Write(); el.second.release(); }
    //    SaveRootFile->mkdir("EnergyMaps/Estep");
    //    SaveRootFile->cd("EnergyMaps/Estep");
    //    for (auto &el : mEstep)   { el.second->Write(); el.second.release(); }
    //    SaveRootFile->mkdir("EnergyMaps/EdepNeutron");
    //    SaveRootFile->cd("EnergyMaps/EdepNeutron");
    //    for (auto &el : mEdepNeutron)    { el.second->Write(); el.second.release(); }
    //    SaveRootFile->mkdir("EnergyMaps/EkinNeutron");
    //    SaveRootFile->cd("EnergyMaps/EkinNeutron");
    //    for (auto &el : mEkinNeutron)    { el.second->Write(); el.second.release(); }
    
    SaveRootFile->cd();
    
    TVectorD* v = new TVectorD(3);
    (*v)[0] = static_cast<Double_t>(NbEvtGenerator);
    (*v)[1] = static_cast<Double_t>(NbEvtDelayed);
    (*v)[2] = static_cast<Double_t>(NbEvtOther);
    theTree->GetUserInfo()->Add(v);
    
    // clean up the data structure
    // => no need, avoid SEGFAULT
    
    //Save EM process cross-section
    if (ParamBase::GetDataBase().Get<bool>("save_em_processes")) {
        
        //Retrieve list of defined regions in the geometry
        GetListOfRegions();
        
        SaveEMProcesses(G4Gamma::Definition());
        SaveEMProcesses(G4Electron::Definition());
        SaveEMProcesses(G4Positron::Definition());
        SaveEMProcesses(G4Proton::Definition());
    }
}

void DCRunAction_ROOT::GetListOfRegions(){
    
    G4RegionStore *store = G4RegionStore::GetInstance();
    for (const auto &vec : *store) {
        //std::cout << vec->GetName() << std::endl;
        if (vec->GetName() != "DefaultRegionForParallelWorld") {theListOfRegions.push_back(vec);}
    }
}

std::map<G4String,G4int> DCRunAction_ROOT::GetListOfEMProcesses(G4ParticleDefinition* thePart){
    
    G4ProcessManager* pmanager = thePart->GetProcessManager();
    G4ProcessVector *vProc =  pmanager->GetProcessList();
    
    std::map<G4String,G4int> theProcessList;
    
    for(G4int i=0; i<vProc->length(); i++) {
        G4String procName = (*vProc)[i]->GetProcessName();
        if((*vProc)[i]->GetProcessType() == fElectromagnetic && (*vProc)[i]->GetProcessSubType() !=10){ //avoid "msc"
            theProcessList[procName] = (*vProc)[i]->GetProcessSubType();
            //(*vProc)[i]->DumpInfo();
        }
    }
    return theProcessList;
}

void DCRunAction_ROOT::SaveEMProcesses(G4ParticleDefinition* thePart){
    
    std::map<G4String,G4int> theProcessList = GetListOfEMProcesses(thePart);
    
    //Recover EM process quantities for a given particle and save them in a TGraph
    G4EmCalculator emCalculator;
    //emCalculator.SetVerbose(2);
    G4double LogE = std::log10(1e-6);
    G4double logStep = 0.01;
    std::vector<Double_t> Ekin;
    std::vector<Double_t> massSigma;
    TGraph* gEM = nullptr;
    
    TString BaseDirName = "EMProcesses/";
    
    //Loop on G4Regions
    for (auto &it : theListOfRegions){
        
        //Retrieve prodution cuts
        G4ProductionCuts* regCuts = it->GetProductionCuts();
        
        std::vector<G4Material*>::const_iterator mItr = it->GetMaterialIterator();
        size_t nMaterial = it->GetNumberOfMaterials();
        
        //Loop on G4Materials
        for(size_t iMate=0;iMate<nMaterial;iMate++){
            
            if ((*mItr)->GetName() != "Vac"){
                
                //Material density
                //G4double density = (*mItr)->GetDensity();
                
                //Creation of root TDirectory. Structure hierarchy is: 1. region name, 2. material name, 3. particle name
                TString DirName = BaseDirName.Data() + it->GetName() + "/" + (*mItr)->GetName() + "/" + thePart->GetParticleName();
                if(!SaveRootFile->GetDirectory(DirName)){SaveRootFile->mkdir(DirName);}
                SaveRootFile->cd(DirName);
                
                for (const auto &v : theProcessList) {
                    
                    G4String theProcName = v.first;
                    //G4cout << "\t- " << theProcName << G4endl;
                    
                    TString yaxis_title, theGraphTitle;
                    
                    G4double ElectronCut = emCalculator.ComputeEnergyCutFromRangeCut(regCuts->GetProductionCut("e-"),G4Electron::Electron(),(*mItr))*MeV;
                    G4double GammaCut = emCalculator.ComputeEnergyCutFromRangeCut(regCuts->GetProductionCut("gamma"),G4Gamma::Gamma(),(*mItr))*MeV;
                    
                    G4double E = 0;
                    G4double MS = 0;
                    
                    switch (v.second) {
                            
                        case fNuclearStopping:
                            
                            yaxis_title = "dE/dx [MeV mm^{-1}]";
                            theGraphTitle = "dEdx_" + theProcName;
                            
                            while (LogE < 6) {
                                
                                E = std::pow(10,LogE)*MeV;
                                MS = emCalculator.ComputeDEDX(E,thePart,theProcName,(*mItr))*mm/MeV; //NuclearStopping models in G4 do not provide for a 'ComputeCrossSection'method, only dE/dx or stopping range.
                                
                                if (!std::isnan(MS) && MS > 0){
                                    Ekin.push_back(E);
                                    massSigma.push_back(MS);
                                }
                                //G4cout << G4BestUnit(std::pow(10,LogE)*MeV,"Energy") << " " << MS/cm << G4endl;
                                LogE += logStep;
                            }
                            
                            break;
                            
                        case fIonisation:
                            
                            yaxis_title = "Total cross-section [cm^{-1}]";
                            theGraphTitle = "Xsec_" + theProcName;
                            
                            while (LogE < 6) {
                                
                                E = std::pow(10,LogE)*MeV;
                                MS = emCalculator.ComputeCrossSectionPerVolume(E,thePart,theProcName,(*mItr),ElectronCut)*cm;
                                
                                if (!std::isnan(MS) && MS > 0){
                                    Ekin.push_back(E);
                                    massSigma.push_back(MS);
                                }
                                //G4cout << G4BestUnit(std::pow(10,LogE)*MeV,"Energy") << " " << MS/cm << G4endl;
                                LogE += logStep;
                            }
                            
                            break;
                            
                        case fBremsstrahlung:
                            
                            yaxis_title = "Total cross-section [cm^{-1}]";
                            theGraphTitle = "Xsec_" + theProcName;
                            
                            while (LogE < 6) {
                                
                                E = std::pow(10,LogE)*MeV;
                                MS = emCalculator.ComputeCrossSectionPerVolume(E,thePart,theProcName,(*mItr),GammaCut)*cm;
                                
                                if (!std::isnan(MS) && MS > 0){
                                    Ekin.push_back(E);
                                    massSigma.push_back(MS);
                                }
                                //G4cout << G4BestUnit(std::pow(10,LogE)*MeV,"Energy") << " " << MS/cm << G4endl;
                                LogE += logStep;
                            }
                            
                            break;
                            
                        default:
                            yaxis_title = "Total cross-section [cm^{-1}]";
                            theGraphTitle = "Xsec_" + theProcName;
                            
                            while (LogE < 6) {
                                
                                E = std::pow(10,LogE)*MeV;
                                MS = emCalculator.ComputeCrossSectionPerVolume(E,thePart,theProcName,(*mItr))*cm;
                                
                                if (!std::isnan(MS) && MS > 0){
                                    Ekin.push_back(E);
                                    massSigma.push_back(MS);
                                }
                                //G4cout << G4BestUnit(std::pow(10,LogE)*MeV,"Energy") << " " << MS/cm << G4endl;
                                LogE += logStep;
                            }
                            
                            break;
                    }
                    
                    //Set properties of TGraph and write to ROOT file
                    if (Ekin.size() > 0) {
                        
                        gEM = new TGraph(Ekin.size(),&Ekin[0],&massSigma[0]);
                        gEM->SetName(theGraphTitle);
                        gEM->SetTitle(theProcName);
                        gEM->GetXaxis()->SetTitle("Energy [MeV]");
                        gEM->GetYaxis()->SetTitle(yaxis_title);
                        
                        //Write TGraph
                        gEM->Write();
                    }
                    
                    //Reset variables
                    LogE = std::log10(1e-6);
                    Ekin.clear();
                    massSigma.clear();
                } //end of loop on processes
            }
            mItr++;
        } //end of loop on materials
    } // end of loop on regions
    return;
}
