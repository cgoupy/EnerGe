// This file is part of the GenericLAND software library.
// $Id: GLG4SteppingAction.cc,v 1.7 2009/06/26 19:08:56 gahs Exp $
//
//
//  GenericLAND Simulation
//
//  Concrete implementation of G4UserSteppingAction
//
//  Current uses:
//    * Measure inter-step CPU time, broken down by process and particle type
//
//  Anticipated uses:
//    * Find PMT _fast_ when entering outer buffer
//
//  Author: Glenn Horton-Smith, April 7, 2000
//  Modified by Max Fechner for Nucifer, 2009
//  Modified by J. Gaffiot for Nucifer, jan 2013
//  Modified by M. Vivier for Choozerent, mar 2017

#include <DCSteppingAction.hh>

#include <DCEventAction.hh>
#include <DCDumpTrackingInfo.hh>
#include <GLG4Scintillation.hh>
#include <Parameters.hh>
using namespace EnerGe;

#include <globals.hh>
#include <G4PhysicalConstants.hh>
#include <G4OpticalPhoton.hh>
#include <G4Neutron.hh>
#include <G4ProcessType.hh>
#include <G4HadronicProcessType.hh>
#include <G4UIcommand.hh>
#include <G4Step.hh>
#include <G4Track.hh>
#include <G4VParticleChange.hh>
#include <G4DynamicParticle.hh>
//#include <G4OpticalProcessIndex.hh>
#include <G4SteppingManager.hh>


#include <G4OpticalPhoton.hh>
#include <G4ProcessManager.hh>
#include <G4FastSimulationManagerProcess.hh>
#include <G4FastSimulationManager.hh>
#include <G4VFastSimulationModel.hh>

#include <iostream>
using namespace std;


DCSteppingAction::DCSteppingAction(DCEventAction* r): myEventAction(r)
{
    VerboseLevel = static_cast<G4int>(ParamBase::GetDataBase()["events_verboselevel"]);
    new GLG4DebugMessenger_Stepping();
}

#ifdef G4DEBUG
#include <G4Timer.hh>
#include <G4VisAttributes.hh>
#include <G4Color.hh>
#include <G4VisExtent.hh>
#include <G4VSolid.hh>
#include <G4VPhysicalVolume.hh>
#include <G4VProcess.hh>
#include <G4ThreeVector.hh>
#include <map>

struct DCSteppingAction_time_s {
    G4double sumtime;
    G4int stepcount;
    DCSteppingAction_time_s() { sumtime=0.0; stepcount=0; }
    void sum(G4double time) { sumtime+= time; stepcount++; }
};

const G4int nrowIlluminationMap = 600;
const G4int ncolIlluminationMap = 600;

map<G4String,DCSteppingAction_time_s> DCSteppingAction_times;
G4double IlluminationMap[6][nrowIlluminationMap][ncolIlluminationMap][3];

G4double DCSteppingAction_internal_time = 0.0;
G4double DCSteppingAction_totEdep = 0.0;
G4double widthIlluminationMap = -1.0;

void updateIlluminationMap(G4int imap, G4double srow, G4double scol, const G4Color *cp)
{
    G4double *mp= IlluminationMap[imap][(G4int)(srow*nrowIlluminationMap)][(int)(scol*ncolIlluminationMap)];
    mp[0] += cp->GetRed();
    mp[1] += cp->GetGreen();
    mp[2] += cp->GetBlue();
}

G4int DCSteppingAction_dump_times(void)
{
    for (map<G4String,DCSteppingAction_time_s>::iterator i= DCSteppingAction_times.begin();
         i != DCSteppingAction_times.end(); i++) {
        cout << i->first << ' ' << i->second.sumtime << ' ' << i->second.stepcount << endl;
    }
    return 1;
}

G4int DCSteppingAction_dump_IlluminationMap(void)
{
    G4double maxval= 0.0, meanval=0.0;
    G4int nsum= 0;
    {for (G4int kmap=0; kmap<6; kmap++)
        for (G4int irow=0; irow< nrowIlluminationMap; irow++) {
            for (G4int jcol=0; jcol< ncolIlluminationMap; jcol++) {
                for (G4int kcol=0; kcol<3; kcol++) {
                    G4double v= IlluminationMap[kmap][irow][jcol][kcol];
                    if (v > 0.0) {
                        if (v > maxval) { maxval= v; }
                        meanval += v;
                        nsum++;
                    }
                }
            }
        }
    }
    if (maxval == 0.0) {
        cout << "Empty Illumination Map" << endl;
        return 0;
    }
    meanval /= nsum;
    {for (G4int kmap=0; kmap<6; kmap++) {
        static char filename[]="map#.ppm";
        filename[3]= kmap+'0';
        ofstream of(filename);
        of << "P6\n# Illumination map " << kmap << "\n"
        << nrowIlluminationMap << ' ' << nrowIlluminationMap << " 255\n";
        for (G4int irow=0; irow< nrowIlluminationMap; irow++) {
            for (G4int jcol=0; jcol< ncolIlluminationMap; jcol++) {
                for (G4int kcol=0; kcol<3; kcol++) {
                    G4double v=IlluminationMap[kmap][irow][jcol][kcol];
                    unsigned char byte;
                    if (v < meanval) { byte= (unsigned char)(128*v/meanval); }
                    else { byte= (unsigned char)(128.0 + 127.99*(v-meanval)/(maxval-meanval)); }
                    of.put(byte);
                }
            }
        }
        of.close();
    }
    }
    return 1;
}
#endif /* G4DEBUG */

GLG4DebugMessenger_Stepping::GLG4DebugMessenger_Stepping()
{ // illuminationMap
    G4UIcommand* cmd = new G4UIcommand("/glg4debug/dump_illumination_map", this);
    cmd->SetGuidance("Dump a pretty picture of particle hits, for debugging");
}

void GLG4DebugMessenger_Stepping::SetNewValue(G4UIcommand*command, G4String newValues)
{
    if ( command -> GetCommandName() == "dump_illumination_map") {
#ifdef G4DEBUG
        DCSteppingAction_dump_IlluminationMap();
#else
        cout << "No illumination map -- compiled without G4DEBUG" << endl;
#endif
    } else {
        cout <<"Command "<<command->GetCommandName()<<" not recognized with values "<<newValues<<endl;
    }
}

void DCSteppingAction::UserSteppingAction(const G4Step* aStep)
{
#ifdef G4DEBUG
    static G4Timer timer;
    static G4int num_zero_steps_in_a_row=0;
    const G4int DCSteppingAction_MaxStepNumber= 100000;
    timer.Stop();
    G4double dut= timer.GetUserElapsed();
    timer.Start();
    
    // check for nullptr world volume
    if (track->GetVolume() == nullptr) {
        cerr << "DCSteppingAction: Track in nullptr volume, terminating!\n";
        DumpFullTrackInfo(track);
        track->SetTrackStatus( fStopAndKill );
    }
    
    // check for track out of the world
    if (!track->GetNextVolume()){
        track->SetTrackStatus(fStopAndKill);
        cout << "Track killed : out of the world" << endl;
    }
    
    // check for very high number of steps
    if (track->GetCurrentStepNumber() > DCSteppingAction_MaxStepNumber) {
        cerr << "DCSteppingAction: Too many steps for this track, terminating!\n";
        DumpFullTrackInfo(track);
        track->SetTrackStatus( fStopAndKill );
    }
    
    // check for too many zero steps in a row
    if ( (aStep->GetStepLength() <= 0.0 ||
          ( aStep->GetTotalEnergyDeposit()==0.0 && track->GetDynamicParticle()->GetCharge()!=0 ))
        && track->GetCurrentStepNumber() > 1) {
        ++num_zero_steps_in_a_row;
        if (num_zero_steps_in_a_row >= 4) {
            cerr << "DCSteppingAction: Too many zero steps for this track, terminating!" << endl;
            DumpFullTrackInfo(track);
            track->SetTrackStatus( fStopAndKill );
            num_zero_steps_in_a_row= 0;
        }
    } else { num_zero_steps_in_a_row= 0; }
    
    // debugging (timing) info
    static G4String lastParticleName;
    if (dut > 0.0) {
        G4String key;
        G4String particleName= track->GetDefinition()->GetParticleName();
        const G4VProcess
        *preProcess=aStep->GetPreStepPoint()->GetProcessDefinedStep(),
        *postProcess= aStep->GetPostStepPoint()->GetProcessDefinedStep();
        if (preProcess)
            key += preProcess->GetProcessName();
        key += "_";
        if (postProcess)
            key += postProcess->GetProcessName();
        DCSteppingAction_times[key].sum(dut);
        DCSteppingAction_times[particleName].sum(dut);
        key += "_" + particleName;
        if (particleName != lastParticleName) {
            key += "_" + lastParticleName;
            lastParticleName= particleName;
        }
        DCSteppingAction_times[key].sum(dut);
    }
    
    // Particle illumination map
    G4TrackStatus status= track->GetTrackStatus();
    if ( status == fStopAndKill  ||  status == fKillTrackAndSecondaries ) {
        G4double sx, sy, sz;
        if (widthIlluminationMap <= 0.0) {
            const G4VTouchable * t= track->GetTouchable();
            int depth= t->GetHistoryDepth();
            G4VSolid* s= t->GetSolid( depth ); // this should be the world volume
            G4VisExtent vx( s->GetExtent() );
            sx= fabs(vx.GetXmin())+fabs(vx.GetXmax());
            sy= fabs(vx.GetYmin())+fabs(vx.GetYmax());
            sz= fabs(vx.GetZmin())+fabs(vx.GetZmax());
            // take the middle value
            if (sx <= sy && sy <= sz) widthIlluminationMap= sy;
            else if (sy <= sx && sx <= sz) widthIlluminationMap= sx;
            else if (sx <= sz && sz <= sy) widthIlluminationMap= sz;
            else G4Exception("DCSteppingAction: oh dear, transitivity doesn't work.");
        }
        G4ThreeVector postPos= aStep->GetPostStepPoint()->GetPosition();
        sx= 0.5 + postPos.x() / widthIlluminationMap;
        sy= 0.5 + postPos.y() / widthIlluminationMap;
        sz= 0.5 + postPos.z() / widthIlluminationMap;
        if (sx > 0.0 && sy > 0.0 && sz > 0.0 && sx < 1.0 && sy < 1.0 && sz < 1.0) {
            static const G4Color defaultcolor(0.1,0.1,0.1);
            G4VPhysicalVolume *pv;
            const G4VisAttributes *att;
            const G4Color *c;
            ( ((pv=track->GetNextVolume())||(pv=track->GetVolume()))
             && (att=pv->GetLogicalVolume()->GetVisAttributes())
             && (c=&(att->GetColor()))
             ) || (c=&defaultcolor);
            updateIlluminationMap( ((sx<0.5) ? 0 : 1),  1.0-sz,  sy,  c);
            updateIlluminationMap( ((sy<0.5) ? 2 : 3),  1.0-sz,  sx,  c);
            updateIlluminationMap( ((sz<0.5) ? 4 : 5),  1.0-sy,  sx,  c);
        }
    }
    // reset timer; measure our own elapsed time
    timer.Stop();
    DCSteppingAction_internal_time += timer.GetUserElapsed();
    timer.Start();
#endif
    
    
    // do scintillation photons, and also re-emission
    // invoke scintillation process
    if ( GLG4Scintillation::IsUsed() ) {
        G4VParticleChange* pParticleChange = GLG4Scintillation::GenericPostPostStepDoIt(aStep);
        if (VerboseLevel>3) {
            cout << "\n ### Invoking GLG4Scintillation\n";
            pParticleChange->DumpInfo();
        }
        
        // were any secondaries defined?
        G4int iSecondary = pParticleChange->GetNumberOfSecondaries();
        if (iSecondary > 0) {
            // add secondaries to the list
            while ( (iSecondary--) > 0 ) {
                fpSteppingManager->GetfSecondary()->push_back( pParticleChange->GetSecondary(iSecondary) );
            }
        }
        
        // clear ParticleChange
        pParticleChange->Clear();
    }
    
    // Select choosen step for treatment in EventAction and storing in MCInfo
    /* Selected steps are primary particles steps (ParentID==0)
     * and neutron capture to get the isotope (next step after ProcessType==4
     * and ProcessSubType==131, equivalent to ProcessName==nCapture).
     * Deferred tracks are treated in the next event. */
    G4Track* aTrack = aStep->GetTrack();
    
    //record Energy deposition for studies
    //myEventAction->RecordEnergyDeposition(aTrack->GetDynamicParticle()->GetPDGcode(),aStep);
    
    // treat primary particles
    if ( aTrack->GetParentID()==0 ) {
        if ( aTrack->GetCurrentStepNumber()==1 ) {
            if (VerboseLevel>2) {
                cout << "\n ### New primary particle: "<<aTrack->GetDefinition()->GetParticleName().data()<<"\n";
            }
            myEventAction->TreatPrimaryParticleFirstStep(aStep);
        } else {
            if (VerboseLevel>2) {
                cout << "\n # Next step of primary particle: "<<aTrack->GetDefinition()->GetParticleName().data()<<"\n";
            }
            myEventAction->TreatPrimaryParticleNextStep(aStep);
        }
    }
    else
    {
        if (VerboseLevel>2) {
            cout << "\n # Secondary particle\n";
        }
        myEventAction->TreatSecondaryParticleStep(aStep);
    }
    
    // neutron: special final process treatment
    if ( aTrack->GetDynamicParticle()->GetPDGcode() == 2112 // neutron
        && aStep->GetPostStepPoint()->GetProcessDefinedStep() != nullptr
        && aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessType() == fHadronic // see G4ProcessType.hh
        ){
        // neutron capture
        if (aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessSubType() == fCapture) { NeutronCapture(aStep); } // see G4HadronicProcessType.hh
        
        // others neutron reactions, considered as inelastic scatterings
        /* Geant4 considers (n,alpha) or (n,p) reactions as inelastic reactions, so capture by boron usually ends up as inelastic.
         * Standard neutron inelastic scattering: new particle ID in output, so neutron final destiny is hard to track.
         *  Geant4 splits this process as such:
         *  neutron inelastic scattering (NeutronInelastic)
         *  gamma emission (with secondary tracks, such as e- recoil with process eIoni...)
         *  recoil of target nucleus (hIoni)
         *  decay of target nucleus (Decay)
         *  new neutron tracking (with new track ID compared to initial neutron)
         */
        if ( aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessSubType() == fHadronInelastic ) { NeutronInelasticScattering(aStep); } // see G4HadronicProcessType.hh
    }
    if (VerboseLevel>2) {
        // steps following a neutron capture to get the isotope, but not if the step comes from the defer track process
        // the fist step after capture should be the recoil of the nucleus which captured the neutron (so with n+1), but sometimes it's not
        if ( aTrack->GetCreatorProcess()
            && aTrack->GetCreatorProcess()->GetProcessType() == fHadronic // see G4ProcessType.hh
            && aTrack->GetCreatorProcess()->GetProcessSubType() == fCapture // see G4HadronicProcessType.hh
            && aTrack->GetCurrentStepNumber() == 1
            && aStep->GetPostStepPoint()->GetProcessDefinedStep()
            && ( aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessType() != fUserDefined
                // 	|| aStep->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessSubType() != fDeferTrackProcess // fDeferTrackProcess does not exists !
                )
            ) {
            cout << "\n\n ################ Step created by nCapture ################\n";
        }
    }
    
    // debug display
    if (VerboseLevel>4) { DumpFullStepInfo(aStep); }
    else if (VerboseLevel>3) { DumpStepInfo(aStep); }
    else if (VerboseLevel>0) {
        // count steps number (optical and other)
        if (aTrack->GetDefinition()!=G4OpticalPhoton::OpticalPhotonDefinition()) {
            myEventAction->AddNonOpticalPhoton();
        } else { myEventAction->AddOpticalPhoton(); }
    }
    //cout << "End of UserSteppingAction..." << endl;
}

// find the neutron capture resulting nucleus
void DCSteppingAction::NeutronCapture(const G4Step* aStep)
{
    if (VerboseLevel>2) {
        cout << "\n ################ Neutron capture ################\n";
    }
    if (VerboseLevel>3) { DumpStepInfo(aStep); }
    
    if ( aStep->GetSecondary() == 0 ||  aStep->GetSecondary()->size() == 0) {
        myEventAction->TreatNeutronEnding(aStep,nullptr,true);
        return;
    }
    if (VerboseLevel>2) {
        cout << "\n  Candidates for neutron capture: " << aStep->GetSecondary()->size()<<"\n";
    }
    
    auto it=aStep->GetSecondary()->begin();
    for (auto end=aStep->GetSecondary()->end(); it!=end; ++it) {
        if ( (*it)->GetDynamicParticle() == nullptr
            || (*it)->GetDynamicParticle()->GetParticleDefinition()->GetAtomicNumber() == 0
            || (*it)->GetDynamicParticle()->GetParticleDefinition() == G4Neutron::NeutronDefinition()
            || (*it)->GetCreatorProcess() == nullptr
            || (*it)->GetCreatorProcess()->GetProcessType() != fHadronic // see G4ProcessType.hh
            || (*it)->GetCreatorProcess()->GetProcessSubType() != fCapture // see G4HadronicProcessType.hh
            ){ continue; }
        if (VerboseLevel > 2) {
            cout << "\tCapturing nucleus baryon number= " << (*it)->GetDynamicParticle()->GetParticleDefinition()->GetBaryonNumber()
            << "  atomic number= " << (*it)->GetDynamicParticle()->GetParticleDefinition()->GetAtomicNumber()
            << "  PDG code= " << (*it)->GetDynamicParticle()->GetPDGcode()<<"\n";
        } else if (VerboseLevel>2) {
            cout << "\n #### Secondary track n° "<<aStep->GetTrack()->GetTrackID()<<" ####";
            if (VerboseLevel>3) { DumpFullTrackInfo(*it); }
            cout << "\n";
        }
        myEventAction->TreatNeutronEnding(aStep,*it,true);
        break;
    }
    
    if (it == aStep->GetSecondary()->end()) {
        G4Exception("DCSteppingAction::NeutronCapture","0",JustWarning,
                    "#### END OF SECONDARY VECTOR REACHED AFTER NEUTRON ENDING ! ###");
        myEventAction->TreatNeutronEnding(aStep,nullptr,true);
    }
}

// check if neutron inelastic scattering is a (n,baryon) reaction and find the heaviest product in this case
void DCSteppingAction::NeutronInelasticScattering(const G4Step* aStep)
{
    if (VerboseLevel>2) {
        cout << "\n ################ Neutron inelastic scattering ################\n";
    }
    if (VerboseLevel>3) { DumpStepInfo(aStep); }
    
    if ( aStep->GetSecondary() == nullptr ||  aStep->GetSecondary()->size() == 0) { return; }
    if (VerboseLevel>2) {
        cout << "\n  Candidates for inelastic scattering: " << aStep->GetSecondary()->size()<<"\n";
    }
    
    G4Track* heaviest_reaction_product = nullptr;
    for (auto it=aStep->GetSecondary()->begin(), end=aStep->GetSecondary()->end(); it!=end; ++it) {
        if ( (*it)->GetDynamicParticle() == nullptr
            || (*it)->GetDynamicParticle()->GetParticleDefinition()->GetAtomicNumber() == 0
            || (*it)->GetDynamicParticle()->GetParticleDefinition() == G4Neutron::NeutronDefinition()
            || (*it)->GetCreatorProcess() == nullptr
            || (*it)->GetCreatorProcess()->GetProcessType() != fHadronic // see G4ProcessType.hh
            || (*it)->GetCreatorProcess()->GetProcessSubType() != fHadronInelastic // see G4HadronicProcessType.hh
            ) { continue; }
        if (VerboseLevel>2) {
            cout << "\tProduced nucleus baryon number= " << (*it)->GetDynamicParticle()->GetParticleDefinition()->GetBaryonNumber()
            << "  atomic number= " << (*it)->GetDynamicParticle()->GetParticleDefinition()->GetAtomicNumber()
            << "  PDG code= " << (*it)->GetDynamicParticle()->GetPDGcode()<<"\n";
        } else  if (VerboseLevel>2) {
            cout << "\n #### Secondary track n° "<<aStep->GetTrack()->GetTrackID()<<" ####";
            DumpFullTrackInfo(*it);
            cout << "\n";
        }
        if ( heaviest_reaction_product == nullptr
            || ( (*it)->GetDynamicParticle()->GetParticleDefinition()->GetAtomicNumber() >
                heaviest_reaction_product->GetDynamicParticle()->GetParticleDefinition()->GetAtomicNumber() )
            ) { heaviest_reaction_product = *it; }
    }
    if ( heaviest_reaction_product != nullptr ) {
        if (VerboseLevel>2) {
            cout << " Heaviest nucleus found: "<<heaviest_reaction_product->GetDynamicParticle()->GetPDGcode() << "\n";
        }
        myEventAction->TreatNeutronEnding(aStep,heaviest_reaction_product,false);
    }
}
