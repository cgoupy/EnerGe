#include "OrsayCryostat.hh"
#include "DCVolumeHandler.hh"

#include <GLG4Material.hh>
#include <Parameters.hh>

#include <G4Box.hh>
#include <G4Tubs.hh>
#include <G4CutTubs.hh>
#include <G4Orb.hh>
#include <G4Trd.hh>
#include <G4Torus.hh>
#include <G4Polycone.hh>
#include <G4UnionSolid.hh>
#include <G4SubtractionSolid.hh>
#include <G4MultiUnion.hh>

#include <G4LogicalVolume.hh>
#include <G4VPhysicalVolume.hh>
#include <G4PVPlacement.hh>

#include<G4LogicalVolumeStore.hh>

#include <G4RotationMatrix.hh>
#include <G4Transform3D.hh>

//#include <G4SystemOfUnits.hh>

#include <G4VisAttributes.hh>
#include <G4Colour.hh>

#include <iomanip>
#include <list>

OrsayCryostat::OrsayCryostat(): overlap_test(ParamBase::GetDataBase().Get<bool>("overlap_test")),
construction_verbose_level(ParamBase::GetDataBase().Get<int>("construction_verboselevel"))
{
    CryostatElements.clear();
}

G4VSolid* Heater(const G4String& name);
G4VSolid* PulseTube(const G4String& name);

G4LogicalVolume* OrsayCryostat::ConstructCryostat(G4LogicalVolume *mother, const G4ThreeVector &Pos){
    
    // ========= VESSELS ========
    //10mK
    G4double zPla2[10] = {  0.0*mm,   2.5*mm, 2.5*mm, 3.5*mm, 3.5*mm, 5.5*mm, 5.5*mm, 18.0*mm, 18.0*mm, 22.0*mm};
    G4double rInn2[10] = {  0.0*mm,   0.0*mm, 0.0*mm, 0.0*mm, 64.0*mm, 64.0*mm, 64.0*mm, 64.0*mm, 64.0*mm, 64.0*mm};
    G4double rOut2[10] = {108.0*mm, 108.0*mm, 120.0*mm, 120.0*mm, 120.*mm, 120.0*mm, 100.0*mm, 100.0*mm, 78.0*mm, 78.0*mm};
    G4Polycone* c_10mK_Plate_sol = new G4Polycone("c_10mK_Plate_sol", 0*deg, 360*deg, 10, zPla2, rInn2, rOut2);
    G4LogicalVolume* c_10mK_Plate_log = new G4LogicalVolume(c_10mK_Plate_sol, G4Material::GetMaterial("Copper"),"c_10mK_Plate_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_10mK_Plate_log));
    G4ThreeVector LowPos_Plate10mK = Pos;
    new G4PVPlacement(nullptr,LowPos_Plate10mK,c_10mK_Plate_log,"c_10mK_Plate",mother,false,0,overlap_test);
    c_10mK_Plate_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4double zPla1[6] = {  0.0*mm,   2.0*mm, 2.0*mm, 260.0*mm, 260.0*mm, 275.0*mm};
    G4double rInn1[6] = {  0.0*mm,   0.0*mm, 118.0*mm, 118.0*mm, 110.0*mm, 110.0*mm};
    G4double rOut1[6] = {120.0*mm, 120.0*mm, 120.0*mm, 120.0*mm, 120.0*mm, 120.0*mm};
    G4Polycone* c_10mK_Vessel_sol = new G4Polycone("c_10mK_Vessel_sol", 0*deg, 360*deg, 6, zPla1, rInn1, rOut1);
    G4LogicalVolume* c_10mK_Vessel_log = new G4LogicalVolume(c_10mK_Vessel_sol, G4Material::GetMaterial("Copper"),"c_10mK_Vessel_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_10mK_Vessel_log));
    G4ThreeVector LowPos_Vessel10mK = LowPos_Plate10mK+G4ThreeVector(0,0,-275.0+2.5);
    new G4PVPlacement(nullptr,LowPos_Vessel10mK,c_10mK_Vessel_log,"c_10mK_Vessel",mother,false,0,overlap_test);
    c_10mK_Vessel_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4double zPla4[8] = {  0.0*mm,   4.0*mm, 4.0*mm, 11.0*mm, 11.0*mm, 25.0*mm, 25.0*mm, 28.0*mm};
    G4double rInn4[8] = {  0.0*mm,   0.0*mm, 27.0*mm, 27.0*mm, 27.0*mm, 27.0*mm, 0.0*mm, 0.0*mm};
    G4double rOut4[8] = {60.0*mm, 60.0*mm, 34.0*mm, 34.0*mm, 28.*mm, 28.0*mm, 28.0*mm, 28.0*mm};
    G4Polycone* c_10mK_Melt_bot_sol = new G4Polycone("c_10mK_Melt_bot_sol", 0*deg, 360*deg, 8, zPla4, rInn4, rOut4);
    G4LogicalVolume* c_10mK_Melt_bot_log = new G4LogicalVolume(c_10mK_Melt_bot_sol, G4Material::GetMaterial("Copper"),"c_10mK_Melt_bot_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_10mK_Melt_bot_log));
    G4ThreeVector LowPos_Melt_bot = LowPos_Plate10mK+G4ThreeVector(0,0,22.);
    new G4PVPlacement(nullptr,LowPos_Melt_bot,c_10mK_Melt_bot_log,"c_10mK_Melt_bot",mother,false,0,overlap_test);
    c_10mK_Melt_bot_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    //10mk to 100mK
    G4Tubs* c_10mK_100mK_Tub_sol= new G4Tubs("c_10mK_100mK_Tub_sol", 4.*mm, 5.*mm, 65*mm, 0*deg, 360*deg);
    G4LogicalVolume* c_10mK_100mK_Tub_log = new G4LogicalVolume(c_10mK_100mK_Tub_sol, G4Material::GetMaterial("Steel"),"c_10mK_100mK_Tub_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(3,c_10mK_100mK_Tub_log));
    
    new G4PVPlacement(nullptr,LowPos_Melt_bot+G4ThreeVector(7.3,49.5,4.+65.),c_10mK_100mK_Tub_log,"c_10mK_100mK_Tub_1",mother,false,0,overlap_test);
    c_10mK_100mK_Tub_log->SetVisAttributes(GLG4Material::GetVisAttribute("Steel"));
    new G4PVPlacement(nullptr,LowPos_Melt_bot+G4ThreeVector(-46.5,-18.4,4.+65.),c_10mK_100mK_Tub_log,"c_10mK_100mK_Tub_2",mother,false,0,overlap_test);
    new G4PVPlacement(nullptr,LowPos_Melt_bot+G4ThreeVector(39.2,-31.1,4.+65.),c_10mK_100mK_Tub_log,"c_10mK_100mK_Tub_3",mother,false,0,overlap_test);
    
    //100mK
    G4double zPla5[8] = {  0.0*mm,   1.0*mm, 1.0*mm, 4.0*mm, 4.0*mm, 8.0*mm, 8.0*mm, 12.0*mm};
    G4double rInn5[8] = {26.25*mm,   26.25*mm, 0.0*mm, 0.0*mm, 0.0*mm, 0.0*mm, 0.0*mm, 0.0*mm};
    G4double rOut5[8] = {60.0*mm, 60.0*mm, 60.0*mm, 60.0*mm, 32.*mm, 32.0*mm, 26.0*mm, 26.0*mm};
    G4Polycone* c_100mK_Plate_sol = new G4Polycone("c_100mK_Plate_sol", 0*deg, 360*deg, 8, zPla5, rInn5, rOut5);
    G4LogicalVolume* c_100mK_Plate_log = new G4LogicalVolume(c_100mK_Plate_sol, G4Material::GetMaterial("Copper"),"c_100mK_Plate_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_100mK_Plate_log));
    G4ThreeVector LowPos_Plate100mK = LowPos_Melt_bot+G4ThreeVector(0,0,4.+130.);
    new G4PVPlacement(nullptr,LowPos_Plate100mK,c_100mK_Plate_log,"c_100mK_Plate",mother,false,0,overlap_test);
    c_100mK_Plate_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    //100mK-1K
    G4Tubs* c_100mK_1K_Tub_sol= new G4Tubs("c_100mK_1K_Tub_sol", 4.*mm, 5.*mm, 40*mm, 0*deg, 360*deg);
    G4LogicalVolume* c_100mK_1K_Tub_log = new G4LogicalVolume(c_100mK_1K_Tub_sol, G4Material::GetMaterial("Steel"),"c_100mK_1K_Tub_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(3,c_100mK_1K_Tub_log));
    
    new G4PVPlacement(nullptr,LowPos_Plate100mK+G4ThreeVector(7.3,49.5,4.+40.),c_100mK_1K_Tub_log,"c_100mK_1K_Tub_1",mother,false,0,overlap_test);
    c_100mK_1K_Tub_log->SetVisAttributes(GLG4Material::GetVisAttribute("Steel"));
    new G4PVPlacement(nullptr,LowPos_Plate100mK+G4ThreeVector(-46.5,-18.4,4.+40.),c_100mK_1K_Tub_log,"c_100mK_1K_Tub_2",mother,false,0,overlap_test);
    new G4PVPlacement(nullptr,LowPos_Plate100mK+G4ThreeVector(39.2,-31.1,4.+40.),c_100mK_1K_Tub_log,"c_100mK_1K_Tub_3",mother,false,0,overlap_test);
    
    //1K
    G4double zPla6[8] = {0.0*mm,     3.*mm,   3.*mm,   5.*mm,  5.*mm,  8.0*mm,  8.0*mm,  11.*mm};
    G4double rInn6[8] = {0.*mm,      0.*mm,  0.0*mm,  0.0*mm, 0.0*mm,  0.0*mm,  24.0*mm, 24.0*mm};
    G4double rOut6[8] = {23.*mm,    23.*mm, 60.0*mm, 60.0*mm, 75.*mm, 75.0*mm, 25.*mm, 25.*mm};
    G4Polycone* c_1K_Plate_sol = new G4Polycone("c_1K_Plate_sol", 0*deg, 360*deg, 8, zPla6, rInn6, rOut6);
    G4LogicalVolume* c_1K_Plate_log = new G4LogicalVolume(c_1K_Plate_sol, G4Material::GetMaterial("Copper"),"c_1K_Plate_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_1K_Plate_log));
    G4ThreeVector LowPos_Plate1K = LowPos_Plate100mK+G4ThreeVector(0,0,81.);
    new G4PVPlacement(nullptr,LowPos_Plate1K,c_1K_Plate_log,"c_1K_Plate",mother,false,0,overlap_test);
    c_1K_Plate_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4double zPla7[12] = {0.0*mm,   5.*mm,  5.*mm,   20.*mm,  20.*mm,  23.0*mm,  23.0*mm, 26.*mm,  26.*mm,  38*mm, 38.*mm, 41.0*mm};
    G4double rInn7[12] = {25.*mm,   25.*mm, 25.0*mm, 25.0*mm, 11.0*mm, 11.0*mm,  11.0*mm, 11.0*mm, 11.0*mm, 11.0*mm, 0.*mm, 0.*mm};
    G4double rOut7[12] = {34.*mm,   34.*mm, 26.0*mm, 26.0*mm, 26.*mm,  26.0*mm,  17.0*mm, 17.*mm,  13.*mm,  13.*mm, 13.*mm, 13.*mm};
    G4Polycone* c_1K_BoilerBox_sol = new G4Polycone("c_1K_BoilerBox_sol", 0*deg, 360*deg, 12, zPla7, rInn7, rOut7);
    G4LogicalVolume* c_1K_BoilerBox_log = new G4LogicalVolume(c_1K_BoilerBox_sol, G4Material::GetMaterial("Copper"),"c_1K_BoilerBox_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_1K_BoilerBox_log));
    G4ThreeVector LowPos_BoilerBox = LowPos_Plate1K+G4ThreeVector(0,0,8.);
    new G4PVPlacement(nullptr,LowPos_BoilerBox,c_1K_BoilerBox_log,"c_1K_BoilerBox",mother,false,0,overlap_test);
    c_1K_BoilerBox_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4double zPla8[8] = {0.0*mm,   6.*mm,  6.*mm,   7.*mm,       7.*mm,    8.*mm,    8.*mm,  11.*mm};
    G4double rInn8[8] = {64.*mm,   64.*mm, 64.0*mm, 64.*mm,     76.*mm,   76.*mm,  76.0*mm,  76.*mm};
    G4double rOut8[8] = {82.*mm,   82.*mm, 126.*mm, 126.0*mm, 126.0*mm,  126.*mm,  140.*mm, 140.*mm};
    G4Polycone* c_1K_AdaptBride_sol = new G4Polycone("c_1K_AdaptBride_sol", 0*deg, 360*deg, 8, zPla8, rInn8, rOut8);
    G4LogicalVolume* c_1K_AdaptBride_log = new G4LogicalVolume(c_1K_AdaptBride_sol, G4Material::GetMaterial("Copper"),"c_1K_AdaptBride_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_1K_AdaptBride_log));
    G4ThreeVector LowPosAdaptBride1K = LowPos_Plate1K+G4ThreeVector(0,0,-2.);
    new G4PVPlacement(nullptr,LowPosAdaptBride1K,c_1K_AdaptBride_log,"c_1K_AdaptBride",mother,false,0,overlap_test);
    c_1K_AdaptBride_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4Tubs* c_1K_Plate_branches_sol = new G4Tubs("c_1K_Plate_branches_sol", 40*mm, 82.5*mm, 1.5*mm, 0*deg, 360*deg);
    G4LogicalVolume* c_1K_Plate_branches_log = new G4LogicalVolume(c_1K_Plate_branches_sol, G4Material::GetMaterial("Copper"),"c_1K_Plate_branches_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_1K_Plate_branches_log));
    G4ThreeVector CenterPos_Plate1K_branch = LowPos_Plate1K+G4ThreeVector(0,0,73.+1.5*mm);
    new G4PVPlacement(nullptr,CenterPos_Plate1K_branch,c_1K_Plate_branches_log,"c_1K_Plate_branches",mother,false,0,overlap_test);
    c_1K_Plate_branches_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4Tubs* c_1K_Tub_sol= new G4Tubs("c_1K_Tub_sol", 4.*mm, 5.*mm, 32.5*mm, 0*deg, 360*deg);
    G4LogicalVolume* c_1K_Tub_log = new G4LogicalVolume(c_1K_Tub_sol, G4Material::GetMaterial("Steel"),"c_1K_Tub_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(3,c_1K_Tub_log));
    
    new G4PVPlacement(nullptr,LowPos_Plate1K+G4ThreeVector(7.3,49.5,32.5+8.),c_1K_Tub_log,"c_1K_Tub_1",mother,false,0,overlap_test);
    c_1K_Tub_log->SetVisAttributes(GLG4Material::GetVisAttribute("Steel"));
    new G4PVPlacement(nullptr,LowPos_Plate1K+G4ThreeVector(-46.5,-18.4,32.5+8.),c_1K_Tub_log,"c_1K_Tub_2",mother,false,0,overlap_test);
    new G4PVPlacement(nullptr,LowPos_Plate1K+G4ThreeVector(39.2,-31.1,32.5+8.),c_1K_Tub_log,"c_1K_Tub_3",mother,false,0,overlap_test);
    
    G4double zPla17[6] = {0.0*mm,   2.*mm,  2.*mm,   513.*mm,     513.*mm,    528.*mm};
    G4double rInn17[6] = {0.0*mm,   0.0*mm, 138.0*mm, 138.*mm,     130.*mm,   130.*mm};
    G4double rOut17[6] = {140.0*mm,   140.*mm, 140.*mm, 140.*mm, 140.*mm,  140.*mm};
    G4Polycone* c_1K_Vessel_sol = new G4Polycone("c_1K_Vessel_sol", 0*deg, 360*deg, 6, zPla17, rInn17, rOut17);
    G4LogicalVolume* c_1K_Vessel_log = new G4LogicalVolume(c_1K_Vessel_sol, G4Material::GetMaterial("Copper"),"c_1K_Vessel_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_1K_Vessel_log));
    new G4PVPlacement(nullptr,LowPosAdaptBride1K-G4ThreeVector(0,0,520.),c_1K_Vessel_log,"c_1K_Vessel",mother,false,0,overlap_test);
    c_1K_Vessel_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    //4K
    G4double zPla9[6] = {0.0*mm,         6.*mm,     6.*mm,   10.*mm,  10.*mm,    12.*mm};
    G4double rInn9[6] = {127.5*mm,   127.5*mm,  127.5*mm, 127.5*mm,  137.*mm,   137.*mm};
    G4double rOut9[6] = {141.*mm,      141.*mm,   160.*mm, 160.*mm,  160.*mm,   160.*mm};
    G4Polycone* c_4K_AdaptBride_sol = new G4Polycone("c_4K_AdaptBride_sol", 0*deg, 360*deg, 6, zPla9, rInn9, rOut9);
    G4LogicalVolume* c_4K_AdaptBride_log = new G4LogicalVolume(c_4K_AdaptBride_sol, G4Material::GetMaterial("Copper"),"c_4K_AdaptBride_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_4K_AdaptBride_log));
    G4ThreeVector LowPosAdaptBride_4K = CenterPos_Plate1K_branch+G4ThreeVector(0,0,85.5*mm);
    new G4PVPlacement(nullptr,LowPosAdaptBride_4K,c_4K_AdaptBride_log,"c_4K_AdaptBride",mother,false,0,overlap_test);
    c_4K_AdaptBride_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    //equip
    G4Tubs* c_eq_Crown_sol= new G4Tubs("c_eq_Crown_sol", 105.*mm, 125.*mm, 3.*mm, 0*deg, 360*deg);
    G4LogicalVolume* c_eq_Crown_log = new G4LogicalVolume(c_eq_Crown_sol, G4Material::GetMaterial("Copper"),"c_eq_Crown_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_eq_Crown_log));
    G4ThreeVector CenterPosCrown = LowPosAdaptBride_4K+G4ThreeVector(0,0,87.258+3*mm);
    new G4PVPlacement(nullptr,CenterPosCrown,c_eq_Crown_log,"c_eq_Crown",mother,false,0,overlap_test);
    c_eq_Crown_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4Tubs* c_eq_Crown2_sol= new G4Tubs("c_eq_Crown2_sol", 71.5*mm, 102.*mm, 3.*mm, 0*deg, 360*deg);
    G4LogicalVolume* c_eq_Crown2_log = new G4LogicalVolume(c_eq_Crown2_sol, G4Material::GetMaterial("Copper"),"c_eq_Crown2_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_eq_Crown2_log));
    G4ThreeVector CenterPosCrown_2 = CenterPosCrown+G4ThreeVector(0,0,132.5*mm);
    new G4PVPlacement(nullptr,CenterPosCrown_2,c_eq_Crown2_log,"c_eq_Crown2",mother,false,0,overlap_test);
    c_eq_Crown2_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4Tubs* c_eq_Top_Ring_sol = new G4Tubs("c_eq_Top_Ring_sol", 122.5*mm, 135.*mm, 2.*mm, 0*deg, 360*deg);
    G4LogicalVolume* c_eq_Top_Ring_log = new G4LogicalVolume(c_eq_Top_Ring_sol, G4Material::GetMaterial("Steel"),"c_eq_Top_Ring_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_eq_Top_Ring_log));
    G4ThreeVector CenterPosTop_Ring = CenterPosCrown_2+G4ThreeVector(0,0,117.2*mm);
    new G4PVPlacement(nullptr,CenterPosTop_Ring,c_eq_Top_Ring_log,"c_eq_Top_Ring",mother,false,0,overlap_test);
    c_eq_Top_Ring_log->SetVisAttributes(GLG4Material::GetVisAttribute("Steel"));
    
    G4Tubs* c_eq_Bot_Tub_sol= new G4Tubs("c_eq_Bot_Tub_sol", 6*mm, 7.*mm, 70.7*mm, 0*deg, 360*deg);
    G4LogicalVolume* c_eq_Bot_Tub_log = new G4LogicalVolume(c_eq_Bot_Tub_sol, G4Material::GetMaterial("Steel"),"c_eq_Bot_Tub_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(3,c_eq_Bot_Tub_log));
    new G4PVPlacement(nullptr,CenterPosCrown-G4ThreeVector(114.3,13,70.7+3),c_eq_Bot_Tub_log,"c_eq_Bot_Tub_1",mother,false,0,overlap_test);
    c_eq_Bot_Tub_log->SetVisAttributes(GLG4Material::GetVisAttribute("Steel"));
    new G4PVPlacement(nullptr,CenterPosCrown-G4ThreeVector(-92.4,68.4,70.7+3),c_eq_Bot_Tub_log,"c_eq_Bot_Tub_2",mother,false,0,overlap_test);
    new G4PVPlacement(nullptr,CenterPosCrown-G4ThreeVector(-45.9,-105.5,70.7+3),c_eq_Bot_Tub_log,"c_eq_Bot_Tub_3",mother,false,0,overlap_test);
    
    G4double zPla_Tub[6] = {0.0*mm, 41.7*mm,   41.7*mm,   47.7*mm,  47.7*mm,  157.2*mm};
    G4double rInn_Tub[6] = {6.*mm,    6.*mm,   0.*mm,     0.*mm,    11.9*mm,  11.9*mm};
    G4double rOut_Tub[6] = {7.*mm,    7.*mm,   12.5*mm,   12.5*mm,  12.5*mm,  12.5*mm};
    G4Polycone* c_eq_Mid_Tub_sol = new G4Polycone("c_eq_Mid_Tub_sol", 0*deg, 360*deg, 6, zPla_Tub, rInn_Tub, rOut_Tub);
    G4LogicalVolume* c_eq_Mid_Tub_log = new G4LogicalVolume(c_eq_Mid_Tub_sol, G4Material::GetMaterial("Steel"),"c_eq_Mid_Tub_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(3,c_eq_Mid_Tub_log));
    new G4PVPlacement(nullptr,CenterPosCrown+G4ThreeVector(-114.3,-13,3.),c_eq_Mid_Tub_log,"c_eq_Mid_Tub_1",mother,false,0,overlap_test);
    c_eq_Mid_Tub_log->SetVisAttributes(GLG4Material::GetVisAttribute("Steel"));
    new G4PVPlacement(nullptr,CenterPosCrown+G4ThreeVector(92.4,-68.4,3.),c_eq_Mid_Tub_log,"c_eq_Mid_Tub_2",mother,false,0,overlap_test);
    new G4PVPlacement(nullptr,CenterPosCrown+G4ThreeVector(45.9,105.5,3.),c_eq_Mid_Tub_log,"c_eq_Mid_Tub_3",mother,false,0,overlap_test);
    
    G4Tubs* c_eq_Top_Tub_sol= new G4Tubs("c_eq_Top_Tub_sol", 11.9*mm, 12.5*mm, 42.25*mm, 0*deg, 360*deg);
    G4LogicalVolume* c_eq_Top_Tub_log = new G4LogicalVolume(c_eq_Top_Tub_sol, G4Material::GetMaterial("Steel"),"c_eq_Top_Tub_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(3,c_eq_Top_Tub_log));
    new G4PVPlacement(nullptr,CenterPosCrown_2+G4ThreeVector(-114.3,-13,72.95),c_eq_Top_Tub_log,"c_eq_Top_Tub_1",mother,false,0,overlap_test);
    c_eq_Top_Tub_log->SetVisAttributes(GLG4Material::GetVisAttribute("Steel"));
    new G4PVPlacement(nullptr,CenterPosCrown_2+G4ThreeVector(92.4,-68.4,72.95),c_eq_Top_Tub_log,"c_eq_Top_Tub_2",mother,false,0,overlap_test);
    new G4PVPlacement(nullptr,CenterPosCrown_2+G4ThreeVector(45.9,105.5,72.95),c_eq_Top_Tub_log,"c_eq_Top_Tub_3",mother,false,0,overlap_test);
    
    //4K bis
    G4double zPla16[8] = {0.0*mm,        2.5*mm,     2.5*mm,   4.*mm,  4.*mm,    9.2*mm, 9.2*mm, 12.5*mm};
    G4double rInn16[8] = {80.25*mm,   80.25*mm,       0.*mm,   0.*mm,  0.*mm,   0.*mm, 0.*mm, 0.*mm};
    G4double rOut16[8] = {136.*mm,      136.*mm,     136.*mm, 136.*mm,  90.*mm,   90.*mm, 63.35*mm, 63.35*mm};
    G4Polycone* c_4K_Full_Plate_0 = new G4Polycone("c_4K_Full_Plate_0", 0*deg, 360*deg, 8, zPla16, rInn16, rOut16);
    G4SubtractionSolid* c_4K_Full_Plate_1 = new G4SubtractionSolid("c_4K_Full_Plate_1",c_4K_Full_Plate_0,c_eq_Bot_Tub_sol,nullptr,G4ThreeVector(-114.3,-13,0.));
    G4SubtractionSolid* c_4K_Full_Plate_2 = new G4SubtractionSolid("c_4K_Full_Plate_2",c_4K_Full_Plate_1,c_eq_Bot_Tub_sol,nullptr,G4ThreeVector(45.9,105.5,0.));
    G4SubtractionSolid* c_4K_Plate_sol = new G4SubtractionSolid("c_4K_Plate_sol",c_4K_Full_Plate_2,c_eq_Bot_Tub_sol,nullptr,G4ThreeVector(92.4,-68.4,0.));
    
    G4LogicalVolume* c_4K_Plate_log = new G4LogicalVolume(c_4K_Plate_sol, G4Material::GetMaterial("Copper"),"c_4K_Plate_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_4K_Plate_log));
    G4ThreeVector LowPosPlate_4K = LowPosAdaptBride_4K+G4ThreeVector(0,0,10.*mm);
    new G4PVPlacement(nullptr,LowPosPlate_4K,c_4K_Plate_log,"c_4K_Plate",mother,false,0,overlap_test);
    c_4K_Plate_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4double zPla15[12] = {0.0*mm,     12.*mm,     12.*mm,   37.*mm,  37.*mm,    184.3*mm, 184.3*mm, 209.3*mm, 209.3*mm, 221.3*mm, 221.3*mm, 223.8*mm};
    G4double rInn15[12] = {63.5*mm,   63.5*mm,  63.5*mm,    63.5*mm,  63.5*mm,   63.5*mm,  63.5*mm,  63.5*mm, 63.5*mm, 63.5*mm, 63.5*mm, 63.5*mm};
    G4double rOut15[12] = {78.5*mm,   78.5*mm,   67.*mm,     67.*mm,  64.1*mm,   64.1*mm,  67.*mm,  67.*mm, 78.5*mm, 78.5*mm, 65.*mm, 65.*mm};
    G4Polycone* c_4K_Screen_sol = new G4Polycone("c_4K_Screen_sol", 0*deg, 360*deg, 12, zPla15, rInn15, rOut15);
    G4LogicalVolume* c_4K_Screen_log = new G4LogicalVolume(c_4K_Screen_sol, G4Material::GetMaterial("Steel"),"c_4K_Screen_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_4K_Screen_log));
    G4ThreeVector LowPosScreen_4K = LowPosPlate_4K+G4ThreeVector(0,0,9.2*mm);
    new G4PVPlacement(nullptr,LowPosScreen_4K,c_4K_Screen_log,"Screen_4K",mother,false,0,overlap_test);
    c_4K_Screen_log->SetVisAttributes(GLG4Material::GetVisAttribute("Steel"));
    
    G4double zPla18[6] = {0.0*mm,   2.*mm,  2.*mm,   691.*mm,      691.*mm,    706.*mm};
    G4double rInn18[6] = {0.0*mm,   0.0*mm, 158.0*mm, 158.*mm,     145.*mm,   145.*mm};
    G4double rOut18[6] = {160.0*mm,   160.*mm, 160.*mm, 160.*mm,   160.*mm,  160.*mm};
    G4Polycone* c_4K_Vessel_sol = new G4Polycone("c_4K_Vessel_sol", 0*deg, 360*deg, 6, zPla18, rInn18, rOut18);
    G4LogicalVolume* c_4K_Vessel_log = new G4LogicalVolume(c_4K_Vessel_sol, G4Material::GetMaterial("Copper"),"c_4K_Vessel_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_4K_Vessel_log));
    new G4PVPlacement(nullptr,LowPosAdaptBride_4K-G4ThreeVector(0,0,700.*mm),c_4K_Vessel_log,"c_4K_Vessel",mother,false,0,overlap_test);
    c_4K_Vessel_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    //77k
    G4Tubs* c_77K_Plate_sol= new G4Tubs("c_77K_Plate_sol", 79*mm, 147.3*mm, 1.5*mm, 0*deg, 360*deg);
    G4LogicalVolume* c_77K_Plate_log = new G4LogicalVolume(c_77K_Plate_sol, G4Material::GetMaterial("Copper"),"c_77K_Plate_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_77K_Plate_log));
    G4ThreeVector CenterPosPlate77K = CenterPosCrown_2+G4ThreeVector(0,0,29.2*mm);
    new G4PVPlacement(nullptr,CenterPosPlate77K,c_77K_Plate_log,"Plate77K",mother,false,0,overlap_test);
    c_77K_Plate_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4double zPla10[2] = {0.0*mm,     20.*mm};
    G4double rInn10[2] = {48.5*mm,   48.5*mm};
    G4double rOut10[2] = {51.5*mm,   51.5*mm};
    G4Polycone* c_77K_Bride_int = new G4Polycone("c_77K_Bride_int", 0*deg, 360*deg, 2, zPla10, rInn10, rOut10);
    
    G4double zPla10b[10] = {0.0*mm,     3.5*mm,  3.5*mm, 10.5*mm, 10.5*mm,   13.5*mm, 13.5*mm,  17.5*mm,    17.5*mm, 37.5*mm};
    G4double rInn10b[10] = {65.2*mm,   65.2*mm,  63.5*mm, 63.5*mm, 48.5*mm,  48.5*mm,   48.5*mm,  48.5*mm, 77.1*mm, 77.1*mm};
    G4double rOut10b[10] = {78.5*mm,   78.5*mm,  78.5*mm, 78.5*mm, 78.5*mm, 78.5*mm,  94.5*mm,   94.5*mm,   80.*mm,    80.*mm};
    G4Polycone* c_77K_Bride_ext = new G4Polycone("c_77K_Bride_ext", 0*deg, 360*deg, 8, zPla10b, rInn10b, rOut10b);
    
    G4VSolid* c_77K_Bride_sol = new G4UnionSolid("c_77K_Bride_sol",c_77K_Bride_ext,c_77K_Bride_int,nullptr,G4ThreeVector(0., 0., -9.5));
    G4LogicalVolume* c_77K_Bride_log = new G4LogicalVolume(c_77K_Bride_sol, G4Material::GetMaterial("Copper"),"c_77K_Bride_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_77K_Bride_log));
    new G4PVPlacement(nullptr,LowPosScreen_4K+G4ThreeVector(0,0,221.3*mm),c_77K_Bride_log,"c_77K_Bride",mother,false,0,overlap_test);
    c_77K_Bride_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    //100K
    G4double zPla19[8] = {  0.*mm,     10.*mm, 10.*mm, 190.*mm, 190.*mm, 190*mm, 190.*mm, 200.*mm};
    G4double rInn19[8] = {  158.*mm,   158.*mm, 158.*mm, 158.*mm, 128.5*mm, 128.5*mm, 128.5*mm, 128.5*mm};
    G4double rOut19[8] = { 180.*mm,    180.*mm, 180.*mm, 180.*mm, 180.*mm, 180.*mm, 180.*mm, 180.*mm};
    G4Polycone* c_100K_VesselTop_sol = new G4Polycone("c_100K_VesselTop_sol", 0*deg, 360*deg, 8, zPla19, rInn19, rOut19);
    G4LogicalVolume* c_100K_VesselTop_log = new G4LogicalVolume(c_100K_VesselTop_sol, G4Material::GetMaterial("Copper"),"c_100K_VesselTop_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_100K_VesselTop_log));
    G4ThreeVector LowPosVesselTop100K = CenterPosPlate77K-G4ThreeVector(0.,0.,201.5*mm);
    new G4PVPlacement(nullptr,LowPosVesselTop100K,c_100K_VesselTop_log,"c_100K_VesselTop",mother,false,0,overlap_test);
    c_100K_VesselTop_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4double zPla20[6] = {  0.*mm,     2.*mm, 2.*mm, 750.*mm, 750.*mm, 765.*mm};
    G4double rInn20[6] = {  0.*mm,      0.*mm, 178.*mm, 178.*mm, 170.*mm, 170.*mm};
    G4double rOut20[6] = { 180.*mm,    180.*mm, 180.*mm, 180.*mm, 180.*mm, 180.*mm};
    G4Polycone* c_100K_VesselBot_sol = new G4Polycone("c_100K_VesselBot_sol", 0*deg, 360*deg, 6, zPla20, rInn20, rOut20);
    G4LogicalVolume* c_100K_VesselBot_log = new G4LogicalVolume(c_100K_VesselBot_sol, G4Material::GetMaterial("Copper"),"c_100K_VesselBot_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_100K_VesselTop_log));
    new G4PVPlacement(nullptr,LowPosVesselTop100K-G4ThreeVector(0.,0.,765.),c_100K_VesselBot_log,"c_100K_VesselBot",mother,false,0,overlap_test);
    c_100K_VesselBot_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    //300K
    G4double zPla12[6] = {  0.*mm,   8.*mm, 8.*mm, 82.*mm, 82*mm, 90*mm};
    G4double rInn12[6] = {  96.*mm,   96.*mm, 96.*mm, 96.*mm, 96*mm, 96*mm};
    G4double rOut12[6] = { 101.*mm, 101.*mm, 98*mm, 98.*mm, 109*mm, 109*mm};
    G4Polycone* c_300K_Ensemble_sol = new G4Polycone("c_300K_Ensemble_sol", 0*deg, 360*deg, 6, zPla12, rInn12, rOut12);
    G4LogicalVolume* c_300K_Ensemble_log = new G4LogicalVolume(c_300K_Ensemble_sol, G4Material::GetMaterial("Aluminium"),"c_300K_Ensemble_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_300K_Ensemble_log));
    G4ThreeVector LowPosEnsemble300K = CenterPosPlate77K+G4ThreeVector(0.,0.,15.5*mm);
    new G4PVPlacement(nullptr,LowPosEnsemble300K,c_300K_Ensemble_log,"c_300K_Ensemble",mother,false,0,overlap_test);
    c_300K_Ensemble_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));
    
    G4double zPla13[8] = {  0.*mm,   3.5*mm, 3.5*mm, 7.5*mm, 7.5*mm, 37.5*mm, 37.5*mm, 47.5*mm};
    G4double rInn13[8] = {  76.5*mm,   76.5*mm, 75.*mm, 75.*mm, 128.5*mm, 128.5*mm, 128.5*mm, 128.5*mm};
    G4double rOut13[8] = { 129.5*mm, 129.5*mm, 129.5*mm, 129.5*mm, 129.5*mm, 129.5*mm, 147.5*mm, 147.5*mm};
    G4Polycone* c_300K_TankTop_sol = new G4Polycone("c_300K_TankTop_sol", 0*deg, 360*deg, 8, zPla13, rInn13, rOut13);
    G4LogicalVolume* c_300K_TankTop_log = new G4LogicalVolume(c_300K_TankTop_sol, G4Material::GetMaterial("Aluminium"),"c_300K_TankTop_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_300K_TankTop_log));
    G4ThreeVector LowPosTankTop = LowPosEnsemble300K+G4ThreeVector(0.,0.,90*mm);
    new G4PVPlacement(nullptr,LowPosTankTop,c_300K_TankTop_log,"c_300K_TankTop",mother,false,0,overlap_test);
    c_300K_TankTop_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));
    
    G4double zPla22[4] = {  0.*mm,   12.*mm, 12.*mm, 22.*mm};
    G4double rInn22[4] = {  150.*mm,   150.*mm, 75.*mm, 75.*mm};
    G4double rOut22[4] = { 260.*mm, 260.*mm, 175.*mm, 175.*mm};
    G4Polycone* c_300K_Plate_sol = new G4Polycone("c_300K_Plate_sol", 0*deg, 360*deg, 4, zPla22, rInn22, rOut22);
    G4LogicalVolume* c_300K_Plate_log = new G4LogicalVolume(c_300K_Plate_sol, G4Material::GetMaterial("Aluminium"),"c_300K_Plate_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_300K_Plate_log));
    G4ThreeVector LowPosPlate300K = LowPosTankTop+G4ThreeVector(0.,0.,47.5*mm);
    new G4PVPlacement(nullptr,LowPosPlate300K,c_300K_Plate_log,"c_300K_Plate",mother,false,0,overlap_test);
    c_300K_Plate_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));
    
    G4double zPla21[8] = {  0.*mm,     10.*mm, 10.*mm, 140.*mm, 140.*mm, 455.*mm, 455.*mm, 470.*mm};
    G4double rInn21[8] = {  200.*mm,   200.*mm, 200.*mm, 200.*mm, 200.*mm, 200.*mm, 220.*mm, 220.*mm};
    G4double rOut21[8] = { 222.*mm,    220.*mm, 202.*mm, 202.*mm, 222.*mm, 222.*mm, 248.*mm, 248.*mm};
    G4Polycone* c_300K_VesselTop_sol = new G4Polycone("c_300K_VesselTop_sol", 0*deg, 360*deg, 8, zPla21, rInn21, rOut21);
    G4LogicalVolume* c_300K_VesselTop_log = new G4LogicalVolume(c_300K_VesselTop_sol, G4Material::GetMaterial("Aluminium"),"c_300K_VesselTop_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_300K_VesselTop_log));
    G4ThreeVector LowPosVesselTop300K = LowPosPlate300K-G4ThreeVector(0.,0.,470.*mm);
    new G4PVPlacement(nullptr,LowPosVesselTop300K,c_300K_VesselTop_log,"c_300K_VesselTop",mother,false,0,overlap_test);
    c_300K_VesselTop_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));
    
    G4double zPla23[6] = {  0.*mm,     10.*mm,  10.*mm, 660.*mm, 660.*mm, 675.*mm};
    G4double rInn23[6] = {  0.*mm,     0*mm,    200.*mm, 200.*mm, 200.*mm, 200.*mm};
    G4double rOut23[6] = { 202.*mm,    202.*mm, 202.*mm, 202.*mm, 220.*mm, 220.*mm};
    G4Polycone* c_300K_VesselBot_sol = new G4Polycone("c_300K_VesselBot_sol", 0*deg, 360*deg, 6, zPla23, rInn23, rOut23);
    G4LogicalVolume* c_300K_VesselBot_log = new G4LogicalVolume(c_300K_VesselBot_sol, G4Material::GetMaterial("Aluminium"),"c_300K_VesselBot_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_300K_VesselBot_log));
    new G4PVPlacement(nullptr,LowPosVesselTop300K-G4ThreeVector(0.,0.,675.),c_300K_VesselBot_log,"c_300K_VesselBot",mother,false,0,overlap_test);
    c_300K_VesselBot_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));
    
    //================ PULSE TUBE =======================
    G4VSolid* c_PT_Heater_sol = Heater("c_PT_Heater_sol");
    G4LogicalVolume* c_PT_Heater_log = new G4LogicalVolume(c_PT_Heater_sol, G4Material::GetMaterial("Copper"),"c_PT_Heater_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_PT_Heater_log));
    G4ThreeVector BotPosCapHeater= CenterPosCrown-G4ThreeVector(0,0,5.242+6.*mm);
    new G4PVPlacement(nullptr,BotPosCapHeater,c_PT_Heater_log,"c_PT_Heater",mother,false,0,overlap_test);
    c_PT_Heater_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4VSolid* c_PT_PulseTube_sol = PulseTube("c_PT_PulseTube_sol");
    G4LogicalVolume* c_PT_PulseTube_log = new G4LogicalVolume(c_PT_PulseTube_sol, G4Material::GetMaterial("Copper"),"c_PT_PulseTube_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_PT_PulseTube_log));
    new G4PVPlacement(nullptr,BotPosCapHeater+G4ThreeVector(0,0,7.5*mm),c_PT_PulseTube_log,"c_PT_PulseTube",mother,false,0,overlap_test);
    c_PT_PulseTube_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4double zPla11[4] = {  0.0*mm,   10.0*mm, 10.0*mm, 19.7*mm};
    G4double rInn11[4] = {  75.5*mm,   75.5*mm, 75.5*mm, 75.5*mm};
    G4double rOut11[4] = {77.*mm, 77.*mm, 92.5*mm, 92.5*mm};
    G4Polycone* c_PT_High_bride_sol = new G4Polycone("c_PT_High_bride_sol", 0*deg, 360*deg, 4, zPla11, rInn11, rOut11);
    G4LogicalVolume* c_PT_High_bride_log = new G4LogicalVolume(c_PT_High_bride_sol, G4Material::GetMaterial("Copper"),"c_PT_High_bride_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_PT_High_bride_log));
    new G4PVPlacement(nullptr,CenterPosPlate77K+G4ThreeVector(0.,0., 82.5*mm),c_PT_High_bride_log,"c_PT_High_bride",mother,false,0,overlap_test);
    c_PT_High_bride_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4Tubs* c_PT_Roul_soul_sol= new G4Tubs("c_PT_Roul_soul_sol", 75*mm, 75.5*mm, 44.5*mm, 0*deg, 360*deg);
    G4LogicalVolume* c_PT_Roul_soul_log = new G4LogicalVolume(c_PT_Roul_soul_sol, G4Material::GetMaterial("Copper"),"c_PT_Roul_soul_log");
    CryostatElements.push_back(std::pair<G4int,G4LogicalVolume*>(1,c_PT_Roul_soul_log));
    new G4PVPlacement(nullptr,CenterPosPlate77K+G4ThreeVector(0,0,52.),c_PT_Roul_soul_log,"c_PT_Roul_soul",mother,false,0,overlap_test);
    c_PT_Roul_soul_log->SetVisAttributes(GLG4Material::GetVisAttribute("Copper"));
    
    G4double zPla3[5] = {  0.0*mm,   0.0*mm, cryostat_interior_height-12.5, cryostat_interior_height-12.5, cryostat_interior_height};
    G4double rInn3[5] = {  0.0*mm,   0.0*mm, 0.0*mm, 0.0*mm, 0.0*mm};
    G4double rOut3[5] = {0.0*mm, cryostat_interior_radius, cryostat_interior_radius, cryostat_interior_radius-8, cryostat_interior_radius-8};
    G4Polycone* cryostat_int_sol = new G4Polycone("cryostat_int_sol", 0*deg, 360*deg, 5, zPla3, rInn3, rOut3);
    G4LogicalVolume* cryostat_int_log = new G4LogicalVolume(cryostat_int_sol, G4Material::GetMaterial("Vac"),"cryostat_int_log");
    
    new G4PVPlacement(nullptr,Pos+G4ThreeVector(0,0,-cryostat_interior_height),cryostat_int_log,"VHphysic_cryostat_int",mother,false,0,overlap_test);
    cryostat_int_log->SetVisAttributes(GLG4Material::GetVisAttribute("Vac"));
    
    return cryostat_int_log;
}

void OrsayCryostat::AddCryostatRack(G4LogicalVolume* mother, const G4ThreeVector &Pos) {
    
    if(construction_verbose_level > 0) std::cout << "\n   ******************* Cryostat Rack construction enabled *******************\n";

    G4Transform3D tr3d;
    G4RotationMatrix ro0 = G4RotationMatrix(), RoYp45(90.0*deg,45.0*deg,90.0*deg), RoYm45(90.0*deg,315.0*deg,90.0*deg),
      RoXp45(0,45.0*deg,0), RoXm45(0,315.0*deg,0), RoYp90(90.0*deg,90.0*deg,90.0*deg);


    const double front_width       = 0.7*1000.0*mm;
    const double side_width        = 0.5*1868.0*mm;

    const double bar_width         =   0.7*90.0*mm;
    const double bar_depth         =  0.7*180.0*mm;
    const double bar_height        = 0.7*1909.0*mm;

    const double bar_spacing       =  0.7*586.0*mm;

    const double bar_diag1         =  0.7*420.0*mm;
    const double bar_diag2         =  0.7*438.0*mm;

    G4Box* vb1 = new G4Box("vb1", bar_depth/2., bar_width/2., bar_height/2.);                 // Vertical long bar
    G4Box* vb2 = new G4Box("vb2", bar_depth/2., front_width/2.-bar_width, bar_width/2.);      // Horizontal middle bar
    G4Box* vb3 = new G4Box("vb3", bar_depth/2., front_width/2.-bar_depth, bar_width/2.);      // top front bar
    G4Box* vb4 = new G4Box("vb4", side_width/2., bar_depth/2., bar_width/2.);                 // top lateral bar

    G4Trd* tb1 = new G4Trd("tb1", bar_diag1/2., bar_diag1/2.-bar_width, bar_width/2., bar_width/2., bar_width/2.);
    G4Trd* tb2 = new G4Trd("tb2", bar_depth/2., bar_depth/2., bar_diag2/2., bar_diag2/2.-bar_width/2., bar_width/4.);

    G4MultiUnion* IntRack_sol = new G4MultiUnion("VHsolid_CryostatRack");

    // Vertical bars
    IntRack_sol->AddNode(*vb1, tr3d = G4Transform3D(ro0, { +side_width/2.-bar_depth/2., +front_width/2.-bar_width/2., 0}));
    IntRack_sol->AddNode(*vb1, tr3d = G4Transform3D(ro0, { +side_width/2.-bar_depth/2., -front_width/2.+bar_width/2., 0}));
    IntRack_sol->AddNode(*vb1, tr3d = G4Transform3D(ro0, { -side_width/2.+bar_depth/2., +front_width/2.-bar_width/2., 0}));
    IntRack_sol->AddNode(*vb1, tr3d = G4Transform3D(ro0, { -side_width/2.+bar_depth/2., -front_width/2.+bar_width/2., 0}));

    // Middle bars
    IntRack_sol->AddNode(*vb2, tr3d = G4Transform3D(ro0, { +side_width/2.-bar_depth/2., 0, bar_height/2.-bar_spacing-bar_width/2.}));
    IntRack_sol->AddNode(*vb2, tr3d = G4Transform3D(ro0, { +side_width/2.-bar_depth/2., 0, bar_height/2.-2.*bar_spacing-1.5*bar_width}));
    IntRack_sol->AddNode(*vb2, tr3d = G4Transform3D(ro0, { -side_width/2.+bar_depth/2., 0, bar_height/2.-bar_spacing-bar_width/2.}));
    IntRack_sol->AddNode(*vb2, tr3d = G4Transform3D(ro0, { -side_width/2.+bar_depth/2., 0, bar_height/2.-2.*bar_spacing-1.5*bar_width}));

    // Top bars
    IntRack_sol->AddNode(*vb3, tr3d = G4Transform3D(ro0, { +side_width/2.-bar_depth/2., 0, bar_height/2.+bar_width/2.}));
    IntRack_sol->AddNode(*vb3, tr3d = G4Transform3D(ro0, { -side_width/2.+bar_depth/2., 0, bar_height/2.+bar_width/2.}));

    IntRack_sol->AddNode(*vb4, tr3d = G4Transform3D(ro0, { 0, -front_width/2.+bar_depth/2., bar_height/2.+bar_width/2.}));
    IntRack_sol->AddNode(*vb4, tr3d = G4Transform3D(ro0, { 0, +front_width/2.-bar_depth/2., bar_height/2.+bar_width/2.}));

    // transversal bars1
    IntRack_sol->AddNode(*tb1, tr3d = G4Transform3D(RoYp45, { +side_width/2.-bar_depth-(bar_diag1-bar_width)/sqrt(8.), +front_width/2.-bar_width/2., bar_height/2.-(bar_diag1-bar_width)/sqrt(8.)}));
    IntRack_sol->AddNode(*tb1, tr3d = G4Transform3D(RoYp45, { +side_width/2.-bar_depth-(bar_diag1-bar_width)/sqrt(8.), -front_width/2.+bar_width/2., bar_height/2.-(bar_diag1-bar_width)/sqrt(8.)}));
    IntRack_sol->AddNode(*tb1, tr3d = G4Transform3D(RoYm45, { -side_width/2.+bar_depth+(bar_diag1-bar_width)/sqrt(8.), +front_width/2.-bar_width/2., bar_height/2.-(bar_diag1-bar_width)/sqrt(8.)}));
    IntRack_sol->AddNode(*tb1, tr3d = G4Transform3D(RoYm45, { -side_width/2.+bar_depth+(bar_diag1-bar_width)/sqrt(8.), -front_width/2.+bar_width/2., bar_height/2.-(bar_diag1-bar_width)/sqrt(8.)}));

    // transversal bars2
    IntRack_sol->AddNode(*tb2, tr3d = G4Transform3D(RoXp45, { +side_width/2.-bar_depth/2., +front_width/2.-bar_width-(bar_diag2-bar_width/2.)/sqrt(8.), bar_height/2.-(bar_diag2-bar_width/2.)/sqrt(8.)}));
    IntRack_sol->AddNode(*tb2, tr3d = G4Transform3D(RoXp45, { -side_width/2.+bar_depth/2., +front_width/2.-bar_width-(bar_diag2-bar_width/2.)/sqrt(8.), bar_height/2.-(bar_diag2-bar_width/2.)/sqrt(8.)}));
    IntRack_sol->AddNode(*tb2, tr3d = G4Transform3D(RoXm45, { +side_width/2.-bar_depth/2., -front_width/2.+bar_width+(bar_diag2-bar_width/2.)/sqrt(8.), bar_height/2.-(bar_diag2-bar_width/2.)/sqrt(8.)}));
    IntRack_sol->AddNode(*tb2, tr3d = G4Transform3D(RoXm45, { -side_width/2.+bar_depth/2., -front_width/2.+bar_width+(bar_diag2-bar_width/2.)/sqrt(8.), bar_height/2.-(bar_diag2-bar_width/2.)/sqrt(8.)}));


    // Upper part
    const double foot_height   =   0.7*140.0*mm;
    const double rhole1        =   0.7*262.5*mm;
    const double rhole2        =   0.7*310.0*mm;
    const double hz1           =   0.7*50.0*mm;
    const double hz2           =   0.7*60.0*mm;

    G4Box*  ft1  = new G4Box("ft1",bar_width/2., bar_depth/2., foot_height/2.);              // foot upper plane

    G4Box*  up1 = new G4Box("up1", front_width/2., front_width/2., (hz1+hz2)/2.);            // upper plane
    G4Tubs* hl1 = new G4Tubs("hl1", 0., rhole1, hz1+hz2, 0*deg, 360*deg);
    G4Tubs* hl2 = new G4Tubs("hl2", 0., rhole2,     hz2, 0*deg, 360*deg);
    G4SubtractionSolid* sb1 = new G4SubtractionSolid("sb1",up1,hl1,nullptr,{0, 0, 0});
    G4SubtractionSolid* sb2 = new G4SubtractionSolid("sb2",sb1,hl2,nullptr,{0, 0, -(hz1+hz2)/2.});

    IntRack_sol->AddNode(*ft1, tr3d = G4Transform3D(ro0, { +front_width/2.-bar_width, +front_width/2.-bar_depth/2., bar_height/2.+bar_width+foot_height/2.}));
    IntRack_sol->AddNode(*ft1, tr3d = G4Transform3D(ro0, { -front_width/2.+bar_width, +front_width/2.-bar_depth/2., bar_height/2.+bar_width+foot_height/2.}));
    IntRack_sol->AddNode(*ft1, tr3d = G4Transform3D(ro0, { +front_width/2.-bar_width, -front_width/2.+bar_depth/2., bar_height/2.+bar_width+foot_height/2.}));
    IntRack_sol->AddNode(*ft1, tr3d = G4Transform3D(ro0, { -front_width/2.+bar_width, -front_width/2.+bar_depth/2., bar_height/2.+bar_width+foot_height/2.}));

    IntRack_sol->AddNode(*sb2, tr3d = G4Transform3D(ro0, { 0, 0, bar_height/2.+bar_width+foot_height+(hz1+hz2)/2.}));


    //Rear Upper Structure
    const double hbar_z       =  0.7*434.4*mm;
    const double hbar1        =  0.7*910.0*mm;
    const double hbar2        =  0.7*371.0*mm;
    const double bar_height1  = 0.7*1000.0*mm;
    const double bar_spacing2 =  0.7*460.0*mm;
    const double bar_height2  =  0.7*229.0*mm;
    const double plane_height =    0.7*6.0*mm;
    const double plane_z      =  0.7*270.0*mm;
    const double diag_lenght  =  0.7*360.0*mm;

    G4VSolid* ub1 = new G4Box("ub1", bar_width/4., bar_width/2., bar_height1/2.);                   // Vertical   bars
    G4VSolid* ub2 = new G4Box("ub2", bar_width/4., bar_width/2., bar_height2/2.);                   // Short bars
    G4VSolid* ub3 = new G4Box("ub3", bar_width/4., hbar1/2., bar_width/2.);                         // Horizontal bar

    G4VSolid* ps1 = new G4Box("ps1", hbar2/2., hbar1/2., bar_width/4.);
    G4VSolid* ps2 = new G4Box("ps2", hbar2/2.-bar_width/2., hbar1/2.-bar_width/2., bar_width/2.);
    G4SubtractionSolid* psp = new G4SubtractionSolid("psp",ps1,ps2,nullptr,{0, 0, 0});              // Plane support

    G4VSolid* pln = new G4Box("pln", hbar2/2.-bar_width/4., hbar1/2.-bar_width/2., plane_height/2.);

    G4Trd* dg1 = new G4Trd("dg1", diag_lenght/2.-bar_width/2., diag_lenght/2., bar_width/4., bar_width/4., bar_width/4.);

    IntRack_sol->AddNode(*ub1, tr3d = G4Transform3D(ro0, {side_width/2.+bar_width/4., +bar_spacing2/2.+bar_width/2., bar_height/2.+bar_width+hbar_z-bar_height1/2.}));
    IntRack_sol->AddNode(*ub1, tr3d = G4Transform3D(ro0, {side_width/2.+bar_width/4., -bar_spacing2/2.-bar_width/2., bar_height/2.+bar_width+hbar_z-bar_height1/2.}));

    IntRack_sol->AddNode(*ub2, tr3d = G4Transform3D(ro0, {side_width/2.+bar_width/4., +hbar1/2.-bar_width/2., bar_height/2.+bar_width+hbar_z-bar_height2/2.}));
    IntRack_sol->AddNode(*ub2, tr3d = G4Transform3D(ro0, {side_width/2.+bar_width/4., -hbar1/2.+bar_width/2., bar_height/2.+bar_width+hbar_z-bar_height2/2.}));

    IntRack_sol->AddNode(*ub3, tr3d = G4Transform3D(ro0, {side_width/2.+bar_width/4., 0, bar_height/2.+bar_width+hbar_z+bar_width/2.}));

    IntRack_sol->AddNode(*psp, tr3d = G4Transform3D(ro0, {side_width/2.-hbar2/2., 0, bar_height/2.+bar_width+hbar_z-bar_height2+bar_width/4.}));

    IntRack_sol->AddNode(*pln, tr3d = G4Transform3D(ro0, {side_width/2.-hbar2/2.+bar_width/4., 0, bar_height/2.+bar_width+plane_z+plane_height/2.}));

    IntRack_sol->AddNode(*dg1, tr3d = G4Transform3D(RoYm45, { +side_width/2.-(diag_lenght-bar_width/2.)/sqrt(8), +hbar1/2.-bar_width/4., bar_height/2.+bar_width+hbar_z-bar_height2+bar_width/2.+(diag_lenght-bar_width/2.)/sqrt(8)}));
    IntRack_sol->AddNode(*dg1, tr3d = G4Transform3D(RoYm45, { +side_width/2.-(diag_lenght-bar_width/2.)/sqrt(8), -hbar1/2.+bar_width/4., bar_height/2.+bar_width+hbar_z-bar_height2+bar_width/2.+(diag_lenght-bar_width/2.)/sqrt(8)}));


    // Motor tank
    const double b_width  =  0.7*206.0*mm;
    const double b_depth  =   0.7*80.0*mm;
    const double b_height =  0.7*168.0*mm;
    const double c_rad    =   0.7*57.0*mm;
    const double c_len    =  0.7*306.0*mm;
    const double pos_dx   =  0.7*214.0*mm;
    const double pos_dy   =  0.7*166.0*mm;

    G4VSolid* bm1 = new G4Box("bm1", b_height/2., b_width/2., b_depth/2.);           // box
    G4VSolid* tm1 = new G4Tubs("tm1", 0., c_rad, c_len/2., 0*deg, 360*deg);          // cylinder
    G4UnionSolid* mt1 = new G4UnionSolid("mt1",bm1,tm1,nullptr,{0,0,0});

    IntRack_sol->AddNode(*mt1, tr3d = G4Transform3D(RoYp90, { +side_width/2.-pos_dx, hbar1/2.-pos_dy, bar_height/2.+bar_width+plane_z+plane_height+b_height/2.}));
    IntRack_sol->AddNode(*mt1, tr3d = G4Transform3D(RoYp90, { +side_width/2.-pos_dx, hbar1/2.-pos_dy, bar_height/2.+bar_width+plane_z+plane_height+1.5*b_height}));

    IntRack_sol->Voxelize();

    G4LogicalVolume* IntRack_log = new G4LogicalVolume(IntRack_sol, G4Material::GetMaterial("Aluminium"),"VHlogic_CryostatRack");
    IntRack_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));
    G4VPhysicalVolume* IntRack_phys = new G4PVPlacement(nullptr,Pos+G4ThreeVector(0, 0, 0*mm),IntRack_log,"VHphysic_CryostatRack",mother,false,0,overlap_test);

    if(construction_verbose_level > 0) {
      std::cout << "   " << IntRack_phys->GetName() << ":  Material=" << IntRack_log->GetMaterial()->GetName() << " Mass=" << To3Digits(IntRack_log->GetMass()/kg) << " kg\n";
      std::cout << "   ******************* End of Cryostat Rack construction ********************\n\n";
    }

    return;
}

// =====================================================================================================================
// =====================================  Functions for solid volumes construction =====================================
// =====================================================================================================================


G4double OrsayCryostat::To3Digits(G4double x) const {
    G4double f=1.;
    while(x/f > 1000.5 || x/f < 99.5) {
        if(x/f > 1000.5) f=f*10.;
        if(x/f < 99.5) f=f/10.;
    }
    return int(x/f+0.5)*f;
}

G4VSolid* Heater(const G4String& name) {
    G4Tubs* cap = new G4Tubs("cap", 0.*mm, 44.*mm, 3.*mm, 0.*deg, 360.*deg);
    G4Tubs* t0 = new G4Tubs("t0", 43.*mm, 44.*mm, 21.*mm, 0.*deg, 360.*deg);
    G4Tubs* t1 = new G4Tubs("t1", 33.*mm, 34.*mm, 21.*mm, 0.*deg, 360.*deg);
    G4Tubs* t2 = new G4Tubs("t2", 28.*mm, 29.*mm, 21.*mm, 0.*deg, 360.*deg);
    G4Tubs* t3 = new G4Tubs("t3", 23.*mm, 24.*mm, 21.*mm, 0.*deg, 360.*deg);
    G4Tubs* t4 = new G4Tubs("t4", 18.*mm, 19.*mm, 21.*mm, 0.*deg, 360.*deg);
    G4Tubs* t5 = new G4Tubs("t5", 13.*mm, 14.*mm, 21.*mm, 0.*deg, 360.*deg);
    G4Tubs* t6 = new G4Tubs("t6", 8.*mm, 9.*mm, 21.*mm, 0.*deg, 360.*deg);
    G4Tubs* t7 = new G4Tubs("t7", 3.*mm, 4.*mm, 21.*mm, 0.*deg, 360.*deg);
    G4UnionSolid* u0 = new G4UnionSolid("u0",t6,t7,nullptr,G4ThreeVector(0., 0., 0.));
    G4UnionSolid* u1 = new G4UnionSolid("u1",t5,u0,nullptr,G4ThreeVector(0., 0., 0.));
    G4UnionSolid* u2 = new G4UnionSolid("u2",t4,u1,nullptr,G4ThreeVector(0., 0., 0.));
    G4UnionSolid* u3 = new G4UnionSolid("u3",t3,u2,nullptr,G4ThreeVector(0., 0., 0.));
    G4UnionSolid* u4 = new G4UnionSolid("u4",t2,u3,nullptr,G4ThreeVector(0., 0., 0.));
    G4UnionSolid* u5 = new G4UnionSolid("u5",t1,u4,nullptr,G4ThreeVector(0., 0., 0.));
    G4UnionSolid* u6 = new G4UnionSolid("u6",t0,u5,nullptr,G4ThreeVector(0., 0., 0.));
    G4UnionSolid* u7 = new G4UnionSolid(name,cap,u6,nullptr,G4ThreeVector(0., 0., -21.));
    return u7;
}

G4VSolid* PulseTube(const G4String& name) {
    G4Tubs* Base = new G4Tubs("Base", 0.*mm, 42.8625*mm, 4.5*mm, 0.*deg, 360.*deg);
    G4Tubs* Tub1_sub = new G4Tubs("Tub1_sub", 0*mm, 9.525*mm, 92.2945*mm, 0.*deg, 360.*deg);
    G4Tubs* Tub1 = new G4Tubs("Tub1", 7.525*mm, 9.525*mm, 92.2945*mm, 0.*deg, 360.*deg);
    G4Tubs* Tub2_sub = new G4Tubs("Tub2_sub", 0*mm, 14.9225*mm, 92.2945*mm, 0.*deg, 360.*deg);
    G4Tubs* Tub2 = new G4Tubs("Tub2", 12.9225*mm, 14.9225*mm, 92.2945*mm, 0.*deg, 360.*deg);
    G4Tubs* Disk = new G4Tubs("Disk", 0*mm, 47.*mm, 1.*mm, 0.*deg, 360.*deg);
    G4Tubs* Cyl = new G4Tubs("Cyl", 0*mm, 60.325*mm, 6.25*mm, 0.*deg, 360.*deg);
    G4Tubs* Top_Tub1 = new G4Tubs("Top_Tub1", 27.21*mm, 29.21*mm, 69.205*mm, 0.*deg, 360.*deg);
    G4Tubs* Top_Tub2 = new G4Tubs("Top_Tub2", 7.525*mm, 9.525*mm, 69.205*mm, 0.*deg, 360.*deg);
    G4Tubs* Top_Tub3 = new G4Tubs("Top_Tub3", 18.64*mm, 20.64*mm, 69.205*mm, 0.*deg, 360.*deg);
    G4Tubs* Top_cyl = new G4Tubs("Top_cyl", 0*mm, 88.9*mm, 4.7625*mm, 0.*deg, 360.*deg);
    G4Tubs* Simp_Top_part = new G4Tubs("Simp_Top_part", 64.12*mm, 66.12*mm, 104.44*mm, 0.*deg, 360.*deg);
    G4Tubs* Cap = new G4Tubs("Cap", 0*mm, 66.12*mm, 5.1*mm, 0.*deg, 360.*deg);
    
    G4RotationMatrix Ro0 = G4RotationMatrix();             // No Rotation

    G4Transform3D tr3d;
    
    G4SubtractionSolid* Disk_sub = new G4SubtractionSolid("Disk_sub",Disk,Tub1_sub,nullptr,G4ThreeVector(22.822, -13.858, 0));
    G4SubtractionSolid* Disk_sub_2 = new G4SubtractionSolid("Disk_sub_2",Disk_sub,Tub2_sub,nullptr,G4ThreeVector(0.,25.4,0));

    G4MultiUnion* PT_sol = new G4MultiUnion("VHsolid_"+name);
    PT_sol->AddNode(*Base,tr3d = G4Transform3D(Ro0, {0, 0, 0}));
    PT_sol->AddNode(*Disk_sub_2,tr3d = G4Transform3D(Ro0, {0, 0, 44.55*mm}));
    PT_sol->AddNode(*Disk_sub_2,tr3d = G4Transform3D(Ro0, {0, 0, 25+44.55*mm}));
    PT_sol->AddNode(*Disk_sub_2,tr3d = G4Transform3D(Ro0, {0, 0, 50+44.55*mm}));
    PT_sol->AddNode(*Tub1,tr3d = G4Transform3D(Ro0, {22.822, -13.858, 96.7945*mm}));
    PT_sol->AddNode(*Tub2,tr3d = G4Transform3D(Ro0, {0., 25.4, 96.7945*mm}));
    PT_sol->AddNode(*Cyl,tr3d = G4Transform3D(Ro0, {0., 0, 195.339*mm}));
    PT_sol->AddNode(*Top_Tub2,tr3d = G4Transform3D(Ro0, {22.822, -13.858, 270.794*mm}));
    PT_sol->AddNode(*Top_Tub1,tr3d = G4Transform3D(Ro0, {0., 25.4, 270.794*mm}));
    PT_sol->AddNode(*Top_Tub3,tr3d = G4Transform3D(Ro0, {-28., -23.2, 270.794*mm}));
    PT_sol->AddNode(*Top_cyl,tr3d = G4Transform3D(Ro0, {0, 0, 345.2615*mm}));
    PT_sol->AddNode(*Simp_Top_part,tr3d = G4Transform3D(Ro0, {0, 0, 454.464*mm}));
    PT_sol->AddNode(*Cap,tr3d = G4Transform3D(Ro0, {0, 0, 564.004*mm}));
    PT_sol->Voxelize();
    return PT_sol;
}
