#include <DCDumpTrackingInfo.hh>

#include <G4SystemOfUnits.hh>
#include <G4Step.hh>
#include <G4Track.hh>
#include <G4StepPoint.hh>
#include <G4TrackVector.hh>
#include <G4LogicalVolume.hh>
#include <G4VPhysicalVolume.hh>
#include <G4VProcess.hh>
#include <G4VUserTrackInformation.hh>
#include <G4Material.hh>
#include <G4MaterialCutsCouple.hh>
#include <G4VSensitiveDetector.hh>
#include <G4ThreeVector.hh>
#include <G4DynamicParticle.hh>

#include <vector>
#include <iostream>

using namespace std;

// Dump selected step members
void DumpStepInfo(const G4Step* step)
{
  cout <<"\n ++++++++ Dump Step Info ++++++++\n";
  cout  <<"\n TrackID= "<<step->GetTrack()->GetTrackID()<<"\t ParentID= "<<step->GetTrack()->GetParentID()
  <<"\t TrackStatus= "<<step->GetTrack()->GetTrackStatus()<<"\t CurrentStepNumber= "<<step->GetTrack()->GetCurrentStepNumber();
  
  cout << "\n ParticleName= "<<step->GetTrack()->GetDefinition()->GetParticleName().data()
  <<"\t ParticleType= "<<step->GetTrack()->GetDefinition()->GetParticleType().data()
  <<"\t ParticleSubType= "<<step->GetTrack()->GetDefinition()->GetParticleSubType().data()
  <<"\t PDGCode= "<<step->GetTrack()->GetDynamicParticle()->GetPDGcode() ;
  cout << "\n PRE ProcessDefinedStep: "; DumpProcess(step->GetPreStepPoint()->GetProcessDefinedStep());
  cout << " POST ProcessDefinedStep: "; DumpProcess(step->GetPostStepPoint()->GetProcessDefinedStep());
  
  cout << "\n\t PreStepPoint:";
  cout << "\n PhysicalVolume: "; DumpPhysicalVolume(step->GetPreStepPoint()->GetPhysicalVolume());
  cout << "\t Material: "; DumpMaterial(step->GetPreStepPoint()->GetMaterial());
  cout <<"\t StepStatus= "<<step->GetPreStepPoint()->GetStepStatus()
  <<"\n GlobalTime= "<<step->GetPreStepPoint()->GetGlobalTime()<<"\t LocalTime= "<<step->GetPreStepPoint()->GetLocalTime()
  <<"\t ProperTime= "<<step->GetPreStepPoint()->GetProperTime()
  <<"\n KineticEnergy= "<<step->GetPreStepPoint()->GetKineticEnergy()<<"\t TotalEnergy= "<<step->GetPreStepPoint()->GetTotalEnergy();
  
  cout << "\n\t PostStepPoint:";
  cout << "\n PhysicalVolume: "; DumpPhysicalVolume(step->GetPostStepPoint()->GetPhysicalVolume());
  cout << "\t Material: "; DumpMaterial(step->GetPostStepPoint()->GetMaterial());
  cout <<"\t StepStatus= "<<step->GetPostStepPoint()->GetStepStatus()
  <<"\n GlobalTime= "<<step->GetPostStepPoint()->GetGlobalTime()<<"\t LocalTime= "<<step->GetPostStepPoint()->GetLocalTime()
  <<"\t ProperTime= "<<step->GetPostStepPoint()->GetProperTime()
  <<"\n KineticEnergy= "<<step->GetPostStepPoint()->GetKineticEnergy()<<"\t TotalEnergy= "<<step->GetPostStepPoint()->GetTotalEnergy();
  
  cout <<"\n\n IsFirstStepInVolume= "<<step->IsFirstStepInVolume()
  <<"\t IsLastStepInVolume= "<<step->IsLastStepInVolume() << "\t ControlFlag= "<<step->GetControlFlag()
  << "\t IsBelowThreshold= " << step->GetTrack()->IsBelowThreshold()
  <<"\t IsGoodForTrackingAnyway= " << step->GetTrack()->IsGoodForTracking();
  
  cout <<"\n TotalEnergyDeposit= "<<step->GetTotalEnergyDeposit()
  <<"\t NonIonizingEnergyDeposit= "<<step->GetNonIonizingEnergyDeposit()<<"\t DeltaEnergy= "<<step->GetDeltaEnergy()
  <<"\t VertexKineticEnergy= "<<step->GetTrack()->GetVertexKineticEnergy();
  cout << "\n ++++++++++++++++++++++++++++++++\n";
}

// Dump all step members except touchables (because I don't know what it is)
void DumpFullStepInfo(const G4Step* step)
{
  cout <<"\n\n ++++++++ Dump Full Step Info ++++++++\n";
  cout << "\n"; DumpFullTrackInfo(step->GetTrack());
  
  cout <<" IsFirstStepInVolume= "<<step->IsFirstStepInVolume()
  <<"\t IsLastStepInVolume= "<<step->IsLastStepInVolume() << "\t ControlFlag= "<<step->GetControlFlag()
  <<"\n StepLength= "<<step->GetStepLength()
  <<"\n TotalEnergyDeposit= "<<step->GetTotalEnergyDeposit()
  <<"\t NonIonizingEnergyDeposit= "<<step->GetNonIonizingEnergyDeposit()<<"\t DeltaEnergy= "<<step->GetDeltaEnergy()
  <<"\n DeltaPosition= "<<step->GetDeltaPosition()<<"\n DeltaMomentum= "<<step->GetDeltaMomentum();
  
  cout << "\n Secondary: "; DumpFullTrackVector(step->GetSecondary());
  cout << "\n\n\t PointerToVectorOfAuxiliaryPoints: "; DumpPointVector(step->GetPointerToVectorOfAuxiliaryPoints());
  cout << "\n PreStepPoint:\n"; DumpFullStepPointInfo(step->GetPreStepPoint());
  cout << "\n PostStepPoint:\n"; DumpFullStepPointInfo(step->GetPostStepPoint());
  cout << " ++++++++++++++++++++++++++++++++\n\n";
}

void DumpTrackInfo(const G4Track* track)
{
  cout <<"\n -------- Dump Track Info --------\n";
  cout  <<" TrackStatus= "<<track->GetTrackStatus()
    <<" ParentID= "<<track->GetParentID()<< " TrackID= "<<track->GetTrackID()
    <<" CurrentStepNumber= "<<track->GetCurrentStepNumber()<<"\n";
  cout << track->GetDefinition()->GetParticleName()
    << " KineticEnergy=" << track->GetKineticEnergy()/MeV << " Position=" << track->GetPosition()
    << " GlobalTime= "<<track->GetGlobalTime()<< " LocalTime= "<<track->GetLocalTime()<<" ProperTime= "<<track->GetProperTime()
    << " Volume: "; DumpPhysicalVolume(track->GetVolume());
  cout <<"\n";
}

// Dump all track members except touchables (because I don't know what it is), Step members and StepPoint members
void DumpFullTrackInfo(const G4Track*track)
{
  cout <<"\n -------- Dump Full Track Info --------\n";
  cout  <<"\n TrackStatus= "<<track->GetTrackStatus()
  <<"\n ParentID= "<<track->GetParentID()<< "\t TrackID= "<<track->GetTrackID()
  <<"\t CurrentStepNumber= "<<track->GetCurrentStepNumber();
  
  cout << "\n UserInformation: "; DumpUserInformation(track->GetUserInformation());
  cout << "\n CreatorProcess: "; DumpProcess(track->GetCreatorProcess());
  cout << "\n DynamicParticle: "; DumpDynamicParticle(track->GetDynamicParticle());
  
  cout << " IsBelowThreshold= " << track->IsBelowThreshold()
  << "\t IsGoodForTrackingAnyway= " << track->IsGoodForTracking();
  cout <<"\n KineticEnergy= "<<track->GetKineticEnergy()<<"\t Weight= "<<track->GetWeight()<<"\t Velocity= "<<track->GetVelocity()
  <<"\n TrackLength= "<<track->GetTrackLength()<<"\t StepLength= "<<track->GetStepLength()
  <<"\n GlobalTime= "<<track->GetGlobalTime()<<"\t LocalTime= "<<track->GetLocalTime()<<"\t ProperTime= "<<track->GetProperTime()
  <<"\n Position= "<<track->GetPosition()<<"\n VertexPosition= "<<track->GetVertexPosition()
  <<"\n VertexKineticEnergy= "<<track->GetVertexKineticEnergy()
  <<"\n VertexMomentumDirection= "<<track->GetVertexMomentumDirection();
  
  cout << "\n LogicalVolumeAtVertex: "; DumpLogicalVolume(track->GetLogicalVolumeAtVertex());
  cout << "\t Volume: "; DumpPhysicalVolume(track->GetVolume());
  cout << "\t NextVolume: "; DumpPhysicalVolume(track->GetNextVolume());
}

// Dump all step point members except touchables (because I don't know what it is)
void DumpFullStepPointInfo(const G4StepPoint* point)
{
  cout <<" StepStatus= "<<point->GetStepStatus()
  <<"\n Weight= "<<point->GetWeight()<<"\t Safety= "<<point->GetSafety()
  <<"\n Mass= "<<point->GetMass()<<"\t Charge= "<<point->GetCharge()
  <<"\t MagneticMoment= "<<point->GetMagneticMoment()
  <<"\n GlobalTime= "<<point->GetGlobalTime()<<"\t LocalTime= "<<point->GetLocalTime()
  <<"\t ProperTime= "<<point->GetProperTime()
  <<"\n KineticEnergy= "<<point->GetKineticEnergy()<<"\t TotalEnergy= "<<point->GetTotalEnergy()
  <<"\n Beta= "<<point->GetBeta()<<"\t Gamma= "<<point->GetGamma()<<"\t Velocity= "<<point->GetVelocity()
  <<"\n Position= "<<point->GetPosition()<<"\n Polarization= "<<point->GetPolarization()
  <<"\n Momentum= "<<point->GetMomentum()<<"\n MomentumDirection= "<<point->GetMomentumDirection();
  
  cout << "\n PhysicalVolume: "; DumpPhysicalVolume(point->GetPhysicalVolume());
  cout << "\n ProcessDefinedStep: "; DumpProcess(point->GetProcessDefinedStep());
  cout << "\n Material: "; DumpMaterial(point->GetMaterial());
  cout << "\t MaterialCutsCouple: "; DumpMaterialCutsCouple(point->GetMaterialCutsCouple());
  cout << "\n SensitiveDetector: "; DumpSensitiveDetector(point->GetSensitiveDetector());
}

void DumpFullTrackVector(const G4TrackVector* v)
{
  if (v == 0) { cout << "\t=> None"; }
  cout << "\n\tTrack vector size = " << v->size()<<"\n";
  G4int i=0;
  for (auto it=v->begin(); it!=v->end(); it++,i++) {
    cout << "\n ### Secondary track n° "<<i<<" ####";
    DumpFullTrackInfo(*it);
    cout << "\n";
  }
}

void DumpLogicalVolume(const G4LogicalVolume* lvol)
{ // just name
  if (lvol) { cout << "\t" << lvol->GetName(); }
  else { cout << "\t=> None"; }
}

void DumpPhysicalVolume(const G4VPhysicalVolume* pvol)
{ // just name
  if (pvol) { cout << "\t" << pvol->GetName(); }
  else { cout << "\t=> None"; }
}

void DumpProcess(const G4VProcess* proc)
{ // probably no other method
  if (proc) { proc->DumpInfo(); }
  else { cout << "\t=> None"; }
}

void DumpUserInformation(const G4VUserTrackInformation* info)
{ // no other method
  if (info) { info->Print(); }
  else { cout << "\t=> None"; }
}

void DumpMaterial(const G4Material* mat)
{ // just name
  if (mat) { cout << "\t" << mat->GetName(); }
  else { cout << "\t=> None"; }
}

void DumpMaterialCutsCouple(const G4MaterialCutsCouple* mat)
{ // just name
  if (mat) { cout << "\t" << mat->GetMaterial()->GetName(); }
  else { cout << "\t=> None"; }
}

void DumpSensitiveDetector(const G4VSensitiveDetector* det)
{ // just name
  if (det) { cout << "\t" << det->GetName(); }
  else { cout << "\t=> None"; }
}

void DumpPointVector(const std::vector<G4ThreeVector>* v)
{
  if (v) { cout << "\tsize=" << v->size(); }
  else { cout << "\t=> None"; }
}

void DumpDynamicParticle(const G4DynamicParticle* part)
{ // others members ?
  if (part) {
    cout << "\n DynamicParticle Info:\n"<< " PDGcode= "<<part->GetPDGcode()<<endl;
    part->DumpInfo();
    //     cout << " DynamicParticle Definition Table:";
    //     part->GetDefinition()->DumpTable();
    //   track->GetDefinition(); // G4ParticleDefinition*
    //   track->GetProperTime(); // double
    //   track->GetKineticEnergy(); // double
    //   track->GetTotalEnergy(); // double
    //   track->GetMomentum(); // G4ThreeVector
    //   track->GetMomentumDirection(); // G4ThreeVector
    //   track->GetPolarization(); // G4ThreeVector
  } else { cout << "\t=> None"; }
}
