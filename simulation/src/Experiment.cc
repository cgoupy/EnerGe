#include "Experiment.hh"
#include "ExperimentalSite.hh"

#include "AttenuationLab.hh"

#include <Parameters.hh>
#include <DCGLG4BuildOpticalProperties.hh>
#include <GLG4InputDataReader.hh>

#include <G4GeometryManager.hh>
#include <G4PhysicalVolumeStore.hh>
#include <G4LogicalVolumeStore.hh>
#include <G4SolidStore.hh>

#include <fstream>
#include <sys/stat.h>
#include <unistd.h>

Experiment::~Experiment()
{
    // delete the optic file (if existing)!
    struct stat buf;
    if ( stat(OpticsFileName.c_str(),&buf) == 0) {
        RemoveOpticFile();
    }
}

void Experiment::RemoveOpticFile()
{
    if (remove(OpticsFileName.c_str()) == 0 ) {
        std::cout<<" Successfully deleted the optic file " << OpticsFileName << std::endl;
    }
    else std::cout<<"***Error ~Experiment: Could not delete the optic file " << OpticsFileName << std::endl;
}

G4VPhysicalVolume* Experiment::ConstructWorld() const{
    
    G4VPhysicalVolume* world = nullptr;
    
    switch(ExperimentSiteChoice){
            
        case 0:
        {
            world = WorldBuilder::EmptySite().BuildWorld();
            break;
        }
            
        case 1:
            world = WorldBuilder::AttenuationLab(5*CLHEP::m, {ParamBase::GetDataBase()["slab_thickness"]*CLHEP::m, ParamBase::GetDataBase()("slab_material")}).BuildWorld();
            break;
            
        case 2:
            world = WorldBuilder::OrsayLab().BuildWorld();
            break;
            
        case 3:
            world = WorldBuilder::VeryNearSite().BuildWorld();
            break;
            
        default:
            G4Exception("Experiment::ConstructWorld","21",FatalErrorInArgument, scat(" Invalid geometry option: ",ExperimentSiteChoice).c_str() );
    }
    
    if (constructionVerbosity > 3) PrintWorldVolumes();
    if(constructionVerbosity) std::cout<<"============= Experiment: End of world construction ============\n\n";
    
    return world;
    
}

//	This class implements some options and prerequisites before introducing the real geometry,
//	and returning the World.

G4VPhysicalVolume* Experiment::Construct(){
    
    ReadBasicDataBase();
    
    if(constructionVerbosity) std::cout<<"=======================================================\n"
        <<"+++ Experiment: building the experiment +++\n";
    
    CleanGeometry();
    GLG4Material::CreateMaterials();
#ifdef USE_SCINTILLATOR
    SetUpOtpicalProperties();
#endif
    return ConstructWorld();
    
}

void Experiment::ReadBasicDataBase(){
    
    constructionVerbosity = ParamBase::GetDataBase().Get<int>("construction_verboselevel");
    ExperimentSiteChoice = ParamBase::GetDataBase().Get<int>("experimental_site");
    
}

// Clean old geometry
void Experiment::CleanGeometry(){
    
    G4GeometryManager::GetInstance()->OpenGeometry();
    G4PhysicalVolumeStore::GetInstance()->Clean();
    G4LogicalVolumeStore::GetInstance()->Clean();
    G4SolidStore::GetInstance()->Clean();
    
}

void Experiment::SetUpOtpicalProperties(){
    
    /// Set optical properties, mainly from materials_optic_properties_card.dat
    /// Scintillation properties are found in TunedDCOptics.dat or build from files in data/ScintComponents/*/
    // Define all needed paths
    OpticsFileName = scat(DirPropVec,"EnerGeOptics_",getpid(),".dat"); // must be unique, so add the pid in the file name
    
    std::ofstream OpticsFile(OpticsFileName); // output material properties vector file
    if (!OpticsFile) G4Exception("Experiment::SetUpOtpicalProperties","1",FatalException, scat("Can not open OpticsFile: ",OpticsFileName).c_str() );
    
    std::string DirMaterialData = std::getenv("ENERGE_DATA");
    auto RefOpticsFileName = DirMaterialData + "/TunedDCOptics.dat";
    
    if (ParamBase::GetDataBase()["optical_model_choice"]) {
        // Now reading the reference optical properties file
        std::cout<<" Reading reference optical properties for this simulation from file: "<<RefOpticsFileName<<"\n";
        
        std::ifstream RefOpticsFile(RefOpticsFileName);
        if (!RefOpticsFile)
            G4Exception("Experiment::SetUpOtpicalProperties","2",FatalException, scat("Can not open RefOpticsFile: ",RefOpticsFileName).c_str() );
        
        std::string tmpstr;
        while (std::getline(RefOpticsFile,tmpstr)) OpticsFile << tmpstr.c_str() << std::endl;
    }
    else {
        // Now calling the builder of the optical properties file
        std::cout<<" Building optical properties for this simulation ...\n";
        if ( DCGLG4BuildOpticalProperties::BuildProperties(OpticsFile, GLG4Material::Get()->GetIVLiquid()->GetName()) )
            G4Exception("Experiment::SetUpOtpicalProperties","3",FatalException,"There were errors during optical calculations...");
    }
    std::cout<<" Properties will be written in " << OpticsFileName << "\n";
    
    OpticsFile.close();
    
    // Read all other material properties (those calculated run-time and the constant ones)
    std::ifstream ifsOptics(OpticsFileName);
    std::ifstream ifsGeneric((DirMaterialData+"/materials_optic_properties_card.dat").c_str());
    
    if (!ifsOptics)
        G4Exception("Experiment::SetUpOtpicalProperties","4",FatalException, "Optics material properties file could not be opened.\n");
    
    if (!ifsGeneric)
        G4Exception("Experiment::SetUpOtpicalProperties","5",FatalException,"Generic material properties file could not be opened.\n");
    
    // now read optics properties, keeping error count
    G4int errorCount_ReadMaterials = GLG4InputDataReader::ReadMaterials(ifsOptics);
    if (errorCount_ReadMaterials)
        G4Exception("Experiment::SetUpOtpicalProperties","6",FatalException,
                    scat("Reading DCGLG4BuildOpticalProperties: error count after reading variable properties file is: ", errorCount_ReadMaterials," \n").c_str());
    
    // now read generic properties, keeping error count
    errorCount_ReadMaterials = GLG4InputDataReader::ReadMaterials(ifsGeneric);
    if (errorCount_ReadMaterials) {
        G4Exception("Experiment::SetUpOtpicalProperties","7",FatalException,
                    scat("Reading generic material properties: error count after reading constant properties file is: ", errorCount_ReadMaterials," \n").c_str());
    }
    
    // has to be done now, after reading optical properties of materials
    GLG4Material::Get()->UpdatePhotocathodeMPT();
    
}

void Experiment::PrintWorldVolumes() const{
    
    std::cout<<"\nG4PhysicalVolumeStore:\n";
    for (auto el : *G4PhysicalVolumeStore::GetInstance()) std::cout<<"   "<<el->GetName()<<"\n";
    std::cout<<"\nG4LogicalVolumeStore:\n";
    for (auto el : *G4LogicalVolumeStore::GetInstance()) std::cout<<"   "<<el->GetName()<<"\n";
    std::cout<<"\nG4SolidStore:\n";
    for (auto el : *G4SolidStore::GetInstance()) std::cout<<"   "<<el->GetName()<<"\n";
    
}
