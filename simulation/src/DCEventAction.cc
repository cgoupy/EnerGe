#include <DCEventAction.hh>

#include <DCRunAction.hh>
#include <DCTrajectory.hh>

#include <Index.h>
#include <DataDefinition.h>
#include <Parameters.hh>
#include <String.hh>
#include <make_unique.hh>

using namespace EnerGe;

#include <GLG4PrimaryGeneratorAction.hh>
#include <GLG4Scintillation.hh>
#include <GLG4PMTOpticalModel.hh>
#include <GLG4HitPhoton.hh>

#include <Randomize.hh>
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <G4TransportationManager.hh>
#include <G4Navigator.hh>
#include <G4RunManager.hh>
#include <G4Run.hh>
#include <G4VPhysicalVolume.hh>
#include <G4ProcessType.hh>
#include <G4VProcess.hh>
#include <G4ParticleTable.hh>
#include <G4VVisManager.hh>
#include <G4ThreeVector.hh>
#include <G4Event.hh>
#include <G4Step.hh>
#include <G4Track.hh>

#include <TMath.h>

#include <GLG4VertexGen.hh>

#include <iostream>
#include <iomanip>
#include <memory>
#include <utility>
#include <map>


//Add for filling physical process in particle
#include <G4HadronicProcessType.hh>
#include <G4Neutron.hh>

using namespace std;


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DCEventAction::~DCEventAction()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DCEventAction::DCEventAction(DCRunAction* Run): runaction(Run)
{
    G4ParticleTable::GetParticleTable();  // instantiate the G4ParticleTable
    
    const ParamBase &db( ParamBase::GetDataBase() );
    
    VerboseLevel    = db.Get<G4int>("events_verboselevel");
    UseVisualization = db.Get<Bool_t>("use_visualization");
    SaveSecondaries = db.Get<Bool_t>("save_secondaries");
    SecondaryVolumeName = db("volume_secondaries");
    KillTrack = db.Get<Bool_t>("kill_track");
    
    myMC = runaction->GetMCInfo();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DCEventAction::Clear()
{
    GLG4PMTOpticalModel::ResetTotalIncidentPhoton();
    GLG4Scintillation::ResetTotEdep();
    theHitPMTCollection.Clear(); // clearing theHitPMTCollection clears away the HitPhotons and HitPMTs
    ParticleMap.clear();
    SecondaryParticleMap.clear();
    
    NbPrimary = 0;
    NbSecondary = 0;
    NbOpticalPhoton = 0ull;
    NbNonOpticalPhotons = 0ull;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DCEventAction::EndOfEventAction(const G4Event* evt)
{
    FillData(evt);
    
    if (UseVisualization) {
        if ( G4VVisManager::GetConcreteInstance() && drawFlag != "none") {
            G4TrajectoryContainer* trajectoryContainer = evt->GetTrajectoryContainer();
            for (Int_t i=0; i < (trajectoryContainer?trajectoryContainer->entries():0); i++) {
                DCTrajectory* trj = static_cast<DCTrajectory*>((*trajectoryContainer)[i]);
                trj->DrawTrajectory();
            }
        }
    }
    
    // Matthieu - May 2017: no need for now
    //    if (VerboseLevel>0) {
    //        cout << " Total incident photon on PMTs: " << GLG4PMTOpticalModel::GetTotalIncidentPhoton()
    //        << ", number of optical photons steps: "<<NbOpticalPhoton
    //        << ", and of other particles steps: "<<NbNonOpticalPhotons<<"\n\n";
    //    }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

EnerGe::Volumes DCEventAction::Physical2Number(const G4VPhysicalVolume* pv)
{
    if(VolumeMap.count(pv)) return VolumeMap[pv];
    
    G4String str(pv->GetName());
    EnerGe::Volumes vol= VOL_END;
    //These are the volumes in which we can score energy deposition.
    //Please ensure correspondence with enumerated volumes in dataformat/Index.h
    
    //************************
    // *** Attenuation lab ***
    //************************
    if (str.find("VHphysic_Lab")!=G4String::npos)    { vol= VOL_LAB; }
    else if (str.find("VHphysic_Slab")!=G4String::npos)    { vol= VOL_SLAB; }

    //**************
    // *** WORLD ***
    //**************
    else if (str.find("world")!=G4String::npos)    { vol= VOL_WORLD; }
    else if (str.find("VHphysic_world")!=G4String::npos)    { vol= VOL_WORLD; }
    
    //************************
    // *** COV Prototype ***
    //************************
    else if (str.find("VHphysic_Top_COVProto")!=G4String::npos) {vol=VOL_TOP_GE; }
    else if (str.find("VHphysic_LWO_COVProto")!=G4String::npos) {vol=VOL_MID_LWO; }
    else if (str.find("VHphysic_Bot_COVProto")!=G4String::npos) {vol=VOL_BOT_GE; }
    
    //*************************
    // *** CEA portable HPGe***
    //*************************
    else if (str.find("VHphysic_HPGe")!=G4String::npos) {vol=VOL_HPGE; }
    
    //************************
    // *** Cryostat ***
    //************************
    else if (str.find("c_10mK_Vessel")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_10mK_Plate")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_10mK_Melt_bot")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_100mK_Plate")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_1K_Plate")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_1K_BoilerBox")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_1K_AdaptBride")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_1K_Plate_branches")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_1K_Tub_1")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_1K_Tub_2")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_1K_Tub_3")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_100mK_1K_Tub_1")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_100mK_1K_Tub_2")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_100mK_1K_Tub_3")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_1K_Vessel")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_4K_AdaptBride")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_eq_Crown")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_eq_Crown2")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_eq_Top_Ring")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_eq_Bot_Tub_1")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_eq_Bot_Tub_2")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_eq_Bot_Tub_3")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_eq_Mid_Tub_1")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_eq_Mid_Tub_2")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_eq_Mid_Tub_3")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_eq_Top_Tub_1")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_eq_Top_Tub_2")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_eq_Top_Tub_3")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_4K_Plate")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("Screen_4K")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_4K_Vessel")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("Plate77K")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_77K_Bride")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_100K_VesselTop")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_100K_VesselBot")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_300K_Ensemble")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_300K_TankTop")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_300K_Plate")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_300K_VesselTop")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_300K_VesselBot")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_PT_Heater")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_PT_PulseTube")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_PT_High_bride")!=G4String::npos) {vol=VOL_CRYO; }
    else if (str.find("c_PT_Roul_soul")!=G4String::npos) {vol=VOL_CRYO; }
    
    else if (str.find("VHphysic_Shielding")!=G4String::npos) {vol=VOL_SLAB; }
    
    else {vol=VOL_INACTIVE;}
    
    VolumeMap[pv]= vol;//next time no need to parse the strings..
    
    if (VerboseLevel>2) cout << " Physical2Number: volume " << str << " is number " << vol << endl;
    return vol;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

Int_t DCEventAction::Process2Number(const G4VProcess* proc)
{
    Int_t code = 0;
    if ( proc) code = proc->GetProcessType()*1000 + proc->GetProcessSubType();
    
    if (VerboseLevel>2) cout << " Process2Number: process " << proc->GetProcessName().data() << " is number " << code << endl;
    
    return code;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// used in SteppingAction for MCinfo filling
void DCEventAction::TreatPrimaryParticleFirstStep(const G4Step* step)
{
    if (VerboseLevel>2) cout << "\tBegin of TreatPrimaryParticleFirstStep\n";
    
    // set Edep quenched for previous particle
    if (GLG4Scintillation::IsUsed() && NbPrimary) {
        myMC->GetMCEvent(NbPrimary-1)->SetEdepQuenched(GLG4Scintillation::GetTotEdepQuenched());
        GLG4Scintillation::ResetTotEdep();
    }
    
    // build a new MC event in MCInfo and get the associated primary particle
    MCEvent* myMCEvent = myMC->ConstructMCEventAt(NbPrimary);
    Particle* particle = myMCEvent->GetPrimaryParticle();
    ParticleMap[step->GetTrack()->GetTrackID()] = NbPrimary;
    ++NbPrimary;
    
    // init some locals
    G4StepPoint* Pre = step->GetPreStepPoint();
    G4StepPoint* Post = step->GetPostStepPoint();
    G4ThreeVector myPos(Pre->GetPosition());
    G4ThreeVector myDir(Pre->GetMomentumDirection());
    
    if (VerboseLevel>1) cout << "Injection at global: "<<myPos<< ", and local: "
        <<Pre->GetTouchableHandle()->GetHistory()->GetTopTransform().TransformPoint(myPos)<<"\n";
    
    // set initial values
    particle->SetVolInj(Physical2Number(Pre->GetPhysicalVolume()));
    particle->SetEinj(step->GetTrack()->GetVertexKineticEnergy()); // kinetic energy at injection vertex
    particle->SetPDG(step->GetTrack()->GetDynamicParticle()->GetPDGcode()); // particle PDG code
    
    particle->SetPosition(Particle::Injection,myPos.x(),myPos.y(),myPos.z()); // position of injection vertex
    particle->SetMomentumDirection(Particle::Injection,myDir.x(),myDir.y(),myDir.z()); // direction of momentum at the injection vertex
    
    if ( Post->GetProcessDefinedStep() && Post->GetProcessDefinedStep()->GetProcessType() == fTransportation) { // no actual interaction...
        particle->SetPosition(Particle::FirstInteraction,myPos.x(),myPos.y(),myPos.z()); // ... so first interaction vertex set to injection vertex
    } else { // real first interaction
        myPos = Post->GetPosition();
        particle->SetPosition(Particle::FirstInteraction,myPos.x(),myPos.y(),myPos.z()); // position of first interaction vertex
    }
    
    particle->SetPosition(Particle::LastInteraction,myPos.x(),myPos.y(),myPos.z()); // position of last interaction vertex, to be updated
    
    // set energy deposited by the first step
    myMCEvent->AddEdepVol(Physical2Number(Pre->GetPhysicalVolume()),(Float_t)step->GetTotalEnergyDeposit());
    
    // set final process, the first one could be the last
    particle->SetFinalProcess(Process2Number(Post->GetProcessDefinedStep()));
    
    //record primary particle if it reaches the desired volume
    if(SaveSecondaries) RecordSecondaries(step, myMCEvent, SecondaryVolumeName);
    
    if (VerboseLevel>2) {
        cout << "\tEnd of TreatPrimaryParticleFirstStep\n";
    }
    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// used in SteppingAction for MCinfo filling
void DCEventAction::TreatPrimaryParticleNextStep(const G4Step* step)
{
    //    if (VerboseLevel>2) {
    //        cout << "\tBegin of TreatPrimaryParticleNextStep\n";
    //    }
    // get the MC event and primary particle
    MCEvent* myMCEvent = myMC->GetMCEvent(ParticleMap[step->GetTrack()->GetTrackID()]);
    Particle* particle = myMCEvent->GetPrimaryParticle();
    //Particle* particle = myMC->GetParticle(ParticleMap[step->GetTrack()->GetTrackID()]);
    
    // update energy deposition
    myMCEvent->AddEdepVol(Physical2Number(step->GetPreStepPoint()->GetPhysicalVolume()),(Float_t)step->GetTotalEnergyDeposit());
    
    // update final process
    particle->SetFinalProcess(Process2Number(step->GetPostStepPoint()->GetProcessDefinedStep()));
    
    // first interaction vertex if first step was a transportation
    if ( particle->GetPosition(Particle::FirstInteraction) == particle->GetPosition(Particle::Injection)
        && step->GetPostStepPoint()->GetProcessDefinedStep()->GetProcessType() != fTransportation
        ) {
        G4ThreeVector myPos(step->GetPreStepPoint()->GetPosition());
        particle->SetPosition(Particle::FirstInteraction,myPos.x(),myPos.y(),myPos.z()); // position of first interaction vertex
    }
    
    // record kinetic energy when reaching crystal for shielding studies
    //KineticEnergyReachingVolume(step, particle, "crystal");
    
    // update final vertex position
    G4ThreeVector myPos(step->GetPostStepPoint()->GetPosition());
    particle->SetPosition(Particle::LastInteraction,myPos.x(),myPos.y(),myPos.z()); // position of last interaction vertex
    
    //check if primary particle reaches desired volume, if so, record it
    if(SaveSecondaries) RecordSecondaries(step, myMCEvent, SecondaryVolumeName);
    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DCEventAction::TreatSecondaryParticleStep(const G4Step* step)
{
    //    if (VerboseLevel>2) {
    //        cout << "\tBegin of TreatSecondaryParticleStep\n";
    //    }
    // search if the current primary particle is in the ParticleMap
    auto TrackID = step->GetTrack()->GetTrackID();
    if (!ParticleMap.count(TrackID)) {
        auto PrimaryID = ParticleMap[step->GetTrack()->GetParentID()];
        while(PrimaryID > NbPrimary) PrimaryID = ParticleMap[PrimaryID];
        ParticleMap[TrackID] = PrimaryID;
    }
    
    // get the corresponding MC event and update energy deposition made by secondary particle
    MCEvent* myMCEvent = myMC->GetMCEvent(ParticleMap[TrackID]);
    
//    bool sec_Np =(step->GetTrack()->GetDynamicParticle()->GetDefinition()->GetParticleName()).find("Np237") != string::npos;
//    if(sec_Np){
//        cout<<(step->GetTrack()->GetDynamicParticle()->GetDefinition()->GetParticleName())<<endl;
//        G4Track * aTrack = step->GetTrack();
//        aTrack->SetTrackStatus(fStopAndKill);
//    }
    myMCEvent->AddEdepVol(Physical2Number(step->GetPreStepPoint()->GetPhysicalVolume()),(Float_t)step->GetTotalEnergyDeposit());
    
    //record secondary particles entering SecondaryVolumeName
    if(SaveSecondaries) RecordSecondaries(step, myMCEvent, SecondaryVolumeName);
    
    if(VerboseLevel > 1) PrintSecondaryInfo(step, SecondaryVolumeName);
    
    if(VerboseLevel > 2) cout << "\tEnd of TreatSecondaryParticleStep\n";
    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// used in SteppingAction for MCinfo filling
void DCEventAction::TreatNeutronEnding(const G4Step* current_step, const G4Track* recoil_track, G4bool isCapture)
{
    if (VerboseLevel > 2) cout << " @@@@@@@@@@@@@@@@@@@@@@@@@@ TreatNeutronEnding @@@@@@@@@@@@@@@@@@@@@@@@@@\n";
    Particle* particle = myMC->GetMCEvent(ParticleMap[current_step->GetTrack()->GetParentID()])->GetPrimaryParticle();
    G4ThreeVector myPos(current_step->GetPostStepPoint()->GetPosition());
    particle->SetPosition(Particle::LastInteraction,myPos.x(),myPos.y(),myPos.z());
    
    if (recoil_track != nullptr) particle->SetFinalProcess(recoil_track->GetDynamicParticle()->GetPDGcode()+(G4int)isCapture*1000000000);
    else particle->SetFinalProcess(Process2Number(current_step->GetPostStepPoint()->GetProcessDefinedStep()));// if no nucleus bas been found in secondaries
    
    if (VerboseLevel>2) cout << "\tEnd of TreatNeutronEnding\n";
    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DCEventAction::FillData(const G4Event* evt)
{
    // set Edep quenched for last particle
    if (GLG4Scintillation::IsUsed() && NbPrimary!=0) {
        myMC->GetMCEvent(NbPrimary-1)->SetEdepQuenched(GLG4Scintillation::GetTotEdepQuenched());
    }
    
    GLG4PrimaryGeneratorAction* thePGA = GLG4PrimaryGeneratorAction::GetTheGLG4PrimaryGeneratorAction();
    if ( evt->GetPrimaryVertex()->GetPrimary() == nullptr) {
        G4Exception("DCEventAction::FillData","1",RunMustBeAborted,"No primary in this event...");
    }
    
    if (VerboseLevel>-1) {
        //     cout.setf(ios::fixed);
        //     cout.precision(0);
        cout << "Event shot (n°, PDGcode, type, (x,y,z), Ekin, (px,py,pz), universal time, delta time";
        if (VerboseLevel>0) { cout << ", physical volume, logical volume and material): \n"; }
        else { cout << ":\n "; }
        cout << evt->GetEventID() << ", "
        << evt->GetPrimaryVertex()->GetPrimary()->GetPDGcode() << ", "
        << thePGA->GetTypeOfCurrentEvent() << ", "
        << evt->GetPrimaryVertex()->GetPosition() << ", "
        /*<< setprecision(3)*/ << evt->GetPrimaryVertex()->GetPrimary()->GetKineticEnergy() << ", "
        << evt->GetPrimaryVertex()->GetPrimary()->GetMomentum() << ", "
        /*<< setprecision(0)*/ << thePGA->GetUniversalTime()/microsecond << ", "
        << thePGA->GetUniversalTime()/microsecond - UT /*<< defaultfloat*/;
        if (VerboseLevel>0) {
            G4VPhysicalVolume* thePhysiVol = G4TransportationManager::GetTransportationManager()->GetNavigatorForTracking()
            ->LocateGlobalPointAndSetup(evt->GetPrimaryVertex()->GetPosition(),nullptr,false);
            cout<<"  "<<thePhysiVol->GetName()
            <<", "<<thePhysiVol->GetLogicalVolume()->GetName()
            <<", "<<thePhysiVol->GetLogicalVolume()->GetMaterial()->GetName()<<endl;
        } else { cout << endl; }
        UT = thePGA->GetUniversalTime()/microsecond;
    }
    
    // count type of events to get real ratio of detected over generated
    if ( thePGA->GetTypeOfCurrentEvent() == (G4int)OldGene::defered_track_process
        or thePGA->GetTypeOfCurrentEvent() == (G4int)NewGene::defered_track_process
        ) {
        runaction->IncreaseNbEvtDelayed();
    } else {
        runaction->IncreaseNbEvtGenerator();
        //     runaction->IncreaseNbEvtOther();
    }
    
    //    if ( Keep == false
    //        && theHitPMTCollection.GetEntries()==0
    //        ) { // do not save the data if no PMT is hit, to save space for bkg simulation
    //        runaction->Clear();
    //        return;
    //    }
    
    // Primary infos for MCInfo, Particles have already been filled during SteppingAction
    myMC->SetPrimaryType(thePGA->GetTypeOfCurrentEvent());
    myMC->SetTime(thePGA->GetUniversalTime()/microsecond);
    
    //    GLG4HitPMT* a_pmt = nullptr;
    //    Int_t NbPEs=0, NbCount=0;
    
    if (VerboseLevel>0) {
        if (GLG4Scintillation::IsUsed() == true) {
            cout << " GLG4Scint -> Edep= "<< GLG4Scintillation::GetTotEdep()
            << " Edep quenched= "<< GLG4Scintillation::GetTotEdepQuenched()
            << " Num photons= "<< GLG4Scintillation::GetTotNumPhotons()
            << " Num sec= "<< GLG4Scintillation::GetTotNumSecondaries()
            << " Scint centroid= "<< GLG4Scintillation::GetScintCentroid()
            << endl;
        }
        //cout << "(**) MCInfo -> Number of primary particles= "<<myMC->GetNbEvents()<<" Primary type="<<myMC->GetPrimaryType()<<" Einj= "<<myMC->GetEinj()<<" Edep= "<<myMC->GetEdep()<<"\n";
        for (Int_t k=0; k<myMC->GetNbEvents(); k++) {
            MCEvent* myMCEvent = myMC->GetMCEvent(k);
            Particle* particle = myMCEvent->GetPrimaryParticle();
            cout << "\t --- Primary particle properties ---" << endl;
            //cout << "\t  Particle "<<k<<"\tPDG= "<<particle->GetPDG()<<"\tEinj= "<<particle->GetEinj()<<"\tEdep= "<<myMCEvent->GetEdep() << "\n";
            cout << "\t  --- Energy depostion per volume ---" << endl;
            for (UInt_t l=0; l<NbVolumes; l++) {
                G4double Edep_vol = myMCEvent->GetEdepVol(Volumes(l));
                if (Edep_vol > 0.) cout << "\tEdepVol["<<Volumes(l)<<"]= "<<myMCEvent->GetEdepVol(Volumes(l));
            }
            cout << "\n";
            if(myMCEvent->GetSecondaryParticleTCA()->GetEntriesFast()>0){
                cout << "\t --- Saved secondaries reaching " << SecondaryVolumeName << " ---" << endl;
                for (Int_t l=0; l<myMCEvent->GetSecondaryParticleTCA()->GetEntriesFast(); l++) {
                    Particle* mySecPart = myMCEvent->GetSecondaryParticle(l);
                    cout << "\t PDG= " << mySecPart->GetPDG() << "\t Einj= " << mySecPart->GetEinj() << " MeV" << "\n";
                }
            }
            cout << endl;
        }
    }
    
    //    for (Int_t ipmt=0; ipmt<theHitPMTCollection.GetEntries(); ipmt++) { //loop over touched PMTs
    //        a_pmt = theHitPMTCollection.GetPMT(ipmt);
    //
    //        //Sort the hits in time order
    //        a_pmt->SortTimeAscending();
    //
    //        for(Int_t i=0; i<a_pmt->GetEntries(); i++) { //loop over hits of each touched pmt
    //            const GLG4HitPhoton* photon = a_pmt->GetPhoton(i);
    //            NbCount = photon->GetCount();
    //            NbPEs += NbCount;
    //        }
    //    }
    runaction->FillEvent();
}

void DCEventAction::PrintSecondaryInfo(const G4Step* Step, const G4String& VolumeName)
{
    
    G4Track * aTrack = Step->GetTrack();
    
    if(aTrack->GetTrackStatus() < 2 //to avoid seg fault because a track goes outside the world volume
       && Step->GetPreStepPoint()->GetTouchableHandle()->GetVolume()->GetName() != VolumeName
       && Step->GetPostStepPoint()->GetTouchableHandle()->GetVolume()->GetName() == VolumeName){
        
        G4ThreeVector Momentum(Step->GetPreStepPoint()->GetMomentum());
        G4ThreeVector Direction(Step->GetPreStepPoint()->GetMomentumDirection());
        G4ThreeVector PrePosition(Step->GetPreStepPoint()->GetPosition());
        G4ThreeVector PosPosition(Step->GetPostStepPoint()->GetPosition());
        
        
        cout << "\n";
        cout << "*** Found a secondary particle associated to parent particle ID: " << aTrack->GetParentID() <<  endl;
        cout << "\t *Particle: " << Step->GetTrack()->GetDynamicParticle()->GetDefinition()->GetParticleName() << "\n";
        cout << "\t *Volume information: " << Step->GetPreStepPoint()->GetTouchableHandle()->GetVolume()->GetName() << " ===>> " << Step->GetPostStepPoint()->GetTouchableHandle()->GetVolume()->GetName() << "\n" ;
        if (Step->GetPreStepPoint()->GetProcessDefinedStep())
        {
            cout << "\t *Step process information: " << Step->GetPreStepPoint()->GetProcessDefinedStep()->GetProcessName() <<  "\n" ;
        }
        else cout << "\t *Step process information: NONE \n" ; //Apparently, if no process is attributed to a step, it means "transportation". Could see that from displaying all the tracking information related to an event (/tracking/verbose 1 command in a .mac file). MV - Feb 2018
        //cout << "\t *Is first step in volume? " << Step->IsFirstStepInVolume() << "\n";
        cout << "\t *Pre Step position: (" << PrePosition.getX()/cm << "," << PrePosition.getY()/cm << "," << PrePosition.getZ()/cm << ")\n" ;
        cout << "\t *Post Step position: (" << PosPosition.getX()/cm << "," << PosPosition.getY()/cm << "," << PosPosition.getZ()/cm << ")\n" ;
        cout << "\t *Step length: " << Step->GetStepLength()/cm << " cm\n";
        cout << "\t *Momentum: (" << Momentum.getX()/MeV << "," << Momentum.getY()/MeV << "," << Momentum.getZ()/MeV << ")\n" ;
        cout << "\t *Direction: (" << Direction.getX() << "," << Direction.getY() << "," << Direction.getZ() << ")\n" ;
        cout << "\t *Pre Step Kinetic energy: " << Step->GetPreStepPoint()->GetKineticEnergy()/MeV << " MeV \n";
        cout << "\t *Post Step Kinetic energy: " << Step->GetPostStepPoint()->GetKineticEnergy()/MeV << " MeV \n";
        cout << "\t *Energy deposit: " << Step->GetTotalEnergyDeposit()/MeV << " MeV \n";
        cout << "\t *Track kinetic energy: " << aTrack->GetKineticEnergy()/MeV << " MeV \n"  ;
        cout << "\t *Track initial kinetic energy: " << aTrack->GetVertexKineticEnergy()/MeV << " MeV \n"  ;
        cout << "\t *Track ID: " << Step->GetTrack()->GetTrackID() << endl;
        cout << "\t *Process through which track was created: " << aTrack->GetCreatorProcess()->GetProcessName() << endl;
        if(Step->GetPostStepPoint()->GetStepStatus() != fGeomBoundary){
            cout << "Careful: step does not end on geometry boundary...!" << endl;
        }
    }
}

void DCEventAction::RecordSecondaries(const G4Step* step, MCEvent* myMCEvent, const G4String& VolumeName){
    
    G4Track * aTrack = step->GetTrack();
    
    if(aTrack->GetTrackStatus() < 2 //to avoid seg fault because a track goes outside the world volume
       && step->GetPreStepPoint()->GetTouchableHandle()->GetVolume()->GetName() != VolumeName //we want the secondary particle to be in the desired volume for the 1st time
       && step->GetPostStepPoint()->GetTouchableHandle()->GetVolume()->GetName() == VolumeName){
        
        // check if secondary particle has not been already created (could for example happen that a secondary particle scatters back & forth into the desired volume)
        G4int TrackID = aTrack->GetTrackID();
        if (SecondaryParticleMap.count(TrackID) == 0) {
            SecondaryParticleMap[aTrack->GetTrackID()] = NbSecondary;
            
            //create secondary particle
            //cout << aTrack->GetTrackID() << "\n" ; //to check that two secondary particles with same trackID are not created...
            
            myMCEvent->ConstructOneParticleAt(NbSecondary);
            Particle* mySecPart = myMCEvent->GetSecondaryParticle(NbSecondary);
            NbSecondary++;
            
            G4StepPoint* Post = step->GetPostStepPoint();
            //G4ThreeVector Momentum(step->GetPreStepPoint()->GetMomentum());
            G4ThreeVector Direction(Post->GetMomentumDirection());
            G4ThreeVector Position(Post->GetPosition()); // normally, when a particle crosses a boundary, the post step point position should lie on the boundary.
            
            // set desired values for secondary particle
            mySecPart->SetVolInj(Physical2Number(Post->GetPhysicalVolume()));
            mySecPart->SetEinj(Post->GetKineticEnergy()); // kinetic energy at injection vertex
            mySecPart->SetPDG(step->GetTrack()->GetDynamicParticle()->GetPDGcode()); // particle PDG code
            
            mySecPart->SetPosition(Particle::Injection,Position.x(),Position.y(),Position.z()); // position of the intersection between track and volume boundary
            mySecPart->SetMomentumDirection(Particle::Injection,Direction.x(),Direction.y(),Direction.z()); // direction of momentum at the injection vertex
            
            if(step->GetPostStepPoint()->GetStepStatus() != fGeomBoundary){
                cout << "DCEventAction::RecordSecondaries() WARNING: step does not end on geometry boundary...!" << endl;
            }
            if (VerboseLevel > 2) cout << "\tSaved secondary\n";
            
            //kill the track?
            if (KillTrack && TrackID > 1){ //we don't want to kill the primary particle...
                aTrack->SetTrackStatus(fStopAndKill);
                if (VerboseLevel > 2) cout << "DCEventAction::RecordSecondaries() Track ID " << TrackID << " stopped and killed\n";
            }
        }
    }
}

