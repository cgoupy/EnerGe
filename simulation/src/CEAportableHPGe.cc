#include "CEAportableHPGe.hh"

#include <G4Tubs.hh>
#include <G4Polycone.hh>
#include <G4Orb.hh>

#include <G4VisExtent.hh>
#include <G4VisAttributes.hh>
#include <G4Colour.hh>

#include <G4SystemOfUnits.hh>

#include <iomanip>
#include <cstdio>

G4VSolid* LWO_holder(const G4String& name, G4double Boxes_gap);

CEAportableHPGe::CEAportableHPGe():
overlap_test(ParamBase::GetDataBase().Get<bool>("overlap_test")),
construction_verbose_level(ParamBase::GetDataBase().Get<int>("construction_verboselevel")){}

void CEAportableHPGe::Build(G4LogicalVolume* mother, G4ThreeVector& Pos0){

  // Empty sphere fully containing the setup; it can be rotated about the Y axis, if needed, changing roY angle
  G4RotationMatrix* roY = new G4RotationMatrix;  roY->rotateY(270.*deg);               // 270 deg --> HPGe axis horizontal, x>0949.21690
//  auto GeGhost = new Volume<G4Orb>("GeGhost", "Air", mother, roY, Pos0+G4ThreeVector(-262*mm, 0, 0), 400.*mm);
//  GeGhost->GetLogic()->SetVisAttributes(new G4VisAttributes(false));
    auto GeGhost = new Volume<G4Tubs>("GeGhost", "Air", mother, roY, Pos0+G4ThreeVector(-287*mm, 0, 0), 0, 120.*mm, 318.*mm, 0*deg, 360*deg);
    
    //Detector position with respect to the sphere center
  G4ThreeVector Pos = G4ThreeVector(0, 0, 287.*mm);   // Origin at centre of the HPGe crystal

  auto DewarWalls = new Volume<G4Tubs>("DewarWalls", "Aluminium", GeGhost->GetLogic(), Pos+G4ThreeVector(0, 0, -433.5*mm), 0, 111.*mm, 171.5*mm, 0*deg, 360*deg);
  auto DewarVac = new Volume<G4Tubs>("DewarVacuum", "Vac", DewarWalls->GetLogic(), {0, 0, 0}, 0, 107.*mm, 167.5*mm, 0*deg, 360*deg);
  new Volume<G4Tubs>("LiquidN2", "LN2", DewarVac->GetLogic(), {0, 0, 0}, 0,  86.*mm, 146.5*mm, 0*deg, 360*deg);

  G4double zPla0[8] = {   0.0*mm,   3.0*mm,   3.0*mm, 127.0*mm, 127.0*mm, 234.5*mm, 234.5*mm, 236.0*mm};
  G4double rInn0[8] = {   5.75*mm,   5.75*mm,  36.6*mm,  36.6*mm,  36.6*mm,  36.6*mm, 32.25*mm, 32.25*mm};
  G4double rOut0[8] = {  39.5*mm,  39.5*mm,  39.5*mm,  39.5*mm,  38.1*mm,  38.1*mm,  38.1*mm,  38.1*mm};
  G4Polycone* Endcap_sol = new G4Polycone("VHsolid_Endcap", 0*deg, 360*deg, 8, zPla0, rInn0, rOut0);
  G4LogicalVolume* Endcap_log = new G4LogicalVolume(Endcap_sol, G4Material::GetMaterial("Aluminium"),"VHlogic_Endcap");
  new G4PVPlacement(nullptr,Pos+G4ThreeVector(0,0,-205.25*mm),Endcap_log,"VHphysic_Endcap",GeGhost->GetLogic(),false,0,overlap_test);
  Endcap_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));

  new Volume<G4Tubs>("BeWindow", "Beryllium", GeGhost->GetLogic(), Pos+G4ThreeVector(0,0,29*mm), 0,  32.5*mm, 0.25*mm, 0*deg, 360*deg);

    auto CapVacuum = new Volume<G4Tubs>("CapVacuum", "Vac", GeGhost->GetLogic(), Pos-G4ThreeVector(0,0,86.5*mm), 0,  36.6*mm, 115.75*mm, 0*deg, 360*deg);
//  CapVacuum->GetLogic()->SetVisAttributes(new G4VisAttributes(false));

  G4double zPla2[18] = {  0.0*mm,  6.0*mm,  6.0*mm, 25.0*mm, 25.0*mm, 28.0*mm, 28.0*mm, 54.0*mm, 54.0*mm, 60.0*mm, 60.0*mm, 83.0*mm, 83.0*mm, 89.0*mm, 89.0*mm,105.5*mm,105.5*mm,107.0*mm};
  G4double rInn2[18] = {  5.75*mm,  5.75*mm,  6.5*mm,  6.5*mm,  6.5*mm,  6.5*mm,28.75*mm,28.75*mm,28.75*mm,28.75*mm,28.75*mm,28.75*mm,28.75*mm,28.75*mm,28.75*mm,28.75*mm,28.75*mm,28.75*mm};
  G4double rOut2[18] = { 12.5*mm, 12.5*mm, 12.5*mm, 12.5*mm,29.75*mm,29.75*mm,29.75*mm,29.75*mm,32.25*mm,32.25*mm,29.75*mm,29.75*mm,32.25*mm,32.25*mm,29.75*mm,29.75*mm,32.25*mm,32.25*mm};
  G4Polycone* Holder_sol = new G4Polycone("VHsolid_Holder", 0*deg, 360*deg, 18, zPla2, rInn2, rOut2);
  G4LogicalVolume* Holder_log = new G4LogicalVolume(Holder_sol, G4Material::GetMaterial("Aluminium"),"VHlogic_Holder");
  new G4PVPlacement(nullptr,{0,0,8.25*mm},Holder_log,"VHphysic_Holder",CapVacuum->GetLogic(),false,0,overlap_test);
  Holder_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));

  G4double zPla3[4] = {  0.0*mm, 44.0*mm, 44.0*mm, 55.0*mm};
  G4double rInn3[4] = { 5.75*mm, 5.75*mm,  0.0*mm,  0.0*mm};
  G4double rOut3[4] = {27.75*mm,27.75*mm,27.75*mm,27.75*mm};
  G4Polycone* GeDead_sol = new G4Polycone("VHsolid_GeDead", 0*deg, 360*deg, 4, zPla3, rInn3, rOut3);
  G4LogicalVolume* GeDead_log = new G4LogicalVolume(GeDead_sol, G4Material::GetMaterial("Germanium"),"VHlogic_GeDead");
  new G4PVPlacement(nullptr,{0,0,58.75*mm},GeDead_log,"VHphysic_GeDead",CapVacuum->GetLogic(),false,0,overlap_test);
  GeDead_log->SetVisAttributes(GLG4Material::GetVisAttribute("Germanium"));

  G4double zPla4[4] = {    0.0*mm,   44.0*mm,   44.0*mm,54.9997*mm};
  G4double rInn4[4] = {   6.25*mm,   6.25*mm,    0.0*mm,    0.0*mm};
  G4double rOut4[4] = {27.7497*mm,27.7497*mm,27.7497*mm,27.7497*mm};
  G4Polycone* HPGe_sol = new G4Polycone("VHsolid_HPGe", 0*deg, 360*deg, 4, zPla4, rInn4, rOut4);
  G4LogicalVolume* HPGe_log = new G4LogicalVolume(HPGe_sol, G4Material::GetMaterial("Germanium"),"VHlogic_HPGe");
  new G4PVPlacement(nullptr,{0,0,0},HPGe_log,"VHphysic_HPGe",GeDead_log,false,0,overlap_test);
  HPGe_log->SetVisAttributes(GLG4Material::GetVisAttribute("Germanium"));
 
  new Volume<G4Tubs>("ColdFinger_end", "Copper", CapVacuum->GetLogic(), {0,0,-6.5*mm}, 0,  5.75*mm, 109.25*mm, 0*deg, 360*deg);
  new Volume<G4Tubs>("ColdFinger_mid", "Copper", GeGhost->GetLogic(), Pos+G4ThreeVector(0,0,(-203.25-29.375)*mm), 0,  5.75*mm, 29.375*mm, 0*deg, 360*deg);
    
  if(construction_verbose_level > 0) {
    std::cout << "\n\t ++++++++++ CEA portable HPGe Geometry +++++++++\n";

    G4VisExtent ve = GeDead_sol->GetExtent();
    std::cout << "\t\tHPGe diameter: " << (ve.GetXmax()-ve.GetXmin())/mm << " mm" << std::endl;
    std::cout << "\t\tHPGE height:   " << (ve.GetZmax()-ve.GetZmin())/mm << " mm" << std::endl;
    std::cout << "\t\tHPGe mass (g): " << GeDead_log->GetMass()/g <<  std::endl;

    G4ThreeVector PosGe = Pos0;
    std::cout << "\t\tBounding box in the volume (" << mother->GetName() << "):\n" <<
      "\t\t   X:[" << PosGe.z()-ve.GetZmax()/2. << ", " << PosGe.z()+ve.GetZmax()/2. << "] mm \n" <<
      "\t\t   Y:[" << PosGe.x()+ve.GetXmin()/2. << ", " << PosGe.x()+ve.GetXmax()/2. << "] mm \n" <<
      "\t\t   Z:[" << PosGe.y()+ve.GetYmin()/2. << ", " << PosGe.y()+ve.GetYmax()/2. << "] mm \n";

    std::cout << "\t +++++++++++++++++++++++++++++++++++++++++++++++\n\n";
    }
}
