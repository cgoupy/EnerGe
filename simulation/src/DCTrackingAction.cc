#include <DCTrackingAction.hh>

#include <DCTrajectory.hh>

#include <G4Track.hh>
#include <G4TrackingManager.hh>
#include <G4ParticleTypes.hh>
#include <G4OpticalPhoton.hh>

void DCTrackingAction::PreUserTrackingAction(const G4Track* aTrack)
{
  G4float percentageOfCherenkovPhotonsToDraw = 0.001;

  if ( aTrack->GetDefinition() != G4OpticalPhoton::OpticalPhotonDefinition()
       || G4UniformRand() < percentageOfCherenkovPhotonsToDraw )
    {
      DCTrajectory* thisTrajectory = new DCTrajectory(aTrack);
      fpTrackingManager->SetTrajectory(thisTrajectory);
      fpTrackingManager->SetStoreTrajectory(true);
    }
  else {
    fpTrackingManager->SetStoreTrajectory(false);
  }
}
