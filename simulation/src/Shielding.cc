#include "Shielding.hh"
#include "DCVolumeHandler.hh"

#include <GLG4Material.hh>
#include <Parameters.hh>

#include <G4Box.hh>
#include <G4Tubs.hh>
#include <G4Torus.hh>
#include <G4Polycone.hh>
#include <G4UnionSolid.hh>
#include <G4SubtractionSolid.hh>
#include <G4MultiUnion.hh>

#include <G4LogicalVolume.hh>
#include <G4VPhysicalVolume.hh>
#include <G4PVPlacement.hh>

#include<G4LogicalVolumeStore.hh>

#include <G4SystemOfUnits.hh>

#include <G4VisAttributes.hh>
#include <G4Colour.hh>

#include <iomanip>
#include <list>

Shielding::Shielding(): overlap_test(ParamBase::GetDataBase().Get<bool>("overlap_test")),
construction_verbose_level(ParamBase::GetDataBase().Get<int>("construction_verboselevel")){
    ShieldingLayers.clear();
}

void Shielding::PrintShieldingMass() const{
    
    G4double totalmass = 0;
    if (ShieldingLayers.size()){
        
        std::cout << "\n******************** SHIELDING MASS ******************\n";
        std::cout << "--------------------------------------------------------\n";
        std::cout << " >> Material <<                         >> Mass <<      \n";
        std::cout << "--------------------------------------------------------\n";
        
        for (const auto& layer : ShieldingLayers){
            
            totalmass += layer->GetMass(true,false);
            std::cout << std::left << std::setw(40) << layer->GetName() << std::setw(7) << layer->GetMass(false,false)/kg <<" kg\n";
            
        }
        std::cout << "--------------------------------------------------------\n";
        std::cout << " --> TOTAL:                             " << totalmass/kg << " kg\n\n";
    }
}

OrsayLeadShielding::OrsayLeadShielding(G4double ExternalSizeX, G4double ExternalSizeY, G4double ExternalSizeZ, G4double Thickness, G4double InnerSizeX, G4double InnerSizeY, G4double InnerSizeZ): ExternalSizeX(ExternalSizeX), ExternalSizeY(ExternalSizeY), ExternalSizeZ(ExternalSizeZ), Thickness(Thickness), InnerSizeX(InnerSizeX), InnerSizeY(InnerSizeY), InnerSizeZ(InnerSizeZ)
{
    if(construction_verbose_level > 1) {
        std::cout << "\n   ***** Lead shielding geometry *****\n";
        std::cout << "\t- " << material << " with " << ExternalSizeX/2./cm-InnerSizeX/2./cm << " cm thickness\n";
        std::cout << "\t- Shielding external size: "<< ExternalSizeX/cm << " cm x " << ExternalSizeY/cm << " cm x " << ExternalSizeZ/cm << " cm \n";
        std::cout << "\n   *****************************\n";
    }
}

void OrsayLeadShielding::ConstructNew(G4LogicalVolume *mother, const G4ThreeVector &Pos){
    G4Box* ShieldingBox = new G4Box("ShieldingBox",  ExternalSizeX/2., ExternalSizeY/2., ExternalSizeZ/2.);
    G4Box* ShieldingInt = new G4Box("ShieldingInt",  InnerSizeX/2., InnerSizeY/2., InnerSizeZ/2.);
    G4SubtractionSolid* Shielding_sol = new G4SubtractionSolid("Shielding_sol", ShieldingBox, ShieldingInt, nullptr, G4ThreeVector(0.,0.,ExternalSizeZ/2.-InnerSizeZ/2.));
    G4LogicalVolume* Shielding_log = new G4LogicalVolume(Shielding_sol, GLG4Material::GetMaterial(material), "Shielding_log");
    new G4PVPlacement(0, Pos,Shielding_log,"VHphysic_Shielding",mother,false,0, overlap_test);
    Shielding_log->SetVisAttributes(GLG4Material::GetVisAttribute(material));
    ShieldingLayers.push_back(Shielding_log);
}

void OrsayLeadShielding::ConstructOld(G4LogicalVolume *mother, const G4ThreeVector &Pos){
    G4Box* ShieldingBox = new G4Box("ShieldingBox",  ExternalSizeX/2., ExternalSizeY/2., ExternalSizeZ/2.);
    G4Box* ShieldingInt = new G4Box("ShieldingInt",  InnerSizeX/2., InnerSizeY/2., InnerSizeZ/2.);
    G4Box* ShieldingGap = new G4Box("ShieldingGap",  5*cm, ExternalSizeY/2.-InnerSizeY/2., 15*cm);
    G4SubtractionSolid* Shielding_sol = new G4SubtractionSolid("Shielding_sol", ShieldingBox, ShieldingInt, nullptr, G4ThreeVector(0.,(ExternalSizeY/2.-InnerSizeY/2.)-Thickness,ExternalSizeZ/2.-InnerSizeZ/2.));
    G4SubtractionSolid* Shielding_f_sol = new G4SubtractionSolid("Shielding_f_sol", Shielding_sol, ShieldingGap, nullptr, G4ThreeVector(0.,-(Thickness+InnerSizeY/2.),ExternalSizeZ/2.-17.5/2));
    G4LogicalVolume* Shielding_log = new G4LogicalVolume(Shielding_f_sol, GLG4Material::GetMaterial(material), "Shielding_log");
    new G4PVPlacement(0, Pos - G4ThreeVector(0., (ExternalSizeY/2.-InnerSizeY/2.)-Thickness,0.), Shielding_log,"VHphysic_Shielding",mother,false,0, overlap_test);
    Shielding_log->SetVisAttributes(GLG4Material::GetVisAttribute(material));
    ShieldingLayers.push_back(Shielding_log);
}

/////////////////////////////////////
/////// NUCLEUS SHIELDING ////////
/////////////////////////////////////

//Forward declaration of some necessary functions
G4VSolid* UPNBeam80x45(const G4String &name, G4double height);
G4VSolid* UPNBeam100x55(const G4String &name, G4double height);
G4VSolid* Rail(const G4String &name, G4double length);

NUCLEUSLeadShielding::NUCLEUSLeadShielding(Layer outlayer_): outLayer(std::move(outlayer_)){
    
    if(construction_verbose_level > 1) {
        std::cout << "\n   ***** NUCLEUS passive shielding geometry *****\n";
        
        std::cout << "\t- Outer layer: " << outLayer.material << " with " << outLayer.thickness/cm << " cm thickness\n";
        std::cout << "\t- Steel plate below the Outer layer up: " << steel_plate_thickness/cm << " cm thickness\n";
        std::cout << "\t- Shielding external size: "<< GetExternalSizeX()/cm << " cm x " << GetExternalSizeY()/cm << " cm x " << GetExternalSizeZ()/cm << " cm \n";
        std::cout << "\n   *****************************\n";
    }
}

G4VSolid* NUCLEUSLeadShielding::ConstructLayer(const G4String &name, G4double OuterHeight, G4double OuterWidth, G4double InnerHeight, G4double InnerWidth, G4double rHole){
    
    G4VSolid* box1 = new G4Box("Box1",OuterWidth/2,OuterWidth/2,OuterHeight/2);
    
    //Volume to be extruded
    G4VSolid* box2 = new G4Box("Box2",InnerWidth/2,InnerWidth/2,InnerHeight/2);
    
    //Hole on the top layer to be extruded
    G4VSolid* disk = new G4Tubs("Hole",0.,rHole, OuterHeight/4-InnerHeight/4+1*mm, 0*deg, 360*deg);
    
    //Subtraction of solids
    G4SubtractionSolid* sub1 = new G4SubtractionSolid("s1",box1,box2,nullptr,{0,0,0});
    G4SubtractionSolid* sub2 = new G4SubtractionSolid(name,sub1,disk,nullptr,G4ThreeVector(0,0,OuterHeight/4+InnerHeight/4));
    
    return sub2;
}

G4LogicalVolume* NUCLEUSLeadShielding::Construct(G4LogicalVolume* mother, const G4ThreeVector &Pos){

    const double InnerVolWidth = 43*CLHEP::cm;
    
    const double TotalHeight = GetExternalSizeZ();
    const double TotalWidth = GetExternalSizeX();
    
    //First shielding layer (out)
    auto b1 = ConstructLayer("Layer1_sol", TotalHeight, TotalWidth, TotalHeight-2.*outLayer.thickness, TotalWidth-2.*outLayer.thickness, InnerVolWidth/2.);
    G4LogicalVolume* b1_log = new G4LogicalVolume(b1,G4Material::GetMaterial(outLayer.material),"VHlogic_outLayer");
    b1_log->SetVisAttributes(GLG4Material::GetVisAttribute(outLayer.material));
    new G4PVPlacement(nullptr,Pos,b1_log,"VHphysic_outLayer",mother,false,0,overlap_test);
    ShieldingLayers.push_back(b1_log);
    
    //Steel plate
    G4VSolid* sp1 = new G4Box("sp1",362.5*mm,364.165*mm,steel_plate_thickness/2);
    G4VSolid* sp2 = new G4Box("sp2",365.0*mm, 25.665*mm,steel_plate_thickness);
    G4VSolid* sp3 = new G4Tubs("sp3",0.,220.0*mm, steel_plate_thickness, 0*deg, 360*deg);
    G4SubtractionSolid* sp4 = new G4SubtractionSolid("sp4",sp1,sp2,nullptr,G4ThreeVector(0,0,0));
    G4SubtractionSolid* sp_sol = new G4SubtractionSolid("SteelPlate_sol",sp4,sp3,nullptr,G4ThreeVector(0,0,0));
    G4LogicalVolume* sp_log = new G4LogicalVolume(sp_sol,G4Material::GetMaterial("Steel"),"VHlogic_SteelPlate");
    sp_log->SetVisAttributes(GLG4Material::GetVisAttribute("Steel"));
    G4ThreeVector SP_Pos =   Pos + G4ThreeVector(0, 0, TotalHeight/2-outLayer.thickness-steel_plate_thickness/2);
    new G4PVPlacement(nullptr,SP_Pos,sp_log,"VHphysic_SteelPlate",mother,false,0,overlap_test);
    ShieldingLayers.push_back(sp_log);

    G4ThreeVector Layer_Pos  = Pos + G4ThreeVector(0,0,-steel_plate_thickness/2);
    
    // =================== OPTIONAL MUON VETO, COLD PASSIVE SHIELD AND SUPPORT FRAME ===================;
    AddSupportFrame(mother, Pos);
    AddMuonVeto(mother, Pos);
    if (ParamBase::GetDataBase().Get<int>("add_cold_lead_disk"))AddColdPassiveShield(mother, Pos+G4ThreeVector( 0, 0, 20*cm));
    // =================================================================================================

    if(construction_verbose_level > 1) {
        PrintShieldingMass();
        std::cout << "Shielding position in " << mother->GetName() << ": ("<< Pos.x() << "," << Pos.y() << "," << Pos.z() << ")\n" ;
    }

    return b1_log;
}



void NUCLEUSLeadShielding::AddMuonVeto(G4LogicalVolume* mother, const G4ThreeVector &Pos){
    
    if(construction_verbose_level > 1) std::cout << "\n   ***** External Muon Veto construction *****\n";
    
    // See pdf file at wiki.cryocluster.org/display/NUC/Muon+Veto+geometry for Parts A,B,C,D definition
    
    // Half-size dimensions for the panel scintillators in part A
    const G4double dx1_A1 = 142.225*mm;
    const G4double dx2_A1 = 145.515*mm;
    const G4double dx1_A2 = 150.76*mm;
    const G4double dx2_A2 = 144.175*mm;
    const G4double dy_A   = 314.00*mm;
    const G4double dz_A   = 25.*mm;
    const G4double gapA   = 0.4*mm;               // Full gap
    const G4double gapAA  = 4.2*mm;               // Full gap @ y=0 between tw0 A blocks
    const G4double rr_A   = 221.*mm;
    const G4double gapAPb = 53.40*mm;             // Space between upper muon veto panels and lead shield
    
    // Half-size dimensions for the panel scintillators in Part B
    const G4double dx_B  = 25.*mm;
    const G4double dy_B  = 141.*mm;
    const G4double dz_B  = 590.*mm;
    const G4double gapB  = 17./3.*mm;             // Full gap
    const G4double gapBB = 17./3.*mm;             // Full gap @ y=0 between two B blocks
    
    // Half-size dimensions for the panel scintillators in Part C
    const G4double dx_C  = 639.5*mm;
    const G4double dy_C  = 25.*mm;
    const G4double dz_C  = 121.5*mm;
    const G4double gapC  = 5.*mm;                 // Full gap
    const G4double gapAC = 9.5*mm;                // Full z gap between bottom A and top C
    const G4double gapBC = 17.5*mm;               // Full y gap between side B and C
    
    // Half-size dimensions for the panel scintillators in Part D
    const G4double dx_D   = 129.5*mm;
    const G4double dy_D   = 640.0*mm;
    const G4double dz_D   = 25.*mm;
    const G4double gapD1  = 5.*mm;                // Full gap between top D panels
    const G4double gapD2  = 431.0*mm;             // Full gap between bottom D panels
    const G4double gapD   = 15.*mm;               // Full gap between top and bottom D panels
    
    const G4double gapAD  = 1106.0*mm;            // Space between panels A and bottom D panels
    
    // Offsets of the different panels with respect to the center of the NUCLEUS passive shield
    const G4double offset_A = GetExternalSizeZ()/2. + gapAPb + dz_A;        // z=   +508.4*mm;
    const G4double offset_B = dx_C-dx_B;                                    // x=+/-(614.5)*mm;
    const G4double offset_C = 4.*dy_B + gapBB/2. + gapB + gapBC + dy_C;     // y=+/-(615.0)*mm;
    const G4double offset_D = offset_A - dz_A - gapAD - dz_D;               // z=   -647.6*mm;

    G4String MVmat = "PVT";                       // Muon veto panels active material

    // Panels A
    G4RotationMatrix* RoA1 = new G4RotationMatrix();
    G4RotationMatrix* RoA4 = new G4RotationMatrix();
    RoA1->rotateX(90.*deg);
    RoA4->rotateX(90.*deg);
    RoA4->rotateZ(180.*deg);
    
    G4ThreeVector A1Pos =   Pos + G4ThreeVector( -dx1_A1/2.-dx2_A1/2.-dx1_A2-dx2_A2-1.5*gapA, -dy_A-0.5*gapAA, offset_A);
    G4ThreeVector A2Pos =   Pos + G4ThreeVector( -(dx1_A2+dx2_A2)/2.-0.5*gapA,                -dy_A-0.5*gapAA, offset_A);
    G4ThreeVector A3Pos =   Pos + G4ThreeVector( +(dx1_A2+dx2_A2)/2.+0.5*gapA,                -dy_A-0.5*gapAA, offset_A);
    G4ThreeVector A4Pos =   Pos + G4ThreeVector( +dx1_A1/2.+dx2_A1/2.+dx1_A2+dx2_A2+1.5*gapA, -dy_A-0.5*gapAA, offset_A);
    G4ThreeVector A5Pos = A1Pos + G4ThreeVector( 0, +2.*dy_A+gapAA, 0);
    G4ThreeVector A6Pos = A2Pos + G4ThreeVector( 0, +2.*dy_A+gapAA, 0);
    G4ThreeVector A7Pos = A3Pos + G4ThreeVector( 0, +2.*dy_A+gapAA, 0);
    G4ThreeVector A8Pos = A4Pos + G4ThreeVector( 0, +2.*dy_A+gapAA, 0);
    
    G4VSolid* hole = new G4Tubs("hole", 0., rr_A, 2.*dz_A, 0*deg, 360*deg);
    G4VSolid* A2tmp = new G4Trd("A2tmp", dx1_A2, dx2_A2, dy_A, dy_A, dz_A);
    G4VSolid* A3tmp = new G4Trd("A3tmp", dx2_A2, dx1_A2, dy_A, dy_A, dz_A);
    
    auto A1 = new Volume<G4Trap>("MVPanelA1", MVmat, mother, RoA1, A1Pos, 2.*dy_A, 2.*dz_A, 2.*dx2_A1, 2.*dx1_A1);
    
    G4SubtractionSolid* A2_sol = new G4SubtractionSolid("VHsolid_MVPanelA2",A2tmp,hole,nullptr,G4ThreeVector(+(dx1_A2+dx2_A2)/2.+0.5*gapA,+dy_A, 0));
    G4LogicalVolume* A2_log = new G4LogicalVolume(A2_sol,GLG4Material::GetMaterial(MVmat),"VHlogic_MVPanelA2");
    A2_log->SetVisAttributes(GLG4Material::GetVisAttribute(MVmat));
    new G4PVPlacement(nullptr,A2Pos,A2_log,"VHphysic_MVPanelA2",mother,false,0,overlap_test);
    
    G4SubtractionSolid* A3_sol = new G4SubtractionSolid("VHsolid_MVPanelA3",A3tmp,hole,nullptr,G4ThreeVector(-(dx1_A2+dx2_A2)/2.-0.5*gapA,+dy_A, 0));
    G4LogicalVolume* A3_log = new G4LogicalVolume(A3_sol,GLG4Material::GetMaterial(MVmat),"VHlogic_MVPanelA3");
    A3_log->SetVisAttributes(GLG4Material::GetVisAttribute(MVmat));
    new G4PVPlacement(nullptr,A3Pos,A3_log,"VHphysic_MVPanelA3",mother,false,0,overlap_test);
    
    auto A4 = new Volume<G4Trap>("MVPanelA4", MVmat, mother, RoA4, A4Pos, 2.*dy_A, 2.*dz_A, 2.*dx2_A1, 2.*dx1_A1);
    auto A5 = new Volume<G4Trap>("MVPanelA5", MVmat, mother, RoA1, A5Pos, 2.*dy_A, 2.*dz_A, 2.*dx1_A1, 2.*dx2_A1);
    
    G4SubtractionSolid* A6_sol = new G4SubtractionSolid("VHsolid_MVPanelA6",A3tmp,hole,nullptr,G4ThreeVector(+(dx1_A2+dx2_A2)/2.+0.5*gapA,-dy_A, 0));
    G4LogicalVolume* A6_log = new G4LogicalVolume(A6_sol,GLG4Material::GetMaterial(MVmat),"VHlogic_MVPanelA6");
    A6_log->SetVisAttributes(GLG4Material::GetVisAttribute(MVmat));
    new G4PVPlacement(nullptr,A6Pos,A6_log,"VHphysic_MVPanelA6",mother,false,0,overlap_test);
    
    G4SubtractionSolid* A7_sol = new G4SubtractionSolid("VHsolid_MVPanelA7",A2tmp,hole,nullptr,G4ThreeVector(-(dx1_A2+dx2_A2)/2.-0.5*gapA,-dy_A, 0));
    G4LogicalVolume* A7_log = new G4LogicalVolume(A7_sol,GLG4Material::GetMaterial(MVmat),"VHlogic_MVPanelA7");
    A7_log->SetVisAttributes(GLG4Material::GetVisAttribute(MVmat));
    new G4PVPlacement(nullptr,A7Pos,A7_log,"VHphysic_MVPanelA7",mother,false,0,overlap_test);
    
    auto A8 = new Volume<G4Trap>("MVPanelA8", MVmat, mother, RoA4, A8Pos, 2.*dy_A, 2.*dz_A, 2.*dx1_A1, 2.*dx2_A1);
    
    
    // Panels B
    G4ThreeVector B1Pos =   Pos + G4ThreeVector(+offset_B, -3.*dy_B -0.5*gapBB -gapB, offset_A+dz_A-dz_B);
    G4ThreeVector B2Pos = B1Pos + G4ThreeVector(       0., +2.*dy_B + gapB,    0.);
    G4ThreeVector B3Pos = B2Pos + G4ThreeVector(       0., +2.*dy_B + gapBB,   0.);
    G4ThreeVector B4Pos = B3Pos + G4ThreeVector(       0., +2.*dy_B + gapB,    0.);
    
    auto B1 = new Volume<G4Box>("MVPanelB1", MVmat, mother, B1Pos, dx_B, dy_B, dz_B);
    auto B2 = new Volume<G4Box>("MVPanelB2", MVmat, mother, B2Pos, dx_B, dy_B, dz_B);
    auto B3 = new Volume<G4Box>("MVPanelB3", MVmat, mother, B3Pos, dx_B, dy_B, dz_B);
    auto B4 = new Volume<G4Box>("MVPanelB4", MVmat, mother, B4Pos, dx_B, dy_B, dz_B);
    
    G4ThreeVector B5Pos =   Pos + G4ThreeVector(-offset_B, -3.*dy_B -0.5*gapBB -gapB, offset_A+dz_A-dz_B);
    G4ThreeVector B6Pos = B5Pos + G4ThreeVector(       0., +2.*dy_B + gapB,    0.);
    G4ThreeVector B7Pos = B6Pos + G4ThreeVector(       0., +2.*dy_B + gapBB,   0.);
    G4ThreeVector B8Pos = B7Pos + G4ThreeVector(       0., +2.*dy_B + gapB,    0.);
    
    auto B5 = new Volume<G4Box>("MVPanelB5", MVmat, mother, B5Pos, dx_B, dy_B, dz_B);
    auto B6 = new Volume<G4Box>("MVPanelB6", MVmat, mother, B6Pos, dx_B, dy_B, dz_B);
    auto B7 = new Volume<G4Box>("MVPanelB7", MVmat, mother, B7Pos, dx_B, dy_B, dz_B);
    auto B8 = new Volume<G4Box>("MVPanelB8", MVmat, mother, B8Pos, dx_B, dy_B, dz_B);

    
    // Panels C
    G4ThreeVector C1Pos =   Pos + G4ThreeVector(0., +offset_C, offset_A-dz_A-gapAC-7.*dz_C-3.*gapC);
    G4ThreeVector C2Pos = C1Pos + G4ThreeVector(0.,         0, +2.*dz_C + gapC);
    G4ThreeVector C3Pos = C2Pos + G4ThreeVector(0.,         0, +2.*dz_C + gapC);
    G4ThreeVector C4Pos = C3Pos + G4ThreeVector(0.,         0, +2.*dz_C + gapC);
    
    auto C1 = new Volume<G4Box>("MVPanelC1", MVmat, mother, C1Pos, dx_C, dy_C, dz_C);
    auto C2 = new Volume<G4Box>("MVPanelC2", MVmat, mother, C2Pos, dx_C, dy_C, dz_C);
    auto C3 = new Volume<G4Box>("MVPanelC3", MVmat, mother, C3Pos, dx_C, dy_C, dz_C);
    auto C4 = new Volume<G4Box>("MVPanelC4", MVmat, mother, C4Pos, dx_C, dy_C, dz_C);
    
    G4ThreeVector C5Pos =   Pos + G4ThreeVector(0., -offset_C, offset_A-dz_A-gapAC-7.*dz_C-3.*gapC);
    G4ThreeVector C6Pos = C5Pos + G4ThreeVector(0.,         0, +2.*dz_C + gapC);
    G4ThreeVector C7Pos = C6Pos + G4ThreeVector(0.,         0, +2.*dz_C + gapC);
    G4ThreeVector C8Pos = C7Pos + G4ThreeVector(0.,         0, +2.*dz_C + gapC);
    
    auto C5 = new Volume<G4Box>("MVPanelC5", MVmat, mother, C5Pos, dx_C, dy_C, dz_C);
    auto C6 = new Volume<G4Box>("MVPanelC6", MVmat, mother, C6Pos, dx_C, dy_C, dz_C);
    auto C7 = new Volume<G4Box>("MVPanelC7", MVmat, mother, C7Pos, dx_C, dy_C, dz_C);
    auto C8 = new Volume<G4Box>("MVPanelC8", MVmat, mother, C8Pos, dx_C, dy_C, dz_C);
    
    
    // Panels D
    G4ThreeVector D1Pos =   Pos + G4ThreeVector( -dx_D-0.5*gapD2, 0., offset_D);
    G4ThreeVector D2Pos =   Pos + G4ThreeVector( -dx_D-0.5*gapD1, 0., offset_D+2.*dz_D+gapD);
    G4ThreeVector D3Pos =   Pos + G4ThreeVector( +dx_D+0.5*gapD1, 0., offset_D+2.*dz_D+gapD);
    G4ThreeVector D4Pos =   Pos + G4ThreeVector( +dx_D+0.5*gapD2, 0., offset_D);
    
    auto D1 = new Volume<G4Box>("MVPanelD1", MVmat, mother, D1Pos, dx_D, dy_D, dz_D);
    auto D2 = new Volume<G4Box>("MVPanelD2", MVmat, mother, D2Pos, dx_D, dy_D, dz_D);
    auto D3 = new Volume<G4Box>("MVPanelD3", MVmat, mother, D3Pos, dx_D, dy_D, dz_D);
    auto D4 = new Volume<G4Box>("MVPanelD4", MVmat, mother, D4Pos, dx_D, dy_D, dz_D);

    
    ShieldingLayers.push_back(A1->GetLogic());
    ShieldingLayers.push_back(A2_log);
    ShieldingLayers.push_back(A3_log);
    ShieldingLayers.push_back(A4->GetLogic());
    ShieldingLayers.push_back(A5->GetLogic());
    ShieldingLayers.push_back(A6_log);
    ShieldingLayers.push_back(A7_log);
    ShieldingLayers.push_back(A8->GetLogic());
    ShieldingLayers.push_back(B1->GetLogic());
    ShieldingLayers.push_back(B2->GetLogic());
    ShieldingLayers.push_back(B3->GetLogic());
    ShieldingLayers.push_back(B4->GetLogic());
    ShieldingLayers.push_back(B5->GetLogic());
    ShieldingLayers.push_back(B6->GetLogic());
    ShieldingLayers.push_back(B7->GetLogic());
    ShieldingLayers.push_back(B8->GetLogic());
    ShieldingLayers.push_back(C1->GetLogic());
    ShieldingLayers.push_back(C2->GetLogic());
    ShieldingLayers.push_back(C3->GetLogic());
    ShieldingLayers.push_back(C4->GetLogic());
    ShieldingLayers.push_back(C5->GetLogic());
    ShieldingLayers.push_back(C6->GetLogic());
    ShieldingLayers.push_back(C7->GetLogic());
    ShieldingLayers.push_back(C8->GetLogic());
    ShieldingLayers.push_back(D1->GetLogic());
    ShieldingLayers.push_back(D2->GetLogic());
    ShieldingLayers.push_back(D3->GetLogic());
    ShieldingLayers.push_back(D4->GetLogic());
    
    AddCaseMV_A("CaseA1", mother, (A2Pos+A3Pos)/2., 1);
    AddCaseMV_A("CaseA2", mother, (A6Pos+A7Pos)/2., 2);
    AddCaseMV_B("CaseB1", mother, B1Pos, 2);
    AddCaseMV_B("CaseB2", mother, B2Pos, 2);
    AddCaseMV_B("CaseB3", mother, B3Pos, 2);
    AddCaseMV_B("CaseB4", mother, B4Pos, 2);
    AddCaseMV_B("CaseB5", mother, B5Pos, 1);
    AddCaseMV_B("CaseB6", mother, B6Pos, 1);
    AddCaseMV_B("CaseB7", mother, B7Pos, 1);
    AddCaseMV_B("CaseB8", mother, B8Pos, 1);
    AddCaseMV_C("CaseC1", mother, C1Pos, 2);
    AddCaseMV_C("CaseC2", mother, C2Pos, 2);
    AddCaseMV_C("CaseC3", mother, C3Pos, 2);
    AddCaseMV_C("CaseC4", mother, C4Pos, 2);
    AddCaseMV_C("CaseC5", mother, C5Pos, 1);
    AddCaseMV_C("CaseC6", mother, C6Pos, 1);
    AddCaseMV_C("CaseC7", mother, C7Pos, 1);
    AddCaseMV_C("CaseC8", mother, C8Pos, 1);
    AddCaseMV_D("CaseD1", mother, D1Pos, 2);
    AddCaseMV_D("CaseD2", mother, D2Pos, 1);
    AddCaseMV_D("CaseD3", mother, D3Pos, 2);
    AddCaseMV_D("CaseD4", mother, D4Pos, 1);
    
    AddNorcan("Norcan", mother, (D2Pos+D3Pos)/2.);

    return;
}

void NUCLEUSLeadShielding::AddCaseMV_A(const G4String &name, G4LogicalVolume* mother, const G4ThreeVector &Pos, G4int type){

    G4RotationMatrix Ro0 = G4RotationMatrix();             // No Rotation
    G4RotationMatrix* RoZ180 = new G4RotationMatrix();     // Z180
    RoZ180->rotateZ(180.*deg);

    G4Transform3D tr3d;

    G4VSolid* box1 = new G4Box("box1",587.0*mm, 448.5*mm, 32.5*mm);
    G4VSolid* box2 = new G4Box("box2",586.0*mm, 436.0*mm, 31.5*mm);
    G4VSolid* tub1 = new G4Tubs("tub1",0.,220.0*mm, 40.*mm, 0*deg, 360*deg);

    G4SubtractionSolid* ext1 = new G4SubtractionSolid("ext1",box1,box2,nullptr,G4ThreeVector(0, +11.5*mm,0));
    G4SubtractionSolid* ext2 = new G4SubtractionSolid("ext2",ext1,tub1,nullptr,G4ThreeVector(0,+448.5*mm,0));

    G4VSolid* tub2 = new G4Tubs("tub2",220.01*mm,221.0*mm, 31.5*mm, 180*deg, 180*deg);
    G4VSolid* box3 = new G4Box("box3", 585.5*mm, 35.0*mm, 6.0*mm);
    G4VSolid* box4 = new G4Box("box4", 575.0*mm, 22.5*mm, 5.0*mm);
    G4VSolid* box5 = new G4Box("box5",  25.0*mm, 90.0*mm, 5.0*mm);

    G4MultiUnion* A_sol = new G4MultiUnion("VHsolid_"+name);
    A_sol->AddNode(*ext2,tr3d = G4Transform3D(Ro0, {0, 0, 0}));
    //A_sol->AddNode(*tub2,tr3d = G4Transform3D(Ro0, {0, 448.5*mm, 0}));
    A_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, {0,-111.5*mm, 25.5*mm}));
    A_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, {0,+178.5*mm, 25.5*mm}));
    A_sol->AddNode(*box4,tr3d = G4Transform3D(Ro0, {0,-204.0*mm,-26.5*mm}));
    A_sol->AddNode(*box5,tr3d = G4Transform3D(Ro0, {+352.0*mm,-316.5*mm,-26.5*mm}));
    A_sol->AddNode(*box5,tr3d = G4Transform3D(Ro0, {        0,-316.5*mm,-26.5*mm}));
    A_sol->AddNode(*box5,tr3d = G4Transform3D(Ro0, {-352.0*mm,-316.5*mm,-26.5*mm}));
    A_sol->Voxelize();

    G4LogicalVolume* A_log = new G4LogicalVolume(A_sol, G4Material::GetMaterial("Aluminium"),"VHlogic_"+name);
    A_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));

    if (type == 1) new G4PVPlacement(     0, Pos+G4ThreeVector(0, -132.5*mm, 6.5*mm), A_log, "VHphysic_"+name, mother, false, 0, overlap_test);
    if (type == 2) new G4PVPlacement(RoZ180, Pos+G4ThreeVector(0, +132.5*mm, 6.5*mm), A_log, "VHphysic_"+name, mother, false, 0, overlap_test);
    
    ShieldingLayers.push_back(A_log);
    return;
}

void NUCLEUSLeadShielding::AddCaseMV_B(const G4String &name, G4LogicalVolume* mother, const G4ThreeVector &Pos, G4int type){

    G4RotationMatrix Ro0 = G4RotationMatrix();             // No Rotation
    G4RotationMatrix* RoZ180 = new G4RotationMatrix();     // Z180
    RoZ180->rotateZ(180.*deg);

    G4Transform3D tr3d;

    G4VSolid* box1 = new G4Box("box1",34.0*mm, 143.5*mm, 731.0*mm);
    G4VSolid* box2 = new G4Box("box2",33.0*mm, 142.5*mm, 717.0*mm);
    G4SubtractionSolid* bext = new G4SubtractionSolid("bext",box1,box2,nullptr,G4ThreeVector(0,0,-8.0*mm));

    G4VSolid* box3 = new G4Box("box3", 6.0*mm, 142.0*mm, 78.5*mm);

    G4MultiUnion* B_sol = new G4MultiUnion("VHsolid_"+name);
    B_sol->AddNode(*bext,tr3d = G4Transform3D(Ro0, {0, 0, 0}));
    B_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, {-27.0*mm, 0, -149.5*mm}));
    B_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, {-27.0*mm, 0, +354.5*mm}));
    B_sol->Voxelize();

    G4LogicalVolume* B_log = new G4LogicalVolume(B_sol, G4Material::GetMaterial("Aluminium"),"VHlogic_"+name);
    B_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));

    if (type == 1) new G4PVPlacement(     0, Pos+G4ThreeVector(-8.0*mm, 0, 135.0*mm), B_log, "VHphysic_"+name, mother, false, 0, overlap_test);
    if (type == 2) new G4PVPlacement(RoZ180, Pos+G4ThreeVector(+8.0*mm, 0, 135.0*mm), B_log, "VHphysic_"+name, mother, false, 0, overlap_test);
    
    ShieldingLayers.push_back(B_log);
    return;
}

void NUCLEUSLeadShielding::AddCaseMV_C(const G4String &name, G4LogicalVolume* mother, const G4ThreeVector &Pos, G4int type){

    G4RotationMatrix Ro0 = G4RotationMatrix();             // No Rotation
    G4RotationMatrix* RoZ180 = new G4RotationMatrix();     // Z180
    RoZ180->rotateZ(180.*deg);

    G4Transform3D tr3d;

    G4VSolid* box1 = new G4Box("box1",908.5*mm, 31.5*mm, 124.0*mm);
    G4VSolid* box2 = new G4Box("box2",886.5*mm, 30.5*mm, 123.0*mm);
    G4SubtractionSolid* bext = new G4SubtractionSolid("bext",box1,box2,nullptr,G4ThreeVector(0,0,0));

    G4VSolid* box3 = new G4Box("box3", 48.0*mm,  5.0*mm, 122.0*mm);

    G4MultiUnion* C_sol = new G4MultiUnion("VHsolid_"+name);
    C_sol->AddNode(*bext,tr3d = G4Transform3D(Ro0, {0, 0, 0}));
    C_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, {-510.0*mm, -25.5*mm, 0}));
    C_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, {        0, -25.5*mm, 0}));
    C_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, {+510.0*mm, -25.5*mm, 0}));
    C_sol->Voxelize();

    G4LogicalVolume* C_log = new G4LogicalVolume(C_sol, G4Material::GetMaterial("Aluminium"),"VHlogic_"+name);
    C_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));

    if (type == 1) new G4PVPlacement(     0, Pos+G4ThreeVector(0, -5.5*mm, 0), C_log, "VHphysic_"+name, mother, false, 0, overlap_test);
    if (type == 2) new G4PVPlacement(RoZ180, Pos+G4ThreeVector(0, +5.5*mm, 0), C_log, "VHphysic_"+name, mother, false, 0, overlap_test);
    
    ShieldingLayers.push_back(C_log);
    return;
}

void NUCLEUSLeadShielding::AddCaseMV_D(const G4String &name, G4LogicalVolume* mother, const G4ThreeVector &Pos, G4int type){

    G4RotationMatrix Ro0 = G4RotationMatrix();             // No Rotation
    G4RotationMatrix* RoZ180 = new G4RotationMatrix();     // Z180
    RoZ180->rotateZ(180.*deg);

    G4Transform3D tr3d;

    G4VSolid* box1 = new G4Box("box1",138.5*mm, 886.5*mm, 26.5*mm);
    G4VSolid* box2 = new G4Box("box2",137.5*mm, 866.5*mm, 25.5*mm);
    G4SubtractionSolid* bext = new G4SubtractionSolid("bext",box1,box2,nullptr,G4ThreeVector(0,0,0));

    G4VSolid* box3 = new G4Box("box3", 124.5*mm, 530.0*mm, 0.5*mm);

    G4MultiUnion* D_sol = new G4MultiUnion("VHsolid_"+name);
    D_sol->AddNode(*bext,tr3d = G4Transform3D(Ro0, {0, 0, 0}));
    D_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, {0, 0, -25.0*mm}));
    D_sol->Voxelize();

    G4LogicalVolume* D_log = new G4LogicalVolume(D_sol, G4Material::GetMaterial("Aluminium"),"VHlogic_"+name);
    D_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));

    if (type == 1) new G4PVPlacement(     0, Pos+G4ThreeVector( -6.5*mm, 0, -0.5*mm), D_log, "VHphysic_"+name, mother, false, 0, overlap_test);
    if (type == 2) new G4PVPlacement(RoZ180, Pos+G4ThreeVector( +6.5*mm, 0, -0.5*mm), D_log, "VHphysic_"+name, mother, false, 0, overlap_test);
    
    ShieldingLayers.push_back(D_log);
    return;
}


void NUCLEUSLeadShielding::AddCaseMV_I(const G4String &name, G4LogicalVolume* mother, const G4ThreeVector &Pos, G4double dw, G4int type){

    G4RotationMatrix Ro0 = G4RotationMatrix();             // No Rotation
    G4RotationMatrix* RoZ090 = new G4RotationMatrix();
    G4RotationMatrix* RoZ180 = new G4RotationMatrix();
    G4RotationMatrix* RoZ270 = new G4RotationMatrix();
    RoZ090->rotateZ( 90.*deg);
    RoZ180->rotateZ(180.*deg);
    RoZ270->rotateZ(270.*deg);

    G4Transform3D tr3d;

    G4VSolid* box1 = new G4Box("box1",31.5*mm, dw+2.5*mm, 481.5*mm);
    G4VSolid* box2 = new G4Box("box2",30.5*mm, dw+1.5*mm, 467.5*mm);
    G4SubtractionSolid* bext = new G4SubtractionSolid("bext",box1,box2,nullptr,G4ThreeVector(0,0,-8.0*mm));

    G4VSolid* box3 = new G4Box("box3", 5.0*mm, dw+0.5*mm, 48.0*mm);

    G4MultiUnion* I_sol = new G4MultiUnion("VHsolid_"+name);
    I_sol->AddNode(*bext,tr3d = G4Transform3D(Ro0, {0, 0, 0}));
    I_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, {-25.5*mm, 0, -190.0*mm}));
    I_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, {-25.5*mm, 0, +106.0*mm}));
    I_sol->Voxelize();

    G4LogicalVolume* I_log = new G4LogicalVolume(I_sol, G4Material::GetMaterial("Aluminium"),"VHlogic_"+name);
    I_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));

    if (type == 1) new G4PVPlacement(     0, Pos+G4ThreeVector(-5.5*mm,      0, 135.0*mm), I_log, "VHphysic_"+name, mother, false, 0, overlap_test);
    if (type == 2) new G4PVPlacement(RoZ270, Pos+G4ThreeVector(      0,-5.5*mm, 135.0*mm), I_log, "VHphysic_"+name, mother, false, 0, overlap_test);
    if (type == 3) new G4PVPlacement(RoZ180, Pos+G4ThreeVector(+5.5*mm,      0, 135.0*mm), I_log, "VHphysic_"+name, mother, false, 0, overlap_test);
    if (type == 4) new G4PVPlacement(RoZ090, Pos+G4ThreeVector(      0,+5.5*mm, 135.0*mm), I_log, "VHphysic_"+name, mother, false, 0, overlap_test);

    ShieldingLayers.push_back(I_log);
    return;
}

void NUCLEUSLeadShielding::AddColdPassiveShield(G4LogicalVolume* mother, const G4ThreeVector &Pos){

    G4double is_outer_diameter  = 297.0*mm;     // Outer diameter of all the components of the inner shield
    G4double is_inner_min_dia   =   7.0*mm;     // Inner diameter of copper disk 2, 3, 4 and outer/middle/inner layers
    G4double is_inner_max_dia   =  45.0*mm;     // Inner diameter of copper disk 1
    
    if(construction_verbose_level > 1) {
        std::cout << "\n   ********************** Cylindrical Cold Passive Shield enabled **********************\n";
        std::cout << "\t- Outer diameter: " << is_outer_diameter/mm << " mm\n";
        std::cout << "\t- Outer layer: " << outLayer.material << " with height " << outLayer.thickness/mm << " mm and " << is_inner_min_dia/mm << " mm inner diameter\n";
        std::cout <<   "   *************************************************************************************\n\n";
    }

    G4ThreeVector ISOuterLayer_Pos    = Pos + G4ThreeVector( 0, 0, GetExternalSizeZ()/2.-outLayer.thickness/2.);
    auto ISOuterLayer  =   new Volume<G4Tubs>("ISOuterLayer",    outLayer.material, mother, ISOuterLayer_Pos,  is_inner_min_dia/2., is_outer_diameter/2., outLayer.thickness/2.,    0*deg, 360*deg);
    ShieldingLayers.push_back(ISOuterLayer->GetLogic());

}

void NUCLEUSLeadShielding::AddNorcan(const G4String &name, G4LogicalVolume* mother, const G4ThreeVector &Pos){

    G4RotationMatrix Ro0 = G4RotationMatrix();      // No Rotation
    G4Transform3D tr3d;

    G4double tck = 5.25*mm;                         // Norcan walls thickness
    G4double eps = 0.05*mm;
    
    G4VSolid* box0a = new G4Box("box0a", 45.0*mm,     869.0*mm-eps, 27.5*mm);
    G4VSolid* box0b = new G4Box("box0b", 45.0*mm-tck, 869.0*mm,     27.5*mm-tck);
    G4SubtractionSolid *box0 = new G4SubtractionSolid("box0",box0a,box0b);
    
    G4VSolid* box1a = new G4Box("box1a", 22.5*mm,     869.0*mm-eps, 27.5*mm);
    G4VSolid* box1b = new G4Box("box1b", 22.5*mm-tck, 869.0*mm,     27.5*mm-tck);
    G4SubtractionSolid *box1 = new G4SubtractionSolid("box1",box1a,box1b);

    G4VSolid* box2a = new G4Box("box2a",198.0*mm,      22.5*mm,     27.5*mm);
    G4VSolid* box2b = new G4Box("box2b",198.0*mm+eps,  22.5*mm-tck, 27.5*mm-tck);
    G4SubtractionSolid *box2 = new G4SubtractionSolid("box2",box2a,box2b);

    G4VSolid* box3a = new G4Box("box3a", 54.0*mm-eps,  45.0*mm,     27.5*mm);
    G4VSolid* box3b = new G4Box("box3b", 54.0*mm,      45.0*mm-tck, 27.5*mm-tck);
    G4SubtractionSolid *box3 = new G4SubtractionSolid("box3",box3a,box3b);

    G4MultiUnion* Norcan_sol = new G4MultiUnion("VHsolid_"+name);
    Norcan_sol->AddNode(*box0,tr3d = G4Transform3D(Ro0, {        0,        0, 0}));
    Norcan_sol->AddNode(*box1,tr3d = G4Transform3D(Ro0, {+175.5*mm,        0, 0}));
    Norcan_sol->AddNode(*box1,tr3d = G4Transform3D(Ro0, {-175.5*mm,        0, 0}));
    Norcan_sol->AddNode(*box2,tr3d = G4Transform3D(Ro0, {        0,+891.5*mm, 0}));
    Norcan_sol->AddNode(*box2,tr3d = G4Transform3D(Ro0, {        0,-891.5*mm, 0}));
    Norcan_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, { +99.0*mm,+491.0*mm, 0}));
    Norcan_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, { -99.0*mm,+491.0*mm, 0}));
    Norcan_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, { +99.0*mm,        0, 0}));
    Norcan_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, { -99.0*mm,        0, 0}));
    Norcan_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, { +99.0*mm,-491.0*mm, 0}));
    Norcan_sol->AddNode(*box3,tr3d = G4Transform3D(Ro0, { -99.0*mm,-491.0*mm, 0}));
    Norcan_sol->Voxelize();

    G4LogicalVolume* Norcan_log = new G4LogicalVolume(Norcan_sol, G4Material::GetMaterial("Aluminium"),"VHlogic_"+name);
    Norcan_log->SetVisAttributes(GLG4Material::GetVisAttribute("Aluminium"));

    new G4PVPlacement(0, Pos+G4ThreeVector(0, 0, -54.5*mm), Norcan_log, "VHphysic_"+name, mother, false, 0, overlap_test);

    ShieldingLayers.push_back(Norcan_log);
    return;
}

void NUCLEUSLeadShielding::AddSupportFrame(G4LogicalVolume* mother, const G4ThreeVector &Pos){
    
    if(construction_verbose_level > 1) std::cout << "\n   ***** Shielding Support Frame construction enabled *****\n";

    // See twiki file: https://twiki.cern.ch/twiki/pub/NUCLEUS/Shielding_Support_Frame_Geometry/Nucleus_Shielding_Support_Frame.pdf
   
    // X: Horizontal axis along longer VNS building side
    // Y: Horizontal axis along shorter VNS building side
    // Z: Vertical axis

    // UPN profiles size
    const double upn_w = 80.0*mm;
    const double upn_t = 45.0*mm;

    // Support Frame Sizes
    const double pxy_r  = 240.0*mm;
    const double pxy_d  =   5.0*mm;
    const double pxy_45 = 227.4*mm;

    const double pxy_gap_x1 = 541.0*mm;
    const double pxy_gap_x2 = 102.0*mm;

    const double pxy_gap_y1 =  48.3*mm;
    const double pxy_gap_y2 = 524.3*mm;
    const double pxy_gap_y3 = 1128.3*mm;

    const double pzx_gap_x1 = 242.5*mm;
    const double pzx_gap_z1 = 230.0*mm;
    const double pzx_gap_z2 = 220.0*mm;
    const double pzx_gap_z3 = 240.0*mm;
    const double pzx_gap_z4 =  10.0*mm;

    const double pzy_gap_y0 =  95.0*mm;
    const double pzy_gap_y1 = 245.0*mm;
    const double pzy_gap_y2 = 170.0*mm;
    const double pzy_gap_y3 =  10.0*mm;

    const double plane_z = 810.0*mm;
    const double plane_t =   6.0*mm;

    const double sf_x = 1037.0*mm;
    const double sf_y = 1037.0*mm;
    const double sf_z = pzx_gap_z1+pzx_gap_z2+pzx_gap_z3+pzx_gap_z4+2.*upn_w+upn_t+plane_t;   // 911.0*mm

    const double eps = 0.05*mm;           // small increment to avoid conflicts of coplanar surfaces

    G4RotationMatrix Ro0 = G4RotationMatrix(),   // No rotation
      RoX270(0,270.0*deg,0),                     // X270
      RoY090(90.0*deg,90.0*deg,270.0*deg),       //  Y90
      RoZ090(90.0*deg,0,0),                      //  Z90
      RoZ180(180.0*deg,0,0),                     // Z180
      RoZ270(270.0*deg,0,0),                     // Z270
      RoYX01(90.0*deg,90.0*deg,90.0*deg),        //  Y90 + X180
      RoZY01(90.0*deg,90.0*deg,0),               //  Z90 +  Y90
      RoZY02(270.0*deg,90.0*deg,0),              // Z270 +  Y90
      RoXY01(0*deg,270.0*deg,90.0*deg),          // X270 +  Y90
      RoXY02(0*deg,90.0*deg,90.0*deg),           //  X90 +  Y90
      RoXZ01(0*deg,270.0*deg,45.0*deg),          // X270 +  Z
      RoXZ02(0*deg,270.0*deg,135.0*deg),         // X270 +  Z
      RoXZ03(0*deg,270.0*deg,225.0*deg),         // X270 +  Z
      RoXZ04(0*deg,270.0*deg,315.0*deg);         // X270 +  Z
    
    G4Transform3D tr3d;

    G4MultiUnion* SSF_sol = new G4MultiUnion("VHsolid_ShieldingSupportFrame");

    // Planes Z vs X
    auto upn1 = UPNBeam80x45("upn1",sf_z-upn_t-plane_t-pzx_gap_z4-eps);
    double px = pzx_gap_x1 + upn_w;
    double py = (sf_y-upn_t)/2.;
    double pz = (pzx_gap_z3-pzx_gap_z1)/2.;
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(Ro0,    {  0, -py,  pz}));
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(Ro0,    {+px, -py,  pz}));
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(Ro0,    {-px, -py,  pz}));
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(RoZ180, {  0, +py,  pz}));
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(RoZ180, {+px, +py,  pz}));
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(RoZ180, {-px, +py,  pz}));

    auto upn2 = UPNBeam80x45("upn2",pzx_gap_x1-eps);
    px = (pzx_gap_x1+upn_w)/2.;
    pz = (pzx_gap_z2+upn_w)/2.;
    SSF_sol->AddNode(*upn2, tr3d = G4Transform3D(RoY090, {+px, -py, +pz}));
    SSF_sol->AddNode(*upn2, tr3d = G4Transform3D(RoY090, {+px, -py, -pz}));
    SSF_sol->AddNode(*upn2, tr3d = G4Transform3D(RoY090, {-px, -py, +pz}));
    SSF_sol->AddNode(*upn2, tr3d = G4Transform3D(RoY090, {-px, -py, -pz}));
    SSF_sol->AddNode(*upn2, tr3d = G4Transform3D(RoYX01, {+px, +py, +pz}));
    SSF_sol->AddNode(*upn2, tr3d = G4Transform3D(RoYX01, {+px, +py, -pz}));
    SSF_sol->AddNode(*upn2, tr3d = G4Transform3D(RoYX01, {-px, +py, +pz}));
    SSF_sol->AddNode(*upn2, tr3d = G4Transform3D(RoYX01, {-px, +py, -pz}));

    auto upn3 = UPNBeam80x45("upn3",(sf_x-2.*pzx_gap_x1-3.*upn_w)/2.-eps);
    px = (sf_x+2.*pzx_gap_x1+3.*upn_w)/4.;
    SSF_sol->AddNode(*upn3, tr3d = G4Transform3D(RoY090, {+px, -py, +pz}));
    SSF_sol->AddNode(*upn3, tr3d = G4Transform3D(RoY090, {+px, -py, -pz}));
    SSF_sol->AddNode(*upn3, tr3d = G4Transform3D(RoY090, {-px, -py, +pz}));
    SSF_sol->AddNode(*upn3, tr3d = G4Transform3D(RoY090, {-px, -py, -pz}));
    SSF_sol->AddNode(*upn3, tr3d = G4Transform3D(RoYX01, {+px, +py, +pz}));
    SSF_sol->AddNode(*upn3, tr3d = G4Transform3D(RoYX01, {+px, +py, -pz}));
    SSF_sol->AddNode(*upn3, tr3d = G4Transform3D(RoYX01, {-px, +py, +pz}));
    SSF_sol->AddNode(*upn3, tr3d = G4Transform3D(RoYX01, {-px, +py, -pz}));

    auto layzx = new G4Box("layzx", sf_x/2.-upn_t-plane_t, plane_t/2., plane_z/2.);
    py = sf_y/2.-upn_t-plane_t/2.-eps;
    pz = -pzx_gap_z2/2.-upn_w-pzx_gap_z1+plane_z/2.;
    SSF_sol->AddNode(*layzx,tr3d = G4Transform3D(Ro0,    {  0, -py,  pz}));
    SSF_sol->AddNode(*layzx,tr3d = G4Transform3D(Ro0,    {  0, +py,  pz}));


    // Planes Z vs Y
    px = (sf_x-upn_t)/2.;
    py = (pzy_gap_y1+upn_w)/2.;
    pz = (pzx_gap_z3-pzx_gap_z1)/2.;
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(RoZ090, {-px, +py,  pz}));
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(RoZ090, {-px, -py,  pz}));
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(RoZ270, {+px, +py,  pz}));
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(RoZ270, {+px, -py,  pz}));
    py += pzy_gap_y2+upn_w;
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(RoZ090, {-px, +py,  pz}));
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(RoZ090, {-px, -py,  pz}));
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(RoZ270, {+px, +py,  pz}));
    SSF_sol->AddNode(*upn1, tr3d = G4Transform3D(RoZ270, {+px, -py,  pz}));

    auto upn4 = UPNBeam80x45("upn4",pzy_gap_y2-eps);
    py = (pzy_gap_y1+pzy_gap_y2)/2.+upn_w;
    pz = (pzx_gap_z2+upn_w)/2.;
    SSF_sol->AddNode(*upn4, tr3d = G4Transform3D(RoZY01, {-px, +py, +pz}));
    SSF_sol->AddNode(*upn4, tr3d = G4Transform3D(RoZY01, {-px, +py, -pz}));
    SSF_sol->AddNode(*upn4, tr3d = G4Transform3D(RoZY01, {-px, -py, +pz}));
    SSF_sol->AddNode(*upn4, tr3d = G4Transform3D(RoZY01, {-px, -py, -pz}));
    SSF_sol->AddNode(*upn4, tr3d = G4Transform3D(RoZY02, {+px, +py, +pz}));
    SSF_sol->AddNode(*upn4, tr3d = G4Transform3D(RoZY02, {+px, +py, -pz}));
    SSF_sol->AddNode(*upn4, tr3d = G4Transform3D(RoZY02, {+px, -py, +pz}));
    SSF_sol->AddNode(*upn4, tr3d = G4Transform3D(RoZY02, {+px, -py, -pz}));

    auto upn5 = UPNBeam80x45("upn5",(pzy_gap_y1-pzy_gap_y0)/2.-eps);
    py = (pzy_gap_y0+pzy_gap_y1)/4.;
    pz = (pzx_gap_z2+upn_w)/2.;
    SSF_sol->AddNode(*upn5, tr3d = G4Transform3D(RoZY02, {-px, +py, +pz}));
    SSF_sol->AddNode(*upn5, tr3d = G4Transform3D(RoZY02, {-px, +py, -pz}));
    SSF_sol->AddNode(*upn5, tr3d = G4Transform3D(RoZY02, {-px, -py, +pz}));
    SSF_sol->AddNode(*upn5, tr3d = G4Transform3D(RoZY02, {-px, -py, -pz}));
    SSF_sol->AddNode(*upn5, tr3d = G4Transform3D(RoZY01, {+px, +py, +pz}));
    SSF_sol->AddNode(*upn5, tr3d = G4Transform3D(RoZY01, {+px, +py, -pz}));
    SSF_sol->AddNode(*upn5, tr3d = G4Transform3D(RoZY01, {+px, -py, +pz}));
    SSF_sol->AddNode(*upn5, tr3d = G4Transform3D(RoZY01, {+px, -py, -pz}));

    auto layzy = new G4Box("layzy", plane_t/2., (pzy_gap_y1+2.*pzy_gap_y2+2.*pzy_gap_y3+4.*upn_w-pzy_gap_y0)/4., plane_z/2.);
    px = sf_x/2.-upn_t-plane_t/2.-eps;
    py = (pzy_gap_y1+2.*pzy_gap_y2+2.*pzy_gap_y3+4.*upn_w+pzy_gap_y0)/4.;
    pz = -pzx_gap_z2/2.-upn_w-pzx_gap_z1+plane_z/2.;
    SSF_sol->AddNode(*layzy,tr3d = G4Transform3D(Ro0,    {-px, +py,  pz}));
    SSF_sol->AddNode(*layzy,tr3d = G4Transform3D(Ro0,    {-px, -py,  pz}));
    SSF_sol->AddNode(*layzy,tr3d = G4Transform3D(Ro0,    {+px, +py,  pz}));
    SSF_sol->AddNode(*layzy,tr3d = G4Transform3D(Ro0,    {+px, -py,  pz}));


    // Planes Y vs X
    auto upn6 = UPNBeam80x45("upn6",(pxy_gap_y3-pxy_gap_y1)/2.);
    px = pxy_gap_x1/2.+pxy_gap_x2+1.5*upn_w;
    py = (pxy_gap_y3+pxy_gap_y1)/4.;
    pz = sf_z-pzx_gap_z2/2.-upn_w-pzx_gap_z1-upn_t/2.;
    SSF_sol->AddNode(*upn6, tr3d = G4Transform3D(RoX270, {+px, +py,  pz}));
    SSF_sol->AddNode(*upn6, tr3d = G4Transform3D(RoX270, {+px, -py,  pz}));
    SSF_sol->AddNode(*upn6, tr3d = G4Transform3D(RoX270, {-px, +py,  pz}));
    SSF_sol->AddNode(*upn6, tr3d = G4Transform3D(RoX270, {-px, -py,  pz}));

    auto upn7 = UPNBeam80x45("upn7",(pxy_gap_y2-pxy_gap_y1)/2.-eps);
    px = (pxy_gap_x1+upn_w)/2.;
    py = (pxy_gap_y2+pxy_gap_y1)/4.;
    SSF_sol->AddNode(*upn7, tr3d = G4Transform3D(RoX270, {+px, +py,  pz}));
    SSF_sol->AddNode(*upn7, tr3d = G4Transform3D(RoX270, {+px, -py,  pz}));
    SSF_sol->AddNode(*upn7, tr3d = G4Transform3D(RoX270, {-px, +py,  pz}));
    SSF_sol->AddNode(*upn7, tr3d = G4Transform3D(RoX270, {-px, -py,  pz}));

    auto upn8 = UPNBeam80x45("upn8",pxy_gap_x2-eps);
    py = pxy_gap_y2/2.+upn_w+pxy_gap_x2/2.;
    SSF_sol->AddNode(*upn8, tr3d = G4Transform3D(RoX270, {+px, +py,  pz}));
    SSF_sol->AddNode(*upn8, tr3d = G4Transform3D(RoX270, {+px, -py,  pz}));
    SSF_sol->AddNode(*upn8, tr3d = G4Transform3D(RoX270, {-px, +py,  pz}));
    SSF_sol->AddNode(*upn8, tr3d = G4Transform3D(RoX270, {-px, -py,  pz}));

    auto upn9 = UPNBeam80x45("upn9", pxy_gap_x1+2.*pxy_gap_x2+2.*upn_w-eps);
    px = 0;
    py = (pxy_gap_y2+upn_w)/2.;
    SSF_sol->AddNode(*upn9, tr3d = G4Transform3D(RoXY01, { px, +py,  pz}));
    SSF_sol->AddNode(*upn9, tr3d = G4Transform3D(RoXY01, { px, -py,  pz}));
    py += pxy_gap_x2+upn_w;
    SSF_sol->AddNode(*upn9, tr3d = G4Transform3D(RoXY01, { px, +py,  pz}));
    SSF_sol->AddNode(*upn9, tr3d = G4Transform3D(RoXY01, { px, -py,  pz}));

    px = (pxy_gap_x1+pxy_gap_x2)/2.+upn_w;
    py = (pxy_gap_y1+upn_w)/2.;
    SSF_sol->AddNode(*upn8, tr3d = G4Transform3D(RoXY01, {+px, +py,  pz}));
    SSF_sol->AddNode(*upn8, tr3d = G4Transform3D(RoXY01, {+px, -py,  pz}));
    SSF_sol->AddNode(*upn8, tr3d = G4Transform3D(RoXY01, {-px, +py,  pz}));
    SSF_sol->AddNode(*upn8, tr3d = G4Transform3D(RoXY01, {-px, -py,  pz}));

    auto layyx_1 = new G4Box("layyx_1", pxy_gap_x1/2.+pxy_gap_x2+2.*upn_w+pxy_d, (pxy_gap_y2-pxy_gap_y1)/4.+upn_w+pxy_gap_x2/2.+pxy_d, plane_t/2.);
    auto layyx_2 = new G4Tubs("layyx_2", 0., pxy_r, plane_t, 0*deg, 360*deg);
    auto layyx = new G4SubtractionSolid("layyx",layyx_1,layyx_2,nullptr,G4ThreeVector(0,(pxy_gap_y2+pxy_gap_y1)/4.+upn_w+pxy_gap_x2/2.,0));
    px = 0;
    py = (pxy_gap_y2+pxy_gap_y1)/4.+upn_w+pxy_gap_x2/2.;
    pz = sf_z-pzx_gap_z2/2.-upn_w-pzx_gap_z1-upn_t-plane_t/2.;
    SSF_sol->AddNode(*layyx,tr3d = G4Transform3D(RoZ180, { px, +py,  pz}));
    SSF_sol->AddNode(*layyx,tr3d = G4Transform3D(Ro0,    { px, -py,  pz}));

    auto upntr_1 = new G4Trd("upntr_1", (pxy_45-eps)/2., (pxy_45-eps)/2.-upn_w, upn_t/2., upn_t/2., upn_w/2.);
    auto upntr_2 = new G4Box("upntr_2", pxy_45, upn_t/2., 32.0*mm);
    auto upntr = new G4SubtractionSolid("upntr",upntr_1,upntr_2,nullptr,G4ThreeVector(0,+5.0*mm,0));
    px = pxy_gap_x1/2.-(pxy_45-upn_w)/sqrt(8.);
    py = pxy_gap_y2/2.-(pxy_45-upn_w)/sqrt(8.);
    pz = sf_z-pzx_gap_z2/2.-upn_w-pzx_gap_z1-upn_t/2.;
    SSF_sol->AddNode(*upntr,tr3d = G4Transform3D(RoXZ01, {-px, -py,  pz}));
    SSF_sol->AddNode(*upntr,tr3d = G4Transform3D(RoXZ02, {-px, +py,  pz}));
    SSF_sol->AddNode(*upntr,tr3d = G4Transform3D(RoXZ03, {+px, +py,  pz}));
    SSF_sol->AddNode(*upntr,tr3d = G4Transform3D(RoXZ04, {+px, -py,  pz}));


    // Charriot
    const double upnc_w = 100.0*mm;
    const double upnc_t =  55.0*mm;
    
    const double ch_gap_x1 = 200.0*mm;
    const double ch_gap_x2 = 267.5*mm;
    const double ch_gap_x3 =  45.0*mm;

    const double ch_gap_y1 =  10.0*mm;
    const double ch_gap_y2 =  90.0*mm;
    const double ch_gap_y3 =  80.0*mm;
    const double ch_gap_y4 =  45.0*mm;

    const double ch_plan_h =  10.0*mm;

    const double ch_x = ch_gap_x1 + 2.*(ch_gap_x2+ch_gap_x3) + 4.*upnc_t;
    const double ch_y = ch_gap_y1 + 2.*ch_gap_y2 + 4.*ch_gap_y3 + 4.*ch_gap_y4 + 8.*upnc_t;

    auto plane = new G4Box("plane",ch_x/2.,(ch_y-ch_gap_y1)/4., (ch_plan_h-eps)/2.);
    py = (ch_y+ch_gap_y1)/4.;
    pz = -pzx_gap_z1-pzx_gap_z2/2.-upn_w-ch_plan_h/2.;
    SSF_sol->AddNode(*plane,tr3d = G4Transform3D(Ro0,    {  0, +py,  pz}));
    SSF_sol->AddNode(*plane,tr3d = G4Transform3D(Ro0,    {  0, -py,  pz}));

    auto upnc1 = UPNBeam100x55("upnc1",(ch_y-ch_gap_y1)/2.);
    px = (ch_x-upnc_t)/2.-ch_gap_x3;
    py = (ch_y+ch_gap_y1)/4.;
    pz = -pzx_gap_z1-pzx_gap_z2/2.-upn_w-ch_plan_h-upnc_w/2.;
    SSF_sol->AddNode(*upnc1,tr3d = G4Transform3D(RoZY02, {+px, +py,  pz}));
    SSF_sol->AddNode(*upnc1,tr3d = G4Transform3D(RoZY02, {+px, -py,  pz}));
    SSF_sol->AddNode(*upnc1,tr3d = G4Transform3D(RoZY01, {-px, +py,  pz}));
    SSF_sol->AddNode(*upnc1,tr3d = G4Transform3D(RoZY01, {-px, -py,  pz}));

    auto upnc2 = UPNBeam100x55("upnc2",ch_x-2.*(ch_gap_x3+upnc_t)-eps);
    py = ch_gap_y4+(ch_gap_y1+upnc_t)/2.;
    SSF_sol->AddNode(*upnc2,tr3d = G4Transform3D(RoYX01, {  0, +py,  pz}));
    SSF_sol->AddNode(*upnc2,tr3d = G4Transform3D(RoY090, {  0, -py,  pz}));
    py += ch_gap_y3+upnc_t;
    SSF_sol->AddNode(*upnc2,tr3d = G4Transform3D(RoYX01, {  0, +py,  pz}));
    SSF_sol->AddNode(*upnc2,tr3d = G4Transform3D(RoY090, {  0, -py,  pz}));
    py += ch_gap_y2+upnc_t;
    SSF_sol->AddNode(*upnc2,tr3d = G4Transform3D(RoY090, {  0, +py,  pz}));
    SSF_sol->AddNode(*upnc2,tr3d = G4Transform3D(RoYX01, {  0, -py,  pz}));
    py += ch_gap_y3+upnc_t;
    SSF_sol->AddNode(*upnc2,tr3d = G4Transform3D(RoY090, {  0, +py,  pz}));
    SSF_sol->AddNode(*upnc2,tr3d = G4Transform3D(RoYX01, {  0, -py,  pz}));

    auto upnc3 = UPNBeam100x55("upnc3",ch_gap_y2-eps);
    auto upnc4 = UPNBeam100x55("upnc4",ch_gap_y3-eps);
    px = (ch_gap_x1+upnc_t)/2.;
    py = ch_gap_y4+(ch_gap_y1+ch_gap_y3)/2.+upnc_t;
    SSF_sol->AddNode(*upnc4,tr3d = G4Transform3D(RoZY01, {+px, +py,  pz}));
    SSF_sol->AddNode(*upnc4,tr3d = G4Transform3D(RoZY01, {+px, -py,  pz}));
    SSF_sol->AddNode(*upnc4,tr3d = G4Transform3D(RoZY02, {-px, +py,  pz}));
    SSF_sol->AddNode(*upnc4,tr3d = G4Transform3D(RoZY02, {-px, -py,  pz}));
    py += upnc_t+(ch_gap_y2+ch_gap_y3)/2.;
    SSF_sol->AddNode(*upnc3,tr3d = G4Transform3D(RoZY01, {+px, +py,  pz}));
    SSF_sol->AddNode(*upnc3,tr3d = G4Transform3D(RoZY01, {+px, -py,  pz}));
    SSF_sol->AddNode(*upnc3,tr3d = G4Transform3D(RoZY02, {-px, +py,  pz}));
    SSF_sol->AddNode(*upnc3,tr3d = G4Transform3D(RoZY02, {-px, -py,  pz}));
    py += upnc_t+(ch_gap_y2+ch_gap_y3)/2.;
    SSF_sol->AddNode(*upnc4,tr3d = G4Transform3D(RoZY01, {+px, +py,  pz}));
    SSF_sol->AddNode(*upnc4,tr3d = G4Transform3D(RoZY01, {+px, -py,  pz}));
    SSF_sol->AddNode(*upnc4,tr3d = G4Transform3D(RoZY02, {-px, +py,  pz}));
    SSF_sol->AddNode(*upnc4,tr3d = G4Transform3D(RoZY02, {-px, -py,  pz}));

    auto wheel = new G4Tubs("wheel",0.,37.5*mm, 22.5*mm, 0*deg, 360*deg);
    px = 513.5*mm;
    py = ch_y/2.-45.*mm;
    pz -= 8.0*mm;
    SSF_sol->AddNode(*wheel,tr3d = G4Transform3D(RoY090, {+px, +py,  pz}));
    SSF_sol->AddNode(*wheel,tr3d = G4Transform3D(RoY090, {+px, -py,  pz}));
    SSF_sol->AddNode(*wheel,tr3d = G4Transform3D(RoY090, {-px, +py,  pz}));
    SSF_sol->AddNode(*wheel,tr3d = G4Transform3D(RoY090, {-px, -py,  pz}));
    py = ch_gap_y1/2.+45.*mm;
    SSF_sol->AddNode(*wheel,tr3d = G4Transform3D(RoY090, {+px, +py,  pz}));
    SSF_sol->AddNode(*wheel,tr3d = G4Transform3D(RoY090, {+px, -py,  pz}));
    SSF_sol->AddNode(*wheel,tr3d = G4Transform3D(RoY090, {-px, +py,  pz}));
    SSF_sol->AddNode(*wheel,tr3d = G4Transform3D(RoY090, {-px, -py,  pz}));


    // Rails
    const double rail_length = 3600.0*mm;
    px =  500.5*mm;
    py = -Pos.y();
    if (ParamBase::GetDataBase().Get<int>("experimental_site") == 1 ) py =  125.0*mm-Pos.y();
    pz = -612.6*mm;
    auto rail = Rail("rail", rail_length);
    SSF_sol->AddNode(*rail, tr3d = G4Transform3D(RoZ180, {+px,  py,  pz}));
    SSF_sol->AddNode(*rail, tr3d = G4Transform3D(Ro0,    {-px,  py,  pz}));

    auto sleep = new G4Box("sleep", 695.*mm, 150.*mm, 7.5*mm);
    pz = -697.1*mm;
    SSF_sol->AddNode(*sleep,tr3d = G4Transform3D(Ro0, { 0, py+1650.*mm,  pz}));
    SSF_sol->AddNode(*sleep,tr3d = G4Transform3D(Ro0, { 0, py +900.*mm,  pz}));
    SSF_sol->AddNode(*sleep,tr3d = G4Transform3D(Ro0, { 0, py +300.*mm,  pz}));
    SSF_sol->AddNode(*sleep,tr3d = G4Transform3D(Ro0, { 0, py -300.*mm,  pz}));
    SSF_sol->AddNode(*sleep,tr3d = G4Transform3D(Ro0, { 0, py -900.*mm,  pz}));
    SSF_sol->AddNode(*sleep,tr3d = G4Transform3D(Ro0, { 0, py-1650.*mm,  pz}));

    auto bars = new G4Box("bars", 12.5*mm, 1050.0*mm, 12.5*mm);
    pz = -677.1*mm;
    SSF_sol->AddNode(*bars, tr3d = G4Transform3D(Ro0, {-399.5*mm,  py,  pz}));
    SSF_sol->AddNode(*bars, tr3d = G4Transform3D(Ro0, {-239.5*mm,  py,  pz}));
    SSF_sol->AddNode(*bars, tr3d = G4Transform3D(Ro0, {+239.5*mm,  py,  pz}));
    SSF_sol->AddNode(*bars, tr3d = G4Transform3D(Ro0, {+399.5*mm,  py,  pz}));

    auto upnr = UPNBeam80x45("upnr",860.0*mm);
    SSF_sol->AddNode(*upnr, tr3d = G4Transform3D(RoXY02, { 0, +975.0*mm, -548.4*mm}));
    SSF_sol->AddNode(*upnr, tr3d = G4Transform3D(RoXY02, { 0, -975.0*mm, -548.4*mm}));

    SSF_sol->Voxelize();

    G4LogicalVolume* SSF_log = new G4LogicalVolume(SSF_sol, G4Material::GetMaterial("Steel"),"VHlogic_ShieldingSupportFrame");
    new G4PVPlacement(0, Pos+G4ThreeVector(0,0,-10.*mm), SSF_log, "VHphysic_ShieldingSupportFrame", mother, false, 0, overlap_test);
    SSF_log->SetVisAttributes(GLG4Material::GetVisAttribute("Steel"));

    ShieldingLayers.push_back(SSF_log);
    return;
}

G4VSolid* UPNBeam80x45(const G4String &name, G4double height){
    G4VSolid* box1 = new G4Box("box1",40.0*mm, 22.5*mm, height/2);
    G4VSolid* box2 = new G4Box("box2",32.0*mm, 22.5*mm, height);
    G4SubtractionSolid* sub = new G4SubtractionSolid(name,box1,box2,nullptr,G4ThreeVector(0,+5.0*mm,0));
    return sub;
}

G4VSolid* UPNBeam100x55(const G4String &name, G4double height){
    G4VSolid* box1 = new G4Box("box1",50.0*mm, 27.5*mm, height/2);
    G4VSolid* box2 = new G4Box("box2",42.5*mm, 27.5*mm, height);
    G4SubtractionSolid* sub = new G4SubtractionSolid(name,box1,box2,nullptr,G4ThreeVector(0,+4.5*mm,0));
    return sub;
}

G4VSolid* Rail(const G4String &name, G4double length){
    G4VSolid* boxv = new G4Box("boxv", 13./2.*mm, length/2., 75.0*mm);
    G4VSolid* boxt = new G4Box("boxt",104./2.*mm, length/2., 19./2.*mm);
    G4VSolid* boxb = new G4Box("boxb",146./2.*mm, length/2., 19./2.*mm);
    G4UnionSolid* u1 = new G4UnionSolid("u1",boxv,boxt,nullptr,{16.0*mm, 0, +67.5*mm});
    G4UnionSolid* u2 = new G4UnionSolid(name,  u1,boxb,nullptr,{      0, 0, -67.5*mm});
    return u2;
}
