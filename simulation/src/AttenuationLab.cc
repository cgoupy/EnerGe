#include "AttenuationLab.hh"
#include <GLG4Material.hh>

#include <memory>
#include <G4SystemOfUnits.hh>
#include <G4VisAttributes.hh>

namespace WorldBuilder{

  AttenuationLab::AttenuationLab(double world_size_, Slab slab_):
  world_half_size(world_size_/2),slab(std::move(slab_)),
  overlap_test(ParamBase::GetDataBase().Get<bool>("overlap_test")),
  construction_verbose_level(ParamBase::GetDataBase().Get<int>("construction_verboselevel")){

  }

  G4VPhysicalVolume* AttenuationLab::BuildWorld() const{
    
    auto world = new Volume<G4Box>("World", "Vac", nullptr, {0,0,0}, world_half_size, world_half_size, world_half_size);
    new Volume<G4Box>("Slab", slab.material, world->GetLogic(), {0,0,0}, world_half_size, world_half_size, slab.thickness/2);
    new Volume<G4Box>("Lab", "Vac", world->GetLogic(), {0,0,-world_half_size/2-slab.thickness/4}, world_half_size, world_half_size, world_half_size/2 - slab.thickness/4);
    
    return world->GetPhysic();
    
  }
  
}
