#include "ExperimentalSite.hh"
#include "COVPrototype.hh"
#include "CEAportableHPGe.hh"
#include "OrsayCryostat.hh"
#include "Shielding.hh"

#include <DCVolumeHandler.hh>
#include <G4Box.hh>
#include <utility>
#include <G4ThreeVector.hh>

#include <GLG4Material.hh>

#include <Parameters.hh>

#include <G4PhysicalConstants.hh>
#include <G4Colour.hh>
#include <G4VisAttributes.hh>
#include <G4UnionSolid.hh>
#include <G4IntersectionSolid.hh>
#include <G4SubtractionSolid.hh>
#include <G4Orb.hh>
#include <G4Tubs.hh>
#include <G4VisAttributes.hh>

#include <iostream>

namespace WorldBuilder{

ExperimentalSite::ExperimentalSite(): overlap_test(ParamBase::GetDataBase().Get<bool>("overlap_test")),
construction_verbose_level(ParamBase::GetDataBase().Get<int>("construction_verboselevel")){
}

//Detector
void ExperimentalSite::AddDetector(G4LogicalVolume *mother){
    switch (ParamBase::GetDataBase().Get<int>("detector_choice")) {
        case 0:
            break;
            
        case 1: {
            if(construction_verbose_level > 1) std::cout << "\n---- Constructing COV Prototype...\n";
            auto COVPrototype_ = new COVPrototype();
            COVPrototype_->Build(mother, DetPos);
            break;
        }
            
        case 2: {
            if(construction_verbose_level > 1) std::cout << "\n---- Constructing CEA portable HPGe...\n";
            auto CEAportableHPGe_ = new CEAportableHPGe();
            CEAportableHPGe_->Build(mother, DetPos);
            break;
        }
            
        default:
            break;
    }
}

//Shielding
void ExperimentalSite::AddShielding(G4LogicalVolume*  mother){
    switch (ParamBase::GetDataBase().Get<int>("add_shielding")) {
        case 0: {
            break;
        }
            
        case 1: {
            if(construction_verbose_level > 1) std::cout << "\n---- Constructing Orsay lead shielding v1...\n";
            auto LeadShielding = new OrsayLeadShielding(60.5*cm, 60.5*cm, 61.5*cm, 10.*cm, 40.5*cm, 40.5*cm, 51.5*cm);
            G4ThreeVector Pos=ShieldingPos+G4ThreeVector(0.,0., 52.5*mm-LeadShielding->GetExternalSizeZ()/2.);
            LeadShielding->ConstructNew(mother, Pos);
            break;
        }
            
        case 2: {
            if(construction_verbose_level > 1) std::cout << "\n---- Constructing Orsay lead shielding v2...\n";
            auto LeadShielding = new OrsayLeadShielding(66.*cm, 66.*cm, 62*cm, 5.*cm, 56*cm, 51*cm, 52*cm);
            G4ThreeVector Pos=ShieldingPos+G4ThreeVector(0.,0., 52.5*mm-LeadShielding->GetExternalSizeZ()/2.);
            LeadShielding->ConstructOld(mother, Pos);
            break;
        }
            
        case 3: { //NUCLEUS Lead shielding

            if(construction_verbose_level > 1) std::cout << "\n---- Constructing NUCLEUS shielding...\n";
            
            G4double ShieldingInnerSize = 43*CLHEP::cm;
            G4double ShieldingInnerSize_z = 35*CLHEP::cm;

            //Out layer parameters
            G4double out_layer_InnerSize = ShieldingInnerSize + 0.40*CLHEP::m;
            G4double out_layer_InnerSize_z = ShieldingInnerSize_z + 0.40*CLHEP::m;
            G4double out_layer_thickness = 0.05*CLHEP::m;
            std::string out_layer_material = "Lead";
            Layer out_layer{out_layer_thickness,out_layer_InnerSize,out_layer_InnerSize,out_layer_InnerSize_z,out_layer_material};

            auto shielding_ = new NUCLEUSLeadShielding(out_layer);
                
            //Defining position of shielding
            G4ThreeVector PosShielding = G4ThreeVector(0.,0.,0.);
            if (ParamBase::GetDataBase().Get<int>("detector_choice")==2){
                PosShielding += G4ThreeVector(-20*cm,0.,20*cm);
            }
            shielding_->Construct(mother,PosShielding);
                                
            break;
        }
            
        default: {
            break;
        }
    }
}


/// ORSAY LAB///
OrsayLab::OrsayLab() {
    worldsize = 6*m;
}

G4VPhysicalVolume* OrsayLab::BuildWorld(){
    
    if(construction_verbose_level) std::cout << " +++++ Constructing experiment at Orsay Laboratory +++++\n";
    
    auto world = new Volume<G4Box>("World", "Vac", nullptr, {0,0,0}, worldsize/2, worldsize/2, worldsize/2);
    
    auto cryostatInterior = AddCryostat(world->GetLogic());
    AddShielding(world->GetLogic());
    AddDetector(cryostatInterior);
    
    if(construction_verbose_level) std::cout << " +++ End of experiment construction at Orsay Laboratory +++\n";
    
    return world->GetPhysic();
}

//OrsayCryostat
G4LogicalVolume* OrsayLab::AddCryostat(G4LogicalVolume*  mother){
    
    G4LogicalVolume* CryostatInterior = nullptr;
    
    switch (ParamBase::GetDataBase().Get<int>("add_cryostat")) {
        case 0: {
            CryostatInterior = mother;
            DetPos=G4ThreeVector(0.,0., 0.);
            ShieldingPos=G4ThreeVector(0.,0., 0.);
            break;
        }
            
        case 1: {
            if(construction_verbose_level > 1) std::cout << "\n---- Constructing cryostat...\n";
            auto cryostat = new OrsayCryostat();
            G4ThreeVector Pos=G4ThreeVector(0.,0., 0.);
            if (ParamBase::GetDataBase().Get<int>("detector_choice")==1){
                Pos=G4ThreeVector(0.,0., 47.5*mm); //place the 20mK vessel in order to center the COV prototype in {0.,0.,0.}
                DetPos=G4ThreeVector(0.,0., cryostat->GetCryoInteriorHeight()-47.5*mm); //place the COV attached to the 20mK vessel
                ShieldingPos=G4ThreeVector(0.,0., 47.5*mm);
            }
            
            if (ParamBase::GetDataBase().Get<int>("add_cryostat_rack")==1){
            cryostat->AddCryostatRack(mother, Pos);
            }
            
            CryostatInterior = cryostat->ConstructCryostat(mother, Pos);
            break;
        }
            
        default : {
            CryostatInterior = mother;
            DetPos=G4ThreeVector(0.,0., 0.);
            ShieldingPos=G4ThreeVector(0.,0., 0.);
        }
    }
    
    return CryostatInterior;
}


/// VERY NEAR SITE AT CHOOZ///
VeryNearSite::VeryNearSite(){
    worldsize = 20*m;
    
}

G4VPhysicalVolume* VeryNearSite::BuildWorld(){
    
    if(construction_verbose_level) std::cout << " +++++ Constructing CEvNS experiment at Very Near Site (Chooz) +++++\n";
    
    auto world = new Volume<G4Box>("World", "Vac", nullptr, {0,0,0}, worldsize, worldsize, worldsize);
    auto roomCentre = AddBuilding(world->GetLogic());
    DetPos = G4ThreeVector(1.5*m, -1.*m, -VNS.height/2.);
    AddDetector(roomCentre);
    std::cout<<"Detector at position in the VNS room : "<<DetPos<<std::endl;
    
    if(construction_verbose_level) std::cout << " +++ End of CEvNS experiment construction at Very Near Site +++\n";
    
    return world->GetPhysic();
}

G4LogicalVolume* VeryNearSite::AddBuilding(G4LogicalVolume* logicWorld){
    
    if(construction_verbose_level > 1) std::cout << "\n---- Constructing Very Near Site floor...\n";
    
    G4String bw_mat = "Concrete";          // Basement bearing walls material
    
    BuildingCentre = {0, 0, -worldsize + building.rock_thickness + (VNS.height + 2*building.VNS_ceiling_thickness)/2.};
    
    // Ground
    new Volume<G4Box>("Rock","ChoozRock", logicWorld, {0,0,-worldsize + building.rock_thickness/2. + VNS.height/2.+building.VNS_ceiling_thickness},
                      worldsize, worldsize, building.rock_thickness/2. + VNS.height/2. + building.VNS_ceiling_thickness);
    
    // Underground floor
    G4VSolid* basement_out = BuildingShape("VHsolid_Basement_out", building.width_min_basement, VNS.height/2. + building.VNS_ceiling_thickness, VNS.height/2. + building.VNS_ceiling_thickness, 0);
    G4LogicalVolume* basement_outLV = new G4LogicalVolume(basement_out, G4Material::GetMaterial(bw_mat), "VHlogic_Basement_out");
    new G4PVPlacement(0, {0,0,building.rock_thickness/2.}, basement_outLV, "VHphysic_Basement_out", DCVVolumeHandler::GetDCVolumeHandler("Rock")->GetLogic(), false, 0, overlap_test);
    basement_outLV->SetVisAttributes(G4Colour::Red());
    
    G4VSolid* basement_in = BuildingShape("VHsolid_Basement_in_", building.width_min_basement, VNS.height/2., VNS.height/2., building.wall_thickness);
    
    // VNS room
    double room_displacement_x = (-building.length_min/2.-(VNS.length-building.wall_thickness+2*7.5*cm)/2.);
    double room_displacement_y = +(building.width_min_basement/2.-(VNS.width+building.wall_thickness+2*7.5*cm)/2.);
    
    VNSCentre = {room_displacement_x, room_displacement_y, -worldsize + building.rock_thickness + (VNS.height + building.VNS_ceiling_thickness)/2.};

    //---- VNS Walls
    G4Box* box10b = new G4Box("box10b", (VNS.length+building.wall_thickness+15*cm)/2., (VNS.width+building.wall_thickness+15*cm)/2., (VNS.height + 2*building.VNS_ceiling_thickness)/2.);
    G4Box* box11b = new G4Box("box11b", 115*cm-2*7.5*cm, 75*cm, (VNS.height + 2*building.VNS_ceiling_thickness)/2.+10*cm);
    G4SubtractionSolid* VNSroom_out = new G4SubtractionSolid("VHsolid_VNS_out",box10b,box11b,nullptr,G4ThreeVector(-VNS.length/2.+175*cm,-(VNS.width+building.wall_thickness+15*cm)/2.,0));
    G4LogicalVolume* VNSroom_outLV = new G4LogicalVolume(VNSroom_out, G4Material::GetMaterial("Concrete"),"VHlogic_VNS_out");
    new G4PVPlacement(0, {room_displacement_x,room_displacement_y,0}, VNSroom_outLV, "VHphysic_VNS_out", basement_outLV, false, 0, overlap_test);
    VNSroom_outLV->SetVisAttributes(G4Colour::Blue());
    
    G4SubtractionSolid* basement_in_noVNS = new G4SubtractionSolid("VHsolid_basement_in",basement_in,VNSroom_out,nullptr, {room_displacement_x,room_displacement_y,0});
    G4LogicalVolume* basement_in_noVNS_LV = new G4LogicalVolume(basement_in_noVNS, G4Material::GetMaterial("Air"),"VHlogic_basement_in");
    new G4PVPlacement(0, {0.,0.,0.}, basement_in_noVNS_LV, "VHphysic_basement_in", basement_outLV, false, 0, overlap_test);
    
    G4Box* box10 = new G4Box("box10", VNS.length/2., VNS.width/2., VNS.height/2.);
    G4Box* box11 = new G4Box("box11", 115*cm, 75*cm, VNS.height/2.+10*cm);
    G4Box* box12 = new G4Box("box12",  75*cm, 25*cm, 200*cm);
    G4Box* door = new G4Box("door", 50*cm, 7.5*cm, VNS.height/2.-20*cm);
    G4SubtractionSolid* s1 = new G4SubtractionSolid("s1",box10,box11,nullptr,G4ThreeVector(-VNS.length/2.+175*cm+building.wall_thickness/2.-7.5*cm,-VNS.width/2.,0));
    G4SubtractionSolid* s2 = new G4SubtractionSolid("s2",s1,box12,nullptr,G4ThreeVector(VNS.length/2.-75*cm,-VNS.width/2.,0));
    G4UnionSolid* VNSroom_in = new G4UnionSolid("VHsolid_VNS_in",s2,door,nullptr,G4ThreeVector(-VNS.length/2.+175*cm+building.wall_thickness/2.-15/2.*cm+50*cm,-VNS.width/2.+75*cm-7.5*cm,-20*cm));
    G4LogicalVolume* VNSroom_inLV = new G4LogicalVolume(VNSroom_in, G4Material::GetMaterial("Air"),"VHlogic_VNS_in");
    new G4PVPlacement(0, {-building.wall_thickness/2.+15/2*cm,-building.wall_thickness/2.+7.5*cm,0}, VNSroom_inLV, "VHphysic_VNS_in", VNSroom_outLV, false, 0, overlap_test);
    
    // Basement partition walls - version using MultiUnion to make a single volume
    double bxmin = building.length_min/2. - building.wall_thickness;     // Building right wall 1 x
    double bxmax = building.length_max/2. - building.wall_thickness;     // Building right wall 2 x
    double bymin = building.width_min_basement/2.  - building.wall_thickness;     // Building top wall 1 y
    double bymax = building.width_max/2.  - building.wall_thickness;     // Building top wall 2 y
    
    G4Transform3D tr3d;
    G4RotationMatrix ro0 = G4RotationMatrix();                           // No rotation
    
//    G4Box* b01 = new G4Box("b01", 7.5*cm, VNS.width/2., VNS.height/2.);
//    G4Box* b02 = new G4Box("b02", 10*cm, VNS.width/2.+7.5*cm, VNS.height/2.);
//    G4Box* b03 = new G4Box("b03", 75*cm, 20*cm, VNS.height/2.);
//    G4Box* b04 = new G4Box("b04", 385*cm-VNS.length/2., 7.5*cm, VNS.height/2.);
//    G4Box* b05 = new G4Box("b05", VNS.length/2.-215*cm, 7.5*cm, VNS.height/2.);
//    G4Box* b06 = new G4Box("b06", 5*cm, 37.5*cm, VNS.height/2.);
//    G4Box* b07 = new G4Box("b07", 5*cm, 37.5*cm, VNS.height/2.);
//    G4Box* b08 = new G4Box("b08", 60*cm, 5*cm, VNS.height/2.);
//    G4Box* b09 = new G4Box("b09", 45*cm, 5*cm, VNS.height/2.-105*cm);
    G4Box* b10 = new G4Box("b10", 30*cm, 20*cm, VNS.height/2.);
    G4Box* b11 = new G4Box("b11", 75*cm, 7.5*cm, VNS.height/2.-105*cm);
    G4Box* b12 = new G4Box("b12", (bxmax-bxmin-930*cm)/2., 7.5*cm, VNS.height/2.);
    G4Box* b13 = new G4Box("b13", 7.5*cm, 45*cm, VNS.height/2.-105*cm);
    G4Box* b14 = new G4Box("b14", 7.5*cm, 82.5*cm, VNS.height/2.);
    G4Box* b15 = new G4Box("b15", (bxmax-bxmin-1040*cm)/2., 5*cm, VNS.height/2.);
    G4Box* b16 = new G4Box("b16", 55*cm, 7.5*cm, VNS.height/2.);
    G4Box* b17 = new G4Box("b17", 80*cm, 7.5*cm, VNS.height/2.-105*cm);
    G4Box* b18 = new G4Box("b18", 20*cm, 20*cm, VNS.height/2.);
    G4Box* b19 = new G4Box("b19", 5*cm, (2.*bymin-VNS.width-205*cm)/2., VNS.height/2.);
    G4Box* b20 = new G4Box("b20", 5*cm, (2.*bymin-VNS.width-180*cm)/2., VNS.height/2.);
    G4Box* b21 = new G4Box("b21", 10*cm, (2.*bymin-VNS.width-165*cm)/2., VNS.height/2.);
    G4Box* b22 = new G4Box("b22", 45*cm, 7.5*cm, VNS.height/2.-105*cm);
    G4Box* b23 = new G4Box("b23", 235*cm, 7.5*cm, VNS.height/2.);
    G4Box* b24 = new G4Box("b24", 20*cm, 20*cm, VNS.height/2.);
    G4Box* b25 = new G4Box("b25", 55*cm, 7.5*cm, VNS.height/2.-105*cm);
    G4Box* b26 = new G4Box("b26", 10*cm, VNS.width/2.+7.5*cm, VNS.height/2.);
    G4Box* b27 = new G4Box("b27", 75*cm, 20*cm, VNS.height/2.);
    G4Box* b28 = new G4Box("b28", 10*cm, (2.*bymin-VNS.width-165*cm)/2., VNS.height/2.);
    G4Box* b29 = new G4Box("b29", 20*cm, 20*cm, VNS.height/2.);
    G4Box* b30 = new G4Box("b30", 20*cm, 20*cm, VNS.height/2.);
    G4Box* b31 = new G4Box("b31", 30*cm, 20*cm, VNS.height/2.);
    G4Box* b32 = new G4Box("b32", bxmin-120*cm, 10*cm, VNS.height/2.);
    G4Box* b33 = new G4Box("b33", 60*cm, 5*cm, VNS.height/2.-105*cm);
    G4Box* b34 = new G4Box("b34", 5*cm, 45*cm, VNS.height/2.);
    G4Box* b35 = new G4Box("b35", 55*cm, 5*cm, VNS.height/2.-105*cm);
    G4Box* b36 = new G4Box("b36", 45*cm, 5*cm, VNS.height/2.);
    G4Box* b37 = new G4Box("b37", 75*cm, 5*cm, VNS.height/2.-105*cm);
    G4Box* b38 = new G4Box("b38", 100*cm, 5*cm, VNS.height/2.);
    G4Box* b39 = new G4Box("b39", 5*cm, 75*cm, VNS.height/2.);
    G4Box* b40 = new G4Box("b40", 280*cm, 5*cm, VNS.height/2.);
    G4Box* b41 = new G4Box("b41", 5*cm, (2.*bymin-VNS.width-205*cm)/2., VNS.height/2.);
    G4Box* b42 = new G4Box("b42", 5*cm, 65*cm, VNS.height/2.);
    G4Box* b43 = new G4Box("b43", 35*cm,  5*cm, VNS.height/2.);
    G4Box* b44 = new G4Box("b44", 45*cm,  5*cm, VNS.height/2.-105*cm);
    G4Box* b45 = new G4Box("b45", 135*cm, 5*cm, VNS.height/2.);
    G4Box* b46 = new G4Box("b46", 5*cm, (bymax+bymin-VNS.width-710*cm)/2., VNS.height/2.);
    G4Box* b47 = new G4Box("b47", 5*cm, 47.5*cm, VNS.height/2.-105*cm);
    G4Box* b48 = new G4Box("b48", 35*cm, 5*cm, VNS.height/2.);
    G4Box* b49 = new G4Box("b49", 10*cm, 125*cm, VNS.height/2.);
    G4Box* b50 = new G4Box("b50", bxmin-295*cm,  10*cm, VNS.height/2.);
    G4Box* b51 = new G4Box("b51", 10*cm, 220*cm, VNS.height/2.);
    G4Box* b52 = new G4Box("b52", 10*cm, 165*cm, VNS.height/2.);
    G4Box* b53 = new G4Box("b53", 10*cm, 5*cm, VNS.height/2.);
    G4Box* b54 = new G4Box("b54", 5*cm, (bymax+bymin-VNS.width-645*cm)/2., VNS.height/2.);
    G4Box* b55 = new G4Box("b55", 55*cm, 5*cm, VNS.height/2.-105*cm);
    G4Box* b56 = new G4Box("b56", 70*cm, 5*cm, VNS.height/2.-105*cm);

    G4MultiUnion* pw_sol = new G4MultiUnion("VHsolid_BasementPartitionWalls");

//    pw_sol->AddNode(*b01, tr3d = G4Transform3D(ro0, {+room_displacement_x-VNS.length/2.-7.5*cm, +room_displacement_y, 0}));
//    pw_sol->AddNode(*b02, tr3d = G4Transform3D(ro0, {-bxmin-10*cm, +room_displacement_y-7.5*cm, 0}));
//    pw_sol->AddNode(*b03, tr3d = G4Transform3D(ro0, {-bxmin-95*cm, +room_displacement_y-VNS.width/2.+5*cm, 0}));
//    pw_sol->AddNode(*b04, tr3d = G4Transform3D(ro0, {+room_displacement_x-315*cm, +room_displacement_y-VNS.width/2.-7.5*cm, 0}));
//    pw_sol->AddNode(*b05, tr3d = G4Transform3D(ro0, {+room_displacement_x+65*cm,  +room_displacement_y-VNS.width/2.-7.5*cm, 0}));
//    pw_sol->AddNode(*b06, tr3d = G4Transform3D(ro0, {+room_displacement_x-VNS.length/2. +65*cm, +room_displacement_y-VNS.width/2.+37.5*cm, 0}));
//    pw_sol->AddNode(*b07, tr3d = G4Transform3D(ro0, {+room_displacement_x-VNS.length/2.+285*cm, +room_displacement_y-VNS.width/2.+37.5*cm, 0}));
//    pw_sol->AddNode(*b08, tr3d = G4Transform3D(ro0, {+room_displacement_x-VNS.length/2.+130*cm, +room_displacement_y-VNS.width/2.+70*cm, 0}));
//    pw_sol->AddNode(*b09, tr3d = G4Transform3D(ro0, {+room_displacement_x-VNS.length/2.+235*cm, +room_displacement_y-VNS.width/2.+70*cm, 105*cm}));
    pw_sol->AddNode(*b10, tr3d = G4Transform3D(ro0, {-bxmin-750*cm,             +room_displacement_y-VNS.width/2.+5*cm, 0}));
    pw_sol->AddNode(*b11, tr3d = G4Transform3D(ro0, {-bxmin-855*cm,             +room_displacement_y-VNS.width/2.-7.5*cm, 105*cm}));
    pw_sol->AddNode(*b12, tr3d = G4Transform3D(ro0, {-(bxmax+bxmin+930*cm)/2.,  +room_displacement_y-VNS.width/2.-7.5*cm, 0}));
    pw_sol->AddNode(*b13, tr3d = G4Transform3D(ro0, {-bxmin-1047.5*cm,          +room_displacement_y-VNS.width/2.-60*cm, 105*cm}));
    pw_sol->AddNode(*b14, tr3d = G4Transform3D(ro0, {-bxmin-1047.5*cm,          +room_displacement_y-VNS.width/2.-187.5*cm, 0}));
    pw_sol->AddNode(*b15, tr3d = G4Transform3D(ro0, {-(bxmax+bxmin+1040*cm)/2., +room_displacement_y-VNS.width/2.-275*cm, 0}));
    pw_sol->AddNode(*b16, tr3d = G4Transform3D(ro0, {-bxmin-985*cm, +room_displacement_y-VNS.width/2.-172.5*cm, 0}));
    pw_sol->AddNode(*b17, tr3d = G4Transform3D(ro0, {-bxmin-850*cm, +room_displacement_y-VNS.width/2.-172.5*cm, 105*cm}));
    pw_sol->AddNode(*b18, tr3d = G4Transform3D(ro0, {-bxmin-750*cm, +room_displacement_y-VNS.width/2.-185*cm, 0}));
    pw_sol->AddNode(*b19, tr3d = G4Transform3D(ro0, {-bxmin-750*cm, +(-VNS.width-205*cm)/2, 0}));
    pw_sol->AddNode(*b20, tr3d = G4Transform3D(ro0, {-bxmin-240*cm, +(-VNS.width-180*cm)/2, 0}));
    pw_sol->AddNode(*b21, tr3d = G4Transform3D(ro0, {-bxmin- 10*cm, +(-VNS.width-165*cm)/2, 0}));
    pw_sol->AddNode(*b22, tr3d = G4Transform3D(ro0, {-bxmin-685*cm, +room_displacement_y-VNS.width/2.-172.5*cm, 105*cm}));
    pw_sol->AddNode(*b23, tr3d = G4Transform3D(ro0, {-bxmin-405*cm, +room_displacement_y-VNS.width/2.-172.5*cm, 0}));
    pw_sol->AddNode(*b24, tr3d = G4Transform3D(ro0, {-bxmin-150*cm, +room_displacement_y-VNS.width/2.-185*cm, 0}));
    pw_sol->AddNode(*b25, tr3d = G4Transform3D(ro0, {-bxmin- 75*cm, +room_displacement_y-VNS.width/2.-172.5*cm, 105*cm}));
    pw_sol->AddNode(*b26, tr3d = G4Transform3D(ro0, {+bxmin+10*cm,  +room_displacement_y-10*cm, 0}));
    pw_sol->AddNode(*b27, tr3d = G4Transform3D(ro0, {+bxmin+95*cm,  +room_displacement_y-VNS.width/2.+5*cm, 0}));
    pw_sol->AddNode(*b28, tr3d = G4Transform3D(ro0, {+bxmin+10*cm,  +(-VNS.width-165*cm)/2, 0}));
    pw_sol->AddNode(*b29, tr3d = G4Transform3D(ro0, {+bxmin+150*cm, +room_displacement_y-VNS.width/2.-185*cm, 0}));
    pw_sol->AddNode(*b30, tr3d = G4Transform3D(ro0, {+bxmin+750*cm, +room_displacement_y-VNS.width/2.-185*cm, 0}));
    pw_sol->AddNode(*b31, tr3d = G4Transform3D(ro0, {+bxmin+750*cm, +room_displacement_y-VNS.width/2.+5*cm, 0}));
    pw_sol->AddNode(*b32, tr3d = G4Transform3D(ro0, {0,             +room_displacement_y-VNS.width/2.-5*cm, 0}));
    pw_sol->AddNode(*b33, tr3d = G4Transform3D(ro0, {-bxmin+60*cm,  +room_displacement_y-VNS.width/2.-10*cm, 105*cm}));
    pw_sol->AddNode(*b34, tr3d = G4Transform3D(ro0, {+bxmin-115*cm, +room_displacement_y-VNS.width/2.+30*cm, 0}));
    pw_sol->AddNode(*b35, tr3d = G4Transform3D(ro0, {+bxmin- 55*cm, +room_displacement_y-VNS.width/2.+70*cm, 105*cm}));
    pw_sol->AddNode(*b36, tr3d = G4Transform3D(ro0, {+bxmin+215*cm, +room_displacement_y-VNS.width/2.-10*cm, 0}));
    pw_sol->AddNode(*b37, tr3d = G4Transform3D(ro0, {+bxmin+335*cm, +room_displacement_y-VNS.width/2.-10*cm, 105*cm}));
    pw_sol->AddNode(*b38, tr3d = G4Transform3D(ro0, {+bxmin+510*cm, +room_displacement_y-VNS.width/2.-10*cm, 0}));
    pw_sol->AddNode(*b39, tr3d = G4Transform3D(ro0, {+bxmin+605*cm, +room_displacement_y-VNS.width/2. -90*cm, 0}));
    pw_sol->AddNode(*b40, tr3d = G4Transform3D(ro0, {+bxmin+450*cm, +room_displacement_y-VNS.width/2.-170*cm, 0}));
    pw_sol->AddNode(*b41, tr3d = G4Transform3D(ro0, {+bxmin+765*cm, +(-VNS.width-205*cm)/2, 0}));
    pw_sol->AddNode(*b42, tr3d = G4Transform3D(ro0, {+bxmin+175*cm, +room_displacement_y-VNS.width/2.-240*cm, 0}));
    pw_sol->AddNode(*b43, tr3d = G4Transform3D(ro0, {+bxmin+145*cm, +room_displacement_y-VNS.width/2.-310*cm, 0}));
    pw_sol->AddNode(*b44, tr3d = G4Transform3D(ro0, {+bxmin+ 65*cm, +room_displacement_y-VNS.width/2.-310*cm, 105*cm}));
    pw_sol->AddNode(*b45, tr3d = G4Transform3D(ro0, {+bxmin-135*cm, -bymax+540*cm, 0}));
    pw_sol->AddNode(*b46, tr3d = G4Transform3D(ro0, {+bxmin-265*cm, +(-bymax+bymin-VNS.width+380*cm)/2., 0}));
    pw_sol->AddNode(*b47, tr3d = G4Transform3D(ro0, {+bxmin-185*cm, -bymax+487.5*cm, 105*cm}));
    pw_sol->AddNode(*b48, tr3d = G4Transform3D(ro0, {+bxmin-215*cm, -bymax+435*cm, 0}));
    pw_sol->AddNode(*b49, tr3d = G4Transform3D(ro0, {+bxmin-260*cm, -bymax+315*cm, 0}));
    pw_sol->AddNode(*b50, tr3d = G4Transform3D(ro0, {+25*cm,        -bymax+200*cm, 0}));
    pw_sol->AddNode(*b51, tr3d = G4Transform3D(ro0, {-bxmin+310*cm, -bymax+220*cm, 0}));
    pw_sol->AddNode(*b52, tr3d = G4Transform3D(ro0, {-bxmin+150*cm, -bymax+325*cm, 0}));
    pw_sol->AddNode(*b53, tr3d = G4Transform3D(ro0, {-bxmin+130*cm, -bymax+485*cm, 0}));
    pw_sol->AddNode(*b54, tr3d = G4Transform3D(ro0, {-bxmin+115*cm, +(-bymax+bymin-VNS.width+315*cm)/2., 0}));
    pw_sol->AddNode(*b55, tr3d = G4Transform3D(ro0, {-bxmin+ 55*cm, +room_displacement_y-VNS.width/2.-170*cm, 105*cm}));
    pw_sol->AddNode(*b56, tr3d = G4Transform3D(ro0, {-bxmin+230*cm, -bymax+435*cm, 105*cm}));

    pw_sol->Voxelize();

    G4LogicalVolume* BasementPartitionWalls_LV = new G4LogicalVolume(pw_sol, G4Material::GetMaterial("Concrete"),"VHlogic_BasementPartitionWalls");
    new G4PVPlacement(0, {0,0,0}, BasementPartitionWalls_LV, "VHphysic_BasementPartitionWalls", basement_in_noVNS_LV, false, 0, overlap_test);
    BasementPartitionWalls_LV->SetVisAttributes(G4Colour::Green());

    if(construction_verbose_level > 1){
        
        std::cout << "\t------------------------------------------------\n Building mass info : " << std::endl;
        std::cout << "\t\tVNS walls:   " << VNSroom_outLV->GetMass()/kg << " kg"<< std::endl;
        std::cout << "\t\tBasement: " << basement_outLV->GetMass()/kg <<  " kg"<< std::endl;
        std::cout << "\t\tPartition walls: " << BasementPartitionWalls_LV->GetMass()/kg <<  " kg"<< std::endl;
        std::cout << "\t\tGround: " << DCVVolumeHandler::GetDCVolumeHandler("Rock")->GetLogic()->GetMass()/kg <<" kg"<<  std::endl;
        
        std::cout << "\nVNS room centre in world: "<< VNSCentre <<"\n"
        <<"Building centre in world: "<< BuildingCentre <<"\n";
    }
    
    return VNSroom_inLV;
}

G4VSolid* VeryNearSite::BuildingShape(const G4String& name, G4double width_min, G4double mid_height, G4double height, G4double dthick) const{
    
    G4Box* b1 = new G4Box("b1", building.length_max/2.-dthick, width_min/2.-dthick, height);
    G4Box* b2 = new G4Box("b2", building.length_min/2.-dthick, building.width_max/2.-dthick, mid_height);
    G4UnionSolid* b3 = new G4UnionSolid("b3",b1,b2,nullptr,G4ThreeVector(0,0,mid_height-height));
    
    G4Tubs* c1 = new G4Tubs("c1", 0, building.rStaircase-dthick, mid_height, 180*deg, 180*deg);
    G4UnionSolid* b4 = new G4UnionSolid(name,b3,c1,nullptr,G4ThreeVector(-building.length_min/2.+building.rStaircase,-building.width_max/2+dthick, mid_height-height));
    return b4;
}


/// EMPTY SITE///
EmptySite::EmptySite() {
    worldsize = 6*m;
}

G4VPhysicalVolume* EmptySite::BuildWorld(){
    
    if(construction_verbose_level) std::cout << " +++++ Constructing experiment in an Empty Site +++++\n";
    
    auto world = new Volume<G4Box>("World", "Vac", nullptr, {0,0,0}, worldsize/2, worldsize/2, worldsize/2);
    
    AddShielding(world->GetLogic());
    AddDetector(world->GetLogic());
    
    if(construction_verbose_level) std::cout << " +++ End of experiment construction at Empty Site +++\n";
    
    return world->GetPhysic();
}
}
