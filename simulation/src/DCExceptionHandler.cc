#include <DCExceptionHandler.hh>

#include <globals.hh>
#include <G4ios.hh>
#include <G4String.hh>
#include <G4StateManager.hh>
#include <G4RunManager.hh>

#include <stdexcept>
#include <sstream>
#include <iostream>
using namespace std;

G4bool DCExceptionHandler::Notify(const char* originOfException, const char* exceptionCode,
				       G4ExceptionSeverity severity, const char* description)
{
  ostringstream message;
  G4ApplicationState state = G4StateManager::GetStateManager()->GetCurrentState();
  message << "*** DCExceptionHandler : " << exceptionCode
	  << "\n      issued by : " << originOfException << "\n  " << description << endl;
  switch(severity) {
   case FatalException:
    cerr << "\n------------------- DCExceptionHandler -----------------\n"
      << message.str() << "  *** Fatal Exception ***" << endl;
    break;
   case FatalErrorInArgument:
    cerr << "\n------------------- DCExceptionHandler -----------------\n"
    << message.str() << "*** Fatal Error In Argument ***\n" << endl;
    break;
   case RunMustBeAborted:
    G4cerr << "\n------------------- DCExceptionHandler -----------------\n"
      << message.str() << "*** Run Must Be Aborted ***\n" << G4endl;
    if (state==G4State_EventProc) {
      G4RunManager::GetRunManager()->AbortRun();
      return false;
    }
	break;
   case EventMustBeAborted:
    G4cerr << "\n------------------- DCExceptionHandler -----------------\n"
      << message.str() << "*** Event Must Be Aborted ***\n" << G4endl;
    if (state==G4State_EventProc) {
      G4RunManager::GetRunManager()->AbortEvent();
      return false;
    }
    break;
   default:
    // the whole DCExceptionHandler was written just to get rid of this particular warning
    /* in geant4.10.01/source/source/processes/electromagnetic/lowenergy/src/G4UAtomicDeexcitation.cc:
     *      G4Exception("G4UAtomicDeexcitation::GenerateParticles()","de0001",JustWarning, "Energy deposited locally");
     */
    if (G4String("de0001") == G4String(exceptionCode)) { return false; }

    // other warnings
    G4cout << "\n------------------- DCExceptionHandler -----------------\n"
    << message.str() << "*** This is just a warning message. ***\n" << G4endl;
    return false;
  }

  if ( G4StateManager::GetStateManager()->SetNewState(G4State_Abort) ) {
    cerr <<  "*** DCExceptionHandler: Aborting execution correctly ***\n" << endl;
  } else {
    cerr << "*** DCExceptionHandler: Abortion suppressed ***"
	   << "\n*** No guarantee for further execution ***" << endl;
  }

  return true;
}
