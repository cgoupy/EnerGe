/* DC Physics List build for GEANT4.10.1, and perhaps higher version.
 * We assume that NeutronHP is mandatory.
 * We make an intensive use of (DC)GLG4Sim, the neutrino detector
 * package written for KamLAND adapted later to Double Chooz,
 * thanks to the work of Glenn Horton-Smith and Karim Zbiri.
 *
 * Jonathan Gaffiot, feb 2015
 * Matthieu Vivier, mar 2017
 */

#include <DCPhysicsList.hh>

/// Communication with data cards
#include <Parameters.hh>
#include <String.hh>

/// Generic headers
#include <globals.hh>
#include "G4Version.hh"
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <G4RegionStore.hh>
#include <G4String.hh>

/// Particles
#include <G4ParticleDefinition.hh>
#include <G4ParticleTable.hh>
// Bosons
#include <G4Geantino.hh>
#include <G4ChargedGeantino.hh>
#include <G4Gamma.hh>
#include <G4OpticalPhoton.hh>
// Leptons
#include <G4MuonPlus.hh>
#include <G4MuonMinus.hh>
#include <G4Electron.hh>
#include <G4Positron.hh>
#include <G4NeutrinoE.hh>
#include <G4AntiNeutrinoE.hh>
// Baryons
#include <G4Proton.hh>
#include <G4Neutron.hh>
// Ions
// Nuclei
#include <G4Alpha.hh>
#include <G4Deuteron.hh>
#include <G4Triton.hh>
#include <G4He3.hh>
#include <G4IonTable.hh>
#include <G4GenericIon.hh>
// Particle constructors for use_high_energy_physics
#include <G4BosonConstructor.hh>
#include <G4LeptonConstructor.hh>
#include <G4BaryonConstructor.hh>
#include <G4MesonConstructor.hh>
#include <G4ShortLivedConstructor.hh>
#include <G4IonConstructor.hh>

/// Process
// management
#include <G4ProcessManager.hh>
#include <G4FastSimulationManagerProcess.hh>
#include <G4HadronicProcessStore.hh>
#include <G4HadronicInteractionRegistry.hh>

// special
#include <GLG4DeferTrackProc.hh>	// from GLG4
// optical
#include <G4Cerenkov.hh>		// from G4
#include <G4OpBoundaryProcess.hh>	// from G4
#include <G4OpRayleigh.hh>		// from G4
#include <G4OpAbsorption.hh>		// from G4
#include <G4Scintillation.hh>		// from G4
#include <GLG4Scintillation.hh>		// from GLG4 -> Replace G4Scintillation, said to be limited
#include <GLG4OpticalAttenuation.hh>	// from GLG4 -> Replace G4OpRayleigh and G4OpAbsorption, said to be limited
// general / decay
#include <G4Decay.hh>
#include <G4RadioactiveDecay.hh>
// EM
#include <G4EmLivermorePhysics.hh>
#include <G4EmStandardPhysics.hh>
#include <G4EmStandardPhysics_option1.hh>
#include <G4EmStandardPhysics_option2.hh>
#include <G4EmStandardPhysics_option3.hh>
#include <G4EmStandardPhysics_option4.hh>
#include <G4EmLowEPPhysics.hh>
#include <G4EmPenelopePhysics.hh>
#include <G4EmDNAPhysics.hh>
#include <G4EmDNAPhysics_option1.hh>
#include <G4EmExtraPhysics.hh>
#include <G4EmProcessOptions.hh>
// Hadronic
#include <G4IonPhysics.hh>
#include <G4HadronElasticPhysicsHP.hh>
#include <G4HadronElasticPhysicsPHP.hh>
#include <G4StoppingPhysics.hh>
#include <G4HadronPhysicsShielding.hh>		//Underground shielding
#include <G4HadronPhysicsQGSP_BERT_HP.hh>	//QGSP BERTINI cascade NEUTRONHP model
#include <G4HadronPhysicsQGSP_BIC_HP.hh>	//QGSP BINARY cascade NEUTRONHP model
#include <G4HadronPhysicsQGSP_BIC_AllHP.hh>
#include <G4HadronPhysicsFTFP_BERT_HP.hh>	//FTFP BERTINI cascade NEUTRONHP model

/// Custom model
// Elastic processes:
#include <G4HadronElasticProcess.hh>
#include <G4ChipsElasticModel.hh>
#include <G4ElasticHadrNucleusHE.hh>
// Inelastic processes:
#include <G4HadronInelasticProcess.hh>
#include <G4PionPlusInelasticProcess.hh>
#include <G4PionMinusInelasticProcess.hh>
#include <G4KaonPlusInelasticProcess.hh>
#include <G4KaonZeroSInelasticProcess.hh>
#include <G4KaonZeroLInelasticProcess.hh>
#include <G4KaonMinusInelasticProcess.hh>
#include <G4ProtonInelasticProcess.hh>
#include <G4AntiProtonInelasticProcess.hh>
#include <G4NeutronInelasticProcess.hh>
#include <G4AntiNeutronInelasticProcess.hh>
#include <G4DeuteronInelasticProcess.hh>
#include <G4TritonInelasticProcess.hh>
#include <G4AlphaInelasticProcess.hh>
// High energy FTFP model and Bertini cascade
#include <G4FTFModel.hh>
#include <G4LundStringFragmentation.hh>
#include <G4ExcitedStringDecay.hh>
#include <G4PreCompoundModel.hh>
#include <G4GeneratorPrecompoundInterface.hh>
#include <G4TheoFSGenerator.hh>
#include <G4CascadeInterface.hh>
// Cross sections
#include <G4VCrossSectionDataSet.hh>
#include <G4CrossSectionDataSetRegistry.hh>
#include <G4CrossSectionElastic.hh>
#include <G4BGGPionElasticXS.hh>
#include <G4AntiNuclElastic.hh>
#include <G4CrossSectionInelastic.hh>
#include <G4PiNuclearCrossSection.hh>
#include <G4CrossSectionPairGG.hh>
#include <G4BGGNucleonInelasticXS.hh>
#include <G4ComponentAntiNuclNuclearXS.hh>
#if G4VERSION_NUMBER < 1020
#include <G4GGNuclNuclCrossSection.hh>
#else
#include <G4ComponentGGNuclNuclXsc.hh>
#endif
#include <G4HadronElastic.hh>
#include <G4HadronCaptureProcess.hh>
// Stopping processes
#include <G4PiMinusAbsorptionBertini.hh>
#include <G4KaonMinusAbsorptionBertini.hh>
#include <G4AntiProtonAbsorptionFritiof.hh>
#include <G4AntiNeutronAnnihilationAtRest.hh>
// Neutron high-precision models: < 20 MeV
#include <G4NeutronHPElastic.hh>
#include <G4NeutronHPElasticData.hh>
#include <G4NeutronHPCapture.hh>
#include <G4NeutronHPCaptureData.hh>
#include <G4NeutronHPInelastic.hh>
#include <G4NeutronHPInelasticData.hh>
//thermal libraries
#include <G4NeutronHPThermalScattering.hh>
#include <G4NeutronHPThermalScatteringData.hh>

#ifdef USE_NEUTRON_TH
// Neutron models for thermal neutron: < 4 eV
#include <NeutronTHElastic.hh>
#include <NeutronTHElasticData.hh>
#include <NeutronTHCapture.hh>
#include <NeutronTHCaptureData.hh>
#endif

// #include <G4HadronHElasticPhysics.hh>
// #include <G4NeutronTrackingCut.hh>
// #include <G4IonBinaryCascadePhysics.hh>

#include <cstdlib>
#include <stdexcept>
#include <iostream>
using namespace std;

G4double DCPhysicsList::theNeutronMin = 0.*eV;
G4double DCPhysicsList::theNeutronTHElasticMax = 4.*eV;
G4double DCPhysicsList::theNeutronTHCaptureMax = 300.*eV;
G4double DCPhysicsList::theNeutronHPMax = 20.*MeV;


////////////////////////////////////////////////////////////////
//////////////////////////// Methods ///////////////////////////
////////////////////////////////////////////////////////////////
/// Creator
DCPhysicsList::DCPhysicsList() : G4VUserPhysicsList()
{
    const ParamBase &db( ParamBase::GetDataBase() );
    
    try {
        
        if (db["generic_cut_value"] > 0.) { defaultCutValue = db["generic_cut_value"]*mm; }
        (db["generic_ion_cut_value"] > 0.) ? (generic_ion_cut_value = db["generic_ion_cut_value"]*mm) : (generic_ion_cut_value = defaultCutValue);
        photon_cut_value = db["photon_cut_value"]*mm;
        neutron_cut_value = db["neutron_cut_value"]*mm;
        
        RegionCuts.clear();
        
        OpticsVerboseLevel = static_cast<G4int>(db["optics_verboselevel"]);
        PhysicsVerboseLevel = static_cast<G4int>(db["physics_verboselevel"]);
        SetVerboseLevel(PhysicsVerboseLevel);
        
        EnableHighEnergyPhysics = static_cast<G4bool>(db["enable_high_energy_physics"]);
        HadronicModel = static_cast<G4int>(db["high_energy_hadronic_model"]);
        UseNeutronTH = db["use_NeutronTH"];
        UseNeutronHPThermal = db["use_NeutronHPThermal"];
        
#ifndef USE_NEUTRON_TH
        if (UseNeutronTH) {
            G4Exception("VHadronicConstructor::VHadronicConstructor","0",JustWarning,
                        "NeutronTH is currently under construction and has been disabled");
            UseNeutronTH = false;
        }
#endif
        
        EmModel = static_cast<G4int>(db["em_model"]);
        UseFluo= static_cast<G4bool>(db["use_Fluo"]);
        UseAuger= static_cast<G4bool>(db["use_Auger"]);
        UsePIXE= static_cast<G4bool>(db["use_PIXE"]);
        
        EnableOptics = static_cast<G4bool>(db["enable_optics"]);
        UseCerenkov = static_cast<G4bool>(db["use_Cerenkov"]);
        UseGLG4Scintillation = static_cast<G4bool>(db["use_GLG4Scintillation"]);
        UseGLG4Attenuation = static_cast<G4bool>(db["use_GLG4Attenuation"]);
        
        EnableGLG4DeferTrackProc = static_cast<G4bool>(db["enable_GLG4DeferTrackProc"]);
    }
    catch (parambase_except &err) {
        G4Exception("DCPhysicsList::DCPhysicsList","1",FatalException,
                    scat("Missing parameter in database:\n  ",err.what()).c_str() );
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
/// Particles
void DCPhysicsList::ConstructParticle()
{
    cout<<"=======================================================\n";
    cout << " +++ Constructing particles +++\n";
    if (EnableHighEnergyPhysics) {
        cout << " Using high energy physics: creating all particles\n";
        ConstructAllParticles();
    } else {
        cout << " Using low energy physics: creating minimal particles\n";
        ConstructOnlyLowEnergyParticles();
    }
    cout<<"============= End of particle construction ============\n";
}

////////////////////////////////////////////////////////////////
// Whole stack of particles if high energy physics
void DCPhysicsList::ConstructAllParticles()
{
    G4BosonConstructor bosons;
    bosons.ConstructParticle();
    
    G4LeptonConstructor leptons;
    leptons.ConstructParticle();
    
    G4BaryonConstructor baryons;
    baryons.ConstructParticle();
    
    G4MesonConstructor mesons;
    mesons.ConstructParticle();
    
    G4ShortLivedConstructor shortliveds;
    shortliveds.ConstructParticle();
    
    G4IonConstructor ions;
    ions.ConstructParticle();
}

////////////////////////////////////////////////////////////////
// Tuned particles for low energy physics (below 20 MeV)
void DCPhysicsList::ConstructOnlyLowEnergyParticles()
{
    // Bosons: disable ChargedGeantino compared to G4BosonConstructor
    // pseudo-particle
    G4Geantino::GeantinoDefinition(); // seems mandatory
    // gamma
    G4Gamma::GammaDefinition(); // part of antinuE signal
    // optical photon
    if (EnableOptics == true) {
        G4OpticalPhoton::OpticalPhotonDefinition(); // part of antinuE signal
    }
    
    // Leptons: disable (anti)tau, (anti)mu, all neutrinos compared to G4LeptonConstructor
    G4Electron::ElectronDefinition(); // part of antinuE signal
    G4Positron::PositronDefinition(); // part of antinuE signal
    
    // Baryons: keep only proton and neutron compared to G4BaryonConstructor (no stranged, no charmed, no bottomed, no anti)
    G4Proton::ProtonDefinition(); // for neutron reactions
    G4Neutron::NeutronDefinition(); // part of antinuE signal
    
    // Ions: no antinuclei compared to G4IonConstructor
    // light nuclei: can be produced by neutron reaction
    G4Alpha::AlphaDefinition(); // also to simulate alpha decays, such as U/Th decay chain
    G4Deuteron::DeuteronDefinition();
    G4Triton::TritonDefinition();
    G4He3::He3Definition();
    //  generic ion
    //   G4ParticleTable::GetParticleTable()->GetIonTable(); ?? see KLPhysicsList
    G4GenericIon::GenericIonDefinition(); // needed for neutron reactions
    
    // Mesons: none
    // ShortLived: none
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
/// Process
void DCPhysicsList::ConstructProcess()
{
    cout<<"=======================================================\n";
    cout << " +++ Constructing processes +++\n";
    G4HadronicProcessStore::Instance()->SetVerbose(PhysicsVerboseLevel);
    
    cout << " Using transportation\n";
    AddTransportation(); // mandatory
    //AddParametrisation(); // Not mandatory for us. Woulf if we were simulating optics with GLG4Sim (MV Apr 2021).
    
    ConstructHadronic();
    ConstructEM();
    ConstructDecay();
    
    if (EnableOptics) { ConstructOpticalProcesses(); }
    else { cout << " SKIPPING optical processes\n"; }
    
    if (EnableGLG4DeferTrackProc) { ConstructDeferTrackProcess(); }
    else { cout << " SKIPPING DeferTrack process\n"; }
    
    DumpProcessTable();
    cout<<"============ End of processes construction ============\n";
}

////////////////////////////////////////////////////////////////

void DCPhysicsList::ConstructEM()
{
    switch(EmModel) {
        case 1: {
            cout << " Using Standard (no option) EM Physics List\n";
            G4EmStandardPhysics *phys = new G4EmStandardPhysics(PhysicsVerboseLevel);
            phys->ConstructProcess();
        } break;
        case 2: {
            cout << " Using Standard with option 1 EM Physics List\n";
            G4EmStandardPhysics_option1 *phys = new G4EmStandardPhysics_option1(PhysicsVerboseLevel);
            phys->ConstructProcess();
        } break;
        case 3: {
            cout << " Using Standard with option 2 EM Physics List\n";
            G4EmStandardPhysics_option2 *phys = new G4EmStandardPhysics_option2(PhysicsVerboseLevel);
            phys->ConstructProcess();
        } break;
        case 4: {
            cout << " Using Standard with option 3 EM Physics List\n";
            G4EmStandardPhysics_option3 *phys = new G4EmStandardPhysics_option3(PhysicsVerboseLevel);
            phys->ConstructProcess();
        } break;
        case 5: {
            cout << " Using Standard with option 4 EM Physics List\n";
            G4EmStandardPhysics_option4 *phys = new G4EmStandardPhysics_option4(PhysicsVerboseLevel);
            phys->ConstructProcess();
        } break;
        case 6: {
            cout << " Using Low-energy EM Physics List\n";
            G4EmLowEPPhysics *phys = new G4EmLowEPPhysics(PhysicsVerboseLevel);
            phys->ConstructProcess();
        } break;
        case 7: {
            cout << " Using Penelope EM Physics List\n";
            G4EmPenelopePhysics *phys = new G4EmPenelopePhysics(PhysicsVerboseLevel);
            phys->ConstructProcess();
        } break;
        case 8: {
            cout << " Using DNA (no option) EM Physics List\n";
            G4EmDNAPhysics *phys = new G4EmDNAPhysics(PhysicsVerboseLevel);
            phys->ConstructProcess();
        } break;
        case 9: {
            cout << " Using DNA with option 1 EM Physics List\n";
            G4EmDNAPhysics_option1 *phys = new G4EmDNAPhysics_option1(PhysicsVerboseLevel);
            phys->ConstructProcess();
        } break;
        default: {
            cout << " Using Livermore EM Physics List\n";
            G4EmLivermorePhysics *phys = new G4EmLivermorePhysics(PhysicsVerboseLevel);
            phys->ConstructProcess();
        }
    }
    
    if (EnableHighEnergyPhysics == true) {
        cout << " Using Extra EM Physics List\n";
        G4EmExtraPhysics *extra = new G4EmExtraPhysics(PhysicsVerboseLevel);
        extra->ConstructProcess();
    }
    
    //All atomic deexcitation related processes are here switched off by default
    //(because in some EM physics lists, e.g. Livermore & EM_stand_opt4, fluorescence from photons and electrons is activated by default)
    G4EmProcessOptions opt;
    opt.SetVerbose(PhysicsVerboseLevel);
    opt.SetFluo(false);
    opt.SetAuger(false);
    opt.SetPIXE(false);
    //opt.SetMscStepLimitation(fMinimal); Haven't yet understood what this MSCStep limitation does. Would not change it here, because it is for example set by default to 'fUseDistanceToBoundary' in Livermore EM physicis list constructor. MV March 2021.
    
    if ( UseFluo == true ) {
        cout << " Enabling fluorescence (from e+/e- and photons) emission\n";
        opt.SetFluo(true);
        opt.SetDeexcitationIgnoreCuts(true);
    }
    
    if ( UseAuger == true ) {
        cout << " Enabling Auger emission\n";
        opt.SetAuger(true);
        opt.SetDeexcitationIgnoreCuts(true);
    }
    
    if ( UsePIXE == true ) {
        cout << " Enabling PIXE\n";
        opt.SetPIXE(true);
        opt.SetDeexcitationIgnoreCuts(true);
    }
}

////////////////////////////////////////////////////////////////

void DCPhysicsList::ConstructHadronic()
{
    cout << " Using Ion Physics List\n";
    G4IonPhysics *ion = new G4IonPhysics(verboseLevel);
    ion->ConstructProcess();
    
    // Hadron Physics
    if ( EnableHighEnergyPhysics == false ) {
        cout << " Using low energy physics: very simplified hadronic physics\n";
        ConstructHadronic_LowEnergy();
        return;
    }
    cout << " Using high energy physics: full hadronic physics\n";
    
    cout << " Using Stopping Physics List\n";
    G4StoppingPhysics* stopping = new G4StoppingPhysics(PhysicsVerboseLevel);
    stopping->ConstructProcess();
    
    /*if (HadronicModel == 0) {
        cout << " Using hadronic Physics List: custom hadronic physics\n";
        ConstructHadronic_HighEnergy();
        return;
    }*/
    
#ifdef USE_NEUTRON_TH
    if (UseNeutronTH) {
        G4Exception("DCPhysicsList::ConstructHadronic","1",JustWarning,
                    "NeutronTH can not be used with Geant4 PhysicsConstructor: use a custom Hadronic model");
    }
#endif
    
    // Very very VERY stupidily, hadronic physics list do not manage neutron elastic physics,
    // because NeutronBuilder (which has nothing to do with VNeutronBuilder or NeutronHPBuilder)
    // does not contain neutron elastic process, only neutron inelastic process, neutron capture process,
    // and optionnaly neutron fission process.
    // This is needed (it seems) by all the following physics lists, but not by the custom ones.
    cout << " Using Hadron Elastic HP Physics List\n";
    G4HadronElasticPhysicsHP* hadElHP = new G4HadronElasticPhysicsHP(PhysicsVerboseLevel);
    hadElHP->ConstructProcess();
    
    //   cout << " Using Hadron Elastic PHP Physics List\n";
    //   G4HadronElasticPhysicsHP* hadElPHP = new G4HadronElasticPhysicsPHP(PhysicsVerboseLevel);
    //   hadElPHP->ConstructProcess();
    
    switch(HadronicModel) {
        case 1: {
            cout << " Using hadronic Physics List: QGSP_BERT_HP model\n";
            G4HadronPhysicsQGSP_BERT_HP *hadPhysicsList = new G4HadronPhysicsQGSP_BERT_HP();
            hadPhysicsList->ConstructProcess();
        } break;
        case 2: {
            cout << " Using hadronic Physics List: FTFP_BERT_HP model\n";
            G4HadronPhysicsFTFP_BERT_HP *hadPhysicsList = new G4HadronPhysicsFTFP_BERT_HP();
            hadPhysicsList->ConstructProcess();
        } break;
        case 3: {
            cout << " Using hadronic Physics List: QSGP_BIC_HP model\n";
            G4HadronPhysicsQGSP_BIC_HP *hadPhysicsList = new G4HadronPhysicsQGSP_BIC_HP();
            hadPhysicsList->ConstructProcess();
        } break;
        case 4: {
            cout << " Using hadronic Physics List: QGSP_BIC_AllHP model\n";
            G4HadronPhysicsQGSP_BIC_AllHP *hadPhysicsList = new G4HadronPhysicsQGSP_BIC_AllHP();
            hadPhysicsList->ConstructProcess();
        } break;
        case 5: {
            cout << " Using hadronic Physics List: Underground Shielding model\n";
            G4HadronPhysicsShielding *hadPhysicsList = new G4HadronPhysicsShielding("Shielding",true);
            //       hadPhysicsList->UseLEND("???");
            hadPhysicsList->ConstructProcess();
        } break;
        default:
            G4Exception("DCPhysicsList::ConstructHadronic","2",FatalErrorInArgument,
                        "Incorrect choice of hadronic model, check the value of high_energy_hadronic_model in EnerGe_Options.dat");
    }
    
    if (UseNeutronHPThermal){
        cout << " Adding thermal neutron elastic scattering to the hadronic physics list\n";
        //Get the elastic scattering process for neutrons
        G4ParticleDefinition* nPD = G4Neutron::Definition();
        
        G4ProcessVector* pvec = nPD->GetProcessManager()->GetProcessList();
        G4HadronicProcess* nElastic = 0;
        for (size_t i=0; i<pvec->size(); i++) {
            if ((*pvec)[i]->GetProcessSubType() != fHadronElastic) continue;
            nElastic = dynamic_cast<G4HadronicProcess*>((*pvec)[i]);
            break;
        }
        assert(nElastic != 0);
        
        // Get the "regular" HP elastic scattering model, exclude thermal scattering region
        G4HadronicInteraction* nElasticHP = G4HadronicInteractionRegistry::Instance()->FindModel("NeutronHPElastic");
        assert(nElasticHP != 0);
        nElasticHP->SetMinEnergy(4.*eV);
        
        // Attach HP thermal scattering model and data files to process
        nElastic->RegisterMe(new G4NeutronHPThermalScattering);
        nElastic->AddDataSet(new G4NeutronHPThermalScatteringData);
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
/* At beginning taken from KLSim, not knowing what it does (JG - feb 2013).
 * But we definitively need the G4FastSimulationManagerProcess:
 * GLG4PMTOpticalModel is a G4VFastSimulationModel!
 * The same lines are present in the original Nucifer simulation in DCGLG4PhysicsParam.
 */
void DCPhysicsList::AddParametrisation()
{
    cout << " Using fast simulation manager\n";
    G4FastSimulationManagerProcess*  theFastSimulationManagerProcess = new G4FastSimulationManagerProcess();
    theFastSimulationManagerProcess->SetVerboseLevel(PhysicsVerboseLevel);
#if G4VERSION_NUMBER > 1020
    auto theParticleIterator = GetParticleIterator();
#endif
    theParticleIterator->reset();
    while( (*theParticleIterator)() ){
        G4ParticleDefinition* particle = theParticleIterator->value();
        G4ProcessManager* pmanager = particle->GetProcessManager();
        // both postStep and alongStep action are required if the detector
        // makes use of ghost volumes. If no ghost, the postStep is sufficient (and faster?).
        pmanager->AddProcess(theFastSimulationManagerProcess, -1, -1, 1); // AtRest, ALongStep, PostStep
        pmanager->SetVerboseLevel(PhysicsVerboseLevel);
    }
}

////////////////////////////////////////////////////////////////

void DCPhysicsList::ConstructDeferTrackProcess()
{
    cout << " Using DeferTrack process\n";
    GLG4DeferTrackProc* theDeferProcess = new GLG4DeferTrackProc();
    theDeferProcess->SetVerboseLevel(PhysicsVerboseLevel);
#if G4VERSION_NUMBER > 1020
    auto theParticleIterator = GetParticleIterator();
#endif
    theParticleIterator->reset();
    while( (*theParticleIterator)() ) {
        G4ParticleDefinition* particle = theParticleIterator->value();
        G4ProcessManager* pmanager = particle->GetProcessManager();
        theParticleIterator->value()->GetProcessManager()->AddDiscreteProcess(theDeferProcess);
        pmanager->SetVerboseLevel(PhysicsVerboseLevel);
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

void DCPhysicsList::ConstructDecay()
{
    ConstructIonDecay();
    if (EnableHighEnergyPhysics) { ConstructFermionDecay(); }
}

void DCPhysicsList::ConstructFermionDecay()
{
    cout << " Using fermion decay\n";
    //   new G4DecayPhysics(PhysicsVerboseLevel);
    // Add fermion decay process
    G4Decay* theDecayProcess = new G4Decay(PhysicsVerboseLevel);
#if G4VERSION_NUMBER > 1020
    auto theParticleIterator = GetParticleIterator();
#endif
    theParticleIterator->reset();
    while( (*theParticleIterator)() ) {
        G4ParticleDefinition* particle = theParticleIterator->value();
        G4ProcessManager* pmanager = particle->GetProcessManager();
        
        if (theDecayProcess->IsApplicable(*particle) && !particle->IsShortLived()) {
            pmanager ->AddProcess(theDecayProcess);
            // set ordering for PostStepDoIt and AtRestDoIt
            pmanager ->SetProcessOrdering(theDecayProcess, idxPostStep);
            pmanager ->SetProcessOrdering(theDecayProcess, idxAtRest);
            pmanager->SetVerboseLevel(PhysicsVerboseLevel);
        }
    }
}

////////////////////////////////////////////////////////////////

void DCPhysicsList::ConstructIonDecay()
{
    cout << " Using ion decay\n";
    //   new G4RadioactiveDecayPhysics(PhysicsVerboseLevel);
    // Declare radioactive decay to the GenericIon in the IonTable.
    const G4IonTable *theIonTable = G4ParticleTable::GetParticleTable()->GetIonTable();
    G4RadioactiveDecay *theRadioactiveDecay = new G4RadioactiveDecay(PhysicsVerboseLevel);
    theRadioactiveDecay->SetICM(true);               //Internal Conversion
    theRadioactiveDecay->SetARM(true);               //Atomic Rearangement
    
    for (G4int i=0; i<theIonTable->Entries(); i++) {
        if (theIonTable->GetParticle(i)->GetParticleName() == G4GenericIon::Definition()->GetParticleName()) {
            G4ProcessManager* pmanager = theIonTable->GetParticle(i)->GetProcessManager();
            pmanager->SetVerboseLevel(PhysicsVerboseLevel);
            pmanager->AddProcess(theRadioactiveDecay);
            pmanager->SetProcessOrdering(theRadioactiveDecay, idxPostStep);
            pmanager->SetProcessOrdering(theRadioactiveDecay, idxAtRest);
        }
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
// adapted from DCGLG4Sim
void DCPhysicsList::ConstructOpticalProcesses()
{
    cout << " Using optical boundary process\n";
    G4OpBoundaryProcess* theBoundaryProcess = new G4OpBoundaryProcess();
    
    if (OpticsVerboseLevel > 0) { theBoundaryProcess->DumpInfo(); }
    theBoundaryProcess->SetVerboseLevel( (OpticsVerboseLevel>2 ? OpticsVerboseLevel-2 : 0) );
#if G4VERSION_NUMBER > 1020
    auto theParticleIterator = GetParticleIterator();
#endif
    
    theParticleIterator->reset();
    while( (*theParticleIterator)() ){
        G4ParticleDefinition* particle = theParticleIterator->value();
        G4ProcessManager* pmanager = particle->GetProcessManager();
        pmanager->SetVerboseLevel(PhysicsVerboseLevel);
        if (particle->GetParticleName() == G4OpticalPhoton::Definition()->GetParticleName()) {
            pmanager->AddDiscreteProcess(theBoundaryProcess);
        }
    }
    
    ConstructCerenkov();
    ConstructAttenuation();
    ConstructScintillation();
}

////////////////////////////////////////////////////////////////

void DCPhysicsList::ConstructCerenkov()
{
    if (UseCerenkov == false) { return; }
    
    cout << " Using optical Cerenkov process\n";
    G4Cerenkov* theCerenkovProcess = new G4Cerenkov();
    
    // request that cerenkov and scintillation photons be tracked first,
    // before continuing originating particle step.  Otherwise, we get too many secondaries!
    theCerenkovProcess->SetTrackSecondariesFirst(true);
    
    theCerenkovProcess->SetVerboseLevel( (OpticsVerboseLevel>2 ? OpticsVerboseLevel-2 : 0) );
    if (OpticsVerboseLevel > 0) { theCerenkovProcess->DumpInfo(); }
    
    // G4int MaxNumPhotons = 300;
    // theCerenkovProcess->SetMaxNumPhotonsPerStep(MaxNumPhotons);
    
    // G4OpticalSurfaceModel themodel = unified;
    // theBoundaryProcess->SetModel(themodel);
    
#if G4VERSION_NUMBER > 1020
    auto theParticleIterator = GetParticleIterator();
#endif
    theParticleIterator->reset();
    while( (*theParticleIterator)() ){
        G4ParticleDefinition* particle = theParticleIterator->value();
        G4ProcessManager* pmanager = particle->GetProcessManager();
        
        if (theCerenkovProcess->IsApplicable(*particle)) {
            pmanager->AddProcess(theCerenkovProcess);
            pmanager->SetProcessOrdering(theCerenkovProcess,idxPostStep);
            pmanager->SetVerboseLevel(PhysicsVerboseLevel);
        }
    }
}

////////////////////////////////////////////////////////////////

void DCPhysicsList::ConstructAttenuation()
{
    //  GLG4OpAttenuation implements Rayleigh scattering.
    //  At the time of the writting of KLG4sim (KamLAND simulation), G4OpRayleigh is not used for the following two reasons:
    //    1) It doesn't even try to work for anything other than water.
    //    2) It doesn't actually work for water, either.
    if (UseGLG4Attenuation) {
        cout << " Using optical attenuation from GLG4 process\n";
        GLG4OpticalAttenuation* theAttenuationProcess = new GLG4OpticalAttenuation();
        
        theAttenuationProcess->SetVerboseLevel(OpticsVerboseLevel);
        if (OpticsVerboseLevel > 0) { theAttenuationProcess->DumpInfo(); }
#if G4VERSION_NUMBER > 1020
    auto theParticleIterator = GetParticleIterator();
#endif
        
        theParticleIterator->reset();
        while( (*theParticleIterator)() ){
            G4ParticleDefinition* particle = theParticleIterator->value();
            G4ProcessManager* pmanager = particle->GetProcessManager();
            if (particle->GetParticleName() == G4OpticalPhoton::Definition()->GetParticleName()) {
                pmanager->AddDiscreteProcess(theAttenuationProcess);
            }
            pmanager->SetVerboseLevel(PhysicsVerboseLevel);
        }
    } else {
        cout << " Using default optical attenuation process\n";
        // default Geant4 attenuation: absorption and Rayleigh scattering
        G4OpAbsorption* theAbsorptionProcess         = new G4OpAbsorption();
        G4OpRayleigh*   theRayleighScatteringProcess = new G4OpRayleigh();
        
        theAbsorptionProcess->SetVerboseLevel(OpticsVerboseLevel);
        theRayleighScatteringProcess->SetVerboseLevel(OpticsVerboseLevel);
        if (OpticsVerboseLevel > 0) {
            theAbsorptionProcess->DumpInfo();
            theRayleighScatteringProcess->DumpInfo();
        }
        
#if G4VERSION_NUMBER > 1020
    auto theParticleIterator = GetParticleIterator();
#endif
        theParticleIterator->reset();
        while( (*theParticleIterator)() ){
            G4ParticleDefinition* particle = theParticleIterator->value();
            G4ProcessManager* pmanager = particle->GetProcessManager();
            
            if (particle->GetParticleName() == G4OpticalPhoton::Definition()->GetParticleName()) {
                pmanager->AddDiscreteProcess(theAbsorptionProcess);
                pmanager->AddDiscreteProcess(theRayleighScatteringProcess);
            }
            pmanager->SetVerboseLevel(PhysicsVerboseLevel);
        }
    }
}

////////////////////////////////////////////////////////////////

void DCPhysicsList::ConstructScintillation()
{
    // GLG4Scintillation allows many things more than G4Scintillation, see the the GLG4Scintillation.hh file comments
    if (UseGLG4Scintillation) {
        cout << " Using optical scintillation from GLG4 process\n";
        GLG4Scintillation* theDefaultScintProcess = new GLG4Scintillation("default", 0.0);
        GLG4Scintillation* theNeutronScintProcess = new GLG4Scintillation("neutron", 0.9*G4Neutron::Neutron()->GetPDGMass());
        GLG4Scintillation* theAlphaScintProcess = new GLG4Scintillation("alpha", 0.9*G4Alpha::Alpha()->GetPDGMass());
        
        if (OpticsVerboseLevel > 0) {
            theDefaultScintProcess->DumpInfo();
            theNeutronScintProcess->DumpInfo();
            theAlphaScintProcess->DumpInfo();
        }
        G4int theGLG4ScintVerboseLevel = max<G4int>(OpticsVerboseLevel,static_cast<G4int>(ParamBase::GetDataBase()["GLG4scint_verboselevel"]));
        theDefaultScintProcess->SetVerboseLevel(theGLG4ScintVerboseLevel);
        theAlphaScintProcess->SetVerboseLevel(theGLG4ScintVerboseLevel);
        theNeutronScintProcess->SetVerboseLevel(theGLG4ScintVerboseLevel);
        // scintillation process is no longer a Geant4 "Process",
        // so it is not added to the process manager's list
    } else {
        cout << " Using default optical scintillation process\n";
        // default scintillation process
        G4Scintillation* theScintProcessDef = new G4Scintillation("Scintillation");
        //   theScintProcessDef->SetScintillationByParticleType(true);
        theScintProcessDef->SetTrackSecondariesFirst(true);
        theScintProcessDef->SetScintillationYieldFactor(1.0);
        theScintProcessDef->SetScintillationExcitationRatio(1.0);
        theScintProcessDef->SetFiniteRiseTime(45*ns);
        
        // Use Birks's correction in the scintillation process
        //   G4EmSaturation*  theEmSaturation = G4LossTableManager::Instance()->EmSaturation();
        //   theEmSaturation->DumpBirksCoefficients();
        //   theScintProcessDef->AddSaturation(theEmSaturation);
        
        theScintProcessDef->SetVerboseLevel(OpticsVerboseLevel);
        if (OpticsVerboseLevel > 0) {
            theScintProcessDef->DumpInfo();
            theScintProcessDef->DumpPhysicsTable();
        }
        
#if G4VERSION_NUMBER > 1020
    auto theParticleIterator = GetParticleIterator();
#endif
        theParticleIterator->reset();
        while( (*theParticleIterator)() ) {
            G4ParticleDefinition* particle = theParticleIterator->value();
            G4ProcessManager* pmanager = particle->GetProcessManager();
            
            if (theScintProcessDef->IsApplicable(*particle)) {
                pmanager->AddProcess(theScintProcessDef,ordDefault,ordInActive,ordDefault);
                pmanager->SetProcessOrderingToLast(theScintProcessDef,idxAtRest);
                pmanager->SetProcessOrderingToLast(theScintProcessDef,idxPostStep);
                pmanager->SetVerboseLevel(PhysicsVerboseLevel);
            }
        }
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
/// Cuts
void DCPhysicsList::SetCuts() {
    cout<<"=======================================================\n";
    cout << " +++ Setting cuts +++\n";
    
    //Set min and max energy for the production of particles
    /*
     - 1e-3 eV is because of thermal neutrons: not sure it is a valid reason. Particle production cuts applies to gamma, e+/e- for EM processes, and to all ions (nuclear recoils) for hadron elastic processes (MV: Mar 2021)
     - 20 MeV is the highest energy for fission neutron, and it includes gamma from neutron capture: same comment than before. According to G4 documentation, at rest processes do not have particle production cuts (MV: Mar 2021)
     */
    if (EnableHighEnergyPhysics) {
        G4ProductionCutsTable::GetProductionCutsTable()->SetEnergyRange(1e-3*eV,100.*GeV);
    }
    
    else {
        G4ProductionCutsTable::GetProductionCutsTable()->SetEnergyRange(1e-3*eV,20.*MeV);
    }
    
    //Before cuts tuning, begin with the default cut value for all particle types
    SetCutsWithDefault();
    
    // different (lower) cut for some particle
    if (generic_ion_cut_value > 0.) {
        /*SetCutValue(generic_ion_cut_value, G4GenericIon::Definition()->GetParticleName());
        SetCutValue(generic_ion_cut_value, G4Proton::Definition()->GetParticleName());
        SetCutValue(generic_ion_cut_value, G4Deuteron::Definition()->GetParticleName());
        SetCutValue(generic_ion_cut_value, G4Triton::Definition()->GetParticleName());
        SetCutValue(generic_ion_cut_value, G4Alpha::Definition()->GetParticleName());
        SetCutValue(generic_ion_cut_value, G4He3::Definition()->GetParticleName());*/
        
        //No need to specify cuts for each ions. G4 documentation explicitely tells that setting a 'production' cut to protons actually applies it to any recoiling ions produced in hadron elastic scattering processes (MV Mar 2021)
        SetCutValue(generic_ion_cut_value,"proton");
    }
    
    //MV Mar 2021: probably makes no differences to the production and tracking of optical photons and neutrons. G4 documentation states that there are no production cuts for neutrons. Keep it for now for consistency with previous simulations.
    if (photon_cut_value > 0. and EnableOptics) { SetCutValue(photon_cut_value, G4OpticalPhoton::Definition()->GetParticleName()); }
    if (neutron_cut_value > 0.) { SetCutValue(neutron_cut_value, G4Neutron::Definition()->GetParticleName()); }
    
    if (PhysicsVerboseLevel>0) { DumpCutValuesTable(); }
    
    cout<<"================== End of cut setting =================\n";
}

void DCPhysicsList::AssignCutsToRegion(G4String regName, G4double &gamma_cut, G4double &electron_cut, G4double &positron_cut, G4double &proton_cut) {
    
    G4Region* reg = G4RegionStore::GetInstance()->GetRegion(regName);
    G4ProductionCuts* PPcuts = new G4ProductionCuts();
    PPcuts->SetProductionCut(proton_cut,G4ProductionCuts::GetIndex("proton"));
    PPcuts->SetProductionCut(gamma_cut,G4ProductionCuts::GetIndex("gamma"));
    PPcuts->SetProductionCut(electron_cut,G4ProductionCuts::GetIndex("e-"));
    PPcuts->SetProductionCut(positron_cut,G4ProductionCuts::GetIndex("e+"));
    reg->SetProductionCuts(PPcuts);
    
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
/// Print
void DCPhysicsList::DumpProcessTable()
{
    cout << "\n Process table:\n";
    vector<G4int> vProcessIndex;
#if G4VERSION_NUMBER > 1020
    auto theParticleIterator = GetParticleIterator();
#endif
    theParticleIterator->reset();
    while( (*theParticleIterator)() ){
        G4ProcessManager* pmanager = theParticleIterator->value()->GetProcessManager();
        pmanager->SetVerboseLevel(PhysicsVerboseLevel);
        G4ProcessVector *temp =  pmanager->GetProcessList();
        for(G4int i=0; i<pmanager->GetProcessListLength(); i++) {
            G4int index = 1000*(*temp)[i]->GetProcessType() + (*temp)[i]->GetProcessSubType() ;
            G4bool IndexExists=false;
            for (auto &el : vProcessIndex) { if (el == index) { IndexExists=true; break; } }
            if (IndexExists == true) { continue; }
            cout << " Process Map Table - Index: " << index << " ; Process Name: "
            << (*temp)[i]->GetProcessName() << endl;
            vProcessIndex.push_back(index);
        }
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////

void DCPhysicsList::ConstructHadronic_LowEnergy()
{
    cout << " Using hadronic Physics List: custom low energy\n";
    
    const G4double theMin = theNeutronMin;
    const G4double theMax = theNeutronHPMax;
    
    //Elastic models
    G4HadronElastic* elastic_lhep = new G4HadronElastic();
    elastic_lhep->SetMaxEnergy( theMax );
    G4ChipsElasticModel* elastic_chip = new G4ChipsElasticModel();
    elastic_chip->SetMaxEnergy( theMax );
    
    // Inelastic scattering
    G4CascadeInterface * theBERTModel = new G4CascadeInterface;
    theBERTModel->SetMinEnergy( theMin );
    theBERTModel->SetMaxEnergy( theMax );
    
    G4CrossSectionDataSetRegistry* theXSreg = G4CrossSectionDataSetRegistry::Instance();
    
#if G4VERSION_NUMBER < 1020
    G4VCrossSectionDataSet* theGGNuclNuclData = theXSreg->GetCrossSectionDataSet(G4GGNuclNuclCrossSection::Default_Name());
#else
    G4VCrossSectionDataSet* theGGNuclNuclData = new G4CrossSectionInelastic(new G4ComponentGGNuclNuclXsc());
#endif
    
#if G4VERSION_NUMBER > 1020
    auto theParticleIterator = GetParticleIterator();
#endif
    
    theParticleIterator->reset();
    while ((*theParticleIterator)()) {
        G4ParticleDefinition* particle = theParticleIterator->value();
        G4ProcessManager* pmanager = particle->GetProcessManager();
        G4String particleName = particle->GetParticleName();
        pmanager->SetVerboseLevel(PhysicsVerboseLevel);
        
        // proton
        if (particleName == G4Proton::Definition()->GetParticleName()) {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess();
            theElasticProcess->AddDataSet(theXSreg->GetCrossSectionDataSet(G4ChipsProtonElasticXS::Default_Name()));
            theElasticProcess->RegisterMe( elastic_chip );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4ProtonInelasticProcess* theInelasticProcess = new G4ProtonInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( new G4BGGNucleonInelasticXS( G4Proton::Proton() ) );
            theInelasticProcess->RegisterMe( theBERTModel );
            pmanager->AddDiscreteProcess( theInelasticProcess );
        } // neutron
        else if (particleName == G4Neutron::Definition()->GetParticleName()) {
            // elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess();
            G4NeutronHPElastic * theElasticNeutronHP = new G4NeutronHPElastic;
            theElasticNeutronHP->SetMinEnergy( UseNeutronTH ? theNeutronTHElasticMax : theNeutronMin );
            theElasticNeutronHP->SetMaxEnergy( theNeutronHPMax );
            theElasticProcess->RegisterMe( theElasticNeutronHP );
            theElasticProcess->AddDataSet( new G4NeutronHPElasticData );
#ifdef USE_NEUTRON_TH
            if (UseNeutronTH) {
                NeutronTHElastic* theElasticTHNeutron = new NeutronTHElastic();
                theElasticProcess->RegisterMe(theElasticTHNeutron);
                theElasticProcess->AddDataSet(new NeutronTHElasticData());
                theElasticTHNeutron->SetMinEnergy(theNeutronMin);
                theElasticTHNeutron->SetMaxEnergy(theNeutronTHElasticMax);
            }
#endif
            pmanager->AddDiscreteProcess( theElasticProcess );
            
            // inelastic scattering
            G4NeutronInelasticProcess* theInelasticProcess = new G4NeutronInelasticProcess("inelastic");
            G4NeutronHPInelastic * theNeutronInelasticHPModel = new G4NeutronHPInelastic();
            theNeutronInelasticHPModel->SetMinEnergy( theNeutronMin );
            theNeutronInelasticHPModel->SetMaxEnergy( theNeutronHPMax );
            theInelasticProcess->RegisterMe( theNeutronInelasticHPModel );
            theInelasticProcess->AddDataSet( new G4NeutronHPInelasticData );
            pmanager->AddDiscreteProcess(theInelasticProcess);
            
            // capture
            G4HadronCaptureProcess* theCaptureProcess = new G4HadronCaptureProcess;
            G4NeutronHPCapture * theNeutronHPCapture = new G4NeutronHPCapture();
            theNeutronHPCapture->SetMinEnergy( UseNeutronTH ? theNeutronTHCaptureMax : theNeutronMin );
            theNeutronHPCapture->SetMaxEnergy( theNeutronHPMax );
            theCaptureProcess->RegisterMe(theNeutronHPCapture);
            theCaptureProcess->AddDataSet( new G4NeutronHPCaptureData);
#ifdef USE_NEUTRON_TH
            if (UseNeutronTH) {
                NeutronTHCapture* theNeutronTHCaptureModel = new NeutronTHCapture();
                theCaptureProcess->RegisterMe(theNeutronTHCaptureModel);
                theCaptureProcess->AddDataSet(new NeutronTHCaptureData());
                theNeutronTHCaptureModel->SetMinEnergy(theNeutronMin);
                theNeutronTHCaptureModel->SetMaxEnergy(theNeutronTHCaptureMax);
            }
#endif
            pmanager->AddDiscreteProcess(theCaptureProcess);
        } // alpha
        else if (particleName == G4Alpha::Definition()->GetParticleName() ) {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->RegisterMe( elastic_lhep );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4AlphaInelasticProcess* theInelasticProcess = new G4AlphaInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( theGGNuclNuclData );
            theInelasticProcess->RegisterMe( theBERTModel);
            pmanager->AddDiscreteProcess( theInelasticProcess );
        } // deuteron
        else if ( particleName == G4Deuteron::Definition()->GetParticleName() ) {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->RegisterMe( elastic_lhep );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4DeuteronInelasticProcess* theInelasticProcess = new G4DeuteronInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( theGGNuclNuclData );
            theInelasticProcess->RegisterMe( theBERTModel);
            pmanager->AddDiscreteProcess( theInelasticProcess );
        } // triton
        else if ( particleName == G4Triton::Definition()->GetParticleName() ) {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->RegisterMe( elastic_lhep );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4TritonInelasticProcess* theInelasticProcess = new G4TritonInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( theGGNuclNuclData );
            theInelasticProcess->RegisterMe( theBERTModel);
            pmanager->AddDiscreteProcess( theInelasticProcess );
        } // He3
        else if ( particleName == G4He3::Definition()->GetParticleName() ) {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->RegisterMe( elastic_lhep );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4HadronInelasticProcess* theInelasticProcess = new G4HadronInelasticProcess("inelastic",G4He3::Definition());
            theInelasticProcess->AddDataSet( theGGNuclNuclData );
            theInelasticProcess->RegisterMe( theBERTModel);
            pmanager->AddDiscreteProcess( theInelasticProcess );
        }
    }
}

////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////
// Taken from Dark Side simulation
// Makes discrete physics processes for the hadrons, at present limited
// to those particles with GHEISHA interactions (INTRC > 0).
// The processes are: Elastic scattering and Inelastic scattering.
// F.W.Jones  09-JUL-1998

/*void DCPhysicsList::ConstructHadronic_HighEnergy()
{
    cout << " Using hadronic Physics List: custom high energy\n";
    
    //Elastic models
    const G4double elastic_elimitPi = 1.0*GeV;
    
    G4HadronElastic* elastic_lhep0 = new G4HadronElastic();
    G4HadronElastic* elastic_lhep1 = new G4HadronElastic();
    elastic_lhep1->SetMaxEnergy( elastic_elimitPi );
    G4ChipsElasticModel* elastic_chip = new G4ChipsElasticModel();
    G4ElasticHadrNucleusHE* elastic_he = new G4ElasticHadrNucleusHE();
    elastic_he->SetMinEnergy( elastic_elimitPi );
    
    // Inelastic scattering
    const G4double theFTFMin0 =    0.0*GeV;
    const G4double theFTFMin1 =    4.0*GeV;
    const G4double theFTFMax =   100.0*TeV;
    const G4double theBERTMin0 =   0.0*GeV;
    const G4double theBERTMin1 =  20.0*MeV;
    const G4double theBERTMax =    5.0*GeV;
    const G4double elastic_elimitAntiNuc = 100.0*MeV;
    
    G4FTFModel * theStringModel = new G4FTFModel;
    G4ExcitedStringDecay * theStringDecay = new G4ExcitedStringDecay( new G4LundStringFragmentation );
    theStringModel->SetFragmentationModel( theStringDecay );
    G4PreCompoundModel * thePreEquilib = new G4PreCompoundModel( new G4ExcitationHandler );
    G4GeneratorPrecompoundInterface * theCascade = new G4GeneratorPrecompoundInterface( thePreEquilib );
    
    G4TheoFSGenerator * theFTFModel0 = new G4TheoFSGenerator( "FTFP" );
    theFTFModel0->SetHighEnergyGenerator( theStringModel );
    theFTFModel0->SetTransport( theCascade );
    theFTFModel0->SetMinEnergy( theFTFMin0 );
    theFTFModel0->SetMaxEnergy( theFTFMax );
    
    G4TheoFSGenerator * theFTFModel1 = new G4TheoFSGenerator( "FTFP" );
    theFTFModel1->SetHighEnergyGenerator( theStringModel );
    theFTFModel1->SetTransport( theCascade );
    theFTFModel1->SetMinEnergy( theFTFMin1 );
    theFTFModel1->SetMaxEnergy( theFTFMax );
    
    G4CascadeInterface * theBERTModel0 = new G4CascadeInterface;
    theBERTModel0->SetMinEnergy( theBERTMin0 );
    theBERTModel0->SetMaxEnergy( theBERTMax );
    
    G4CascadeInterface * theBERTModel1 = new G4CascadeInterface;
    theBERTModel1->SetMinEnergy( theBERTMin1 );
    theBERTModel1->SetMaxEnergy( theBERTMax );
    
    G4VCrossSectionDataSet * thePiData = new G4CrossSectionPairGG( new G4PiNuclearCrossSection, 91*GeV );
    G4VCrossSectionDataSet * theAntiNucleonData = new G4CrossSectionInelastic( new G4ComponentAntiNuclNuclearXS );
    G4CrossSectionDataSetRegistry* theXSreg = G4CrossSectionDataSetRegistry::Instance();
#if G4VERSION_NUMBER < 1020
    G4VCrossSectionDataSet* theGGNuclNuclData = theXSreg->GetCrossSectionDataSet(G4GGNuclNuclCrossSection::Default_Name());
#else
    G4VCrossSectionDataSet* theGGNuclNuclData = new G4CrossSectionInelastic(new G4ComponentGGNuclNuclXsc());
#endif
    
    theParticleIterator->reset();
    while ((*theParticleIterator)()) {
        G4ParticleDefinition* particle = theParticleIterator->value();
        G4ProcessManager* pmanager = particle->GetProcessManager();
        G4String particleName = particle->GetParticleName();
        pmanager->SetVerboseLevel(PhysicsVerboseLevel);
        
        if (particleName == "pi+") {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->AddDataSet( new G4BGGPionElasticXS( particle ) );
            theElasticProcess->RegisterMe( elastic_lhep1 );
            theElasticProcess->RegisterMe( elastic_he );
            pmanager->AddDiscreteProcess( theElasticProcess );
            //Inelastic scattering
            G4PionPlusInelasticProcess* theInelasticProcess = new G4PionPlusInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( thePiData );
            theInelasticProcess->RegisterMe( theFTFModel1 );
            theInelasticProcess->RegisterMe( theBERTModel0 );
            pmanager->AddDiscreteProcess( theInelasticProcess );
        }
        else if (particleName == "pi-") {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->AddDataSet( new G4BGGPionElasticXS( particle ) );
            theElasticProcess->RegisterMe( elastic_lhep1 );
            theElasticProcess->RegisterMe( elastic_he );
            pmanager->AddDiscreteProcess( theElasticProcess );
            //Inelastic scattering
            G4PionMinusInelasticProcess* theInelasticProcess = new G4PionMinusInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( thePiData );
            theInelasticProcess->RegisterMe( theFTFModel1 );
            theInelasticProcess->RegisterMe( theBERTModel0 );
            pmanager->AddDiscreteProcess( theInelasticProcess );
            //Absorption
            pmanager->AddRestProcess(new G4PiMinusAbsorptionBertini, ordDefault);
        }
        else if (particleName == "kaon+") {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->RegisterMe( elastic_lhep0 );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4KaonPlusInelasticProcess* theInelasticProcess = new G4KaonPlusInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( theXSreg->GetCrossSectionDataSet(G4ChipsKaonPlusInelasticXS::Default_Name()));
            theInelasticProcess->RegisterMe( theFTFModel1 );
            theInelasticProcess->RegisterMe( theBERTModel0 );
            pmanager->AddDiscreteProcess( theInelasticProcess );
        }
        else if (particleName == "kaon0S") {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->RegisterMe( elastic_lhep0 );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4KaonZeroSInelasticProcess* theInelasticProcess = new G4KaonZeroSInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( theXSreg->GetCrossSectionDataSet(G4ChipsKaonZeroInelasticXS::Default_Name()));
            theInelasticProcess->RegisterMe( theFTFModel1 );
            theInelasticProcess->RegisterMe( theBERTModel0 );
            pmanager->AddDiscreteProcess( theInelasticProcess );
        }
        else if (particleName == "kaon0L") {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->RegisterMe( elastic_lhep0 );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4KaonZeroLInelasticProcess* theInelasticProcess = new G4KaonZeroLInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( theXSreg->GetCrossSectionDataSet(G4ChipsKaonZeroInelasticXS::Default_Name()));
            theInelasticProcess->RegisterMe( theFTFModel1 );
            theInelasticProcess->RegisterMe( theBERTModel0 );
            pmanager->AddDiscreteProcess( theInelasticProcess );
        }
        else if (particleName == "kaon-") {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->RegisterMe( elastic_lhep0 );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4KaonMinusInelasticProcess* theInelasticProcess = new G4KaonMinusInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( theXSreg->GetCrossSectionDataSet(G4ChipsKaonMinusInelasticXS::Default_Name()));
            theInelasticProcess->RegisterMe( theFTFModel1 );
            theInelasticProcess->RegisterMe( theBERTModel0 );
            pmanager->AddDiscreteProcess( theInelasticProcess );
            pmanager->AddRestProcess(new G4KaonMinusAbsorptionBertini, ordDefault);
        }
        else if (particleName == "proton") {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->AddDataSet(theXSreg->GetCrossSectionDataSet(G4ChipsProtonElasticXS::Default_Name()));
            theElasticProcess->RegisterMe( elastic_chip );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4ProtonInelasticProcess* theInelasticProcess = new G4ProtonInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( new G4BGGNucleonInelasticXS( G4Proton::Proton() ) );
            theInelasticProcess->RegisterMe( theFTFModel1 );
            theInelasticProcess->RegisterMe( theBERTModel0 );
            pmanager->AddDiscreteProcess( theInelasticProcess );
        }
        else if (particleName == "anti_proton") {
            // Elastic scattering
            G4AntiNuclElastic* elastic_anuc = new G4AntiNuclElastic();
            elastic_anuc->SetMinEnergy( elastic_elimitAntiNuc );
            G4CrossSectionElastic* elastic_anucxs = new G4CrossSectionElastic( elastic_anuc->GetComponentCrossSection() );
            G4HadronElastic* elastic_lhep2 = new G4HadronElastic();
            elastic_lhep2->SetMaxEnergy( elastic_elimitAntiNuc );
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->AddDataSet( elastic_anucxs );
            theElasticProcess->RegisterMe( elastic_lhep2 );
            theElasticProcess->RegisterMe( elastic_anuc );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4AntiProtonInelasticProcess* theInelasticProcess = new G4AntiProtonInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( theAntiNucleonData );
            theInelasticProcess->RegisterMe( theFTFModel0 );
            pmanager->AddDiscreteProcess( theInelasticProcess );
            // Absorption
            pmanager->AddRestProcess(new G4AntiProtonAbsorptionFritiof, ordDefault);
        }
        else if (particleName == G4Neutron::Definition()->GetParticleName()) {
            // elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess();
            theElasticProcess->AddDataSet(theXSreg->GetCrossSectionDataSet(G4ChipsNeutronElasticXS::Default_Name()));
            G4HadronElastic* elastic_neutronChipsModel = new G4ChipsElasticModel();
            elastic_neutronChipsModel->SetMinEnergy( theNeutronHPMax );
            theElasticProcess->RegisterMe( elastic_neutronChipsModel );
            G4NeutronHPElastic * theElasticNeutronHP = new G4NeutronHPElastic;
            theElasticNeutronHP->SetMinEnergy( UseNeutronTH ? theNeutronTHElasticMax : theNeutronMin );
            theElasticNeutronHP->SetMaxEnergy( theNeutronHPMax );
            theElasticProcess->RegisterMe( theElasticNeutronHP );
            theElasticProcess->AddDataSet( new G4NeutronHPElasticData );
#ifdef USE_NEUTRON_TH
            if (UseNeutronTH) {
                NeutronTHElastic* theElasticTHNeutron = new NeutronTHElastic();
                theElasticProcess->RegisterMe(theElasticTHNeutron);
                theElasticProcess->AddDataSet(new NeutronTHElasticData());
                theElasticTHNeutron->SetMinEnergy(theNeutronMin);
                theElasticTHNeutron->SetMaxEnergy(theNeutronTHElasticMax);
            }
#endif
            pmanager->AddDiscreteProcess( theElasticProcess );
            
            // inelastic scattering
            G4NeutronInelasticProcess* theInelasticProcess = new G4NeutronInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( new G4BGGNucleonInelasticXS( G4Neutron::Neutron() ) );
            theInelasticProcess->RegisterMe( theFTFModel1 );
            theInelasticProcess->RegisterMe( theBERTModel1 );
            G4NeutronHPInelastic * theNeutronInelasticHPModel = new G4NeutronHPInelastic;
            theNeutronInelasticHPModel->SetMinEnergy( theNeutronMin );
            theNeutronInelasticHPModel->SetMaxEnergy( theNeutronHPMax );
            theInelasticProcess->RegisterMe( theNeutronInelasticHPModel );
            theInelasticProcess->AddDataSet( new G4NeutronHPInelasticData );
            pmanager->AddDiscreteProcess(theInelasticProcess);
            
            // capture
            G4HadronCaptureProcess* theCaptureProcess = new G4HadronCaptureProcess;
            G4NeutronHPCapture * theNeutronHPCapture = new G4NeutronHPCapture();
            theNeutronHPCapture->SetMinEnergy( UseNeutronTH ? theNeutronTHCaptureMax : theNeutronMin );
            theNeutronHPCapture->SetMaxEnergy( theNeutronHPMax );
            theCaptureProcess->RegisterMe(theNeutronHPCapture);
            theCaptureProcess->AddDataSet( new G4NeutronHPCaptureData);
#ifdef USE_NEUTRON_TH
            if (UseNeutronTH) {
                NeutronTHCapture* theNeutronTHCaptureModel = new NeutronTHCapture();
                theCaptureProcess->RegisterMe(theNeutronTHCaptureModel);
                theCaptureProcess->AddDataSet(new NeutronTHCaptureData());
                theNeutronTHCaptureModel->SetMinEnergy(theNeutronMin);
                theNeutronTHCaptureModel->SetMaxEnergy(theNeutronTHCaptureMax);
            }
#endif
            pmanager->AddDiscreteProcess(theCaptureProcess);
        }
        else if (particleName == "anti_neutron") {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->RegisterMe( elastic_lhep0 );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering (include annihilation on-fly)
            G4AntiNeutronInelasticProcess* theInelasticProcess = new G4AntiNeutronInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( theAntiNucleonData );
            theInelasticProcess->RegisterMe( theFTFModel0 );
            pmanager->AddDiscreteProcess( theInelasticProcess );
        }
        else if (particleName == "deuteron") {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->RegisterMe( elastic_lhep0 );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4DeuteronInelasticProcess* theInelasticProcess = new G4DeuteronInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( theGGNuclNuclData );
            theInelasticProcess->RegisterMe( theFTFModel1 );
            theInelasticProcess->RegisterMe( theBERTModel0 );
            pmanager->AddDiscreteProcess( theInelasticProcess );
        }
        else if (particleName == "triton") {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->RegisterMe( elastic_lhep0 );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4TritonInelasticProcess* theInelasticProcess = new G4TritonInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( theGGNuclNuclData );
            theInelasticProcess->RegisterMe( theFTFModel1 );
            theInelasticProcess->RegisterMe( theBERTModel0 );
            pmanager->AddDiscreteProcess( theInelasticProcess );
        }
        else if (particleName == "alpha") {
            // Elastic scattering
            G4HadronElasticProcess* theElasticProcess = new G4HadronElasticProcess;
            theElasticProcess->RegisterMe( elastic_lhep0 );
            pmanager->AddDiscreteProcess( theElasticProcess );
            // Inelastic scattering
            G4AlphaInelasticProcess* theInelasticProcess = new G4AlphaInelasticProcess("inelastic");
            theInelasticProcess->AddDataSet( theGGNuclNuclData );
            theInelasticProcess->RegisterMe( theFTFModel1 );
            theInelasticProcess->RegisterMe( theBERTModel0 );
            pmanager->AddDiscreteProcess( theInelasticProcess );
        }
    }
}*/
