/*
 * HKMuonGenerator.hh
 *
 *  Created on: 11.03.2018
 *      Author: hkluck
 */

#ifndef SIMULATION_INCLUDE_HKMUONGENERATOR_HH_
#define SIMULATION_INCLUDE_HKMUONGENERATOR_HH_

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ThreeVector.hh"
#include "Randomize.hh"
#include "G4Types.hh"

class HKMuonGeneratorMessenger;
class G4Event;
class G4ParticleGun;

/**
 * @brief Generator for atmospheric muons
 *
 * @todo Consider rock overburden.
 * @todo Implement low energy correction to the muon spectrum
 *
 * Based on:
 * H. Kluck, 'Production Yield of Muon-Induced Neutrons in Lead', Springer 2015, doi:10.1007/978-3-319-18527-9
 * M. Horn, 'Simulations of the muon-induced neutron background of the EDELWEISS-II experiment for dark matter search', Universität Karlsruhe (TH) 2008
 */
class HKMuonGenerator: public G4VUserPrimaryGeneratorAction {

public:
	virtual ~HKMuonGenerator();

	/**
	 * @brief Generate primary vertex
	 *
	 * @param argEvent The actual event
	 */
	//Wrapper: pass the call to the actual generators
	virtual void GeneratePrimaries(G4Event * argEvent);

	/**
	 * @brief Return instance of the HKMuonGenerator
	 * @return Instance of HKMuonGenerator
	 */
	static HKMuonGenerator* GetInstance();

	void InitSource();
	void SetVerboseLevel(G4int myVerbosity);
	void SetEnergyMax(G4double eMax);
	void SetEnergyMin(G4double eMin);
	void SetNbOfEnergyBins(G4int eBins);
	void SetNbOfZenithBins(G4int zBins);
	void SetNbOfAzimuthBins(G4int aBins);
	void SetRadiusHemisphere(G4double radius);
	void SetRadiusSphere(G4double radius);

private:
	//Delete copy constructor and assignment operator because it is a singleton
	HKMuonGenerator(const HKMuonGenerator&) = delete;
	HKMuonGenerator& operator=(const HKMuonGenerator&) = delete;
	//Private constructor
	HKMuonGenerator();

	G4double calculateMuonFlux(G4double energy, G4double theta);
	void generateMuonDistribution();
	void getParameterToProbability(G4double prob);
	void getRandomMuon();
	void getStartPosition();

	//The messenger
	HKMuonGeneratorMessenger *TheMessenger;

	//Verbosity 0: Prints only a summary of the properties
	//Verbosity 1: Prints also every setting of the paramters
	//Verbosity 2: Prints progress information during the creation of the look-up table, every 10%
	//Verbosity 4: Prints each single entry of the look-up table (very slow!)
	G4int Verbosity;
	G4ParticleGun *myParticleGun;
	G4int PrintEvery;
	G4double (*bins)[4];
	G4int nbOfBins;
	G4bool generated;
	G4double energyMin;	  //Lower bound of the muon energy range
	G4double energyMax;	  //Upper bound of the muon energy range
	G4int eSteps;//Number of bins of the logarithmic energy scale: (Log(energyMax)-Log(energyMin))/eSteps
	G4int pSteps;	  //Number of bins along the azimuth direction
	G4int tSteps;	  //Number of bins along the zenith direction
	G4double radiusHemisphere;//Radius of the hemisphere where the muons start. Markus Horn used in his thesis a value of 30.0*m
	G4double radiusSphere;//The deviation of the muon start position on the surface of the outer hemisphere. A deviation of less or equal d metres lead to a sphere of radius d in the centre of the outer hemisphere which is homogeneous illuminated by muons. In Markus' thesis a value of 5*m was used.
	G4ThreeVector startPosition;
	G4double phi0;//Start direction of the muon, specified as azimuth (phi0) and zenith (theta0) angle
	G4double theta0;
	G4ThreeVector startDirection;
	G4double startEnergy;
	G4String particleID;
};

#endif /* SIMULATION_INCLUDE_HKMUONGENERATOR_HH_ */
