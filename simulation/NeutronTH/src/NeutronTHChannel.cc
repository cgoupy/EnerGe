#include <NeutronTHChannel.hh>
#include <NeutronTHFinalState.hh>

#include <globals.hh>
#include <G4HadTmpUtil.hh>
#include <G4SystemOfUnits.hh>
#include <G4NeutronHPThermalBoost.hh>

#include <algorithm>

G4double NeutronTHChannel::GetXsec(G4double energy)
{
  return std::max(0., theChannelData->GetXsec(energy));
}

G4double NeutronTHChannel::GetWeightedXsec(G4double energy, G4int isoNumber)
{
  return theIsotopeWiseData[isoNumber].GetXsec(energy);
}

G4double NeutronTHChannel::GetFSCrossSection(G4double energy, G4int isoNumber)
{
  return theFinalStates[isoNumber]->GetXsec(energy);
}

void NeutronTHChannel::Init(G4Element * anElement, const G4String dirName, const G4String aFSType)
{
  theFSType = aFSType;
  Init(anElement, dirName);
}

void NeutronTHChannel::Init(G4Element * anElement, const G4String dirName)
{
  theDir = dirName;
  theElement = anElement;
}

G4bool NeutronTHChannel::Register(NeutronTHFinalState *theFS)
{
  registerCount++;
  G4int Z = G4lrint(theElement->GetZ());
  if(registerCount<5) {
    Z = Z-registerCount;
  }
  if(Z==theElement->GetZ()-5) throw G4HadronicException(__FILE__, __LINE__, "Channel: Do not know what to do with this material");
  G4int count = 0;
  if(registerCount==0) count = theElement->GetNumberOfIsotopes();
  if(count == 0||registerCount!=0) count +=
      theStableOnes.GetNumberOfIsotopes(Z);
  niso = count;
  if(theIsotopeWiseData!=nullptr) delete [] theIsotopeWiseData;
  theIsotopeWiseData = new NeutronTHIsoData [niso];
  if(active!=nullptr) delete [] active;
  active = new G4bool[niso];
  if(theFinalStates!=nullptr) delete [] theFinalStates;
  theFinalStates = new NeutronTHFinalState * [niso];
  delete theChannelData;
  theChannelData = new G4NeutronHPVector;
  for(G4int i=0; i<niso; i++) {
    theFinalStates[i] = theFS->New();
  }
  count = 0;
  G4int nIsos = niso;
  if(theElement->GetNumberOfIsotopes()!=0&&registerCount==0) {
    for (G4int i1=0; i1<nIsos; i1++) {
      // G4cout <<" Init: normal case"<<G4endl;
      G4int A = theElement->GetIsotope(i1)->GetN();
      G4double frac = theElement->GetRelativeAbundanceVector()[i1]/perCent;
      theFinalStates[i1]->SetA_Z(A, Z);
      UpdateData(A, Z, count++, frac);
    }
  } else {
    G4int first = theStableOnes.GetFirstIsotope(Z);
    for(G4int i1=0; i1<theStableOnes.GetNumberOfIsotopes(Z); i1++) {
      G4int A = theStableOnes.GetIsotopeNucleonCount(first+i1);
      G4double frac = theStableOnes.GetAbundance(first+i1);
      theFinalStates[i1]->SetA_Z(A, Z);
      UpdateData(A, Z, count++, frac);
    }
  }
  G4bool result = HasDataInAnyFinalState();
  return result;
}

void NeutronTHChannel::UpdateData(G4int A, G4int Z, G4int index, G4double abundance)
{
  theFinalStates[index]->Init(A, Z, theDir, theFSType);
  if(!theFinalStates[index]->HasAnyData()) return; // nothing there for exactly this isotope.

  // the above has put the X-sec into the FS
  theBuffer = nullptr;
  if(theFinalStates[index]->HasXsec()) {
    theBuffer = theFinalStates[index]->GetXsec();
    theBuffer->Times(abundance/100.);
    theIsotopeWiseData[index].FillChannelData(theBuffer);
  } else {	// get data from CrossSection directory
    G4String tString = "/CrossSection/";
    active[index] = theIsotopeWiseData[index].Init(A, Z, abundance, theDir, tString);
    if(active[index]) theBuffer = theIsotopeWiseData[index].MakeChannelData();
  }
  if(theBuffer != nullptr) Harmonise(theChannelData, theBuffer);
}

void NeutronTHChannel::Harmonise(G4NeutronHPVector *& theStore, G4NeutronHPVector * theNew)
{
  G4int M=0, a=0, p=0, t=0;

  G4NeutronHPVector * theMerge = new G4NeutronHPVector;
  G4NeutronHPVector * anActive = theStore;
  G4NeutronHPVector * aPassive = theNew;
  G4NeutronHPVector * tmp = nullptr;

  while (a<anActive->GetVectorLength()&&p<aPassive->GetVectorLength()) {
    if(anActive->GetEnergy(a) <= aPassive->GetEnergy(p)) {
      G4double xa  = anActive->GetEnergy(a);
      theMerge->SetData(M, xa, anActive->GetXsec(a)+std::max(0., aPassive->GetXsec(xa)) );
      M++;
      a++;
      G4double xp = aPassive->GetEnergy(p);
      if( std::abs(std::abs(xp-xa)/xa)<0.001 ) {
        p++;
      }
    } else {
      tmp = anActive;
      t=a;
      anActive = aPassive;
      a=p;
      aPassive = tmp;
      p=t;
    }
  }
  while (a!=anActive->GetVectorLength()) {
    theMerge->SetData(M++, anActive->GetEnergy(a), anActive->GetXsec(a));
    a++;
  }
  while (p!=aPassive->GetVectorLength()) {
    if(std::abs(theMerge->GetEnergy(std::max(0,M-1))-aPassive->GetEnergy(p))/aPassive->GetEnergy(p)>0.001)
      theMerge->SetData(M++, aPassive->GetEnergy(p), aPassive->GetXsec(p));
    p++;
  }
  delete theStore;
  theStore = theMerge;
}


G4HadFinalState * NeutronTHChannel::ApplyYourself(const G4HadProjectile & theTrack, G4int anIsotope)
{
  if(anIsotope != -1) return theFinalStates[anIsotope]->ApplyYourself(theTrack);
  G4double sum=0;
  G4int it=0;
  G4double * xsec = new G4double[niso];
  G4NeutronHPThermalBoost aThermalE;
  for (G4int i=0; i<niso; i++) {
    if(theFinalStates[i]->HasAnyData()) {

      xsec[i] = theIsotopeWiseData[i].GetXsec(aThermalE.GetThermalEnergy(theTrack,
                                              theFinalStates[i]->GetN(),
                                              theFinalStates[i]->GetZ(),
                                              theTrack.GetMaterial()->GetTemperature()));
      sum += xsec[i];
    } else {
      xsec[i]=0;
    }
  }
  if(sum == 0) {
    it = static_cast<G4int>(niso*G4UniformRand());
  } else {
    G4double random = G4UniformRand();
    G4double running=0;
    for (G4int ix=0; ix<niso; ix++) {
      running += xsec[ix];
      if(random<=running/sum) {
        it = ix;
        break;
      }
    }
    if(it==niso) it--;
  }
  delete [] xsec;
  G4HadFinalState * theFinalState=nullptr;
  while(theFinalState==nullptr) {
    theFinalState = theFinalStates[it]->ApplyYourself(theTrack);
  }
  return theFinalState;
}

