#include <NeutronTHElasticData.hh>
#include <NeutronTHData.hh>

#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <G4ElementTable.hh>
#include <G4Nucleus.hh>
#include <G4NucleiProperties.hh>
#include <G4Neutron.hh>
#include <G4Electron.hh>

G4bool NeutronTHElasticData::IsApplicable(const G4DynamicParticle*aP, const G4Element*)
{
  if ( aP->GetKineticEnergy()>4.0e-6*MeV
       or aP->GetDefinition()!=G4Neutron::Neutron()
     ) {
    return false;
  }
  return true;
}

NeutronTHElasticData::NeutronTHElasticData()
{
  theCrossSections = nullptr;
  BuildPhysicsTable(*G4Neutron::Neutron());
}

NeutronTHElasticData::~NeutronTHElasticData()
{
  if ( theCrossSections != nullptr )
    theCrossSections->clearAndDestroy();
  delete theCrossSections;
}

void NeutronTHElasticData::BuildPhysicsTable(const G4ParticleDefinition& aP)
{
  if(&aP!=G4Neutron::Neutron())
    throw G4HadronicException(__FILE__, __LINE__, "Attempt to use NeutronHP data for particles other than neutrons!!!");
  size_t numberOfElements = G4Element::GetNumberOfElements();
  if(theCrossSections == nullptr) theCrossSections = new G4PhysicsTable( numberOfElements );

  // make a PhysicsVector for each element

  static const G4ElementTable *theElementTable = G4Element::GetElementTable();
  for( size_t i=0; i<numberOfElements; ++i ) {
    G4PhysicsVector* physVec = NeutronTHData::
                               Instance()->MakePhysicsVector((*theElementTable)[i], this);
    theCrossSections->push_back(physVec);
  }
}

void NeutronTHElasticData::DumpPhysicsTable(const G4ParticleDefinition& )
{
  G4cout << "Not implemented" << G4endl;
}

G4double NeutronTHElasticData::GetCrossSection(const G4DynamicParticle* aP, const G4Element*anE, G4double aT)
{
  G4double result = 0;
  G4bool outOfRange;
  G4int index = anE->GetIndex();

  // prepare neutron
  G4double eKinetic = aP->GetKineticEnergy();
  G4ReactionProduct theNeutron( aP->GetDefinition() );
  theNeutron.SetMomentum( aP->GetMomentum() );
  theNeutron.SetKineticEnergy( eKinetic );

  // prepare thermal nucleus
  G4Nucleus aNuc;
  G4double eps = 0.0001;
  G4double theA = anE->GetN();
  G4double theZ = anE->GetZ();
  G4double eleMass;

  eleMass = ( G4NucleiProperties::GetNuclearMass(static_cast<G4int>(theA+eps), static_cast<G4int>(theZ+eps))
            ) / G4Neutron::Neutron()->GetPDGMass();

  G4ReactionProduct boosted;
  G4double 	aXsection;
  G4double aXsection_bound = 81.325e-22;
  G4double	Meff;


  if(theZ == 1.0) {
    // Dodecane
    const G4int	nparam = 4;
    G4double	freq[nparam] = {9.0e-08,1.5e-07,1.8e-07,3.6e-07};
    G4double	sigma[nparam] = {6.5e-09,6.5e-09,3.0e-08,1.0e-08};
    G4double	mass[nparam] = {6.54,121.86,1.90,3.5};
    G4double	value1[nparam],
              value2[nparam],
              value3[nparam],
              value4[nparam],
              value5[nparam],
              value6[nparam],
              value7[nparam],
              value8[nparam];

    G4double	n_l[nparam];
    G4double	E_l[nparam];
    G4double	k_b_T, k_b_tau;
    G4double	zed;
    G4double	es;
    G4double	Ge;
    G4double	A;
    G4double	Lambda1[nparam], Lambda2[nparam];
    G4double	zed1[nparam], zed2[nparam];

    G4double	Eeff1[nparam], Eeff2[nparam];

    for(int i=0; i<nparam; i++) {
      value1[i] = freq[i]/sigma[i];
      value2[i] = std::exp((-1.0)*value1[i]*value1[i]);
      value3[i] = erf(value1[i]);
      value4[i] = value2[i] + (value1[i]*pi)*(1.0+value3[i]);
      value5[i] = (eKinetic - freq[i])/sigma[i];
      value6[i] = std::exp((-1.0)*value5[i]*value5[i]);
      value7[i] = erf((-1.0)*value5[i]);
      value8[i] = value6[i] + (value1[i]*pi)*(1.0+value7[i]);
      value8[i] /= value4[i];
    }

    for(int i=0; i<nparam; i++) {
      n_l[i] = 1.0/(std::exp(freq[i]/(k_Boltzmann*aT))-1.0);
      E_l[i] = freq[i]*(n_l[i]+0.5);
    }

    Meff = 1.0/eleMass;
    for(int i=0; i<nparam; i++) {
      Meff -= value8[i]/mass[i];
    }
    Meff = 1.0/Meff;

    Ge = 1.0;
    k_b_T = k_Boltzmann*aT;

    for(G4int i=0; i<nparam; i++) {
      Ge += value8[i]*(2.0*n_l[i]+1.0)/(mass[i]*freq[i]);
      k_b_T += eleMass*((E_l[i]-k_Boltzmann*aT)/mass[i]);
    }

    k_b_tau = Meff*(k_b_T/eleMass);

    for(G4int i=0; i<nparam; i++) {
      k_b_tau -= Meff*(value8[i]*E_l[i]/mass[i]);
    }


    zed = 	Meff*eKinetic/k_b_tau;
    es = 1.0/(1.0 + (1+Meff)*(1+Meff)/(4.0*Ge*Meff*k_b_tau));

    aXsection = aXsection_bound*(((1.0-erfc(std::sqrt(zed)))-std::sqrt(1.0-es)*std::exp((-1.0)*zed*es)*(1.0-
                                  erfc(std::sqrt(zed)*std::sqrt(1.0-es)))));
    aXsection /= (4.0*Ge*eKinetic);

    A = Meff*k_b_tau*((1.0-es)/(1+Meff))*((1.0-es)/(1+Meff));

    for(G4int i=0; i<nparam; i++) {
      zed1[i] = Meff*(eKinetic+freq[i]-sigma[i])/k_b_tau;
      zed2[i] = Meff*(eKinetic-freq[i]+sigma[i])/k_b_tau;

      Eeff1[i] = std::sqrt(1.0 + (freq[i]-sigma[i])/(eKinetic));

      Lambda1[i] = aXsection_bound*(erf(std::sqrt(zed1[i]))/(4.0*Ge) -
                                    A*std::sqrt(zed1[i]/pi)*std::exp(-zed1[i]) -
                                    std::sqrt(1.0-es)*std::exp(-zed1[i]*es)* erf(std::sqrt(zed1[i]*(1.0-es))) *
                                    (1.0/(4.0*Ge) + A*zed1[i] + A/(2.0*(1.0-es))))/((eKinetic+freq[i]-sigma[i])*Ge);

      if((freq[i]-sigma[i])/eKinetic <= 1.0) {
        Eeff2[i] = std::sqrt(1.0 - (freq[i]-sigma[i])/(eKinetic));

        Lambda2[i] = aXsection_bound*(erf(std::sqrt(zed2[i]))/(4.0*Ge) -
                                      A*std::sqrt(zed2[i]/pi)*std::exp(-zed2[i]) -
                                      std::sqrt(1.0-es)*std::exp(-zed2[i]*es)* erf(std::sqrt(zed2[i]*(1.0-es))) *
                                      (1.0/(4.0*Ge) + A*zed2[i] + A/(2.0*(1.0-es))))/((eKinetic-freq[i]+sigma[i])*Ge);

      } else {
        Eeff2[i] = 0.0;
        Lambda2[i] = 0.0;
      }
    }

    for(G4int i=0; i<nparam; i++) {
      aXsection += value8[i]/(mass[i]*freq[i])*(n_l[i]*Eeff1[i]*Lambda1[i]+(1.0+n_l[i])*Eeff2[i]*Lambda2[i]);
    }

    result = aXsection;

    return result;
  }

  // MC integration loop
  G4int counter = 0;
  G4double buffer = 0;
  G4int size = G4int(std::max(10., aT/60*kelvin));
  G4ThreeVector neutronVelocity = 1./G4Neutron::Neutron()->GetPDGMass()*theNeutron.GetMomentum();
  G4double neutronVMag = neutronVelocity.mag();

  while(counter == 0 || std::abs(buffer-result/std::max(1,counter)) > 0.03*buffer) {
    if(counter) buffer = result/counter;
    while (counter<size) {
      counter ++;
      G4ReactionProduct aThermalNuc = aNuc.GetThermalNucleus(eleMass, aT);
      boosted.Lorentz(theNeutron, aThermalNuc);
      G4double theEkin = boosted.GetKineticEnergy();
      aXsection = (*((*theCrossSections)(index))).GetValue(theEkin, outOfRange);

      // velocity correction.
      G4ThreeVector targetVelocity = 1./aThermalNuc.GetMass()*aThermalNuc.GetMomentum();
      aXsection *= (targetVelocity-neutronVelocity).mag()/neutronVMag;
      result += aXsection;
    }
    size += size;
  }
  result /= counter;

  return result;
}
