#include <NeutronTHData.hh>
#include <G4LPhysicsFreeVector.hh>

NeutronTHData::NeutronTHData()
{
  numEle = G4Element::GetNumberOfElements();
  theData = new NeutronTHElementData[numEle];

  for (G4int i=0; i<numEle; i++) {
    theData[i].Init((*(G4Element::GetElementTable()))[i]);
  }
}

NeutronTHData::~NeutronTHData()
{
  delete [] theData;
}

NeutronTHData * NeutronTHData::Instance()
{
  static NeutronTHData theCrossSectionData;
  return &theCrossSectionData;
}

G4PhysicsVector * NeutronTHData::DoPhysicsVector(G4NeutronHPVector * theVector)
{

  G4int len = theVector->GetVectorLength();

  if(len==0) return new G4LPhysicsFreeVector(0, 0, 0);
  G4double emin = theVector->GetX(0);
  G4double emax = theVector->GetX(len-1);

  G4LPhysicsFreeVector * theResult = new G4LPhysicsFreeVector(len, emin, emax);
  for (G4int i=0; i<len; i++) {
    theResult->PutValues(i, theVector->GetX(i), theVector->GetY(i));
  }
  return theResult;
}
