#include <NeutronTHCapture.hh>
#include <NeutronTHCaptureFS.hh>

#include <G4SystemOfUnits.hh>
#include <G4NeutronHPThermalBoost.hh>
#include <G4NeutronHPDeExGammas.hh>
#include <G4ParticleTable.hh>
#include <G4IonTable.hh>

NeutronTHCapture::NeutronTHCapture(): G4HadronicInteraction("NeutronTHCapture")
{
  SetMinEnergy(0.0);
  SetMaxEnergy(0.0003*MeV);

  if(!getenv("G4NEUTRONHPDATA"))
    throw G4HadronicException(__FILE__, __LINE__, "Please setenv G4NEUTRONHPDATA to point to the neutron cross-section files.");
  dirName = getenv("G4NEUTRONHPDATA");
  G4String tString = "/Capture/";
  dirName = dirName + tString;
  numEle = G4Element::GetNumberOfElements();

  theCapture = new NeutronTHChannel[numEle];

  NeutronTHCaptureFS * theFS = new NeutronTHCaptureFS;
  for (G4int i=0; i<numEle; i++) {
    theCapture[i].Init((*(G4Element::GetElementTable()))[i], dirName);
    theCapture[i].Register(theFS);
  }
  delete theFS;
}

NeutronTHCapture::~NeutronTHCapture()
{
  delete [] theCapture;
}

G4HadFinalState * NeutronTHCapture::ApplyYourself(const G4HadProjectile& aTrack, G4Nucleus& )
{
  const G4Material * theMaterial = aTrack.GetMaterial();
  G4int n = theMaterial->GetNumberOfElements();
  G4int index = theMaterial->GetElement(0)->GetIndex();
  if(n!=1) {
    xSec = new G4double[n];
    G4double sum=0;
    G4int i;
    const G4double * NumAtomsPerVolume = theMaterial->GetVecNbOfAtomsPerVolume();
    G4double rWeight;
    G4NeutronHPThermalBoost aThermalE;
    for (i=0; i<n; i++) {
      index = theMaterial->GetElement(i)->GetIndex();
      rWeight = NumAtomsPerVolume[i];
      xSec[i] = theCapture[index].GetXsec(aThermalE.GetThermalEnergy(aTrack,
                                          theMaterial->GetElement(i),
                                          theMaterial->GetTemperature()));
      xSec[i] *= rWeight;
      sum+=xSec[i];
    }
    G4double random = G4UniformRand();
    G4double running = 0;
    for (i=0; i<n; i++) {
      running += xSec[i];
      index = theMaterial->GetElement(i)->GetIndex();
      if(random<=running/sum) break;
    }
    if(i==n) i=std::max(0, n-1);
    delete [] xSec;
  }
  return theCapture[index].ApplyYourself(aTrack);
}
