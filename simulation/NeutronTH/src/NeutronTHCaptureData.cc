#include <NeutronTHCaptureData.hh>
#include <NeutronTHData.hh>

#include <G4Neutron.hh>
#include <G4ElementTable.hh>
#include <G4SystemOfUnits.hh>


G4bool NeutronTHCaptureData::IsApplicable(const G4DynamicParticle*aP, const G4Element*)
{
  G4bool result = true;
  G4double eKin = aP->GetKineticEnergy();
  if(eKin>0.0003*MeV||aP->GetDefinition()!=G4Neutron::Neutron()) result = false;
  return result;
}

NeutronTHCaptureData::NeutronTHCaptureData()
{
  BuildPhysicsTable(*G4Neutron::Neutron());
}

NeutronTHCaptureData::~NeutronTHCaptureData()
{
  delete theCrossSections;
}

void NeutronTHCaptureData::BuildPhysicsTable(const G4ParticleDefinition& aP)
{
  if(&aP!=G4Neutron::Neutron())
    throw G4HadronicException(__FILE__, __LINE__, "Attempt to use NeutronHP data for particles other than neutrons!!!");
  size_t numberOfElements = G4Element::GetNumberOfElements();

  theCrossSections = new G4PhysicsTable( numberOfElements );

  // make a PhysicsVector for each element

  static const G4ElementTable *theElementTable = G4Element::GetElementTable();
  for( size_t i=0; i<numberOfElements; ++i ) {
    if(getenv("CaptureDataIndexDebug")) {
      G4int index_debug = ((*theElementTable)[i])->GetIndex();
      G4cout << "IndexDebug "<< i <<" "<<index_debug<<G4endl;
    }
    G4PhysicsVector* physVec = NeutronTHData::
                               Instance()->MakePhysicsVector((*theElementTable)[i], this);
    theCrossSections->push_back(physVec);
  }
}

void NeutronTHCaptureData::DumpPhysicsTable(const G4ParticleDefinition& aP)
{
  if(&aP!=G4Neutron::Neutron())
    throw G4HadronicException(__FILE__, __LINE__, "Attempt to use NeutronHP data for particles other than neutrons!!!");
  G4cout << "NeutronTHCaptureData::DumpPhysicsTable still to be implemented"<<G4endl;
}

#include <G4NucleiProperties.hh>

G4double NeutronTHCaptureData::
GetCrossSection(const G4DynamicParticle* aP, const G4Element*anE, G4double aT)
{
  G4double result = 0;
  G4bool outOfRange;
  G4int index = anE->GetIndex();

  // prepare neutron
  G4double eKinetic = aP->GetKineticEnergy();
  G4ReactionProduct theNeutron( aP->GetDefinition() );
  theNeutron.SetMomentum( aP->GetMomentum() );
  theNeutron.SetKineticEnergy( eKinetic );

  // prepare thermal nucleus
  G4Nucleus aNuc;
  G4double eps = 0.0001;
  G4double theA = anE->GetN();
  G4double theZ = anE->GetZ();
  G4double eleMass;

  eleMass = G4NucleiProperties::GetNuclearMass( static_cast<G4int>(theA+eps) , static_cast<G4int>(theZ+eps) ) / G4Neutron::Neutron()->GetPDGMass();

  G4ReactionProduct boosted;
  G4double aXsection;

  // MC integration loop
  G4int counter = 0;
  G4double buffer = 0;
  G4int size = G4int(std::max(10., aT/60*kelvin));
  G4ThreeVector neutronVelocity = 1./G4Neutron::Neutron()->GetPDGMass()*theNeutron.GetMomentum();
  G4double neutronVMag = neutronVelocity.mag();
  while(counter == 0 || std::abs(buffer-result/counter) > 0.03*buffer) {
    if(counter) buffer = result/counter;
    while (counter<size) {
      counter ++;
      G4ReactionProduct aThermalNuc = aNuc.GetThermalNucleus(eleMass, aT);
      boosted.Lorentz(theNeutron, aThermalNuc);
      G4double theEkin = boosted.GetKineticEnergy();
      aXsection = (*((*theCrossSections)(index))).GetValue(theEkin, outOfRange);
      // velocity correction, or luminosity factor...
      G4ThreeVector targetVelocity = 1./aThermalNuc.GetMass()*aThermalNuc.GetMomentum();
      aXsection *= (targetVelocity-neutronVelocity).mag()/neutronVMag;
      result += aXsection;
    }
    size += size;
  }
  result /= counter;
  return result;
}
