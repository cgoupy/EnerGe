//
// Class ContinuumTHGammaDeexcitation.cc
//
//       Concrete class derived from G4VGammaDeexcitation
//
//
#include <ContinuumTHGammaDeexcitation.hh>

#include <G4Gamma.hh>
#include <ContinuumTHGammaTransition.hh>
#include <NuclearTHLevelManager.hh>
#include <NuclearTHLevelStore.hh>
#include <G4Fragment.hh>
#include <G4ConstantLevelDensityParameter.hh>

G4VGammaTransition* ContinuumTHGammaDeexcitation::CreateTransition(G4Fragment* aNucleus)
{
  G4int Z = aNucleus->GetZ_asInt();
  G4int A = aNucleus->GetA_asInt();
  G4double excitation = aNucleus->GetExcitationEnergy();

  if (_nucleusA != A || _nucleusZ != Z) {
    _levelManager = NuclearTHLevelStore::GetInstance()->GetManager(Z,A);
    _nucleusA = A;
    _nucleusZ = Z;
  }

  if (_verbose > 1)
    G4cout << "ContinuumTHGammaDeexcitation::CreateTransition - Created" << G4endl;

  return new ContinuumTHGammaTransition(_levelManager,Z,A,excitation,_verbose );
}


G4bool ContinuumTHGammaDeexcitation::CanDoTransition(G4Fragment* aNucleus)
{
  G4bool canDo = true;

  if (_transition == 0) {
    canDo = false;

    if (_verbose > 0)
      G4cout << "ContinuumTHGammaDeexcitation::CanDoTransition - Null transition " << G4endl;
  }

  G4double excitation = aNucleus->GetExcitationEnergy();
  G4double A = aNucleus->GetA();
  G4double Z = aNucleus->GetZ();
  if (A <2 || Z<3) {
    canDo = false;
    if (_verbose > 0)
      G4cout << "ContinuumTHGammaDeexcitation::CanDoTransition - n/p/H"<< G4endl;
  }

  if (excitation <= 0.) {
    canDo = false;
    if (_verbose > 0)
      G4cout
          << "ContinuumTHGammaDeexcitation::CanDoTransition -  Excitation <= 0"
          << G4endl;
  }

  G4double theEMax = 0.0;
  if (_levelManager->NumberOfLevels() - 2 >= 0)
    theEMax = (_levelManager->MaxLevelEnergy())/4.0;
  else
    theEMax = _levelManager->MaxLevelEnergy();

  if (excitation <= theEMax) {
    canDo = false;
    if (_verbose > 0)
      G4cout << "ContinuumTHGammaDeexcitation::CanDoTransition -  Excitation "
             << excitation << " below max discrete level "
             << _levelManager->MaxLevelEnergy() << G4endl;
  }

  if (canDo) {
    if (_verbose > 1)
      G4cout <<"ContinuumTHGammaDeexcitation::CanDoTransition - CanDo"
             << G4endl;
  }

  return canDo;

}

