#include <NeutronTHElementData.hh>

#include <G4SystemOfUnits.hh>

#include <algorithm>
#include <utility>

NeutronTHElementData::NeutronTHElementData()
{
  precision = 0.02;
  theCaptureData = new G4NeutronHPVector;
  theElasticData = new G4NeutronHPVector;
  theIsotopeWiseData = nullptr;
}

NeutronTHElementData::~NeutronTHElementData()
{
  if(theCaptureData!=nullptr) delete theCaptureData;
  if(theElasticData!=nullptr) delete theElasticData;
  if(theIsotopeWiseData!=nullptr) delete [] theIsotopeWiseData;
}

void NeutronTHElementData::Init(G4Element * theElement)
{
  G4int count = theElement->GetNumberOfIsotopes();
  if(count == 0) { count += theStableOnes.GetNumberOfIsotopes(static_cast<G4int>(theElement->GetZ())); }
  theIsotopeWiseData = new NeutronTHIsoData[count];

  count = 0;
  G4int nIso = theElement->GetNumberOfIsotopes();
  G4int Z = static_cast<G4int> (theElement->GetZ());
  if(nIso!=0) {
    for (G4int i1=0; i1<nIso; i1++) {
      G4int A = theElement->GetIsotope(i1)->GetN();
      G4double frac = theElement->GetRelativeAbundanceVector()[i1]/perCent;
      UpdateData(A, Z, count++, frac);
    }
  } else {
    G4int first = theStableOnes.GetFirstIsotope(Z);
    for(G4int i1=0; i1<theStableOnes.GetNumberOfIsotopes(static_cast<G4int>(theElement->GetZ()) ); i1++) {
      G4int A = theStableOnes.GetIsotopeNucleonCount(first+i1);
      G4double frac = theStableOnes.GetAbundance(first+i1);

      UpdateData(A, Z, count++, frac);
    }
  }
  theElasticData->ThinOut(precision);
  theCaptureData->ThinOut(precision);
}

void NeutronTHElementData::UpdateData(G4int A, G4int Z, G4int index, G4double abundance)
{
  //Reads in the Data, using G4NeutronHPIsoData[], and its Init
  theIsotopeWiseData[index].Init(A, Z, abundance);

  theBuffer = theIsotopeWiseData[index].MakeElasticData();
  Harmonise(theElasticData, theBuffer);
  delete theBuffer;

  theBuffer = theIsotopeWiseData[index].MakeCaptureData();
  Harmonise(theCaptureData, theBuffer);
  delete theBuffer;
}

void NeutronTHElementData::Harmonise(G4NeutronHPVector *& theStore, G4NeutronHPVector * theNew)
{
  if(theNew == nullptr) { return; }
  G4int a=0, b=0;
  G4NeutronHPVector * theMerge = new G4NeutronHPVector(theStore->GetVectorLength());
  while ( theStore->GetEnergy(a)<theNew->GetEnergy(0)&&a<theStore->GetVectorLength() ) {
    theMerge->SetData(b++, theStore->GetEnergy(a), theStore->GetXsec(a));
    a++;
  }

  G4NeutronHPVector *active = theStore;
  G4NeutronHPVector * passive = theNew;

  G4int a2 = a, n = 0;

  while (a2<active->GetVectorLength()&&n<passive->GetVectorLength()) {
    if(active->GetEnergy(a2) <= passive->GetEnergy(n)) {
      theMerge->SetData(b, active->GetEnergy(a2), active->GetXsec(a2));
      G4double x  = theMerge->GetEnergy(b);
      G4double y = std::max(0., passive->GetXsec(x));
      theMerge->SetData(b, x, theMerge->GetXsec(b)+y);
            b++;
      a2++;
    } else {
      std::swap(active,passive);
      std::swap(a2,n);
    }
  }

  while (a2!=active->GetVectorLength()) {
    theMerge->SetData(b++, active->GetEnergy(a2), active->GetXsec(a2));
    a2++;
  }

  while (n!=passive->GetVectorLength()) {
    theMerge->SetData(b++, passive->GetEnergy(n), passive->GetXsec(n));
        n++;
  }

  delete theStore;
  theStore = theMerge;
}

G4NeutronHPVector * NeutronTHElementData::MakePhysicsVector(G4Element * theElement,
    G4ParticleDefinition * theP,
    NeutronTHCaptureData * theSet)
{
  if(theP != G4Neutron::Neutron()) throw G4HadronicException(__FILE__, __LINE__, "not a neutron");
  Init ( theElement );
  return GetData(theSet);
}

G4NeutronHPVector * NeutronTHElementData::MakePhysicsVector(G4Element * theElement,
    G4ParticleDefinition * theP,
    NeutronTHElasticData * theSet)
{
  if(theP != G4Neutron::Neutron()) throw G4HadronicException(__FILE__, __LINE__, "not a neutron");
  Init ( theElement );
  return GetData(theSet);
}
