#include <PhotonTHEvaporation.hh>

#include <globals.hh>
#include <Randomize.hh>
#include <G4Gamma.hh>
#include <G4LorentzVector.hh>
#include <G4VGammaTransition.hh>
#include <G4Fragment.hh>
#include <G4FragmentVector.hh>
#include <ContinuumTHGammaDeexcitation.hh>
#include <DiscreteTHGammaDeexcitation.hh>
#include <G4E1Probability.hh>
#include <G4AtomicDeexcitation.hh>

#include <NuclearTHLevelManager.hh>
#include <NuclearTHLevelStore.hh>

PhotonTHEvaporation::PhotonTHEvaporation()
{
  probAlgorithm = new G4E1Probability;
  discrDeexcitation = new DiscreteTHGammaDeexcitation();
  contDeexcitation = new ContinuumTHGammaDeexcitation();
  discrDeexcitation->SetICM(false);
}


PhotonTHEvaporation::~PhotonTHEvaporation()
{
  if(myOwnProbAlgorithm) delete probAlgorithm;
  delete discrDeexcitation;
  delete contDeexcitation;
}


void PhotonTHEvaporation::Initialize(const G4Fragment& fragment)
{
  Nucleus = fragment;
}


G4FragmentVector* PhotonTHEvaporation::BreakUp(const G4Fragment& nucleus)
{
  Nucleus = nucleus;
  G4FragmentVector* products = new G4FragmentVector();

  // Do one photon emission
  // Products from continuum gamma transitions

  G4FragmentVector* contProducts = new G4FragmentVector();
  contDeexcitation->DoChain(contProducts, &Nucleus);

  if (contProducts->size() > 0) {
    for (auto i = contProducts->begin(); i != contProducts->end(); i++) {
      products->push_back(*i);
    }
  } else {
    // Products from discrete gamma transitions
    G4FragmentVector* discrProducts = new G4FragmentVector();
    discrDeexcitation->DoChain(discrProducts, &Nucleus);

    if (verbose > 0)
      G4cout << " = BreakUp = " << discrProducts->size()
             << " gammas from DiscreteDeexcitation " << G4endl;

    for (auto i = discrProducts->begin(); i != discrProducts->end(); i++) {
      products->push_back(*i);
    }
    discrProducts->clear();
    delete discrProducts;
  }

  gammaE = 0.;
  if (products->size() > 0) {
    gammaE = (*(products->begin()))->GetMomentum().e();
  }

  contProducts->clear();
  delete contProducts;  // delete vector, not fragments

  // Add deexcited nucleus to products
  G4Fragment* finalNucleus = new G4Fragment(Nucleus);
  products->push_back(finalNucleus);

  if (verbose > 0)
    G4cout << "*-*-*-* Photon evaporation: " << products->size() << G4endl;

  return products;
}


G4FragmentVector* PhotonTHEvaporation::BreakItUp(const G4Fragment& nucleus)
{
  Nucleus = nucleus;

  G4FragmentVector* products = new G4FragmentVector();
  G4FragmentVector* contProducts = new G4FragmentVector();
  G4FragmentVector* discrProducts = new G4FragmentVector();

  // Do the whole gamma chain
  if(G4UniformRand() > NuclearTHLevelStore::GetInstance()->GetManager(nucleus.GetZ_asInt(),nucleus.GetA_asInt())->DiscreteProb()/100.0) {
    contDeexcitation->DoChain(contProducts, &Nucleus);
  }

  // Products from continuum gamma transitions
  if (verbose > 0)
    G4cout << " = BreakItUp = " << contProducts->size()
           << " gammas from ContinuumDeexcitation " << ", Z=" << nucleus.GetZ() << ", A=" << nucleus.GetA() << G4endl;

  if (contProducts->size() > 0) {
    for (auto i = contProducts->begin(); i != contProducts->end(); i++) {
      products->push_back(*i);
    }
  }

  // Products from discrete gamma transitions
  discrDeexcitation->DoChain(discrProducts, &Nucleus);

  eOccupancy = discrDeexcitation->GetEO();
  vShellNumber = discrDeexcitation->GetVacantSN();
  // eOccupancy.DumpInfo() ;
  discrDeexcitation->SetVaccantSN(-1);

  if (verbose > 0)
    G4cout << " = BreakItUp = " << discrProducts->size()
           << " gammas from DiscreteDeexcitation " << ", Z=" << nucleus.GetZ() << ", A=" << nucleus.GetA() << G4endl;


  for (auto i = discrProducts->begin(); i != discrProducts->end(); i++) {
    products->push_back(*i);
  }

  // Add deexcited nucleus to products
  G4Fragment* finalNucleus = new G4Fragment(Nucleus);
  products->push_back(finalNucleus);
  if (verbose > 0)
    G4cout << " = BreakItUp = Nucleus added to products" << G4endl;

  if (verbose > 0)
    G4cout << "*-*-* Photon evaporation: " << products->size() << G4endl;

  if(contProducts) {
    contProducts->clear();
    delete contProducts;  // delete vector, not fragments
  }
  if(discrProducts) {
    discrProducts->clear();
    delete discrProducts;
  }
  return products;
}


G4double PhotonTHEvaporation::GetEmissionProbability(G4Fragment* theNucleus)
{
  if (probAlgorithm) return probAlgorithm->EmissionProbability(*theNucleus,gammaE);
  return 0.;
}


void PhotonTHEvaporation::SetEmissionStrategy(G4VEmissionProbability * probAlgorithm_)
{
  if(myOwnProbAlgorithm) delete probAlgorithm;

  probAlgorithm = probAlgorithm_;

  myOwnProbAlgorithm = false;
}


void PhotonTHEvaporation::SetVerboseLevel(G4int verbose_)
{
  verbose = verbose_;
  contDeexcitation->SetVerboseLevel(verbose);
  discrDeexcitation->SetVerboseLevel(verbose);
}

void PhotonTHEvaporation::SetICM(G4bool ic)
{
  discrDeexcitation->SetICM(ic);
}

void PhotonTHEvaporation::SetMaxHalfLife(G4double hl)
{
  discrDeexcitation->SetHL(hl);
}

void PhotonTHEvaporation::RDMForced(G4bool fromRDM)
{
  discrDeexcitation->SetRDM(fromRDM);
}

void PhotonTHEvaporation::SetEOccupancy(G4ElectronOccupancy eo)
{
  discrDeexcitation->SetEO(eo);
}
