#include <NuclearTHLevelStore.hh>
#include <sstream>

std::map<G4String,NuclearTHLevelManager*> NuclearTHLevelStore::theManagers;
G4String NuclearTHLevelStore::dirName("");
G4String NuclearTHLevelStore::dirName2("");

NuclearTHLevelStore* NuclearTHLevelStore::GetInstance()
{
  static NuclearTHLevelStore theInstance;
  return &theInstance;
}

NuclearTHLevelStore::NuclearTHLevelStore()
{
  char* env = getenv("TH_NEUTRON_GAMMA_DATA");
  if (env == 0) 	{
    G4cout << "G4NuclarLevelStore: please set the  environment variable TH_NEUTRON_GAMMA_DATA\n";
    dirName = "NeutronTH/CaptureGammaTH/";
  } else {
    dirName = env;
    dirName += '/';
  }

  env = getenv("G4LEVELGAMMADATA");
  if (env == 0) 	{
    G4cout << "G4NuclarLevelStore: please set the G4LEVELGAMMADATA environment variable\n";
    dirName2 = "";
  } else {
    dirName2 = env;
    dirName2 += '/';
  }
}


NuclearTHLevelStore::~NuclearTHLevelStore()
{
  std::map<G4String,NuclearTHLevelManager*>::iterator i;
  for (i = theManagers.begin(); i != theManagers.end(); ++i) {
    if ( (*i).second ) delete (*i).second;
  }
}

G4String NuclearTHLevelStore::GenerateKey(const G4int Z, const G4int A)
{
  std::ostringstream streamName;
  streamName << 'z' << Z << ".a" << A;
  G4String name(streamName.str());
  return name;
}


NuclearTHLevelManager* NuclearTHLevelStore::GetManager(const G4int Z, const G4int A)
{
  NuclearTHLevelManager * result = 0;
  if (A < 1 || Z < 1 || A < Z) {
    G4cerr << "NuclearTHLevelStore::GetManager: Wrong values Z = " << Z
           << " A = " << A << '\n';
    return result;
  }
  // Generate the key = filename
  G4String key(this->GenerateKey(Z,A));

  // Check if already exists that key
  std::map<G4String,NuclearTHLevelManager*>::iterator idx = theManagers.find(key);
  // If doesn't exists then create it
  if ( idx == theManagers.end() ) {
    result = new NuclearTHLevelManager();
    if(!(result->SetNucleus(Z,A,dirName + key)))
      result->SetNucleus(Z,A,dirName2 + key);
    theManagers.insert(std::make_pair(key,result));
  }
  // But if it exists...
  else {
    result = idx->second;
  }

  return result;
}
