#include <NeutronTHIsoData.hh>

#include <G4NeutronHPDataUsed.hh>
#include <G4SystemOfUnits.hh>

G4bool NeutronTHIsoData::Init(G4int A, G4int Z, G4double abun, G4String dirName, G4String aFSType)
{
  theChannelData = nullptr;
  G4double abundance = abun/100.;
  G4String filename;
  G4bool result = true;
  G4NeutronHPDataUsed aFile = theNames.GetName(A, Z, dirName, aFSType, result);
  filename = aFile.GetName();

  std::ifstream theChannel(filename);

  if(Z==1 && (aFile.GetZ()!=Z || std::abs(aFile.GetA()-A)>0.0001) ) {
    if(getenv("NeutronHPNamesLogging")) G4cout << "Skipped = "<< filename <<" "<<A<<" "<<Z<<G4endl;
    theChannel.close();
    return false;
  }
  if(!theChannel) {
    theChannel.close();
    return false;
  }
  // accommodating deficiencie of some compilers
  if(theChannel.eof()) {
    theChannel.close();
    return false;
  }
  if(!theChannel) {
    theChannel.close();
    return false;
  }
  G4int dummy;
  theChannel >> dummy >> dummy;

  theChannelData = new G4NeutronHPVector;
  G4int nData;
  theChannel >> nData;
  theChannelData->Init(theChannel, nData, eV, abundance*barn);
  theChannel.close();
  return result;
}

void NeutronTHIsoData::Init(G4int A, G4int Z, G4double abun) //fill PhysicsVector for this Isotope
{
  G4String dirName;
  if(!getenv("G4NEUTRONHPDATA"))
    throw G4HadronicException(__FILE__, __LINE__, "Please setenv G4NEUTRONHPDATA to point to the neutron cross-section files.");
  G4String baseName = getenv("G4NEUTRONHPDATA");

  dirName = baseName+"/Capture";
  Init(A, Z, abun, dirName, "/CrossSection/");
  theCaptureData = theChannelData;
  theChannelData = nullptr;
  dirName = baseName+"/Elastic";
  Init(A, Z, abun, dirName, "/CrossSection/");
  theElasticData = theChannelData;
  theChannelData = nullptr;
}

G4String NeutronTHIsoData::GetName(G4int A, G4int Z, G4String base, G4String rest)
{
  G4bool dbool;
  return (theNames.GetName(A, Z, base, rest, dbool)).GetName();
}


