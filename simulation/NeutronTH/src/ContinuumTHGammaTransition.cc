//
//  Class ContinuumTHGammaTransition.cc
//

#include <ContinuumTHGammaTransition.hh>

#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <G4VLevelDensityParameter.hh>
#include <G4ConstantLevelDensityParameter.hh>
#include <G4PtrLevelVector.hh>

#include <CLHEP/Random/RandGeneral.h>

ContinuumTHGammaTransition::ContinuumTHGammaTransition(
  const NuclearTHLevelManager* levelManager,
  G4int Z, G4int A,
  G4double excitation_,
  G4int verbose_):
  verbose(verbose_), nucleusA(A), nucleusZ(Z),
  excitation(excitation_), LevelManager(levelManager)
{
  const G4PtrLevelVector* levels = levelManager->GetLevels();
  G4double eTolerance = 0.;
  if (levels != 0) {
    G4int lastButOne = levelManager->NumberOfLevels() - 2;
    if (lastButOne >= 0) {
      eTolerance = levelManager->MaxLevelEnergy() - levels->at(lastButOne)->Energy();
      if (eTolerance < 0.) eTolerance = 0.;
    }
  }

  eGamma = 0.;
  gammaCreationTime = 0.;

  maxLevelE = levelManager->MaxLevelEnergy() + eTolerance;
  minLevelE = levelManager->MinLevelEnergy();

  // Energy range for photon generation; upper limit is defined 5*Gamma(GDR) from GDR peak
  //  eMin = 0.001 * MeV;
  eMin = 1.0 * MeV;
  // Giant Dipole Resonance energy
  G4double energyGDR = (40.3 / std::pow(G4double(nucleusA),0.2) ) * MeV;
  // Giant Dipole Resonance width
  G4double widthGDR = 0.30 * energyGDR;
  // Extend
  G4double factor = 5;
  eMax = energyGDR + factor * widthGDR;
  if (eMax > excitation) eMax = excitation;

}


//
// Override GammaEnergy function from G4VGammaTransition
//
#include <G4RandomTools.hh>
void ContinuumTHGammaTransition::SelectGamma()
{

  eGamma = 0.;

  G4int nBins = 200;
  G4double sampleArray[200];
//  	G4int nBins = 5000;
//  	G4double sampleArray[5000];

  G4int i;

  for (i=0; i<nBins; i++) {
    G4double e = eMin + ( (eMax - eMin) / nBins) * i;
    sampleArray[i] = E1Pdf(e);

    if(verbose > 10)
      G4cout << "*---* G4ContinuumTransition: e = " << e
             << " pdf = " << sampleArray[i] << G4endl;
  }
  CLHEP::RandGeneral randGeneral(sampleArray, nBins);
  G4double random = randGeneral.shoot();

  eGamma = eMin + (eMax - eMin) * random;

  G4double finalExcitation = excitation - eGamma;

  if(verbose > 10)
    G4cout << "*---*---* G4ContinuumTransition: eGamma = " << eGamma
           << "   finalExcitation = " << finalExcitation
           << " random = " << random << G4endl;

  if(finalExcitation < minLevelE/2.0) {
    eGamma = excitation;
    finalExcitation = 0.;
  }

  if (finalExcitation < (LevelManager->MaxLevelEnergy())/3.6 && finalExcitation > 0.) 	{
    G4double levelE = LevelManager->NearestLevel(finalExcitation)->Energy();
    G4double diff = finalExcitation - levelE;
    eGamma = eGamma + diff;
  }

  gammaCreationTime = GammaTime();

  if(verbose > 10)
    G4cout << "*---*---* G4ContinuumTransition: gammaCreationTime = "
           << gammaCreationTime/second << G4endl;

  return;
}

G4double ContinuumTHGammaTransition::GetGammaEnergy()
{
  return eGamma;
}

G4double ContinuumTHGammaTransition::GetGammaCreationTime()
{
  return gammaCreationTime;
}

void ContinuumTHGammaTransition::SetEnergyFrom(const G4double energy)
{
  if (energy > 0.) excitation = energy;
  return;

}


G4double ContinuumTHGammaTransition::E1Pdf(G4double e)
{
  G4double theProb = 0.0;

  if( (excitation - e) < 0.0 || e < 0 || excitation < 0) return theProb;

  G4ConstantLevelDensityParameter ldPar;
  G4double aLevelDensityParam = ldPar.LevelDensityParameter(nucleusA,nucleusZ,excitation);

  G4double levelDensBef = std::exp(2.0*std::sqrt(aLevelDensityParam*excitation));
  G4double levelDensAft = std::exp(2.0*std::sqrt(aLevelDensityParam*(excitation - e)));
  levelDensBef /= std::pow(excitation,7.0/4.0);
  levelDensAft /= std::pow(excitation - e,7.0/4.0);

  if(verbose > 20)
    G4cout << nucleusA << " LevelDensityParameter = " <<  aLevelDensityParam
           << " Bef Aft " << levelDensBef << " " << levelDensAft << G4endl;

  // Now form the probability density

  // Define constants for the photoabsorption cross-section (the reverse
  // process of our de-excitation)

  //  G4double sigma0 = 2.5 * nucleusA * millibarn;
  G4double sigma0 = 2.5 * nucleusA;

  G4double Egdp = (40.3 / std::pow(G4double(nucleusA),0.2) )*MeV;
  G4double GammaR = 0.30 * Egdp;

  G4double normC = 1.0 / (pi * hbarc)*(pi * hbarc);

  G4double numerator = sigma0 * e*e * GammaR*GammaR;
  G4double denominator = (e*e - Egdp*Egdp)* (e*e - Egdp*Egdp) + GammaR*GammaR*e*e;
  //  if (denominator < 1.0e-9) denominator = 1.0e-9;

  G4double sigmaAbs = numerator/denominator ;

  if(verbose > 20)
    G4cout << ".. " << Egdp << " .. " << GammaR
           << " .. " << normC << " .. " << sigmaAbs
           << " .. " << e*e << " .. " << levelDensAft/levelDensBef
           << G4endl;

  //  theProb = normC * sigmaAbs * e*e * levelDensAft/levelDensBef;
  theProb =  sigmaAbs * e*e * levelDensAft/levelDensBef;

  return theProb;
}


G4double ContinuumTHGammaTransition::GammaTime()
{

  G4double GammaR = 0.30 * (40.3 / std::pow(G4double(nucleusA),0.2) )*MeV;
  G4double tau = hbar_Planck/GammaR;

  G4double tMin = 0;
  G4double tMax = 10.0 * tau;
  G4int nBins = 200;
  G4double sampleArray[200];

  for(G4int i = 0; i<nBins; i++) {
    G4double t = tMin + ((tMax-tMin)/nBins)*i;
    sampleArray[i] = (std::exp(-t/tau))/tau;
  }

  CLHEP::RandGeneral randGeneral(sampleArray, nBins);
  G4double random = randGeneral.shoot();

  G4double creationTime = tMin + (tMax - tMin) * random;

  return creationTime;
}
