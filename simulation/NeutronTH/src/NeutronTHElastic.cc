#include <NeutronTHElastic.hh>

#include <NeutronTHElasticFS.hh>
#include <NeutronTHChannel.hh>
#include <Math.hh>

#include <globals.hh>
#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <G4HadFinalState.hh>
#include <G4HadProjectile.hh>
#include <G4Nucleus.hh>
#include <G4Element.hh>
#include <G4ElementTable.hh>
#include <G4String.hh>
#include <G4NeutronHPThermalBoost.hh>

#include <cstdlib>

NeutronTHElastic::NeutronTHElastic(): G4HadronicInteraction("NeutronTHElastic")
{
  NeutronTHElasticFS * theFS = new NeutronTHElasticFS;
  if(!getenv("G4NEUTRONHPDATA")) {
    G4Exception("NeutronTHElastic::NeutronTHElastic","0",FatalException,
                "The environment variable G4NEUTRONHPDATA is not defined, it should point to the neutron cross-section folder");
  }
  dirName = getenv("G4NEUTRONHPDATA");
  dirName += "/Elastic/";

  numEle = G4Element::GetNumberOfElements();
  theElastic = new NeutronTHChannel[numEle];
  for (G4int i=0; i<numEle; i++) {
    theElastic[i].Init(G4Element::GetElementTable()->at(i), dirName);
    while( !theElastic[i].Register(theFS) ) {}
  }
  delete theFS;
  SetMinEnergy(0.*eV);
  SetMaxEnergy(4.0*eV);
}

NeutronTHElastic::~NeutronTHElastic()
{
  delete [] theElastic;
}


G4HadFinalState * NeutronTHElastic::ApplyYourself(const G4HadProjectile& aTrack, G4Nucleus& )
{
// Dodecane

  const G4int nparam = 4;
  G4double freq[nparam] = {9.0e-08,1.5e-07,1.8e-07,3.6e-07};
  G4double sigma[nparam] = {6.5e-09,6.5e-09,3.0e-08,1.0e-08};
  G4double mass[nparam] = {6.54,121.86,1.90,3.5};

  G4double value1[nparam],
           value2[nparam],
           value3[nparam],
           value4[nparam],
           value5[nparam],
           value6[nparam],
           value7[nparam],
           value8[nparam];
//   G4double value[nparam];

  G4double n_l[nparam];
  G4double E_l[nparam];
  G4double k_b_T, k_b_tau;
  G4double zed;
  G4double es;
  G4double Ge;
  G4double A;
  G4double Lambda1[nparam], Lambda2[nparam];
  G4double zed1[nparam], zed2[nparam];

  G4double Eeff1[nparam], Eeff2[nparam];

  G4double aXsection;
  G4double aXsection_bound = 81.325e-22;

  G4double eKinetic;
  G4double eleMass = 0.99917;
  G4double Meff;
  G4double aT;

  const G4Material * theMaterial = aTrack.GetMaterial();
  G4int n = theMaterial->GetNumberOfElements();
  G4int index = theMaterial->GetElement(0)->GetIndex();
  if(n!=1) {
    G4double sum=0;
    G4double rWeight;
    xSec = new G4double[n];
    const G4double * NumAtomsPerVolume = theMaterial->GetVecNbOfAtomsPerVolume();
    G4NeutronHPThermalBoost aThermalE;
    for (G4int i=0; i<n; i++) {
      index = theMaterial->GetElement(i)->GetIndex();
      rWeight = NumAtomsPerVolume[i];

      if(theMaterial->GetElement(i)->GetName() == "H") {
        eKinetic = aTrack.GetKineticEnergy();
        aT = theMaterial->GetTemperature();

        for(G4int j=0; j<nparam; j++) {
          value1[j] = freq[j]/sigma[j];
          value2[j] = std::exp((-1.0)*value1[j]*value1[j]);
          value3[j] = erf(value1[j]);
          value4[j] = value2[j] + (value1[j]*pi)*(1.0+value3[j]);
          value5[j] = (eKinetic - freq[j])/sigma[j];
          value6[j] = std::exp((-1.0)*value5[j]*value5[j]);
          value7[j] = erf((-1.0)*value5[j]);
          value8[j] = value6[j] + (value1[j]*pi)*(1.0+value7[j]);
          value8[j] /= value4[j];

// 	  value[j] = ( std::exp(-1.*Math::sq((eKinetic - freq[j])/sigma[j])) + (freq[j]/sigma[j]*pi ) * (1.+erf(-1.*(eKinetic - freq[j])/sigma[j])) )
// 	           / ( std::exp(-1.*Math::sq(freq[j]/sigma[j]))              + (freq[j]/sigma[j]*pi ) * (1.+erf(freq[j]/sigma[j])) );
        }

        for(G4int j=0; j<nparam; j++) {
          n_l[j] = 1.0/(std::exp(freq[j]/(k_Boltzmann*aT))-1.0);
          E_l[j] = freq[j]*(n_l[j]+0.5);
        }

        Meff = 1.0/eleMass;
        for (G4int j=0; j<nparam; j++) {
          Meff -= value8[j]/mass[j];
        }
        Meff = 1.0/Meff;

        Ge = 1.0;
        k_b_T = k_Boltzmann*aT;

        for(G4int j=0; j<nparam; j++) {
          Ge += value8[j]*(2.0*n_l[j]+1.0)/(mass[j]*freq[j]);
          k_b_T += eleMass*((E_l[j]-k_Boltzmann*aT)/mass[j]);
        }

        k_b_tau = Meff*(k_b_T/eleMass);

        for(G4int j=0; j<nparam; j++) {
          k_b_tau -= Meff*(value8[j]*E_l[j]/mass[j]);
        }

        zed =  Meff*eKinetic/k_b_tau;
        es = 1.0/(1.0 + (1+Meff)*(1+Meff)/(4.0*Ge*Meff*k_b_tau));

        aXsection = aXsection_bound * (((1.0-erfc(std::sqrt(zed)))-std::sqrt(1.0-es)*std::exp((-1.0)*zed*es)*(1.0-
                                        erfc(std::sqrt(zed)*std::sqrt(1.0-es)))));
        aXsection /= (4.0*Ge*eKinetic);

        A = Meff*k_b_tau*((1.0-es)/(1+Meff))*((1.0-es)/(1+Meff));

        for(G4int j=0; j<nparam; j++) {
          zed1[j] = Meff*(eKinetic+freq[j]-sigma[j])/k_b_tau;
          zed2[j] = Meff*(eKinetic-freq[j]+sigma[j])/k_b_tau;
          Eeff1[j] = std::sqrt(1.0 + (freq[j]-sigma[j])/(eKinetic));

          Lambda1[j] = aXsection_bound*(erf(std::sqrt(zed1[j]))/(4.0*Ge) -
                                        A*std::sqrt(zed1[j]/pi)*std::exp(-zed1[j]) -
                                        std::sqrt(1.0-es)*std::exp(-zed1[j]*es)* erf(std::sqrt(zed1[j]*(1.0-es))) *
                                        (1.0/(4.0*Ge) + A*zed1[j] + A/(2.0*(1.0-es))))/((eKinetic+freq[j]-sigma[j])*Ge);

          if ( (freq[j]-sigma[j])/eKinetic <= 1.0 ) {
            Eeff2[j] = std::sqrt(1.0 - (freq[j]-sigma[j])/(eKinetic));
            Lambda2[j] = aXsection_bound*(erf(std::sqrt(zed2[j]))/(4.0*Ge) -
                                          A*std::sqrt(zed2[j]/pi)*std::exp(-zed2[j]) -
                                          std::sqrt(1.0-es)*std::exp(-zed2[j]*es)* erf(std::sqrt(zed2[j]*(1.0-es))) *
                                          (1.0/(4.0*Ge) + A*zed2[j] + A/(2.0*(1.0-es))))/((eKinetic-freq[j]+sigma[j])*Ge);

          } else {
            Eeff2[j] = 0.0;
            Lambda2[j] = 0.0;
          }
        }

        for(G4int j=0; j<nparam; j++) {
          aXsection += value8[j]/(mass[j]*freq[j])*(n_l[j]*Eeff1[j]*Lambda1[j]+(1.0+n_l[j])*Eeff2[j]*Lambda2[j]);
        }

        xSec[i] = aXsection;
      } else {
        xSec[i] = theElastic[index].GetXsec(aThermalE.GetThermalEnergy(aTrack, theMaterial->GetElement(i), theMaterial->GetTemperature()));
      }
      xSec[i] *= rWeight;
      sum+=xSec[i];
    }
    G4double random = G4UniformRand();
    G4double running = 0;
    for (G4int i=0; i<n; i++) {
      running += xSec[i];
      index = theMaterial->GetElement(i)->GetIndex();
      if(random<=running/sum) break;
    }
    delete [] xSec;
    // it is element-wise initialised.
  }
  return theElastic[index].ApplyYourself(aTrack);
}
