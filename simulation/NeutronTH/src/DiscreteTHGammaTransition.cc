#include <DiscreteTHGammaTransition.hh>

#include <Randomize.hh>
#include <G4AtomicShells.hh>


void DiscreteTHGammaTransition::SelectGamma()
{
  GammaEnergy = 0.;
  G4int nGammas = Level.NumberOfGammas();
  if (nGammas > 0) {
    G4double random = G4UniformRand();

    G4int iGamma = 0;
    for(iGamma=0; iGamma < nGammas; iGamma++) {
      if(random <= (Level.GammaCumulativeProbabilities())[iGamma]) {
        break;
      }
    }

    // Small correction due to the fact that there are mismatches between
    // nominal level energies and emitted gamma energies

    G4double eCorrection = Level.Energy() - Excitation;
    GammaEnergy = (Level.GammaEnergies())[iGamma] - eCorrection;

    if (GammaEnergy < Level.Energy()*10e-5) GammaEnergy = Excitation;
    // now decide whether Internal Conversion electron should be emitted instead
    if (icm) {
      random = G4UniformRand() ;
      if ( random <= (Level.TotalConvertionProbabilities())[iGamma]*(Level.GammaWeights())[iGamma]
           /((Level.TotalConvertionProbabilities())[iGamma]*(Level.GammaWeights())[iGamma]+(Level.GammaWeights())[iGamma])) {
        G4int iShell = 9;
        random = G4UniformRand() ;
        if ( random <= (Level.KConvertionProbabilities())[iGamma]) { iShell = 0; }
        else if ( random <= (Level.L1ConvertionProbabilities())[iGamma]) { iShell = 1; }
        else if ( random <= (Level.L2ConvertionProbabilities())[iGamma]) { iShell = 2; }
        else if ( random <= (Level.L3ConvertionProbabilities())[iGamma]) { iShell = 3; }
        else if ( random <= (Level.M1ConvertionProbabilities())[iGamma]) { iShell = 4; }
        else if ( random <= (Level.M2ConvertionProbabilities())[iGamma]) { iShell = 5; }
        else if ( random <= (Level.M3ConvertionProbabilities())[iGamma]) { iShell = 6; }
        else if ( random <= (Level.M4ConvertionProbabilities())[iGamma]) { iShell = 7; }
        else if ( random <= (Level.M5ConvertionProbabilities())[iGamma]) { iShell = 8; }
        // the following is needed to match the ishell to that used in  G4AtomicShells
        if ( iShell == 9) {
          if ( (NucleusZ < 28) && (NucleusZ > 20)) {
            iShell--;
          } else if ( NucleusZ == 20 || NucleusZ == 19 ) {
            iShell = iShell -2;
          }
        }
        BondE = G4AtomicShells::GetBindingEnergy(NucleusZ, iShell);
        GammaEnergy = GammaEnergy - BondE;
        OrbitE = iShell;
        aGamma = false ;   // emitted is not a gamma now
      }
    }

    G4double tau = Level.HalfLife() / std::log(2.0);

    G4double tMin = 0;
    G4double tMax = 10.0 * tau;

    if (tau != 0 ) {
      random = G4UniformRand() ;
      GammaCreationTime = -(std::log(random*(std::exp(-tMax/tau) - std::exp(-tMin/tau)) + std::exp(-tMin/tau)));
    } else {
      GammaCreationTime=0.;
    }
  }
  return;
}
