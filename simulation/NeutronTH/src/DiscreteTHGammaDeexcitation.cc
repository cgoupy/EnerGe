#include <DiscreteTHGammaDeexcitation.hh>
#include <DiscreteTHGammaTransition.hh>

#include <NuclearTHLevelManager.hh>
#include <NuclearTHLevelStore.hh>

#include <G4ios.hh>
#include <G4SystemOfUnits.hh>

#include <fstream>
#include <sstream>


DiscreteTHGammaDeexcitation::DiscreteTHGammaDeexcitation():
  NucleusZ(0), NucleusA(0), Max_hl(1e-6*second), icm(false),
  rdm(false), LevelManager(0)
{
  Tolerance = 0.1 * MeV;
}

G4VGammaTransition* DiscreteTHGammaDeexcitation::CreateTransition(G4Fragment* nucleus)
{
  G4int A = static_cast<G4int>(nucleus->GetA());
  G4int Z = static_cast<G4int>(nucleus->GetZ());

  if (NucleusA != A || NucleusZ != Z) 	{
    NucleusA = A;
    NucleusZ = Z;
    LevelManager = NuclearTHLevelStore::GetInstance()->GetManager(Z,A);
  }

  if (LevelManager->IsValid()) {
    if (_verbose > 1) {
      G4cout << "DiscreteTHGammaDeexcitation::CreateTransition - (A,Z) is valid\n";
    }

    G4double excitation = nucleus->GetExcitationEnergy();
    //      const G4NuclearLevel* level =_levelManager.NearestLevel(excitation, _tolerance);
    const G4NuclearLevel* level =LevelManager->NearestLevel(excitation);

    if (level != 0)  {
      if (_verbose > 0) {
        G4cout << "DiscreteTHGammaDeexcitation::CreateTransition - Created from level energy "
            << level->Energy() << ", excitation is " << excitation << G4endl;
      }
      DiscreteTHGammaTransition* dtransit = new DiscreteTHGammaTransition(*level,Z);
      dtransit->SetICM(icm);
      return dtransit;
    } else {
      if (_verbose > 0)
        G4cout
            << "DiscreteTHGammaDeexcitation::CreateTransition - No transition created from "
            << excitation << " within tolerance " << Tolerance << G4endl;

      return nullptr;
    }
  } else return nullptr;
}

G4bool DiscreteTHGammaDeexcitation::CanDoTransition(G4Fragment* nucleus)
{
  if (_transition == nullptr) {
    if (_verbose > 0)
      G4cout << "DiscreteTHGammaDeexcitation::CanDoTransition - Null transition\n";
    return false;
  }

  G4double Z = nucleus->GetZ();
  if (Z>98) {
    if (_verbose > 0) {
        G4cout << "DiscreteTHGammaDeexcitation::CanDoTransition - n/p/H/>U\n";
    }
    return false;
  }

  G4double excitation = nucleus->GetExcitationEnergy();
  if (excitation <= 0.) {
    if (_verbose > 0) {
      G4cout << "DiscreteTHGammaDeexcitation::CanDoTransition -  Excitation <= 0\n";
    }
    return false;
  }

  // The following is a protection to avoid looping in case of elements with very low ensdf levels
  if ( excitation > LevelManager->MaxLevelEnergy() + Tolerance
    or excitation < LevelManager->MinLevelEnergy() - Tolerance
    or excitation < LevelManager->MinLevelEnergy() * 0.9
  ) {
    if (_verbose > 0) {
      G4cout << "DiscreteTHGammaDeexcitation::CanDoTransition -  Excitation "<< excitation
       << ", Min-Max are " << LevelManager->MinLevelEnergy() << " " << LevelManager->MaxLevelEnergy() << G4endl;
    }
    return false;
  }

  const G4NuclearLevel* level = LevelManager->NearestLevel(excitation);
  if ( level == nullptr
    or (level->HalfLife() > Max_hl && !rdm )
  ) {
    if (_verbose > 0) 	{
      G4cout << "DiscreteTHGammaDeexcitation::CanDoTransition -  Halflife "
	     << level->HalfLife() << ", Calling from RDM "
	     << (rdm ? " True " : " False ")  << " Max-HL = " <<  Max_hl << G4endl;
    }
    return false;
  }

  return true;
}

