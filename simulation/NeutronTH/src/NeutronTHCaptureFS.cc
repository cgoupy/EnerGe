#include <NeutronTHCaptureFS.hh>

#include <PhotonTHEvaporation.hh>

#include <G4SystemOfUnits.hh>
#include <G4NeutronHPDataUsed.hh>
#include <G4ReactionProduct.hh>
#include <G4ParticleTable.hh>
#include <G4IonTable.hh>
#include <G4Nucleus.hh>
#include <G4Fragment.hh>
#include <G4Gamma.hh>

G4HadFinalState * NeutronTHCaptureFS::ApplyYourself(const G4HadProjectile & theTrack)
{
  G4int i;
  theResult.Clear();
  // prepare neutron
  G4double eKinetic = theTrack.GetKineticEnergy();
  const G4HadProjectile *incidentParticle = &theTrack;
  G4ReactionProduct theNeutron( const_cast<G4ParticleDefinition *>(incidentParticle->GetDefinition()) );
  theNeutron.SetMomentum( incidentParticle->Get4Momentum().vect() );
  theNeutron.SetKineticEnergy( eKinetic );

  // prepare target
  G4ReactionProduct theTarget;
  G4Nucleus aNucleus;
  G4double eps = 0.0001;
  if(targetMass<500*MeV)
    targetMass = ( G4NucleiProperties::GetNuclearMass( static_cast<G4int>(theBaseA+eps) , static_cast<G4int>(theBaseZ+eps) )) /
                 G4Neutron::Neutron()->GetPDGMass();
  G4ThreeVector neutronVelocity = 1./G4Neutron::Neutron()->GetPDGMass()*theNeutron.GetMomentum();
  G4double temperature = theTrack.GetMaterial()->GetTemperature();
  theTarget = aNucleus.GetBiasedThermalNucleus(targetMass, neutronVelocity, temperature);

  // go to nucleus rest system
  theNeutron.Lorentz(theNeutron, -1*theTarget);
  eKinetic = theNeutron.GetKineticEnergy();

  G4ReactionProductVector * thePhotons = nullptr;

  G4ThreeVector aCMSMomentum = theNeutron.GetMomentum()+theTarget.GetMomentum();
  G4LorentzVector p4(aCMSMomentum, theTarget.GetTotalEnergy() + theNeutron.GetTotalEnergy());
  G4Fragment nucleus(static_cast<G4int>(theBaseA+1), static_cast<G4int>(theBaseZ) ,p4);
  PhotonTHEvaporation photonEvaporation;
  photonEvaporation.SetICM(TRUE);
  G4FragmentVector* products = photonEvaporation.BreakItUp(nucleus);

  G4FragmentVector::iterator j;
  thePhotons = new G4ReactionProductVector;
  for(j=products->begin(); j!=products->end(); j++) {
    G4ReactionProduct * theOne = new G4ReactionProduct;
    if ( (*j)->GetParticleDefinition() != nullptr )
      theOne->SetDefinition( (*j)->GetParticleDefinition() );
    else
      theOne->SetDefinition( G4Gamma::Gamma() );

    G4ParticleTable* theTable = G4ParticleTable::GetParticleTable();
    if((*j)->GetMomentum().mag() > 10.*MeV)
      theOne->SetDefinition(
        theTable->GetIonTable()->FindIon(static_cast<G4int>(theBaseZ), static_cast<G4int>(theBaseA+1), 0, static_cast<G4int>(theBaseZ)) );
    theOne->SetMomentum( (*j)->GetMomentum().vect() ) ;
    theOne->SetTotalEnergy( (*j)->GetMomentum().t() );
    thePhotons->push_back(theOne);
    delete *j;
  }
  delete products;

  // add them to the final state

  G4int nPhotons = 0;
  if(thePhotons!=nullptr) nPhotons=thePhotons->size();
//   G4int nParticles = nPhotons;
//   if(1==nPhotons) nParticles = 2;

  // back to lab system
  for(i=0; i<nPhotons; i++) {
    thePhotons->operator[](i)->Lorentz(*(thePhotons->operator[](i)), theTarget);
  }

  // Recoil, if only one gamma
  if (1==nPhotons) {
    G4DynamicParticle * theOne = new G4DynamicParticle;
    G4ParticleDefinition * aRecoil = G4ParticleTable::GetParticleTable()->GetIonTable()->FindIon(
      static_cast<G4int>(theBaseZ), static_cast<G4int>(theBaseA+1), 0, static_cast<G4int>(theBaseZ));
    theOne->SetDefinition(aRecoil);
    // Now energy;
    // Can be done slightly better @
    G4ThreeVector aMomentum =  theTrack.Get4Momentum().vect()
                               +theTarget.GetMomentum()
                               -thePhotons->operator[](0)->GetMomentum();

    G4ThreeVector theMomUnit = aMomentum.unit();
    G4double aKinEnergy =  theTrack.GetKineticEnergy()
                           +theTarget.GetKineticEnergy(); // gammas come from Q-value
    G4double theResMass = aRecoil->GetPDGMass();
    G4double theResE = aRecoil->GetPDGMass()+aKinEnergy;
    G4double theAbsMom = std::sqrt(theResE*theResE - theResMass*theResMass);
    G4ThreeVector theMomentum = theAbsMom*theMomUnit;
    theOne->SetMomentum(theMomentum);
    theResult.AddSecondary(theOne);
  }

  // Now fill in the gammas.
  for(i=0; i<nPhotons; i++) {
    // back to lab system
    G4DynamicParticle * theOne = new G4DynamicParticle;
    theOne->SetDefinition(thePhotons->operator[](i)->GetDefinition());
    theOne->SetMomentum(thePhotons->operator[](i)->GetMomentum());
    theResult.AddSecondary(theOne);
    delete thePhotons->operator[](i);
  }
  delete thePhotons;
  // clean up the primary neutron
  theResult.SetStatusChange(stopAndKill);
  return &theResult;
}

void NeutronTHCaptureFS::Init (G4double A, G4double Z, G4String & dirName, G4String & )
{
  G4String tString = "/FS/";

  G4bool dbool;
  G4NeutronHPDataUsed aFile = theNames.GetName(static_cast<G4int>(A), static_cast<G4int>(Z), dirName, tString, dbool);
  G4String filename = aFile.GetName();

  theBaseA = A;
  theBaseZ = G4int(Z+.5);
  if(!dbool || ( Z<2.5 && ( std::abs(theBaseZ - Z)>0.0001 || std::abs(theBaseA - A)>0.0001))) {
    hasAnyData = false;
    hasFSData = false;
    hasXsec = false;
    return;
  }
  std::ifstream theData(filename, std::ios::in);


  hasFSData = theFinalStatePhotons.InitMean(theData);

  if(hasFSData) {
    targetMass = theFinalStatePhotons.GetTargetMass();
    theFinalStatePhotons.InitAngular(theData);
    theFinalStatePhotons.InitEnergies(theData);
  }
  theData.close();
}
