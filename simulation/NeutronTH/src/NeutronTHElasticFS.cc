#include <NeutronTHElasticFS.hh>

//#include <NucleusTH.hh>

#include <G4SystemOfUnits.hh>
#include <G4PhysicalConstants.hh>
#include <G4ThreeVector.hh>
#include <G4LorentzVector.hh>
#include <G4ParticleTable.hh>
#include <G4Nucleus.hh>
#include <G4Neutron.hh>
#include <G4Proton.hh>
#include <G4Deuteron.hh>
#include <G4Triton.hh>
#include <G4Alpha.hh>
#include <G4IonTable.hh>
#include <G4ReactionProduct.hh>
#include <G4NeutronHPDataUsed.hh>

void NeutronTHElasticFS::Init (G4double A, G4double Z, G4String & dirName, G4String & )
{
  G4String tString = "/FS/";
  G4bool dbool;
  G4NeutronHPDataUsed aFile = theNames.GetName(static_cast<G4int>(A), static_cast<G4int>(Z), dirName, tString, dbool);
  G4String filename = aFile.GetName();
  theBaseA = aFile.GetA();
  theBaseZ = aFile.GetZ();
  if(!dbool) {
    hasAnyData = false;
    hasFSData = false;
    hasXsec = false;
    return;
  }
  std::ifstream theData(filename, std::ios::in);
  theData >> repFlag >> targetMass >> frameFlag;

  if(repFlag==1) {
    G4int nEnergy;
    theData >> nEnergy;
    theCoefficients = new G4NeutronHPLegendreStore(nEnergy);
    theCoefficients->InitInterpolation(theData);
    G4double temp, energy;
    G4int tempdep, nLegendre;
    G4int i, ii;
    for (i=0; i<nEnergy; i++) {
      theData >> temp >> energy >> tempdep >> nLegendre;
      energy *=eV;
      theCoefficients->Init(i, energy, nLegendre);
      theCoefficients->SetTemperature(i, temp);
      G4double coeff=0;
      for(ii=0; ii<nLegendre; ii++) {
        // load legendre coefficients.
        theData >> coeff;
        theCoefficients->SetCoeff(i, ii+1, coeff); // @@@HPW@@@
      }
    }
  } else if (repFlag==2) {
    G4int nEnergy;
    theData >> nEnergy;
    theProbArray = new G4NeutronHPPartial(nEnergy, nEnergy);
    theProbArray->InitInterpolation(theData);
    G4double temp, energy;
    G4int tempdep, nPoints;
    for(G4int i=0; i<nEnergy; i++) {
      theData >> temp >> energy >> tempdep >> nPoints;
      energy *= eV;
      theProbArray->InitInterpolation(i, theData);
      theProbArray->SetT(i, temp);
      theProbArray->SetX(i, energy);
      G4double prob, costh;
      for(G4int ii=0; ii<nPoints; ii++) {
        // fill probability arrays.
        theData >> costh >> prob;
        theProbArray->SetX(i, ii, costh);
        theProbArray->SetY(i, ii, prob);
      }
    }
  } else if (repFlag==0) {
    theData >> frameFlag;
  } else {
    G4cout << "unusable number for repFlag: repFlag="<<repFlag<<G4endl;
    throw G4HadronicException(__FILE__, __LINE__, "NeutronTHElasticFS::Init -- unusable number for repFlag");
  }

  theData.close();
}

G4HadFinalState * NeutronTHElasticFS::ApplyYourself(const G4HadProjectile & theTrack)
{
  theResult.Clear();
  G4double eKinetic = theTrack.GetKineticEnergy();
  const G4HadProjectile *incidentParticle = &theTrack;
  G4ReactionProduct theNeutron( const_cast<G4ParticleDefinition *>(incidentParticle->GetDefinition()) );
  theNeutron.SetMomentum(incidentParticle->Get4Momentum().vect());
  theNeutron.SetKineticEnergy(eKinetic);

  G4ReactionProduct theTarget;
  G4Nucleus aNucleus;

  G4ThreeVector neuVelo = (1./incidentParticle->GetDefinition()->GetPDGMass())*theNeutron.GetMomentum();

  G4double Meff = 0.0;

  if(targetMass < 1.1) {

    const G4int	nparam = 4;
    G4double	freq[nparam] = {9.0e-08,1.5e-07,1.8e-07,3.6e-07};
    G4double	sigma[nparam] = {6.5e-09,6.5e-09,3.0e-08,1.0e-08};
    G4double	mass[nparam] = {6.54,121.86,1.90,3.5};


    G4double	value1[nparam],
              value2[nparam],
              value3[nparam],
              value4[nparam],
              value5[nparam],
              value6[nparam],
              value7[nparam],
              value8[nparam];

    G4double	n_l[nparam];
    G4double	E_l[nparam];
    G4double	k_b_T,
              k_b_tau;

    G4double	Ge;

    G4double	aT = 	theTrack.GetMaterial()->GetTemperature();

    for(int i=0; i<nparam; i++) {
      value1[i] = freq[i]/sigma[i];
      value2[i] = std::exp((-1.0)*value1[i]*value1[i]);
      value3[i] = erf(value1[i]);
      value4[i] = value2[i] + (value1[i]*pi)*(1.0+value3[i]);
      value5[i] = (eKinetic - freq[i])/sigma[i];
      value6[i] = std::exp((-1.0)*value5[i]*value5[i]);
      value7[i] = erf((-1.0)*value5[i]);
      value8[i] = value6[i] + (value1[i]*pi)*(1.0+value7[i]);
      value8[i] /= value4[i];
    }

    for(int i=0; i<nparam; i++) {
      n_l[i] = 1.0/(std::exp(freq[i]/(k_Boltzmann*aT))-1.0);
      E_l[i] = freq[i]*(n_l[i]+0.5);
    }

    Meff = 1.0/targetMass;

    for(int i=0; i<nparam; i++) {
      Meff -= value8[i]/mass[i];
    }
    Meff = 1.0/Meff;

    Ge = 1.0;
    k_b_T = k_Boltzmann*aT;

    for(G4int i=0; i<nparam; i++) {
      Ge += value8[i]*(2.0*n_l[i]+1.0)/(mass[i]*freq[i]);
      k_b_T += targetMass*((E_l[i]-k_Boltzmann*aT)/mass[i]);
    }

    k_b_tau = Meff*(k_b_T/targetMass);

    for(G4int i=0; i<nparam; i++) {
      k_b_tau -= Meff*(value8[i]*E_l[i]/mass[i]);
    }

    theTarget = aNucleus.GetThermalNucleus(Meff,k_b_tau/k_Boltzmann);

  } else {
    theTarget = aNucleus.GetBiasedThermalNucleus(targetMass,neuVelo,theTrack.GetMaterial()->GetTemperature());
  }

  // neutron and target defined as reaction products.

  // prepare lorentz-transformation to Lab.

  G4ThreeVector the3Neutron = theNeutron.GetMomentum();
  G4double nEnergy = theNeutron.GetTotalEnergy();

  G4ThreeVector the3Target = theTarget.GetMomentum();
  G4double tEnergy = theTarget.GetTotalEnergy();


  G4ReactionProduct theCMS;
  G4double totE = nEnergy+tEnergy;
  G4ThreeVector the3CMS = the3Target+the3Neutron;
  theCMS.SetMomentum(the3CMS);
  theCMS.SetMass( std::sqrt( (totE-the3CMS.mag()) * (totE+the3CMS.mag()) ) );
  theCMS.SetTotalEnergy(totE);

  // data come as fcn of n-energy in nuclear rest frame
  G4ReactionProduct boosted;
  boosted.Lorentz(theNeutron, theTarget);
  eKinetic = boosted.GetKineticEnergy(); // get kinetic energy for scattering
  G4double cosTh = -2;

  if(repFlag == 1) {
    cosTh = theCoefficients->SampleElastic(eKinetic);
  } else if (repFlag==2) {
    cosTh = theProbArray->Sample(eKinetic);
  } else if (repFlag==0) {
    cosTh = 2.*G4UniformRand()-1.;
  } else {
    G4cout << "unusable number for repFlag: repFlag="<<repFlag<<G4endl;
    throw G4HadronicException(__FILE__, __LINE__, "NeutronTHElasticFS::Init -- unusable number for repFlag");
  }

  if(targetMass < 1.1)	cosTh = 2.*G4UniformRand()-1.;

  if(cosTh<-1.1) return nullptr;
  G4double phi = twopi*G4UniformRand();
  G4double theta = std::acos(cosTh);
  G4double sinth = std::sin(theta);

  if(targetMass < 1.1) {
    theNeutron.Lorentz(theNeutron, theCMS);
    theTarget.Lorentz(theTarget, theCMS);
    G4double en = theNeutron.GetTotalMomentum(); // already in CMS.
    G4ThreeVector cmsMom=theNeutron.GetMomentum(); // for neutron direction in CMS
    G4double cms_theta=cmsMom.theta();
    G4double cms_phi=cmsMom.phi();
    G4ThreeVector tempVector;

    tempVector.setX(std::cos(theta)*std::sin(cms_theta)*std::cos(cms_phi)
                    +std::sin(theta)*std::cos(phi)*std::cos(cms_theta)*std::cos(cms_phi)
                    -std::sin(theta)*std::sin(phi)*std::sin(cms_phi)  );
    tempVector.setY(std::cos(theta)*std::sin(cms_theta)*std::sin(cms_phi)
                    +std::sin(theta)*std::cos(phi)*std::cos(cms_theta)*std::sin(cms_phi)
                    +std::sin(theta)*std::sin(phi)*std::cos(cms_phi)  );
    tempVector.setZ(std::cos(theta)*std::cos(cms_theta)
                    -std::sin(theta)*std::cos(phi)*std::sin(cms_theta)  );

    tempVector *= en;

    theNeutron.SetMomentum(tempVector);
    theTarget.SetMomentum(-tempVector);
    G4double tP = theTarget.GetTotalMomentum();
    G4double tM = theTarget.GetMass();
    theTarget.SetTotalEnergy(std::sqrt((tP+tM)*(tP+tM)-2.*tP*tM));
    theNeutron.Lorentz(theNeutron, (-1.)*theCMS);

    if (theNeutron.GetKineticEnergy() < 0) {
      theNeutron.SetMomentum( G4ThreeVector(0) );
      theNeutron.SetTotalEnergy (theNeutron.GetMass());
    }

    theTarget.Lorentz(theTarget, -1.*theCMS);

    if (theTarget.GetKineticEnergy() < 0) {
      theTarget.SetMomentum( G4ThreeVector(0));
      theTarget.SetTotalEnergy(theTarget.GetMass());
    }
  } else {

    if (frameFlag == 1) {	// final state data given in target rest frame.
      // we have the scattering angle, now we need the energy, then do the
      // boosting.
      // relativistic elastic scattering energy angular correlation:
      theNeutron.Lorentz(theNeutron, theTarget);
      G4double e0 = theNeutron.GetTotalEnergy();
      G4double p0 = theNeutron.GetTotalMomentum();
      G4double mN = theNeutron.GetMass();
      G4double mT = theTarget.GetMass();
      G4double eE = e0+mT;
      G4double ap = (mT+eE)*(mT-eE) + (p0+mN)*(p0-mN);
      G4double a = 4*(eE+p0*cosTh)*(eE-p0*cosTh);
      G4double b = 4*ap*p0*cosTh;
      G4double c = (2.*eE*mN-ap)*(2.*eE*mN+ap);
      G4double en = (-b+std::sqrt(b*b - 4*a*c) )/(2*a);
      G4ThreeVector tempVector(en*sinth*std::cos(phi), en*sinth*std::sin(phi), en*std::cos(theta) );
      theNeutron.SetMomentum(tempVector);
      theNeutron.SetTotalEnergy(std::sqrt(en*en+theNeutron.GetMass()*theNeutron.GetMass()));
      // first to lab
      theNeutron.Lorentz(theNeutron, -1.*theTarget);
      // now to CMS
      theNeutron.Lorentz(theNeutron, theCMS);
      theTarget.SetMomentum(-theNeutron.GetMomentum());
      theTarget.SetTotalEnergy(theNeutron.GetTotalEnergy());
      // and back to lab
      theNeutron.Lorentz(theNeutron, -1.*theCMS);

      theTarget.Lorentz(theTarget, -1.*theCMS);
    } else if (frameFlag == 2) {	// CMS
      theNeutron.Lorentz(theNeutron, theCMS);
      theTarget.Lorentz(theTarget, theCMS);
      G4double en = theNeutron.GetTotalMomentum(); // already in CMS.
      G4ThreeVector cmsMom=theNeutron.GetMomentum(); // for neutron direction in CMS
      G4double cms_theta=cmsMom.theta();
      G4double cms_phi=cmsMom.phi();
      G4ThreeVector tempVector;
      tempVector.setX(std::cos(theta)*std::sin(cms_theta)*std::cos(cms_phi)
                      +std::sin(theta)*std::cos(phi)*std::cos(cms_theta)*std::cos(cms_phi)
                      -std::sin(theta)*std::sin(phi)*std::sin(cms_phi)  );
      tempVector.setY(std::cos(theta)*std::sin(cms_theta)*std::sin(cms_phi)
                      +std::sin(theta)*std::cos(phi)*std::cos(cms_theta)*std::sin(cms_phi)
                      +std::sin(theta)*std::sin(phi)*std::cos(cms_phi)  );
      tempVector.setZ(std::cos(theta)*std::cos(cms_theta)
                      -std::sin(theta)*std::cos(phi)*std::sin(cms_theta)  );
      tempVector *= en;
      theNeutron.SetMomentum(tempVector);
      theTarget.SetMomentum(-tempVector);
      G4double tP = theTarget.GetTotalMomentum();
      G4double tM = theTarget.GetMass();
      theTarget.SetTotalEnergy(std::sqrt((tP+tM)*(tP+tM)-2.*tP*tM));
      theNeutron.Lorentz(theNeutron, -1.*theCMS);
      theTarget.Lorentz(theTarget, -1.*theCMS);
    } else {
      G4cout <<"Value of frameFlag (1=LAB, 2=CMS): "<<frameFlag;
      throw G4HadronicException(__FILE__, __LINE__, "NeutronTHElasticFS::ApplyYourSelf frameflag incorrect");
    }
  }
  theResult.SetEnergyChange(theNeutron.GetKineticEnergy());
  theResult.SetMomentumChange(theNeutron.GetMomentum().unit());


  G4DynamicParticle* theRecoil = new G4DynamicParticle;


  if(targetMass<4.5) {
    if(targetMass<1) {
      // proton
      theRecoil->SetDefinition(G4Proton::Proton());
    } else if(targetMass<2 ) {
      // deuteron
      theRecoil->SetDefinition(G4Deuteron::Deuteron());
    } else if(targetMass<2.999 ) {
      // 3He
      theRecoil->SetDefinition(G4He3::He3());
    } else if(targetMass<3 ) {
      // Triton
      theRecoil->SetDefinition(G4Triton::Triton());
    } else {
      // alpha
      theRecoil->SetDefinition(G4Alpha::Alpha());
    }
  } else {
    theRecoil->SetDefinition(G4ParticleTable::GetParticleTable()->GetIonTable()->FindIon(
      static_cast<G4int>(theBaseZ), static_cast<G4int>(theBaseA), 0, static_cast<G4int>(theBaseZ)));
  }

  theRecoil->SetMomentum(theTarget.GetMomentum());
  theResult.AddSecondary(theRecoil);
  theResult.SetStatusChange(suspend);

  return &theResult;
}
