#ifndef CONTINUUM_TH_GAMMA_DEEXCITATION_HH
#define CONTINUUM_TH_GAMMA_DEEXCITATION_HH

#include <G4VGammaDeexcitation.hh>

#include <globals.hh>

#include <ContinuumTHGammaTransition.hh>
#include <G4Fragment.hh>

class NuclearTHLevelManager;

class ContinuumTHGammaDeexcitation : public G4VGammaDeexcitation
{
public:
  virtual ~ContinuumTHGammaDeexcitation() {}

public:
  virtual G4VGammaTransition* CreateTransition(G4Fragment* aNucleus);
  virtual G4bool CanDoTransition(G4Fragment* aNucleus);

private:
  G4int _nucleusZ = 0;
  G4int _nucleusA = 0;
  NuclearTHLevelManager * _levelManager = nullptr;
};

#endif
