
#ifndef NEUTRON_TH_FINALSTATE_H
#define NEUTRON_TH_FINALSTATE_H

#include <G4Material.hh>
#include <G4FastVector.hh>
#include <G4HadFinalState.hh>
#include <G4NeutronHPNames.hh>
#include <G4NeutronHPVector.hh>
#include <G4HadProjectile.hh>

class NeutronTHFinalState {
public:

  NeutronTHFinalState() {
    hasFSData = true;
    hasXsec = true;
    hasAnyData = true;
    theBaseZ = 0;
    theBaseA = 0;
  };

  virtual ~NeutronTHFinalState() {};

  virtual void Init (G4double A, G4double Z, G4String & dirName, G4String & aFSType) = 0;
  virtual G4HadFinalState * ApplyYourself(const G4HadProjectile & ) 	{
    throw G4HadronicException(__FILE__, __LINE__,
                              "G4HadFinalState * ApplyYourself(const G4HadProjectile & theTrack) needs implementation");
    return nullptr;
  }

  virtual NeutronTHFinalState * New() = 0;

  G4bool HasXsec() {
    return hasXsec;
  }
  G4bool HasFSData() {
    return hasFSData;
  }
  G4bool HasAnyData() {
    return hasAnyData;
  }

  virtual G4double GetXsec(G4double ) {
    return 0;
  }
  virtual G4NeutronHPVector * GetXsec() {
    return nullptr;
  }

  void     SetA_Z(G4double anA, G4double aZ) {
    theBaseA = anA;
    theBaseZ = aZ;
  }
  G4double GetZ() {
    return theBaseZ;
  }
  G4double GetN() {
    return theBaseA;
  }

protected:

  G4bool hasXsec;
  G4bool hasFSData;
  G4bool hasAnyData;
  G4NeutronHPNames theNames;

  G4HadFinalState theResult;

  G4double theBaseA;
  G4double theBaseZ;

};
#endif
