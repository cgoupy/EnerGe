#ifndef CONTINUUM_TH_GAMMA_TRANSITION_HH
#define CONTINUUM_TH_GAMMA_TRANSITION_HH

#include <globals.hh>
#include <G4VGammaTransition.hh>
#include <NuclearTHLevelManager.hh>
#include <G4VLevelDensityParameter.hh>

class ContinuumTHGammaTransition : public G4VGammaTransition {
public:

  ContinuumTHGammaTransition(const NuclearTHLevelManager* levelManager,
                             G4int Z, G4int A, G4double excitation,
                             G4int verbose);

  virtual ~ContinuumTHGammaTransition() {}

  virtual void SetEnergyFrom(const G4double energy);
  virtual G4double GetGammaEnergy();
  virtual G4double GetGammaCreationTime();
  virtual void SelectGamma();

private:

  G4double E1Pdf(G4double energy);
  G4double GammaTime();

  G4int verbose;
  G4int nucleusA;
  G4int nucleusZ;
  G4double eMin;
  G4double eMax;
  G4double maxLevelE;
  G4double minLevelE;
  G4double excitation;
  G4double eGamma;
  G4double gammaCreationTime;
  const NuclearTHLevelManager* LevelManager;

};

#endif
