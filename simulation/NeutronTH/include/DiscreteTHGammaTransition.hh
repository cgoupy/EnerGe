#ifndef DISCRETE_TH_GAMMA_TRANSITION_HH
#define DISCRETE_TH_GAMMA_TRANSITION_HH

#include <globals.hh>
#include <G4VGammaTransition.hh>
#include <G4NuclearLevel.hh>

class DiscreteTHGammaTransition : public G4VGammaTransition {
public:
  virtual ~DiscreteTHGammaTransition() {}

  DiscreteTHGammaTransition(const G4NuclearLevel& level): Level(level) {}
  DiscreteTHGammaTransition(const G4NuclearLevel& level, G4int Z): NucleusZ(Z), Level(level) {}

public:
  virtual void SelectGamma();

  virtual void SetEnergyFrom(const G4double energy) { Excitation = energy; }
  inline void SetICM(G4bool ic) { icm = ic; }

  inline virtual G4double GetGammaEnergy() { return GammaEnergy; }
  inline virtual G4double GetGammaCreationTime() { return GammaCreationTime; }
  inline G4double GetBondEnergy () const { return BondE; }
  inline G4int    GetOrbitNumber() const { return OrbitE; }
  inline G4bool   IsAGamma()       const { return aGamma; }

private:
  G4int NucleusZ;
  G4int OrbitE = -1;
  G4double BondE = 0.;
  G4bool aGamma = true;
  G4bool icm = false;

  G4double GammaEnergy = 0.;
  G4double Excitation = 0.;
  G4double GammaCreationTime = 0.;
  G4NuclearLevel Level;
};

#endif

