#ifndef NuclearTHLevelStore_hh
#define NuclearTHLevelStore_hh 1

#include <NuclearTHLevelManager.hh>
#include <map>

class NuclearTHLevelStore {
private:
  NuclearTHLevelStore();

public:

  static NuclearTHLevelStore* GetInstance();

  NuclearTHLevelManager * GetManager(const G4int Z, const G4int A);


  ~NuclearTHLevelStore();

private:

  G4String GenerateKey(const G4int Z, const G4int A);


  static std::map<G4String,NuclearTHLevelManager*> theManagers;
  static G4String dirName;
  static G4String dirName2;

};
#endif
