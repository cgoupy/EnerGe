#ifndef DISCRETE_TH_GAMMA_DEEXCITATION_HH
#define DISCRETE_TH_GAMMA_DEEXCITATION_HH

#include <DiscreteTHGammaTransition.hh>

#include <globals.hh>
#include <G4VGammaDeexcitation.hh>
#include <G4Fragment.hh>

class NuclearTHLevelManager;

class DiscreteTHGammaDeexcitation : public G4VGammaDeexcitation
{
public:
  virtual ~DiscreteTHGammaDeexcitation() {}
  DiscreteTHGammaDeexcitation();

public:

  virtual G4VGammaTransition * CreateTransition(G4Fragment* nucleus);

  virtual G4bool CanDoTransition(G4Fragment* nucleus);

  inline void SetICM(G4bool hl) { icm = hl; };
  inline void SetRDM(G4bool hl) { rdm = hl; };
  inline void SetHL(G4double hl) { Max_hl = hl; };

private:
  G4int NucleusZ;
  G4int NucleusA;
  G4double Tolerance;
  G4double Max_hl;
  G4bool icm;
  G4bool rdm;
  NuclearTHLevelManager* LevelManager;
};

#endif



