#ifndef NEUTRON_TH_DATA_H
#define NEUTRON_TH_DATA_H 1

#include <globals.hh>
#include <G4Element.hh>
#include <NeutronTHElasticData.hh>
#include <NeutronTHCaptureData.hh>
#include <NeutronTHElementData.hh>

class NeutronTHData {
public:

  NeutronTHData();

  ~NeutronTHData();


  inline G4PhysicsVector * MakePhysicsVector(G4Element * thE, NeutronTHCaptureData * theP) {
    return DoPhysicsVector(theData[thE->GetIndex()].GetData(theP));
  }

  inline G4PhysicsVector * MakePhysicsVector(G4Element * thE, NeutronTHElasticData * theP) {
    return DoPhysicsVector(theData[thE->GetIndex()].GetData(theP));
  }

  G4PhysicsVector * DoPhysicsVector(G4NeutronHPVector * theVector);

  static NeutronTHData * Instance();

private:

  NeutronTHElementData * theData;

  G4int numEle;

};

#endif
