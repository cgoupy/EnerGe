#ifndef NEUTRON_TH_ELASTIC_H
#define NEUTRON_TH_ELASTIC_H 1

#include <NeutronTHChannel.hh>

#include <G4Types.hh>
#include <G4String.hh>
#include <G4HadronicInteraction.hh>

class G4HadFinalState;
class G4HadProjectile;
class G4Nucleus;

class NeutronTHElastic : public G4HadronicInteraction {
public:
  virtual ~NeutronTHElastic();
  NeutronTHElastic();

  G4HadFinalState* ApplyYourself(const G4HadProjectile& aTrack, G4Nucleus& aTargetNucleus);
  inline G4int GetNiso() {
    return theElastic[0].GetNiso();
  }

private:
  G4double* xSec;
  NeutronTHChannel* theElastic;
  G4String dirName;
  G4int numEle;
};

#endif
