#ifndef NEUTRON_TH_ELASTIC_FS_H
#define NEUTRON_TH_ELASTIC_FS_H 1

#include <globals.hh>
#include <G4HadProjectile.hh>
#include <G4HadFinalState.hh>
//#include <G4NeutronHPFinalState.hh>
#include <NeutronTHFinalState.hh>
#include <G4NeutronHPLegendreStore.hh>
#include <G4NeutronHPPartial.hh>
#include <G4NeutronHPFastLegendre.hh>
#include <G4NeutronHPInterpolator.hh>
#include <G4NeutronHPNames.hh>

class NeutronTHElasticFS : public NeutronTHFinalState {
public:

  NeutronTHElasticFS() {
    hasXsec = false;
    theCoefficients = nullptr;
    theProbArray = nullptr;
  }

  ~NeutronTHElasticFS() {
    if(theCoefficients!=nullptr) delete theCoefficients;
    if(theProbArray!=nullptr) delete theProbArray;
  }

  void Init (G4double A, G4double Z, G4String & dirName, G4String & aFSType);
  G4HadFinalState * ApplyYourself(const G4HadProjectile & theTrack);
  NeutronTHFinalState * New() 	{
    NeutronTHElasticFS * theNew = new NeutronTHElasticFS;
    return theNew;
  }

private:

  G4int repFlag;    // Legendre coeff(1), or probability array(2), or isotropic(0).
  G4double targetMass; // in neutronmass units.
  G4int frameFlag;  // CMS or Lab system.

  G4NeutronHPLegendreStore * theCoefficients; // the legendre coefficients
  G4NeutronHPPartial * theProbArray; // the probability array p,costh for energy
  G4NeutronHPInterpolator theInt; // interpolation

  G4NeutronHPFastLegendre theLegend; // fast look-up for leg-integrals
};
#endif
