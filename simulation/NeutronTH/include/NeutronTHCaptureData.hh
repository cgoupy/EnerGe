#ifndef NEUTRON_TH_CAPTURE_DATA_H
#define NEUTRON_TH_CAPTURE_DATA_H 1

#include <G4VCrossSectionDataSet.hh>
#include <G4DynamicParticle.hh>
#include <G4Element.hh>
#include <G4ParticleDefinition.hh>
#include <G4PhysicsTable.hh>

class NeutronTHCaptureData : public G4VCrossSectionDataSet {
public:

  NeutronTHCaptureData();

  ~NeutronTHCaptureData();

  G4bool IsApplicable(const G4DynamicParticle*, const G4Element*);

  G4double GetCrossSection(const G4DynamicParticle*, const G4Element*, G4double aT);

  void BuildPhysicsTable(const G4ParticleDefinition&);

  void DumpPhysicsTable(const G4ParticleDefinition&);

private:

  G4PhysicsTable * theCrossSections;
};

#endif
