#ifndef NEUTRON_TH_ELEMENT_DATA_H
#define NEUTRON_TH_ELEMENT_DATA_H 1

#include <globals.hh>
#include <NeutronTHIsoData.hh>
#include <G4NeutronHPVector.hh>
#include <G4Material.hh>
#include <G4HadronCrossSections.hh>
#include <G4HadronCaptureProcess.hh>
#include <NeutronTHElasticData.hh>
#include <NeutronTHCaptureData.hh>
#include <G4StableIsotopes.hh>

class NeutronTHElementData : public G4HadronCrossSections {
public:

  NeutronTHElementData();

  ~NeutronTHElementData();

  void Init(G4Element * theElement);

  void UpdateData(G4int A, G4int Z, G4int index, G4double abundance);

  void Harmonise(G4NeutronHPVector *& theStore, G4NeutronHPVector * theNew);

  inline G4NeutronHPVector * GetData(NeutronTHCaptureData * ) {
    return theCaptureData;
  }

  inline G4NeutronHPVector * GetData(NeutronTHElasticData * ) {
    return theElasticData;
  }

  G4NeutronHPVector * MakePhysicsVector(G4Element * theElement,
                                        G4ParticleDefinition * theP,
                                        NeutronTHCaptureData * theSet);

  G4NeutronHPVector * MakePhysicsVector(G4Element * theElement,
                                        G4ParticleDefinition * theP,
                                        NeutronTHElasticData * theSet);


private:

  G4NeutronHPVector * theCaptureData;
  G4NeutronHPVector * theElasticData;
  G4double precision;

  G4NeutronHPVector * theBuffer;

  NeutronTHIsoData * theIsotopeWiseData;

  G4StableIsotopes theStableOnes;

  G4String filename;

};

#endif
