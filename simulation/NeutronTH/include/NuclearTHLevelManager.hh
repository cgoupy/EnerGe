#ifndef NUCLEAR_TH_LEVEL_MANAGER_HH
#define NUCLEAR_TH_LEVEL_MANAGER_HH

#include <globals.hh>
#include <G4SystemOfUnits.hh>

#include <fstream>
#include <vector>

class G4NuclearLevel;
typedef std::vector<G4NuclearLevel *> G4PtrLevelVector;

class NuclearTHLevelManager {
public:
  NuclearTHLevelManager();
  NuclearTHLevelManager(const G4int Z, const G4int A, const G4String& filename);
  NuclearTHLevelManager(const NuclearTHLevelManager & right);

  ~NuclearTHLevelManager();

//  void SetNucleus(const G4int Z, const G4int A, const G4String& filename);
  G4bool SetNucleus(const G4int Z, const G4int A, const G4String& filename);

  inline G4bool IsValid() const { return _validity; }
  inline G4double DiscreteProb() const { return _discrProb; }
  inline const G4PtrLevelVector* GetLevels() const { return _levels; }
  G4int NumberOfLevels() const;

  const G4NuclearLevel* NearestLevel(const G4double energy, const G4double eDiffMax=9999.*GeV) const;

  const G4NuclearLevel* LowestLevel() const;
  const G4NuclearLevel* HighestLevel() const;

  G4double MinLevelEnergy() const;
  G4double MaxLevelEnergy() const;


  void PrintAll();

private:
  const NuclearTHLevelManager& operator=(const NuclearTHLevelManager &right) = delete;
  G4bool operator==(const NuclearTHLevelManager &right) const = delete;
  G4bool operator!=(const NuclearTHLevelManager &right) const = delete;

  G4bool Read(std::ifstream& aDataFile);

  void MakeLevels();

  G4int _nucleusA;
  G4int _nucleusZ;
  G4String _fileName;
  G4bool _validity;
  G4PtrLevelVector* _levels;

  G4double _levelEnergy;
  G4double _gammaEnergy;
  G4double _probability;
  G4double _polarity;
  G4double _halfLife;
  G4double _angularMomentum;
  G4double _kCC;
  G4double _l1CC;
  G4double _l2CC;
  G4double _l3CC;
  G4double _m1CC;
  G4double _m2CC;
  G4double _m3CC;
  G4double _m4CC;
  G4double _m5CC;
  G4double _nPlusCC;
  G4double _totalCC;

  G4double _discrProb;	// total probability of discrete gamma dexcitation (from highest level)
};

#endif
