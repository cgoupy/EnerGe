#ifndef NEUTRON_TH_CAPTURE_H
#define NEUTRON_TH_CAPTURE_H 1

#include <globals.hh>
#include <NeutronTHChannel.hh>
#include <G4HadronicInteraction.hh>

class NeutronTHCapture : public G4HadronicInteraction {
public:

  NeutronTHCapture();

  ~NeutronTHCapture();

  G4HadFinalState * ApplyYourself(const G4HadProjectile& aTrack, G4Nucleus& aTargetNucleus);

private:

  G4double * xSec;
  NeutronTHChannel * theCapture;
  G4String dirName;
  G4int numEle;
  G4int it;

  G4HadFinalState theResult;
};

#endif
