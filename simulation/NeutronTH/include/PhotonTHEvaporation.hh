#ifndef PHOTON_TH_EVAPORATION_HH
#define PHOTON_TH_EVAPORATION_HH

#include <globals.hh>
#include <G4VPhotonEvaporation.hh>
#include <G4VEvaporationChannel.hh>
#include <G4ElectronOccupancy.hh>

class G4Fragment;
class G4VEmissionProbability;

class DiscreteTHGammaDeexcitation;
class ContinuumTHGammaDeexcitation;

class PhotonTHEvaporation : public G4VPhotonEvaporation, public G4VEvaporationChannel
{
public:
  PhotonTHEvaporation();

  virtual ~PhotonTHEvaporation();

  virtual G4FragmentVector * BreakItUp(const G4Fragment & nucleus);
  virtual G4FragmentVector * BreakUp(const G4Fragment& nucleus);

  virtual void Initialize(const G4Fragment & fragment);

  void RDMForced (G4bool);

  virtual void SetEmissionStrategy(G4VEmissionProbability * probAlgorithm);
  void SetVerboseLevel(G4int verbose);
  void SetICM (G4bool);
  void SetMaxHalfLife(G4double) ;
  void SetEOccupancy( G4ElectronOccupancy  eOccupancy) ;

  virtual G4double GetEmissionProbability(G4Fragment* theNucleus);
  inline G4ElectronOccupancy GetEOccupancy () const { return eOccupancy; }
  inline G4int GetVacantShellNumber () const { return vShellNumber; }

private:

  G4int verbose=0;
  G4bool myOwnProbAlgorithm=true;
  G4VEmissionProbability * probAlgorithm;
  DiscreteTHGammaDeexcitation * discrDeexcitation;
  ContinuumTHGammaDeexcitation * contDeexcitation;

  G4ElectronOccupancy eOccupancy{0};
  G4int vShellNumber=-1;

  G4Fragment Nucleus;
  G4double gammaE=0.;

  PhotonTHEvaporation(const PhotonTHEvaporation & right);

  const PhotonTHEvaporation & operator = (const PhotonTHEvaporation & right);

  // MGP - Check == and != multiple inheritance... must be a mess!
  G4bool operator == (const PhotonTHEvaporation & right) const;
  G4bool operator != (const PhotonTHEvaporation & right) const;
};

#endif



