#ifndef NEUTRON_TH_ISO_DATA_H
#define NEUTRON_TH_ISO_DATA_H 1

#include <globals.hh>
#include <G4ios.hh>
#include <fstream>
#include <sstream>
#include <cstdlib>
#include <G4NeutronHPVector.hh>
#include <G4NeutronHPNames.hh>

class NeutronTHIsoData {
public:

  NeutronTHIsoData() {
    theChannelData = nullptr;
    theCaptureData = nullptr;
    theElasticData = nullptr;
  }

  ~NeutronTHIsoData() {
    if(theChannelData!=nullptr) delete theChannelData;
  }

  inline G4double GetXsec(G4double energy) {
    return std::max(0., theChannelData->GetXsec(energy));
  }
  G4bool Init(G4int A, G4int Z, G4double abun, G4String dirName, G4String aFSType);

  void Init(G4int A, G4int Z, G4double abun); //fill PhysicsVector for this Isotope

  inline G4NeutronHPVector * MakeElasticData() {
    return theElasticData;
  }
  inline G4NeutronHPVector * MakeCaptureData() {
    return theCaptureData;
  }
  inline G4NeutronHPVector * MakeChannelData() {
    return theChannelData;
  }

  G4String GetName(G4int A, G4int Z, G4String base, G4String rest);

  inline void FillChannelData(G4NeutronHPVector * aBuffer) {
    if(theChannelData!=nullptr) throw G4HadronicException(__FILE__, __LINE__,
          "IsoData has channel full already!!!");
    theChannelData = new G4NeutronHPVector;
    for(G4int i=0; i<aBuffer->GetVectorLength(); i++) {
      theChannelData->SetPoint(i, aBuffer->GetPoint(i));
    }
  }

  inline void ThinOut(G4double precision) {
    if(theCaptureData) theCaptureData->ThinOut(precision);
    if(theElasticData) theElasticData->ThinOut(precision);
  }

private:

  G4NeutronHPVector * theCaptureData;
  G4NeutronHPVector * theElasticData;
  G4NeutronHPVector * theChannelData;

  G4String theFileName;
  G4NeutronHPNames theNames;
};

#endif
