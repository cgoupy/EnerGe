#ifndef NEUTRON_TH_CAPTURE_FS_H
#define NEUTRON_TH_CAPTURE_FS_H 1

#include <globals.hh>
#include <G4HadProjectile.hh>
#include <G4HadFinalState.hh>
#include <NeutronTHFinalState.hh>
#include <G4ReactionProductVector.hh>
#include <G4NeutronHPNames.hh>
#include <G4NeutronHPPhotonDist.hh>

class NeutronTHCaptureFS : public NeutronTHFinalState {
public:

  NeutronTHCaptureFS() {
    hasXsec = false;
    targetMass = 0;
  }

  ~NeutronTHCaptureFS() {}

  void Init (G4double A, G4double Z, G4String & dirName, G4String & aFSType);
  G4HadFinalState * ApplyYourself(const G4HadProjectile & theTrack);
  NeutronTHFinalState * New() {
    NeutronTHCaptureFS * theNew = new NeutronTHCaptureFS;
    return theNew;
  }

private:

  G4double targetMass;

  G4NeutronHPPhotonDist theFinalStatePhotons;

  G4NeutronHPNames theNames;

  G4double theCurrentA;
  G4double theCurrentZ;
};
#endif
