#ifndef NEUTRONTH_CHANNEL_HH
#define NEUTRONTH_CHANNEL_HH 1

#include <NeutronTHIsoData.hh>
#include <NeutronTHCaptureFS.hh>
//#include <G4NeutronHPFinalState.hh>
#include <NeutronTHFinalState.hh>

#include <globals.hh>
#include <G4NeutronHPVector.hh>
#include <G4Material.hh>
#include <G4HadProjectile.hh>
#include <G4NeutronInelasticProcess.hh>
#include <G4HadronFissionProcess.hh>
#include <G4HadronElasticProcess.hh>
#include <G4HadronCaptureProcess.hh>
#include <G4StableIsotopes.hh>
#include <G4Element.hh>

class NeutronTHChannel {
public:

  NeutronTHChannel() {

    theChannelData = new G4NeutronHPVector;
    theBuffer = nullptr;
    theIsotopeWiseData = nullptr;
    theFinalStates = nullptr;
    active = nullptr;
    registerCount = -1;
  }

  ~NeutronTHChannel() {
    delete theChannelData;
    // Following statement disabled to avoid SEGV
    // theBuffer is also deleted as "theChannelData" in
    // ~G4NeutronHPIsoData.  FWJ 06-Jul-1999
    //if(theBuffer != nullptr) delete theBuffer;
    if(theIsotopeWiseData != nullptr) delete [] theIsotopeWiseData;
    // Deletion of FinalStates disabled to avoid endless looping
    // in the destructor heirarchy.  FWJ 06-Jul-1999
    //if(theFinalStates != nullptr)
    //{
    //  for(i=0; i<niso; i++)
    //  {
    //    delete theFinalStates[i];
    //  }
    //  delete [] theFinalStates;
    //}
    // FWJ experiment
    //if(active!=nullptr) delete [] active;

  }

  G4double GetXsec(G4double energy);
  G4double GetWeightedXsec(G4double energy, G4int isoNumber);
  G4double GetFSCrossSection(G4double energy, G4int isoNumber);

  inline G4bool IsActive(G4int isoNumber) {
    return active[isoNumber];
  }
  inline G4bool HasFSData(G4int isoNumber) {
    return theFinalStates[isoNumber]->HasFSData();
  }
  inline G4bool HasAnyData(G4int isoNumber) {
    return theFinalStates[isoNumber]->HasAnyData();
  }

  void Init(G4Element * theElement, const G4String dirName);
  void Init(G4Element * theElement, const G4String dirName, const G4String fsType);

  G4bool Register(NeutronTHFinalState *theFS);
  void UpdateData(G4int A, G4int Z, G4int index, G4double abundance);
  void Harmonise(G4NeutronHPVector *& theStore, G4NeutronHPVector * theNew);
  G4HadFinalState * ApplyYourself(const G4HadProjectile & theTrack, G4int isoNumber=-1);

  inline G4int GetNiso() {
    return niso;
  }
  inline G4double GetN(G4int i) {
    return theFinalStates[i]->GetN();
  }
  inline G4double GetZ(G4int i) {
    return theFinalStates[i]->GetZ();
  }

  inline G4bool HasDataInAnyFinalState() {
    for(G4int i=0; i<niso; i++) {
      if(theFinalStates[i]->HasAnyData()) {
        return true;
      }
    }
    return false;
  }

private:

  G4NeutronHPVector * theChannelData;  // total (element) cross-section for this channel
  G4NeutronHPVector * theBuffer;

  NeutronTHIsoData * theIsotopeWiseData; // these are the isotope-wise cross-sections for each final state.
  NeutronTHFinalState ** theFinalStates; // also these are isotope-wise pionters, parallel to the above.
  G4bool * active;
  G4int niso;

  G4StableIsotopes theStableOnes;

  G4String theDir;
  G4String theFSType;
  G4Element * theElement;

  G4int registerCount;
};

#endif // NEUTRONTH_CHANNEL_HH
