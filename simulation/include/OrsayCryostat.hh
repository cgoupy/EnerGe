#ifndef ORSAY_CRYOSTAT_HH
#define ORSAY_CRYOSTAT_HH

#include <G4ThreeVector.hh>
#include "G4SystemOfUnits.hh"

#include<vector>
#include <map>

class G4VSolid;
class G4LogicalVolume;
///class G4VPhysicalVolume;

class OrsayCryostat
{
public:
    OrsayCryostat();
    G4LogicalVolume* ConstructCryostat(G4LogicalVolume* mother, const G4ThreeVector &Pos);
    inline double GetCryoInteriorHeight() const {return cryostat_interior_height;};
    void AddCryostatRack(G4LogicalVolume* mother, const G4ThreeVector &Pos);
    
protected:
    bool overlap_test;
    int construction_verbose_level;
    std::vector<std::pair<int, G4LogicalVolume*>> CryostatElements;
    void PrintElements() const;
    
private:
    G4double To3Digits(G4double x) const;
    
    const double cryostat_interior_height = 270.5*mm;
    const double cryostat_interior_radius = 118*mm;
    const double cryo_outer_height = 178.8*cm;                // To be checked
    
    //Position into the world
    G4ThreeVector cryostat_interior_position;
};

#endif // ORSAY_CRYOSTAT_HH
