#ifndef COVPROTOTYPE_HH
#define COVPROTOTYPE_HH

#include <DCVolumeHandler.hh>
#include <G4Box.hh>
#include <utility>
#include <G4ThreeVector.hh>

class G4LogicalVolume;
class G4VPhysicalVolume;

class COVPrototype
{
    
public:
    COVPrototype();
    void Build(G4LogicalVolume* mother, G4ThreeVector& Pos);
    
private:
    
    bool overlap_test;
    int construction_verbose_level;
    int detector_choice;
    bool copper_boxes;
};


#endif /* COVPROTOTYPE_HH */

