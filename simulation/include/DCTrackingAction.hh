#ifndef DC_TRACKING_ACTION_H
#define DC_TRACKING_ACTION_H

#include <set>
#include <G4UserTrackingAction.hh>
#include <globals.hh>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4Track;

class DCTrackingAction : public G4UserTrackingAction
{
public:
   void PreUserTrackingAction (const G4Track* aTrack);
   void PostUserTrackingAction(const G4Track*) {}

private:
  std::set<G4String> ProcessList;
  std::set<G4int> ParticleList;
  std::set<G4int> pi0List;
};


#endif


