#ifndef DC_DUMP_TRACKING_INFO_HH
#define DC_DUMP_TRACKING_INFO_HH 1

#include <vector>

class G4Step;
class G4Track;
class G4StepPoint;
class G4LogicalVolume;
class G4VPhysicalVolume;
class G4VProcess;
class G4VUserTrackInformation;
class G4Material;
class G4MaterialCutsCouple;
class G4VSensitiveDetector;
class G4DynamicParticle;
namespace CLHEP { class Hep3Vector; }

void DumpStepInfo(const G4Step* step);
void DumpFullStepInfo(const G4Step* step);
void DumpTrackInfo(const G4Track* track);
void DumpFullTrackInfo(const G4Track* track);
void DumpFullStepPointInfo(const G4StepPoint* point);
void DumpFullTrackVector(const std::vector<G4Track*>* v);

void DumpLogicalVolume(const G4LogicalVolume* lvol);
void DumpPhysicalVolume(const G4VPhysicalVolume* pvol);
void DumpProcess(const G4VProcess* proc);
void DumpUserInformation(const G4VUserTrackInformation* info);
void DumpMaterial(const G4Material* mat);
void DumpMaterialCutsCouple(const G4MaterialCutsCouple* mat);
void DumpSensitiveDetector(const G4VSensitiveDetector* det);
void DumpPointVector(const std::vector<CLHEP::Hep3Vector>* v);
void DumpDynamicParticle(const G4DynamicParticle* part);

#endif // DC_DUMP_TRACKING_INFO_HH
