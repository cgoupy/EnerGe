#ifndef DC_PHYSICS_LIST_H
#define DC_PHYSICS_LIST_H 1

#include <G4Types.hh>
#include <G4VUserPhysicsList.hh>

class DCPhysicsList: public G4VUserPhysicsList {
public:
    DCPhysicsList();
    virtual ~DCPhysicsList() {}
    DCPhysicsList(const G4VUserPhysicsList& ) = delete;
    
private:
    virtual void ConstructParticle();
    virtual void ConstructProcess();
    virtual void SetCuts();
    
    void ConstructAllParticles();
    void ConstructOnlyLowEnergyParticles();
    
    void ConstructEM();
    void ConstructHadronic();
    void ConstructHadronic_LowEnergy();
    void ConstructHadronic_HighEnergy();
    
    void ConstructDecay();
    void ConstructIonDecay();
    void ConstructFermionDecay();
    
    void ConstructOpticalProcesses();
    void ConstructScintillation();
    void ConstructAttenuation();
    void ConstructCerenkov();
    
    void ConstructDeferTrackProcess();
    
    void AssignCutsToRegion(G4String regName, G4double &gamma_cut, G4double &electron_cut, G4double &positron_cut, G4double &proton_cut);
    inline void AssignCutsToRegion(G4String regName, G4double &cut) {AssignCutsToRegion(regName,cut,cut,cut,cut);}
    
    void AddParametrisation();
    void DumpProcessTable();
    
    G4bool EnableHighEnergyPhysics;
    G4bool EnableOptics;
    G4bool UseGLG4Scintillation;
    G4bool UseGLG4Attenuation;
    G4bool EnableGLG4DeferTrackProc;
    G4bool UseCerenkov;
    G4bool UseFluo;
    G4bool UseAuger;
    G4bool UsePIXE;
    G4bool UseNeutronTH; //to activate neutronTH module (deprecated...)
    G4bool UseNeutronHPThermal; //to activate the use of thermal neutron libraries in the neutronHP package
    G4int  HadronicModel;
    G4int  EmModel;
    G4int  OpticsVerboseLevel;
    G4int  PhysicsVerboseLevel;
    
    G4double photon_cut_value;
    G4double neutron_cut_value;
    G4double generic_ion_cut_value;
    
    std::map<G4String, G4double> RegionCuts;
    
    static G4double theNeutronMin;
    static G4double theNeutronTHElasticMax;
    static G4double theNeutronTHCaptureMax;
    static G4double theNeutronHPMax;
};

#endif
