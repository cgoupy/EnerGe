#ifndef WORLD_BUILDER_EXPERIMENTAL_SITE
#define WORLD_BUILDER_EXPERIMENTAL_SITE

#include <vector>
#include <G4ThreeVector.hh>
#include <G4SystemOfUnits.hh>
#include <G4MultiUnion.hh>

class G4VPhysicalVolume;
class G4LogicalVolume;
class Shielding;
class G4VSolid;

namespace WorldBuilder{

/// EXPERIMENTAL SITE BASE CLASS ///

class ExperimentalSite {
    
public:
    ExperimentalSite();
    virtual ~ExperimentalSite(){};
    
    virtual G4VPhysicalVolume* BuildWorld() = 0;
    
protected:
    
    bool overlap_test;
    int construction_verbose_level;
    double worldsize;
    
    G4ThreeVector DetPos, ShieldingPos;
    void AddDetector(G4LogicalVolume* mother);
    void AddShielding(G4LogicalVolume* mother);
    
};

class EmptySite: public ExperimentalSite
{
public:
    EmptySite();
    virtual G4VPhysicalVolume* BuildWorld();
};


class OrsayLab: public ExperimentalSite
{
public:
    OrsayLab();
    virtual G4VPhysicalVolume* BuildWorld();
    
private:
    G4LogicalVolume* AddCryostat(G4LogicalVolume* mother);
};

struct Room {
    double length;
    double width;
    double height;
};

class VeryNearSite: public ExperimentalSite
{
public:
    VeryNearSite();
    virtual G4VPhysicalVolume* BuildWorld();
    
private:
    
    struct BuildingDimensions {
        double wall_thickness     = 20*cm;
        double floor_thickness    = 22*cm;
        double VNS_ceiling_thickness = 16*cm;
        double ceiling_height[8]  = {3.205*m, 3.055*m, 3.08*m, 3.08*m, 3.08*m, 3.08*m, 3.08*m, 3.08*m};
        int    nb_floors          = sizeof(ceiling_height)/sizeof(double);
        double height             = 23.20*m;
        double length_min         =  7.90*m;
        double length_max         = 35.23*m;
        double width_min_basement =  10.35*m;
        double width_min          =  11.98*m;
        double width_max          =  14.89*m;
        double rStaircase         =  1.50*m + wall_thickness;
        double rock_thickness     = 3.0*m;
        double window_width       = 140*cm;
        double window_height      = 165*cm;
        double window_z           = 95*cm;
        double window_gap         = 10*cm;
        double part_wall_thick    = 13*mm;
    };
    
    Room VNS{6.02*m,4.35*m,2.9*m};
    G4ThreeVector VNSCentre;
    
    inline G4ThreeVector GetVNSCentre() const {return VNSCentre;}
    
    BuildingDimensions building;
    G4ThreeVector BuildingCentre;
    
    inline G4ThreeVector GetBuildingCentre() const {return BuildingCentre;}
    
    G4LogicalVolume* AddBuilding(G4LogicalVolume* logicWorld);
    inline double GetBuildingDimension(){return sqrt(pow(building.height,2)+pow(building.width_max/2.,2)+pow(building.length_max/2.,2));};
    
    G4VSolid* BuildingShape(const G4String& name, G4double width_min, G4double mid_height, G4double height, G4double dthick) const;
};

}
#endif
