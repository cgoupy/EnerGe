// This file is part of the GenericLAND software library.
// Author: Glenn Horton-Smith
// Modified by M. Fechner for Nucifer, 2009-2010
// Modified by J. Gaffiot for Nucifer, jan 2015
// Modified by M. Vivier for Choozerent, mar 2017

#ifndef DC_STEPPING_ACTION_H
#define DC_STEPPING_ACTION_H 1

#include <G4UImessenger.hh>
#include <G4UserSteppingAction.hh>
#include <G4String.hh>

class DCEventAction;
class G4UIcommand;
class G4Step;

class GLG4DebugMessenger_Stepping : public G4UImessenger
{
public:
  GLG4DebugMessenger_Stepping();
  void SetNewValue(G4UIcommand * command,G4String newValues);
};

class DCSteppingAction : public G4UserSteppingAction
{
public:
  DCSteppingAction(DCEventAction* r);

  void UserSteppingAction(const G4Step* aStep);

private:
  void NeutronCapture(const G4Step* aStep);
  void NeutronInelasticScattering(const G4Step* aStep);

  DCEventAction* myEventAction = nullptr;
  G4int VerboseLevel = 0;
};

#endif
