#ifndef DC_TRAJECTORY_H
#define DC_TRAJECTORY_H 1

#include <G4Types.hh>
#include <G4Trajectory.hh>
#include <G4Allocator.hh>

class G4ParticleDefinition;
class G4Track;

class DCTrajectory : public G4Trajectory
{
public:
  virtual ~DCTrajectory() {}
  DCTrajectory() {}
  DCTrajectory(const G4Track* aTrack);
  DCTrajectory(DCTrajectory &);

  virtual void DrawTrajectory() const;

  inline void* operator new(size_t);
  inline void  operator delete(void*);

  inline void SetDrawTrajectory(G4bool b) { drawit=b; }
  inline void SetForceDrawTrajectory(G4bool b) { forceDraw=b; }
  inline void SetForceNoDrawTrajectory(G4bool b) { forceNoDraw=b; }
  
private:
  G4bool wls = false;
  G4bool drawit = false;
  G4bool forceNoDraw = false;
  G4bool forceDraw = false;
  G4ParticleDefinition* particleDefinition = nullptr;
};

extern G4Allocator<DCTrajectory> DCTrajectoryAllocator;

inline void* DCTrajectory::operator new(size_t)
{
  void* aTrajectory;
  aTrajectory = (void*)DCTrajectoryAllocator.MallocSingle();
  return aTrajectory;
}

inline void DCTrajectory::operator delete(void* aTrajectory)
{
  DCTrajectoryAllocator.FreeSingle((DCTrajectory*)aTrajectory);
}

#endif
