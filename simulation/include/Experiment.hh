#ifndef EXPERIMENT_H
#define EXPERIMENT_H

#include <G4VUserDetectorConstruction.hh>

class G4VPhysicalVolume;

class Experiment : public G4VUserDetectorConstruction
{
public:
  Experiment(const G4String& output_dir): DirPropVec(output_dir),constructionVerbosity(0){}
  virtual ~Experiment();

  G4VPhysicalVolume* Construct();
  void RemoveOpticFile();
  void PrintWorldVolumes() const;

private:
  G4String DirPropVec;
  std::string OpticsFileName;
  int constructionVerbosity;
  int ExperimentSiteChoice;
  
  void ReadBasicDataBase();
  void CleanGeometry();
  void SetUpOtpicalProperties();
  G4VPhysicalVolume* ConstructWorld() const;
  
};

#endif
