#ifndef DC_RUN_ACTION_H
#define DC_RUN_ACTION_H 1

#include <G4UserRunAction.hh>
#include <G4String.hh>

#include <DataDefinition.h>

#include <RtypesCore.h>
#include <TString.h>

#include <unordered_map>
#include <memory>
#include <iosfwd>

class G4Run;
class G4Step;
class G4Material;
class G4ParticleDefinition;
class G4Region;

class TFile;
class TTree;
class TClonesArray;
class TH1D;

class DCRunAction : public G4UserRunAction {
public:
    virtual ~DCRunAction();
    DCRunAction(const G4String& out_file, const G4String& mac_file);
    
    void BeginOfRunAction(const G4Run*);
    virtual void EndOfRunAction(const G4Run*);
    void Clear();
    
    inline MCInfo*      GetMCInfo()      { return MCINFO;}
    
    inline void IncreaseNbEvtGenerator() { NbEvtGenerator++; }
    inline void IncreaseNbEvtDelayed()   { NbEvtDelayed++; }
    inline void IncreaseNbEvtOther()     { NbEvtOther++; }
    
    virtual Int_t FillEvent() =0;
    //inline virtual void RecordEnergyDeposition(G4int, const G4Step*) {}
    
protected:
    TString FileName;
    ULong64_t NbEvtGenerator=0ull, NbEvtDelayed=0ull, NbEvtOther=0ull;
    
    //pointers towards all the components of the event (tree branches)
    MCInfo*       MCINFO=nullptr;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class DCRunAction_ROOT : public DCRunAction {
public:
    virtual ~DCRunAction_ROOT();
    DCRunAction_ROOT(const G4String& out_file, const G4String& mac_file);
    
    virtual void EndOfRunAction(const G4Run*);
    
    virtual Int_t FillEvent();
    //virtual void RecordEnergyDeposition(G4int PDG, const G4Step* step);
    
private:
    
    void GetMaterialsFromGeometry();
    std::map<G4String,G4int> GetListOfEMProcesses(G4ParticleDefinition* thePart);
    void SaveEMProcesses(G4ParticleDefinition* thePart);
    void GetListOfRegions();
    
private:
    struct DelTree { void operator()(TTree* t); };
    struct DelFile { void operator()(TFile* p); };
    std::unique_ptr<TFile> SaveRootFile;
    std::unique_ptr<TTree,DelTree> theTree;

    std::vector<G4Region*> theListOfRegions;
    //std::unordered_map<int,std::unique_ptr<TH1D>> mEdep, mEvertex, mEkinPre, mEkinPost, mEstep, mEdepNeutron, mEkinNeutron;
};

#endif
