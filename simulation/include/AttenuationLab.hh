#ifndef WORLD_BUILDER_ATTENUATION_LAB
#define WORLD_BUILDER_ATTENUATION_LAB

#include <G4SystemOfUnits.hh>
#include <DCVolumeHandler.hh>
#include <G4Box.hh>
#include <string>

class G4VPhysicalVolume;

namespace WorldBuilder{
  
  class AttenuationLab{
    
    struct Slab{
      
      double thickness;
      std::string material;
      
    };
    
    double world_half_size;
    Slab slab;

  public:
    AttenuationLab(double world_size, Slab slab);
    G4VPhysicalVolume* BuildWorld() const;
    
  private:
    bool overlap_test;
    int construction_verbose_level;
  };
  
}

#endif
