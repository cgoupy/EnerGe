#ifndef CEAPORTABLEHPGE_HH
#define CEAPORTABLEHPGE_HH

#include <DCVolumeHandler.hh>
#include <G4Box.hh>
#include <utility>
#include <G4ThreeVector.hh>

class G4LogicalVolume;
class G4VPhysicalVolume;

class CEAportableHPGe
{
    
public:
    CEAportableHPGe();
    void Build(G4LogicalVolume* mother, G4ThreeVector& Pos);
    
private:
    
    bool overlap_test;
    int construction_verbose_level;
};


#endif /* CEAPORTABLEHPGE_HH */
