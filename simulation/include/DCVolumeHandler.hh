#ifndef DC_VOLUME_HANDLER_HH
#define DC_VOLUME_HANDLER_HH 1

#include <GLG4Material.hh>

#include <Parameters.hh>
#include <String.hh> // for cat

#include <globals.hh>
#include <G4CSGSolid.hh>
#include <G4LogicalVolume.hh>
#include <G4VPhysicalVolume.hh>
#include <G4PVPlacement.hh>
#include <G4Material.hh>
#include <G4ThreeVector.hh>
#include <G4String.hh>

#include <cstddef> // for std::nullptr_t
#include <utility> // for std::forward
#include <map>

//////////////////////////////////////////////////////////////////////////////////

class DCVVolumeHandler;
using DCVolumeHandlerList = std::map<G4String, DCVVolumeHandler*>;

//////////////////////////////////////////////////////////////////////////////////

class DCVVolumeHandler
{
protected:
    virtual ~DCVVolumeHandler() {}
    DCVVolumeHandler() {}
    DCVVolumeHandler(const DCVVolumeHandler& ) = delete;
    
protected:
    G4CSGSolid* theSolid=nullptr;
    G4LogicalVolume* theLogic=nullptr;
    G4VPhysicalVolume* thePhysic=nullptr;
    
public:
    inline G4CSGSolid*        GetSolid()  const { return theSolid; }
    inline G4LogicalVolume*   GetLogic()  const { return theLogic; }
    inline G4VPhysicalVolume* GetPhysic() const { return thePhysic; }
    
protected:
    static DCVolumeHandlerList theVolumeList;
    
public:
    static const DCVolumeHandlerList& GetDCVolumeHandlerList() { return theVolumeList; }
    static DCVVolumeHandler const * GetDCVolumeHandler(const G4String& name) {
        if (theVolumeList.count(name) == 0) { return nullptr; }
        return theVolumeList.at(name);
    }
    
protected:
    constexpr static std::nullptr_t NoRot = nullptr;
    constexpr static G4bool NoMany = false;
    constexpr static G4int NoCopy = 0;
};

//////////////////////////////////////////////////////////////////////////////////

template<class solid>
class DCVolumeHandler : public DCVVolumeHandler
{
public:
    virtual ~DCVolumeHandler() {}
    DCVolumeHandler(const DCVolumeHandler& ) = delete;
    DCVolumeHandler() = delete;
    
    template<class... Args>
    DCVolumeHandler(const G4String& name, const G4String& material, G4LogicalVolume* mother, G4RotationMatrix* rot, const G4ThreeVector& pos, Args... par);
    template<class... Args>
    DCVolumeHandler(const G4String& name, const G4String& material, const G4String& mother, G4RotationMatrix* rot, const G4ThreeVector& pos, Args... par):
    DCVolumeHandler(name,material,theVolumeList.at(mother)->GetLogic(),rot,pos,par...) {}
    template<class... Args>
    DCVolumeHandler(const G4String& name, const G4String& material, G4LogicalVolume* mother, G4RotationMatrix* rot, G4double x, G4double y, G4double z, Args... par):
    DCVolumeHandler(name,material,mother,rot,G4ThreeVector(x,y,z),par...) {}
    template<class... Args>
    DCVolumeHandler(const G4String& name, const G4String& material, const G4String& mother, G4RotationMatrix* rot, G4double x, G4double y, G4double z, Args... par):
    DCVolumeHandler(name,material,theVolumeList.at(mother)->GetLogic(),rot,G4ThreeVector(x,y,z),par...) {}
    template<class... Args>
    DCVolumeHandler(const G4String& name, const G4String& material, G4LogicalVolume* mother, const G4ThreeVector& pos, Args... par):
    DCVolumeHandler(name,material,mother,NoRot,pos,par...) {}
    template<class... Args>
    DCVolumeHandler(const G4String& name, const G4String& material, const G4String& mother, const G4ThreeVector& pos, Args... par):
    DCVolumeHandler(name,material,theVolumeList.at(mother)->GetLogic(),NoRot,pos,par...) {}
    template<class... Args>
    DCVolumeHandler(const G4String& name, const G4String& material, G4LogicalVolume* mother, G4double x, G4double y, G4double z, Args... par):
    DCVolumeHandler(name,material,mother,NoRot,G4ThreeVector(x,y,z),par...) {}
    template<class... Args>
    DCVolumeHandler(const G4String& name, const G4String& material, const G4String& mother, G4double x, G4double y, G4double z, Args... par):
    DCVolumeHandler(name,material,theVolumeList.at(mother)->GetLogic(),NoRot,G4ThreeVector(x,y,z),par...) {}
};

//////////////////////////////////////////////////////////////////////////////////

template<class solid>
using Volume = DCVolumeHandler<solid>;

//////////////////////////////////////////////////////////////////////////////////

template<class solid> template<class... Args>
DCVolumeHandler<solid>::DCVolumeHandler(const G4String& name, const G4String& material,
                                        G4LogicalVolume* mother, G4RotationMatrix* rot, const G4ThreeVector& pos, Args... par): DCVVolumeHandler()
{
    if (theVolumeList.count(name) == 0) {
        theVolumeList[name] = this;
    } else {
        G4Exception("DCVolumeHandler::DCVolumeHandler","1",FatalException, scat(" Handler already exists: ",name).c_str() );
    }
    
    theSolid = new solid("VHsolid_"+name, std::forward<Args>(par)...);
    theLogic = new G4LogicalVolume(theSolid, G4Material::GetMaterial(material), "VHlogic_"+name);
    thePhysic = new G4PVPlacement(rot, pos, theLogic, "VHphysic_"+name, mother, NoMany, NoCopy, ParamBase::GetDataBase()["overlap_test"]);
    
    if ( GLG4Material::GetVisAttribute(material) ) {
        theLogic->SetVisAttributes(GLG4Material::GetVisAttribute(material));
    } else {
        G4Exception("DCVolumeHandler::DCVolumeHandler","2",JustWarning,
                    scat(" Material has no predefined color: ",material).c_str() );
        theLogic->SetVisAttributes(GLG4Material::GetVisAttribute("Default"));
    }
}
#endif // DCVOLUMEHANDLER_HH
