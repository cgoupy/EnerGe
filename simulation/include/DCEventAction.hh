#ifndef DC_EVENT_ACTION_H
#define DC_EVENT_ACTION_H 1

#include <GLG4VEventAction.hh>

#include <Index.h>
#include <DCRunAction.hh>

#include <G4Types.hh>
#include <G4String.hh>

#include <Rtypes.h>

#include <map>


//added
#include <DataDefinition.h>

//forward declaration of useful classes defined in "DataDef" library
class MCInfo;
class MCEvent;
class Particle;

class G4VPhysicalVolume;
class G4VProcess;
class G4Event;
class G4Step;
class G4Track;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class DCEventAction  : public GLG4VEventAction {
public:
    DCEventAction(DCRunAction* Run);
    virtual ~DCEventAction();
    
    inline virtual void OpenFile(const G4String&, Bool_t) {}
    inline virtual void CloseFile() {}
    virtual void Clear();
    
    inline virtual void BeginOfEventAction(const G4Event*) { Clear(); }
    virtual void EndOfEventAction(const G4Event* evt);
    virtual void FillData(const G4Event*);
    
    // link with SteppingAction
    void TreatPrimaryParticleFirstStep(const G4Step* step);
    void TreatPrimaryParticleNextStep(const G4Step* step);
    void TreatSecondaryParticleStep(const G4Step* step);
    void TreatNeutronEnding(const G4Step* current_step, const G4Track* recoil_track, G4bool isCapture);
    void PrintSecondaryInfo(const G4Step *Step, const G4String& VolumeName);
    void RecordSecondaries(const G4Step *step, MCEvent* myMCEvent, const G4String& VolumeName);
    
    inline void AddOpticalPhoton()    { NbOpticalPhoton++; }
    inline void AddNonOpticalPhoton() { NbNonOpticalPhotons++; }
    //inline void RecordEnergyDeposition(G4int PDG, const G4Step* step)  { runaction->RecordEnergyDeposition(PDG,step); }
    
private:
    // string to integer
    EnerGe::Volumes Physical2Number(const G4VPhysicalVolume*);
    Int_t Process2Number(const G4VProcess*);
    
    DCRunAction* runaction = nullptr;
    //G4bool Keep;
    G4bool UseVisualization=false;
    G4bool SaveSecondaries=false;
    G4bool KillTrack=false;
    G4int VerboseLevel=0;
    std::string SecondaryVolumeName;
    
    MCInfo*      myMC = nullptr;
    
    Int_t NbPrimary=0; // number of primary particles to store in MCInfo
    Int_t NbSecondary=0;
    std::map< Int_t,Int_t > ParticleMap; // link between track ID and primary number
    std::map< Int_t,Int_t > SecondaryParticleMap;
    std::map< const G4VPhysicalVolume*, EnerGe::Volumes > VolumeMap; // link between physical volume and EnerGe code
    ULong64_t NbOpticalPhoton=0ull, NbNonOpticalPhotons=0ull; // steps counters
    
    Double_t UT=0.;
    
    
};

#endif
