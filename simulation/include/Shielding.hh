#ifndef SHIELDING_HH
#define SHIELDING_HH

#include <DCVolumeHandler.hh>
#include <G4Box.hh>
#include <utility>
#include <G4ThreeVector.hh>

#include <G4Trd.hh>
#include <G4Trap.hh>

class G4LogicalVolume;
class G4VPhysicalVolume;


////// SHIELDING BASE CLASS ///////
class Shielding {
public:
    Shielding();
    virtual ~Shielding() {};
    
    //to get the cubic shielding inner volume dimensions
    inline virtual G4double GetInnerSizeX() = 0;
    inline virtual G4double GetInnerSizeY() = 0;
    inline virtual G4double GetInnerSizeZ() = 0;
    
    inline virtual G4double GetExternalSizeX() = 0;
    inline virtual G4double GetExternalSizeY() = 0;
    inline virtual G4double GetExternalSizeZ() = 0;
    
protected:
    bool overlap_test;
    int construction_verbose_level;
    std::vector<G4LogicalVolume*> ShieldingLayers;
    void PrintShieldingMass() const;
    
private:
    std::string material;
};


class OrsayLeadShielding: public Shielding {
public:
    OrsayLeadShielding(G4double ExternalSizeX, G4double ExternalSizeY, G4double ExternalSizeZ, G4double Thickness, G4double InnerSizeX, G4double InnerSizeY, G4double InnerSizeZ);
    virtual void ConstructOld(G4LogicalVolume* mother, const G4ThreeVector &Pos);
    virtual void ConstructNew(G4LogicalVolume* mother, const G4ThreeVector &Pos);
    
    inline virtual G4double GetExternalSizeX() {return ExternalSizeX;};
    inline virtual G4double GetExternalSizeY() {return ExternalSizeY;};
    inline virtual G4double GetExternalSizeZ() {return ExternalSizeZ;};
    
    inline virtual G4double GetInnerSizeX() {return InnerSizeX;};
    inline virtual G4double GetInnerSizeY() {return InnerSizeY;};
    inline virtual G4double GetInnerSizeZ() {return InnerSizeZ;};
    
private:
    std::string material= "Lead";
    G4double ExternalSizeX;
    G4double ExternalSizeY;
    G4double ExternalSizeZ;
    G4double Thickness;
    G4double InnerSizeX;
    G4double InnerSizeY;
    G4double InnerSizeZ;
};

/////////////////////////////////////
/////// NUCLEUS SHIELDING ////////
/////////////////////////////////////

struct Layer{

double thickness;
double inner_size_x;
double inner_size_y;
double inner_size_z;
std::string material;

};

class NUCLEUSLeadShielding: public Shielding {
    
    Layer outLayer;

  
public:
    NUCLEUSLeadShielding(Layer outLayer);
    virtual G4LogicalVolume* Construct(G4LogicalVolume* mother, const G4ThreeVector &Pos);

    void AddMuonVeto(G4LogicalVolume* mother, const G4ThreeVector &Pos);
    void AddSupportFrame(G4LogicalVolume* mother, const G4ThreeVector &Pos);
    void AddColdPassiveShield(G4LogicalVolume* mother, const G4ThreeVector &Pos);
    
    //Inner size
    inline virtual double GetInnerSizeX() {return outLayer.inner_size_x;};
    inline virtual double GetInnerSizeY() {return outLayer.inner_size_y;};
    inline virtual double GetInnerSizeZ() {return outLayer.inner_size_z;};
    
    // Thickness of steel plate between mid-layer-up and out-layer-up
    const G4double steel_plate_thickness = 10*CLHEP::mm;

    //External size
    inline virtual double GetExternalSizeX() {return outLayer.inner_size_x+outLayer.thickness*2;}
    inline virtual double GetExternalSizeY() {return outLayer.inner_size_y+outLayer.thickness*2;}
    inline virtual double GetExternalSizeZ() {return outLayer.inner_size_z+outLayer.thickness*2+steel_plate_thickness;}
    
private:
    G4VSolid* ConstructLayer(const G4String &name, G4double OuterHeight, G4double OuterWidth, G4double InnerHeight, G4double InnerWidth, G4double rHole);
    void AddCaseMV_A(const G4String &name, G4LogicalVolume* mother, const G4ThreeVector &Pos, G4int type);
    void AddCaseMV_B(const G4String &name, G4LogicalVolume* mother, const G4ThreeVector &Pos, G4int type);
    void AddCaseMV_C(const G4String &name, G4LogicalVolume* mother, const G4ThreeVector &Pos, G4int type);
    void AddCaseMV_D(const G4String &name, G4LogicalVolume* mother, const G4ThreeVector &Pos, G4int type);
    void AddCaseMV_I(const G4String &name, G4LogicalVolume* mother, const G4ThreeVector &Pos, G4double dw, G4int type);
    void AddNorcan(const G4String &name, G4LogicalVolume* mother, const G4ThreeVector &Pos);
};

#endif /* SHIELDING_HH */

