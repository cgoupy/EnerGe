#ifndef DC_EXCEPTION_HANDLER
#define DC_EXCEPTION_HANDLER 1

#include <G4VExceptionHandler.hh>
#include <G4ExceptionSeverity.hh>

class DCExceptionHandler : public G4VExceptionHandler {
public:
  virtual ~DCExceptionHandler() {}

  DCExceptionHandler(): G4VExceptionHandler() {}
  DCExceptionHandler(const G4VExceptionHandler &right) = delete;
  DCExceptionHandler& operator=(const DCExceptionHandler &right) = delete;

  inline G4int operator==(const DCExceptionHandler &right) const { return (this == &right); }
  inline G4int operator!=(const DCExceptionHandler &right) const { return (this != &right); }

public:
  virtual G4bool Notify(const char* originOfException, const char* exceptionCode,
			G4ExceptionSeverity severity, const char* description) ;
    // If TRUE returned, core dump will be generated, while FALSE returned, program execution continues.
};

#endif // DC_EXCEPTION_HANDLER
