#include <Parameters.hh>

#include <DCPhysicsList.hh>
#include <Experiment.hh>
#include <DCRunAction.hh>
#include <DCTrackingAction.hh>
#include <DCSteppingAction.hh>
#include <DCEventAction.hh>
#include <DCExceptionHandler.hh>

#include <GLG4PrimaryGeneratorAction.hh>

#include <G4RunManager.hh>
#include <G4UImanager.hh>
#include <G4UIsession.hh>
#include <G4UIterminal.hh>
#include <G4UItcsh.hh>
#include <G4StateManager.hh>

#include <CLHEP/Random/Random.h>
#include <CLHEP/Random/RanluxEngine.h>

//#ifdef G4VIS_USE

#include <G4VisManager.hh>
#include <G4VisExecutive.hh>
#include <G4TrajectoryDrawByParticleID.hh>
#include <G4Colour.hh>

#ifdef G4VIS_USE_OPENGLQT
# include <G4UIQt.hh>
# include <G4Qt.hh>
#endif

#ifdef G4UI_BUILD_WT_SESSION
# include <G4UIWt.hh>
# include <G4Wt.hh>
#endif

#ifdef G4UI_BUILD_XM_SESSION
# include <G4UIXm.hh>
#endif

//#endif

#include <iostream>
#include <cstdlib>
#include <ctime>    // std::time
#include <unistd.h> // getpid()

G4String GetDataPath(const char* energe_path, const char* energe_data){

  G4String data_path;
  
  if(!std::getenv(energe_path)) {
    
    if(!std::getenv(energe_data))
      G4Exception("EnerGe_sim","1",FatalException,"ENERGE environment variables must be defined!\n");
    else data_path = std::getenv(energe_data);
    
  }
  else{
    
    data_path = std::getenv(energe_path);
    data_path += "/simulation/data";
    setenv(energe_data,data_path.c_str(),1);

  }

  return data_path;
  
}

G4int main(G4int argc, char** argv)
{
    if ( argc <= 1)  {
        std::cerr << "Usage:\n"
        << " EnerGe_sim [-b|-i] [-k] [-s seed] [ -m mac_file -o root_file ] [-f opt file]\n"
        << "  Options:\n"
        << "  -b: batch mode\n"
        << "  -i: interactive mode (default if a mac file is given)\n"
        << "  -s seed: set random generator seed\n"
        << "  -m mac_file: input macro file\n"
        << "  -o root_file: output root file\n"
        << "  -f opt file: option file (default: EnerGe_Options.dat)\n";
        return 0;
    }

    char c;
    G4bool batch=false;
    G4int seed=-1;
    G4String outfile, macfile, optfile;
    while ( (c=getopt(argc,argv,"m:o:f:s:bi")) != -1) {
        switch (c) {
            case 'b': batch=true; break;
            case 'i': batch=false; break;
            case 's': seed = atoi(optarg); break;
            case 'm': macfile = G4String(optarg); break;
            case 'o': outfile = G4String(optarg); break;
            case 'f': optfile = G4String(optarg); break;
            case '?':
                if (optopt == 'm' || optopt == 'o' || optopt == 'f' || optopt == 's') {
                    std::cerr << " Option -" << optopt << " needs an argument\n";
                } else if ( isprint(optopt)) {
                    std::cerr << " Unkown option " << optopt << "\n";
                } else {
                    std::cerr << " Incorrect argument...\n";
                }
                return 1;
            default:
                std::cerr << " Incorrect option: "<<c<<"\n";
                return 2;
        }
    }
    
    if(!std::ifstream(macfile.c_str())) 
      throw std::runtime_error("Could not open: "+macfile);

    // manage the random number generator
    //CLHEP::HepRandom::setTheEngine(new CLHEP::RanecuEngine());
    CLHEP::HepRandom::setTheEngine(new CLHEP::RanluxEngine());
    if (seed < 0) {
        srand(std::time(nullptr)+getpid());
        seed = rand();
    }
    CLHEP::HepRandom::getTheEngine()->setSeed(seed,5);

    // -- database
    auto data_path = GetDataPath("ENERGE_PATH", "ENERGE_DATA");
    ParamBase &InputDB ( ParamBase::GetInputDataBase() );
#ifdef USE_SCINTILLATOR
    InputDB.ReadFile((data_path+"/PMT_card.dat").data()); // add PMT options and settings to parameter list
    InputDB.ReadFile((data_path+"/fluids_card.dat").data()); // add scintillators/fluids-specific parameters to parameter list
#endif
    if ( optfile == "") { optfile = data_path + "/EnerGe_Options.dat"; }// add EnerGe options and settings to parameter list
    InputDB.ReadFile(optfile.data());
    if (batch) {
        std::cout<<"=======================================================\n";
        std::cout<<"          +++ Parameters at initialization +++         \n";
        ParamBase::GetDataBase().WriteText(std::cout);
        std::cout<<"========== End of parameters at initialization ========\n";
    }

    // set mandatory initialization classes
    G4RunManager runManager;

    // set exception handler for G4Exception
    // DCExceptionHandler has to be created AFTER G4RunManager because G4RunManagerKernel contain a G4ExceptionHandler,
    // which if created after DCExceptionHandler would crush our DCExceptionHandler in G4StateManager
    new DCExceptionHandler();

    // geometry from G4VUserDetectorConstruction
    G4String output_dir{outfile.substr(0,outfile.find_last_of("/")+1)}; // take path from output file
    if (output_dir.empty()) output_dir = "./";

    // set detector construction from G4VUserDetectorConstruction
    runManager.SetUserInitialization(new Experiment(output_dir));

    //  use own DC physics list from G4VUserPhysicsList
    runManager.SetUserInitialization(new DCPhysicsList());

    // set mandatory user action class from G4VUserPrimaryGeneratorAction
    runManager.SetUserAction(GLG4PrimaryGeneratorAction::GetTheGLG4PrimaryGeneratorAction());

    // set run action class from G4UserRunAction
    DCRunAction* run_action = new DCRunAction_ROOT(outfile, macfile); // name of the output ROOT file and input mac file as arguments
    runManager.SetUserAction(run_action);

    //set event action from GLG4VEventActionP
    DCEventAction* event_action = new DCEventAction(run_action);
    runManager.SetUserAction(event_action);

    // set tracking action class from G4UserTrackingAction
    runManager.SetUserAction(new DCTrackingAction());

    // set stepping action class from G4UserSteppingAction
    runManager.SetUserAction(new DCSteppingAction(event_action));

    // Initialize G4 kernel
    runManager.Initialize();

    // Get the pointer to the UI manager
    G4UImanager* UI = G4UImanager::GetUIpointer();

//#ifdef G4VIS_USE
    // Visualization managers
    G4VisManager* visManager = nullptr;

    const ParamBase& db(ParamBase::GetDataBase());
    G4int visu = static_cast<G4int>(db["use_visualization"]);
    
    if (visu) {
        std::cout<<"=======================================================\n";
        std::cout<<"           +++ Setting the visualization +++           \n";
        /*
         Simple graded message scheme - give first letter or a digit:
         0) quiet,         // Nothing is printed.
         1) startup,       // Startup and endup messages are printed...
         2) errors,        // ...and errors...
         3) warnings,      // ...and warnings...
         4) confirmations, // ...and confirming messages...
         5) parameters,    // ...and parameters of scenes and views...
         6) all            // ...and everything available.
         */

        G4int VisuVerboseLevel = static_cast<G4int>(db["visualization_verboselevel"]);
        visManager = new G4VisExecutive("all");
        visManager->SetVerboseLevel(VisuVerboseLevel);
        visManager->Initialize();

        // change default : draw particles by ID
//        G4TrajectoryDrawByParticleID* model = new G4TrajectoryDrawByParticleID("better");
//        model->SetDefault(G4Colour::Cyan());
//        // kill the optical photons
//        //  model->Set("optical_photon", G4Colour(0.0,0.0,0.0)); // no color
//        model->Set("optical_photon", G4Colour::White());
//        model->Set("gamma",          G4Colour::Green());
//        model->Set("e+",             G4Colour::Yellow());
//        model->Set("e-",             G4Colour::Magenta());
//        model->Set("neutron",        G4Colour::Blue());
//
//        visManager->RegisterModel(model);
//        visManager->SelectTrajectoryModel("better");

        UI->ApplyCommand("/vis/scene/create");
    }
//#endif

    // List of viewers
    /* Candidates : ATree DAWNFILE HepRepXML HepRepFile RayTracer VRML1FILE VRML2FILE gMocrenFile
     * OGL OGLI OGLS OGL_FALLBACK OGLI_FALLBACK OGLS_FALLBACK OGLIX OGLSX OGLIQt OGLSQt OGLIQt_FALLBACK OGLSQt_FALLBACK*/
    //  OpenGLImmediateQt (OGLI, OGLIQt)
    //  OpenGLImmediateX (OGLIX, OGLI_FALLBACK, OGLIQt_FALLBACK)
    //  OpenGLStoredQt (OGL, OGLS, OGLSQt)
    //  OpenGLStoredX (OGLSX, OGL_FALLBACK, OGLS_FALLBACK, OGLSQt_FALLBACK)

    G4UIsession* theSession = nullptr;
    
    if ( not batch ) {
//#ifdef G4VIS_USE
        switch (visu) {
            case 1: theSession = new G4UIterminal(new G4UItcsh);
                std::cout << " Creating UItcsh\n";
                UI->ApplyCommand("/vis/open OGLSX");
                std::cout << " Opening OGLSX\n";
                break;
#ifdef G4VIS_USE_OPENGLQT
            case 2: theSession = new G4UIQt(0,nullptr);
                std::cout << " Creating UIQt\n";
                UI->ApplyCommand("/vis/open OGLSQt");
                std::cout << " Opening OGLSQt\n";
                break;
#endif
#ifdef G4UI_BUILD_XM_SESSION
            case 3: theSession = new G4UIXm();
                //UI->ApplyCommand("/vis/open ");
                std::cout << " Creating G4UIXm\n";
                break;
#endif
#ifdef G4UI_BUILD_WTCol_SESSION
            case 4: theSession = new G4UIWt();
                std::cout << " Creating G4UIWt\n";
                break;
#endif
#ifdef G4VIS_USE_RAYTRACERX
            case 5: theSession = new G4UIterminal(new G4UItcsh);
                std::cout << " Creating UItcsh\n";
                UI->ApplyCommand("/vis/open RayTracerX");
                std::cout << " Opening RayTracerX\n";
                break;
#endif
            default: theSession = new G4UIterminal(new G4UItcsh); // no G4VIS_USE or no use_visualization
                std::cout << " Creating default UItcsh: either you called no specific visualisation driver or the one you asked for has not been compiled against G4!\n";
        }
//#endif
    }

//#ifdef G4VIS_USE
    if (visu) {
        // default visualization parameters
        UI->ApplyCommand("/vis/sceneHandler/attach");
        UI->ApplyCommand("/vis/viewer/set/upVector 0 0 1");
        UI->ApplyCommand("/vis/viewer/set/viewpointThetaPhi 90 0");
        UI->ApplyCommand("/vis/viewer/zoom 1");
        UI->ApplyCommand("/vis/viewer/panTo 0. 0.");
        UI->ApplyCommand("/vis/scene/add/axes 1. 1. 1. 1.");
        //UI->ApplyCommand("/tracking/storeTrajectory 1");
        //UI->ApplyCommand("/vis/scene/add/trajectories");
        //UI->ApplyCommand("/vis/viewer/refresh");
        //UI->ApplyCommand("/vis/viewer/update");
        G4int visuType = static_cast<G4int>(db["visualization_type"]);  //Choose between visu with wireframe or surface
        if (visuType) {
            UI->ApplyCommand("/vis/viewer/set/style surface");
        }
        else {
            UI->ApplyCommand("/vis/viewer/set/style wireframe");
        }
        G4int visuAccumulate = static_cast<G4int>(db["accumulate_event_action"]); //Option to visualize all the events or just the last one
        if (visuAccumulate) {
            UI->ApplyCommand("/vis/scene/endOfEventAction accumulate 10000");
        }
		UI->ApplyCommand("/vis/viewer/set/auxiliaryEdge");
        std::cout<<"============= End setting of visualization ============\n";
    }
//#endif

    if (not batch) {
        std::cout << "============= Beginning interactive session ============\n";
        theSession->SessionStart();
    }   
    UI->ApplyCommand("/control/execute "+macfile);
    if (not batch) {
        theSession->PauseSessionStart("G4_pause> ");
    }

//#ifdef G4VIS_USE
    G4cout << "Deleting vis manager..." << G4endl;
    delete visManager;
    G4cout << "Vis manager deleted." << G4endl;
//#endif
    delete theSession;

    std::cout.flush(); std::cerr.flush();
    std::cout << " End of EnerGe_sim\n\n";

    return 0;
    
    // RunManager got the ownership of other objects and deletes them properly.
}

