cmake_minimum_required(VERSION 2.8...3.20.5)

#----------------------------------------------#
## Declare the project
project(simulation)

#----------------------------------------------#
## Look for Qt5 and its components
find_package(Qt5 COMPONENTS Core Gui Widgets PrintSupport QUIET)
if (Qt5_FOUND)
  message(STATUS "Qt5 has been found and can be used in G4 viewers")
endif()

#--------------------------#
## Find the correct version of Geant4 with necessary options
find_package(Geant4 10.1 REQUIRED CONFIG)
if ( Geant4_FOUND )
	find_package(Geant4 10.1 COMPONENTS qt vis_opengl_x11 vis_raytracer_x11 CONFIG QUIET)
	if ( NOT Geant4_qt_FOUND OR NOT Geant4_vis_opengl_x11_FOUND OR NOT Geant4_vis_raytracer_x11_FOUND)
  		message(STATUS "Geant4 was built without Qt or OpenGL or RayTracer support")
	endif()

endif ( Geant4_FOUND )
add_definitions(${Geant4_DEFINITIONS})

#--------------------------#
## Add Geant4 C++ flags to CMAKE_CXX_FLAGS
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} ${Geant4_CXX_FLAGS}")

## Need to transform the string to list to remove duplicates and some warnings
set(CMAKE_CXX_FLAGS_LIST ${CMAKE_CXX_FLAGS})
separate_arguments(CMAKE_CXX_FLAGS_LIST)
list(REMOVE_DUPLICATES CMAKE_CXX_FLAGS_LIST)
 #-- Wshadow: too much warnings because ROOT uses a variable "s", the variable for second for G4
 #-- W and pedantic: same as Wextra and Wpedantic respectively, already set
list(REMOVE_ITEM CMAKE_CXX_FLAGS_LIST "-W" "-pedantic" "-std=c++98")
## Back to a string
string (REPLACE ";" " " CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS_LIST}")
# message
message(STATUS "Geant4_DEFINITIONS: " ${Geant4_DEFINITIONS})
message(STATUS "Geant4_CXX_FLAGS: " ${Geant4_CXX_FLAGS})
message(STATUS "Updated CMAKE_CXX_FLAGS: " ${CMAKE_CXX_FLAGS})

#----------------------------------------------#
## Unmaintained GLG4Sim features
option( USE_NEUTRON_TH "GLG4sim Thermal Neutron package written for Geant4 9" OFF)
option( USE_SCINTILLATOR "GLG4sim liquid scintillator and optics" OFF)

if( USE_NEUTRON_TH )
add_definitions(-DUSE_NEUTRON_TH)
message(STATUS "GLG4: neutron TH module activated")
endif()

if( USE_SCINTILLATOR )
  add_definitions(-DUSE_SCINTILLATOR)
  message(STATUS "GLG4: liquid scintillators and optics activated")
endif()
#----------------------------------------------------------------------------
# Setup Geant4 include directories and compile definitions
include_directories(
  ${dataformat_SOURCE_DIR}
  ${common_SOURCE_DIR}/include
  ${PROJECT_SOURCE_DIR}/include
  ${simulation_SOURCE_DIR}/GLG4Sim/include
)
if( USE_NEUTRON_TH )
  include_directories( ${simulation_SOURCE_DIR}/NeutronTH/include )
endif()
include_directories(SYSTEM
  ${ROOT_INCLUDE_DIRS}
  ${Geant4_INCLUDE_DIRS}
)

add_subdirectory(GLG4Sim)
if( USE_NEUTRON_TH )
	add_subdirectory(NeutronTH)
endif()

#----------------------------------------------------------------------------
# Locate sources and headers for this project
# NB: headers are included so they will show up in IDEs
file(GLOB sources ${simulation_SOURCE_DIR}/src/*.cc)
file(GLOB headers ${simulation_SOURCE_DIR}/include/*.hh  )

#----------------------------------------------------------------------------
# Add the executable, and link it to the Geant4 libraries
add_executable(EnerGe_sim EnerGe_sim.cc ${sources} ${headers})
list(APPEND libraries ${ROOT_LIBRARIES} ${Geant4_LIBRARIES} GLG4Sim EnerGeCommon DataDef)
if( USE_NEUTRON_TH )
	list(APPEND libraries NeutronTH)
endif()
target_link_libraries(EnerGe_sim ${libraries})
set_target_properties(EnerGe_sim PROPERTIES RUNTIME_OUTPUT_DIRECTORY ${simulation_SOURCE_DIR})
install(TARGETS EnerGe_sim DESTINATION ${CMAKE_INSTALL_FULL_BINDIR})
install(FILES 
    data/EnerGe_Options.dat
    data/Physics/AtmosphericNeutronFluxData.dat
DESTINATION data)

#----------------------------------------------#
# --creating symbolic link EnerGe_sim->$G4WORKDIR/().../EnerGe_sim--#
# For now, work only for Mac and Linux system...

if ($ENV{G4WORKDIR})
  if(${CMAKE_SYSTEM_NAME} MATCHES "Darwin")
    set(LinkDir $ENV{G4WORKDIR}/bin/Darwin-g++/)
  else()
    set(LinkDir $ENV{G4WORKDIR}/bin/Linux-g++/)
  endif()

  add_custom_command(
    OUTPUT ${LinkDir}/EnerGe_sim_new
    COMMAND /bin/ln
    ARGS -s ${simulation_SOURCE_DIR}/EnerGe_sim ${LinkDir}/EnerGe_sim_new
    COMMENT "Symbolic link")

  add_custom_target(SimulationSymLink ALL DEPENDS ${LinkDir}/EnerGe_sim_new)
  add_dependencies(SimulationSymLink ${simulation_SOURCE_DIR}/EnerGe_sim)
endif()
